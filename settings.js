exports.themeLocationEU = './wp-content/themes/somfyEU/';
exports.themeLocationPRO = './wp-content/themes/somfyPRO/';
exports.pluginLocation = './wp-content/plugins/somfy-site-manager/';
exports.urlToPreview = 'http://localhost/somfy';
