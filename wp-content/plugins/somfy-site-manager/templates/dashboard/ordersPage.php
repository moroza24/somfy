<div class="wrap ordersPage__container">
    <h1 class="wp-heading-inline">ניהול הזמנות</h1>
    <hr class="wp-header-end">
 
    <!-- Orders -->
    <div class="orders_resultes__container">
        <div id="orders__container" class="orders_container">
            <div class="results_header">
                <div class="sorting_filters_container">
                    <p class="title">סדר לפי: </p>
                    <a class="sf_btn sf_btn__small sort_orders_btn" 
                        data-order='asc' data-for="orders_results">תאריך</a>
                </div>
                <div class="search_input__container">
                    <input type="text" id="tranid_search_input" placeholder="מספר הזמנה">
                    <button id="submit_tranid_search_input"><i class="fas fa-search"></i></button>
                </div>
                <div id="total_results_counter"></div>
            </div>

            <div class="loader" id="orders__loader"><div class="spinner-loader"></div>טוען הזמנות...</div>
            <div id="no_orders__message" class="hidden">לא נמצאו תוצאות</div>
    
            <div class="results" id="orders_results">
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">מס הזמנה</th>
                            <th scope="col">לקוח</th>
                            <th scope="col">תאריך</th>
                            <th scope="col">סטטוס</th>
                            <th scope="col">שולם</th>
                            <th scope="col">סונכרן</th>
                            <th scope="col">סה"כ</th>
                            <th scope="col">פעולות</th>
                        </tr>
                    </thead>
                    <tbody data-for="orders_results">
                        
                    </tbody>
                </table>

                <div class="pagination__container">
                    <a id="prev_page"><i class="fas fa-chevron-right" alt="prev"></i></a>
                    <div class="pagination_pages">
                    </div>
                    <a id="next_page"><i class="fas fa-chevron-left" alt="next"></i></a>
                </div>
            </div>
        </div>

    </div>
</div>

<div id="order_details_popup" class="popup__overlay">
    
    <div class="popup__container">
       
        <div class="container_header">
            <h2>הזמנה מספר: <span id="order_details__order_tranid"></span></h2>
            <a class="popup__overlay__close" id="order_details_popup__close">
                <i class="fas fa-times"></i>
            </a>
        </div>
        
        <div class="container_body">
            <div class="loader" id="order_details__loader"><div class="spinner-loader"></div>טוען פרטים...</div>
            <div id="no_ordersitems__message" class="hidden">לא נמצאו שורות להזמנה</div>

            <!-- Order details results -->
            <div id="order_details__results">
                <div class="order_status">סטטוס: 
                    <span id="order_details__order_status"></span>
                </div>
                <br>
                <div class="order_inv">
                    <ul id="order_details__order_inv"></ul>
                </div>

                <table class="table" id="order_details__orderitems">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">מק"ט</th>
                            <th scope="col">שם מוצר</th>
                            <th scope="col">כמות</th>
                            <th scope="col">כמות שסופקה</th>
                            <th scope="col">מחיר</th>
                        </tr>
                    </thead>
                    <tbody data-for="order_details__orderitems">
                        
                    </tbody>
                </table>

                <div class="order_details__summery">
                    <div class="order_details__subtotal">מחיר ביניים 
                        <span id="order_details__subtotal"></span>
                    </div>
<!-- 
                    <div class="order_details__deliverytotal">משלוח
                        <span id="order_details__deliverytotal"></span>
                    </div> -->

                    <hr>

                    <div class="order_details__total">סכום סופי
                        <span id="order_details__total"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>