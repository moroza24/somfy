
<?php
    use \Inc\Classes\SyncLog;

    $SyncLog = new SyncLog();
    $logRecords = $SyncLog->getLog();

    $lastLog = $SyncLog->getLog(['limit' => 1]);
    $syncLog_lastLog = $lastLog ? $lastLog[0] : null;

?>

<div class="wrap" id="sync_log_dashboard" style="direction: rtl;">
    <h1 class="wp-heading-inline" style="direction: rtl;">לוג סינכרון ERP</h1>

    <div class="container">
        <!-- Last sync log status -->
        <div>
            <h2>סינכרון אחרון</h2>
            <?php 
                if(!$syncLog_lastLog){
                    echo 'לא זמין';
                } else { 
            ?>
                <p>סטטוס: <?php echo $syncLog_lastLog->status; ?></p>
                <?php if($syncLog_lastLog->error){ ?>
                    <p>שגיאה: <?php echo $syncLog_lastLog->error; ?></p>
                    <p>תאריך עדכון: <?php echo $syncLog_lastLog->update_date; ?></p>
                <?php } ?>
                <p>תאריך התחלה: <?php echo $syncLog_lastLog->start_date; ?></p>
                <p>תאריך סיום: <?php echo $syncLog_lastLog->end_date; ?></p>
            <?php } ?>
        </div>
    </div>

    <?php 
        if(!$logRecords){
            echo 'לא נמצאו רשומות';
        } else { 
    ?>
        <!-- Filter Bar -->
        <div class="filter_bar">
            <input type="datetime" name="fromDate" id="fromDate" placeholder="From Date:">
            <input type="datetime" name="toDate" id="toDate" placeholder="To Date:">
            <select name="sync_type" id="sync_type">
                <option value="0" selected>Sync Type</option>
                <option value="products">Products</option>
                <option value="orders">Orders</option>
            </select>
        </div>

        <!-- Log Table -->
        <table>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>sync_type</th>
                    <th>status</th>
                    <th>retries</th>
                    <th>error</th>
                    <th>start_date</th>
                    <th>end_date</th>
                    <th>update_date</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    for($i = 0; $i < count($logRecords); $i++){
                        echo "<tr>
                            <td>" . $logRecords[$i]->ID ."</td>
                            <td>" . $logRecords[$i]->sync_type ."</td>
                            <td>" . $logRecords[$i]->status ."</td>
                            <td>" . $logRecords[$i]->retries ."</td>
                            <td>" . $logRecords[$i]->error ."</td>
                            <td>" . $logRecords[$i]->start_date ."</td>
                            <td>" . $logRecords[$i]->end_date ."</td>
                            <td>" . $logRecords[$i]->update_date ."</td>
                        </tr>"; 
                    }
                ?>
            </tbody>
        </table>
    <?php } ?>
</div>

