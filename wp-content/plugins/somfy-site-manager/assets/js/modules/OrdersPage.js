
import $ from 'jquery';


class OrdersPage {

    constructor() {
        this.pagination = {
            page: 1,
            limit: 20
        };
        this.sort = 'DESC';
        this.tranid = '';

        this.orders__loader = $('#orders__loader');
        this.no_orders__message = $('#no_orders__message');
        this.orders_results = $('#orders_results');

        this.orderDetails_popup = $('#order_details_popup');
        this.orderDetails_popup__close = $('#order_details_popup__close');
        this.orderDetails__loader = $('#order_details__loader');
        this.no_ordersitems__message = $('#no_ordersitems__message');
        this.order_details__results = $('#order_details__results');
        this.sort_orders_btn = $('.sort_orders_btn');

        this.submit_tranid_search_input = $('#submit_tranid_search_input');
        this.isLoading = false;
        
        this.initEvents();
        this.getOrders();
        
    }


    initEvents(){
        this.submit_tranid_search_input.on('click',()=>{
            let tranid_search_input = $('#tranid_search_input').val();
            
            this.tranid = tranid_search_input;
            this.getOrders();
        });

        $(document).on('keydown', this.keyPressDispatch.bind(this));

    }


    getOrders(){
        if(this.isLoading) return;
        this.isLoading = true;

        this.orders__loader.show();
        this.no_orders__message.addClass('hidden');
        this.orders_results.hide();

        console.log(this.pagination);
        console.log(this.sort);
        $.ajax({
            url : somfyData.ajax_url,
            type : 'POST',
            data : {
                action : 'somfy_admin_get_order',
                pagination: this.pagination,
                sort: this.sort,
                tranid: this.tranid,
                nonce: somfyData.ajax_nonce
            },
            dataType: "json",
            success: (response) =>{
                console.log(response);
                if(response.status){
                    if(response.orders && response.orders.length > 0){
                        // print orders
                        this.printOrders(response.orders);
                        this.setPagination(response.total);
                        $('#total_results_counter').html('סה"כ תוצאות: ' + response.total + " (" + response.orders.length + ")");
                    } else {
                        // show no orders message
                        this.no_orders__message.removeClass('hidden');
                    }
                } else {
                    // SHOW ERROR
                    console.log(response);
                }
                this.orders__loader.hide();
                this.isLoading = false;
            },
            error:  (jqXHR, status) => {
                console.log(jqXHR);
                console.log(status);
            }
        });
    }

    printOrders(orders) {
        let ordersResultsTbody = $('tbody[data-for="orders_results"]');
        ordersResultsTbody.html('');
        orders.forEach(order => {
            let orderTrHtml = `<tr>
                    <th scope='row'>
                        <a class='show_order_items' data-order='${order.ID}'>${order.data.tranid}</a></td>
                    </th>
                    <td>
                        <a href='${order.customerData.edit_user_link}' target='_blank' alt='Edit user'>
                            ${order.customerData.companyname == '' || order.customerData.companyname == null ? order.customerData.first_name + ' ' + order.customerData.last_name : order.customerData.companyname}</td>
                        </a>
                    <td>${order.data.saleseffectivedate}</td>
                    <td>${order.data.status}</td>
                    <td class='sm_ceneter'>${order.data.is_payed == 1 ? '<i class="far fa-check-circle"></i>' : '<i class="fas fa-times"></i>'}</td>
                    <td class='sm_ceneter'>${order.data.is_synced == 1 ? '<i class="far fa-check-circle"></i>' : '<i class="fas fa-times"></i>'}</td>
                    <td>${this.numberWithCommas(order.data.total)} ₪</td>
                    <td class='order_actions'>
                        <a class='show_order_items' data-order='${order.ID}' alt='פרטי הזמנה'><i class="far fa-list-alt"></i></a>
                    </td>
                </tr>`;

            ordersResultsTbody.append(orderTrHtml);
        });
        this.orders_results.show();

        this.initShowOrderItemsEvent();
    }


    setPagination(total){
        let pagination__container = $('.pagination__container');
        let prev_page = $('#prev_page');
        let next_page = $('#next_page');
        let pagination_pages = $('.pagination_pages');
        pagination_pages.html('');

        if(total > this.pagination.limit){
            // calc how many pages needed
            let totalPages = Math.ceil(total/this.pagination.limit);

            // print pages
            pagination_pages.html('');
            for(let i = 1; i <= totalPages; i++){
                pagination_pages.append(`<a class="set_page ${this.pagination.page == i ? 'active' : ''}" data-page="${i}">${i}</a>`)
            }

            console.log(this.pagination.page);
            // show/hide next and previous btn
            if(this.pagination.page == 1){
                prev_page.hide();
            } else {
                prev_page.show();
            }
            
            if(this.pagination.page == totalPages){
                next_page.hide();
            } else {
                next_page.show();
            }

            prev_page.off('click').on('click', ()=>{
                this.pagination.page = parseInt(this.pagination.page) - 1;
                this.getOrders(); 
            });

            next_page.off('click').on('click', ()=>{
                this.pagination.page = parseInt(this.pagination.page) + 1;
                this.getOrders(); 
            })

            $('.set_page').off('click').on('click', (e)=>{
                this.pagination.page = e.currentTarget.dataset.page;
                this.getOrders(); 
            })
        }
    }


    initShowOrderItemsEvent(){
        let showOrderItemsBtn = $('.show_order_items');

        showOrderItemsBtn.on('click', (e)=>{
            let orderID = e.currentTarget.dataset.order;
            
            this.resetOrderInfo();
            this.orderDetails__loader.show();
            this.no_ordersitems__message.hide();
            this.order_details__results.hide();
            this.orderDetails_popup.fadeIn('fast');

            $.ajax({
                url : somfyData.ajax_url,
                type : 'POST',
                data : {
                    action : 'somfy_order_details',
                    orderID: orderID,
                    nonce: somfyData.ajax_nonce
                },
                dataType: "json",
                success: (response) =>{
                    console.log(response);
                    if(response.status){
                        if(response.items && response.items.length > 0){
                            this.printOrderItems(response.items);
                            this.printOrderInfo(response.orderInfo);
                            this.order_details__results.show();
                        } else {
                            // show no results found
                            this.no_ordersitems__message.show();
                        }

                        // hide loader
                        this.orderDetails__loader.hide();
                    } else {
                        // SHOW ERROR
                        console.log(response);
                    }
    
                },
                error:  (jqXHR, status) => {
                    console.log(jqXHR);
                    console.log(status);
                }
            });
        });

        this.orderDetails_popup__close.on('click', () =>{
            this.orderDetails_popup.fadeOut('fast');
        })

        this.sort_orders_btn.on('click', (e) =>{
            let newOrder = this.sort == 'ASC' ? 'DESC' : 'ASC';
            this.sort = newOrder;
            this.getOrders();
        })
    }


    
    printOrderItems(items){
        let orderDetails__orderitems = $('tbody[data-for="order_details__orderitems"]');
        console.log(items);
        items.forEach(item => {
            let orderItemTrHtml = `<tr>
                    <th scope='row'>
                        ${item.itemid}
                    </th>
                    <td>${item.post_title}</td>
                    <td>${item.quantity}</td>
                    <td>${item.quantityshiprecv}</td>
                    <td>${item.grossamount}</td>
                </tr>`;

            orderDetails__orderitems.append(orderItemTrHtml);
        });
    }


    printOrderInfo(orderInfo){
        let invHtml = '';
        if(orderInfo.inv && orderInfo.inv.length > 0){
            orderInfo.inv.forEach(inv => {
                invHtml += `<li class='inv'>${inv.tranid}</li>`;
            });
        }
        
        $('#order_details__subtotal').html("₪" + this.numberWithCommas(orderInfo.subtotal));
        $('#order_details__total').html("₪" + this.numberWithCommas(orderInfo.total));
        $('#order_details__order_tranid').html(orderInfo.tranid);
        $('#order_details__order_inv').html(invHtml);
        $('#order_details__order_status').html(orderInfo.status);
    }


    resetOrderInfo(){
        $('#order_details__subtotal').html("");
        $('#order_details__total').html("");
        $('#order_details__order_tranid').html("");
        $('#order_details__order_status').html("");
    }


    numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }


    keyPressDispatch(e){
        if(e.keyCode == 13 && !this.isLoading && $('input#tranid_search_input').is(':focus')){
            this.submit_tranid_search_input.trigger('click');
        }
    }
}

export default OrdersPage;
