
import $ from 'jquery';


class SettingPage {

    constructor() {
        this.formTables = $('table.form-table');
        this.triggers = $('#wpbody-content form>h2');

        this.initEvents();
    }

    initEvents(){
        this.triggers.on('click', (e)=>{
            let table = $(e.currentTarget.nextElementSibling);
            table.fadeToggle('fast');
        });

        // $('#wpbody-content').css('direction','ltr');
        $('#wpbody-content form').find('h2').addClass('form_trigger');
        $('#wpbody-content form').find('.form-table').addClass('form-table_content');
        // $('#wpbody-content form').find('p.submit').addClass('submit_left');
    }

}

export default SettingPage;
