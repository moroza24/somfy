// 3rd party packages from NPM
import $ from 'jquery';

// Our modules / classes

// Pages
import SettingPage from './modules/SettingPage';
import OrdersPage from './modules/OrdersPage';


$.fn.extend({
    toggleText: function(a, b){
        return this.text(this.text() == b ? a : b);
    }
});


// Instantiate a new object using our modules/classes
$(document).ready(() => {
    var pathname = window.location.search;

    // Home page scripts
    if(pathname.includes('somfy_site_manager_plugin_settings')){
        var settingPage = new SettingPage();
    }

    // Order page scripts
    if(pathname.includes('somfy_site_manager_orders')){
        var ordersPage = new OrdersPage();
    }
});