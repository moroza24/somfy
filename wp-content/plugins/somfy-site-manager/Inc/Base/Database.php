<?php
/**
 * @package  SomfySiteManager
 */

namespace Inc\Base;

use \Inc\Base\BaseController;

class Database extends BaseController
{
    function register(){
        register_activation_hook( __FILE__, array($this, 'db_installer'));
        $this->db_installer();


    }

    function db_installer(){
        global $wpdb;

        $charset_collate = $wpdb->get_charset_collate();

        $base_prefix = $wpdb->base_prefix;

        /**
         * Global database tables
         */
        
         // User discount codes Database Table
        $discountcodes_db = $wpdb->base_prefix . "discountcodes";
        $discountcodes_db_version = '1.1.2';

        if ( $wpdb->get_var( "SHOW TABLES LIKE '{$discountcodes_db}'" ) != $discountcodes_db ) {

            $discountcodes_db_sql = "CREATE TABLE $discountcodes_db (
                    ID bigint(20) NOT NULL AUTO_INCREMENT,
                    `internalid` varchar(20) NOT NULL, 
                    `custrecord_discountcode` varchar(20) NULL DEFAULT NULL, 
                    `custrecord_discountgroup` text NULL DEFAULT NULL, 
                    `custrecord_discount_percent` varchar(20) NULL DEFAULT NULL, 
                    PRIMARY KEY  (ID)
            )    $charset_collate;";


            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $discountcodes_db_sql );
            add_option( 'discountcodes_db_version', $discountcodes_db_version );
        }

        // Sync Log Database Table
        $synclog_db = $wpdb->base_prefix . "synclog";
        $synclog_db_version = '1.2.2';

        if ( $wpdb->get_var( "SHOW TABLES LIKE '{$synclog_db}'" ) != $synclog_db ) {

            $synclog_db_sql = "CREATE TABLE $synclog_db (
                    ID bigint(20) NOT NULL AUTO_INCREMENT,
                    `sync_type` varchar(20) NOT NULL, /** products, customers */
                    `status` varchar(20) NOT NULL, /** started, retry, ended */
                    `retries` int(11) NOT NULL DEFAULT 0, 
                    `error` text NULL DEFAULT NULL, /** no response fron netsuite, ... */
                    `updated` int(11) NOT NULL DEFAULT 0, 
                    `start_date` datetime NOT NULL default CURRENT_TIMESTAMP,
                    `end_date` datetime NULL,
                    `update_date` datetime NOT NULL default CURRENT_TIMESTAMP,
                    PRIMARY KEY  (ID)
            )    $charset_collate;";


            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $synclog_db_sql );
            add_option( 'synclog_db_version', $synclog_db_version );
        }

        // somfy Log Database Table
        $somfylog_db = $wpdb->base_prefix . "somfylog";
        $somfylog_db_version = '1.0.1';

        if ( $wpdb->get_var( "SHOW TABLES LIKE '{$somfylog_db}'" ) != $somfylog_db ) {

            $somfylog_db_sql = "CREATE TABLE $somfylog_db (
                    ID bigint(20) NOT NULL AUTO_INCREMENT,
                    `title` text NULL DEFAULT NULL, 
                    `message` text NULL DEFAULT NULL, 
                    `create_date` datetime NOT NULL default CURRENT_TIMESTAMP,
                    PRIMARY KEY  (ID)
            )    $charset_collate;";


            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $somfylog_db_sql );
            add_option( 'somfylog_db_version', $somfylog_db_version );
        }


        /**
         * EU site database tables
         */

        // Order table

        // OrderItem table
        $orderitem_db = EU_SITE_PREFIX . "orderitem";
        $orderitem_db_version = '1.5.0';

        if ( $wpdb->get_var( "SHOW TABLES LIKE '{$orderitem_db}'" ) != $orderitem_db ) {

            $orderitem_db_sql = "CREATE TABLE $orderitem_db (
                    ID bigint(20) NOT NULL AUTO_INCREMENT,
                    `internalid` bigint(20) NULL,
                    `tranid` varchar(20) NULL,
                    `orderid` bigint(20) NOT NULL,
                    `lineuniquekey` varchar(20) NULL, 
                    `item` varchar(20) NOT NULL, 
                    `quantity` int(11) NOT NULL DEFAULT 0, 
                    `quantityshiprecv` int(11) NOT NULL DEFAULT 0, 
                    `quantitypicked` int(11) NOT NULL DEFAULT 0, 
                    `quantityfulfilled` int(11) NOT NULL DEFAULT 0, 
                    `quantitybilled` int(11) NOT NULL DEFAULT 0, 
                    `pricelevel` varchar(20) NOT NULL, 
                    `custcol_originalitemrate` FLOAT NULL DEFAULT 0,
                    `rate` FLOAT NULL DEFAULT 0,
                    `custcol_discountcode` varchar(20) NOT NULL, 
                    `custcol_discountpercentage` FLOAT NULL DEFAULT 0,
                    `amount` FLOAT NULL DEFAULT 0,
                    `taxamount` FLOAT NULL DEFAULT 0,
                    `grossamount` FLOAT NULL DEFAULT 0,
                    `closed` tinyint(2) NOT NULL DEFAULT 0, 
                    `custbody_is_upgrade_required` tinyint(2) NOT NULL DEFAULT 0, 
                    `custcol_awaitingcompletion` tinyint(2) NOT NULL DEFAULT 0, 
                    PRIMARY KEY  (ID)
            )    $charset_collate;";


            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $orderitem_db_sql );
            add_option( 'orderitem_db_version', $orderitem_db_version );
        }

        // payment_log table
        $payment_log_db = EU_SITE_PREFIX . "payment_log";
        $payment_log_db_version = '1.4.0';

        if ( $wpdb->get_var( "SHOW TABLES LIKE '{$payment_log_db}'" ) != $payment_log_db ) {

            $payment_log_db_sql = "CREATE TABLE $payment_log_db (
                    ID bigint(20) NOT NULL AUTO_INCREMENT,
                    `user_id` varchar(20) NOT NULL, 
                    `internalid` bigint(20) NULL,
                    `orderid` bigint(20) NOT NULL,
                    `status` varchar(20) NULL,
                    `method` varchar(20) NULL,
                    `data` text NULL, 
                    `create_date` datetime NOT NULL default CURRENT_TIMESTAMP,
                    `update_date` datetime NOT NULL default CURRENT_TIMESTAMP,
                    PRIMARY KEY  (ID)
            )    $charset_collate;";

            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $payment_log_db_sql );
            add_option( 'payment_log_db_version', $payment_log_db_version );
        }


        // Calc inputs
        $calculator_inputs_db = EU_SITE_PREFIX . "calculator_inputs";
        $calculator_inputs_db_version = '2.6.0';

        if ( $wpdb->get_var( "SHOW TABLES LIKE '{$calculator_inputs_db}'" ) != $calculator_inputs_db ) {

            $calculator_inputs_db_sql = "CREATE TABLE $calculator_inputs_db (
                    ID bigint(20) NOT NULL AUTO_INCREMENT,
                    `orderID` varchar(20) NOT NULL,
                    `userID` varchar(20) NOT NULL,
                    `productID` varchar(20) NOT NULL,
                    `userInput` text NOT NULL,
                    `create_date` datetime NOT NULL default CURRENT_TIMESTAMP,
                    PRIMARY KEY  (ID)
            )    $charset_collate;";


            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $calculator_inputs_db_sql );
            add_option( 'calculator_inputs_db_version', $calculator_inputs_db_version );
        }
        
        // Cart item table
        $cartitem_db = EU_SITE_PREFIX . "cartitem";
        $cartitem_db_version = '2.0.0';

        if ( $wpdb->get_var( "SHOW TABLES LIKE '{$cartitem_db}'" ) != $cartitem_db ) {

            $cartitem_db_sql = "CREATE TABLE $cartitem_db (
                    ID bigint(20) NOT NULL AUTO_INCREMENT,
                    `user_id` varchar(20) NOT NULL, 
                    `cart_items` text NULL, 
                    `create_date` datetime NOT NULL default CURRENT_TIMESTAMP,
                    `update_date` datetime NOT NULL default CURRENT_TIMESTAMP,
                    PRIMARY KEY  (ID)
            )    $charset_collate;";


            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $cartitem_db_sql );
            add_option( 'cartitem_db_version', $cartitem_db_version );
        }

        // Stock Notification table
        $stock_notification_db = EU_SITE_PREFIX . "stock_notification";
        $stock_notification_db_version = '1.3.0';

        if ( $wpdb->get_var( "SHOW TABLES LIKE '{$stock_notification_db}'" ) != $stock_notification_db ) {

            $stock_notification_db_sql = "CREATE TABLE $stock_notification_db (
                    ID bigint(20) NOT NULL AUTO_INCREMENT,
                    `site` varchar(20) NOT NULL, 
                    `user_email` varchar(100) NOT NULL, 
                    `user_fullname` varchar(100) NOT NULL, 
                    `user_phone` varchar(100) NOT NULL, 
                    `user_id` varchar(20) NULL DEFAULT NULL, 
                    `product_id` varchar(20) NOT NULL, 
                    `required_amount` int(11) NOT NULL DEFAULT 0, 
                    `user_notified` tinyint(2) NOT NULL DEFAULT 0, 
                    `create_date` datetime NOT NULL default CURRENT_TIMESTAMP,
                    `update_date` datetime NOT NULL default CURRENT_TIMESTAMP,
                    PRIMARY KEY  (ID)
            )    $charset_collate;";


            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $stock_notification_db_sql );
            add_option( 'stock_notification_db_version', $stock_notification_db_version );
        }


        // Wishlist
        $wishlist_db = EU_SITE_PREFIX . "wishlist";
        $wishlist_db_version = '1.0.1';

        if ( $wpdb->get_var( "SHOW TABLES LIKE '{$wishlist_db}'" ) != $wishlist_db ) {

            $wishlist_db_sql = "CREATE TABLE $wishlist_db (
                    ID bigint(20) NOT NULL AUTO_INCREMENT,
                    `user_id` varchar(20) NOT NULL, 
                    `product_id` varchar(20) NOT NULL, 
                    `create_date` datetime NOT NULL default CURRENT_TIMESTAMP,
                    PRIMARY KEY  (ID)
            )    $charset_collate;";


            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $wishlist_db_sql );
            add_option( 'wishlist_db_version', $wishlist_db_version );
        }


        // coupon_usage log
        $coupon_usage_db = EU_SITE_PREFIX . "coupon_usage";
        $coupon_usage_db_version = '1.0.1';

        if ( $wpdb->get_var( "SHOW TABLES LIKE '{$coupon_usage_db}'" ) != $coupon_usage_db ) {

            $coupon_usage_db_sql = "CREATE TABLE $coupon_usage_db (
                    ID bigint(20) NOT NULL AUTO_INCREMENT,
                    `user_id` varchar(20) NOT NULL, 
                    `coupon_id` varchar(20) NOT NULL, 
                    `order_id` varchar(20) NOT NULL, 
                    `create_date` datetime NOT NULL default CURRENT_TIMESTAMP,
                    PRIMARY KEY  (ID)
            )    $charset_collate;";


            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $coupon_usage_db_sql );
            add_option( 'coupon_usage_db_version', $coupon_usage_db_version );
        }

         /**
         * PRO site database tables
         */

        // product price table
        $product_pricing_db = PRO_SITE_PREFIX . "product_pricing";
        $product_pricing_db_version = '1.3.1';

        if ( $wpdb->get_var( "SHOW TABLES LIKE '{$product_pricing_db}'" ) != $product_pricing_db ) {

            $product_pricing_db_sql = "CREATE TABLE $product_pricing_db (
                    `key` varchar(100) NOT NULL,
                    `item` varchar(20) NOT NULL, 
                    `customer` varchar(20) NOT NULL, 
                    `quantityrange` varchar(20) NOT NULL,
                    `unitprice` FLOAT NOT NULL DEFAULT 0.00, 
                    `pricelevel` varchar(20) NOT NULL,
                    `currency` varchar(20) NOT NULL,
                    PRIMARY KEY  (`key`),
                    INDEX index_name (`item`,`customer`,c4)
            )    $charset_collate;";


            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $product_pricing_db_sql );
            add_option( 'product_pricing_db_version', $product_pricing_db_version );
        }
        
        // OrderItem table
        $orderitem_db = PRO_SITE_PREFIX . "orderitem";
        $orderitem_db_version = '1.2.1';

        if ( $wpdb->get_var( "SHOW TABLES LIKE '{$orderitem_db}'" ) != $orderitem_db ) {

            $orderitem_db_sql = "CREATE TABLE $orderitem_db (
                    ID bigint(20) NOT NULL AUTO_INCREMENT,
                    `internalid` bigint(20) NULL,
                    `tranid` varchar(20) NULL,
                    `orderid` bigint(20) NOT NULL,
                    `lineuniquekey` varchar(20) NULL, 
                    `item` varchar(20) NOT NULL, 
                    `quantity` int(11) NOT NULL DEFAULT 0, 
                    `quantityshiprecv` int(11) NOT NULL DEFAULT 0, 
                    `quantitypicked` int(11) NOT NULL DEFAULT 0, 
                    `quantityfulfilled` int(11) NOT NULL DEFAULT 0, 
                    `quantitybilled` int(11) NOT NULL DEFAULT 0, 
                    `pricelevel` varchar(20) NOT NULL, 
                    `custcol_originalitemrate` FLOAT NULL DEFAULT 0,
                    `rate` FLOAT NULL DEFAULT 0,
                    `custcol_discountcode` varchar(20) NOT NULL, 
                    `custcol_discountpercentage` FLOAT NULL DEFAULT 0,
                    `amount` FLOAT NULL DEFAULT 0,
                    `taxamount` FLOAT NULL DEFAULT 0,
                    `grossamount` FLOAT NULL DEFAULT 0,
                    `closed` tinyint(2) NOT NULL DEFAULT 0, 
                    `custbody_is_upgrade_required` tinyint(2) NOT NULL DEFAULT 0, 
                    `custcol_awaitingcompletion` tinyint(2) NOT NULL DEFAULT 0, 
                    PRIMARY KEY  (ID)
            )    $charset_collate;";


            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $orderitem_db_sql );
            add_option( 'orderitem_db_version', $orderitem_db_version );
        }


        // Calc inputs
        $calculator_inputs_db = PRO_SITE_PREFIX . "calculator_inputs";
        $calculator_inputs_db_version = '2.6.0';

        if ( $wpdb->get_var( "SHOW TABLES LIKE '{$calculator_inputs_db}'" ) != $calculator_inputs_db ) {

            $calculator_inputs_db_sql = "CREATE TABLE $calculator_inputs_db (
                    ID bigint(20) NOT NULL AUTO_INCREMENT,
                    `orderID` varchar(20) NOT NULL,
                    `userID` varchar(20) NOT NULL,
                    `productID` varchar(20) NOT NULL,
                    `userInput` text NOT NULL,
                    `create_date` datetime NOT NULL default CURRENT_TIMESTAMP,
                    PRIMARY KEY  (ID)
            )    $charset_collate;";


            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $calculator_inputs_db_sql );
            add_option( 'calculator_inputs_db_version', $calculator_inputs_db_version );
        }


        // orderdelivery table
        $orderdelivery_db = PRO_SITE_PREFIX . "orderdelivery";
        $orderdelivery_db_version = '1.2.1';

        if ( $wpdb->get_var( "SHOW TABLES LIKE '{$orderdelivery_db}'" ) != $orderdelivery_db ) {

            $orderdelivery_db_sql = "CREATE TABLE $orderdelivery_db (
                    `name` varchar(20) NOT NULL, 
                    `order_internalid` bigint(20) NOT NULL,
                    `orderid` bigint(20) NOT NULL,
                    `custrecord_fc_dn_related_parent` varchar(20) NULL, 
                    `custrecord_fc_dn_related_if` varchar(20) NULL, 
                    `custrecord_fc_dn_status` varchar(20) NULL, 
                    `custrecord_fc_packed_date` datetime NULL default CURRENT_TIMESTAMP,
                    `custrecord_fc_shipped_date` datetime NULL default CURRENT_TIMESTAMP,
                    PRIMARY KEY  (`name`)
            )    $charset_collate;";


            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $orderdelivery_db_sql );
            add_option( 'orderdelivery_db_version', $orderdelivery_db_version );
        }

        // balance table
        $balance_db = PRO_SITE_PREFIX . "balance";
        $balance_db_version = '1.4.1';

        if ( $wpdb->get_var( "SHOW TABLES LIKE '{$balance_db}'" ) != $balance_db ) {

            $balance_db_sql = "CREATE TABLE $balance_db (
                    `key` varchar(100) NOT NULL,
                    `tranid` varchar(20) NULL,
                    `internalid` varchar(20) NULL,
                    `type` varchar(20) NOT NULL,
                    `entityid` varchar(20) NULL, 
                    `createdfrom` varchar(20) NULL, 
                    `orderID` varchar(20) NULL, 
                    `order_tranid` varchar(20) NULL, 
                    `statusref` varchar(20) NOT NULL, 
                    `amount` FLOAT NOT NULL DEFAULT 0,
                    `fxamount` FLOAT NOT NULL DEFAULT 0,
                    `currency` varchar(10) NOT NULL DEFAULT '1',
                    `daysopen` int(11) NULL,
                    `trandate` varchar(10) NULL,
                    PRIMARY KEY  (`key`)
            )    $charset_collate;";


            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $balance_db_sql );
            add_option( 'balance_db_version', $balance_db_version );
        }


        // Cart item table
        $cartitem_db = PRO_SITE_PREFIX . "cartitem";
        $cartitem_db_version = '1.0.1';

        if ( $wpdb->get_var( "SHOW TABLES LIKE '{$cartitem_db}'" ) != $cartitem_db ) {

            $cartitem_db_sql = "CREATE TABLE $cartitem_db (
                    ID bigint(20) NOT NULL AUTO_INCREMENT,
                    `user_id` varchar(20) NOT NULL, 
                    `cart_items` text NULL, 
                    `create_date` datetime NOT NULL default CURRENT_TIMESTAMP,
                    `update_date` datetime NOT NULL default CURRENT_TIMESTAMP,
                    PRIMARY KEY  (ID)
            )    $charset_collate;";


            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $cartitem_db_sql );
            add_option( 'cartitem_db_version', $cartitem_db_version );
        }

        // Stock Notification table
        $stock_notification_db = PRO_SITE_PREFIX . "stock_notification";
        $stock_notification_db_version = '1.0.1';

        if ( $wpdb->get_var( "SHOW TABLES LIKE '{$stock_notification_db}'" ) != $stock_notification_db ) {

            $stock_notification_db_sql = "CREATE TABLE $stock_notification_db (
                    ID bigint(20) NOT NULL AUTO_INCREMENT,
                    `site` varchar(20) NOT NULL, 
                    `user_email` varchar(100) NOT NULL, 
                    `user_fullname` varchar(100) NOT NULL, 
                    `user_phone` varchar(100) NOT NULL, 
                    `user_id` varchar(20) NULL DEFAULT NULL, 
                    `product_id` varchar(20) NOT NULL, 
                    `required_amount` int(11) NOT NULL DEFAULT 0, 
                    `user_notified` tinyint(2) NOT NULL DEFAULT 0, 
                    `create_date` datetime NOT NULL default CURRENT_TIMESTAMP,
                    `update_date` datetime NOT NULL default CURRENT_TIMESTAMP,
                    PRIMARY KEY  (ID)
            )    $charset_collate;";


            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $stock_notification_db_sql );
            add_option( 'stock_notification_db_version', $stock_notification_db_version );
        }


        // Wishlist
        $wishlist_db = PRO_SITE_PREFIX . "wishlist";
        $wishlist_db_version = '1.0.1';

        if ( $wpdb->get_var( "SHOW TABLES LIKE '{$wishlist_db}'" ) != $wishlist_db ) {

            $wishlist_db_sql = "CREATE TABLE $wishlist_db (
                    ID bigint(20) NOT NULL AUTO_INCREMENT,
                    `user_id` varchar(20) NOT NULL, 
                    `product_id` varchar(20) NOT NULL, 
                    `create_date` datetime NOT NULL default CURRENT_TIMESTAMP,
                    PRIMARY KEY  (ID)
            )    $charset_collate;";


            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $wishlist_db_sql );
            add_option( 'wishlist_db_version', $wishlist_db_version );
        }


        // coupon_usage log
        $coupon_usage_db = PRO_SITE_PREFIX . "coupon_usage";
        $coupon_usage_db_version = '1.0.1';

        if ( $wpdb->get_var( "SHOW TABLES LIKE '{$coupon_usage_db}'" ) != $coupon_usage_db ) {

            $coupon_usage_db_sql = "CREATE TABLE $coupon_usage_db (
                    ID bigint(20) NOT NULL AUTO_INCREMENT,
                    `user_id` varchar(20) NOT NULL, 
                    `coupon_id` varchar(20) NOT NULL, 
                    `order_id` varchar(20) NOT NULL, 
                    `create_date` datetime NOT NULL default CURRENT_TIMESTAMP,
                    PRIMARY KEY  (ID)
            )    $charset_collate;";


            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $coupon_usage_db_sql );
            add_option( 'coupon_usage_db_version', $coupon_usage_db_version );
        }


        // RMA head table
        $rma_head_db = PRO_SITE_PREFIX . "rma_head";
        $rma_head_db_version = '1.4.1';

        if ( $wpdb->get_var( "SHOW TABLES LIKE '{$rma_head_db}'" ) != $rma_head_db ) {

            $rma_head_db_sql = "CREATE TABLE $rma_head_db (
                    ID bigint(20) NOT NULL AUTO_INCREMENT,
                    `externalid` varchar(20) NOT NULL, 
                    `internalid` varchar(20) NULL DEFAULT NULL, 
                    `tranid` varchar(20) NULL DEFAULT NULL, 
                    `statusref` varchar(20) NULL DEFAULT NULL, 
                    `memo` text NULL DEFAULT NULL, 
                    `custbody_rmatype` varchar(20) NULL DEFAULT NULL, 
                    `custbody_refundapproved` varchar(20) NULL DEFAULT NULL, 
                    `custbody_shippingtracknumber` varchar(20) NULL DEFAULT NULL, 
                    `user_id` varchar(20) NOT NULL, 
                    `entity` varchar(20) NOT NULL, 
                    `vatregnumber` varchar(100) NOT NULL, 
                    `companyname` varchar(100) NULL, 
                    `contactname` varchar(100) NULL DEFAULT NULL, 
                    `email` varchar(100) NOT NULL, 
                    `phone` varchar(100) NULL DEFAULT NULL, 
                    `create_date` datetime NOT NULL default CURRENT_TIMESTAMP,
                    `datecreated` varchar(20) NULL DEFAULT NULL, 
                    `synced` tinyint(2) NOT NULL DEFAULT 0, 
                    PRIMARY KEY  (ID)
            )    $charset_collate;";


            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $rma_head_db_sql );
            add_option( 'rma_head_db_version', $rma_head_db_version );
        }

        // RMA item table
        $rma_items_db = PRO_SITE_PREFIX . "rma_items";
        $rma_items_db_version = '1.2.1';

        if ( $wpdb->get_var( "SHOW TABLES LIKE '{$rma_items_db}'" ) != $rma_items_db ) {

            $rma_items_db_sql = "CREATE TABLE $rma_items_db (
                    ID bigint(20) NOT NULL AUTO_INCREMENT,
                    `externalid` varchar(20) NOT NULL, 
                    `internalid` varchar(20) NULL DEFAULT NULL, 
                    `tranid` varchar(20) NULL DEFAULT NULL, 
                    `item` varchar(20) NOT NULL, 
                    `custcol_rmaclassification` varchar(20) NOT NULL, 
                    `custcol_invoicereference` varchar(20) NULL DEFAULT NULL, 
                    `quantity` int(11) NOT NULL DEFAULT 0, 
                    PRIMARY KEY  (ID)
            )    $charset_collate;";


            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $rma_items_db_sql );
            add_option( 'rma_items_db_version', $rma_items_db_version );
        }
    }
}