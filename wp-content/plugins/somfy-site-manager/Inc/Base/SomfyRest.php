<?php
/**
 * @package SomfySiteManager
 */

namespace Inc\Base;

use \Inc\Classes\SyncServer;

class SomfyRest extends \WP_REST_Controller {
    public function register() {

        add_action('rest_api_init', array( $this, "recivePriceUpdatesRoute"));
    }

    function somfy_must_login() {
        echo "You must be logged in!";
        die();
    }

    
    public function recivePriceUpdatesRoute(){
        $SyncServer = new SyncServer();
        register_rest_route('smf/v1', 'prices', [
            'methods' => 'POST',
            'callback' => array( $SyncServer, "recivePriceUpdates")
        ]);
    }
}