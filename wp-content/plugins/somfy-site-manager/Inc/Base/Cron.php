<?php
/**
 * @package  SomfySiteManager
 */

namespace Inc\Base;

use \Inc\Base\BaseController;
use \Inc\Classes\SyncServer;
use \Inc\Classes\SyncLog;
use \Inc\Classes\StockNotifictaion;

class Cron extends BaseController{
    public $SyncServer;
    public $SyncLog;
    public $StockNotifictaion;

    function __construct(){
        $this->SyncLog = new SyncLog();
        $this->SyncServer = new SyncServer();
        $this->StockNotifictaion = new StockNotifictaion();
    }

    function register(){
        /** Stock Notifications */
        add_action( 'somfy_stockNotifictaion_EU_cron', array($this->StockNotifictaion, 'checkStockNotificationEU'));
        add_action( 'somfy_stockNotifictaion_PRO_cron', array($this->StockNotifictaion, 'checkStockNotificationPRO'));

        /** PRODUCTS SYNC */
        add_action( 'somfy_syncProducts_EU_cron', array($this, 'syncProductsEU'));
        add_action( 'somfy_syncProducts_EU_retry_cron', array($this, 'retryProductSyncEU'));
        
        add_action( 'somfy_syncProducts_PRO_cron', array($this, 'syncProductsPRO'));
        add_action( 'somfy_syncProducts_PRO_retry_cron', array($this, 'retryProductSyncPRO'));

        /** Customer SYNC */
        add_action( 'somfy_syncCustomers_EU_cron', array($this, 'syncCustomersEU'));
        add_action( 'somfy_syncCustomers_EU_retry_cron', array($this, 'retryCustomersSyncEU'));
        
        add_action( 'somfy_syncCustomers_PRO_cron', array($this, 'syncCustomersPRO'));
        add_action( 'somfy_syncCustomers_PRO_retry_cron', array($this, 'retryCustomersSyncPRO'));
        
        add_action( 'somfy_ClearLogs_cron', array($this->SyncLog, 'cron_clearLog'));
    }


    /** PRODUCTS SYNC */
    public function syncProductsEU(){
        $productWebSites = array("1", "3");

        $this->somfylog('syncProductsEU cron started', '');
        $syncRes = $this->SyncServer->syncProducts('eu', $productWebSites);
        $this->somfylog('syncProductsEU cron ended', $syncRes);
        // return $syncRes;
    }

    public function retryProductSyncEU(){
        $productWebSites = array("1", "3");

        $this->somfylog('retryProductSyncEU cron started', '');
        $syncRes = $this->SyncServer->retryProductSync('eu', $productWebSites);
        $this->somfylog('retryProductSyncEU cron ended', $syncRes);
    }

    public function syncProductsPRO(){
        $productWebSites = array("2", "3");
        $this->somfylog('syncProductsPRO cron started', '');
        $syncRes = $this->SyncServer->syncProducts('pro', $productWebSites);
        $this->somfylog('syncProductsPRO cron ended', $syncRes);
        // return $syncRes;
    }


    public function retryProductSyncPRO(){
        $productWebSites = array("2", "3");
        $this->somfylog('retryProductSyncPRO cron started', '');
        $syncRes = $this->SyncServer->retryProductSync('pro', $productWebSites);
        $this->somfylog('retryProductSyncPRO cron ended', $syncRes);
    }


    /** CUSTOMERS SYNC */
    public function syncCustomersEU(){
        $customerTypes = array("8");

        $this->somfylog('syncCustomersEU cron started', '');
        $syncRes = $this->SyncServer->syncCustomers('eu', $customerTypes);
        $this->somfylog('syncCustomersEU cron ended', $syncRes);
    }

    public function retryCustomersSyncEU(){
        $customerTypes = array("8");

        $this->somfylog('retryCustomersSyncEU cron started', '');
        $syncRes = $this->SyncServer->retryCustomerSync('eu', $customerTypes);
        $this->somfylog('retryCustomersSyncEU cron ended', $syncRes);
    }

    public function syncCustomersPRO(){
        $customerTypes = array("1", "2", "12");

        $this->somfylog('syncCustomersPRO cron started', '');
        $syncRes = $this->SyncServer->syncCustomers('pro', $customerTypes);
        $this->somfylog('syncCustomersPRO cron ended', $syncRes);
        return $syncRes;

    }

    public function retryCustomersSyncPRO(){
        $customerTypes = array("1", "2", "12");

        $this->somfylog('retryCustomersSyncPRO cron started', '');
        $syncRes = $this->SyncServer->retryCustomerSync('pro', $customerTypes);
        $this->somfylog('retryCustomersSyncPRO cron ended', $syncRes);
    }

    
}