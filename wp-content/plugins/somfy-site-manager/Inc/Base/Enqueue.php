<?php 
/**
 * @package  SomfySiteManager
 */

namespace Inc\Base;
use \Inc\Base\BaseController;


class Enqueue extends BaseController
{
	public function register() {
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue'), $priority = 1);
	}
	
	function enqueue() {

		// fontawesome
		wp_enqueue_style( 'fontawesome', 'https://use.fontawesome.com/releases/v5.7.1/css/all.css' ,array(), '1.0');	

		// Somfy
		wp_enqueue_style( 'somfyadmin-style', $this->plugin_url . 'assets/style.css' ,array(), '1.0');		
		wp_enqueue_script( 'somfyadmin-script', $this->plugin_url . 'assets/js/scripts-bundled.js', array() , '1.0.0', true);
		wp_localize_script('somfyadmin-script', 'somfyData', array(
			'root_url' => get_site_url(),
			'ajax_url' => admin_url('admin-ajax.php'),
			'ajax_nonce' => wp_create_nonce(NONCE_SECRET)
		));
	}
}