<?php
/**
 * @package SomfySiteManager
 */

namespace Inc\Base;

use Inc\Classes\Orders;

class SomfyAjax {
    public function register() {
        $Orders = new Orders();


        // get user orders
        add_action("wp_ajax_somfy_order_details", array( $Orders, "getOrderDetails")); // logged user
        add_action("wp_ajax_nopriv_somfy_user_orders", array( $this, "somfy_must_login")); // not logged user


        add_action("wp_ajax_somfy_admin_get_order", array( $Orders, "getOrders")); // logged user
        add_action("wp_ajax_nopriv_somfy_admin_get_order", array( $this, "somfy_must_login")); // not logged user
    }


    function somfy_must_login() {
        echo "You must be logged in!";
        die();
    }
}