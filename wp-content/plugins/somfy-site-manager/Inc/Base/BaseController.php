<?php 
/**
 * @package  SomfySiteManager
 */
namespace Inc\Base;


class BaseController
{
	public $plugin_path;
	public $plugin_url;
    public $plugin;

    public $dbSomfylog;
    
	public function __construct() {
		$this->plugin_path = plugin_dir_path( dirname( __FILE__, 2 ) );
		$this->plugin_url = plugin_dir_url( dirname( __FILE__, 2 ) );
		$this->plugin = plugin_basename( dirname( __FILE__, 3 ) ) . '/somfy-site-manager.php';
	}

	public function somfylog($title, $message){
		global $wpdb;
        $this->dbSomfylog = EU_SITE_PREFIX . "somfylog";

		$createDate = current_time( "Y-m-d H:i:s", 0 );
		$message = json_encode($message);
		
		$query = "INSERT INTO $this->dbSomfylog
			(
                `ID`, 
                `title`, 
                `message`, 
                `create_date`
            ) VALUES (
                NULL,
				'$title',
				'$message',
				'$createDate'
		)";

        $wpdb->query($query);
	}
}