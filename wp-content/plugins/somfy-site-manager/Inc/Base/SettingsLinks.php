<?php
/**
 * @package  SomfySiteManager
 */

namespace Inc\Base;

use \Inc\Base\BaseController;

class SettingsLinks extends BaseController
{

    public function register() 
	{
		add_filter( "plugin_action_links_$this->plugin", array( $this, 'settings_link' ) );
		add_action('members_get_capabilities', array( $this, 'plugin_name_extra_caps'));
	}

    public function settings_link( $links ) 
	{
		$settings_link = '<a href="admin.php?page=somfy_site_manager_plugin_dashboard">Dashboard</a>';
		array_push( $links, $settings_link );
		return $links;
	}

    function plugin_name_extra_caps( $caps ) {
		$caps[] = 'manage_events';
		return $caps;
	}
}