<?php
/**
 * @package  SomfySiteManager
 */

namespace Inc\Classes;

use \Inc\Classes\NetSuiteApi;
use \Inc\Classes\ProductPricing;

class Products {
    public $fieldsToExclude;
    public $booleanFields;
    public $relatedProductsFields;
    public $dateFields;
    public $netSuiteApi;
    public $ProductPricing;

    function __construct(){
        $this->netSuiteApi = new NetSuiteApi();
        $this->ProductPricing = new ProductPricing();
        $this->fieldsToExclude = array(
            'custitem_product_picture',
            'custitem_external_files',
            'storedisplayname',
            'stock',
            'created',
            'modified',
            'custitem_somfy_web_product_category',
            'custitem_productgroup',
            'custitem_sub_product_group',
            'custitem_sub2productgroup',
            'base_price',
            'custitem_installation_traning_video',
            'custitem_promotional_video'
        );

        $this->booleanFields = array(
            'custitem_csi',
            'custitem_wf',
            'custitem_connected',
        );

        $this->relatedProductsFields = array(
            'custitem_complementary_product',
            'custitem_up_sell',
        );

        $this->dateFields = array(
            'created',
            'modified',
        );
    }


    /** 
     * Description: updateProducts
     *  1. Loop all products
     *  2. check if product exits with internalID
     *  2.1. if not create new product post
     *  3. update the fields
     * @param
     * @return 
     */
    public function updateProducts($netSuite_products, $productWebSites, $bid){
        $updatedCounter = 0;
        $createdCounter = 0;
        switch_to_blog($bid); 

        // create internalid array for pricing sync
        $priceSyncData = array(
            'internals' => array(),
            'data' => array()
        );

        foreach($netSuite_products as $product){
            // check if needed to update base on web site
            if(!in_array($product['custitem_web_sites'], $productWebSites) 
                || ($product['type'] == 'InvtPart' && $product['custitem_productcategory'] == '4')){
                continue;
            }


            $productInternalID = $product['internalid'];
            $productName = $product['storedisplayname'];


            // 2. check if product exits with internalID
            $productExists = $this->checkIfProductExists($productInternalID, $bid);

            if(count($productExists) == 0){
                //  2.1. if not create new product post
                $productID = wp_insert_post(array(
                    'post_title'=> $productName, 
                    'post_type'=>'product'
                ));
                $createdCounter += 1;
                array_push($priceSyncData['internals'], $productInternalID);
            } else {
                $productID = $productExists[0];
            }

            // 3. update the fields
            $productUpdated = $this->updateProductData($productID, $product, $bid);
            $updatedCounter += 1;

            $priceSyncData['data'][$productInternalID] = $productID;

            error_log(json_encode(array("product synced: updated - $updatedCounter, created - $createdCounter", $productID)));
        }

        // Sync prices
        $pricesUpdated = $this->syncPrices($priceSyncData, $bid);

        return array(
            'status' => true,
            'msg' => 'Product updated successfully',
            'pricesUpdated' => $pricesUpdated,
            'updated' => array(
                'total' => $updatedCounter + $createdCounter,
                'updatedCounter' => $updatedCounter,
                'createdCounter' => $createdCounter,
            )
        );
    }


    /** 
     * Description: syncPrices
     * @param
     * @return 
     */
    public function syncPrices($priceSyncData, $bid){
        switch_to_blog($bid); 

        if($bid == 1){
            //  EU site
            $netPricesData = $this->netSuiteApi->getProductsPriceEU($priceSyncData['internals']);
            if(!$netPricesData['status']){
                return array(
                    'status' => false,
                    'error' => 'Couldnt get response, set for retry',
                    'netPricesData' => $netPricesData
                );
            } else {
                // loop and update data 
                $rate = get_option('shop_rate');
                $productsPrices = $netPricesData['data'];

                foreach($productsPrices as $product){
                    $unitprice = $product['unitprice'];
                    $taxAmount = round(($rate / 100) * $unitprice);
                    $productPrice = round($unitprice + $taxAmount);
                    $base_price_no_rate = $productPrice - $taxAmount;
                    
                    $item = $product['item'];

                    if(isset($priceSyncData['data'][$item])){
                        $productID = $priceSyncData['data'][$item];

                        update_field('base_price', $productPrice, $productID);
                        update_field('taxamount', $taxAmount, $productID);
                        update_field('base_price_no_rate', $base_price_no_rate, $productID);
                    }
                    
                }

                return array(
                    'status' => true
                );
            }
        } else if($bid == 2){
        // PRO site
            $netPricesData = $this->netSuiteApi->getProductsPricePRO(null, 0, $priceSyncData['internals']);
            if(!$netPricesData['status']){
                return array(
                    'status' => false,
                    'error' => 'Couldnt get response, set for retry'
                );
            } else {
                // update database 
                $productsPrices = $netPricesData['data'];

                $chunks = array_chunk($productsPrices, 500);
                $chunks_results = array();

                foreach($chunks as $chunk){
                    $query = $this->ProductPricing->upsert($chunk);
                    array_push($chunks_results, $query);
                }

                return array(
                    'status' => true,
                    'chunks_results' => $chunks_results,
                    'count' => count($netPricesData['data'])
                );
            }
        }

    }


    
    /** 
     * Description: updateProductData
     * @param
     * @return 
     */
    public function updateProductData($productID, $product, $bid){
        switch_to_blog($bid); 

        foreach($product as $key => $value){

            // Update boolean fields
            if(in_array($key, $this->booleanFields)){
                $modifiedValue = $value == 'F' ? '0' : '1';
                update_field($key, $modifiedValue, $productID);
                continue;
            }

            // Update related products fields
            if(in_array($key, $this->relatedProductsFields)){
                $modifiedValue = $this->checkIfProductExists($value, $bid);
                update_field($key, $modifiedValue, $productID);

                continue;
            }

            // Update other text, number, select fields
            if(!in_array($key, $this->fieldsToExclude)){
                update_field($key, $value, $productID);
                continue;
            }
        }

        // update product stock
        if(isset($product['stock'][0]['locationquantityavailable'])){
            update_field('stock_quantity', $product['stock'][0]['locationquantityavailable'], $productID);
        } else {
            update_field('stock_quantity', 0, $productID);
        }
        
        // update product Title
        $this->updateProductTitle($productID, $product['storedisplayname'], $bid);

        // $taxUpdated = $this->updateProductTaxonomy($productID, $product, $bid);

        // get taxonomy post id by id from the erp
        $categoriesInternals = explode(',', $product['custitem_somfy_web_product_category']);
        $subgroupInternals = explode(',', $product['custitem_sub_product_group']);
        $sub2groupInternals = explode(',', $product['custitem_sub2productgroup']);
       
        $categoryID = $this->getTaxonomyByID('product_category', $categoriesInternals, $bid);
        $subgroupID = $this->getTaxonomyByID('product_subgroup', $subgroupInternals, $bid);
        $sub2groupID = $this->getTaxonomyByID('product_2_subgroup', $sub2groupInternals, $bid);
        
        update_field('custitem_somfy_web_product_category', $categoryID, $productID);
        update_field('custitem_sub_product_group', $subgroupID, $productID);
        update_field('custitem_sub2productgroup', $sub2groupID, $productID);

        // return array(
        //     'product' => $product,
        //     'productID' => $productID,
        //     'categoriesInternals' => $categoriesInternals,
        //     'subgroupInternals' => $subgroupInternals,
        //     'sub2groupInternals' => $sub2groupInternals,
        //     'categoryID' => $sub2groupInternals,
        //     'subgroupID' => $subgroupID,
        //     'sub2groupID' => $sub2groupID
        // );  //TODO:
        return true;
    }



    /** 
     * Description: checkIfProductExists
     * @param
     * @return 
     */
    public function checkIfProductExists($internalID, $bid){
        switch_to_blog($bid); 

        $internalIds = explode(',',$internalID);

        $product = new \WP_Query(array(
            'post_type' => 'product',
            'post_status' => array('publish','pending','draft','auto-draft'),
            'posts_per_page'   => -1,
            'meta_key' => 'internalid',
            'meta_query' => array(
                array(
                    'key' => 'internalid',
                    'compare' => 'IN',
                    'value' => $internalIds
                )
            )
        )); 
        $ids = array();

        if(count($product->posts) > 0){
            foreach($product->posts as $product){
                array_push($ids, $product->ID);
            }
        }

        return $ids;
    }

    
    /** 
     * Description: updateProductTitle
     * @param
     * @return 
     */
    public function updateProductTitle($productID, $title, $bid){
        switch_to_blog($bid); 

        $args = array(
            'ID' => $productID,
            'post_title' => $title,
            'post_name' => ''
        );
        wp_update_post( $args, true );
    }


    /** 
     * Description: updateProductTaxonomy
     * @param
     * @return 
     */
    public function updateProductTaxonomy($productID, $product, $bid){
        switch_to_blog($bid); 

        // get taxonomy post id by id from the erp
        $categoriesInternals = explode(',', $product['custitem_somfy_web_product_category']);
        $subgroupInternals = explode(',', $product['custitem_sub_product_group']);
        $sub2groupInternals = explode(',', $product['custitem_sub2productgroup']);
       
        $categoryID = $this->getTaxonomyByID('product_category', $categoriesInternals, $bid);
        $subgroupID = $this->getTaxonomyByID('product_subgroup', $subgroupInternals, $bid);
        $sub2groupID = $this->getTaxonomyByID('product_2_subgroup', $sub2groupInternals, $bid);
        
        update_field('custitem_somfy_web_product_category', $categoryID, $productID);
        update_field('custitem_sub_product_group', $subgroupID, $productID);
        update_field('custitem_sub2productgroup', $sub2groupID, $productID);
        return array(
            'categoriesInternals' => $categoriesInternals,
            'subgroupInternals' => $subgroupInternals,
            'sub2groupInternals' => $sub2groupInternals,
            'categoryID' => $sub2groupInternals,
            'subgroupID' => $subgroupID,
            'sub2groupID' => $sub2groupID
        );
    }

    /** 
     * Description: getTaxonomyByID
     * @param
     * @return 
     */
    public function getTaxonomyByID($cpt, $taxonomyERPId, $bid){
        switch_to_blog($bid); 

        $taxonomy = new \WP_Query(array(
            'post_type' => $cpt,
            'posts_per_page'   => -1,
            'meta_key' => 'id',
            'meta_query' => array(
                array(
                    'key' => 'id',
                    'compare' => 'IN',
                    'value' => $taxonomyERPId
                )
            )
        )); 
        $result = array();

        if($taxonomy){
            while ($taxonomy->have_posts()) {
                $taxonomy->the_post();
                $taxonomyID = get_the_ID();
                array_push($result, $taxonomyID);
            }
        } 
        return $result;
    }

    

    public function updateProductIndex_pro(){
        global $wpdb;
        $status = false;
        
        $dbProductIndex = PRO_SITE_PREFIX . "product_index_full";
        $dbPosts = PRO_SITE_PREFIX . "posts";
        $dbPostmeta = PRO_SITE_PREFIX . "postmeta";

        $deleteQuery = "DELETE FROM $dbProductIndex";

        // Start Transaction
        $wpdb->query( "START TRANSACTION" );
        
        // delete and insert new DiscountCodes
        $deleteResult = $wpdb->query($deleteQuery);

        $insertQuery = "
            INSERT INTO $dbProductIndex
            select 
                sp.ID as product_ID ,
                sp.post_title ,
                max(case when sp2.meta_key = 'internalid' then sp2.meta_value else null end) as `internalid`,
                max(case when sp2.meta_key = 'itemid' then sp2.meta_value else null end) as `itemid`,
                max(case when sp2.meta_key = 'stock_quantity' and sp2.meta_value != '' then CAST(sp2.meta_value AS UNSIGNED) else 0 end) as `stock_quantity`,
                max(case when sp2.meta_key = 'custitem_discountcode' then sp2.meta_value else null end) as `custitem_discountcode`,
                max(case when sp2.meta_key = 'on_sell' then sp2.meta_value else null end) as `on_sell`,
                max(case when sp2.meta_key = 'discount_price' then sp2.meta_value else null end) as `discount_price`,
                max(case when sp2.meta_key = 'discount_percentage' then sp2.meta_value else null end) as `discount_percentage`,
                
                max(case when sp2.meta_key = 'custitem_somfy_web_product_category' then sp2.meta_value else null end) as `custitem_somfy_web_product_category`,
                max(case when sp2.meta_key = 'custitem_sub_product_group' then sp2.meta_value else null end) as `custitem_sub_product_group`,
                max(case when sp2.meta_key = 'custitem_sub2productgroup' then sp2.meta_value else null end) as `custitem_sub2productgroup`,
                max(case when sp2.meta_key = 'custitem_productline' then sp2.meta_value else null end) as `custitem_productline`,
                max(case when sp2.meta_key = 'custitem_series' then sp2.meta_value else null end) as `custitem_series`,
                max(case when sp2.meta_key = 'custitem_radiomodel' then sp2.meta_value else null end) as `custitem_radiomodel`,
                max(case when sp2.meta_key = 'custitem_wireradio' then sp2.meta_value else null end) as `custitem_wireradio`,
                
                max(case when sp2.meta_key = 'custitem_radiofreaquency' then sp2.meta_value else null end) as `custitem_radiofreaquency`,
                max(case when sp2.meta_key = 'custitem_remote_type' then sp2.meta_value else null end) as `custitem_remote_type`,
                max(case when sp2.meta_key = 'custitem_rpm' then sp2.meta_value else null end) as `custitem_rpm`,
                max(case when sp2.meta_key = 'custitem_torque' then sp2.meta_value else null end) as `custitem_torque`,
                max(case when sp2.meta_key = 'custitem_csi' then sp2.meta_value else null end) as `custitem_csi`,
                max(case when sp2.meta_key = 'custitem_diameter' then sp2.meta_value else null end) as `custitem_diameter`,
                max(case when sp2.meta_key = 'custitem_color' then sp2.meta_value else null end) as `custitem_color`,
                max(case when sp2.meta_key = 'custitem_channels' then sp2.meta_value else null end) as `custitem_channels`,
                max(case when sp2.meta_key = 'custitem_lsu_capacity' then sp2.meta_value else null end) as `custitem_lsu_capacity`,
                max(case when sp2.meta_key = 'custitem_manuela' then sp2.meta_value else null end) as `custitem_manuela`,
                max(case when sp2.meta_key = 'custitem_motor_diameter' then sp2.meta_value else null end) as `custitem_motor_diameter`,
                max(case when sp2.meta_key = 'custitem_opening_mm' then sp2.meta_value else null end) as `custitem_opening_mm`,
                max(case when sp2.meta_key = 'custitem_power_w' then sp2.meta_value else null end) as `custitem_power_w`,
                
                max(case when sp2.meta_key = 'custitem_related_regular_item' then sp2.meta_value else null end) as `custitem_related_regular_item`,
                max(case when sp2.meta_key = 'custitem_voltage' then sp2.meta_value else null end) as `custitem_voltage`,
                max(case when sp2.meta_key = 'custitem_warranty' then sp2.meta_value else null end) as `custitem_warranty`,
                max(case when sp2.meta_key = 'custitem_wf' then sp2.meta_value else null end) as `custitem_wf`

            from $dbPosts sp 

            join $dbPostmeta sp2 
                on sp2.post_id = sp.ID
            where sp.post_type = 'product' and sp.post_status = 'publish' 

            group by sp.ID ,
            sp.post_title;
        ";

        $inserted = $wpdb->query($insertQuery);

        if(($deleteResult > 0 || $inserted > 0)) {
            $wpdb->query('COMMIT'); // if you come here then well done
            $status = true;
        } else {
            $wpdb->query('ROLLBACK'); // // something went wrong, Rollback
            $status = false;
        }

        return array('status' => $status, 'deleteResult' => $deleteResult, 'inserted' => $inserted);
    }



    public function updateProductTitleIndex_pro(){
        global $wpdb;
        $status = false;
        
        $dbProductTitleIndex = PRO_SITE_PREFIX . "product_title_index";
        $dbPosts = PRO_SITE_PREFIX . "posts";
        $dbPostmeta = PRO_SITE_PREFIX . "postmeta";

        $deleteQuery = "DELETE FROM $dbProductTitleIndex";

        // Start Transaction
        $wpdb->query( "START TRANSACTION" );
        
        // delete and insert new DiscountCodes
        $deleteResult = $wpdb->query($deleteQuery);

        $insertQuery = "
            INSERT INTO $dbProductTitleIndex
            select 
                sp.ID,
                sp.post_title ,
                max(case when sp2.meta_key = 'internalid' then sp2.meta_value else null end) as `internalid`,
                max(case when sp2.meta_key = 'itemid' then sp2.meta_value else null end) as `itemid`,
                max(case when sp2.meta_key = 'custitem_discountcode' then sp2.meta_value else null end) as `custitem_discountcode`

            from $dbPosts sp 

            join $dbPostmeta sp2 
                on sp2.post_id = sp.ID
            
            where sp.post_type = 'product' and sp.post_status = 'publish' 

            group by sp.ID,
            sp.post_title;
        ";

        $inserted = $wpdb->query($insertQuery);

        if(($deleteResult > 0 || $inserted > 0)) {
            $wpdb->query('COMMIT'); // if you come here then well done
            $status = true;
        } else {
            $wpdb->query('ROLLBACK'); // // something went wrong, Rollback
            $status = false;
        }
        return array('status' => $status, 'deleteResult' => $deleteResult, 'inserted' => $inserted);
    }
}