<?php
/**
 * @package  SomfySiteManager
 */

namespace Inc\Classes;

use Inc\Classes\Customers;

class Orders{
    /** 
     * Description: getOrders
     * @param 
     * @return
     */
    public function getOrders($request){
        if ( !wp_verify_nonce( $_REQUEST['nonce'], NONCE_SECRET)) {
            exit("No naughty business please");
        }  

        global $wpdb;
        if(get_current_blog_id() == '1'){
            $prefix = EU_SITE_PREFIX;
        } else {
            $prefix = PRO_SITE_PREFIX;
        }

        $dbPosts = $prefix . "posts";
        $dbPostmeta = $prefix . "postmeta";


        //  validate params
        $pagination = isset($_REQUEST['pagination']) ? $_REQUEST['pagination'] : null;
        $sort = isset($_REQUEST['sort']) ? $_REQUEST['sort'] : null;
        $tranid = isset($_REQUEST['tranid']) ? $_REQUEST['tranid'] : null;
            
        if($pagination){
            $offset = intval($pagination['limit']) * (intval($pagination['page']) - 1);
            $limit = intval($pagination['limit']);
        } else {
            $offset = 0;
            $limit = 999999999;
        }

        // set tranid filter
        $tranidJoin = "";
        $tranidWhere = "";
        if(isset($tranid) && strlen($tranid) > 2){
            $tranidJoin = "JOIN $dbPostmeta pmt
                ON pmt.meta_key = 'tranid' AND pmt.post_id = p.ID ";
            $tranidWhere = "AND pmt.meta_value LIKE '%$tranid%'";
        }


        $query = "SELECT p.ID, pm.meta_value AS saleseffectivedate
            FROM $dbPosts p
            LEFT JOIN $dbPostmeta pm
                ON pm.meta_key = 'saleseffectivedate' AND pm.post_id = p.ID
            $tranidJoin
            WHERE p.post_type = 'somfy_order' AND p.post_status NOT IN ('trash')
                $tranidWhere
            ORDER BY STR_TO_DATE(pm.meta_value,'%d/%m/%Y') $sort, p.post_title DESC
        ";
        
        $orders = $wpdb->get_results($query);
        
        $ordersResults = array();
        
        if($orders){
            $Customers = new Customers();
            $resCounter = 0;
            $totalCounter = 0;
            
            for($i = 0; $i < count($orders); $i++){
                if($i < $offset) continue;

                $orderID = $orders[$i]->ID;

                // get customer data
                $customerData = $Customers->getCustomerData(get_field('entity', $orderID, true));
                $orderData = get_fields($orderID, true);
                
                array_push($ordersResults, array(
                    'ID' => $orderID,
                    'data' => $orderData,
                    'customerData' => $customerData
                ));
                if(count($ordersResults) > $limit - 1) break;
            }
        }

        // return orders
        $result['status'] = true;
        $result['orders'] = $ordersResults;
        $result['total'] = count($orders);
        echo json_encode($result);
        die();
        
    }


    /** 
     * Description: getOrderDetails
     * @param internalid
     * @return
     */
    public function getOrderDetails($request){
        if ( !wp_verify_nonce( $_REQUEST['nonce'], NONCE_SECRET)) {
            exit("No naughty business please");
         }  

        //  validate params
        $orderID = isset($_REQUEST['orderID']) ? $_REQUEST['orderID'] : null;
        
        if(!isset($orderID)){
            $result['status'] = false;
            $result['message'] = 'חלק מהנתונים חסרים';
            echo json_encode($result);
            die();
        }

        // get items and info
        $orderInfo = get_fields($orderID, true);
        $orderItems = $this->getOrderItemsData($orderID);

        $result['status'] = true;
        $result['items'] = $orderItems;
        $result['orderInfo'] = $orderInfo;
        echo json_encode($result);
        die();
    }

/** 
     * Description: getOrderItemsData
     * @param
     * @return
     */
    public function getOrderItemsData($orderID){
        global $wpdb;

        if(get_current_blog_id() == '1'){
            $prefix = EU_SITE_PREFIX;
        } else {
            $prefix = PRO_SITE_PREFIX;
        }

        $dbOrderItem = $prefix . "orderitem";
        $dbPosts = $prefix . "posts";
        $dbPostmeta = $prefix . "postmeta";

        $query = "SELECT 
                oi.*, 
                pd.ID AS productID, 
                pd.post_title, 
                pd.internalid
            FROM $dbOrderItem oi
            LEFT JOIN (
                SELECT p.ID, p.post_title, pm.meta_value AS internalid
                FROM $dbPosts p
                JOIN $dbPostmeta pm
                    ON pm.meta_key = 'internalid' AND pm.post_id = p.ID
                WHERE p.post_type = 'product'
            ) pd 
                ON pd.internalid = oi.item
            WHERE oi.orderid = '$orderID'";

        $results = $wpdb->get_results($query);
        
        if(count($results) > 0){
            for($i = 0; $i < count($results); $i++){
                $pid = $results[$i]->productID;
                $results[$i]->itemid = get_field('itemid', $pid);
            }
        }
        return $results;
    }
}