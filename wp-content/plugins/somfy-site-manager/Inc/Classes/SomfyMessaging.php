<?php
/**
 * @package  SomfySiteManager
 */

namespace Inc\Classes;

class SomfyMessaging {
    private $inWizeApiKey;


    function __construct(){
        $this->ApiURL = get_option('inwise_api_url');
        $this->inWizeTestApiKey = get_option('inwise_test_api_key');
        $this->inWizeApiKey = get_option('inwise_api_key');
    }

    /** 
     * Description: POST request
     * @param
     * @return 
     */
    public function sendSMS($sendTo, $content, $add_unsubscribe = false){
        $endPoint = $this->ApiURL .'/transactional/sms/send';
        $from = get_option('inwise_sms_from_name');

        $args = array(
            'message' => array(
                'content' => $content,
                'charset' => 'unicode',
                'from' => 'somfy',
                'to' => array(
                    array(
                        'mobile_number' => $sendTo
                    )
                ),    
            // 'async' => 1,
            // 'send_at' => // as a UTC timestamp in YYYY-MM-DD HH:MM:SS format. 
            )
        );

        if($add_unsubscribe){
            $args['message']['add_unsubscribe_link'] = 1;
            $args['message']['unsubscribe_text'] = 'להסרה השב 1';
            $args['message']['add_unsubscribe_sms_reply'] = 1;
        }



        $curl = curl_init();


        curl_setopt_array($curl, array(
            CURLOPT_URL => $endPoint,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($args),
            CURLOPT_HTTPHEADER => array(
                'X-API-Key: ' . $this->inWizeTestApiKey,
                'Content-Type: application/json',
            ),
            ));

        $response = curl_exec($curl);
		$info = curl_getinfo($curl);

		// check for errors
		if(!$info["http_code"] || $info["http_code"]!=200){
            return array(
                'status' => false, 
                'error' => 'Http Error', 
                'args' => $args, 
                'response' => $response
            );
        } 
        $res = json_decode($response, true);
        
        curl_close($curl);

        return $res;
    }
    
    /** 
     * Description: 
     * @param
     * @return 
     */
    public function sendMail($toEmail, $subject, $htmlBody, $data = null){
        $endPoint = $this->ApiURL .'/transactional/emails/send';
        $from_email = get_option('inwise_email_from_email');
        $from_name = get_option('inwise_email_from_name');

        // Set Default
        $from_email = $from_email == '' ? 'eshop.il@somfy.com' : $from_email;
        $from_name = $from_name == '' ? 'Somfy Shop' : $from_name;
        
        // check for data and replace shortcode
        if($data){
            $emailContent = $this->replace_shortcodes($htmlBody, $data);
        } else {
            $emailContent = $htmlBody;
        }

        $emailContent = "<div style='direction: rtl; text-align: right;'>" . $emailContent . "</div>";
        $args = array(
            "message" => array(
                "html" => $emailContent,
                "subject" => $subject,
                "from_name" => $from_name,
                "charset" => "utf-8",
                "content_type" => "html",
                "to" => array(
                    array(
                        "email" => $toEmail,
                        "type" => "to"
                    ),
                    array(
                        "email" => 'moroz.a24@gmail.com',
                        "type" => "cc"
                    )
                ),
            )
        );
            
             
        $curl = curl_init();


        curl_setopt_array($curl, array(
            CURLOPT_URL => $endPoint,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($args),
            CURLOPT_HTTPHEADER => array(
                'X-API-Key: ' . $this->inWizeTestApiKey,
                'Content-Type: application/json',
            ),
            ));

        $response = curl_exec($curl);
		$info = curl_getinfo($curl);

		// check for errors
		if(!$info["http_code"] || $info["http_code"]!=200){
            return array(
                'status' => false, 
                'error' => 'Http Error', 
                'args' => $args, 
                'response' => $response
            );
        } 
        $res = json_decode($response, true);
        
        curl_close($curl);

        return $res;
    }


    /** 
     * Description: addClient
     * @param
     * @return 
     */
    public function addClient($userEmail, $userID = ''){
        $endPoint = $this->ApiURL .'/contacts';
        $inwiseGroup = get_option('newsletter_sub__inwise_group');

        $args = array(
            'id' => 0,
            'email' => $userEmail,
            'external_id' => $userID,
            'add_to_groups_ids' => array($inwiseGroup),
        );


        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $endPoint,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($args),
            CURLOPT_HTTPHEADER => array(
                'X-API-Key: ' . $this->inWizeTestApiKey,
                'Content-Type: application/json',
            ),
            ));

        $response = curl_exec($curl);
		$info = curl_getinfo($curl);

		// check for errors
		if(!$info["http_code"] || $info["http_code"]!=200){
            return array(
                'status' => false, 
                'error' => 'Http Error', 
                'args' => $args, 
                'response' => $response
            );
        } 
        
        $res = json_decode($response, true);
        curl_close($curl);
        return $res;
    }


    /** 
     * Description: getTemplate
     * @param
     * @return 
     */
    public function getTemplate($template){
        $endPoint = $this->ApiURL ."/templates?template_ids=$template";



        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $endPoint,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'X-API-Key: ' . $this->inWizeTestApiKey,
                'Content-Type: application/json',
            ),
            ));

        $response = curl_exec($curl);
		$info = curl_getinfo($curl);
        curl_close($curl);

		// check for errors
		if(!$info["http_code"] || $info["http_code"]!=200){
            return array(
                'status' => false, 
                'error' => 'Http Error', 
                'args' => $args, 
                'response' => $response
            );
        } else {
            $res = json_decode($response, true);
            if(isset($res[0]['content']) && $res[0]['content'] != ''){
                return $res[0]['content'];
            } else {
                return false;
            }
        }
        
        return $res;
    }


    private function replace_shortcodes($str, $data){
		foreach($data as $k=>$v) {
			$str = str_replace("{" . $k ."}", strval($v), $str);
		}

		return $str;
	}
}


