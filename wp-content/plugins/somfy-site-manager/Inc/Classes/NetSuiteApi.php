<?php
/**
 * @package  SomfySiteManager
 */

namespace Inc\Classes;


class NetSuiteApi{
    
    /**
     * PRODUCTS END POINTS
     */

    /** 
     * Description: Get all Products by modified date
     * @param
     * @return
     */
    public function getAllProducts($last_modified__arr){
        $script_ID = '672';
        $deploy_ID = '4';

        $startdayString = "&startday=" . $last_modified__arr['startday'] . "&sh=" . $last_modified__arr['sh'] . "&sm=" . $last_modified__arr['sm'] . "&sdt=" . $last_modified__arr['sdt'];
        $enddayString = "&endday=" . $last_modified__arr['endday'] . "&eh=" . $last_modified__arr['eh'] . "&em=" . $last_modified__arr['em'] . "&edt=" . $last_modified__arr['edt'];

        $auth_header = $this->getGETAuthHeadersByModifiedDate($deploy_ID, $script_ID, $last_modified__arr);

        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, NETSUITE_URL . '?&script=' . $script_ID . '&deploy=' . $deploy_ID . $startdayString . $enddayString);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: ' . $auth_header,
            'Content-Type: application/json'
        ]);

        $res = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $response = json_decode($res, true);

        curl_close($ch);
        
        if(isset($response['error'])){
            return array(
                'status' => false,
                'http_code' => $http_code,
                'error' => $response['error']
            );
        } else {
            return array(
                'status' => true,
                'http_code' => $http_code,
                'data' => $response
            );
        }
    }

    

    /**
     * Description: Get Product by ID
     * @param
     * @return
     */
    public function getProductByID($internalid){
        $script_ID = '672';
        $deploy_ID = '5';

        $auth_header = $this->getGETAuthHeaders($deploy_ID, $script_ID, $internalid);

        $ch = curl_init();

        curl_setopt($ch,CURLOPT_URL, NETSUITE_URL . '?&script=' . $script_ID . '&deploy=' . $deploy_ID . '&id=' . $internalid);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: ' . $auth_header,
            'Content-Type: application/json'
        ]);

        $res = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $response = json_decode($res, true);

        curl_close($ch);
        
        if(isset($response['error'])){
            return array(
                'status' => false,
                'http_code' => $http_code,
                'error' => $response['error']
            );
        } else {
            return array(
                'status' => true,
                'http_code' => $http_code,
                'data' => $response
            );
        }
    }


    /**
     * Description: Get Product stock
     * @param
     * @return
     */
    public function getProductStock($internalid){
        $script_ID = '672';
        $deploy_ID = '6';

        $auth_header = $this->getGETAuthHeaders($deploy_ID, $script_ID, $internalid);

        $ch = curl_init();

        curl_setopt($ch,CURLOPT_URL, NETSUITE_URL . '?&script=' . $script_ID . '&deploy=' . $deploy_ID . '&id=' . $internalid);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: ' . $auth_header,
            'Content-Type: application/json'
        ]);

        $res = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $response = json_decode($res, true);

        curl_close($ch);
        
        if(isset($response['error'])){
            return array(
                'status' => false,
                'http_code' => $http_code,
                'error' => $response['error']
            );
        } else {
            $currentStockQuantity = $response[0]['locationquantityavailable'];

            return array(
                'status' => true,
                'http_code' => $http_code,
                'currentStockQuantity' => $currentStockQuantity,
                'data' => $response
            );
        }
    }


    /**
     * Description: getProductsPriceEU
     * @param
     * @return
     */
    public function getProductsPriceEU($productArray){
        $script_ID = '672';
        $deploy_ID = '11';

        $currency = 1;
        $qty = 0;
        $itemsString = implode(",",$productArray);

        $auth_header = $this->getGETAuthHeadersPriceEU($deploy_ID, $script_ID, $currency, $qty, $itemsString);
        $ch = curl_init();

        curl_setopt($ch,CURLOPT_URL, NETSUITE_URL . '?&script=' . $script_ID . '&deploy=' . $deploy_ID . '&currency=' . $currency . '&qty=' . $qty . '&item=' . $itemsString);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: ' . $auth_header,
            'Content-Type: application/json'
        ]);

        $res = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $response = json_decode($res, true);

        curl_close($ch);
        
        if(isset($response['error'])){
            return array(
                'status' => false,
                'http_code' => $http_code,
                'error' => $response['error']
            );
        } else {
            return array(
                'status' => true,
                'http_code' => $http_code,
                'data' => $response
            );
        }
    }


    /**
     * Description: getProductsPriceEU
     * @param
     * @return
     */
    public function getProductsPricePRO($customer = null, $qty = 0, $productArray = null){
        $script_ID = '672';
        $deploy_ID = '10';

        $currency = 1;
        
        if(is_null($customer)) $customer = '';
        
        if(!is_null($productArray)){
            $itemsString = implode(",",$productArray);
        } else {
            $itemsString = '';
        }

        $auth_header = $this->getGETAuthHeadersPricePRO($deploy_ID, $script_ID, $customer, $currency, $qty, $itemsString);
        $ch = curl_init();

        curl_setopt($ch,CURLOPT_URL, NETSUITE_URL . '?&script=' . $script_ID . '&deploy=' . $deploy_ID . '&id=' . $customer . '&currency=' . $currency . '&qty=' . $qty . '&item=' . $itemsString);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: ' . $auth_header,
            'Content-Type: application/json'
        ]);

        $res = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $response = json_decode($res, true);

        curl_close($ch);
        
        if(isset($response['error'])){
            return array(
                'status' => false,
                'http_code' => $http_code,
                'error' => $response['error']
            );
        } else {
            return array(
                'status' => true,
                'http_code' => $http_code,
                'data' => $response
            );
        }
    }


    /**
     * CUSTOMERS END POINTS
     */

    /**
     * Description: Get all Customer
     * @param
     * @return
     */
    public function getAllCustomer($last_modified__arr){
        $script_ID = '672';
        $deploy_ID = '2';
        
        $startdayString = "&startday=" . $last_modified__arr['startday'] . "&sh=" . $last_modified__arr['sh'] . "&sm=" . $last_modified__arr['sm'] . "&sdt=" . $last_modified__arr['sdt'];
        $enddayString = "&endday=" . $last_modified__arr['endday'] . "&eh=" . $last_modified__arr['eh'] . "&em=" . $last_modified__arr['em'] . "&edt=" . $last_modified__arr['edt'];

        $auth_header = $this->getGETAuthHeadersByModifiedDate($deploy_ID, $script_ID, $last_modified__arr);

        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, NETSUITE_URL . '?&script=' . $script_ID . '&deploy=' . $deploy_ID . $startdayString . $enddayString);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: ' . $auth_header,
            'Content-Type: application/json'
        ]);

        $res = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $response = json_decode($res, true);

        curl_close($ch);
        
        if(isset($response['error'])){
            return array(
                'status' => false,
                'http_code' => $http_code,
                'error' => $response['error']
            );
        } else {
            return array(
                'status' => true,
                'http_code' => $http_code,
                'data' => $response
            );
        }
    }

    /**
     * Description: Get Customer by ID
     * @param
     * @return
     */
    public function getCustomerByID($internalid){
        $script_ID = '672';
        $deploy_ID = '3';

        $auth_header = $this->getGETAuthHeaders($deploy_ID, $script_ID, $internalid);

        $ch = curl_init();

        curl_setopt($ch,CURLOPT_URL, NETSUITE_URL . '?&script=' . $script_ID . '&deploy=' . $deploy_ID . '&id=' . $internalid);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: ' . $auth_header,
            'Content-Type: application/json'
        ]);

        $res = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $response = json_decode($res, true);

        curl_close($ch);
        
        if(isset($response['error'])){
            return array(
                'status' => false,
                'http_code' => $http_code,
                'error' => $response['error']
            );
        } else {
            return array(
                'status' => true,
                'http_code' => $http_code,
                'data' => $response
            );
        }
    }

   

    // Restlet auth headers
    // param - id
    protected function getGETAuthHeaders($deploy_ID, $script_ID, $internalid){
        $oauth_nonce = md5(mt_rand());
        $oauth_timestamp = time();
        $oauth_signature_method = 'HMAC-SHA1';
        $oauth_version = "1.0";

        $base_string =
            "GET&" . urlencode(NETSUITE_URL) . "&" .
            urlencode(
                "deploy=" . $deploy_ID
                . "&id=" . $internalid
                . "&oauth_consumer_key=" . NETSUITE_CONSUMER_KEY
                . "&oauth_nonce=" . $oauth_nonce
                . "&oauth_signature_method=" . $oauth_signature_method
                . "&oauth_timestamp=" . $oauth_timestamp
                . "&oauth_token=" . NETSUITE_TOKEN_ID
                . "&oauth_version=" . $oauth_version
                . "&script=" . $script_ID
            );

        $sig_string = urlencode(NETSUITE_CONSUMER_SECRET) . '&' . urlencode(NETSUITE_TOKEN_SECRET);
        $signature = base64_encode(hash_hmac("sha1", $base_string, $sig_string, true));

        $auth_header = "OAuth "
            . 'oauth_signature="' . rawurlencode($signature) . '", '
            . 'oauth_version="' . rawurlencode($oauth_version) . '", '
            . 'oauth_nonce="' . rawurlencode($oauth_nonce) . '", '
            . 'oauth_signature_method="' . rawurlencode($oauth_signature_method) . '", '
            . 'oauth_consumer_key="' . rawurlencode(NETSUITE_CONSUMER_KEY) . '", '
            . 'oauth_token="' . rawurlencode(NETSUITE_TOKEN_ID) . '", '  
            . 'oauth_timestamp="' . rawurlencode($oauth_timestamp) . '", '
            . 'realm="' . rawurlencode(NETSUITE_ACCOUNT) .'"';

        return $auth_header;
    }


    // param - last_modified__arr
    protected function getGETAuthHeadersByModifiedDate($deploy_ID, $script_ID, $last_modified__arr){
        $oauth_nonce = md5(mt_rand());
        $oauth_timestamp = time();
        $oauth_signature_method = 'HMAC-SHA1';
        $oauth_version = "1.0";

        $base_string =
            "GET&" . urlencode(NETSUITE_URL) . "&" .
            urlencode(
                "deploy=" . $deploy_ID
                . "&edt=" . $last_modified__arr['edt']
                . "&eh=" . $last_modified__arr['eh']
                . "&em=" . $last_modified__arr['em']
                . "&endday=" . urlencode($last_modified__arr['endday'])
                . "&oauth_consumer_key=" . NETSUITE_CONSUMER_KEY
                . "&oauth_nonce=" . $oauth_nonce
                . "&oauth_signature_method=" . $oauth_signature_method
                . "&oauth_timestamp=" . $oauth_timestamp
                . "&oauth_token=" . NETSUITE_TOKEN_ID
                . "&oauth_version=" . $oauth_version
                . "&script=" . $script_ID
                . "&sdt=" . $last_modified__arr['sdt']
                . "&sh=" . $last_modified__arr['sh']
                . "&sm=" . $last_modified__arr['sm']
                . "&startday=" . urlencode($last_modified__arr['startday'])
            );

        $sig_string = urlencode(NETSUITE_CONSUMER_SECRET) . '&' . urlencode(NETSUITE_TOKEN_SECRET);
        $signature = base64_encode(hash_hmac("sha1", $base_string, $sig_string, true));

        $auth_header = "OAuth "
            . 'oauth_signature="' . rawurlencode($signature) . '", '
            . 'oauth_version="' . rawurlencode($oauth_version) . '", '
            . 'oauth_nonce="' . rawurlencode($oauth_nonce) . '", '
            . 'oauth_signature_method="' . rawurlencode($oauth_signature_method) . '", '
            . 'oauth_consumer_key="' . rawurlencode(NETSUITE_CONSUMER_KEY) . '", '
            . 'oauth_token="' . rawurlencode(NETSUITE_TOKEN_ID) . '", '
            . 'oauth_timestamp="' . rawurlencode($oauth_timestamp) . '", '
            . 'realm="' . rawurlencode(NETSUITE_ACCOUNT) .'"';

        return $auth_header;
    }


    // param - currency, qty, itemsString
    protected function getGETAuthHeadersPriceEU($deploy_ID, $script_ID, $currency, $qty, $itemsString){
        $oauth_nonce = md5(mt_rand());
        $oauth_timestamp = time();
        $oauth_signature_method = 'HMAC-SHA1';
        $oauth_version = "1.0";

        $base_string =
            "GET&" . urlencode(NETSUITE_URL) . "&" .
            urlencode(
                "currency=" . $currency
                . "&deploy=" . $deploy_ID
                . "&item=" . urlencode($itemsString)
                . "&oauth_consumer_key=" . NETSUITE_CONSUMER_KEY
                . "&oauth_nonce=" . $oauth_nonce
                . "&oauth_signature_method=" . $oauth_signature_method
                . "&oauth_timestamp=" . $oauth_timestamp
                . "&oauth_token=" . NETSUITE_TOKEN_ID
                . "&oauth_version=" . $oauth_version
                . "&qty=" . $qty
                . "&script=" . $script_ID
            );

        $sig_string = urlencode(NETSUITE_CONSUMER_SECRET) . '&' . urlencode(NETSUITE_TOKEN_SECRET);
        $signature = base64_encode(hash_hmac("sha1", $base_string, $sig_string, true));

        $auth_header = "OAuth "
            . 'oauth_signature="' . rawurlencode($signature) . '", '
            . 'oauth_version="' . rawurlencode($oauth_version) . '", '
            . 'oauth_nonce="' . rawurlencode($oauth_nonce) . '", '
            . 'oauth_signature_method="' . rawurlencode($oauth_signature_method) . '", '
            . 'oauth_consumer_key="' . rawurlencode(NETSUITE_CONSUMER_KEY) . '", '
            . 'oauth_token="' . rawurlencode(NETSUITE_TOKEN_ID) . '", '  
            . 'oauth_timestamp="' . rawurlencode($oauth_timestamp) . '", '
            . 'realm="' . rawurlencode(NETSUITE_ACCOUNT) .'"';

        return $auth_header;
    }


    // param - currency, qty, itemsString, id
    protected function getGETAuthHeadersPricePRO($deploy_ID, $script_ID, $id, $currency, $qty, $itemsString){
        $oauth_nonce = md5(mt_rand());
        $oauth_timestamp = time();
        $oauth_signature_method = 'HMAC-SHA1';
        $oauth_version = "1.0";

        $base_string =
            "GET&" . urlencode(NETSUITE_URL) . "&" .
            urlencode(
                "currency=" . $currency
                . "&deploy=" . $deploy_ID
                . "&id=" . $id
                . "&item=" . urlencode($itemsString)
                . "&oauth_consumer_key=" . NETSUITE_CONSUMER_KEY
                . "&oauth_nonce=" . $oauth_nonce
                . "&oauth_signature_method=" . $oauth_signature_method
                . "&oauth_timestamp=" . $oauth_timestamp
                . "&oauth_token=" . NETSUITE_TOKEN_ID
                . "&oauth_version=" . $oauth_version
                . "&qty=" . $qty
                . "&script=" . $script_ID
            );

        $sig_string = urlencode(NETSUITE_CONSUMER_SECRET) . '&' . urlencode(NETSUITE_TOKEN_SECRET);
        $signature = base64_encode(hash_hmac("sha1", $base_string, $sig_string, true));

        $auth_header = "OAuth "
            . 'oauth_signature="' . rawurlencode($signature) . '", '
            . 'oauth_version="' . rawurlencode($oauth_version) . '", '
            . 'oauth_nonce="' . rawurlencode($oauth_nonce) . '", '
            . 'oauth_signature_method="' . rawurlencode($oauth_signature_method) . '", '
            . 'oauth_consumer_key="' . rawurlencode(NETSUITE_CONSUMER_KEY) . '", '
            . 'oauth_token="' . rawurlencode(NETSUITE_TOKEN_ID) . '", '  
            . 'oauth_timestamp="' . rawurlencode($oauth_timestamp) . '", '
            . 'realm="' . rawurlencode(NETSUITE_ACCOUNT) .'"';

        return $auth_header;
    }

    protected function getPOSTAuthHeaders($deploy_ID, $script_ID){
        $oauth_nonce = md5(mt_rand());
        $oauth_timestamp = time();
        $oauth_signature_method = 'HMAC-SHA1';
        $oauth_version = "1.0";

        $base_string =
            "POST&" . urlencode(NETSUITE_URL) . "&" .
            urlencode(
                "deploy=" . $deploy_ID
            . "&oauth_consumer_key=" . NETSUITE_CONSUMER_KEY
            . "&oauth_nonce=" . $oauth_nonce
            . "&oauth_signature_method=" . $oauth_signature_method
            . "&oauth_timestamp=" . $oauth_timestamp
            . "&oauth_token=" . NETSUITE_TOKEN_ID
            . "&oauth_version=" . $oauth_version
            . "&realm=" . NETSUITE_ACCOUNT
            . "&script=" . $script_ID
            );

        $sig_string = urlencode(NETSUITE_CONSUMER_SECRET) . '&' . urlencode(NETSUITE_TOKEN_SECRET);
        $signature = base64_encode(hash_hmac("sha1", $base_string, $sig_string, true));

        $auth_header = "OAuth "
            . 'oauth_signature="' . rawurlencode($signature) . '", '
            . 'oauth_version="' . rawurlencode($oauth_version) . '", '
            . 'oauth_nonce="' . rawurlencode($oauth_nonce) . '", '
            . 'oauth_signature_method="' . rawurlencode($oauth_signature_method) . '", '
            . 'oauth_consumer_key="' . rawurlencode(NETSUITE_CONSUMER_KEY) . '", '
            . 'oauth_token="' . rawurlencode(NETSUITE_TOKEN_ID) . '", '  
            . 'oauth_timestamp="' . rawurlencode($oauth_timestamp) . '", '
            . 'realm="' . rawurlencode(NETSUITE_ACCOUNT) .'"';

        return $auth_header;
    }



    // Restlet auth headers
    
}

