<?php
/**
 * @package  SomfySiteManager
 */

namespace Inc\Classes;


class ProductPricing{
    public $product_pricing_db;

    function __construct(){
        global $wpdb;
        $this->product_pricing_db = PRO_SITE_PREFIX . "product_pricing";
    }

    public function upsert($data){
		global $wpdb;

		$query = "INSERT INTO $this->product_pricing_db
			(
                `key`, 
                `item`, 
                `customer`, 
                `quantityrange`, 
                `unitprice`, 
                `pricelevel`, 
                `currency`
            ) VALUES ";

        for($i = 0; $i < count($data); $i++){
            $row = $data[$i];
            $rowKey = $row['item'] ."_". $row['customer']
                ."_". $row['quantityrange']."_". $row['pricelevel'];

            $query .= " (
                '$rowKey',
				'".$row['item']."',
				'".$row['customer']."',
				'".$row['quantityrange']."',
				'".$row['unitprice']."',
				'".$row['pricelevel']."',
				'".$row['currency']."'
            ) ";

            if($i < count($data) - 1) $query .= " ,";
        }
        $query .= " ON DUPLICATE KEY UPDATE 
            unitprice=VALUES(unitprice); ";

        return $wpdb->query($query);
	}
}