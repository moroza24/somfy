<?php
/**
 * @package  SomfySiteManager
 */

namespace Inc\Classes;

class DiscountCode {
    public $dbDiscountcodes;

    function __construct(){
        global $wpdb;
        $this->dbDiscountcodes = EU_SITE_PREFIX . "discountcodes";
    }

    public function getByInternalid($internalid){

    }
    // `internalid`
    // `custrecord_discountcode`
    // `custrecord_discountgroup`
    // `custrecord_discount_percent

    /** 
     * Description: updateDiscountCode
     * @param
     * @return 
     */
    public function updateDiscountCode($internalID, $discountCodes){
        global $wpdb;

        $deleteQuery = $wpdb->prepare("DELETE FROM $this->dbDiscountcodes WHERE internalid = '%s' ", $internalID);

        // Start Transaction
        $wpdb->query( "START TRANSACTION" );
        
        // delete and insert new DiscountCodes
        $deleteResult = $wpdb->query($deleteQuery);

        $insertResult = array();

        foreach ($discountCodes as $code){
            $custrecord_discountcode = $code['custrecord_discountcode'];
            $custrecord_discountgroup = $code['custrecord_discountgroup'];
            $custrecord_discount_percent = $code['custrecord_discount_percent'];
            $inserted = $wpdb->insert(
                $this->dbDiscountcodes, 
                array(
                    'internalid' => $internalID,
                    'custrecord_discountcode' => "$custrecord_discountcode",
                    'custrecord_discountgroup' => "$custrecord_discountgroup",
                    'custrecord_discount_percent' => "$custrecord_discount_percent"
                ), 
                array( 
                    '%s', '%s', '%s', '%s'
                )
            );
            array_push($insertResult, $inserted);
        }
        
        if(($deleteResult == 0 || $deleteResult > 0) && count($insertResult) == count($discountCodes)) {
            $wpdb->query('COMMIT'); // if you come here then well done
        }
        else {
            $wpdb->query('ROLLBACK'); // // something went wrong, Rollback
        }
        
        return array(
            'deleteResult' => $deleteResult,
            'insertResult' => $insertResult
        );
    }
}