<?php
/**
 * @package  SomfySiteManager
 */

namespace Inc\Classes;

use \Inc\Base\BaseController;
use \Inc\Classes\SyncLog;
use \Inc\Classes\NetSuiteApi;
use \Inc\Classes\Products;
use \Inc\Classes\Customers;
use \Inc\Classes\SomfyMessaging;

class SyncServer extends BaseController{

    public $SyncLog;
    public $netSuiteApi;
    public $Products;
    public $Customers;
    public $SomfyMessaging;

    function __construct(){
        $this->SyncLog = new SyncLog();
        $this->netSuiteApi = new NetSuiteApi();
        $this->Products = new Products();
        $this->Customers = new Customers();
        $this->SomfyMessaging = new SomfyMessaging();
    }

    /** 
     * Description: Sync Products from NetSuite to EU site
     *  Run by cron job every: 1 hour
     *  1. get last log entry for syncType = $syncLogType
     *  1.1. check lastLog status.
     *      if status = 'finished' - create new sync log record
     *      else - keep going
     *  1.2. set $last_modified = $syncLog->start_date
     * 
     *  2. call netSuiteAPI->getProducts($last_modified)
     *  2.1. If Error
     *      - if $syncLog->retires == 2 
     *          Send Email to options['sync_error_mailto'], options['sync_error_email_message']
     *          set: $syncLog->status = 'error', 'error' = $response->error
     *      - else
     *          set: $syncLog->status = 'retry', $syncLog->retries = $syncLog->retries + 1
     *  3. Got the DATA!!!
     *  3.1. $products->prepData($data)
     *  3.2. $products->updateData($prepedData)
     *  3.3. set: $syncLog->status = 'finished'
     * @param
     * @return
     */
    public function syncProducts($website, $productWebSites, $logId = null){
        if($website == 'eu'){
            $bid = 1;
        } else {
            $bid = 2;
        }
        switch_to_blog($bid); 
        
        $syncLogType = 'products_'. $website;

        // Get last log
        $startedLog = $this->SyncLog->getLog([
            'sync_type' => $syncLogType, 
            'status' => 'started',
            'limit' => 1
            ]);

        if(count($startedLog) == 1) {
            return array(
                'status' => false,
                'error' => 'one is already running'
            );
        }


        $lastLog = $this->SyncLog->getLog([
            'sync_type' => $syncLogType, 
            'status' => 'finished',
            'limit' => 1
            ]);

        if(count($lastLog) == 1) {
            // set last modified date
            $lastLog = $lastLog[0];
            $last_modified = $lastLog->start_date;
        } else {
            // no previous log found
            // set modified date to month ago
            $last_modified = date("Y-m-d h:i a", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-1 day" ) );
        }
        $last_modified__arr = $this->getModifiedDate($last_modified);

        // check if run by syncRetry and set logid
        if(!$logId){
            $logId = $this->SyncLog->new($syncLogType);
        } 
        
        // 2. call netSuiteAPI->getProducts($last_modified__arr)
        $netSuiteResponse = $this->netSuiteApi->getAllProducts($last_modified__arr);
        
        /*   2.1. If Error
        *      set: $syncLog->status = 'retry'
        * */
        if(!$netSuiteResponse['status']){
            $logRetry = $this->SyncLog->updateRetry($logId);

            return array(
                'status' => false,
                'error' => 'Couldnt get response, set for retry',
                'last_modified__arr' => $last_modified__arr,
                'netSuiteResponse' => $netSuiteResponse,
                'retry' => $logRetry
            );
        } else {
            // 3. Got the DATA!!!
            // check for records
            $products = $netSuiteResponse['data'];
            if($products > 0){
                // 3.1. Prep product data
                // 3.2. update/insert new products
                $productsUpdated = $this->Products->updateProducts($products, $productWebSites, $bid);

                if($productsUpdated['status']){
                    //  3.3. set: $syncLog->status = 'finished'
                    $logEnded = $this->SyncLog->end($logId, $productsUpdated['updated']['total']);

                    if($bid == 2){
                        $indexed_res = $this->Products->updateProductIndex_pro();
                        $indexedTitle_res = $this->Products->updateProductTitleIndex_pro();
                        $this->somfylog('Index - product full', $indexed_res);
                        $this->somfylog('Index - product title', $indexedTitle_res);
                    }

                    return array(
                        'status' => true,
                        'msg' => 'Product updated successfully',
                        'productsUpdated' => $productsUpdated,
                        'last_modified__arr' => $last_modified__arr,
                        'logEnded' => $logEnded
                    );
                } else {
                    // set: $syncLog->status = 'retry', $syncLog->retries = $syncLog->retries + 1
                    $logRetry = $this->SyncLog->updateRetry($logId);
                    return array(
                        'status' => false,
                        'error' => 'Couldnt update products',
                        'netSuiteResponse' => $netSuiteResponse,
                        'productsUpdated' => $productsUpdated,
                        'last_modified__arr' => $last_modified__arr,
                        'logRetry' => $logRetry
                    );
                }
            } else {
                //  3.3. set: $syncLog->status = 'finished'
                $logEnded = $this->SyncLog->end($logId, 0);
                return array(
                    'status' => true,
                    'msg' => 'No Product to update',
                    'last_modified__arr' => $last_modified__arr,
                    'logEnded' => $logEnded
                );
            }
        }
    }



    /** 
     * Description: 
     * @param
     * @return
     */
    public function syncCustomers($website, $customerTypes, $logId = null){
        if($website == 'eu'){
            $bid = 1;
        } else {
            $bid = 2;
        }
        switch_to_blog($bid); 
        $syncLogType = 'customers_'. $website;
        
        // Get last log
        $startedLog = $this->SyncLog->getLog([
            'sync_type' => $syncLogType, 
            'status' => 'started',
            'limit' => 1
            ]);

        if(count($startedLog) == 1) {
            return array(
                'status' => false,
                'error' => 'one is already running'
            );
        }

        $lastLog = $this->SyncLog->getLog([
            'sync_type' => $syncLogType, 
            'status' => 'finished',
            'limit' => 1
            ]);

        if(count($lastLog) == 1) {
            // set last modified date
            $lastLog = $lastLog[0];
            $last_modified = $lastLog->start_date;
        } else {
            // no previous log found
            // set modified date to one day ago
            $last_modified = date("Y-m-d h:i a", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-1 day" ) );
        }

        // set last_modified url params string
        $last_modified__arr = $this->getModifiedDate($last_modified);

        // check if run by syncRetry and set logid
        if(!$logId){
            $logId = $this->SyncLog->new($syncLogType);
        } 

        // 2. call netSuiteAPI->getAllCustomer($last_modified__arr)
        $netSuiteResponse = $this->netSuiteApi->getAllCustomer($last_modified__arr);

        /*   2.1. If Error
        *      set: $syncLog->status = 'retry'
        * */
        if(!$netSuiteResponse['status']){
            $logRetry = $this->SyncLog->updateRetry($logId);

            return array(
                'status' => false,
                'error' => 'Couldnt get response, set for retry',
                'netSuiteResponse' => $netSuiteResponse,
                'last_modified__arr' => $last_modified__arr,
                'retry' => $logRetry
            );
        } else {
            // 3. Got the DATA!!!
            // check for records
            $customers = $netSuiteResponse['data'];
            
            if($customers > 0){
                // 3.1. Prep product data
                // 3.2. update/insert new products
                $customersUpdated = $this->Customers->updateCustomers($customers, $customerTypes, $bid);

                if($customersUpdated['status']){
                    //  3.3. set: $syncLog->status = 'finished'
                    $logEnded = $this->SyncLog->end($logId, $customersUpdated['updated']['total']);
                    return array(
                        'status' => true,
                        'msg' => 'Customers updated successfully',
                        'last_modified__arr' => $last_modified__arr,
                        'customersUpdated' => $customersUpdated,
                        'logEnded' => $logEnded
                    );
                } else {
                    // set: $syncLog->status = 'retry', $syncLog->retries = $syncLog->retries + 1
                    $logRetry = $this->SyncLog->updateRetry($logId);
                    return array(
                        'status' => false,
                        'error' => 'Couldnt update customers',
                        'last_modified__arr' => $last_modified__arr,
                        'netSuiteResponse' => $netSuiteResponse,
                        'customersUpdated' => $customersUpdated,
                        'logRetry' => $logRetry
                    );
                }
            } else {
                //  3.3. set: $syncLog->status = 'finished'
                $logEnded = $this->SyncLog->end($logId, 0);
                return array(
                    'status' => true,
                    'msg' => 'No Customers to update',
                    'last_modified__arr' => $last_modified__arr,
                    'logEnded' => $logEnded
                );
            }
        }
    }


    
    /**
     * Description: recivePriceUpdates
     * incoming interface for updating products prices
     * @param
     * @return
     */
    public function recivePriceUpdates($request){
        $req_token = isset($request->get_headers()['somfy_authorization']) ? $request->get_headers()['somfy_authorization'][0] : null;

        if(!$req_token || $req_token != NETSUITE_REST_TOKEN){
            return new \WP_Error( 'forbidden_access', 'Access denied', array( 'status' => 401 ) );
        };

        $result['status'] = true;
        $result['payload'] = $request->get_params();

        $this->somfylog('recivePriceUpdates', $result);
        
        return $result;
    }



    /** 
     * Description: 
     * @param
     * @return
     */
    public function retryProductSync($website, $productWebSites){
        /**
         * get sync log where: status = retry, retries <= 3
         * for: retries < 3 => Run the sync again
         *  update sync retries count
         * for: retries = 3 => Send email to admin and somfy managment
         *  update sync status = error
         */
        // get sync log where: status = retry, retries <= 3
        $syncLogType = 'products_'. $website;
        $syncToRetry = $this->SyncLog->getLog(array('status' => 'retry', 'sync_type' => $syncLogType, 'limit' => '1'));
        
        if(count($syncToRetry) > 0){

            $syncAgain = $this->syncProducts($website, $productWebSites, $syncToRetry[0]->ID);

            if($syncAgain['status'] == false && $syncToRetry[0]->retries >= 2){
                // send email
                $emailTo = get_option('sync_mail_to');
                $emailMessage = get_option('sync_mail_content');

                $errorMessageSent = $this->SomfyMessaging->sendMail($emailTo, 'Somfy -SyncServer Error', $emailMessage, array(
                    'error_message' => $syncAgain['error'],
                    'website' => $website,
                    'sync_type' => 'Products',
                    'syncAgain' => json_encode($syncAgain)
                ));
                
                // log error
                $logError = $this->SyncLog->updateStatus($syncToRetry[0]->ID, 'error', $syncAgain['error']);
                
                return array(
                    'syncAgain' => $syncAgain,
                    'error' =>  $syncAgain['error'],
                    'logError' =>  $logError,
                    'errorMessageSent' =>  $errorMessageSent,
                    'status' => false
                );
            } else {
                return array(
                    'syncAgain' => $syncAgain,
                    'status' => true
                );
            }
        } else {
            return array(
                'msg' =>  'All Good, nothing to retry!',
                'status' => true
            );
        }
    }



    /** 
     * Description: 
     * @param
     * @return
     */
    public function retryCustomerSync($website, $customerTypes){
        /**
         * get sync log where: status = retry, retries <= 3
         * for: retries < 3 => Run the sync again
         *  update sync retries count
         * for: retries = 3 => Send email to admin and somfy managment
         *  update sync status = error
         */
        // get sync log where: status = retry, retries <= 3
        $syncLogType = 'customers_'. $website;
        $syncToRetry = $this->SyncLog->getLog(array(
            'status' => 'retry', 
            'sync_type' => $syncLogType, 
            'limit' => '1'));
        
        if(count($syncToRetry) > 0){

            $syncAgain = $this->syncCustomers($website, $customerTypes, $syncToRetry[0]->ID);

            if($syncAgain['status'] == false && $syncToRetry[0]->retries >= 2){
                // send email
                $emailTo = get_option('sync_mail_to');
                $emailMessage = get_option('sync_mail_content');

                $errorMessageSent = $this->SomfyMessaging->sendMail($emailTo, 'Somfy - SyncServer Error', $emailMessage, array(
                    'error_message' => $syncAgain['error'],
                    'website' => $website,
                    'sync_type' => 'Customers',
                    'syncAgain' => json_encode($syncAgain)
                ));
                
                // log error
                $logError = $this->SyncLog->updateStatus($syncToRetry[0]->ID, 'error', $syncAgain['error']);
                
                return array(
                    'syncAgain' => $syncAgain,
                    'error' =>  $syncAgain['error'],
                    'logError' =>  $logError,
                    'errorMessageSent' =>  $errorMessageSent,
                    'status' => false
                );
            } else {
                return array(
                    'syncAgain' => $syncAgain,
                    'status' => true
                );
            }
        } else {
            return array(
                'msg' =>  'All Good, nothing to retry!',
                'status' => true
            );
        }
    }


    
    /** 
     * Description: 
     * @param
     * @return
     */
    public function getModifiedDate($last_modified){
        $currentDate = current_time( "Y-m-d H:i:s", 0 );
        $startday_datetime = strtotime($last_modified);
        $endday_datetime = strtotime($currentDate);

        return array(
            // start date
            'startday' => date('d/m/Y', $startday_datetime),
            'sh' => date('h', $startday_datetime),
            'sm' => date('i', $startday_datetime),
            'sdt' => date('a', $startday_datetime),

            // end date
            'endday' => date('d/m/Y', $endday_datetime),
            'eh' => date('h', $endday_datetime),
            'em' => date('i', $endday_datetime),
            'edt' => date('a', $endday_datetime)
        );
    }
}
