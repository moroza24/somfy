<?php
/**
 * @package  SomfySiteManager
 */

namespace Inc\Classes;

use Inc\Classes\SomfyMessaging;
use \Inc\Base\BaseController;

class StockNotifictaion extends BaseController{
    public $dbStockNotification;
    public $dbPostmetaEU;
    public $dbPostmetaPRO;
    
    function __construct(){
        global $wpdb;
        $this->dbPostmetaEU = EU_SITE_PREFIX . "postmeta";
        $this->dbStockNotificationEU = EU_SITE_PREFIX . "stock_notification";

        $this->dbStockNotificationPRO = PRO_SITE_PREFIX . "stock_notification";
        $this->dbPostmetaPRO = PRO_SITE_PREFIX . "postmeta";

    }


    /** 
     * Description: checkStockNotification check "stockNotfications" db 
     *  and check for restock or for required stock
     * @param
     * @return
     */
    public function checkStockNotificationEU(){
        global $wpdb;

        $query = "SELECT sn.*, 
                pmeu.meta_value AS stock_quantity
            FROM $this->dbStockNotificationEU sn
            LEFT JOIN $this->dbPostmetaEU pmeu
                ON pmeu.post_id = sn.product_id AND pmeu.meta_key = 'stock_quantity'
            WHERE sn.user_notified = 0 AND
                (pmeu.meta_value > 0 AND sn.required_amount <= pmeu.meta_value)
            ";

        $res = $wpdb->get_results($query);

        if($res){
            $SomfyMessaging = new SomfyMessaging();
            switch_to_blog( 1 ); 
            
            for($i = 0; $i < count($res); $i++){
                
                $productID = $res[$i]->product_id;
         
                // send email to user

                $sendTo = $res[$i]->user_email;
                $subject = get_option('stock_notification__subject');
                $template = get_option('stock_notification__template');
                $htmlBody = $SomfyMessaging->getTemplate($template);

                $data = array(
                    'userEmail' => $sendTo,
                    'permalink' => get_the_permalink($productID),
                    'thumbnail_url' => get_products_files($productID, 'thumbnail')['url'],
                    'product_title' => get_the_title($productID)
                );
                
                $messageSent = $SomfyMessaging->sendMail($sendTo, $subject, $htmlBody, $data);
                // update db
                if($messageSent[0]['status'] == 'sent'){
                    $dbUpdated = $this->updateStockNotification($res[$i]->ID, 'eu');
                }
            }
            $result['status'] = true;
            $result['res'] = $res;
        } else {
            $result['status'] = true;
            $result['msg'] = 'nothing to update';
            $result['res'] = $res;
        }
        $this->somfylog('checkStockNotificationEU', $result);
    }



    /** 
     * Description: checkStockNotification check "stockNotfications" db 
     *  and check for restock or for required stock
     * @param
     * @return
     */
    public function checkStockNotificationPRO(){
        global $wpdb;

        $query = "SELECT sn.*, 
                pmpro.meta_value AS stock_quantity
            FROM $this->dbStockNotificationPRO sn
            LEFT JOIN $this->dbPostmetaPRO pmpro
                ON pmpro.post_id = sn.product_id AND pmpro.meta_key = 'stock_quantity'
            WHERE sn.user_notified = 0 AND
                (pmpro.meta_value > 0 AND sn.required_amount <= pmpro.meta_value)
            ";

        $res = $wpdb->get_results($query);

        if($res){
            $SomfyMessaging = new SomfyMessaging();
            switch_to_blog( 2 ); 
            
            for($i = 0; $i < count($res); $i++){

                $productID = $res[$i]->product_id;
         
                // send email to user

                $sendTo = $res[$i]->user_email;
                $subject = get_option('stock_notification__subject');
                $htmlBody = get_option('stock_notification__content');
                
                $data = array(
                    'userEmail' => $sendTo,
                    'permalink' => get_the_permalink($productID),
                    'thumbnail_url' => get_products_files($productID, 'thumbnail')['url'],
                    'product_title' => get_the_title($productID)
                );
                
                $messageSent = $SomfyMessaging->sendMail($sendTo, $subject, $htmlBody, $data);
                // update db
                if($messageSent[0]['status'] == 'sent'){
                    $dbUpdated = $this->updateStockNotification($res[$i]->ID, 'pro');
                }
            }
            $result['status'] = true;
            $result['res'] = $res;
        } else {
            $result['status'] = true;
            $result['msg'] = 'nothing to update';
            $result['res'] = $res;
        }
        $this->somfylog('checkStockNotificationPRO', $result);
    }
    /**
     * Description: StockNotification
     * @param string $id
     * @return 1/0 for query execution status
     */
    public function updateStockNotification($id, $site){
        global $wpdb;
        
        $updateDate = current_time( "Y-m-d H:i:s", 0 );

        if($site == 'eu'){
            $query = "UPDATE $this->dbStockNotificationEU
                SET user_notified = 1, 
                    update_date = '$updateDate'
                WHERE ID = $id";
        } else {
            $query = "UPDATE $this->dbStockNotificationPRO
                SET user_notified = 1, 
                    update_date = '$updateDate'
                WHERE ID = $id";
        }
        

        return $wpdb->query($query);

    }
}