<?php
/**
 * @package  SomfySiteManager
 */

namespace Inc\Classes;

use \Inc\Base\BaseController;

class SyncLog extends BaseController{

    public $dbSynclog;
    public $dbSomfylog;
    public $logStatus_started;
    public $logStatus_retry;
    public $logStatus_finished;

    function __construct(){
        global $wpdb;
        $this->dbSynclog = EU_SITE_PREFIX . "synclog";
        $this->dbSomfylog = EU_SITE_PREFIX . "somfylog";

        $this->logStatus_started = 'started'; // on log start, status set to 'started'
        $this->logStatus_retry = 'retry'; // if the sync failed, status set to 'retry' and the 'retries' count increased
        $this->logStatus_error = 'error'; // after 3 retries, status set to 'error'
        $this->logStatus_finished = 'finished'; // if the sync succeeded, status set to 'finished'

        /**
         * Sync types: products_eu, products_pro, customers, prices
         */
    }


    /**
     * Description: Create new sync log record by sync_type
     *  Sync types: products_eu, products_pro, customers, prices
     * @param string $sync_type
     * @return 1/0 for query execution status
     */
    public function new($sync_type){
        global $wpdb;

		$createDate = current_time( "Y-m-d H:i:s", 0 );
        $newStatus = $this->logStatus_started;

		$query = "INSERT INTO $this->dbSynclog
			(
                `ID`, 
                `sync_type`, 
                `status`, 
                `retries`, 
                `error`, 
                `updated`,
                `start_date`, 
                `end_date`, 
                `update_date`
            ) VALUES (
                NULL,
				'$sync_type', 
				'$newStatus',
				'0', 
				NULL, 
				'0', 
				'$createDate', 
				'$createDate', 
				'$createDate'
		)";

        $res = $wpdb->query($query);
        return $wpdb->insert_id;
	}

     
    /**
     * Description: Update Status of Sync log record
     * @param string $id
     * @param string $newStatus
     * @return 1/0 for query execution status
     */
    public function updateStatus($id, $newStatus, $error = null){
        global $wpdb;
        
        $updateDate = current_time( "Y-m-d H:i:s", 0 );
        $updateError = isset($error) ? " error = '$error' , " : '';

        $query = "UPDATE $this->dbSynclog
            SET status = '$newStatus', 
                $updateError
                update_date = '$updateDate'
            WHERE ID = $id";

        return $wpdb->query($query);

    }


    /**
     * Description: Update Retry Sync log recordes
     * @param string $id
     * @return 1/0 for query execution status
     */
    public function updateRetry($id){
        global $wpdb;
        
        $sqlGetLog = "SELECT * 
            FROM $this->dbSynclog l
            WHERE l.ID = $id
            ";
        $log_res = $wpdb->get_row($sqlGetLog);


        $retries = $log_res->retries;

        $retryLogStatus = $this->logStatus_retry;
        $updateDate = current_time( "Y-m-d H:i:s", 0 );
        $newRetriesCount = $retries + 1;

        $query = "UPDATE $this->dbSynclog
            SET status = '$retryLogStatus', 
                update_date = '$updateDate',
                retries = '$newRetriesCount'
            WHERE ID = $id";

        return $wpdb->query($query);
    }



    /**
     * Description: End Sync log record
     * @param string $id
     * @return 1/0 for query execution status
     */
    public function end($id, $updated = 0){
        global $wpdb;
        
        $endDate = current_time( "Y-m-d H:i:s", 0 );
        $endStatus = $this->logStatus_finished;

        $query = "UPDATE $this->dbSynclog
            SET status = '$endStatus', 
                updated = '$updated', 
                update_date = '$endDate', 
                end_date = '$endDate'
            WHERE ID = $id";

        return $wpdb->query($query);

    }


    /**
     * Description: Get all Sync log records
     * @param array $filters Optional - [id, sync_type, status, date, limit]
     * @return array of Sync log records
     */
    public function getLog($filters = null){

        global $wpdb;

        $id = isset($filters['id']) ? $filters['id'] : null;
        $sync_type = isset($filters['sync_type']) ? $filters['sync_type'] : null;; 
        $status = isset($filters['status']) ? $filters['status'] : null;;
        $date = isset($filters['date']) ? $filters['date'] : null;;
        $limit = isset($filters['limit']) ? $filters['limit'] : null;;

        $sqlWhere = "WHERE 1=1";

        // fillter by log ID
        if($id) $sqlWhere .= " AND l.ID = $id";
            
        // fillter by log sync_type
        if($sync_type) $sqlWhere .= " AND l.sync_type = '$sync_type'";
                
        // filter by log status
        if($status) $sqlWhere .= " AND l.status = '$status'";

        // filter by log date range
        if($date){
            $fromDate = $date['fromDate'];
            $toDate = $date['toDate'];

            $fromYear = $fromDate['year'];
            $fromMonth = $fromDate['month'];
            $fromDay = $fromDate['day'];

            $toYear = $toDate['year'];
            $toMonth = $toDate['month'];
            $toDay = $toDate['day'];
            
            $from = date("$fromYear-$fromMonth-$fromDay 00:00:00");
            $to = date("$toYear-$toMonth-$toDay 23:59:00");

            $sqlWhere .= " AND (l.start_date >= '$from' AND l.start_date <= '$till')";
        }

        // Limit
        $sqlLimit = isset($limit) ? " LIMIT 0 ,$limit" : "";

		$sql = "SELECT * 
            FROM $this->dbSynclog l
            $sqlWhere
            ORDER BY l.ID DESC
            $sqlLimit
            ";
        return $wpdb->get_results($sql);
    }


    /**
     * Description: Cron function for clearing the log.
     *  Keep log of last 30 days
     *  Run every day at midnight
     * @param
     * @return 1/0 for query execution status
     */
    public function cron_clearLog(){
        global $wpdb;
        
        $limitDate = date("Y-m-d", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-1 month" ) );
        $limitYear = date('Y', strtotime($limitDate));
        $limitMonth = date('m', strtotime($limitDate));
        $limitDay = date('d', strtotime($limitDate));

        $limitDateSql = date("$limitYear-$limitMonth-$limitDay 00:00:00");

        $sqlSynclog = "DELETE FROM $this->dbSynclog
            WHERE start_date <= '$limitDateSql'
            ";

        $synclogCleared = $wpdb->query($sqlSynclog);


        $sqlSomfylog = "DELETE FROM $this->dbSomfylog
            WHERE create_date <= '$limitDateSql'
            ";

        $somfylogCleared = $wpdb->query($sqlSomfylog);

        $this->somfylog('cron_clearLog', array(
            'somfylogCleared' => $somfylogCleared, 'synclogCleared' => $synclogCleared)
        );
    }

}