<?php
/**
 * @package  SomfySiteManager
 */
namespace Inc\Classes;

use \Inc\Classes\DiscountCode;
use \Inc\Classes\NetSuiteApi;
use \Inc\Classes\ProductPricing;

class Customers {
    public $fieldsToExclude;
    public $booleanFields;
    public $multiselectFields;
    public $DiscountCode;
    public $netSuiteApi;
    public $ProductPricing;

    function __construct(){
        $this->DiscountCode = new DiscountCode();
        $this->netSuiteApi = new NetSuiteApi();
        $this->ProductPricing = new ProductPricing();

        $this->fieldsToExclude = array(
            'recordtype',
            'recordid',
            'externalid',
            'firstname',
            'lastname',
            'discount',
            'email',
            'lastmodifieddate',
            'datecreated',
            'receivablesaccount',
            'salesrep',
            'custitem_somfy_web_product_category',
            'custitem_sub_product_group'
        );

        $this->booleanFields = array(
            'custentity_hipro12model',
            'subscribed',
        );

        $this->multiselectFields = array(
            'custentity_product_category',
        );
    }

    /** 
     * Description: updateCustomers
     *  1. Loop all customers
     *  2. check if customer exits with internalID
     *  2.1. if not create new user with role = customer
     *  3. update the fields
     * @param
     * @return 
     */
    public function updateCustomers($netSuite_customers, $customerTypes, $bid){
        switch_to_blog($bid); 

        $updatedCounter = 0;
        $createdCounter = 0;
        $errorCounter = 0;
        $errorCustomers = array();
        $customerInternals = array();
        
        foreach($netSuite_customers as $customer){
            // check if needed to update base on Customer Type
            if(!in_array($customer['custentity_customertype'], $customerTypes)){
                continue;
            }

            $customerInternalID = $customer['internalid'];

            // 2. check if customer exits with internalID
            $customerRole = get_option('site_customer_role');

            $customerExists = $this->checkIfCustomerExists($customerInternalID, $customerRole, $bid);

            if(count($customerExists) == 0){
                //  2.1. if not create new Customer
                $customerCreated = $this->createNewCustomer($customer, $bid);

                if($customerCreated['status']){
                    $createdCounter += 1;
                    array_push($customerInternals, $customerInternalID);
                } else {
                    $errorCounter += 1;
                    array_push($errorCustomers, array($customerCreated, $customer));
                    continue;
                }
                $customerID = $customerCreated['user_id'];
            } else {
                $customerID = $customerExists[0];
            }

            // 3. update the fields
            $customerUpdated = $this->updateCustomerDate($customerID, $customer, $bid);
            $updatedCounter += 1;
        }

        // Sync prices
        $pricesUpdated = $this->syncPrices($customerInternals, $bid);

        return array(
            'status' => true,
            'msg' => 'Customer updated successfully',
            'pricesUpdated' => $pricesUpdated,
            'updated' => array(
                'total' => $updatedCounter + $errorCounter,
                'updatedCounter' => $updatedCounter,
                'createdCounter' => $createdCounter,
                'errorCounter' => $errorCounter,
                'errorCustomers' => $errorCustomers,
            )
        );
    }

/** 
     * Description: syncPrices
     * @param
     * @return 
     */
    public function syncPrices($customerInternals, $bid){
        switch_to_blog($bid); 
        if($bid == 1){
        //  EU site
            return array(
                'status' => true
            );
        } else if($bid == 2){
        // PRO site
            $customerPriceUpdateResults = array('errors' => array(),'success' => array());

            foreach($customerInternals as $customer){
                $netPricesData = $this->netSuiteApi->getProductsPricePRO($customer, 0, null);
                if(!$netPricesData['status']){
                    array_push($customerPriceUpdateResults['errors'], array(
                        'status' => false,
                        'error' => 'Couldnt get response',
                        'customer' =>$customer
                    ));
                } else {
                    // update database 
                    $productsPrices = $netPricesData['data'];
                    $chunks_results = array();

                    if(is_array($productsPrices) && count($productsPrices) > 0){
                        $chunks = array_chunk($productsPrices, 500);
    
                        foreach($chunks as $chunk){
                            $query = $this->ProductPricing->upsert($chunk);
                            array_push($chunks_results, $query);
                        }
                    }
                    

                    array_push($customerPriceUpdateResults['success'], array(
                        'status' => true,
                        'chunks_results' => $chunks_results,
                        'count' => count($netPricesData['data']),
                        'customer' =>$customer
                    ));
                }
            }
            
            if(count($customerPriceUpdateResults['errors']) > 0){
                return array(
                    'status' => false,
                    'couldnt update prices',
                    'error' => $customerPriceUpdateResults['errors']
                );
            } else {
                return array(
                    'status' => true,
                    'success' => $customerPriceUpdateResults['success']
                );
            }
        }

    }

    /** 
     * Description: updateCustomerDate
     * @param
     * @return 
     */
    public function updateCustomerDate($customerID, $customer, $bid){
        switch_to_blog($bid); 

        foreach($customer as $key => $value){
            // Update boolean fields
            if(in_array($key, $this->booleanFields)){
                $modifiedValue = $value == 'F' ? '0' : '1';
                update_field($key, $modifiedValue, 'user_'.$customerID);
                continue;
            }

            // Update multi select Fields
            if(in_array($key, $this->multiselectFields)){
                $modifiedValue = explode(',', $value);
                update_field($key, $modifiedValue, 'user_'.$customerID);
                continue;
            }

            // Update other text, number, select fields
            if(!in_array($key, $this->fieldsToExclude)){
                update_field($key, $value, 'user_'.$customerID);
                continue;
            }
        }

        // Update Sales rep
        update_field('salesrep', $customer['salesrep'], 'user_'.$customerID);

        // update user wp fields
        if($customer['companyname'] == ''){
            $nickname = $customer['firstname'] ." " .$customer['lastname'];
        } else {
            $nickname = $customer['companyname'];
        }
        update_field('first_name', $customer['firstname'], 'user_'.$customerID);
        update_field('last_name', $customer['lastname'], 'user_'.$customerID);
        update_field('nickname', $nickname, 'user_'.$customerID);
        update_field('display_name', $nickname, 'user_'.$customerID);
        update_field('user_email', $customer['email'], 'user_'.$customerID);
        
        global $wpdb;
        $wpdb->update($wpdb->users, array(
            'user_login' => $customer['email'] == '' ? $customer['entityid'] : $customer['email'],
            'user_email' => $customer['email'],
            'display_name' => $nickname,
        ), array('ID' => $customerID));

        // Update discount codes  PRO only
        if($bid == 2):
            $dicountcode_updated = $this->DiscountCode->updateDiscountCode($customer['internalid'], $customer['discount']);
            
            $syncCustomerCategories = $this->syncCustomerCategories($customer['internalid'], $customerID, $bid);
            $syncCustomerSubgroups = $this->syncCustomerSubgroups($customer['internalid'], $customerID, $bid);
            $syncCustomer2Subgroups = $this->syncCustomer2Subgroups($customer['internalid'], $customerID, $bid);
        endif;
        return true;
    }




    /** 
     * Description: createNewCustomer
     * @param
     * @return 
     */
    public function createNewCustomer($customer, $bid){
        switch_to_blog($bid); 

        // check for email - 
        $userName = $customer['entityid'];

        $customerRole = get_option('site_customer_role');

        $userData = array(
            'user_pass' => wp_generate_password( 10, true, false ), 
            'user_login' => $userName,
            'show_admin_bar_front' => 'false',
            'role' => $customerRole
            
        );

        $user_id = wp_insert_user($userData);

        // On success.
        if ( ! is_wp_error( $user_id ) ) {
            update_user_meta( $user_id, 'account_status', 'awaiting_admin_review');
            update_user_meta( $user_id, 'primary_blog', $bid);

            return array(
                'status' => true,
                'user_id' => $user_id
            );
        } else {
            return array(
                'status' => false,
                'error' => $user_id
            );
        }
    }



    /** 
     * Description: checkIfCustomerExists
     * @param
     * @return 
     */
    public function checkIfCustomerExists($internalID, $role, $bid){
        switch_to_blog($bid); 

        $internalIds = explode(',',$internalID);

        $customer = new \WP_User_Query(array(
            'role' => $role,
            'meta_query' => array(
                array(
                    'key' => 'internalid',
                    'compare' => 'IN',
                    'value' => $internalIds
                )
            )
        )); 
        
        $ids = array();
        if(count($customer->results) > 0){
            foreach($customer->results as $cust){
                array_push($ids, $cust->ID);
            }
        }

        return $ids;
    }


    /** 
     * Description: getCustomerData
     * @param
     * @return 
     */
    public function getCustomerData($internalID){

        $customer = new \WP_User_Query(array(
            'meta_query' => array(
                array(
                    'key' => 'internalid',
                    'compare' => '=',
                    'value' => $internalID
                )
            )
        )); 
        
        $ids = array();
        if(count($customer->results) > 0){
            foreach($customer->results as $cust){
                array_push($ids, $cust->ID);
            }
        }

        $userID = $ids[0];

        return array(
            'userID' => $userID,
            'internalid' => $internalID,
            'companyname' => get_user_meta($userID, 'companyname')[0],
            'first_name' => get_user_meta($userID, 'first_name')[0],
            'last_name' => get_user_meta($userID, 'last_name')[0],
            'edit_user_link' => get_edit_user_link($userID)
        );
    }
    
    /** 
     * Description: getCustomerCategories
     * @param
     * @return 
     */
    public function syncCustomerCategories($internalID, $userID, $bid){
        switch_to_blog($bid); 
        global $wpdb;

        $dbDiscountcode = EU_SITE_PREFIX . "discountcodes";
        $dbPosts = PRO_SITE_PREFIX . "posts";
        $dbPostmeta = PRO_SITE_PREFIX . "postmeta";

        $sql = "SELECT 
                DISTINCT
                c.category
                
            FROM $dbDiscountcode dc
            JOIN (
                SELECT DISTINCT
                    pmd.meta_value AS discountcode,
                    pmc.meta_value AS category
                FROM $dbPosts p
                JOIN $dbPostmeta pmd
                    ON pmd.post_id = p.ID AND pmd.meta_key = 'custitem_discountcode'
            
                JOIN $dbPostmeta pmc
                    ON pmc.post_id = p.ID AND pmc.meta_key = 'custitem_somfy_web_product_category'
                WHERE p.post_type = 'product' AND p.post_status = 'publish'
            ) c ON c.discountcode = dc.custrecord_discountcode
            WHERE dc.internalid = '$internalID'
        ";

        $res = $wpdb->get_results($sql);
        $fa = array();

        if($res){
            foreach($res as $r){
                $cat = $r->category;
                if(is_serialized($cat)){
                    $sCat = maybe_unserialize($cat);
                    foreach($sCat as $c){
                        array_push($fa, $c);
                    }
                } else {
                    array_push($fa, $cat);
                }
            }
        }

        // add categories that open to all
        $openCat = explode(',', str_replace(' ', '', get_option('shop_show_tax__category')));
        $f = array_merge($fa, $openCat);
        return update_field('custitem_somfy_web_product_category', array_unique($f), 'user_'.$userID);
    }

    /** 
     * Description: getCustomerSubgroups
     * @param
     * @return 
     */
    public function syncCustomerSubgroups($internalID, $userID, $bid){
        switch_to_blog($bid); 
        global $wpdb;

        $dbDiscountcode = EU_SITE_PREFIX . "discountcodes";
        $dbPosts = PRO_SITE_PREFIX . "posts";
        $dbPostmeta = PRO_SITE_PREFIX . "postmeta";

        $sql = "SELECT 
                DISTINCT
                c.category
                
            FROM $dbDiscountcode dc
            JOIN (
                SELECT DISTINCT
                    pmd.meta_value AS discountcode,
                    pmc.meta_value AS category
                FROM $dbPosts p
                JOIN $dbPostmeta pmd
                    ON pmd.post_id = p.ID AND pmd.meta_key = 'custitem_discountcode'
            
                JOIN $dbPostmeta pmc
                    ON pmc.post_id = p.ID AND pmc.meta_key = 'custitem_sub_product_group'	
                WHERE p.post_type = 'product' AND p.post_status = 'publish'
            ) c ON c.discountcode = dc.custrecord_discountcode
            WHERE dc.internalid = '$internalID'
        ";

        $res = $wpdb->get_results($sql);
        $fa = array();

        if($res){
            foreach($res as $r){
                $cat = $r->category;
                if(is_serialized($cat)){
                    $sCat = maybe_unserialize($cat);
                    foreach($sCat as $c){
                        array_push($fa, $c);
                    }
                } else {
                    array_push($fa, $cat);
                }
            }
        }

        // add categories that open to all
        $openGroup = explode(',', str_replace(' ', '', get_option('shop_show_tax__subgroup')));
        $f = array_merge($fa, $openGroup);

        return update_field('custitem_sub_product_group', array_unique($f), 'user_'.$userID);
    }

    /** 
     * Description: getCustomerSubgroups
     * @param
     * @return 
     */
    public function syncCustomer2Subgroups($internalID, $userID, $bid){
        switch_to_blog($bid); 
        global $wpdb;

        $dbDiscountcode = EU_SITE_PREFIX . "discountcodes";
        $dbPosts = PRO_SITE_PREFIX . "posts";
        $dbPostmeta = PRO_SITE_PREFIX . "postmeta";

        $sql = "SELECT 
                DISTINCT
                c.category
                
            FROM $dbDiscountcode dc
            JOIN (
                SELECT DISTINCT
                    pmd.meta_value AS discountcode,
                    pmc.meta_value AS category
                FROM $dbPosts p
                JOIN $dbPostmeta pmd
                    ON pmd.post_id = p.ID AND pmd.meta_key = 'custitem_discountcode'
            
                JOIN $dbPostmeta pmc
                    ON pmc.post_id = p.ID AND pmc.meta_key = 'custitem_sub2productgroup'	
                WHERE p.post_type = 'product' AND p.post_status = 'publish'
            ) c ON c.discountcode = dc.custrecord_discountcode
            WHERE dc.internalid = '$internalID'
        ";

        $res = $wpdb->get_results($sql);
        $fa = array();

        if($res){
            foreach($res as $r){
                $cat = $r->category;
                if(is_serialized($cat)){
                    $sCat = maybe_unserialize($cat);
                    foreach($sCat as $c){
                        array_push($fa, $c);
                    }
                } else {
                    array_push($fa, $cat);
                }
            }
        }
        // add categories that open to all
        $openSubGroup = explode(',', str_replace(' ', '', get_option('shop_show_tax__subsubgroup')));
        $f = array_merge($fa, $openSubGroup);

        return update_field('custitem_sub2productgroup', array_unique($f), 'user_'.$userID);
    }

}
