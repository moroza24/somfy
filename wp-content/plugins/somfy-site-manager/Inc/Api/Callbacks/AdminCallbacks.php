<?php 
/**
 * @package  SomfySiteManager
 */

namespace Inc\Api\Callbacks;

use Inc\Base\BaseController;

class AdminCallbacks extends BaseController
{
    public function adminDashboard(){
		return require_once( "$this->plugin_path/templates/dashboard/dashboard.php" );
	}


	public function adminSyncLog(){
		return require_once( "$this->plugin_path/templates/dashboard/syncLog.php" );
	}

	public function adminOrdersPage(){
		return require_once( "$this->plugin_path/templates/dashboard/ordersPage.php" );
	}

	public function siteSettings(){
		return require_once( "$this->plugin_path/templates/dashboard/siteSettings.php" );
	}
}