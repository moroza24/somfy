<?php 
/**
 * @package  SomfySiteManager
 */
namespace Inc\Api\Callbacks;

use Inc\Base\BaseController;

class ManagerCallbacks extends BaseController
{

    
	public function adminSectionManager(){
		// echo 'Manage the Sections and Features of this Plugin by activating the checkboxes from the following list.';
    }


	public function checkboxField( $args ){
		$name = $args['label_for'];
		$classes = $args['class'];
		$checkbox = get_option( $name );
		echo '<input type="checkbox" name="' . $name . '" value="1" class="' . $classes . '" ' . ($checkbox ? 'checked' : '') . '>';
    }
    
    public function textInputField( $args )	{
		$name = $args['label_for'];
        $classes = $args['class'];
        $value = get_option($name);
		$placeholder = isset($args['placeholder']) ? $args['placeholder'] : '';
		echo '<input 
			type="text" 
			name="' . $name . '" 
			class="' . $classes . '" 
			value="' . $value . '" 
			placeholder="' . $placeholder . '">';
	}


	public function numberInputField( $args )	{
		$name = $args['label_for'];
        $classes = $args['class'];
        $value = get_option($name);
		$placeholder = isset($args['placeholder']) ? $args['placeholder'] : '';
		$min = isset($args['min']) ? $args['min'] : '';
		$max = isset($args['max']) ? $args['max'] : '';
		echo '<input 
			type="number" 
			name="' . $name . '" 
			class="' . $classes . '" 
			value="' . $value . '" 
			min="' . $min . '" 
			max="' . $max . '" 
			placeholder="' . $placeholder . '">';
	}


	public function emailInputField( $args )	{
		$name = $args['label_for'];
        $classes = $args['class'];
        $value = get_option($name);
		$placeholder = isset($args['placeholder']) ? $args['placeholder'] : '';
		echo '<input 
			type="email" 
			name="' . $name . '" 
			class="' . $classes . '" 
			value="' . $value . '" 
			placeholder="' . $placeholder . '">';
	}


	public function textareaInputField( $args )	{
		$name = $args['label_for'];
		$classes = isset($args['class']) ? $args['class'] : '';
		$placeholder = isset($args['placeholder']) ? $args['placeholder'] : '';
        $value = get_option($name);
		
		echo '<textarea 
			rows="10"
			name="' . $name . '" 
			class="' . $classes . '" 
			placeholder="' . $placeholder . '">' . $value . '
		</textarea>';
	}

	public function checkboxSanitize( $input )	{
		// return filter_var($input, FILTER_SANITIZE_NUMBER_INT);
		return ( isset($input) ? true : false );
    }
    
    public function textInputSanitize( $input ){
		return $input;
	}

	public function numberInputSanitize( $input ){
		return intval($input);
	}
}