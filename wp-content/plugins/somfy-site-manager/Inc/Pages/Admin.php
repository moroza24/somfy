<?php 
/**
 * @package  SomfySiteManager
 */

namespace Inc\Pages;

use Inc\Api\SettingsApi;
use Inc\Base\BaseController;
use Inc\Api\Callbacks\AdminCallbacks;
use Inc\Api\Callbacks\ManagerCallbacks;


class Admin extends BaseController
{
	public $settings;
	public $adminCallbacks;
	public $managerCallbacks;
	public $managerSettings = array();

	public $pages = array();
	public $subpages = array();


	public function register() {
		$this->settings = new SettingsApi();
		$this->adminCallbacks = new AdminCallbacks();
		$this->managerCallbacks = new ManagerCallbacks();

		$this->setManagerSettings();
		$this->setPages();
		$this->setSubpages();
		$this->setSettings();
		$this->setSections();
		$this->setFields();
		
		$this->settings->addPages( $this->pages )->withSubPage( 'לוח בקרה' )->addSubPages( $this->subpages )->register();
	}


	public function setPages(){
		$this->pages = array(
			array(
				'page_title' => 'Somfy Site Manager Plugin', 
				'menu_title' => 'ניהול אתר - סומפי', 
				'capability' => 'manage_events', 
				'menu_slug' => 'somfy_site_manager_plugin', 
				'callback' => array( $this->adminCallbacks, 'adminDashboard' ), 
				'icon_url' => 'dashicons-groups', 
				'position' => 0
			)
		);
	}

	public function setSubpages(){
		$this->subpages = array(
			array(
				'parent_slug' => 'somfy_site_manager_plugin', 
				'page_title' => 'הזמנות', 
				'menu_title' => 'הזמנות', 
				'capability' => 'manage_options', 
				'menu_slug' => 'somfy_site_manager_orders', 
				'callback' => array( $this->adminCallbacks, 'adminOrdersPage' )
			),
			array(
				'parent_slug' => 'somfy_site_manager_plugin', 
				'page_title' => 'לוג סינכרון', 
				'menu_title' => 'לוג סינכרון', 
				'capability' => 'manage_options', 
				'menu_slug' => 'somfy_site_manager_plugin_sync_log', 
				'callback' => array( $this->adminCallbacks, 'adminSyncLog' )
			),
			array(
				'parent_slug' => 'somfy_site_manager_plugin', 
				'page_title' => 'הגדרות', 
				'menu_title' => 'הגדרות', 
				'capability' => 'manage_options', 
				'menu_slug' => 'somfy_site_manager_plugin_settings', 
				'callback' => array( $this->adminCallbacks, 'siteSettings' )
			)
		);
	}


	public function setSettings(){
		$args = array();
		foreach ( $this->managerSettings as $key => $value ) {
			$args[] = array(
				'option_group' => 'somfy_site_manager_plugin_settings',
				'option_name' => $key,
				'callback' => array( $this->managerCallbacks, $value['setting_callback'] )
			);
		}
		$this->settings->setSettings( $args );
	}


	public function setSections(){
		$args = array(
			array(
				'id' => 'somfy_site_manager_site_general',
				'title' => 'הגדרות כלליות',
				'callback' => array( $this->managerCallbacks, 'adminSectionManager' ),
				'page' => 'somfy_site_manager_plugin_settings'
			),
			array(
				'id' => 'somfy_site_manager_shop',
				'title' => 'הגדרות חנות',
				'callback' => array( $this->managerCallbacks, 'adminSectionManager' ),
				'page' => 'somfy_site_manager_plugin_settings'
			),
			array(
				'id' => 'somfy_site_manager_warranty',
				'title' => 'הגדרות הרחבת אחריות',
				'callback' => array( $this->managerCallbacks, 'adminSectionManager' ),
				'page' => 'somfy_site_manager_plugin_settings'
			),
			array(
				'id' => 'somfy_site_manager_shipping',
				'title' => 'הגדרות משלוח',
				'callback' => array( $this->managerCallbacks, 'adminSectionManager' ),
				'page' => 'somfy_site_manager_plugin_settings'
			),
			array(
				'id' => 'somfy_site_manager_emails',
				'title' => 'מיילים אוטומטים',
				'callback' => array( $this->managerCallbacks, 'adminSectionManager' ),
				'page' => 'somfy_site_manager_plugin_settings'
			),
			array(
				'id' => 'somfy_site_manager_popups',
				'title' => 'פופאפים',
				'callback' => array( $this->managerCallbacks, 'adminSectionManager' ),
				'page' => 'somfy_site_manager_plugin_settings'
			),
			array(
				'id' => 'somfy_site_manager_inwise',
				'title' => 'InWise API',
				'callback' => array( $this->managerCallbacks, 'adminSectionManager' ),
				'page' => 'somfy_site_manager_plugin_settings'
			),
			array(
				'id' => 'somfy_site_manager_sync_server',
				'title' => 'מערכת סינכרון',
				'callback' => array( $this->managerCallbacks, 'adminSectionManager' ),
				'page' => 'somfy_site_manager_plugin_settings'
			)
		);
		$this->settings->setSections( $args );
	}


	public function setFields(){
		$args = array();
		foreach ( $this->managerSettings as $key => $value ) {
			$args[] = array(
				'id' => $key,
				'title' => $value['name'],
				'callback' => array( $this->managerCallbacks, $value['field_callback'] ),
				'page' => 'somfy_site_manager_plugin_settings',
				'section' => $value['section'],
				'args' => $value['args']
			);
		}
		$this->settings->setFields( $args );
	}

	public function setManagerSettings(){
		$this->managerSettings = array(
			// warranty 
			'shop_warranty_product_id' => array(
				"name" =>'אחריות ProductID',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_warranty",
				"args" => array(
					'label_for' => 'shop_warranty_product_id',
					'class' => 'regular-text',
					'placeholder' => 'Product ID'
				)
			),
			'warranty_categories_list' => array(
				"name" =>'הרבחת אחריות - PRODUCT CATEGORY',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_warranty",
				"args" => array(
					'label_for' => 'warranty_categories_list',
					'class' => 'regular-text',
					'placeholder' => ''
				)
			),
			'warranty_subgroups_list' => array(
				"name" =>'הרבחת אחריות - SUB PRODUCT GROUP',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_warranty",
				"args" => array(
					'label_for' => 'warranty_subgroups_list',
					'class' => 'regular-text',
					'placeholder' => ''
				)
			),
			'warranty_sub2groups_list' => array(
				"name" =>'הרבחת אחריות - SUB 2 PRODUCT GROUP',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_warranty",
				"args" => array(
					'label_for' => 'warranty_sub2groups_list',
					'class' => 'regular-text',
					'placeholder' => ''
				)
			),

			// Shipping
			'shop_shipping_product_id' => array(
				"name" =>'משלוח ProductID',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_shipping",
				"args" => array(
					'label_for' => 'shop_shipping_product_id',
					'class' => 'regular-text',
					'placeholder' => 'Product ID'
				)
			),
			'shop_shipping_product_internalid' => array(
				"name" =>'משלוח Product Internalid',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_shipping",
				"args" => array(
					'label_for' => 'shop_shipping_product_internalid',
					'class' => 'regular-text',
					'placeholder' => 'Product ID'
				)
			),
			'shop_free_shipping_threshold' => array(
				"name" =>'משלוח חינם מעל',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_shipping",
				"args" => array(
					'label_for' => 'shop_free_shipping_threshold',
					'class' => 'regular-text',
					'placeholder' => 'Threshold'
				)
			),

			// Shop 
			'shop_product_details_fields' => array(
				"name" =>'Product Details - ACF Group',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_shop",
				"args" => array(
					'label_for' => 'shop_product_details_fields',
					'class' => 'regular-text',
					'placeholder' => ''
				)
			),
			'shop_min_stock_quantity' => array(
				"name" =>'כמות מלאי מינימלית להצגה',
				"setting_callback" => "numberInputSanitize",
				"field_callback" => "numberInputField",
				"section" => "somfy_site_manager_shop",
				"args" => array(
					'label_for' => 'shop_min_stock_quantity',
					'class' => 'regular-text',
					'placeholder' => 'Stock quantity',
					'min' => 0,
					'max' => 1000
				)
			),
			'shop_rate' => array(
				"name" =>'אחוז מע"מ',
				"setting_callback" => "numberInputSanitize",
				"field_callback" => "numberInputField",
				"section" => "somfy_site_manager_shop",
				"args" => array(
					'label_for' => 'shop_rate',
					'class' => 'regular-text',
					'placeholder' => 'Rate'
				)
			),

			'shop_show_tax__category' => array(
				"name" =>'Shop Show category',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_shop",
				"args" => array(
					'label_for' => 'shop_show_tax__category',
					'class' => 'regular-text',
					'placeholder' => ''
				)
			),
			'shop_show_tax__subgroup' => array(
				"name" =>'Shop Show subgroup',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_shop",
				"args" => array(
					'label_for' => 'shop_show_tax__subgroup',
					'class' => 'regular-text',
					'placeholder' => ''
				)
			),
			'shop_show_tax__subsubgroup' => array(
				"name" =>'Shop Show subsubgroup',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_shop",
				"args" => array(
					'label_for' => 'shop_show_tax__subsubgroup',
					'class' => 'regular-text',
					'placeholder' => ''
				)
			),
			


			// site general
			'site_whatsapp_chat_number' => array(
				"name" =>'Whatsapp Chat Number',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_site_general",
				"args" => array(
					'label_for' => 'site_whatsapp_chat_number',
					'class' => 'regular-text',
					'placeholder' => 'Number'
				)
			),
			'site_customer_role' => array(
				"name" =>'Customer Role',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_site_general",
				"args" => array(
					'label_for' => 'site_customer_role',
					'class' => 'regular-text',
					'placeholder' => 'Role'
				)
			),
			
			'site_top_banner_text' => array(
				"name" =>'Top Banner Text',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_site_general",
				"args" => array(
					'label_for' => 'site_top_banner_text',
					'class' => 'regular-text',
					'placeholder' => 'Banner text'
				)
			),
			'site_top_banner_url' => array(
				"name" =>'Top Banner Link',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_site_general",
				"args" => array(
					'label_for' => 'site_top_banner_url',
					'class' => 'regular-text',
					'placeholder' => 'Banner link'
				)
			),

			'somfy_error_mail_to' => array(
				"name" =>'Send Shop error to:',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "emailInputField",
				"section" => "somfy_site_manager_site_general",
				"args" => array(
					'label_for' => 'somfy_error_mail_to',
					'class' => 'regular-text',
					'placeholder' => 'email'
				)
			),

			// Sync Server
			'sync_mail_to' => array(
				"name" =>'Email To',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "emailInputField",
				"section" => "somfy_site_manager_sync_server",
				"args" => array(
					'label_for' => 'sync_mail_to',
					'class' => 'regular-text',
					'placeholder' => 'Email To'
				)
			),
			'sync_mail_content' => array(
				"name" =>'Email Message',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textareaInputField",
				"section" => "somfy_site_manager_sync_server",
				"args" => array(
					'label_for' => 'sync_mail_content',
					'class' => 'regular-text',
					'placeholder' => 'Sync Server error message'
				)
			),
			// InWise API
			'inwise_api_url' => array(
				"name" =>'InWise API URL',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_inwise",
				"args" => array(
					'label_for' => 'inwise_api_url',
					'class' => 'regular-text',
					'placeholder' => 'API URL'
				)
			),
			'inwise_api_key' => array(
				"name" =>'InWise API Key',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_inwise",
				"args" => array(
					'label_for' => 'inwise_api_key',
					'class' => 'regular-text',
					'placeholder' => 'API Key'
				)
			),			
			'inwise_test_api_key' => array(
				"name" =>'InWise TEST API Key',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_inwise",
				"args" => array(
					'label_for' => 'inwise_test_api_key',
					'class' => 'regular-text',
					'placeholder' => 'Test API Key'
				)
			),			
			'inwise_sms_from_name' => array(
				"name" =>'InWise SMS - From name',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_inwise",
				"args" => array(
					'label_for' => 'inwise_sms_from_name',
					'class' => 'regular-text',
					'placeholder' => 'Test API Key'
				)
			),			
			'inwise_email_from_email' => array(
				"name" =>'InWise Email - From email',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_inwise",
				"args" => array(
					'label_for' => 'inwise_email_from_email',
					'class' => 'regular-text',
					'placeholder' => 'Test API Key'
				)
			),			
			'inwise_email_from_name' => array(
				"name" =>'InWise Email - From name',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_inwise",
				"args" => array(
					'label_for' => 'inwise_email_from_name',
					'class' => 'regular-text',
					'placeholder' => 'Test API Key'
				)
			),


			// Popups
			'add_warrany_popup__title' => array(
				"name" =>'פופאפ תוספת אחריות - כותרת',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_popups",
				"args" => array(
					'label_for' => 'add_warrany_popup__title',
					'class' => 'regular-text',
					'placeholder' => 'Title'
				)
			),
			'add_warranty_popup__content' => array(
				"name" =>'פופאפ תוספת אחריות - תוכן',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textareaInputField",
				"section" => "somfy_site_manager_popups",
				"args" => array(
					'label_for' => 'add_warranty_popup__content',
					'class' => 'regular-text',
					'placeholder' => 'Content'
				)
			),


			'stocknotification__title' => array(
				"name" =>'פופאפ התראת מלאי - כותרת',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_popups",
				"args" => array(
					'label_for' => 'stocknotification__title',
					'class' => 'regular-text',
					'placeholder' => 'Title'
				)
			),
			'stocknotification__content' => array(
				"name" =>'פופאפ התראת מלאי - תוכן',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textareaInputField",
				"section" => "somfy_site_manager_popups",
				"args" => array(
					'label_for' => 'stocknotification__content',
					'class' => 'regular-text',
					'placeholder' => 'Content'
				)
			),

			'stocknotification_maxamount__title' => array(
				"name" =>'פופאפ התראת מלאי לכמות - כותרת',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_popups",
				"args" => array(
					'label_for' => 'stocknotification_maxamount__title',
					'class' => 'regular-text',
					'placeholder' => 'Title'
				)
			),
			'stocknotification_maxamount__content' => array(
				"name" =>'פופאפ התראת מלאי לכמות - תוכן',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textareaInputField",
				"section" => "somfy_site_manager_popups",
				"args" => array(
					'label_for' => 'stocknotification_maxamount__content',
					'class' => 'regular-text',
					'placeholder' => 'Subject'
				)
			),

			'wishlist__title' => array(
				"name" =>'פופאפ הוספה למועדפים - כותרת',
				"name" =>'Wishlist - Title',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_popups",
				"args" => array(
					'label_for' => 'wishlist__title',
					'class' => 'regular-text',
					'placeholder' => 'Title'
				)
			),
			'wishlist__content' => array(
				"name" =>'פופאפ הוספה למועדפים - תוכן',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textareaInputField",
				"section" => "somfy_site_manager_popups",
				"args" => array(
					'label_for' => 'wishlist__content',
					'class' => 'regular-text',
					'placeholder' => 'Subject'
				)
			),
			
			// Emails
			'order-confirmation__subject' => array(
				"name" =>'אישור הזמנה - נושא',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_emails",
				"args" => array(
					'label_for' => 'order-confirmation__subject',
					'class' => 'regular-text',
					'placeholder' => 'Subject'
				)
			),
			'order-confirmation__template' => array(
				"name" =>'אישור הזמנה - templateid',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_emails",
				"args" => array(
					'label_for' => 'order-confirmation__template',
					'class' => 'regular-text',
					'placeholder' => 'Content'
				)
			),

			
			'balance_payment_request__to' => array(
				"name" =>'בקשה לתשלום חוב - נמען',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "emailInputField",
				"section" => "somfy_site_manager_emails",
				"args" => array(
					'label_for' => 'balance_payment_request__to',
					'class' => 'regular-text',
					'placeholder' => 'Email To'
				)
			),
			'balance_payment_request__subject' => array(
				"name" =>'בקשה לתשלום חוב - נושא',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_emails",
				"args" => array(
					'label_for' => 'balance_payment_request__subject',
					'class' => 'regular-text',
					'placeholder' => 'Subject'
				)
			),
			'balance_payment_request__template' => array(
				"name" =>'בקשה לתשלום חוב - templateid',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_emails",
				"args" => array(
					'label_for' => 'balance_payment_request__template',
					'class' => 'regular-text',
					'placeholder' => 'Content'
				)
			),
			
			'newsletter_sub__to' => array(
				"name" =>'רישום לניוזלטר - נמען',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "emailInputField",
				"section" => "somfy_site_manager_emails",
				"args" => array(
					'label_for' => 'newsletter_sub__to',
					'class' => 'regular-text',
					'placeholder' => 'Email To'
				)
			),
			'newsletter_sub__subject' => array(
				"name" =>'רישום לניוזלטר - נושא',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_emails",
				"args" => array(
					'label_for' => 'newsletter_sub__subject',
					'class' => 'regular-text',
					'placeholder' => 'Subject'
				)
			),
			'newsletter_sub__template' => array(
				"name" =>'רישום לניוזלטר - templateid',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_emails",
				"args" => array(
					'label_for' => 'newsletter_sub__template',
					'class' => 'regular-text',
					'placeholder' => 'Content'
				)
			),
			'newsletter_sub__customer_subject' => array(
				"name" =>'רישום לניוזלטר - נושא לקוח',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_emails",
				"args" => array(
					'label_for' => 'newsletter_sub__customer_subject',
					'class' => 'regular-text',
					'placeholder' => 'Subject'
				)
			),
			'newsletter_sub__customer_template' => array(
				"name" =>'רישום לניוזלטר - templateid לקוח',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_emails",
				"args" => array(
					'label_for' => 'newsletter_sub__customer_template',
					'class' => 'regular-text',
					'placeholder' => 'Content'
				)
			),
			'newsletter_sub__inwise_group' => array(
				"name" =>'רישום לניוזלטר - InWise Group',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_emails",
				"args" => array(
					'label_for' => 'newsletter_sub__inwise_group',
					'class' => 'regular-text',
					'placeholder' => 'InWise Group'
				)
			),


			'stock_notification__subject' => array(
				"name" =>'התראת מלאי - נושא',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_emails",
				"args" => array(
					'label_for' => 'stock_notification__subject',
					'class' => 'regular-text',
					'placeholder' => 'Subject'
				)
			),
			'stock_notification__template' => array(
				"name" =>'התראת מלאי - templateid',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_emails",
				"args" => array(
					'label_for' => 'stock_notification__template',
					'class' => 'regular-text',
					'placeholder' => 'Content'
				)
			),


			'registration_request_customer_email__subject' => array(
				"name" =>'בקשה לרישום לקוח - נושא (לקוח)',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_emails",
				"args" => array(
					'label_for' => 'registration_request_customer_email__subject',
					'class' => 'regular-text',
					'placeholder' => 'subject'
				)
			),
			'registration_request_customer_email__template' => array(
				"name" =>'בקשה לרישום לקוח - template (לקוח)',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_emails",
				"args" => array(
					'label_for' => 'registration_request_customer_email__template',
					'class' => 'regular-text',
					'placeholder' => 'content'
				)
			),
			'registration_request_somfy_email__to' => array(
				"name" =>'בקשה לרישום לקוח - נמען (סומפי)',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "emailInputField",
				"section" => "somfy_site_manager_emails",
				"args" => array(
					'label_for' => 'registration_request_somfy_email__to',
					'class' => 'regular-text',
					'placeholder' => 'email'
				)
			),
			'registration_request_somfy_email__subject' => array(
				"name" =>'בקשה לרישום לקוח - נושא (סומפי)',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_emails",
				"args" => array(
					'label_for' => 'registration_request_somfy_email__subject',
					'class' => 'regular-text',
					'placeholder' => 'subject'
				)
			),
			'registration_request_somfy_email__template' => array(
				"name" =>'בקשה לרישום לקוח - template (סומפי)',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_emails",
				"args" => array(
					'label_for' => 'registration_request_somfy_email__template',
					'class' => 'regular-text',
					'placeholder' => 'content'
				)
			),
			

			
			'update_account_request_customer_email__subject' => array(
				"name" =>'בקשה לעדכון פרטים - נושא (לקוח)',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_emails",
				"args" => array(
					'label_for' => 'update_account_request_customer_email__subject',
					'class' => 'regular-text',
					'placeholder' => 'subject'
				)
			),
			'update_account_request_customer_email__template' => array(
				"name" =>'בקשה לעדכון פרטים - template (לקוח)',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_emails",
				"args" => array(
					'label_for' => 'update_account_request_customer_email__template',
					'class' => 'regular-text',
					'placeholder' => 'content'
				)
			),
			
			'update_account_request_somfy_email__to' => array(
				"name" =>'בקשה לעדכון פרטים - נמען (סומפי)',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "emailInputField",
				"section" => "somfy_site_manager_emails",
				"args" => array(
					'label_for' => 'update_account_request_somfy_email__to',
					'class' => 'regular-text',
					'placeholder' => 'email'
				)
			),
			'update_account_request_somfy_email__subject' => array(
				"name" =>'בקשה לעדכון פרטים - נושא (סומפי)',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_emails",
				"args" => array(
					'label_for' => 'update_account_request_somfy_email__subject',
					'class' => 'regular-text',
					'placeholder' => 'subject'
				)
			),
			'update_account_request_somfy_email__template' => array(
				"name" =>'בקשה לעדכון פרטים - template (סומפי)',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_emails",
				"args" => array(
					'label_for' => 'update_account_request_somfy_email__template',
					'class' => 'regular-text',
					'placeholder' => 'content'
				)
			),

			'expert_signup_email__to' => array(
				"name" =>'רישום לאקספקט - נמען',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "emailInputField",
				"section" => "somfy_site_manager_emails",
				"args" => array(
					'label_for' => 'expert_signup_email__to',
					'class' => 'regular-text',
					'placeholder' => 'email'
				)
			),
			'expert_signup_email__subject' => array(
				"name" =>'רישום לאקספקט - נושא',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_emails",
				"args" => array(
					'label_for' => 'expert_signup_email__subject',
					'class' => 'regular-text',
					'placeholder' => 'subject'
				)
			),
			'expert_signup_email__template' => array(
				"name" =>'רישום לאקספקט - template',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_emails",
				"args" => array(
					'label_for' => 'expert_signup_email__template',
					'class' => 'regular-text',
					'placeholder' => 'content'
				)
			),

			'contact_form_email__subject' => array(
				"name" =>'צור קשר - נושא',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_emails",
				"args" => array(
					'label_for' => 'contact_form_email__subject',
					'class' => 'regular-text',
					'placeholder' => 'subject'
				)
			),
			'contact_form_email__template' => array(
				"name" =>'צור קשר - template',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_emails",
				"args" => array(
					'label_for' => 'contact_form_email__template',
					'class' => 'regular-text',
					'placeholder' => 'content'
				)
			),


			'new_event_somfy_email__to' => array(
				"name" =>'רישום לאירוע - נמען (סומפי) ',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "emailInputField",
				"section" => "somfy_site_manager_emails",
				"args" => array(
					'label_for' => 'new_event_somfy_email__to',
					'class' => 'regular-text',
					'placeholder' => 'email'
				)
			),
			'new_event_somfy_email__subject' => array(
				"name" =>'רישום לאירוע - נושא (סומפי)',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_emails",
				"args" => array(
					'label_for' => 'new_event_somfy_email__subject',
					'class' => 'regular-text',
					'placeholder' => 'subject'
				)
			),
			'new_event_somfy_email__template' => array(
				"name" =>'רישום לאירוע - template (סומפי)',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_emails",
				"args" => array(
					'label_for' => 'new_event_somfy_email__template',
					'class' => 'regular-text',
					'placeholder' => 'content'
				)
			),
			'new_event_customer_email__subject' => array(
				"name" =>'רישום לאירוע - נושא (לקוח)',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_emails",
				"args" => array(
					'label_for' => 'new_event_customer_email__subject',
					'class' => 'regular-text',
					'placeholder' => 'subject'
				)
			),
			'new_event_customer_email__template' => array(
				"name" =>'רישום לאירוע - template  (לקוח)',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_emails",
				"args" => array(
					'label_for' => 'new_event_customer_email__template',
					'class' => 'regular-text',
					'placeholder' => 'content'
				)
			),

			'pro_stock_notification__subject' => array(
				"name" =>'PRO - התראת מלאי - נושא',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_emails",
				"args" => array(
					'label_for' => 'pro_stock_notification__subject',
					'class' => 'regular-text',
					'placeholder' => 'Subject'
				)
			),
			'pro_stock_notification__template' => array(
				"name" =>'PRO - התראת מלאי - templateid',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "textInputField",
				"section" => "somfy_site_manager_emails",
				"args" => array(
					'label_for' => 'pro_stock_notification__template',
					'class' => 'regular-text',
					'placeholder' => 'Content'
				)
			),
			'pro_stock_notification__to' => array(
				"name" =>'PRO - התראת מלאי - נמען',
				"setting_callback" => "textInputSanitize",
				"field_callback" => "emailInputField",
				"section" => "somfy_site_manager_emails",
				"args" => array(
					'label_for' => 'pro_stock_notification__to',
					'class' => 'regular-text',
					'placeholder' => 'email'
				)
			),
		);
	}
}