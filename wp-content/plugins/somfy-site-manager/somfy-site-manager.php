<?php
/**
 * @package SomfySiteManager
 */

/* 
Plugin Name: SomfySiteManager
Author: AM Desings & Interjet
Description: Plugin to manage synchronization to Somfy NetSuite ERP. Sync Customers, Products, Orders and more. <br> Manage Products prices and visibility for EU and PRO sites
Version: 1.0
License: GNU General Public License v2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags: Somfy
Text Domain: Somfy
 
This plugin, like WordPress, is licensed under the GPL.
Use it to make something cool, have fun, and share what you've learned with others.
*/


// If this file is called firectly, abort!!!
defined( 'ABSPATH' ) or die( 'Hey, what are you doing here? You silly human!' );


// Require once the Composer Autoload
if ( file_exists( dirname( __FILE__ ) . '/vendor/autoload.php' ) ) {
	require_once dirname( __FILE__ ) . '/vendor/autoload.php';
}


/**
 * The code that runs during plugin activation
 */
function activate_somfy_site_manager_plugin() {
	Inc\Base\Activate::activate();
}

register_activation_hook( __FILE__, 'activate_somfy_site_manager_plugin' );



/**
 * The code that runs during plugin deactivation
 */
function deactivate_somfy_site_manager_plugin() {
	Inc\Base\Deactivate::deactivate();
}

register_deactivation_hook( __FILE__, 'deactivate_somfy_site_manager_plugin' );


/**
 * Initialize all the core classes of the plugin
 */
if ( class_exists( 'Inc\\Init' ) ) {
	Inc\Init::register_services();
}