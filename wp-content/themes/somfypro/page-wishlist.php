<?php /* Template Name: Somfy Wishlist */ ?>
<?php 
    if ( ! defined( 'ABSPATH' ) ) exit;
?>
<?php
    // redirect not logged user
    if(!is_user_logged_in()){
        wp_redirect ('./login');
    };
?>
<?php 
    get_header(); 

    get_template_part('templates/shop/wishlist');

    get_footer(); 
?>