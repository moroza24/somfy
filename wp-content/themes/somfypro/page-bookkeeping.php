<?php /* Template Name: Somfy Bookkeeping */ ?>
<?php 
    if ( ! defined( 'ABSPATH' ) ) exit;
?>
<?php
    // redirect not logged user
    if(!is_user_logged_in()){
        wp_redirect ('./login');
    };
?>
<?php 
    get_header(); 
    wp_reset_postdata();
    get_template_part('templates/pages/bookkeeping');

    get_footer(); 
?>
