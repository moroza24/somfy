<?php /* Template Name: Somfy Login */ ?>
<?php 
    if ( ! defined( 'ABSPATH' ) ) exit;
?>
<?php 
    get_header(); 

    get_template_part('templates/account/login');

    get_footer(); 
?>