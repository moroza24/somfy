<?php
/**
 * @package SomfyPRO
 * @version 1.0
 */


/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */

 // Require once the Composer Autoload
if ( file_exists( dirname( __FILE__ ) . '/vendor/autoload.php' ) ) {
	require_once dirname( __FILE__ ) . '/vendor/autoload.php';
}


// Setup
require get_template_directory() . '/Inc/Base/SetupPRO.php';


// Helper function
require get_template_directory() . '/Inc/Base/HelperFunctionsPRO.php';
require get_template_directory() . '/Inc/Base/UM-HelperFunctionsPRO.php';


// Enqueue Style & Scripts
require get_template_directory() . '/Inc/Base/EnqueuePRO.php';


// Register CPT
require get_template_directory() . '/Inc/Base/PostTypesPRO.php';
require get_template_directory() . '/Inc/Base/PostTypesPavel.php';


// global function
if ( ! function_exists( 'get_products_files' ) ) :
    function get_products_files($productID, $requiredData = null) {
		$images = array();
		$files = array();
		$valid_images = array('jpg','png','gif');
		$valid_files = array('pdf');
		
		$itemid = get_field('itemid', $productID, true);
		$productFilesPath = ABSPATH ."somfy_products/$itemid/";


		if(is_dir($productFilesPath)){
			foreach(scandir($productFilesPath) as $file){
				$ext = pathinfo($file, PATHINFO_EXTENSION);
				$filename = pathinfo($file, PATHINFO_FILENAME);
				
				if(in_array($ext, $valid_images)){
					array_push($images, array(
						'fullname' => $file,
						'ext' => $ext,
						'filename' => $filename,
						'url' => get_site_url(1) . "/somfy_products/$itemid/$file"
					));
				} else if(in_array($ext, $valid_files)){
					array_push($files, array(
						'fullname' => $file,
						'ext' => $ext,
						'filename' => $filename,
						'url' => get_site_url() . "/somfy_products/$itemid/$file"
					));
				}
			}
		}

		$results = array(
			'images' => $images,
			'files' => $files,
			'thumbnail' => array(
				'fullname' => 'placeholder-image.png',
				'ext' => 'png',
				'filename' => 'placeholder-image',
				'url' => get_bloginfo('template_url') .'/assets/img/placeholder-image.png'
			)
		);

		// Set thumbnail
		if(count($images) == 1){
			$results['thumbnail'] = $images[0];
		} else if(count($images) > 1){
			$found = false;
			foreach($images as $img){
				if(endsWith($img['fullname'], "_1.".$img['ext'])){
					$found = true;
					$results['thumbnail'] = $img;
					break;
				}
			}
			if($found == false){
				$results['thumbnail'] = $images[0];
			} 
		} 

		if(count($images) == 0){
			$results['images'][0] = $results['thumbnail'];
		}

		if($requiredData){
			return $results[$requiredData];
		} else {
			return $results;
		}

	}
	function endsWith( $haystack, $needle ) {
		$length = strlen( $needle );
		if( !$length ) {
			return true;
		}
		return substr( $haystack, -$length ) === $needle;
	}
endif;


function is_account( $p ) {

	
	if( $p == get_option('um_options')['core_account'] ) {
		return true;
	}
	return false;
}