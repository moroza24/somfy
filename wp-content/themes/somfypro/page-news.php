<?php
  /*
  * Template Name: News
  */

  // get page 
  $pageID = get_the_ID();
  $pageTitle = get_the_title($pageID);
  $pageThumbnail = get_the_post_thumbnail_url($pageID);
  $currentPagePermalink = get_the_permalink($pageID);

  wp_reset_postdata();

  $args = [
    'post_type' => 'costumer-updates',
    'posts_per_page' => 3,
    'order' => 'ASC'
  ];

  $qq_news = new WP_Query($args);
  $max = $qq_news->max_num_pages;


  $args_studies = [
    'post_type' => 'studies',
    'posts_per_page' => 3,
    'order' => 'ASC'
  ];

  $qq_studies = new WP_Query($args_studies);
  $max_studies = $qq_studies->max_num_pages;
  $flds_eves = get_fields();

  global $current_user;
  wp_get_current_user();
  $userEmail = $current_user->user_email;
  $userFullname = $current_user->display_name;
  $userPhone = get_field('custentity_il_mobile', $current_user->ID);

  get_header(); 
?>
<?php 
    if(!is_user_logged_in()){
        $fixStyle_header = 'top: 0; right: 0;'; 
        $fixStyle_news_updates = 'margin-top: 85px;'; 
        $hiddeForNotLogged = ' display: none; ';

    } else {
        $fixStyle_header ='';
        $fixStyle_news_updates ='';
        $hiddeForNotLogged = '';
    }
?>
    

<div class="main_content__with_header" id="news_page">
      <div class="page_header"
          style="background: linear-gradient(-90deg, rgba(29, 26, 52, .7), rgba(29, 26, 52, .7)), url('<?php echo $pageThumbnail; ?>');
              background-size: cover;
              background-position: center center;
              background-repeat: no-repeat;
              <?php echo $fixStyle_header; ?>">
          <ol class="breadcrumbs">
            <li><a href="<?php echo home_url(); ?>" alt='דף בית'>דף בית ></a></li>
            <li><a href="<?php echo $currentPagePermalink; ?>" alt='<?php echo $pageTitle; ?>'><?php echo $pageTitle; ?></a></li>
          </ol>
          <h2 class="page_title"><?php echo $pageTitle; ?></h2>
      </div>
        <div class="news_updates" style="<?php echo $fixStyle_news_updates; ?>">
          <div class="news_tabs">
            <div class="news_tab <?php if(!is_user_logged_in()){ echo '';}else{echo 'active';} ?>" data-tab='1' style="<?php echo $hiddeForNotLogged; ?>">
              <span> עדכוני לקוחות </span>
            </div>
            <div class="news_tab <?php if(is_user_logged_in()){ echo '';}else{echo 'active';} ?>" data-tab='2'>
              <span> הדרכות וימי עיון </span>
            </div>
            <div class="news_tab" data-tab='3' style="<?php echo $hiddeForNotLogged; ?>">
              <span> חומרים להדפסה </span>
            </div>
            <div class="news_tab" data-tab='4' style="<?php echo $hiddeForNotLogged; ?>">
              <span> אירועים </span>
            </div>
          </div>

          <div class="tabs-content  <?php if(!is_user_logged_in()){ echo '';}else{echo 'active';} ?>" data-maxpages='<?php echo $max; ?>' data-tab='1'  style="<?php echo $hiddeForNotLogged; ?>">
            <div class="tab-posts">
              <?php while ($qq_news->have_posts()) { $qq_news->the_post();
                  get_template_part('elements/update-post');
               } wp_reset_postdata(); ?>
            </div>
             <div class="load_more_updates" data-postype='costumer-updates' data-tab='1' style='visibility:<?php echo ($max > 1)  ? "visible" : "hidden"; ?>'>
               <a alt="טען עוד"><i class="fas fa-plus"></i></a>
             </div>
          </div>

          <div class="tabs-content  <?php if(is_user_logged_in()){ echo '';}else{echo 'active';} ?>" data-maxpages='<?php echo $max_studies; ?>' data-tab='2'>
              <div class="tab-posts">
                <?php while ($qq_studies->have_posts()) { $qq_studies->the_post();
                    get_template_part('elements/studies-post');
                 } wp_reset_postdata(); ?>
              </div>
             <div class="load_more_updates " data-postype='studies' data-tab='2' style='visibility:<?php echo ($max > 1)  ? "visible" : "hidden"; ?>'>
               <a alt="טען עוד"><i class="fas fa-plus"></i></a>
             </div>
          </div>

          <div class="tabs-content" data-tab='3'  style="<?php echo $hiddeForNotLogged; ?>">
            <div class="tab-posts">
              <div class="file_downloads">
                <?php
                if($flds_eves):
                 foreach ($flds_eves['downdload_files'] as $fld){ 
                  // print_r($fld['image']);
                   ?>
                  <div class="file_download">
                    <span class="title">
                      <?php echo $fld['title']; ?>
                    </span>
                    <div class="thumb" style='background-image: url(<?php echo $fld['image']['sizes']['large']; ?>);'></div>
                      <div class="list_of_files">
                        
                        <div class="container_header">
                          <a class="popup__overlay__close file_downloads_popup__close" alt='סגור'>
                            <i class="fas fa-times"></i>
                          </a>
                          <h2><?php echo $fld['title']; ?></h2>
                        </div>
                        <div class="container_body">
                          <div class="content files">
                            
                            <?php
                            if($fld['files']):
                            foreach($fld['files'] as $s_file ) { ?>
                              <a href="<?php echo $s_file['file']['url'] ?>" download  alt='<?php echo $s_file['file_name']; ?>'>
                              <i class="fas fa-download"></i>
                                  <?php echo $s_file['file_name']; ?>
                                  
                              </a>
                            <?php } endif; ?>
                          </div>
                        </div>
                        
                      </div>
                    <a href="#"></a>
                  </div>
                <?php } endif; ?>
              </div>
            </div>
          </div>

          <div class="tabs-content" data-tab='4'  style="<?php echo $hiddeForNotLogged; ?>">
            <div class="tab-posts">
              <div class="file_downloads gal_events_items">
                <?php
                if($flds_eves):
                foreach ($flds_eves['events_gallery'] as $flds_eve){ ?>
                  <div class="gal_files">
                    <span class="title">
                      <?php echo $flds_eve['title']; ?>
                    </span>
                    <div class="thumb" style='background-image: url(<?php echo $flds_eve['thumb']['sizes']['large']; ?>);'></div>
                      <!-- <img src="<?php echo $flds_eve['thumb']['url']; ?>" alt="" /> -->
                        <div class="content">
                          <?php if( $flds_eve['images'] ): ?>
                          <div class="gal_events_slider">
                          <?php foreach($flds_eve['images'] as $s_file_eve ) { ?>
                            <div>
                              <img src="<?php echo $s_file_eve['image']['sizes']['large']; ?>" alt="">
                            </div>
                          <?php } endif; ?>
                          </div>
                        </div>
                        <a href="#"></a>
                  </div>
                <?php }
              endif;?>
              </div>
            </div>
          </div>

      </div>
    </div>



  <div class="popup__overlay files_download_popup" style="display: none;">
    <div class="popup__container">
      
      <div class="container_body">
      </div>
    </div>
  </div>


  <div class="popup__overlay gal_events" style="display: none;">
    <a class="popup__overlay__close gal_events_popup__close" alt='סגור'>
        <i class="fas fa-times"></i>
    </a>
    <div class="inner">

    </div>
  </div>

  <div id="studies_popup" class="popup__overlay studies_popup" style="display: none;">
      <div class="popup__container">
          
          <div class="container_header">
            <a class="popup__overlay__close studies_popup__close" alt='סגור'>
                <i class="fas fa-times"></i>
            </a>
              <h2> הרשמה להדרכה </h2>
          </div>
          <div class="container_body">
              <h4 class="content_title">
                <span class="studie__title">
                    <span id="studie__subject"></span>, 
                    תאריך הרשמה הוא <span id="studie__date"></span>
                </span>
                <span class='form_istructions'>
                    אנא מלא את כל השדות בכדי להזמין סדנא
                </span>
               </h4>
              <form id="rgistration_request__form" class='studies_signup' _lpchecked="1">
                  <div class="mb-3 form-input__group">
                      <input type="text" class="form-control form-control-lg" id="rgistration_request__contact_name" 
                        value="<?php echo $userFullname ; ?>" placeholder="איש קשר" minlength="2" required>
                      <p class="form_error" data-input="rgistration_request__contact_name"> יש להזין שם של איש קשר </p>
                  </div>
                  <div class="mb-3 form-input__group">
                      <input type="email" class="form-control form-control-lg" id="rgistration_request__email" 
                        value="<?php echo $userEmail ; ?>" placeholder="אימייל" required>
                      <p class="form_error" data-input="rgistration_request__email">יש להזין אימייל</p>
                  </div>
                  <div class="mb-3 form-input__group">
                      <input type="tel" class="form-control form-control-lg" id="rgistration_request__phone" 
                        value="<?php echo $userPhone ; ?>" placeholder="טלפון" minlength="8" maxlength="10" required
                        onkeydown="if(event.key==='.'){event.preventDefault();}"  oninput="event.target.value = event.target.value.replace(/[^0-9]*/g,'');">
                      <p class="form_error" data-input="rgistration_request__phone">יש להזין מספר טלפון</p>
                  </div>
                  <div class="mb-3 form-input__group">
                    <select class="form-select form-select-lg" aria-label="כמות משתתפים" id="rgistration_request__participants">
                      <option value="" selected=""> כמות משתתפים </option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="more">יותר</option>
                    </select>
                      <p class="form_error" data-input="rgistration_request__participants">יש לבחור כמות משתתפים</p>
                  </div>
                  <input type="hidden" value='' id='hidden_post'>

                  <div class="mb-3 form-input__group">
                    <input type="checkbox" name="other_delivery_address" id="accept_terms" required>
                    <label for="accept_terms"><span>
                        קראתי את
                        <a target="_blank" href="<?php echo home_url(); ?>/תנאי-שימוש-ורכישה/" alt='תנאי ההשתתפות'>
                          תנאי ההשתתפות
                        </a>
                        ואני מסכים איתם
                       </span>
                    </label>
                    <p class="form_error" data-input="accept_terms">יש לאשר תנאי השתתפות</p>
                  </div>
              
                  <input class="sf_btn" type="submit" id="submit_rgistration_request" value="הרשם">
                  <div class="loader" id="rgistration_request__form_loader"><div class="spinner-loader"></div></div>
                  <p id="rgistration_request__form_submit_success">הודעה נשלחה בהצלחה!</p>
                  <p id="rgistration_request__form_submit_error"></p>
              </form>
          </div>
      </div>
  </div>

<?php get_footer(); ?>
