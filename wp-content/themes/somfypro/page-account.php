<?php /* Template Name: Somfy Account */ ?>
<?php 
    if ( ! defined( 'ABSPATH' ) ) exit;
?>
<?php 
    get_header(); 

    get_template_part('templates/account/account');

    get_footer(); 
?>