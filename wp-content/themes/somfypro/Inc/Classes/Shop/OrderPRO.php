<?php
/**
 * @package SomfyPRO
 */

namespace Inc\Classes\Shop;

use Inc\Classes\Shop\ProductsPRO;
use Inc\Classes\Shop\NetSuiteApiPRO;
use Inc\Classes\Shop\CustomerPRO;
use Inc\Classes\Shop\BalancePRO;
use Inc\Classes\Shop\CalculatorPRO;
use Inc\Classes\SomfyMessaging;

class OrderPRO {
    
    public $dbOrderItem;
    public $dbPosts;
    public $dbPostmeta;
    public $NetSuiteApiPRO;

    function __construct(){
        global $wpdb;
        $this->dbOrderItem = PRO_SITE_PREFIX . "orderitem";
        $this->dbPosts = PRO_SITE_PREFIX . "posts";
        $this->dbPostmeta = PRO_SITE_PREFIX . "postmeta";

        $this->NetSuiteApiPRO = new NetSuiteApiPRO();

        $this->orderDefaults = array(
            'orderstatus' => 'A',
            'currency' => '1',
            'currencyPrim' => 'ILS'
        );
    }

    /******     AJAX     ******/

    /** 
     * Description: syncUserOrders
     * @param internalid
     * @return
     */
    public function syncUserOrders($request){
        if ( !wp_verify_nonce( $_REQUEST['nonce'], NONCE_SECRET)) {
            exit("No naughty business please");
         }  

        // validate logged user
        $userID = get_current_user_id();

        if(!$userID){
            $result['status'] = false;
            $result['user_logged'] = false;
            $result['message'] = 'משתמש לא מחובר';
            echo json_encode($result);
            die();
        }

        if( user_can( $userID, 'editor') || user_can( $userID, 'administrator') ) {  
            // Stuff here for administrators or editors
            $result['status'] = true;
            echo json_encode($result);
            die();
        }

        $user_internalid = isset(get_user_meta($userID, 'internalid')[0]) ? get_user_meta($userID, 'internalid')[0] : '';
        
        if($user_internalid == ""){
            if(!$userID){
                $result['status'] = false;
                $result['user_logged'] = true;
                $result['message'] = 'משתמש לא מסונכרן';
                echo json_encode($result);
                die();
            }
        }

        // get order from ERP
        $this->NetSuiteApiPRO->somfylog('start - order sync: ' . $user_internalid, array($userID, $user_internalid));
        $BalancePRO = new BalancePRO();

        // sync orders
        $userOrdersERP = $this->NetSuiteApiPRO->getOrdersByEntityid($user_internalid);

        // sync balance data
        $userBalanceSync = $BalancePRO->syncCustomerBalance($user_internalid, true);
        $result['userBalanceSync'] = $userBalanceSync;

        if($userOrdersERP['status']){
            if(count($userOrdersERP['data']) > 0 && is_array($userOrdersERP['data'])){
                // loop results and update orders and order items
                $ordersSynced = $this->updateOrdersFromERP($userOrdersERP['data'], $user_internalid);

                if($ordersSynced['status']){
                    // all updated
                    $this->NetSuiteApiPRO->somfylog('end - order sync: ' . $user_internalid, array($userID, $user_internalid));

                    $result['status'] = true;
                    $result['userOrdersERP'] = $userOrdersERP;
                    $result['ordersSynced'] = $ordersSynced;
                    echo json_encode($result);
                    die();
                } else {
                    $this->NetSuiteApiPRO->somfylog('error - order sync: ' . $user_internalid, array($userID, $user_internalid, $userOrdersERP, $ordersSynced));
                    $result['status'] = false;
                    $result['message'] = 'לא ניתן לעדכן את ההזמנה. ' . $ordersSynced['message'];
                    $result['userOrdersERP'] = $userOrdersERP;
                    $result['ordersSynced'] = $ordersSynced;
                    echo json_encode($result);
                    die();
                }
            } else {
                $this->NetSuiteApiPRO->somfylog('end - order sync: ' . $user_internalid, array($userID, $user_internalid));
                $result['status'] = true;
                $result['message'] = 'לא נמצאו הזמנות';
                $result['userOrders'] = array();
                echo json_encode($result);
                die();
            }
        } else {
            $this->NetSuiteApiPRO->somfylog('error - order sync: ' . $user_internalid, array($userID, $user_internalid, $userOrdersERP));
            $result['status'] = false;
            $result['message'] = 'לא ניתן לשלוף הזמנות מהמערכת';
            $result['error'] = $userOrdersERP;
            echo json_encode($result);
            die();
        }
        
    }



    /** 
     * Description: getOrdersByUser
     * @param internalid
     * @return
     */
    public function getUserOrders($request){
        if ( !wp_verify_nonce( $_REQUEST['nonce'], NONCE_SECRET)) {
            exit("No naughty business please");
         }  

        //  validate params
        $user_internalid = isset($_REQUEST['user_internalid']) ? $_REQUEST['user_internalid'] : null;
        if(!isset($user_internalid) || $user_internalid == ''){
            $result['status'] = false;
            $result['message'] = 'חלק מהנתונים חסרים';
            $result['user_internalid'] = $user_internalid;
            echo json_encode($result);
            die();
        }

        // validate logged user
        $userID = get_current_user_id();

        if(!$userID){
            $result['status'] = false;
            $result['user_logged'] = false;
            $result['message'] = 'משתמש לא מחובר';
            echo json_encode($result);
            die();
        }

        $userOrders = $this->getLocalUserOrders($user_internalid);
        $result['userOrders'] = $userOrders;

        if(count($userOrders) > 0){
            $result['status'] = true;
        } else {
            $result['status'] = false;
            $result['message'] = 'לא נמצאו הזמנות';
        }
        echo json_encode($result);
        die();
    }



    /** 
     * Description: getOrderDetails
     * @param internalid
     * @return
     */
    public function getOrderDetails($request){
        if ( !wp_verify_nonce( $_REQUEST['nonce'], NONCE_SECRET)) {
            exit("No naughty business please");
         }  

        //  validate params
        $order_tranid = isset($_REQUEST['order_tranid']) ? $_REQUEST['order_tranid'] : null;
        if(!isset($order_tranid)){
            $result['status'] = false;
            $result['message'] = 'חלק מהנתונים חסרים';
            echo json_encode($result);
            die();
        }

        // validate logged user
        $userID = get_current_user_id();

        if(!$userID){
            $result['status'] = false;
            $result['message'] = 'משתמש לא מחובר';
            echo json_encode($result);
            die();
        }

        // get items
        $orderItems = $this->getOrderItemsData($order_tranid);
        $orderInfo = $this->getOrderInfo($order_tranid);

        $result['status'] = true;
        $result['items'] = $orderItems;
        $result['orderInfo'] = $orderInfo;
        echo json_encode($result);
        die();
    }


    /** 
     * Description: newOrder
     * @param
     * @return
     */
    public function addNewOrder($request){
        if ( !wp_verify_nonce( $_REQUEST['nonce'], NONCE_SECRET)) {
            exit("No naughty business please");
        }  

        //  validate params
        $order_args = isset($_REQUEST['newOrder']) ? $_REQUEST['newOrder'] : null;
        $calcItems = isset($_REQUEST['calcItems']) ? $_REQUEST['calcItems'] : null;
        
        $orderItems = isset($order_args['orderItems']) ? $order_args['orderItems'] : null;
        $user_usage_agreement = isset($order_args['user_usage_agreement']) ? $order_args['user_usage_agreement'] : null;
        
        if(!$order_args || !$orderItems
            || (!$user_usage_agreement || $user_usage_agreement == 'false')){
                $result['status'] = false;
                $result['order_args'] = $order_args;
                $result['message'] = 'חלק מהנתונים חסרים';
                echo json_encode($result);
                die();
        }

        // validate logged user
        global $current_user;
        wp_get_current_user();
        $userID = get_current_user_id();

        if(!$userID){
            $result['status'] = false;
            $result['message'] = 'משתמש לא מחובר';
            echo json_encode($result);
            die();
        }

        // check calcItems
        $haveCalcItems = false;
        if(is_array($calcItems) && count($calcItems) > 0){
            $haveCalcItems = true;
        }

        
        // get user data 
        $entity = get_user_meta($userID, 'internalid')[0];
        $CustomerObj = new CustomerPRO();
        $userData = array(
            'ID' => $userID,
            'user_email' => $current_user->user_email,
            'entity' => $entity,
            'display_name' => $current_user->display_name,
            'salesrep' => isset(get_user_meta($userID, 'salesrep')[0]) ? get_user_meta($userID, 'salesrep')[0] : '',
            'customertype' => isset(get_user_meta($userID, 'custentity_customertype')[0]) ? get_user_meta($userID, 'custentity_customertype')[0] : '',
            'first_name' => isset(get_user_meta($userID, 'first_name')[0]) ? get_user_meta($userID, 'first_name')[0] : '',
            'last_name' => isset(get_user_meta($userID, 'last_name')[0]) ? get_user_meta($userID, 'last_name')[0] : ''
        );


        // create local order
        $orderCleared = $this->clearOlderOrders($entity);
        $orderCreated = $this->newOrderFromSite($order_args, $userData);

        
        if($orderCreated['status']){
            $result['status'] = true;

            // save calc inputs
            if($haveCalcItems){
                $CalculatorPro = new CalculatorPRO();
                $calcInputsSaved = $CalculatorPro->saveCalcInputs($orderCreated['newOrderID'], $userID, $calcItems);
            }

            // Send the order to ERP
            $ERPOrderCreated = $this->NetSuiteApiPRO->newOrder(
                $orderCreated, 
                $order_args,
                $userData,
                $this->orderDefaults);

            if($ERPOrderCreated['status']){
                // sync the new order
                $newOrderData = $this->NetSuiteApiPRO->getOrderById($ERPOrderCreated['createdRecordid']);

                update_field('is_synced', 1, $orderCreated['newOrderID']);

                if($newOrderData['status']){
                    $ordersSynced = $this->updateOrdersFromERP($newOrderData['data'], $entity);
                }

                // send order confirmation to customer
                $confirmationSent = $this->sendOrderConfirmationEmail($orderCreated['newOrderID'], $calcItems);

                // check if stock notification required
                $notificationSent= '';
                if(count($orderCreated['orderTotals']['stock_notifictaions']) > 0){
                    // send notification
                    $notificationSent = $this->send_stock_notification($userData, $newOrderData['data'][0], $orderCreated['orderTotals']['stock_notifictaions']);
                }

                $result['status'] = true; 
                $result['userData'] = $userData;
                $result['items'] = $orderCreated['orderTotals']['stock_notifictaions'];
                $result['newOrderData'] = $newOrderData;
                $result['ordersSynced'] = $ordersSynced;
                $result['notificationSent'] = $notificationSent;
            } else {
                wp_trash_post($orderCreated['newOrderID']);
                wp_delete_post($orderCreated['newOrderID']);

                $result['status'] = false;
                $result['ERPOrderCreated'] = $ERPOrderCreated;
                $result['message'] = 'לא ניתן לסנכרן הזמנה.';
            }
        } else {
            $result['status'] = false;
            $result['orderCreated'] = $orderCreated;
            $result['message'] = 'לא ניתן לייצר הזמנה חדשה. ' .$orderCreated['message'];
        }

        $result['order_args'] = $order_args;
        $result['orderCreated'] = $orderCreated;
        $result['userID'] = $userID;
        $result['entity'] = $entity;
        echo json_encode($result);
        die();

    }


    /** 
     * Description: sendOrderConfirmationEmail
     * @param
     * @return
     */
    public function sendOrderConfirmationEmail($order_id, $calcItems = null){
        date_default_timezone_set('Israel');
        
        $CustomerObj = new CustomerPRO();
        $SomfyMessaging = new SomfyMessaging();

        // get order info
        $orderData = get_fields($order_id, true);
        // get customer data
        $entity = $orderData['entity'];
        $tranid = $orderData['tranid'];

        $customer = $CustomerObj->getUserByInternalid($entity);
        $userID = $customer->ID;
        $user_email = $customer->data->user_email;

        $customerData = array(
            'ID' => $userID,
            'user_email' => $user_email,
            'first_name' => isset(get_user_meta($userID, 'first_name')[0]) ? get_user_meta($userID, 'first_name')[0] : '',
            'last_name' => isset(get_user_meta($userID, 'last_name')[0]) ? get_user_meta($userID, 'last_name')[0] : '',
            'phone' => isset(get_user_meta($userID, 'phone')[0]) ? get_user_meta($userID, 'phone')[0] : '',
            'custentity_il_mobile' => isset(get_user_meta($userID, 'custentity_il_mobile')[0]) ? get_user_meta($userID, 'custentity_il_mobile')[0] : '',
            'companyname' => isset(get_user_meta($userID, 'companyname')[0]) ? get_user_meta($userID, 'companyname')[0] : '',
        );
        
        // get items
        $itemsData = $this->getOrderItemsData($tranid);

        // get html template and email subject
        $emailSubjectTemplate = get_option('order-confirmation__subject');
        $template = get_option('order-confirmation__template');
        $emailContentTemplate = $SomfyMessaging->getTemplate($template);

        // create items table html
        $items_table = $this->getItemsTableHtml($orderData, $itemsData);

        // prep calc inputs html if exists
        $calcHtml = '';
        if($calcItems && count($calcItems) > 0){
            $calcHtml = $this->getCalcInputsHtml($calcItems);
        }

        // replace placeholder with data
        $emailContentData = array(
            'order_tranid' => $tranid,
            'order_date' => date('d/m/Y H:i'),
            'items_table' => $items_table,
            'calcHtml' => $calcHtml,
            'first_name' => $customerData['first_name'],
            'last_name' => $customerData['last_name'],
            'customer_email' => $customerData['user_email'],
            'phone' => $customerData['phone'] != '' ? $customerData['phone'] : $customerData['custentity_il_mobile'],
            'billaddressee' => $orderData['billaddressee'],
            'billaddress1' => $orderData['billaddress1'],
            'billcity' => $orderData['billcity'],
            'billcountry' => $orderData['billcountry']['label']
        );

        foreach($emailContentData as $key => $value){
            $emailContentTemplate = str_replace("{{".$key."}}", $value, $emailContentTemplate);
        }

        $emailSubjectTemplate = str_replace("{{order_tranid}}", $tranid, $emailSubjectTemplate);
        
        $sendTo = $customerData['user_email'];
        $subject = $emailSubjectTemplate;
        $htmlBody = $emailContentTemplate;

        // send to customer
        $messageSent = $SomfyMessaging->sendMail($sendTo, $subject, $htmlBody);


        // somfy log
        $this->NetSuiteApiPRO->somfylog('sendOrderConfirmationEmail', array(
            'order_id' => $order_id,
            'customerData' => $customerData
        ));

        return $messageSent;
    }



    /****** order functions ******/

    /** 
     * Description: newOrderFromSite
     * @param
     * @return
     */
    public function newOrderFromSite($order, $userData){
        
        // validate order total and items
        $orderTotals = $this->getOrderSummeryAndStock($order['orderItems'], $userData['ID'], $userData['entity']);
        if($orderTotals['status']){
            // validate order summery
            if(intval($orderTotals['subtotal']) != intval($order['order_subSummary'])
                || intval($orderTotals['total']) != intval($order['order_totalPrice'])){

                // error
                return array('status' => false, 'message' => 'חוסר התאמה במחירים', 'orderTotals' => $orderTotals);
            }
        } else {
            // error
            return array('status' => false, 'message' => $orderTotals['message']);
        }


        
        /**
         * 
         *  All Good continue 
         * 
         * **/
        
        // create new order post 
        $newOrderID = wp_insert_post(array(
            'post_type' => 'somfy_order'
        ));

        if($newOrderID){
            $externalid = 'PROSO#' . date("Y") . $newOrderID;
            $titleUpdateArr = array(
                'ID'           => $newOrderID,
                'post_title'   => $externalid,
            );
           
            // Update the post_title
            wp_update_post( $titleUpdateArr );
            update_field('externalid', $externalid, $newOrderID);
            update_field('is_synced', 0, $newOrderID);
            update_field('salesrep', $userData['salesrep'], $newOrderID);
            update_field('custentity_customertype', $userData['customertype'], $newOrderID);


            // update order ACF - shipping data
            update_field('shipmethod', $order['shipmethod'], $newOrderID);
            
            if($order['shipmethod'] == 'delivery'){
                $isPickup = 'F';
            } else {
                $isPickup = 'T';
            }
            update_field('custbody_pickup',$isPickup, $newOrderID);
            

            // setting payment method
            $terms = '4';
            if($order['paymentmethod'] == 'credit'){
                update_field('paymentmethod', '10', $newOrderID);
                update_field('terms', $terms, $newOrderID);
            } else {
                update_field('paymentmethod', '1', $newOrderID);
                update_field('terms', $order['paymentmethod'], $newOrderID);
                $terms = $order['paymentmethod'];
            }

            // update order ACF - billing data
            update_field('billaddressee', get_field('billaddressee',  'user_'.$userData['ID']), $newOrderID);
            update_field('billaddress1', get_field('billaddress1',  'user_'.$userData['ID']), $newOrderID);
            update_field('billcity', get_field('billcity',  'user_'.$userData['ID']), $newOrderID);
            update_field('billcountry', get_field('billcountry',  'user_'.$userData['ID'])['value'], $newOrderID);

            
            // update customer data
            update_field('entity', $userData['entity'], $newOrderID);
            update_field('customer_email', $userData['user_email'], $newOrderID);

            foreach($this->orderDefaults as $key => $value){
                update_field($key , $value, $newOrderID);
            }


            update_field('subtotal', $order['order_subSummary'], $newOrderID);
            update_field('discountamount', $order['order_totalDiscount'], $newOrderID);
            update_field('taxtotal', 0, $newOrderID);
            update_field('total', $order['order_totalPrice'], $newOrderID);
        
            // create order items
            $itemsCreated = $this->addItemsToOrder($newOrderID, $orderTotals['products']);

            if(!$itemsCreated){
                wp_delete_post($newOrderID);
                return array(
                    'status' => false, 
                    'message' => 'שגיאה ביצירת שורות הזמנה',
                    'itemsCreated' => $itemsCreated,
                    'orderTotals' => $orderTotals
                );
            }

            // Finally we are done!
            return array(
                'status' => true, 
                'newOrderID' => $newOrderID,
                'orderTotals' => $orderTotals,
                'itemsCreated' => $itemsCreated,
                'terms' => $terms
            );
        } else {
            return array(
                'status' => false, 
                'message' => 'לא ניתן לייצר הזמנה');
        }
    }

    

    /** 
     * Description: updateOrderFromERP for entity
     * @param orders:array
     * @param internalid:string
     * @return
     */
    public function updateOrdersFromERP($orders, $user_internalid){
        $errors = false;
       
        // for loop to go throw the orders as $key => $value
        foreach ($orders as $order){
            $internalid = $order['internalid'];
           

            // check by internalid if order already exists
            $existingOrderID = $this->isOrderExists($internalid);

            if($existingOrderID){
                /** if exists:
                * update all ACF by $key => $value
                */
                $updatedOrder = $this->updateFromERP($order, $existingOrderID, $user_internalid);
                
                if(!$updatedOrder['status']){
                    return array(
                        'status' => false, 
                        'message' => 'somthing went wrong in creating the order from ERP ' . $updatedOrder['message']
                    );
                }

                // get and update order delivery data
                $this->syncOrderDeliveryData($internalid, $existingOrderID);

                /* update order items
                    * get array of the current order items ids
                    * create order items (new ones)
                        * if TRUE - remove previuse order items
                        * if FALSE - ERROR
                */
            } else {
                /** if not: 
                * create order post
                * update all ACF by $key => $value
                * create order items
                */ 
                $createdOrder = $this->newFromERP($order, $user_internalid);
                
                if(!$createdOrder['status']){
                    return array(
                        'status' => false, 
                        'message' => 'somthing went wrong in creating the order from ERP ' . $createdOrder['message']
                    );
                }

                // get and update order delivery data
                $this->syncOrderDeliveryData($internalid, $createdOrder['newOrderID']);
            }

            
        }
        return array(
            'status' => true,
            'message' => 'All Synced!'
        );       
    }


    /** 
     * Description: newFromERP order
     * @param
     * @return
     */
    public function newFromERP($order, $entity){
        // create new order post 
        $newOrderID = wp_insert_post(array(
            'post_type' => 'somfy_order'
        ));

        if($newOrderID){
            $titleUpdateArr = array(
                'ID'           => $newOrderID,
                'post_title'   => $order['tranid'],
            );
           
            // Update the order post_title
            wp_update_post( $titleUpdateArr );


            // Update ACF
            $fieldsToExclude = array('item', 'currencyPrim', 'discountamount', 'isSuccess', 'taxcode'); 
            foreach($order as $key => $value){
                if(in_array($key, $fieldsToExclude)) continue;

                update_field($key, $value, $newOrderID);
            }
            

            // update customer data
            $CustomerObj = new CustomerPRO();
            $userData = $CustomerObj->getUserByInternalid($entity);
            $userEmail = $userData->data->user_email;
            update_field('customer_email', $userEmail, $newOrderID);
        

            // create order items
            $itemsCreated = $this->addOrderItemsFromERP($newOrderID,  $order['item']);

            if(!$itemsCreated){
                wp_delete_post($newOrderID);
                return array(
                    'status' => false, 
                    'message' => 'Could not create order items. ' . $itemsCreated['message'],
                    'itemsCreated' => $itemsCreated
                );
            }

            update_field('is_synced', 1, $newOrderID);

            // Finally we are done!
            return array(
                'status' => true, 
                'newOrderID' => $newOrderID,
                'itemsCreated' => $itemsCreated
            );
        } else {
            return array('status' => false, 'message' => 'Cannot create new order');
        }
    }


    /** 
     * Description: updateFromERP order
     * @param
     * @return
     */
    public function updateFromERP($order, $existingOrderID, $user_internalid){
       
        $titleUpdateArr = array(
            'ID'           => $existingOrderID,
            'post_title'   => $order['tranid'],
        );
        
        // Update the order post_title
        wp_update_post( $titleUpdateArr );


        // Update ACF
        $fieldsToExclude = array('item', 'currencyPrim', 'discountamount', 'isSuccess', 'taxcode'); 
        foreach($order as $key => $value){
            if(in_array($key, $fieldsToExclude)) continue;

            update_field($key, $value, $existingOrderID);
        }
        

        // update customer data
        $CustomerObj = new CustomerPRO();
        $userData = $CustomerObj->getUserByInternalid($user_internalid);
        

        $userEmail = $userData->user_email;
        update_field('customer_email', $userEmail, $existingOrderID);
    

        // create order items
        // get current order items
        $currentOrderItems = $this->getOrderItemsArray($existingOrderID, null);
        $itemsCreated = $this->addOrderItemsFromERP($existingOrderID,  $order['item']);

        if(!$itemsCreated){
            wp_delete_post($existingOrderID);
            return array(
                'status' => false, 
                'message' => 'Could not create order items. ' . $itemsCreated['message'],
                'itemsCreated' => $itemsCreated
            );
        } else {
            // delete current order items
            $this->deleteOldOrderItems($currentOrderItems);
        }

        update_field('is_synced', 1, $existingOrderID);

        // Finally we are done!
        return array(
            'status' => true, 
            'existingOrderID' => $existingOrderID,
            'itemsCreated' => $itemsCreated
        );
        
    }

    /** 
     * Description: syncOrderDeliveryData
     * @param
     * @return
     */
    public function syncOrderDeliveryData($internalid, $orderID){
        $netsuit_orderDeliveryData = $this->NetSuiteApiPRO->getOrderDeliveryData($internalid);

        if($netsuit_orderDeliveryData['status'] && is_array($netsuit_orderDeliveryData['data'])){
            // update the data locally
            $this->saveOrderDeliveryData($internalid, $orderID, $netsuit_orderDeliveryData['data']);
        }
    }


    /** 
     * Description: saveOrderDeliveryData
     * @param
     * @return
     */
    public function saveOrderDeliveryData($internalid, $orderID, $deliveryData){
        global $wpdb;
        $orderdelivery_db = PRO_SITE_PREFIX . "orderdelivery";

        

        $query = "INSERT INTO $orderdelivery_db
            (
                `name`,
                `order_internalid`,
                `orderid`,
                `custrecord_fc_dn_related_parent`,
                `custrecord_fc_dn_related_if`,
                `custrecord_fc_dn_status`,
                `custrecord_fc_packed_date`,
                `custrecord_fc_shipped_date`
            ) VALUES ";

        for($i = 0; $i < count($deliveryData); $i++){
            $row = $deliveryData[$i];
            
            $packedDate = '';
            if($row['custrecord_fc_packed_date'] != ''){
                $pd = $date = \DateTime::createFromFormat('j/n/Y h:i:s A', $row['custrecord_fc_packed_date']);
                $packedDate = $pd->format("Y-m-d H:i:s");
            }

            $shippedDate = '';
            if($row['custrecord_fc_shipped_date'] != ''){
                $sd = $date = \DateTime::createFromFormat('j/n/Y h:i:s A', $row['custrecord_fc_shipped_date']);
                $shippedDate = $sd->format("Y-m-d H:i:s");
            }

            $query .= " (
                '".$row['name']."',
                '".$internalid."',
                '".$orderID."',
                '".$row['custrecord_fc_dn_related_parent']."',
                '".$row['custrecord_fc_dn_related_if']."',
                '".$row['custrecord_fc_dn_status']."',
                '".$packedDate."',
                '".$shippedDate."'
            ) ";

            if($i < count($deliveryData) - 1) $query .= " ,";
        }
        $query .= " ON DUPLICATE KEY UPDATE 
            orderid=VALUES(orderid),
            custrecord_fc_dn_related_parent=VALUES(custrecord_fc_dn_related_parent),
            custrecord_fc_dn_related_if=VALUES(custrecord_fc_dn_related_if),
            custrecord_fc_dn_status=VALUES(custrecord_fc_dn_status),
            custrecord_fc_packed_date=VALUES(custrecord_fc_packed_date),
            custrecord_fc_shipped_date=VALUES(custrecord_fc_shipped_date); ";

        return $wpdb->query($query);
    }

    /** 
     * Description: getLocalUserOrders
     * @param
     * @return
     */
    public function getLocalUserOrders($user_internalid){
        // get post types
        $orders = new \WP_Query(array(
            'post_type' => 'somfy_order',
            'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit'),  
            'posts_per_page'   => -1,
            'meta_query' => array(
                array(
                    'key' => 'entity',
                    'compare' => '=',
                    'value' => $user_internalid
                )
            )
        ));

        $results = array();
        
        if($orders){
            while ($orders->have_posts()) {
                $orders->the_post();
                $orderID =  get_the_ID();
                array_push($results, array(
                    'ID' => $orderID,
                    'tranid' => get_field('tranid', $orderID),
                    'saleseffectivedate' => get_field('saleseffectivedate', $orderID),
                    'status' => get_field('status', $orderID),
                    'total' => get_field('total', $orderID),
                    'pickup' => get_field('custbody_pickup', $orderID),
                    'delivery_Data' => $this->getOrderDeliveryData($orderID)
                ));
            }
        }

        return $results;
    }

    /** 
     * Description: getOrderDeliveryData
     * @param
     * @return
     */
    public function getOrderDeliveryData($orderID){
        global $wpdb;

        $orderdelivery_db = PRO_SITE_PREFIX . "orderdelivery";

        $query = "SELECT * 
            FROM $orderdelivery_db
            WHERE orderid='$orderID'";

        return $wpdb->get_results($query);
    }

    /** 
     * Description: getOrderItemsArray
     * @param
     * @return
     */
    public function getOrderItemsArray($orderid, $tranid){
        global $wpdb;

        $whereQuery = 'WHERE 1=1';

        if($tranid) $whereQuery .= " AND tranid = '$tranid'";
        if($orderid) $whereQuery .= " AND orderid = '$orderid'";

        $query = "SELECT ID 
            FROM $this->dbOrderItem
            $whereQuery";

        $results = $wpdb->get_results($query);
        $finaleResults = array();

        if(count($results) >0){
            for($i = 0; $i < count($results); $i++){
                array_push($finaleResults, $results[$i]->ID);
            }
        }

        return $finaleResults;
    }

    /** 
     * Description: getOrderItemsData
     * @param
     * @return
     */
    public function getOrderItemsData($tranid){
        global $wpdb;

        $query = "SELECT 
                oi.*, 
                pd.ID AS productID, 
                pd.post_title, 
                pd.internalid
            FROM $this->dbOrderItem oi
            LEFT JOIN (
                SELECT p.ID, p.post_title, pm.meta_value AS internalid
                FROM $this->dbPosts p
                JOIN $this->dbPostmeta pm
                    ON pm.meta_key = 'internalid' AND pm.post_id = p.ID
                WHERE p.post_type = 'product'
            ) pd 
                ON pd.internalid = oi.item
            WHERE oi.tranid = '$tranid'";

        $results = $wpdb->get_results($query);
        
        if(count($results) > 0){
            for($i = 0; $i < count($results); $i++){
                $pid = $results[$i]->productID;
                $results[$i]->itemid = get_field('itemid', $pid);
                $results[$i]->permalink = get_the_permalink($pid);
            }
        }
        return $results;
    }
    

    /** 
     * Description: getOrderInfo
     * @param
     * @return
     */
    public function getOrderInfo($tranid){
        // get post types
        $orderInfo = new \WP_Query(array(
            'post_type' => 'somfy_order',
            'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit'),  
            'posts_per_page'   => 1,
            'meta_query' => array(
                array(
                    'key' => 'tranid',
                    'compare' => '=',
                    'value' => $tranid
                )
            )
        ));

        if(count($orderInfo->posts) > 0){
            $result = $orderInfo->posts[0];
            $result->acf = get_fields($result->ID, true);

            // get invoices
            $BalancePRO = new BalancePRO();
            $result->inv = $BalancePRO->getInvByOrder($result->ID);
            $result->delivery_Data = $this->getOrderDeliveryData($result->ID);

            return $result;
        } else {
            return false;
        }
    }



    /** 
     * Description: deleteOldOrderItems
     * @param
     * @return
     */
    public function deleteOldOrderItems($orderItems){
        global $wpdb;

        $values = "('".implode("','",$orderItems)."')";


        $sql = "DELETE FROM $this->dbOrderItem
            WHERE ID IN $values";

        return $wpdb->query($sql);
    }



    /** 
     * Description: isOrderExists
     * @param
     * @return
     */
    public function isOrderExists($internalid){
        $userOrder = new \WP_Query(array(
            'post_type' => 'somfy_order',
            'posts_per_page'   => 1,
            'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash'),  
            'meta_query' => array(
                array(
                    'key' => 'internalid',
                    'compare' => '=',
                    'value' => $internalid
                )
            )
        ));
        return $userOrder->have_posts() ? $userOrder->post->ID : false;
    }

    
    /** 
     * Description: addItemsToOrder
     * @param
     * @return
     */
    public function addItemsToOrder($newOrderID, $products){
        global $wpdb;
        $createDate = current_time( "Y-m-d H:i:s", 0 );
        $rate = get_option('shop_rate');

        $query = "INSERT INTO $this->dbOrderItem
			(
                `ID`,
                `internalid`,
                `tranid`,
                `orderid`,
                `lineuniquekey`,
                `item`,
                `quantity`,
                `quantityshiprecv`,
                `quantitypicked`,
                `quantityfulfilled`,
                `quantitybilled`,
                `pricelevel`,
                `custcol_originalitemrate`,
                `rate`,
                `custcol_discountcode`,
                `custcol_discountpercentage`,
                `amount`,
                `taxamount`,
                `grossamount`,
                `closed`,
                `custbody_is_upgrade_required`,
                `custcol_awaitingcompletion`
            ) VALUES ";
        
        for ($i = 0; $i < count($products); $i++){
            $product = $products[$i];
            $item = $product['internalid'];
            $productID = $product['productID'];
            $pricelevel = '1'; // PRO Base Price 

            $custcol_originalitemrate = floatval($product['base_price']);
            if($product['on_sell']){
                $custcol_originalitemrate = floatval($product['discount_price']);
            }

            $quantity = intval($product['quantity']);
            $amount = $custcol_originalitemrate * $quantity;
            $taxamount = $amount * ($rate/100);  
            $grossamount = $amount + $taxamount;

            if($i > 0) $query .= ",";
            $query .= "(
                NULL,
                NULL,
                NULL,
                '$newOrderID',
                '',
                '$item',
                '$quantity',
                '0',
                '0',
                '0',
                '0',
                '$pricelevel',
                '$custcol_originalitemrate',
                '$rate',
                '',
                '',
                '$amount',
                '$taxamount',
                '$grossamount',
                false,
                false,
                false
            )";
        }

        $res = $wpdb->query($query);
        return $res;
    }
    

    /** 
     * Description: addOrderItemsFromERP
     * @param
     * @return
     */
    public function addOrderItemsFromERP($newOrderID, $products){
        global $wpdb;

        $createDate = current_time( "Y-m-d H:i:s", 0 );

        $query = "INSERT INTO $this->dbOrderItem
			(
                `ID`,
                `internalid`,
                `tranid`,
                `orderid`,
                `lineuniquekey`,
                `item`,
                `quantity`,
                `quantityshiprecv`,
                `quantitypicked`,
                `quantityfulfilled`,
                `quantitybilled`,
                `pricelevel`,
                `custcol_originalitemrate`,
                `rate`,
                `custcol_discountcode`,
                `custcol_discountpercentage`,
                `amount`,
                `taxamount`,
                `grossamount`,
                `closed`,
                `custbody_is_upgrade_required`,
                `custcol_awaitingcompletion`
            ) VALUES ";
        
        for ($i = 0; $i < count($products); $i++){
            if($i > 0) $query .= ",";

            $closed = $products[$i]['closed'] == "F" ? false : true;
            $custbody_is_upgrade_required = $products[$i]['custbody_is_upgrade_required'] == "F" ? false : true;
            $custcol_awaitingcompletion = $products[$i]['custcol_awaitingcompletion'] == "F" ? false : true;
            $quantityfulfilled = isset($products[$i]['quantityfulfilled']) ? $products[$i]['quantityfulfilled'] : 0;
            $quantitypicked = isset($products[$i]['quantitypicked']) ? $products[$i]['quantitypicked'] : 0;

            $query .= "(
                NULL,
                '". $products[$i]['internalid'] ."',
                '". $products[$i]['tranid'] ."',
                '$newOrderID',
                '". $products[$i]['lineuniquekey'] ."',
                '". $products[$i]['item'] ."',
                '". $products[$i]['quantity'] ."',
                '". $quantitypicked ."',
                '". $quantityfulfilled ."',
                '". $products[$i]['quantityshiprecv'] ."',
                '". $products[$i]['quantitybilled'] ."',
                '". $products[$i]['pricelevel'] ."',
                '". $products[$i]['custcol_originalitemrate'] ."',
                '". $products[$i]['rate'] ."',
                '". $products[$i]['custcol_discountcode'] ."',
                '". $products[$i]['custcol_discountpercentage'] ."',
                '". $products[$i]['amount'] ."',
                '". $products[$i]['taxamount'] ."',
                '". $products[$i]['Amount (Gross)'] ."',
                '$closed',
                '$custbody_is_upgrade_required',
                '$custcol_awaitingcompletion'
            )";
        }

        $res = $wpdb->query($query);
        return $res;
    }


    /** 
     * Description: getOrderSummeryAndStock
     * @param
     * @return
     */
    public function getOrderSummeryAndStock($orderItems, $userID, $entity){
        $response = array(
            'status' => false,
            'message' => '',
            'subtotal' => 0,
            'discountamount' => 0,
            'taxtotal' => 0,
            'total' => 0,
            'products' => array(),
            'stock_notifictaions' => array(),
            'errors' => array()
        );
        
        $ProductsPRO = new ProductsPRO();
        $CustomerPRO = new CustomerPRO();


        // chech for radio model qualifications
        $cust_radiomodel_qualified = $CustomerPRO->isQualifiedForRadioDiscount($userID);

        if($cust_radiomodel_qualified['status']){
            // get remote item data
            $remoteitem = $ProductsPRO->getRemoteData($cust_radiomodel_qualified['remoteitem'], $cust_radiomodel_qualified['remoteprice'], $entity);
        }

        // check for hipro qualification
        $cust_hipro_qualified = $CustomerPRO->isQualifiedForHiproModel($userID);
        
        $warrantyID = get_option('shop_warranty_product_id');
        $shippingProductID = get_option('shop_shipping_product_id');
        $rate = get_option('shop_rate');


        foreach($orderItems as $item){
            $productID = $item['productID'];
            $internalid = get_field('internalid', $productID);
            $isInternalItem = get_field('is_internal_item', $productID);

            // validate stock
            $currentStock = null;
            if(!$isInternalItem){
                $currentStock = $this->NetSuiteApiPRO->getProductStock($productID, $internalid);

                if($currentStock['status']){
                    $currentStockQuantity = $currentStock['currentStockQuantity'];
                
                    if($item['amount'] > $currentStockQuantity){
                        array_push($response['stock_notifictaions'], 
                            array('title' => 'אין מספיק מלאי', 'product' => array(
                                'itemid' => get_field('itemid', $productID),
                                'title' => get_the_title($productID),
                                'amount' => $item['amount'],
                                'current_stock' => $currentStockQuantity
                            )));
                    }
                } else {
                    array_push($response['stock_notifictaions'], array('title' => 'שגיאה בקבלת מלאי מה-ERP', 'product' => array(
                        'itemid' => get_field('itemid', $productID),
                        'title' => get_the_title($productID),
                        'amount' => $item['amount'],
                        'current_stock' => $currentStockQuantity
                    )));
                }
            } 
            
            
            $is_internal = false;
            $is_radioremote = false;
            if($productID == $warrantyID || $productID == $shippingProductID){
                $itemfinalPrice = get_field('base_price', $productID);
                $tax = round($itemfinalPrice - round( $itemfinalPrice / (1 + $rate/100), 2), 0);
                $itemBasePrice = $itemfinalPrice - $tax;
                $pricelist = array(
                    'base_price' => $itemBasePrice,
                    'base_price_after_discountcode' => $itemBasePrice,
                    'discount_percent' => 0,
                    'subtotal_price' => $itemfinalPrice
                );
                $is_internal = true;
            } else {
                // check Radio model discount
                if($cust_radiomodel_qualified['status']){
                    // set remote item price
                    if($productID == $remoteitem['productID'] && $item['parentProduct'] != ''){
                        $itemfinalPrice = $remoteitem['price_list']['base_price_after_discountcode'];
                        $tax = round($itemfinalPrice - round( $itemfinalPrice / (1 + $rate/100), 2), 0);
                        $itemBasePrice = $itemfinalPrice - $tax;

                        $pricelist = array(
                            'base_price' => $remoteitem['price_list']['base_price'],
                            'base_price_after_discountcode' => $itemBasePrice,
                            'discount_percent' => 0,
                            'subtotal_price' => $remoteitem['price_list']['base_price_after_discountcode']
                        );
                        $is_radioremote = true;
                    } else {
                        $prod_radiomodel_qualified = $ProductsPRO->isProductQualifiedForRadioDiscount($productID, $entity, $cust_radiomodel_qualified['cust_radiomodel']);

                        if($prod_radiomodel_qualified['status']){
                            $radiomodel_qualified = true;
                            $pricelist = $prod_radiomodel_qualified['pricelist'];
                        } else {
                            // check hipro discount
                            if($cust_hipro_qualified['status']){
                                $pricelist = $ProductsPRO->isProductQualifiedForHipro($productID, $internalid, $entity)['pricelist'];
                            } else {
                                $pricelist = $ProductsPRO->getProductPricesList($internalid, $entity, $productID);
                            }
                        }
                    }
                } else {
                    $pricelist = $ProductsPRO->getProductPricesList($internalid, $entity, $productID);
                }
            }
           
            // calc totals
            
            $base_price = $pricelist['base_price'];
            $base_price_after_discountcode = $pricelist['base_price_after_discountcode'];

            $on_sell = get_field('on_sell', $productID);
            $discount_price = floatval(get_field('discount_price', $productID));

            if($is_radioremote == true || $is_internal == true){
                $response['subtotal'] += round(floatval($pricelist['subtotal_price']) * floatval($item['amount']),2);
            } else {
                $response['subtotal'] += round(floatval($base_price_after_discountcode) * floatval($item['amount']),2);
            }
            
            $finalPrice = floatval($base_price_after_discountcode) * floatval($item['amount']);
            $final_base_price = $base_price_after_discountcode;
           
            if($base_price == 0){
                $finalDiscountPercentage = 100;
            } else {
                $finalDiscountPercentage = round(floatval(($base_price - $base_price_after_discountcode)/$base_price) * 100,2);
            }
            // $response['discountamount'] += round((floatval($base_price) - floatval($base_price_after_discountcode)) * floatval($item['amount']),2);
            
            if($on_sell){
                $base_price_after_discountcode = number_format($discount_price * floatval(1 + $rate/100), 2);

                // $response['discountamount'] += round((floatval($base_price) - floatval($discount_price)) * floatval($item['amount']),2);
                $finalPrice = floatval($discount_price) * floatval($item['amount']);
                $final_base_price = $discount_price;
                
                $finalDiscountPercentage = round(floatval(($base_price - $discount_price)/$base_price) * 100,2);
            }
            $finalAmount = round(floatval($final_base_price) * intval($item['amount']), 2);
            $finalTaxAmount = round($finalAmount * $rate / 100, 0);
            
            if($is_radioremote == true || $is_internal == true){
                $response['total'] += round(floatval($pricelist['subtotal_price']) * floatval($item['amount']),2);
            } else {
                $response['total'] += round($finalPrice,2);
            }

            array_push($response['products'], array(
                'productID' => $productID,
                'internalid' => $internalid,
                'quantity' => $item['amount'],
                'base_price' => $base_price,
                'final_base_price' => $final_base_price,
                'finalAmount' => $finalAmount,
                'finalTaxAmount' => $finalTaxAmount,
                'on_sell' => $on_sell,
                'discount_price' => $discount_price,
                'finalPrice' => $finalPrice,
                'finalDiscountPercentage' => $finalDiscountPercentage,
                'currentStockQuantity' => $currentStockQuantity,
                'is_internal' => $is_internal
            ));
        }

        if(count($response['errors']) == 0) $response['status'] = true;
        return $response;
    }
  

    /** 
     * Description: getItemsTableHtml
     * @param
     * @return
     */
    public function getItemsTableHtml($orderData, $itemsData){
        $html = "<style>
            th, td{
                border: 1px solid #ddd;
                padding: 8px;
                text-align: right;
            }
            thead{
                background: #000;
                color: #fff;
                padding: 8px;
            }
        </style>";

        $html .= "<table style='background: #fff; width: 100%; direction: rtl;border-collapse: collapse;'>
            <thead>
                <tr>
                    <th>פריט</th>
                    <th>מק\"ט</th>
                    <th>כמות</th>
                    <th>סך ביניים</th>
                </tr>
            </thead>
            <tbody>";
            
        // print lines
        $haveShipping = false;
        $shippingTotal = 0;
        $shippingProductID = get_option('shop_shipping_product_id');

        foreach($itemsData as $item){
            if($item->productID == $shippingProductID){
                $haveShipping = true;
                $shippingTotal = $item->grossamount;
                continue;
            }
            $html .= "<tr>
                <td>$item->post_title</td>
                <td>$item->itemid</td>
                <td>$item->quantity</td>
                <td>$item->amount ₪</td>
            </tr>";
        }

        // print totals row
        $html .= "<tr style='background: #fef4dd;'>
                <td colspan='4'>
                    <span>סך ביניים: " .number_format($orderData['subtotal'],2) ." ₪</span><br>
                    <span>טיפול ומשלוח: " .number_format($shippingTotal, 2) ." ₪</span><br>
                    <span>הנחה: " .number_format($orderData['discountamount'], 2) ." ₪</span><br>
                    <span>סה\"כ: " .number_format($orderData['total'],2) ." ₪</span>
                </td>
            </tr>";
        $html .= "</tbody>
        </table>";

        return $html;
    }
    

/** 
     * Description: getStockNotificationItemsTableHtml
     * @param
     * @return
     */
    public function getStockNotificationItemsTableHtml($itemsData){
        $html = "<style>
            th, td{
                border: 1px solid #ddd;
                padding: 8px;
                text-align: right;
            }
            thead{
                background: #000;
                color: #fff;
                padding: 8px;
            }
        </style>";

        $html .= "<table style='background: #fff; width: 100%; direction: rtl;border-collapse: collapse;'>
            <thead>
                <tr>
                    <th>מק\"ט</th>
                    <th>שם</th>
                    <th>כמות נדרשת</th>
                    <th>מלאי זמין</th>
                    <th>שגיאה</th>
                </tr>
            </thead>
            <tbody>";
            
        // print lines
        foreach($itemsData as $item){
            $html .= "<tr>
                <td>".$item['product']['itemid']."</td>
                <td>".$item['product']['title']."</td>
                <td>".$item['product']['amount']."</td>
                <td>".$item['product']['current_stock']."</td>
                <td>".$item['title']."</td>
            </tr>";
        }

        $html .= "</tbody>
        </table>";

        return $html;
    }


    /** 
     * Description: getCalcInputsHtml($calcItems)
     * @param
     * @return
     */
    public function getCalcInputsHtml($calcItems){
        $html = "<style>
        </style>";

        $html .= "<ul style='list-style: none; margin: 20px 0; padding: 0;'>";
            
        // print lines
        foreach($calcItems as $item){
            $productTitle = get_the_title($item['productID']);
            $itemBackup = $item['calc_userInput']['backup'] == 'true' ? 'כן' : 'לא';
            $html .= "<li style='margin: 5px 0; padding: 5px; background:#eeeeee;'>";
            if($item['calc_userInput']['clac'] == 1){
                $html .= "<span style='font-weight:bold;'>קלט מחשבון למוצר: " .$productTitle . "</span></br>";
                $html .= "<p style='margin-top: 0; padding-right: 5px;'>
                    רוחב חלון: ". $item['calc_userInput']['windowWidth'] . ". " ."
                    גובה חלון: ". $item['calc_userInput']['windowHeight'] . ". " ."
                    סוג שלב: ". $item['calc_userInput']['blind_type'] . ". " ."
                    גובה שלב: ". $item['calc_userInput']['blind_height'] . ". " ."
                    משקל למ\"ר: ". $item['calc_userInput']['mWeight'] . ". " ."
                    משקל כללי: ". $item['calc_userInput']['totalWeight'] . ". " ."
                    קוטר צינור: ". implode(", ", $item['calc_userInput']['pipeDiameter']) . ". " ."
                    טכנולוגיה: ".$item['calc_userInput']['techTexts'] . ". " ."
                    גיבוי ידני: ". $itemBackup . ". " ."
                    סל\"ד: ".$item['calc_userInput']['rpm'] . ". " ."
                </p>";
            } else if($item['calc_userInput']['clac'] == 2){
                $html .= "<span style='font-weight:bold;'>קלט מחשבון למוצר: " .$productTitle . "</span></br>";
                $html .= "<p style='margin-top: 0; padding-right: 5px;'>
                    פתיחה: ". $item['calc_userInput']['opening'] . ". " ."
                    רוחב סוכך: ". $item['calc_userInput']['width'] . ". " ."
                    מספר זרועות: ". $item['calc_userInput']['arms'] . ". " ."
                    קוטר צינור: ". implode(", ", $item['calc_userInput']['pipeDiameter']) . ". " ."
                    טכנולוגיה: ".$item['calc_userInput']['techTexts'] . ". " ."
                    גיבוי ידני: ". $itemBackup . ". " ."
                </p>";
            } 
            $html .= "</li>";
        }

        // print totals row
        $html .= "</ul>";

        return $html;
    }

    /** 
     * Description: getItemsTableHtml
     * @param
     * @return
     */
    public function clearOlderOrders($entity){
        $orders = new \WP_Query(array(
            'post_type' => 'somfy_order',
            'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash'),  
            'meta_query' => array(
                'relation' => 'AND',
                array(
                    'key' => 'entity',
                    'compare' => '=',
                    'value' => $entity
                ),
                array(
                    'key' => 'is_synced',
                    'compare' => '=',
                    'value' => 0
                )
            )
        ));

        if(count($orders->posts) > 0){
            $results = array();
            while ($orders->have_posts()) {
                $orders->the_post();
                $orderID =  get_the_ID();
                wp_trash_post($orderID);
                wp_delete_post($orderID);
                array_push($results, $orderID);
            }
            return $results;
        } else {
            return false;
        }
    }


    /** 
     * Description: send stock notification
     * @param
     * @return
     */
    public function send_stock_notification($userData, $orderData, $items){
        date_default_timezone_set('Israel');
        
        $SomfyMessaging = new SomfyMessaging();
        
        // get html template and email subject
        $emailSubjectTo = get_option('pro_stock_notification__to');
        $emailSubjectTemplate = get_option('pro_stock_notification__subject');
        $template = get_option('pro_stock_notification__template');
        $emailContentTemplate = $SomfyMessaging->getTemplate($template);

        // create items table html
        $items_table = $this->getStockNotificationItemsTableHtml($items);


        // replace placeholder with data
        $emailContentData = array(
            'order_tranid' => $orderData['tranid'],
            'order_date' => date('d/m/Y H:i'),
            'items_table' => $items_table,
            'display_name' => $userData['display_name'],
            'customer_email' => $userData['user_email']
        );

        foreach($emailContentData as $key => $value){
            $emailContentTemplate = str_replace("{{".$key."}}", $value, $emailContentTemplate);
        }

        $emailSubjectTemplate = str_replace("{{order_tranid}}",  $orderData['tranid'], $emailSubjectTemplate);
        
        $sendTo = $emailSubjectTo;
        $subject = $emailSubjectTemplate;
        $htmlBody = $emailContentTemplate;

        // send to customer
        $messageSent = $SomfyMessaging->sendMail($sendTo, $subject, $htmlBody);

        return $messageSent;
    }
}