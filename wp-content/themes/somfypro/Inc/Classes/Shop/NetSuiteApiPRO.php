<?php
/**
 * @package  SomfySiteManager
 */

namespace Inc\Classes\Shop;


class NetSuiteApiPRO{

    /**
     * PRODUCTS END POINTS
     */
    
    /**
     * Description: Get Product stock
     * @param
     * @return
     */
    public function getProductStock($productID, $internalid){
        $script_ID = '672';
        $deploy_ID = '6';

        $auth_header = $this->getGETAuthHeaders($deploy_ID, $script_ID, $internalid);

        $ch = curl_init();

        curl_setopt($ch,CURLOPT_URL, NETSUITE_URL . '?&script=' . $script_ID . '&deploy=' . $deploy_ID . '&id=' . $internalid);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: ' . $auth_header,
            'Content-Type: application/json'
        ]);

        $res = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $response = json_decode($res, true);

        curl_close($ch);
        
        if(isset($response['error'])){
            return array(
                'status' => false,
                'http_code' => $http_code,
                'error' => $response['error']
            );
        } else {
            $currentStockQuantity = isset($response[0]['locationquantityavailable']) ? $response[0]['locationquantityavailable'] : 0;

            // update product stock
            update_field('stock_quantity', $currentStockQuantity, $productID);

            return array(
                'status' => true,
                'http_code' => $http_code,
                'currentStockQuantity' => $currentStockQuantity,
                'data' => $response
            );
        }
    }


    /**
     * Description: getProductsPriceEU
     * @param
     * @return
     */
    public function getProductsPricePRO($customer = null, $qty = 0, $productArray = null){
        $script_ID = '672';
        $deploy_ID = '10';

        $currency = 1;
        
        if(is_null($customer)) $customer = '';
        
        if(!is_null($productArray)){
            $itemsString = implode(",",$productArray);
        } else {
            $itemsString = '';
        }

        $auth_header = $this->getGETAuthHeadersPricePRO($deploy_ID, $script_ID, $customer, $currency, $qty, $itemsString);
        $ch = curl_init();

        curl_setopt($ch,CURLOPT_URL, NETSUITE_URL . '?&script=' . $script_ID . '&deploy=' . $deploy_ID . '&id=' . $customer . '&currency=' . $currency . '&qty=' . $qty . '&item=' . $itemsString);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: ' . $auth_header,
            'Content-Type: application/json'
        ]);

        $res = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $response = json_decode($res, true);

        curl_close($ch);
        
        if(isset($response['error'])){
            return array(
                'status' => false,
                'http_code' => $http_code,
                'error' => $response['error']
            );
        } else {
            return array(
                'status' => true,
                'http_code' => $http_code,
                'data' => $response
            );
        }
    }

    /**
     * CUSTOMERS END POINTS
     */


    /**
     * Description: Get Customer by ID
     * @param
     * @return
     */
    public function getCustomerByID($internalid){
        $script_ID = '672';
        $deploy_ID = '3';

        $auth_header = $this->getGETAuthHeaders($deploy_ID, $script_ID, $internalid);

        $ch = curl_init();

        curl_setopt($ch,CURLOPT_URL, NETSUITE_URL . '?&script=' . $script_ID . '&deploy=' . $deploy_ID . '&id=' . $internalid);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: ' . $auth_header,
            'Content-Type: application/json'
        ]);

        $res = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $response = json_decode($res, true);

        curl_close($ch);
        
        if(isset($response['error'])){
            return array(
                'status' => false,
                'http_code' => $http_code,
                'error' => $response['error']
            );
        } else {
            return array(
                'status' => true,
                'http_code' => $http_code,
                'data' => $response
            );
        }
    }

    /**
     * Description: New Customer
     * @param
     * @return
     */
    public function newCustomer($newCustomerData){
        $script_ID = '671';
        $deploy_ID = '1';
  
        // get data from user
        $custJSON = '[{
            "recordtype": "customer",
            "externalid":"' . $newCustomerData['user_id'] .'",
            "custentity_customertype":"' . $newCustomerData['custentity_customertype'] .'",
            "subsidiary":"' . $newCustomerData['subsidiary'] .'",
            "taxitem":"' . $newCustomerData['taxitem'] .'",
            "currencyPrim":"' . $newCustomerData['currencyPrim'] .'",
            "currency": "' . $newCustomerData['currency'] .'",
            "custentity_codeification":"' . $newCustomerData['custentity_codeification'] .'",
            "custentity_geocust":"' . $newCustomerData['custentity_geocust'] .'",
            "custentity_hirarchy":"' . $newCustomerData['custentity_hirarchy'] .'",
            "receivablesaccount":"' . $newCustomerData['receivablesaccount'] .'",
            "isperson" : "' . $newCustomerData['isperson'] .'",
            "firstname" : "' .$newCustomerData['first_name'] .'",
            "lastname" : "' .$newCustomerData['last_name'] .'",
            "companyname":"' .$newCustomerData['companyname'] .'",
            "custentity_il_mobile":"' .$newCustomerData['custentity_il_mobile'] .'",
            "email":"' .$newCustomerData['email'] .'",
            "phone":"' .$newCustomerData['phone'] .'"
        }]';

        $auth_header = $this->getPOSTAuthHeaders($deploy_ID, $script_ID);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, NETSUITE_URL . '?&script=' . $script_ID . '&deploy=' . $deploy_ID . '&realm=' . NETSUITE_ACCOUNT);
        curl_setopt($ch, CURLOPT_POST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $custJSON);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: ' . $auth_header,
            'Content-Type: application/json',
            'Content-Length: ' . strlen($custJSON)
        ]);

        $res = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $response = json_decode($res, true);

        curl_close($ch);
        
        if(isset($response['error'])){
            return array(
                'status' => false,
                'http_code' => $http_code,
                'error' => $response['error']
            );
        } else {
            return array(
                'status' => true,
                'http_code' => $http_code,
                'data' => $response,
                'createdRecordid' => $response[0]['createdRecordid']
            );
        }
    }

    /**
     * Description: Update Customer data
     * @param
     * @return
     */
    public function updateCustomer($userData){
        $script_ID = '671';
        $deploy_ID = '1';
  
        // get data from user
        $custJSON = '[{
            "recordtype": "customer",
            "recordid":"' . $userData['internalid'] .'",
            "firstname" : "' .$userData['first_name'] .'",
            "lastname" : "' .$userData['last_name'] .'",
            "companyname":"' .$userData['companyname'] .'",
            "custentity_il_mobile":"' .$userData['custentity_il_mobile'] .'",
            "email":"' .$userData['email'] .'",
            "phone":"' .$userData['phone'] .'",
            "isperson" : "T"
        }]';
        $auth_header = $this->getPOSTAuthHeaders($deploy_ID, $script_ID);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, NETSUITE_URL . '?&script=' . $script_ID . '&deploy=' . $deploy_ID . '&realm=' . NETSUITE_ACCOUNT);
        curl_setopt($ch, CURLOPT_POST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $custJSON);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: ' . $auth_header,
            'Content-Type: application/json',
            'Content-Length: ' . strlen($custJSON)
        ]);

        $res = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $response = json_decode($res, true);

        curl_close($ch);
        
        if(isset($response['error'])){
            return array(
                'status' => false,
                'http_code' => $http_code,
                'error' => $response['error']
            );
        } else {
            return array(
                'status' => true,
                'http_code' => $http_code,
                'data' => $response
            );
        }
    }

    /**
     * Description: Get Customer by ID
     * @param
     * @return
     */
    public function getCustomerBalance($internalid){
        $script_ID = '672';
        $deploy_ID = '8';

        $auth_header = $this->getGETAuthHeaders($deploy_ID, $script_ID, $internalid);

        $ch = curl_init();

        curl_setopt($ch,CURLOPT_URL, NETSUITE_URL . '?&script=' . $script_ID . '&deploy=' . $deploy_ID . '&id=' . $internalid);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: ' . $auth_header,
            'Content-Type: application/json'
        ]);

        $res = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $response = json_decode($res, true);

        curl_close($ch);
        
        if(isset($response['error'])){
            return array(
                'status' => false,
                'http_code' => $http_code,
                'error' => $response['error']
            );
        } else {
            return array(
                'status' => true,
                'http_code' => $http_code,
                'data' => $response
            );
        }
    }


    /**
     * ORDERS END POINTS
     */

    /**
     * Description: Get Order by ID
     * @param
     * @return
     */
    public function getOrderById($order_id){
        $script_ID = '672';
        $deploy_ID = '7';

        $auth_header = $this->getGETAuthHeaders($deploy_ID, $script_ID, $order_id);

        $ch = curl_init();

        curl_setopt($ch,CURLOPT_URL, NETSUITE_URL . '?&script=' . $script_ID . '&deploy=' . $deploy_ID . '&id=' . $order_id);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: ' . $auth_header,
            'Content-Type: application/json'
        ]);

        $res = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $response = json_decode($res, true);

        curl_close($ch);
        
        if(isset($response['error'])){
            return array(
                'status' => false,
                'http_code' => $http_code,
                'error' => $response['error']
            );
        } else {
            return array(
                'status' => true,
                'http_code' => $http_code,
                'data' => $response
            );
        }
    }

    /**
     * Description: Get Orders by entityid
     * @param
     * @return
     */
    public function getOrdersByEntityid($entityid){
        $script_ID = '672';
        $deploy_ID = '7';

        $auth_header = $this->getGETAuthHeadersByEntityid($deploy_ID, $script_ID, $entityid);

        $ch = curl_init();

        curl_setopt($ch,CURLOPT_URL, NETSUITE_URL . '?&script=' . $script_ID . '&deploy=' . $deploy_ID . '&entityid=' . $entityid);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: ' . $auth_header,
            'Content-Type: application/json'
        ]);

        $res = curl_exec($ch);

        
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        // print_r($http_code);
        // die;
        $response = json_decode($res, true);

        curl_close($ch);
        
        if(isset($response['error'])){
            return array(
                'status' => false,
                'http_code' => $http_code,
                'error' => $response['error']
            );
        } else {
            return array(
                'status' => true,
                'http_code' => $http_code,
                'data' => $response
            );
        }

    }


    /**
     * Description: Get Order Delivery Data
     * @param
     * @return
     */
    public function getOrderDeliveryData($order_internalid){
        $script_ID = '672';
        $deploy_ID = '12';

        $auth_header = $this->getGETAuthHeaders($deploy_ID, $script_ID, $order_internalid);

        $ch = curl_init();

        curl_setopt($ch,CURLOPT_URL, NETSUITE_URL . '?&script=' . $script_ID . '&deploy=' . $deploy_ID . '&id=' . $order_internalid);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: ' . $auth_header,
            'Content-Type: application/json'
        ]);

        $res = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $response = json_decode($res, true);

        curl_close($ch);
        
        if(isset($response['error'])){
            return array(
                'status' => false,
                'http_code' => $http_code,
                'error' => $response['error']
            );
        } else {
            return array(
                'status' => true,
                'http_code' => $http_code,
                'data' => $response
            );
        }
    }
    
    /**
     * Description: New Order
     * @param
     * @return
     */
    public function newOrder($orderCreated, $order_args, $userData, $orderDefaults){
        $script_ID = '671';
        $deploy_ID = '1';

        // prep order data
        $newOrderID = $orderCreated['newOrderID'];

        $itemsArr = array();

        $pricelevel = '1'; // PRO Base Price

        $orderItems = $orderCreated['orderTotals']['products'];

        foreach($orderItems as $product){
            $custcol_originalitemrate = floatval($product['finalPrice']);
            if($product['on_sell']){
                $custcol_originalitemrate = floatval($product['discount_price']);
            }
    
            $quantity = intval($product['quantity']);

            array_push($itemsArr, array(
                "item" => $product['internalid'],
                "quantity" => $quantity,
                "pricelevel" => $pricelevel, 
                "custcol_originalitemrate" => $product['base_price'],
                "rate" => $product['final_base_price'],
                "amount" => $product['finalAmount'],
                "custcol_discountpercentage" => $product['finalDiscountPercentage'],
                "taxamount" => $product['finalTaxAmount']
            ));
        }
            
        if($order_args['shipmethod'] == 'delivery'){
            $isPickup = 'F';
        } else {
            $isPickup = 'T';
        }

        // get data from user
        $orderPayload = array(
            array(
                "recordtype" => "salesorder",
                "custbody_websiteorder" => "T",
                "entity" => $userData['entity'],
                "memo" => "PRO Site - test",
                "currencyPrim" => $orderDefaults['currencyPrim'],
                "subtotal" =>round( $orderCreated['orderTotals']['subtotal'],2),
                "discountamount" => round($orderCreated['orderTotals']['discountamount'],2),
                "total" => round($orderCreated['orderTotals']['total'],2),
                "taxtotal" => round($orderCreated['orderTotals']['taxtotal'],2),
                "terms" => $orderCreated['terms'],
                "salesrep"  => $userData['salesrep'],
                "custbody_pickup" => $isPickup,
                "item" => $itemsArr 
            )
        );

        $orderJSON = json_encode($orderPayload);

        $auth_header = $this->getPOSTAuthHeaders($deploy_ID, $script_ID);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, NETSUITE_URL . '?&script=' . $script_ID . '&deploy=' . $deploy_ID . '&realm=' . NETSUITE_ACCOUNT);
        curl_setopt($ch, CURLOPT_POST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $orderJSON);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: ' . $auth_header,
            'Content-Type: application/json',
            'Content-Length: ' . strlen($orderJSON)
        ]);

        $res = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $response = json_decode($res, true);

        error_log(json_encode($response));
        
        curl_close($ch);
        
        if(isset($response['error']) || !isset($response[0]['createdRecordid'])){
            return array(
                'status' => false,
                'http_code' => $http_code,
                'error' => $response['error']
            );
        } else {
            // update local order title and internal ids
            update_field('internalid', $response[0]['createdRecordid'], $newOrderID);


            return array(
                'status' => true,
                'http_code' => $http_code,
                'data' => $response,
                'createdRecordid' => $response[0]['createdRecordid']
            );
        }
    }


    /**
     * Description: New Order
     * @param
     * @return
     */
    public function newRMA_SA($userData, $rmaItems, $entity, $externalid, $rmaType){
        $script_ID = '681';
        $deploy_ID = '1';

        // prep order data
        $itemsArr = array();

        foreach($rmaItems as $item){
            array_push($itemsArr, array(
                "item" => $item['product_internalid'],
                "quantity" => $item['amount_2_pick'],
                "custcol_rmaclassification" => $item['type']
                // ,
                // "custcol_invoicereference" => $item['invInternalid']
            ));
        }
            
        // get data from user
        $rmaPayload = array(
            array(
                "recordtype" => "returnauthorization",
                "externalid" => $externalid,
                "entity" => $entity,
                "memo" => "PRO Site - test",
                "custbody_rmatype"  => $rmaType,
                "item" => $itemsArr 
            )
        );

        $rmaJSON = json_encode($rmaPayload);

        $auth_header = $this->getPOSTAuthHeaders($deploy_ID, $script_ID);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, NETSUITE_URL . '?&script=' . $script_ID . '&deploy=' . $deploy_ID . '&realm=' . NETSUITE_ACCOUNT);
        curl_setopt($ch, CURLOPT_POST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $rmaJSON);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: ' . $auth_header,
            'Content-Type: application/json',
            'Content-Length: ' . strlen($rmaJSON)
        ]);

        $res = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $response = json_decode($res, true);

        curl_close($ch);
        
        if(isset($response['error']) 
            || (!isset($response[0]['createdRecordid']) || is_null($response[0]['createdRecordid'] == null))){
            return array(
                'status' => false,
                'http_code' => $http_code,
                'error' => is_null($response['error']) ? $response[0]['message'] : $response['error']
            );
        } else {
            
            return array(
                'status' => true,
                'http_code' => $http_code,
                'data' => $response,
                'createdRecordid' => $response[0]['createdRecordid']
            );
        }
    }


/**
     * Description: getInvFile recordid
     * @param
     * @return
     */
    public function getInvFile($recordid){
        $script_ID = '696';
        $deploy_ID = '1';

        $auth_header = $this->getGETAuthHeadersByRecordid($deploy_ID, $script_ID, $recordid);

        $ch = curl_init();

        curl_setopt($ch,CURLOPT_URL, NETSUITE_URL . '?&script=' . $script_ID . '&deploy=' . $deploy_ID . '&recordid=' . $recordid);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: ' . $auth_header,
            'Content-Type: application/json'
        ]);

        $res = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $response = json_decode($res, true);

        curl_close($ch);
        
        if(isset($response['error'])){
            return array(
                'status' => false,
                'http_code' => $http_code,
                'error' => $response['error']
            );
        } else {
            return array(
                'status' => true,
                'http_code' => $http_code,
                'data' => $response
            );
        }
    }


    // Restlet auth headers

    // Auth headers - GET with id 
    protected function getGETAuthHeadersByEntityid($deploy_ID, $script_ID, $entityid){
        $oauth_nonce = md5(mt_rand());
        $oauth_timestamp = time();
        $oauth_signature_method = 'HMAC-SHA1';
        $oauth_version = "1.0";

        $base_string =
            "GET&" . urlencode(NETSUITE_URL) . "&" .
            urlencode(
                "deploy=" . $deploy_ID
                . "&entityid=" . $entityid
                . "&oauth_consumer_key=" . NETSUITE_CONSUMER_KEY
                . "&oauth_nonce=" . $oauth_nonce
                . "&oauth_signature_method=" . $oauth_signature_method
                . "&oauth_timestamp=" . $oauth_timestamp
                . "&oauth_token=" . NETSUITE_TOKEN_ID
                . "&oauth_version=" . $oauth_version
                . "&script=" . $script_ID
            );

        $sig_string = urlencode(NETSUITE_CONSUMER_SECRET) . '&' . urlencode(NETSUITE_TOKEN_SECRET);
        $signature = base64_encode(hash_hmac("sha1", $base_string, $sig_string, true));

        $auth_header = "OAuth "
            . 'oauth_signature="' . rawurlencode($signature) . '", '
            . 'oauth_version="' . rawurlencode($oauth_version) . '", '
            . 'oauth_nonce="' . rawurlencode($oauth_nonce) . '", '
            . 'oauth_signature_method="' . rawurlencode($oauth_signature_method) . '", '
            . 'oauth_consumer_key="' . rawurlencode(NETSUITE_CONSUMER_KEY) . '", '
            . 'oauth_token="' . rawurlencode(NETSUITE_TOKEN_ID) . '", '  
            . 'oauth_timestamp="' . rawurlencode($oauth_timestamp) . '", '
            . 'realm="' . rawurlencode(NETSUITE_ACCOUNT) .'"';

        return $auth_header;
    }

    // Auth headers - GET with id 
    protected function getGETAuthHeaders($deploy_ID, $script_ID, $internalid){
        $oauth_nonce = md5(mt_rand());
        $oauth_timestamp = time();
        $oauth_signature_method = 'HMAC-SHA1';
        $oauth_version = "1.0";

        $base_string =
            "GET&" . urlencode(NETSUITE_URL) . "&" .
            urlencode(
                "deploy=" . $deploy_ID
                . "&id=" . $internalid
                . "&oauth_consumer_key=" . NETSUITE_CONSUMER_KEY
                . "&oauth_nonce=" . $oauth_nonce
                . "&oauth_signature_method=" . $oauth_signature_method
                . "&oauth_timestamp=" . $oauth_timestamp
                . "&oauth_token=" . NETSUITE_TOKEN_ID
                . "&oauth_version=" . $oauth_version
                . "&script=" . $script_ID
            );

        $sig_string = urlencode(NETSUITE_CONSUMER_SECRET) . '&' . urlencode(NETSUITE_TOKEN_SECRET);
        $signature = base64_encode(hash_hmac("sha1", $base_string, $sig_string, true));

        $auth_header = "OAuth "
            . 'oauth_signature="' . rawurlencode($signature) . '", '
            . 'oauth_version="' . rawurlencode($oauth_version) . '", '
            . 'oauth_nonce="' . rawurlencode($oauth_nonce) . '", '
            . 'oauth_signature_method="' . rawurlencode($oauth_signature_method) . '", '
            . 'oauth_consumer_key="' . rawurlencode(NETSUITE_CONSUMER_KEY) . '", '
            . 'oauth_token="' . rawurlencode(NETSUITE_TOKEN_ID) . '", '  
            . 'oauth_timestamp="' . rawurlencode($oauth_timestamp) . '", '
            . 'realm="' . rawurlencode(NETSUITE_ACCOUNT) .'"';

        return $auth_header;
    }

    // Auth headers - GET with id 
    protected function getGETAuthHeadersByRecordid($deploy_ID, $script_ID, $recordid){
        $oauth_nonce = md5(mt_rand());
        $oauth_timestamp = time();
        $oauth_signature_method = 'HMAC-SHA1';
        $oauth_version = "1.0";

        $base_string =
            "GET&" . urlencode(NETSUITE_URL) . "&" .
            urlencode(
                "deploy=" . $deploy_ID
                . "&oauth_consumer_key=" . NETSUITE_CONSUMER_KEY
                . "&oauth_nonce=" . $oauth_nonce
                . "&oauth_signature_method=" . $oauth_signature_method
                . "&oauth_timestamp=" . $oauth_timestamp
                . "&oauth_token=" . NETSUITE_TOKEN_ID
                . "&oauth_version=" . $oauth_version
                . "&recordid=" . $recordid
                . "&script=" . $script_ID
            );

        $sig_string = urlencode(NETSUITE_CONSUMER_SECRET) . '&' . urlencode(NETSUITE_TOKEN_SECRET);
        $signature = base64_encode(hash_hmac("sha1", $base_string, $sig_string, true));

        $auth_header = "OAuth "
            . 'oauth_signature="' . rawurlencode($signature) . '", '
            . 'oauth_version="' . rawurlencode($oauth_version) . '", '
            . 'oauth_nonce="' . rawurlencode($oauth_nonce) . '", '
            . 'oauth_signature_method="' . rawurlencode($oauth_signature_method) . '", '
            . 'oauth_consumer_key="' . rawurlencode(NETSUITE_CONSUMER_KEY) . '", '
            . 'oauth_token="' . rawurlencode(NETSUITE_TOKEN_ID) . '", '  
            . 'oauth_timestamp="' . rawurlencode($oauth_timestamp) . '", '
            . 'realm="' . rawurlencode(NETSUITE_ACCOUNT) .'"';

        return $auth_header;
    }

    protected function getPOSTAuthHeaders($deploy_ID, $script_ID){
        $oauth_nonce = md5(mt_rand());
        $oauth_timestamp = time();
        $oauth_signature_method = 'HMAC-SHA1';
        $oauth_version = "1.0";

        $base_string =
            "POST&" . urlencode(NETSUITE_URL) . "&" .
            urlencode(
                "deploy=" . $deploy_ID
            . "&oauth_consumer_key=" . NETSUITE_CONSUMER_KEY
            . "&oauth_nonce=" . $oauth_nonce
            . "&oauth_signature_method=" . $oauth_signature_method
            . "&oauth_timestamp=" . $oauth_timestamp
            . "&oauth_token=" . NETSUITE_TOKEN_ID
            . "&oauth_version=" . $oauth_version
            . "&realm=" . NETSUITE_ACCOUNT
            . "&script=" . $script_ID
            );

        $sig_string = urlencode(NETSUITE_CONSUMER_SECRET) . '&' . urlencode(NETSUITE_TOKEN_SECRET);
        $signature = base64_encode(hash_hmac("sha1", $base_string, $sig_string, true));

        $auth_header = "OAuth "
            . 'oauth_signature="' . rawurlencode($signature) . '", '
            . 'oauth_version="' . rawurlencode($oauth_version) . '", '
            . 'oauth_nonce="' . rawurlencode($oauth_nonce) . '", '
            . 'oauth_signature_method="' . rawurlencode($oauth_signature_method) . '", '
            . 'oauth_consumer_key="' . rawurlencode(NETSUITE_CONSUMER_KEY) . '", '
            . 'oauth_token="' . rawurlencode(NETSUITE_TOKEN_ID) . '", '  
            . 'oauth_timestamp="' . rawurlencode($oauth_timestamp) . '", '
            . 'realm="' . rawurlencode(NETSUITE_ACCOUNT) .'"';

        return $auth_header;
    }


    public function somfylog($title, $message){
		global $wpdb;
        $dbSomfylog = EU_SITE_PREFIX . "somfylog";

		$createDate = current_time( "Y-m-d H:i:s", 0 );
		$message = json_encode($message);
		
		$query = "INSERT INTO $dbSomfylog
			(
                `ID`, 
                `title`, 
                `message`, 
                `create_date`
            ) VALUES (
                NULL,
				'$title',
				'$message',
				'$createDate'
		)";

        $wpdb->query($query);
	}

    // param - currency, qty, itemsString, id
    protected function getGETAuthHeadersPricePRO($deploy_ID, $script_ID, $id, $currency, $qty, $itemsString){
        $oauth_nonce = md5(mt_rand());
        $oauth_timestamp = time();
        $oauth_signature_method = 'HMAC-SHA1';
        $oauth_version = "1.0";

        $base_string =
            "GET&" . urlencode(NETSUITE_URL) . "&" .
            urlencode(
                "currency=" . $currency
                . "&deploy=" . $deploy_ID
                . "&id=" . $id
                . "&item=" . urlencode($itemsString)
                . "&oauth_consumer_key=" . NETSUITE_CONSUMER_KEY
                . "&oauth_nonce=" . $oauth_nonce
                . "&oauth_signature_method=" . $oauth_signature_method
                . "&oauth_timestamp=" . $oauth_timestamp
                . "&oauth_token=" . NETSUITE_TOKEN_ID
                . "&oauth_version=" . $oauth_version
                . "&qty=" . $qty
                . "&script=" . $script_ID
            );

        $sig_string = urlencode(NETSUITE_CONSUMER_SECRET) . '&' . urlencode(NETSUITE_TOKEN_SECRET);
        $signature = base64_encode(hash_hmac("sha1", $base_string, $sig_string, true));

        $auth_header = "OAuth "
            . 'oauth_signature="' . rawurlencode($signature) . '", '
            . 'oauth_version="' . rawurlencode($oauth_version) . '", '
            . 'oauth_nonce="' . rawurlencode($oauth_nonce) . '", '
            . 'oauth_signature_method="' . rawurlencode($oauth_signature_method) . '", '
            . 'oauth_consumer_key="' . rawurlencode(NETSUITE_CONSUMER_KEY) . '", '
            . 'oauth_token="' . rawurlencode(NETSUITE_TOKEN_ID) . '", '  
            . 'oauth_timestamp="' . rawurlencode($oauth_timestamp) . '", '
            . 'realm="' . rawurlencode(NETSUITE_ACCOUNT) .'"';

        return $auth_header;
    }
}

