<?php
/**
 * @package  SomfySiteManager
 */

namespace Inc\Classes\Shop;

use Inc\Classes\Shop\NetSuiteApiPRO;
use Inc\Classes\SomfyMessaging;

class RMAPRO{

    public function new_rma_request($request){
        if ( !wp_verify_nonce( $_REQUEST['nonce'], NONCE_SECRET)) {
           exit("No naughty business please");
        }  
        $rmaType = '3';

        //  validate params
        $userData = isset($_REQUEST['userData']) ? $_REQUEST['userData'] : null;
        $rmaItems = isset($_REQUEST['rmaItems']) ? $_REQUEST['rmaItems'] : null;
        
        
        if(is_null($userData) || is_null($rmaItems)){
            $result['status'] = false;
            $result['p'] = $_REQUEST;
            $result['message'] = 'חלק מהנתונים חסרים';
            echo json_encode($result);
            die();
        }

        // validate logged user
        $userID = get_current_user_id();

        if(!$userID){
            $result['status'] = false;
            $result['message'] = 'משתמש לא מחובר';
            echo json_encode($result);
            die();
        } 

        // prep user data
        $userData['user_id'] = $userID;
        
        $entity = isset(get_user_meta($userID, 'internalid')[0]) ? get_user_meta($userID, 'internalid')[0] : '';
        
        if($entity == ""){
            $result['status'] = false;
            $result['message'] = 'משתמש לא מחובר';
            echo json_encode($result);
            die();
        } else {
            $userData['entity'] = $entity;
        }

        // create rma head
        $headerCreated = $this->insertRMAHeadFromSite($userData, $rmaType);

        if($headerCreated == false) {
            $result['status'] = false;
            $result['headerCreated'] = $headerCreated;
            $result['message'] = 'קרתה תקלה ביצירת טופס החזרה (heade)';
            echo json_encode($result);
            die();
        }

        // add items
        $itemsCreated = $this->insertRMAItemsFromSite($headerCreated['externalid'], $rmaItems);
        
        if(!$itemsCreated){
            $result['status'] = false;
            $result['itemsCreated'] = $itemsCreated;
            $result['message'] = 'קרתה תקלה ביצירת טופס החזרה (items)';
            echo json_encode($result);
            die();
        }


        // sync to ERP
        $NetSuiteApiPRO = new NetSuiteApiPRO();
        $rmaSynced = $NetSuiteApiPRO->newRMA_SA($userData, $rmaItems, $entity, $headerCreated['externalid'], $rmaType);

        if(!$rmaSynced['status']){
            $result['status'] = false;
            $result['itemsCreated'] = $itemsCreated;
            $result['message'] = 'לא ניתן לסנכרן טופס החזרה. ' . $rmaSynced['error'];
            echo json_encode($result);
            die();
        }

        // update erp rma internalid on site
        $recoredupdated = $this->updateRMAInternalid($headerCreated['externalid'], $rmaSynced['createdRecordid']);

        // return response
        $result['status'] = true;
        $result['rmaSynced'] = $rmaSynced;
        $result['recoredupdated'] = $recoredupdated;
        echo json_encode($result);
        die();
    }


    /** 
     * Description: insertRMAHeadFromSite
     * @param
     * @return
     */
    private function insertRMAHeadFromSite($userData, $rmaType){
        global $wpdb;
        $dbRMAHead = PRO_SITE_PREFIX . "rma_head";

        $externalid = 'SRMA#' . time();
        $createDate = current_time( "Y-m-d H:i:s", 0 );

        $query = "INSERT INTO $dbRMAHead
			(
                `ID`,
                `externalid`,
                `internalid`,
                `tranid`,
                `statusref`,
                `memo`, 
                `custbody_rmatype`,
                `custbody_refundapproved`,
                `custbody_shippingtracknumber`,
                `user_id`,
                `entity`,
                `vatregnumber`,
                `companyname`,
                `contactname`,
                `email`,
                `phone`,
                `create_date`,
                `datecreated`,
                `synced`
            ) VALUES (
                NULL,
                '". $externalid ."',
                NULL,
                NULL,
                NULL,
                NULL,
                '". $rmaType ."',
                NULL,
                NULL,
                '". $userData['user_id'] ."',
                '". $userData['entity'] ."',
                '". $userData['vatregnumber'] ."',
                '". $userData['companyname'] ."',
                '". $userData['contactname'] ."',
                '". $userData['email'] ."',
                '". $userData['phone'] ."',
                '". $createDate."',
                NULL,
                0                
            )";

        $res = $wpdb->query($query);

        if($res == 1){
            return array(
                'res' => $res,
                'externalid' => $externalid
            );
        } else {
            return false;
        }
        
    }

    /** 
     * Description: insertRMAItemsFromSite
     * @param
     * @return
     */
    private function insertRMAItemsFromSite($externalid, $rmaItems){
        global $wpdb;
        $dbRMAItems = PRO_SITE_PREFIX . "rma_items";

        $createDate = current_time( "Y-m-d H:i:s", 0 );

        $query = "INSERT INTO $dbRMAItems
			(
                `ID`,
                `externalid`,
                `internalid`,
                `tranid`,
                `item`,
                `custcol_rmaclassification`,
                `custcol_invoicereference`,
                `quantity`
            ) VALUES ";
        
        for ($i = 0; $i < count($rmaItems); $i++){
            if($i > 0) $query .= ",";

            $query .= "(
                NULL,
                '". $externalid ."',
                NULL,
                NULL,
                '". $rmaItems[$i]['product_internalid'] ."',
                '". $rmaItems[$i]['type'] ."',
                '". $rmaItems[$i]['invInternalid'] ."',
                '". $rmaItems[$i]['amount_2_pick'] ."'
            )";
        }
        $res = $wpdb->query($query);
        
        if($res >= 1){
            return true;
        } else {
            return false;
        }
    }


    /** 
     * Description: updateRMAInternalid
     * @param
     * @return
     */
    private function updateRMAInternalid($externalid, $internalid){
        global $wpdb;
        $dbRMAHead = PRO_SITE_PREFIX . "rma_head";
        
        $query = "UPDATE $dbRMAHead
            SET synced = '1', 
                internalid = '$internalid'
            WHERE externalid = '$externalid'";
        return $wpdb->query($query);
    }
}