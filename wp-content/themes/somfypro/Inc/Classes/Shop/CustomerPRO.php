<?php
/**
 * @package  SomfySiteManager
 */

namespace Inc\Classes\Shop;

use Inc\Classes\Shop\NetSuiteApiPRO;
use Inc\Classes\SomfyMessaging;

class CustomerPRO{

    function __construct(){
   
    }

    public function new_registration_request($request){
        if ( !wp_verify_nonce( $_REQUEST['nonce'], NONCE_SECRET)) {
            exit("No naughty business please");
         }  

        //  validate params
        $companyname = isset($_REQUEST['companyname']) ? $_REQUEST['companyname'] : null;
        $city = isset($_REQUEST['city']) ? $_REQUEST['city'] : null;
        $companyid = isset($_REQUEST['companyid']) ? $_REQUEST['companyid'] : null;
        $contact_name = isset($_REQUEST['contact_name']) ? $_REQUEST['contact_name'] : null;
        $customerEmail = isset($_REQUEST['customerEmail']) ? $_REQUEST['customerEmail'] : null;
        $activitytype = isset($_REQUEST['activitytype']) ? $_REQUEST['activitytype'] : null;
        $phone = isset($_REQUEST['phone']) ? $_REQUEST['phone'] : null;
        
        
        if(is_null($companyname) || is_null($city) || is_null($companyid)
            || is_null($contact_name) || is_null($customerEmail) || is_null($activitytype)
            || is_null($phone)){
            $result['status'] = false;
            $result['message'] = 'חלק מהנתונים חסרים';
            echo json_encode($result);
            die();
        }

        // send email to somfy
        $SomfyMessaging = new SomfyMessaging();

        // send data via mail
        $registration_request_somfy_email__mailto = get_option('registration_request_somfy_email__to');
        $registration_request_somfy_email__subject = get_option('registration_request_somfy_email__subject');
        
        $template = get_option('registration_request_somfy_email__template');
        $registration_request_somfy_email__content = $SomfyMessaging->getTemplate($template);
        
        $registration_request_customer_email__subject = get_option('registration_request_customer_email__subject');
        
        $template = get_option('registration_request_customer_email__template');
        $registration_request_customer_email__content = $SomfyMessaging->getTemplate($template);


        // send to customer
        $sendTo = $customerEmail;
        $subject = $registration_request_customer_email__subject;
        $htmlBody = $registration_request_customer_email__content;
        $data = array(
            'companyname' => $companyname
        );

        $customerMessageSent = $SomfyMessaging->sendMail($sendTo, $subject, $htmlBody, $data);

        // send to somfy
        $sendTo = $registration_request_somfy_email__mailto;
        $subject = $registration_request_somfy_email__subject;
        $htmlBody = $registration_request_somfy_email__content;
        $data = array(
            'companyname' => $companyname,
            'city' => $city,
            'companyid' => $companyid,
            'contact_name' => $contact_name,
            'customerEmail' => $customerEmail,
            'phone' => $phone,
            'activitytype' => $activitytype
        );

        $somfyMessageSent = $SomfyMessaging->sendMail($sendTo, $subject, $htmlBody, $data);


        $result['status'] = true;
        $result['customerMessageSent'] = $customerMessageSent;
        $result['somfyMessageSent'] = $somfyMessageSent;
        echo json_encode($result);
        die();
    }
        
    
    public function new_account_update_request($request){
        if ( !wp_verify_nonce( $_REQUEST['nonce'], NONCE_SECRET)) {
            exit("No naughty business please");
        }  

        //  validate params
        $companyname = isset($_REQUEST['companyname']) ? $_REQUEST['companyname'] : null;
        $customerEmail = isset($_REQUEST['customerEmail']) ? $_REQUEST['customerEmail'] : null;
        $message = isset($_REQUEST['message']) ? $_REQUEST['message'] : null;
        $phone = isset($_REQUEST['phone']) ? $_REQUEST['phone'] : null;
        
        
        if(is_null($companyname) || is_null($customerEmail) || is_null($message) || is_null($phone)){
            $result['status'] = false;
            $result['message'] = 'חלק מהנתונים חסרים';
            echo json_encode($result);
            die();
        }

        // validate logged user
        $userID = get_current_user_id();

        if(!$userID){
            $result['status'] = false;
            $result['user_logged'] = false;
            $result['message'] = 'משתמש לא מחובר';
            echo json_encode($result);
            die();
        }

        $SomfyMessaging = new SomfyMessaging();

        // send data via mail
        $update_account_request_somfy_email__mailto = get_option('update_account_request_somfy_email__to');
        $update_account_request_somfy_email__subject = get_option('update_account_request_somfy_email__subject');
        
        $template = get_option('update_account_request_somfy_email__template');
        $update_account_request_somfy_email__content = $SomfyMessaging->getTemplate($template);
        
        $update_account_request_customer_email__subject = get_option('update_account_request_customer_email__subject');
        
        $template = get_option('update_account_request_customer_email__template');
        $update_account_request_customer_email__content = $SomfyMessaging->getTemplate($template);

        // send email to somfy

        // send to customer
        $sendTo = $customerEmail;
        $subject = $update_account_request_customer_email__subject;
        $htmlBody = $update_account_request_customer_email__content;
        
        $data = array(
            'companyname' => $companyname,
            'message' => $message,
            'phone' => $phone
        );

        $customerMessageSent = $SomfyMessaging->sendMail($sendTo, $subject, $htmlBody, $data);

        // send to somfy
        $sendTo = $update_account_request_somfy_email__mailto;
        $subject = $update_account_request_somfy_email__subject;
        $htmlBody = $update_account_request_somfy_email__content;
        $data = array(
            'companyname' => $companyname,
            'customerEmail' => $customerEmail,
            'message' => $message,
            'phone' => $phone
        );

        $somfyMessageSent = $SomfyMessaging->sendMail($sendTo, $subject, $htmlBody, $data);


        $result['status'] = true;
        $result['customerMessageSent'] = $customerMessageSent;
        $result['somfyMessageSent'] = $somfyMessageSent;
        echo json_encode($result);
        die();
    }

    public function new_contact_form_submit($request){
        if ( !wp_verify_nonce( $_REQUEST['nonce'], NONCE_SECRET)) {
            exit("No naughty business please");
        }  

        //  validate params
        $subsub = isset($_REQUEST['subsub']) ? $_REQUEST['subsub'] : null;
        $department = isset($_REQUEST['department']) ? $_REQUEST['department'] : null;
        $departmentName = isset($_REQUEST['departmentName']) ? $_REQUEST['departmentName'] : null;
        $description = isset($_REQUEST['description']) ? $_REQUEST['description'] : null;
        
        
        if(is_null($subsub) || is_null($department) || is_null($departmentName) || is_null($description)){
            $result['status'] = false;
            $result['message'] = 'חלק מהנתונים חסרים';
            echo json_encode($result);
            die();
        }

        // validate logged user
        $userID = get_current_user_id();

        if(!$userID){
            $result['status'] = false;
            $result['user_logged'] = false;
            $result['message'] = 'משתמש לא מחובר';
            echo json_encode($result);
            die();
        }

        // send to somfy

        global $current_user;
        wp_get_current_user();
        $SomfyMessaging = new SomfyMessaging();

        $contact_form_email__subject = get_option('contact_form_email__subject');
        $template = get_option('contact_form_email__template');
        $contact_form_email__content = $SomfyMessaging->getTemplate($template);

        $sendTo = $department;
        $subject = $contact_form_email__subject;
        $htmlBody = $contact_form_email__content;
        
        $data = array(
            'companyname' => get_field('companyname', 'user_'.$userID),
            'user_email' => $current_user->user_email,
            'message' => $description,
            'departmentName' => $departmentName,
            'subsub' => $subsub,
            'custentity_il_mobile' => isset(get_user_meta($userID, 'custentity_il_mobile')[0]) ? get_user_meta($userID, 'custentity_il_mobile')[0] : '',
            'phone' => isset(get_user_meta($userID, 'phone')[0]) ? get_user_meta($userID, 'phone')[0] : '',
        );

        $somfyMessageSent = $SomfyMessaging->sendMail($sendTo, $subject, $htmlBody, $data);


        if($somfyMessageSent[0]['status'] == 'sent'){
            $result['status'] = true;
        } else {
            $result['status'] = false;
            $result['message'] = 'לא ניתן לשלוח הודעה';
        }
        $result['somfyMessageSent'] = $somfyMessageSent;
        echo json_encode($result);
        die();
    }

    public function expert_signup($request){
        if ( !wp_verify_nonce( $_REQUEST['nonce'], NONCE_SECRET)) {
            exit("No naughty business please");
        }  

        // validate logged user
        $userID = get_current_user_id();

        if(!$userID){
            $result['status'] = false;
            $result['user_logged'] = false;
            $result['message'] = 'משתמש לא מחובר';
            echo json_encode($result);
            die();
        }

        global $current_user;
        wp_get_current_user();
        
        $userData = array(
            'user_email' => $current_user->user_email,
            'first_name' => isset(get_user_meta($userID, 'first_name')[0]) ? get_user_meta($userID, 'first_name')[0] : '',
            'last_name' => isset(get_user_meta($userID, 'last_name')[0]) ? get_user_meta($userID, 'last_name')[0] : '',
            'companyname' => isset(get_user_meta($userID, 'companyname')[0]) ? get_user_meta($userID, 'companyname')[0] : '',
            'phone' => isset(get_user_meta($userID, 'phone')[0]) ? get_user_meta($userID, 'phone')[0] : '',
            'custentity_il_mobile' => isset(get_user_meta($userID, 'custentity_il_mobile')[0]) ? get_user_meta($userID, 'custentity_il_mobile')[0] : '',
            'vatregnumber' => isset(get_user_meta($userID, 'vatregnumber')[0]) ? get_user_meta($userID, 'vatregnumber')[0] : ''
        );

        // send data via mail
        $expert_signup_email__mailto = get_option('expert_signup_email__to');
        $expert_signup_email__subject = get_option('expert_signup_email__subject');
        $template = get_option('expert_signup_email__template');
        $expert_signup_email__content = $SomfyMessaging->getTemplate($template);

        // send email to somfy

        // send to customer
        $sendTo = $expert_signup_email__mailto;
        $subject = $expert_signup_email__subject;
        $htmlBody = $expert_signup_email__content;
       
        $SomfyMessaging = new SomfyMessaging();
        $messageSent = $SomfyMessaging->sendMail($sendTo, $subject, $htmlBody, $userData);

        if($somfyMessageSent[0]['status'] == 'sent'){
            $result['status'] = true;
        } else {
            $result['status'] = false;
            $result['message'] = 'לא ניתן לשלוח הודעה';
        }
        $result['messageSent'] = $messageSent;
        echo json_encode($result);
        die();
    }



    public function new_event_registration($request){
        if ( !wp_verify_nonce( $_REQUEST['nonce'], NONCE_SECRET)) {
            exit("No naughty business please");
        }  

        //  validate params
        $eventID = isset($_REQUEST['eventID']) ? $_REQUEST['eventID'] : null;
        $contact_name = isset($_REQUEST['contact_name']) ? $_REQUEST['contact_name'] : null;
        $user_email = isset($_REQUEST['user_email']) ? $_REQUEST['user_email'] : null;
        $phone = isset($_REQUEST['phone']) ? $_REQUEST['phone'] : null;
        $participants = isset($_REQUEST['participants']) ? $_REQUEST['participants'] : null;
        $accept_terms = isset($_REQUEST['accept_terms']) ? $_REQUEST['accept_terms'] : null;
        
        
        if(is_null($eventID) || is_null($contact_name) || is_null($user_email) || is_null($phone)
            || is_null($participants) || is_null($accept_terms)){
            $result['status'] = false;
            $result['message'] = 'חלק מהנתונים חסרים';
            echo json_encode($result);
            die();
        }

        $SomfyMessaging = new SomfyMessaging();
        $is_user_exists =  email_exists($user_email);

        $data = array(
            'eventID' => $eventID,
            'event_name' => get_the_title($eventID),
            'event_date' => get_field('seminar_date', $eventID),
            'event_location' => get_field('seminar_place', $eventID),
            'event_teacher' => get_field('seminar_teacher', $eventID),
            'contact_name' => $contact_name,
            'user_email' => $user_email,
            'phone' => $phone,
            'participants' => $participants,
            'accept_terms' => $accept_terms == 'true' ? 'כן' : 'לא',
            'is_user_exists' => $is_user_exists ? 'כן' : 'לא'
        );
        
        // send data via mail
        $template = get_option('new_event_somfy_email__template');
        $new_event_somfy_email__content = $SomfyMessaging->getTemplate($template);

        // send to somfy
        $sendTo = get_option('new_event_somfy_email__to');
        $subject = get_option('new_event_somfy_email__subject');
        $htmlBody = $new_event_somfy_email__content;
        
        $somfyMessageSent = $SomfyMessaging->sendMail($sendTo, $subject, $htmlBody, $data);

        // send to customer
        $subject = get_option('new_event_customer_email__subject');
        $template = get_option('new_event_customer_email__template');
        $new_event_customer_email__content = $SomfyMessaging->getTemplate($template);

        $customersendTo = $data['user_email'];
        $customerhtmlBody = $new_event_customer_email__content;
        $customerMessageSent = $SomfyMessaging->sendMail($customersendTo, $subject, $customerhtmlBody, $data);


        if(isset($somfyMessageSent[0]['status']) && $somfyMessageSent[0]['status'] == 'sent' && isset($customerMessageSent[0]['status']) && $customerMessageSent[0]['status'] == 'sent'){
            $result['status'] = true;
        } else {
            $result['status'] = false;
            $result['message'] = 'לא ניתן לשלוח הודעה';
        }
        $result['somfyMessageSent'] = $somfyMessageSent;
        $result['customerMessageSent'] = $customerMessageSent;
        echo json_encode($result);
        die();
    }


    /**
     * Returns: 
     * userInternalid
     * cust_radiomodel_qualified
     * cust_hipro_qualified
     */
    public function getUserInfo($userID){
        $result = array();

        $result['userInternalid'] = get_field('internalid', 'user_'.$userID);
        $result['cust_radiomodel_qualified'] = $this->isQualifiedForRadioDiscount($userID);
        $result['cust_hipro_qualified'] = $this->isQualifiedForHiproModel($userID);

        return $result;
    }


    public function getUserByInternalid($internalid){
        $my_user = new \WP_User_Query(
            array(
                'posts_per_page' => 1,
                'meta_query' => array(
                    array(
                        'key'     => 'internalid',
                        'value'   => $internalid,
                        'compare' => '='
                    )
                )
            ));
        return $my_user->get_results()[0];
    } 

 

    public function getCustomerTaxonomy(){
        $userID = get_current_user_id();

        $userCategories = get_field('custitem_somfy_web_product_category', 'user_'.$userID);
        $userSubproduct = get_field('custitem_sub_product_group', 'user_'.$userID);
        $user2Subproduct = get_field('custitem_sub2productgroup', 'user_'.$userID);

        $results = array(
            'categories' => array(),
            'sub_group' => array(),
            'sub2group' => array()
        );

        if($userCategories){
            foreach($userCategories as $cat){
                array_push($results['categories'], $cat->ID);
            }
        }
        

        if($userSubproduct){
            foreach($userSubproduct as $cat){
                array_push($results['sub_group'], $cat->ID);
            }
        }

        if($user2Subproduct){
            foreach($user2Subproduct as $cat){
                array_push($results['sub2group'], $cat->ID);
            }
        }

        return $results;
    }

/** 
     * Description: getCustomerCategories
     * @param
     * @return 
     */
    public function syncCustomerCategories($internalID, $userID){
        global $wpdb;

        $dbDiscountcode = EU_SITE_PREFIX . "discountcodes";
        $dbPosts = PRO_SITE_PREFIX . "posts";
        $dbPostmeta = PRO_SITE_PREFIX . "postmeta";

        $sql = "SELECT 
                DISTINCT
                c.category
                
            FROM $dbDiscountcode dc
            JOIN (
                SELECT DISTINCT
                    pmd.meta_value AS discountcode,
                    pmc.meta_value AS category
                FROM $dbPosts p
                JOIN $dbPostmeta pmd
                    ON pmd.post_id = p.ID AND pmd.meta_key = 'custitem_discountcode'
            
                JOIN $dbPostmeta pmc
                    ON pmc.post_id = p.ID AND pmc.meta_key = 'custitem_somfy_web_product_category'
                WHERE p.post_type = 'product' AND p.post_status = 'publish'
            ) c ON c.discountcode = dc.custrecord_discountcode
            WHERE dc.internalid = '$internalID'
        ";

        $res = $wpdb->get_results($sql);
        $fa = array();

        if($res){
            foreach($res as $r){
                $cat = $r->category;
                if(is_serialized($cat)){
                    $sCat = maybe_unserialize($cat);
                    foreach($sCat as $c){
                        array_push($fa, $c);
                    }
                } else {
                    array_push($fa, $cat);
                }
            }
        }

        // add categories that open to all
        $openCat = explode(',', str_replace(' ', '', get_option('shop_show_tax__category')));
        $f = array_merge($fa, $openCat);

        return update_field('custitem_somfy_web_product_category', array_unique($f), 'user_'.$userID);
    }

    /** 
     * Description: getCustomerSubgroups
     * @param
     * @return 
     */
    public function syncCustomerSubgroups($internalID, $userID){
        global $wpdb;

        $dbDiscountcode = EU_SITE_PREFIX . "discountcodes";
        $dbPosts = PRO_SITE_PREFIX . "posts";
        $dbPostmeta = PRO_SITE_PREFIX . "postmeta";

        $sql = "SELECT 
                DISTINCT
                c.category
                
            FROM $dbDiscountcode dc
            JOIN (
                SELECT DISTINCT
                    pmd.meta_value AS discountcode,
                    pmc.meta_value AS category
                FROM $dbPosts p
                JOIN $dbPostmeta pmd
                    ON pmd.post_id = p.ID AND pmd.meta_key = 'custitem_discountcode'
            
                JOIN $dbPostmeta pmc
                    ON pmc.post_id = p.ID AND pmc.meta_key = 'custitem_sub_product_group'	
                WHERE p.post_type = 'product' AND p.post_status = 'publish'
            ) c ON c.discountcode = dc.custrecord_discountcode
            WHERE dc.internalid = '$internalID'
        ";

        $res = $wpdb->get_results($sql);
        $fa = array();

        if($res){
            foreach($res as $r){
                $cat = $r->category;
                if(is_serialized($cat)){
                    $sCat = maybe_unserialize($cat);
                    foreach($sCat as $c){
                        array_push($fa, $c);
                    }
                } else {
                    array_push($fa, $cat);
                }
            }
        }

        // add categories that open to all
        $openGroup = explode(',', str_replace(' ', '', get_option('shop_show_tax__subgroup')));
        $f = array_merge($fa, $openGroup);

        return update_field('custitem_sub_product_group', array_unique($f), 'user_'.$userID);
    }

    /** 
     * Description: getCustomerSubgroups
     * @param
     * @return 
     */
    public function syncCustomer2Subgroups($internalID, $userID){
        global $wpdb;

        $dbDiscountcode = EU_SITE_PREFIX . "discountcodes";
        $dbPosts = PRO_SITE_PREFIX . "posts";
        $dbPostmeta = PRO_SITE_PREFIX . "postmeta";

        $sql = "SELECT 
                DISTINCT
                c.category
                
            FROM $dbDiscountcode dc
            JOIN (
                SELECT DISTINCT
                    pmd.meta_value AS discountcode,
                    pmc.meta_value AS category
                FROM $dbPosts p
                JOIN $dbPostmeta pmd
                    ON pmd.post_id = p.ID AND pmd.meta_key = 'custitem_discountcode'
            
                JOIN $dbPostmeta pmc
                    ON pmc.post_id = p.ID AND pmc.meta_key = 'custitem_sub2productgroup'	
                WHERE p.post_type = 'product' AND p.post_status = 'publish'
            ) c ON c.discountcode = dc.custrecord_discountcode
            WHERE dc.internalid = '$internalID'
        ";

        $res = $wpdb->get_results($sql);
        $fa = array();

        if($res){
            foreach($res as $r){
                $cat = $r->category;
                if(is_serialized($cat)){
                    $sCat = maybe_unserialize($cat);
                    foreach($sCat as $c){
                        array_push($fa, $c);
                    }
                } else {
                    array_push($fa, $cat);
                }
            }
        }
        
        // add categories that open to all
        $openSubGroup = explode(',', str_replace(' ', '', get_option('shop_show_tax__subsubgroup')));
        $f = array_merge($fa, $openSubGroup);

        return update_field('custitem_sub2productgroup', array_unique($f), 'user_'.$userID);
    }


    public function isQualifiedForRadioDiscount($userID){
        // check if user is qualified for radio discount
        $radiomodel = get_field('custentity_radiomodel', 'user_'.$userID, false);
        
        if($radiomodel != '-1'){
            $radioModelValidationDate = get_field('custentity_radiomodelvalidationdate', 'user_'.$userID);
            $isRadioModelValid = $this->validateRadioModaleValidationDate($radioModelValidationDate);

            if($isRadioModelValid){
                $cust_in_radiomodel = true;

                // get remote product and price
                $radioModel_remoteitem = get_field('custentity_remoteitem', 'user_'.$userID);
                $radioModel_remoteprice = get_field('custentity_remoteprice', 'user_'.$userID);

                return array(
                    'status' => true,
                    'cust_radiomodel' => $radiomodel,
                    'remoteitem' => $radioModel_remoteitem,
                    'remoteprice' => intval($radioModel_remoteprice)
                );
            } else {
                return array('status' => false);
            }
        } else {
            return array('status' => false);
        }
    }

    /** 
     * Description: validateRadioModaleValidationDate
     * @param
     * @return
     */
    public function validateRadioModaleValidationDate($radioModelValidationDate){
        if(!$radioModelValidationDate || $radioModelValidationDate == '') return false;

        $array = explode('/', $radioModelValidationDate);
        $tmp = $array[1] .'/'. $array[0] . '/'. $array[2];
        
        $date_now = new \DateTime();
        $date2    = new \DateTime($tmp);
    
        if ($date_now < $date2) {
            return true;
        }else{
            return false;
        }
    }



    public function isQualifiedForHiproModel($userID){
        // check if user is qualified for radio discount
        $hiproModel = get_field('custentity_hipro12model', 'user_'.$userID, false);
        if($hiproModel){
            return array('status' => true);
        } else {
            return array('status' => false);
        }
    }
}