<?php
/**
 * @package SomfyPRO
 */

namespace Inc\Classes\Shop;

class CalculatorPRO {
    

    public function saveCalcInputs($orderID, $userID, $calcInputs){
        global $wpdb;
        $calc_db = PRO_SITE_PREFIX . "calculator_inputs";
        $createDate = current_time( "Y-m-d H:i:s", 0 );

		$query = "INSERT INTO $calc_db
			(
                `ID`, 
                `orderID`, 
                `userID`, 
                `productID`, 
                `userInput`, 
                `create_date`
            ) VALUES ";

        for($i = 0; $i < count($calcInputs); $i++){
            $row = $calcInputs[$i];
            $userInput = json_encode($row['calc_userInput']);
            $query .= " (
                NULL,
				'".$orderID."',
				'".$userID."',
				'".$row['productID']."',
				'".$userInput."',
                '". $createDate."'
            ) ";

            if($i < count($calcInputs) - 1) $query .= " ,";
        }
        return $wpdb->query($query);
    }
}