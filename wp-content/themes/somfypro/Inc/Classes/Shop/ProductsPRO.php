<?php
/**
 * @package SomfyPRO
 */

namespace Inc\Classes\Shop;

use Inc\Classes\Shop\CustomerPRO;
use Inc\Classes\Shop\NetSuiteApiPRO;
use Inc\Classes\Shop\ProductPricingPRO;

class ProductsPRO {
    public $dbPosts;
    public $dbPostmeta;
    public $dbStockNotification;
    public $dbProductPricing;
 
    function __construct(){
        global $wpdb;
        $this->dbPosts = PRO_SITE_PREFIX . "posts";
        $this->dbPostmeta = PRO_SITE_PREFIX . "postmeta";
        $this->dbDiscountcodes = EU_SITE_PREFIX . "discountcodes";
        $this->dbStockNotification = PRO_SITE_PREFIX . "stock_notification";
        $this->dbProductPricing = PRO_SITE_PREFIX . "product_pricing";

        $this->dbUsers = EU_SITE_PREFIX . "users";
        $this->dbUsermeta = EU_SITE_PREFIX . "usermeta";
        $this->dbProductIndex = PRO_SITE_PREFIX . "product_index_full";
        $this->dbProductTitleIndex = PRO_SITE_PREFIX . "product_title_index";


        $this->templateURL = get_template_directory_uri();

        $this->ProductPricing = new ProductPricingPRO();
        $this->netSuiteApi = new NetSuiteApiPRO();
        $this->CustomerPRO = new CustomerPRO();


        $json_a = file_get_contents(get_template_directory() . '/Inc/Classes/Shop/hipro12model.json');
        $this->hipro12model_matrix = json_decode($json_a, true);
    }


    /** 
     * Description: getProductByID with lots of filters
     * @param productID
     * @return products array 
     */
    public function getProductByID($request){
        if ( !wp_verify_nonce( $_REQUEST['nonce'], NONCE_SECRET)) {
            exit("No naughty business please");
        } 
        
        //  validate params
        $productID = isset($_REQUEST['productID']) ? $_REQUEST['productID'] : null;

        if(!isset($productID) || $productID == ''){
            $result['status'] = false;
            $result['message'] = 'חלק מהנתונים חסרים';
            echo json_encode($result);
            die();
        }

        // validate logged user
        $userID = get_current_user_id();

        if(!$userID){
            $result['status'] = false;
            $result['message'] = 'משתמש לא מחובר';
            echo json_encode($result);
            die();
        }


        $LoggedUser = $this->CustomerPRO->getUserInfo($userID);

        $userInternalid = $LoggedUser['userInternalid'];
        
        // check for hipro qualification
        $cust_hipro_qualified = $LoggedUser['cust_hipro_qualified'];
        
        // chech for radio model qualifications
        $cust_radiomodel_qualified = $LoggedUser['cust_radiomodel_qualified'];
        $result['cust_radiomodel_qualified'] = $cust_radiomodel_qualified;

        if($cust_radiomodel_qualified['status']){
            // get remote item data
            $result['remoteitem'] = $this->getRemoteData($cust_radiomodel_qualified['remoteitem'], $cust_radiomodel_qualified['remoteprice'], $userInternalid);
        }
         

        $short_desc = htmlspecialchars_decode(get_field('storedescription', $productID));
        
        $productData = array(
            'productID' => $productID,
            'internalid' => get_field('internalid', $productID),
            'itemid' => get_field('itemid', $productID),
            'title' => get_the_title($productID),
            'thumbnail_url' => get_products_files($productID, 'thumbnail')['url'],
            'permalink' => get_the_permalink($productID),
            'warranty_applicable' => $this->checkIfWarrantyReleated($productID),
            'on_sell' => get_field('on_sell', $productID),
            'short_desc' => $short_desc
        );

        // Prices
        // check Radio model discount
        $productData['radiomodel_qualified'] = false;

        if($cust_radiomodel_qualified['status']){
            $prod_radiomodel_qualified = $this->isProductQualifiedForRadioDiscount($productID, $userInternalid, $cust_radiomodel_qualified['cust_radiomodel']);

            if($prod_radiomodel_qualified['status']){
                $productData['radiomodel_qualified'] = true;
                $pricelist = $prod_radiomodel_qualified['pricelist'];
            } else {
                // check hipro discount
                if($cust_hipro_qualified['status']){
                    $pricelist = $this->isProductQualifiedForHipro($productID, $productData['internalid'], $userInternalid)['pricelist'];
                } else {
                    $pricelist = $this->getProductPricesList($productData['internalid'], $userInternalid, $productID);
                }
            }
        } else {
            $pricelist = $this->getProductPricesList($productData['internalid'], $userInternalid, $productID);
        }


        $productData['base_price'] = $pricelist['base_price'];
        $productData['base_price_after_discountcode'] = $pricelist['base_price_after_discountcode'];


        if($productData['on_sell']){
            $productData['discount_price'] = floatval(get_field('discount_price', $productID));
            $productData['discount_percentage'] = get_field('discount_percentage', $productID);
            $rate = get_option('shop_rate');
            $productData['base_price_after_discountcode'] = number_format($productData['discount_price'] * floatval(1 + $rate/100), 2);
        } else {
            $productData['discount_percentage'] = '';
            $productData['discount_price'] = '';
        }

        $result['status'] = true;
        $result['productData'] = $productData;
        echo json_encode($result);
        die();
    }

    /** 
     * Description: getShopProducts with lots of filters
     * @param 
     *  $limit - pagination
     *  $page - pagination
     * @return products array 
     *  
     */
    public function getShopProducts($request){
        global $wpdb;
        if ( !wp_verify_nonce( $_REQUEST['nonce'], NONCE_SECRET)) {
            exit("No naughty business please");
        }  

        
        //  validate params
        $advancedFilters = isset($_REQUEST['advancedFilters']) ? $_REQUEST['advancedFilters'] : null;
        $sortFilter = isset($_REQUEST['sortFilter']) ? $_REQUEST['sortFilter'] : null;
        $paginators = isset($_REQUEST['paginators']) ? $_REQUEST['paginators'] : null;
        $filtersPageId = isset($_REQUEST['filtersPageId']) ? $_REQUEST['filtersPageId'] : null;
        
        $comingFromCalculator = isset($_REQUEST['comingFromCalculator']) ? $_REQUEST['comingFromCalculator'] : null;
        $calcNameQuery = isset($_REQUEST['calcNameQuery']) ? $_REQUEST['calcNameQuery'] : null;
        
        // dont include internal products
        $warrantyProductID = get_option('shop_warranty_product_id');
        $shippingProductID = get_option('shop_shipping_product_id');
        $internalProducts = "('$warrantyProductID', '$shippingProductID')";

        if(!isset($paginators) || !isset($filtersPageId)){
            $result['status'] = false;
            $result['message'] = 'חלק מהנתונים חסרים';
            echo json_encode($result);
            die();
        }



        // validate logged user
        $userID = get_current_user_id();

        if(!$userID){
            $result['status'] = false;
            $result['message'] = 'משתמש לא מחובר';
            echo json_encode($result);
            die();
        }


        
        $LoggedUser = $this->CustomerPRO->getUserInfo($userID);
        $userInternalid = $LoggedUser['userInternalid'];
        
        // check for hipro qualification
        $cust_hipro_qualified = $LoggedUser['cust_hipro_qualified'];
        
        // chech for radio model qualifications
        $cust_radiomodel_qualified = $LoggedUser['cust_radiomodel_qualified'];
        $result['cust_radiomodel_qualified'] = $cust_radiomodel_qualified;

        if($cust_radiomodel_qualified['status']){
            // get remote item data
            $result['remoteitem'] = $this->getRemoteData($cust_radiomodel_qualified['remoteitem'], $cust_radiomodel_qualified['remoteprice'], $userInternalid);
        }

        
        // // set limit and offset
        $limit = intval($paginators['limit']);
        $page = intval($paginators['page']);

        $startAfter = ($page - 1) * $limit;


        // if comingFromCalculator add name query to the mix
        $calcNameWhereQuery = '';
        if($comingFromCalculator && $comingFromCalculator == 'true'){
            if(!$calcNameQuery || !is_array($calcNameQuery) || count($calcNameQuery) == 0){
                $result['status'] = false;
                $result['calcNameQuery'] = $calcNameQuery;
                $result['message'] = 'חלק מהנתונים חסרים למציאת תוצאות מחשבון';
                echo json_encode($result);
                die();
            }

            $calcNameWhereQuery .= " AND (";

            $relWhere = '';
            for ($i = 0; $i < count($calcNameQuery); $i++){
                if($i > 0) $relWhere .= ' OR ';

                $relWhere .= " (spi.post_title LIKE '%" .$calcNameQuery[$i] . '%\') ';
            }
            $calcNameWhereQuery .= $relWhere;
            $calcNameWhereQuery .= ") 
            ";
        }
        


        // // prep sort order filters
        $orderByQuery = '';
        $sortJoin = '';
        $priceFieldAdded = false;
        $priceCalcPriceQuery = '';
        $padcField = '';
        if(isset($sortFilter) && $sortFilter['sortType'] != '' && $sortFilter['sortOrder'] != ''){
            $sortOrder = $sortFilter['sortOrder'];
            $sortType = $sortFilter['sortType'];
            
            switch($sortType){
                case 'post_title':
                    $orderByQuery = " ORDER BY spi.post_title $sortOrder ";
                    break;
                case 'base_price' : 
                    $padcField = " spp.unitprice - spp.unitprice * ((1 * REPLACE(dc.custrecord_discount_percent, '%','')) / 100) as base_price_after_discountcode, ";
                    $orderByQuery = " ORDER BY spp.unitprice - spp.unitprice * ((1 * REPLACE(dc.custrecord_discount_percent, '%','')) / 100) $sortOrder ";
                    break;
                case 'on_sell' : 
                    $orderByQuery = " ORDER BY spi.on_sell $sortOrder ";
                    break;
            }
        }
        

        // // prep advancedFilters query
        $afWhere = '';
        $pageCategories = array();
        if(isset($advancedFilters) && count($advancedFilters) > 0){
            foreach ($advancedFilters as $af){
                $filterName = $af['filterName'];
                if(!isset($af['value']) && is_array($af['value']) && $af['value'][0] == '') continue;
                
                switch($af['filterType']){
                    case 'select':
                        $values = " ('".implode("','",$af['value'])."') ";
                        $afWhere .= " AND spi.$filterName IN $values 
                        ";
                        break;
                    case 'relationship':
                        $relWhere = '';
                        $relWhere = ' AND ( ';
                        for ($i = 0; $i < count($af['value']); $i++){
                            if($i > 0) $relWhere .= ' OR ';

                            $relWhere .= " (spi.$filterName LIKE '%s:" .strlen($af['value'][$i]) . ':"' . $af['value'][$i] . '"%\''
                                ." OR spi.$filterName = '" . $af['value'][$i] . "') ";
                            
                            if($filterName = "custitem_somfy_web_product_category") array_push($pageCategories, $af['value'][$i]);
                        }
                        $relWhere .= ' ) 
                        ';

                        $afWhere .= $relWhere;
                        break;
                    
                    case 'number':
                        $relWhere = '';
                        $compare_type = $af['compare_type'] != '' ? $af['compare_type'] : '=';
                        
                        if(!is_array($af['value'])){
                            $values = " ('".implode("','",$af['value'])."') ";
                        } else {
                            $values = $af['value'];
                        }

                        $relWhere .= ' AND ( ';
                        for ($i = 0; $i < count($af['value']); $i++){
                            if($i > 0) $relWhere .= ' OR ';

                            $relWhere .= " ( CAST(spi.$filterName AS CHAR) $compare_type " . intval($af['value'][$i]) . ") ";

                        }
                        $relWhere .= ' ) 
                        ';

                        $afWhere .= $relWhere;
                        break;

                    case 'range':
                        $min = $af['min'];
                        $max = $af['max'];
                        
                        $padcField = " spp.unitprice - spp.unitprice * ((1 * REPLACE(dc.custrecord_discount_percent, '%','')) / 100) as base_price_after_discountcode, ";
                        $afWhere .= " AND (spp.unitprice - spp.unitprice * ((1 * REPLACE(dc.custrecord_discount_percent, '%','')) / 100) >= cast('$min' as CHAR) AND spp.unitprice - spp.unitprice * ((1 * REPLACE(dc.custrecord_discount_percent, '%','')) / 100) <= cast('$max' as CHAR)) 
                        ";
                        break;
                    case "boolen":
                        $val = $af['value'] == 'true' ? 1 : 0;
                        $afWhere .= " AND spi.$filterName = $val 
                        ";
                        break;
                }
            }
        }


        // // FINAL QUERY
        $query = "SELECT 
                dc.custrecord_discountcode,
                dc.custrecord_discountgroup,
                REPLACE(dc.custrecord_discount_percent, '%','') as custrecord_discount_percent,
                spp.unitprice,
                $padcField
                spi.*
            FROM $this->dbUsers u
            JOIN $this->dbUsermeta um
                ON um.meta_key = 'internalid' AND um.user_id = u.ID
            JOIN $this->dbDiscountcodes dc
                ON dc.internalid = um.meta_value
                
            JOIN $this->dbProductIndex spi
                ON spi.custitem_discountcode = dc.custrecord_discountcode 
            
            join $this->dbProductPricing spp 
                on spp.item = spi.internalid 
                    and spp.customer = um.meta_value 
                    and spp.quantityrange = '1+' 
                    and spp.pricelevel = 1 
                    and spp.currency = 1 
                    and spp.unitprice > 0 
           
            
            WHERE u.ID = $userID AND spi.ID NOT IN $internalProducts
                $afWhere
                $calcNameWhereQuery

            $orderByQuery
        ";
        $result['query'] = $query;

        $res = $wpdb->get_results($query);
        

        $total_count = 0;
        $final_products_results = array();
        $final_filtersObjects = array();
        $rate = get_option('shop_rate');

        $fieldsForFilters = get_field('filters', $filtersPageId);
        $filtersObjects = $this->getProductFilters($fieldsForFilters);

        if($res){
            // filter user taxonomy

            $customerTaxonomy = $this->CustomerPRO->getCustomerTaxonomy();
            $userCategories = $customerTaxonomy['categories'];
            
            // show only releated groups and subgroups
            $subgroupsToShow = array();
            foreach($pageCategories as $c){
                $f = get_field('sub_groups', $c, false);
                if(!is_array($f)) $f = array();
                $subgroupsToShow = array_merge($subgroupsToShow , $f);
            }
            $userSubgroup = array_unique($subgroupsToShow);
            
            $sub2groupsToShow = array();
            foreach($userSubgroup as $c){
                $f = get_field('2_subgroups', $c, false);
                if(!is_array($f)) $f = array();
                $sub2groupsToShow = array_merge($sub2groupsToShow , $f);
            }
            $userSub2group = array_unique($sub2groupsToShow);
            
            // prep advancedFilter return obj
            for($i = 0; $i < count($res); $i++){
                $resID = $res[$i]->ID;

                // set advanced filters totals
                for($j = 0 ; $j < count($filtersObjects) ; $j++){
                    if($filtersObjects[$j]['type'] == "relationship"){
                        $productVal = get_field($filtersObjects[$j]['key'], $resID);
                        $filtersObjects[$j]['productVal'] = $productVal;
                        $haveChoices = 0;
                        
                        // filter user taxonomy
                        foreach($filtersObjects[$j]['choices'] as $choice){
                            switch($filtersObjects[$j]['post_type']){
                                case 'product_category':
                                    if(!in_array($choice['value'], $userCategories)){
                                        unset($filtersObjects[$j]['choices'][$choice['value']]);
                                    }
                                    break;
                                
                                case 'product_subgroup' : 
                                    if(!in_array($choice['value'], $userSubproduct)){
                                        unset($filtersObjects[$j]['choices'][$choice['value']]);
                                    }
                                    break;

                                case 'product_2_subgroup' : 
                                    if(!in_array($choice['value'], $userSub2group)){
                                        unset($filtersObjects[$j]['choices'][$choice['value']]);
                                    }
                                    break;
                            }
                        }

                        foreach($productVal as $product){
                            if(isset($filtersObjects[$j]['choices']["$product->ID"])){
                                $filtersObjects[$j]['choices']["$product->ID"]['results'] += 1;
                                $haveChoices++;
                            }
                        }
                        $filtersObjects[$j]['haveChoices'] = $haveChoices;
                    } else {

                        if($filtersObjects[$j]['type'] == "select"){
                            $pv = get_field($filtersObjects[$j]['key'], $resID);
                            $productVal = $pv != '' ? $pv['value'] : '-1';
                            $filtersObjects[$j]['productVal'] = $productVal;

                            $filtersObjects[$j]['choices']["$productVal"]['results'] += 1;
                            
                        }
                    }
                }
                $total_count++;
                
                
                // add final product data to FINAL RESULTS
                if(count($final_products_results) < $limit && $i >= $startAfter){

                    $res[$i]->permalink = get_the_permalink($resID);
                    $res[$i]->thumbnail_url = get_products_files($resID, 'thumbnail')['url'];
                    $res[$i]->warranty_applicable = $this->checkIfWarrantyReleated($resID);


                    // check Radio model discount
                    $res[$i]->radiomodel_qualified = false;

                    if($cust_radiomodel_qualified['status']){
                        $prod_radiomodel_qualified = $this->isProductQualifiedForRadioDiscount($resID, $userInternalid, $cust_radiomodel_qualified['cust_radiomodel']);

                        if($prod_radiomodel_qualified['status']){
                            $res[$i]->radiomodel_qualified = true;
                            $res[$i]->pricelist = $prod_radiomodel_qualified['pricelist'];
                        } else {
                            // check hipro discount
                            if($cust_hipro_qualified['status']){
                                $res[$i]->pricelist = $this->isProductQualifiedForHipro($resID, $res[$i]->internalid, $userInternalid)['pricelist'];
                            } else {
                                $res[$i]->pricelist = array(
                                    'base_price' => $res[$i]->unitprice,
                                    'discount_percent' => $res[$i]->custrecord_discount_percent,
                                    'base_price_after_discountcode' => round((
                                        floatval($res[$i]->unitprice) - floatval($res[$i]->unitprice) * ($res[$i]->custrecord_discount_percent / 100)),2)
                                );
                            }
                        }
                    } else {
                        $res[$i]->pricelist = array(
                            'base_price' => $res[$i]->unitprice,
                            'discount_percent' => $res[$i]->custrecord_discount_percent,
                            'base_price_after_discountcode' => round((
                                floatval($res[$i]->unitprice) - floatval($res[$i]->unitprice) * ($res[$i]->custrecord_discount_percent / 100)),2)
                        );
                    }


                    if(count($res[$i]->pricelist) == 0) {
                        $total_count--;
                        continue;
                    }

                    array_push($final_products_results, $res[$i]);
                }
            }

            $result['status'] = true;
            $result['products'] = $final_products_results;
            $result['filtersObjects'] = $filtersObjects;
            $result['total'] = $total_count;

            $result['sortFilter'] = $sortFilter;
            
            echo json_encode($result);
            die();
        } 
        
        $result['status'] = false;
        $result['res'] = $res;
        $result['message'] = 'לא נמצאו תוצאות';
        echo json_encode($result);
        die();        
    }



    /** 
     * Description: searchProducts for search Page
     * @param $term - search term
     *  $shortResults - for short results (for global serach results preivew)
     *  $limit - pagination
     *  $page - pagination
     * @return products array 
     *  if $shortResults = true - return without meta data
     */
    public function searchProducts($term, $filters, $limit = NULL, $page = NULL, $autoload = false){
        global $wpdb;

        // validate logged user
        $userID = get_current_user_id();
        $userInternalid = get_field('internalid', 'user_'.$userID);

        // chech for radio model qualifications
        $cust_radiomodel_qualified = $this->CustomerPRO->isQualifiedForRadioDiscount($userID);
        $cust_radiomodel_remoteitem = null;
        if($cust_radiomodel_qualified['status']){
            // get remote item data
            $cust_radiomodel_remoteitem = $this->getRemoteData($cust_radiomodel_qualified['remoteitem'], $cust_radiomodel_qualified['remoteprice'], $userInternalid);
        }

        // check for hipro qualification
        $cust_hipro_qualified = $this->CustomerPRO->isQualifiedForHiproModel($userID);
        

        // dont include internal products
        $warrantyProductID = get_option('shop_warranty_product_id');
        $shippingProductID = get_option('shop_shipping_product_id');
        $internalProducts = "('$warrantyProductID', '$shippingProductID')";

        // set limit and offset
        $offset = isset($page) && isset($limit) ? ($page - 1) * $limit : 0;
        
        if($autoload){
            $queryLimit = isset($limit) ? " LIMIT ".$limit : " ";
            $queryOffset = isset($page) && isset($limit) ? " OFFSET " .$offset : "";
        } else {
            $queryLimit = "";
            $queryOffset = "";
        }
        

        // Prep term and WHERE query
        $prepedTerm = str_replace(' ', '%', $term);

        $titleWhere = " spi.post_title LIKE '%$prepedTerm%'";
        $itemidWhere = "spi.itemid LIKE '%$prepedTerm%'";

        $whereQuery = " AND (($titleWhere) OR ($itemidWhere))";

        // prep order filters
        $orderByQuery = '';
        $sortJoin = '';
        $padcField = '';

        if(isset($sortFilter) && $sortFilter['sortType'] != '' && $sortFilter['sortOrder'] != ''){
            $sortOrder = $sortFilter['sortOrder'];
            $sortType = $sortFilter['sortType'];
            
            switch($sortType){
                case 'post_title':
                    $orderByQuery = " ORDER BY spi.post_title $sortOrder ";
                    break;
                case 'base_price' : 
                    $padcField = " spp.unitprice - spp.unitprice * ((1 * REPLACE(dc.custrecord_discount_percent, '%','')) / 100) as base_price_after_discountcode, ";
                    $orderByQuery = " ORDER BY spp.unitprice - spp.unitprice * ((1 * REPLACE(dc.custrecord_discount_percent, '%','')) / 100) $sortOrder ";
                    break;
                case 'on_sell' : 
                    $orderByQuery = " ORDER BY spi.on_sell $sortOrder ";
                    break;
            }
        }

        if($autoload){
            // // FINAL QUERY
            $query = "SELECT  *
                FROM $this->dbUsers u
                JOIN $this->dbUsermeta um
                    ON um.meta_key = 'internalid' AND um.user_id = u.ID
                JOIN $this->dbDiscountcodes dc
                    ON dc.internalid = um.meta_value
                    
                JOIN $this->dbProductTitleIndex spi
                    ON spi.custitem_discountcode = dc.custrecord_discountcode 
                
                WHERE u.ID = $userID AND spi.ID NOT IN $internalProducts
                    $whereQuery

                $queryLimit
                $queryOffset
                $orderByQuery
            ";
        } else {
            // // FINAL QUERY
            $query = "SELECT 
                    dc.custrecord_discountcode,
                    dc.custrecord_discountgroup,
                    REPLACE(dc.custrecord_discount_percent, '%','') as custrecord_discount_percent,
                    spp.unitprice,
                    $padcField
                    spi.*
                FROM $this->dbUsers u
                
                JOIN $this->dbUsermeta um
                    ON um.meta_key = 'internalid' AND um.user_id = u.ID
                JOIN $this->dbDiscountcodes dc
                    ON dc.internalid = um.meta_value

                JOIN $this->dbProductIndex spi
                    ON spi.custitem_discountcode = dc.custrecord_discountcode 

                join $this->dbProductPricing spp 
                    on spp.item = spi.internalid 
                        and spp.customer = um.meta_value 
                        and spp.quantityrange = '1+' 
                        and spp.pricelevel = 1 
                        and spp.currency = 1 
                        and spp.unitprice > 0 


                WHERE u.ID = $userID AND spi.ID NOT IN $internalProducts
                $whereQuery

                $orderByQuery
            ";
        }
        

        error_log(json_encode($query));
        $res = $wpdb->get_results($query);
        $rate = get_option('shop_rate');
        $user_internalid = get_field('internalid', 'user_'.$userID);

        // $autoload = false => return full product data
        $finalResults = array();
        if($res && !$autoload){
            for($i = $offset ; $i < min($offset + $limit, count($res)); $i++){
                
                $res[$i]->permalink = get_the_permalink($res[$i]->ID);
                $res[$i]->thumbnail_url = get_products_files($res[$i]->ID, 'thumbnail')['url'];
                $res[$i]->warranty_applicable = $this->checkIfWarrantyReleated($res[$i]->ID);
                $res[$i]->radiomodel_qualified = false;

                // check Radio model discount
                if($cust_radiomodel_qualified['status']){
                    $prod_radiomodel_qualified = $this->isProductQualifiedForRadioDiscount($res[$i]->ID, $userInternalid, $cust_radiomodel_qualified['cust_radiomodel']);

                    if($prod_radiomodel_qualified['status']){
                        $res[$i]->radiomodel_qualified = true;
                        $res[$i]->pricelist = $prod_radiomodel_qualified['pricelist'];
                    } else {
                        // check hipro discount
                        if($cust_hipro_qualified['status']){
                            $res[$i]->pricelist = $this->isProductQualifiedForHipro($res[$i]->ID, $res[$i]->internalid, $userInternalid)['pricelist'];
                        } else {
                            $res[$i]->pricelist = array(
                                'base_price' => $res[$i]->unitprice,
                                'discount_percent' => $res[$i]->custrecord_discount_percent,
                                'base_price_after_discountcode' => round((
                                    floatval($res[$i]->unitprice) - floatval($res[$i]->unitprice) * ($res[$i]->custrecord_discount_percent / 100)),2)
                            );
                        }
                    }
                } else {
                   $res[$i]->pricelist = array(
                        'base_price' => $res[$i]->unitprice,
                        'discount_percent' => $res[$i]->custrecord_discount_percent,
                        'base_price_after_discountcode' => round((
                            floatval($res[$i]->unitprice) - floatval($res[$i]->unitprice) * ($res[$i]->custrecord_discount_percent / 100)),2)
                    );
                }
                array_push($finalResults, $res[$i]);
            }                    

        } else if($res && $autoload){
            // $autoload = true => return for preview only
            // add permalink
            for($i = 0 ; $i < count($res); $i++){
                $res[$i]->permalink = get_the_permalink($res[$i]->ID);
                array_push($finalResults, $res[$i]);
            }
        }

        
        $total_count = count($res);
        

        return array(
            'res' => $finalResults,
            'cust_radiomodel_qualified' => $cust_radiomodel_qualified,
            'remoteitem' => $cust_radiomodel_remoteitem,
            'total_count' => $total_count
        );
    }


    public function get_product_ids($request){
        global $wpdb;
        if ( !wp_verify_nonce( $_REQUEST['nonce'], NONCE_SECRET)) {
            exit("No naughty business please");
        }  

        // validate logged user
        $userID = get_current_user_id();

        if(!$userID){
            $result['status'] = false;
            $result['message'] = 'משתמש לא מחובר';
            echo json_encode($result);
            die();
        }
        $userInternalid = get_field('internalid', 'user_'.$userID);
        
        //  validate params
        $fileUploadedData = isset($_REQUEST['fileUploadedData']) ? $_REQUEST['fileUploadedData'] : null;
        $itemidsArr = isset($_REQUEST['itemidsArr']) ? $_REQUEST['itemidsArr'] : null;
        

        if(!isset($fileUploadedData) || !isset($itemidsArr) || !is_array($fileUploadedData) || !is_array($itemidsArr)){
            $result['status'] = false;
            $result['message'] = 'חלק מהנתונים חסרים';
            echo json_encode($result);
            die();
        }

        $itemidsQuery = "( '". implode("','", $itemidsArr) . "')";
        // // FINAL QUERY
        $query = "SELECT 
                p.ID, 
                p.post_title,
                pmiid.meta_value AS product_internalid,
                pmii.meta_value AS product_itemid
            FROM $this->dbPosts p

            JOIN $this->dbPostmeta pmdc 
                ON pmdc.meta_key = 'custitem_discountcode' AND pmdc.post_id = p.ID
            JOIN $this->dbPostmeta pmiid
                ON pmiid.meta_key = 'internalid' AND pmiid.post_id = p.ID
            JOIN $this->dbDiscountcodes discountcodes
                ON  discountcodes.internalid = '$userInternalid' AND discountcodes.custrecord_discountcode = pmdc.meta_value
            JOIN $this->dbPostmeta pmii 
                ON pmii.meta_key = 'itemid' AND pmii.post_id = p.ID
            WHERE (p.post_type = 'product' AND p.post_status = 'publish') AND pmii.meta_value IN $itemidsQuery
        ";
        
        $res = $wpdb->get_results($query);

        $finalFileUploadedData = array();
        if($res){


            for($i = 0; $i < count($fileUploadedData); $i++){
                $found = false;
                for($j = 0; $j < count($res); $j++){
                    $resID = $res[$j]->ID;
                    // add final product data
                    $res[$j]->meta = get_fields($resID);

                    if($fileUploadedData[$i]['itemid'] == $res[$j]->product_itemid){
                        array_push($finalFileUploadedData, array(
                            'product_data' => $res[$i],
                            'file_data' => $fileUploadedData[$i]
                        ));
                        $found = true;
                        break;
                    }
                }

                if(!$found) array_push($finalFileUploadedData, array(
                    'product_data' => null,
                    'file_data' => $fileUploadedData[$i]
                ));

            }

            $result['status'] = true;
            $result['finalFileUploadedData'] = $finalFileUploadedData;
            
            echo json_encode($result);
            die();
        } 
        
        $result['status'] = false;
        $result['res'] = $res;
        $result['query'] = $query;
        $result['message'] = 'לא נמצאו תוצאות';
        echo json_encode($result);
        die();        
    }



    /** 
     * Description: getByID
     * @param $productID
     * @return
     */
    public function getByID($productID){

        $res = get_post($productID, ARRAY_A);
        $res['meta'] = get_fields($productID);

        return $res;
    }

    /** 
     * Description: getProductForCart
     * @param $productID
     * @return
     */
    public function getProductForCart($request){
        if ( !wp_verify_nonce( $_REQUEST['nonce'], NONCE_SECRET)) {
            exit("No naughty business please");
        }  

        // validate logged user
        $userID = get_current_user_id();

        if(!$userID){
            $result['status'] = false;
            $result['message'] = 'משתמש לא מחובר';
            echo json_encode($result);
            die();
        }

        //  validate params
        $cartItems = isset($_REQUEST['cartItems']) ? $_REQUEST['cartItems'] : null;

        if(!isset($cartItems)){
            $result['status'] = false;
            $result['message'] = 'חלק מהנתונים חסרים';
            echo json_encode($result);
            die();
        }

        $productDataResults = array();
        $rate = get_option('shop_rate');
        $user_internalid = get_field('internalid', 'user_'.$userID);
        $warrantyID = get_option('shop_warranty_product_id');
        $shippingProductID = get_option('shop_shipping_product_id');


        // chech for radio model qualifications
        $cust_radiomodel_qualified = $this->CustomerPRO->isQualifiedForRadioDiscount($userID);

        if($cust_radiomodel_qualified['status']){
            $result['cust_radiomodel_qualified'] = $cust_radiomodel_qualified;

            // get remote item data
            $result['remoteitem'] = $this->getRemoteData($cust_radiomodel_qualified['remoteitem'], $cust_radiomodel_qualified['remoteprice'], $user_internalid);
        }


        // check for hipro qualification
        $cust_hipro_qualified = $this->CustomerPRO->isQualifiedForHiproModel($userID);
        
        if(count($cartItems) > 0){
            foreach ($cartItems as $item){
                $productID = $item['productID'];

                $product = array();
        
                $product['productID'] = $productID;
                $product['itemid'] = get_field('itemid', $productID);
                $product['internalid'] = get_field('internalid', $productID);
                $product['title'] = get_the_title($productID);
                $product['permalink'] = get_the_permalink($productID);
                $product['thumbnail'] = get_products_files($productID, 'thumbnail')['url'];
                $product['on_sell'] = get_field('on_sell', $productID);


                // Prices
                if($productID == $warrantyID || $productID == $shippingProductID){
                    $itemBasePrice = floatval(get_field('base_price', $productID));
                    $pricelist = array(
                        'base_price' => $itemBasePrice,
                        'base_price_after_discountcode' => $itemBasePrice,
                        'discount_percent' => 0
                    );
                } else {
                    // check Radio model discount
                    $product['radiomodel_qualified'] = false;
                    if($cust_radiomodel_qualified['status']){
                        // set remote item price
                        if($productID == $result['remoteitem']['productID'] && $item['parentProduct'] != ''){
                            $pricelist = $result['remoteitem']['price_list'];
                        } else {
                            $prod_radiomodel_qualified = $this->isProductQualifiedForRadioDiscount($productID, $user_internalid, $cust_radiomodel_qualified['cust_radiomodel']);
                            
                            if($prod_radiomodel_qualified['status']){
                                $product['radiomodel_qualified'] = true;
                                $pricelist = $prod_radiomodel_qualified['pricelist'];
                            } else {
                                // check hipro discount
                                if($cust_hipro_qualified['status']){
                                    $pricelist = $this->isProductQualifiedForHipro($productID, $product['internalid'], $user_internalid)['pricelist'];
                                } else {
                                    $pricelist = $this->getProductPricesList($product['internalid'], $user_internalid, $productID);
                                }
                            }
                        }

                    } else {
                        $pricelist = $this->getProductPricesList($product['internalid'], $user_internalid, $productID);
                    }
                }
                
                
                $product['pricelist'] = $pricelist;
                $product['base_price'] = $pricelist['base_price'];
                $product['base_price_after_discountcode'] = $pricelist['base_price_after_discountcode'];


                if($product['on_sell']){
                    $product['discount_price'] = floatval(get_field('discount_price', $productID));
                    $product['discount_percentage'] = get_field('discount_percentage', $productID);
                    $rate = get_option('shop_rate');
                    $product['base_price_after_discountcode'] = number_format($product['discount_price'] * floatval(1 + $rate/100), 2);
                } else {
                    $product['discount_percentage'] = '';
                    $product['discount_price'] = '';
                }

                $product['amount'] = $item['amount'];
                $product['stock_quantity'] = get_field('stock_quantity', $productID);
                $product['warranty_applicable'] = $this->checkIfWarrantyReleated($productID);
                
                $product['parentProduct'] = $item['parentProduct'];
                $product['childeProduct'] = $item['childeProduct'];
                array_push($productDataResults, $product);
            }
        }
        $result['status'] = true;
        $result['productDataResults'] = $productDataResults;
        $result['warrantyProduct'] = $this->getWarrantyProduct();
        echo json_encode($result);
        die();
    }


       /** 
     * Description: getWarrantyProduct
     * @param
     * @return
     */
    public function getWarrantyProduct(){
        $productID = get_option('shop_warranty_product_id');

        $product = array();

        $product['productID'] = $productID;
        $product['itemid'] = get_field('itemid', $productID);
        $product['title'] = get_the_title($productID);
        $product['thumbnail'] = get_products_files($productID, 'thumbnail')['url'];

        $base_price = floatval(get_field('base_price', $productID));
        $product['base_price'] = $base_price;
        $product['base_price_after_discountcode'] = $base_price;
        $product['discount_percentage'] = 0;
        $product['pricelist'] = array(
            'base_price' => $base_price,
            'base_price_after_discountcode' => $base_price,
            'discount_percentage' => 0
        );
        
        return $product;
    }

    
    /** 
     * Description: stockNotifictaionSignup
     * @param
     * @return
     */
    public function stockNotifictaionSignup($request){
        if ( !wp_verify_nonce( $_REQUEST['nonce'], NONCE_SECRET)) {
            exit("No naughty business please");
         }  

        //  validate params
        $userEmail = isset($_REQUEST['userEmail']) ? $_REQUEST['userEmail'] : null;
        $userFullname = isset($_REQUEST['userFullname']) ? $_REQUEST['userFullname'] : null;
        $userPhone = isset($_REQUEST['userPhone']) ? $_REQUEST['userPhone'] : null;
        $productID = isset($_REQUEST['productID']) ? $_REQUEST['productID'] : null;

        $requiredAmount = isset($_REQUEST['requiredAmount']) ? $_REQUEST['requiredAmount'] : 0;

        if(!isset($userEmail) || !isset($productID) || !isset($userFullname) || !isset($userPhone)){
            $result['status'] = false;
            $result['message'] = 'חלק מהנתונים חסרים';
            echo json_encode($result);
            die();
        }

        
        // logged user
        $userID = null;

        $userExists = get_user_by( 'email', $userEmail );
        if($userExists){
            $userID = $userExists->data->ID;
        }


        // add stock notification db entry
        $notificationAdded = $this->addStockNotification($productID, $userEmail, $userFullname, $userPhone, $userID, $requiredAmount);

        if($notificationAdded['status']){
            $result['status'] = true;
            $result['notificationAdded'] = $notificationAdded;
            $result['message'] = 'פרטיך נשמרו, תקבל הודעה ברגע שהמלאי יהיה זמין';
        } else {
            $result['status'] = false;
            $result['messageSent'] = $notificationAdded;
            $result['message'] = 'קרתה שגיאה, נסה שנית';
        }

        echo json_encode($result);
        die();
    }

    /** 
     * Description: addStockNotification
     * @param
     * @return
     */
    public function addStockNotification($productID, $userEmail, $userFullname, $userPhone, $userID = null, $requiredStock = 0){
        global $wpdb;
		$createDate = current_time( "Y-m-d H:i:s", 0 );

        $query = "INSERT INTO $this->dbStockNotification
			(
                `ID`, 
                `site`, 
                `user_email`, 
                `user_fullname`, 
                `user_phone`, 
                `user_id`, 
                `product_id`, 
                `required_amount`, 
                `user_notified`, 
                `create_date`,
                `update_date`
            ) VALUES (
                NULL,
                'pro',
                '$userEmail', 
                '$userFullname', 
                '$userPhone', 
                '$userID', 
                '$productID', 
                '$requiredStock', 
                0, 
                '$createDate',
                '$createDate'
            )";

        $res = $wpdb->query($query);
        
        if($res){
            return array('status' => true, 'result' => $res);
        } else {
            return array('status' => false, 'result' => $res);
        }
    }


    /**
     * Description: getProductFilters
     */
    public function getProductFilters($fieldsForFilters = null){

        if(!$fieldsForFilters){
            $fieldChoises = get_field_object('field_5fe9d888b6f4d');
            $fieldsForFilters = $fieldChoises['choices'];
        }

        $groups = acf_get_field_groups(array('post_type' => 'product'));
                        
        $fields = array();

        foreach ($groups as $group){

            $groupFields = acf_get_fields($group['key']);
            
            foreach ($groupFields as $field){
                if(!in_array($field['name'], $fieldsForFilters)) continue;

                $field_type = $field['type'];
                $field_name = $field['name'];

                $fieldData = array(
                    'key' => $field['key'],
                    'label' => $field['label'],
                    'name' => $field_name,
                    'type' => $field['type'],
                );
                
                if($field_type == 'relationship'){
                    $postType = $field['post_type'][0];
                    wp_reset_postdata();

                    // get post types
                    $posts = new \WP_Query(array(
                        'post_type' => $postType,
                        'posts_per_page'   => -1,
                        'meta_key' => 'id',
                        'orderby' => 'meta_value_num',
                        'order' => 'ASC',
                        'meta_query' => array(
                            array(
                                'key' => 'show_on_site',
                                'compare' => '=',
                                'value' => TRUE
                            )
                        )
                    ));
                    
                    $postsChoices = array();

                    if($posts){
                        while ($posts->have_posts()) {
                            $posts->the_post();
                            $choiseID = get_the_ID();

                            $postsChoices["$choiseID"] = array(
                                'value' => $choiseID,
                                'title' => get_the_title(),
                                'results' => 0
                            );
                        }
                    }
                    wp_reset_postdata();

                    $fieldData['post_type'] = $field['post_type'][0];
                    $fieldData['choices'] = $postsChoices;
                } else if($field_type == 'select'){
                    $selectChoices = array();
                    foreach ($field['choices'] as $key => $value) {
                        $selectChoices["$key"] = array(
                            'value' => $key,
                            'title' => $value,
                            'results' => 0
                        );
                    }
                    $fieldData['choices'] = $selectChoices;
                } else if($field_type == 'number'){
                    $fieldData['min'] = $field['min'];
                    $fieldData['max'] = $field['max'];
                    $fieldData['step'] = $field['step'];
                } else {
                    $fieldData = $field;
                }
                array_push($fields, $fieldData);
            }
        }

        return $fields;
    }   

    /**
     * Description: getProductPricesList
     */
    public function getProductPricesList($internalID, $customer, $productID, $retry = false){
        global $wpdb;

        $query = "SELECT pp.*, dc.*
            FROM $this->dbProductPricing pp
            JOIN $this->dbPostmeta pm
                ON pm.meta_key = 'custitem_discountcode' AND pm.post_id = '$productID'
            JOIN $this->dbDiscountcodes dc
                ON dc.internalid = '$customer' AND dc.custrecord_discountcode = pm.meta_value
            WHERE pp.item = '$internalID' AND pp.customer = '$customer'
        ";
        
        $results = $wpdb->get_results($query);

        $response = array(
        );

        if(count($results) > 0){
            foreach($results as $res){
                if($res->custrecord_discount_percent != ''){
                    $discount_percent = intval(str_replace("%","",$res->custrecord_discount_percent));
                } else {
                    $discount_percent = 0;
                }
    
                if($res->quantityrange == '1+'){
                    $response['base_price'] = floatval($res->unitprice);
                    $response['discount_percent'] = $discount_percent;
                    
                    $response['base_price_after_discountcode'] = round((
                        floatval($res->unitprice) - floatval($res->unitprice) * ($discount_percent / 100)),2);
                } else {
                    $response[$res->quantityrange]['base_price'] = floatval($res->unitprice);
                    $response[$res->quantityrange]['base_price_after_discountcode'] = round((
                        floatval($res->unitprice) - floatval($res->unitprice) * ($discount_percent / 100)),2);
                    $response['discount_percent'] = $discount_percent;
                }
            }
        } else {
            if($retry == false){
                // get from erp 
                $this->syncPrices($customer, array($internalID));
                return $this->getProductPricesList($internalID, $customer, $productID, $retry = true);
            } else {
                return $response;
            }
        }
        
        return $response;
    }

    /**
     * Description: getProductBasePrice
     */
    public function getProductBasePrice($internalID, $customer, $productID, $retry = false){
        global $wpdb;

        $query = "SELECT pp.*
            FROM $this->dbProductPricing pp
            JOIN $this->dbPostmeta pm
                ON pm.meta_key = 'custitem_discountcode' AND pm.post_id = '$productID'
            WHERE pp.item = '$internalID' AND pp.customer = '$customer'
        ";
        
        $results = $wpdb->get_results($query);

        $base_price = null;

        if(count($results) > 0){
            foreach($results as $res){
                if($res->quantityrange == '1+'){
                    $base_price = floatval($res->unitprice);
                    break;
                } 
            }
        } 
        
        return $base_price;
    }
/** 
     * Description: syncPrices
     * @param
     * @return 
     */
    public function syncPrices($customer, $priceSyncData){
        // PRO site
        $netPricesData = $this->netSuiteApi->getProductsPricePRO($customer, 0, $priceSyncData);
        
        if($netPricesData['status']){
            // update database 
            $productsPrices = $netPricesData['data'];

            $chunks = array_chunk($productsPrices, 500);

            foreach($chunks as $chunk){
                $query = $this->ProductPricing->upsert($chunk);
            }
        }
    }

    /** 
     * Description: checkIfWarrantyReleated
     * @param
     * @return
     */
    public function checkIfWarrantyReleated($productID){
        //  validate params
        if(!isset($productID)){
            return false;
        }

        $warranty_categories_list = explode(',', get_option('warranty_categories_list'));
        $warranty_subgroups_list = explode(',', get_option('warranty_subgroups_list'));
        $warranty_sub2groups_list = explode(',', get_option('warranty_sub2groups_list'));

        $productTax = array(
            'category' => get_field('custitem_somfy_web_product_category',$productID, false),
            'sub_product_group' => get_field('custitem_sub_product_group',$productID, false),
            'sub_2product_group' => get_field('custitem_sub2productgroup',$productID, false),
        );

        if(!is_array($productTax['category']) || 
            !is_array($productTax['sub_product_group']) || 
            !is_array($productTax['sub_2product_group'])) return false;

        if(count(array_intersect($productTax['category'], $warranty_categories_list)) >= 1 
            && count(array_intersect($productTax['sub_product_group'],$warranty_subgroups_list)) >= 1 
            && count(array_intersect($productTax['sub_2product_group'],$warranty_sub2groups_list)) >=1){
                return true;
            }
        return false;
    }



    public function isProductQualifiedForRadioDiscount($productID, $userInternalid, $cust_radiomodel){
        // check if product qualified for radio discount for specific customer
        $prod_internalid = get_field('internalid', $productID);
        $prod_radiomodel = get_field('custitem_radiomodel', $productID);
        $prod_related_regular_item =  get_field('custitem_related_regular_item', $productID);

        $isQualified = false;
        $pricelist = null;

        if($prod_radiomodel != '' && $prod_related_regular_item != ''){
            $prod_radiomodel_array = explode(',', $prod_radiomodel);
            
            if(in_array($cust_radiomodel, $prod_radiomodel_array)){
                // product qualified for discount
                // get price for prod_related_regular_item
                $prod_related_regular_item_productID = $this->getProductIDByInternalid($prod_related_regular_item);
                if($prod_related_regular_item_productID){
                    $isQualified = true;
                    $pricelist = $this->getProductPricesList($prod_related_regular_item, $userInternalid, $prod_related_regular_item_productID);
                }
            } else {
                $isQualified = false;
                $pricelist = $this->getProductPricesList($prod_internalid, $userInternalid, $productID);
            }
        } else {
            $isQualified = false;
            $pricelist = $this->getProductPricesList($prod_internalid, $userInternalid, $productID);
        }
        return array('status' => $isQualified, 'pricelist' => $pricelist);
    }
    

    /** check if product is qualified for hipro model discount and return the price list
    * else return the original item price list
    **/
    public function isProductQualifiedForHipro($productID, $prod_internalid, $userInternalid){
        // check if product qualified for radio discount for specific customer
        $newProductInternalid = false;

        foreach ($this->hipro12model_matrix as $item) {
            if($item['originalinternalid'] == $prod_internalid){
                $newProductInternalid = $item['newinternalid'];
                break;
            }
        }
        
        if($newProductInternalid != ''){
            $newProductproductID = $this->getProductIDByInternalid($newProductInternalid);
            if($newProductproductID != ''){
                $newBasePrice = $this->getProductBasePrice($newProductInternalid, $userInternalid, $newProductproductID);

                if($newBasePrice){
                    $oldPricelist = $this->getProductPricesList($prod_internalid, $userInternalid, $productID);
                    
                    $pricelist['base_price'] = floatval($newBasePrice);
                    $pricelist['discount_percent'] = $oldPricelist['discount_percent'];
                    $pricelist['base_price_after_discountcode'] = round((
                        floatval($pricelist['base_price']) - floatval($pricelist['base_price']) * ($pricelist['discount_percent'] / 100)),2);

                    return array(
                        'status' => true, 
                        'pricelist' => $pricelist
                    );
                }
            }
        } 

        $pricelist = $this->getProductPricesList($prod_internalid, $userInternalid, $productID);
        return array(
            'status' => false, 
            'pricelist' => $pricelist
        );
    }
    

    
    public function getProductIDByInternalid($internalID){

        $internalIds = explode(',',$internalID);

        $product = new \WP_Query(array(
            'post_type' => 'product',
            'post_status' => array('publish','pending','draft','auto-draft'),
            'posts_per_page'   => -1,
            'meta_key' => 'internalid',
            'meta_query' => array(
                array(
                    'key' => 'internalid',
                    'compare' => 'IN',
                    'value' => $internalIds
                )
            )
        )); 
        $ids = array();

        if(count($product->posts) > 0){
            foreach($product->posts as $product){
                array_push($ids, $product->ID);
            }
        }

        return isset($ids[0]) ? $ids[0] : false;
    }



    public function getRemoteData($internalid, $remotePrice, $user_internalid){
        $productID = $this->getProductIDByInternalid($internalid);

        $pricelist = $this->getProductPricesList($internalid, $user_internalid, $productID);
        if(intval($remotePrice) == 0){
            $discount_percent = 100;
        } else {
            $discount_percent = intval(1 - $pricelist['base_price'] / $remotePrice) * 100;
        }

        return array(
            'productID' => $productID,
            'internalid' => $internalid,
            'itemid' => get_field('itemid', $productID),
            'title' => get_the_title($productID),
            'thumbnail_url' => get_products_files($productID, 'thumbnail')['url'],
            'permalink' => get_the_permalink($productID),
            'short_desc' => htmlspecialchars_decode(get_field('storedescription', $productID)),
            'price_list' => array(
                'base_price' => $pricelist['base_price'],
                'discount_percent' => $discount_percent,
                'base_price_after_discountcode' => floatval($remotePrice)
            )
        );
    }
}