<?php
/**
 * @package  SomfySiteManager
 */

namespace Inc\Classes\Shop;

use Inc\Classes\Shop\NetSuiteApiPRO;
use Inc\Classes\SomfyMessaging;

class BalancePRO{
    
    public function get_customer_balance($request){
        if ( !wp_verify_nonce( $_REQUEST['nonce'], NONCE_SECRET)) {
            exit("No naughty business please");
        }  


         //  validate params
        $paginators = isset($_REQUEST['paginators']) ? $_REQUEST['paginators'] : null;
 
        if(!isset($paginators)){
            $paginators = array(
                'page' => 1,
                'limit' => 10
            );
        }

        // validate logged user
        $userID = get_current_user_id();

        if(!$userID){
            $result['status'] = false;
            $result['message'] = 'משתמש לא מחובר';
            echo json_encode($result);
            die();
        }
        
        $internalid = isset(get_user_meta($userID, 'internalid')[0]) ? get_user_meta($userID, 'internalid')[0] : null;
        
        if(is_null($internalid) || $internalid == ''){
            $result['status'] = false;
            $result['message'] = 'משתמש לא מסונכרן';
            echo json_encode($result);
            die();
        }
        
        $synced = $this->syncCustomerBalance($internalid);
        
        if(!$synced['status']){
            $result['status'] = false;
            $result['message'] = $synced['message'];
            $result['synced'] = $synced;
            echo json_encode($result);
            die();
        } 


        $balanceResultsData = $synced['balanceResultsData'];
        
        // get balance data
        $result['totalResults'] = count($balanceResultsData);
        $result['maxPages'] = ceil($result['totalResults'] / $paginators['limit']);
        $resultsPagesChunks = array_chunk($balanceResultsData, $paginators['limit']);

        $result['balanceResultsData'] = $resultsPagesChunks[$paginators['page'] - 1];
        $result['totalOverdue'] = $synced['totalOverdue'];

        
        // add customer terms data
        $termsData = get_field('terms', "user_$userID" ,true);
        if(isset($termsData)){
            $term = $termsData['label'];
            if(strpos($term, 'שוטף ') !== false){
                $result['termDaysLimit'] = intval(str_replace('שוטף ', '', $term));
            } else {
                $result['termDaysLimit'] = null;
            }
        };


        $result['status'] = true;
        echo json_encode($result);
        die();
    }
    
	
    public function get_customer_inv_autoload($request){
        if ( !wp_verify_nonce( $_REQUEST['nonce'], NONCE_SECRET)) {
            exit("No naughty business please");
        }  


         //  validate params
        $term = isset($_REQUEST['term']) ? $_REQUEST['term'] : null;
 
        if(!isset($term)){
            $result['status'] = false;
            $result['message'] = 'לא הוזן שום ערך חיפוש';
            echo json_encode($result);
            die();
        }
        $term = trim($term);

        // validate logged user
        $userID = get_current_user_id();

        if(!$userID){
            $result['status'] = false;
            $result['message'] = 'משתמש לא מחובר';
            echo json_encode($result);
            die();
        }

        $internalid = isset(get_user_meta($userID, 'internalid')[0]) ? get_user_meta($userID, 'internalid')[0] : null;
        
        if(is_null($internalid)){
            $result['status'] = false;
            $result['message'] = 'משתמש לא מסונכרן';
            echo json_encode($result);
            die();
        }

        global $wpdb;
        $balance_db = PRO_SITE_PREFIX . "balance";

        $query = "SELECT * 
            FROM $balance_db
            WHERE entityid = '$internalid' AND tranid LIKE '%$term%'
            LIMIT 5";
        
        $customer_invs = $wpdb->get_results($query);
        $result['status'] = true;
        $result['results'] = $customer_invs;
        echo json_encode($result);
        die();
    }
    

    public function new_balance_payment_request($request){
        if ( !wp_verify_nonce( $_REQUEST['nonce'], NONCE_SECRET)) {
            exit("No naughty business please");
         }  

        //  validate params
        $items = isset($_REQUEST['items']) ? $_REQUEST['items'] : null;
        $user_contact = isset($_REQUEST['user_contact']) ? $_REQUEST['user_contact'] : null;
        $user_email = isset($_REQUEST['user_email']) ? $_REQUEST['user_email'] : null;
        $phone = isset($_REQUEST['phone']) ? $_REQUEST['phone'] : null;
        
        
        if(is_null($items) || is_null($user_contact) 
            || is_null($user_email) || is_null($phone)){
            $result['status'] = false;
            $result['request'] = $_REQUEST;
            $result['message'] = 'חלק מהנתונים חסרים';
            echo json_encode($result);
            die();
        }

        // validate logged user
        $userID = get_current_user_id();

        if(!$userID){
            $result['status'] = false;
            $result['message'] = 'משתמש לא מחובר';
            echo json_encode($result);
            die();
        }

        date_default_timezone_set('Israel');
        $SomfyMessaging = new SomfyMessaging();

        // prep balance table
        $data = array(
            'request_date' => date('d/m/Y H:i'),
            'user_contact' => $user_contact,
            'entityid' => get_field('entityid', 'user_'.$userID),
            'user_email' => $user_email,
            'phone' => $phone,
            'items_table' => $this->getTableHtml($items)
        );

        // send data via mail
        $balance_payment_request__to = get_option('balance_payment_request__to');
        $balance_payment_request__subject = get_option('balance_payment_request__subject');
        $template = get_option('balance_payment_request__template');
        $balance_payment_request__content = $SomfyMessaging->getTemplate($template);

        // send email to somfy

        // send to somfy
        $sendTo = $balance_payment_request__to;
        $subject = $balance_payment_request__subject;
        $htmlBody = $balance_payment_request__content;
        

        $somfyMessageSent = $SomfyMessaging->sendMail($sendTo, $subject, $htmlBody, $data);


        $result['status'] = true;
        $result['somfyMessageSent'] = $somfyMessageSent;
        echo json_encode($result);
        die();
    }

    public function get_inv_file($request){
        if ( !wp_verify_nonce( $_REQUEST['nonce'], NONCE_SECRET)) {
            exit("No naughty business please");
        }  

         //  validate params
        $recordid = isset($_REQUEST['recordid']) ? $_REQUEST['recordid'] : null;
 
        if(!isset($recordid) || is_null($recordid)){
            $result['status'] = false;
            $result['message'] = 'לא הוזן recordid';
            echo json_encode($result);
            die();
        }

        // validate logged user
        $userID = get_current_user_id();

        if(!$userID){
            $result['status'] = false;
            $result['message'] = 'משתמש לא מחובר';
            echo json_encode($result);
            die();
        }
        
        // if first load - sync data from ERP
        $NetSuiteApi = new NetSuiteApiPRO();
        $invFileData = $NetSuiteApi->getInvFile($recordid);

        if(!$invFileData['status'] || !is_array($invFileData['data'])){
            $result['status'] = false;
            $result['invFileData'] = $invFileData;
            $result['message'] = 'לא ניתן לקבל נתונים מהמערכת';
            echo json_encode($result);
            die();
        } else {
            $result['status'] = true;
            $result['invFileData'] = $invFileData['data'][0];
            echo json_encode($result);
            die(); 
        }    
    }
    
    
    public function getInvByOrder($orderID){
        global $wpdb;
        $balance_db = PRO_SITE_PREFIX . "balance";

        $query = "SELECT * 
            FROM $balance_db
            WHERE orderID = '$orderID'";

        return $wpdb->get_results($query);
    }


    public function syncCustomerBalance($internalid, $justSync = false){
        // if first load - sync data from ERP
        $NetSuiteApi = new NetSuiteApiPRO();
        $balanceResults = $NetSuiteApi->getCustomerBalance($internalid);

        if(!$balanceResults['status'] || !is_array($balanceResults['data'])){
            $result['status'] = false;
            $result['balanceResults'] = $balanceResults;
            $result['message'] = 'לא ניתן לקבל נתונים מהמערכת';
            return $result;
        } else {
            // add order data
            $balanceResultsData = array();
            $balanceForSync = array();
            $totalOverdue = 0;
            foreach ($balanceResults['data'] as $inv){
                $inv['orderData'] = $this->getOrderByInternalid($inv['createdfrom']);
                array_push($balanceForSync, $inv);

                if($inv['statusref'] != 'open') continue;
                $totalOverdue += floatval($inv['fxamount']);
                array_push($balanceResultsData, $inv);
            }

            // Sync results to db
            $result['status'] = true;

            if(!$justSync){
                $result['balanceResultsData'] = $balanceResultsData;
                $result['balanceForSync'] = $balanceForSync;
                $result['totalOverdue'] = round($totalOverdue, 2);
            }
            
            $result['synced'] = $this->upsert($balanceForSync);
        }   
        
        return $result;
    }

    /** 
     * Description: getTableHtml
     * @param
     * @return
     */
    public function getTableHtml($itemsData){
        $html = "<style>
            th, td{
                border: 1px solid #ddd;
                padding: 8px;
                text-align: right;
            }
            thead{
                background: #000;
                color: #fff;
                padding: 8px;
            }
        </style>";

        $html .= "<table style='background: #fff; width: 100%; direction: rtl;border-collapse: collapse;'>
            <thead>
                <tr>
                    <th>מס חשבונית</th>
                    <th>תאריך</th>
                    <th>סכום מקורי</th>
                    <th>יתרה</th>
                    <th>סכום לתשלום</th>
                </tr>
            </thead>
            <tbody>";
            
        // print lines
        foreach($itemsData as $item){
            $tranid = $item['tranid'];
            $trandate = $item['itemData']['trandate'];
            $amount = $item['itemData']['amount'];
            $fxamount = $item['itemData']['fxamount'];
            $amountToPay = $item['amount'];

            $html .= "<tr>
                <td>$tranid</td>
                <td>$trandate</td>
                <td>$amount</td>
                <td>$fxamount</td>
                <td>$amountToPay ₪</td>
            </tr>";
        }

        $html .= "</tbody>
        </table>";

        return $html;
    }

    public function getOrderByInternalid($internalid){
        $res = new \WP_Query(
            array(
                'posts_per_page' => 1,
                'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit'),  
                'post_type' => 'somfy_order',
                'meta_query' => array(
                    array(
                        'key'     => 'internalid',
                        'value'   => $internalid,
                        'compare' => '='
                    )
                )
            ));

        if($res->post_count > 0){
            return array(
                'ID' => $res->posts[0]->ID,
                'tranid' => get_field('tranid',$res->posts[0]->ID)
            );
        } else {
            return false;
        }
    }


    public function upsert($data){
		global $wpdb;
        $balance_db = PRO_SITE_PREFIX . "balance";

		$query = "INSERT INTO $balance_db
			(
                `key`, 
                `tranid`, 
                `internalid`, 
                `type`, 
                `entityid`, 
                `createdfrom`, 
                `orderID`, 
                `order_tranid`, 
                `statusref`, 
                `amount`,
                `fxamount`,
                `currency`,
                `daysopen`,
                `trandate`
            ) VALUES ";

        for($i = 0; $i < count($data); $i++){
            $row = $data[$i];
            $rowKey = $row['entityid'] ."_". $row['tranid'];

            $query .= " (
                '$rowKey',
				'".$row['tranid']."',
				'".$row['internalid']."',
				'".$row['type']."',
				'".$row['entityid']."',
				'".$row['createdfrom']."',
				'".$row['orderData']['ID']."',
				'".$row['orderData']['tranid']."',
				'".$row['statusref']."',
				'".$row['amount']."',
				'".$row['fxamount']."',
				'".$row['currency']."',
				'".$row['daysopen']."',
				'".$row['trandate']."'
            ) ";

            if($i < count($data) - 1) $query .= " ,";
        }
        $query .= " ON DUPLICATE KEY UPDATE 
            amount=VALUES(amount),
            fxamount=VALUES(fxamount),
            fxamount=VALUES(daysopen),
            createdfrom=VALUES(createdfrom),
            orderID=VALUES(orderID),
            order_tranid=VALUES(order_tranid); ";

        return $wpdb->query($query);
	}
}