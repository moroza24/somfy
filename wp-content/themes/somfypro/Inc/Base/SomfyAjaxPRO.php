<?php
/**
 * @package SomfyPRO
 */

namespace Inc\Base;

use Inc\Classes\GlobalSearchPRO;

use Inc\Classes\Shop\CartPRO;
use Inc\Classes\Shop\OrderPRO;
use Inc\Classes\Shop\ProductsPRO;
use Inc\Classes\Shop\WishListPRO;
use Inc\Classes\Shop\CustomerPRO;
use Inc\Classes\Shop\BalancePRO;
use Inc\Classes\Shop\RMAPRO;

class SomfyAjaxPRO {
    public function register() {
        $GlobalSearch = new GlobalSearchPRO();
        $Cart = new CartPRO();
        $Order = new OrderPRO();
        $Products = new ProductsPRO();
        $Wishlist = new WishListPRO();
        $Customer = new CustomerPRO();
        $Balance = new BalancePRO();
        $RMA = new RMAPRO();
        

        /**
         * Shop AJAX
         */
            add_action("wp_ajax_somfy_pro_shop_products", array( $Products, "getShopProducts")); // logged user
            add_action("wp_ajax_nopriv_somfy_pro_shop_products", array( $this, "somfy_must_login")); // not logged user

            add_action("wp_ajax_somfy_pro_product_preview", array( $Products, "getProductByID")); // logged user
            add_action("wp_ajax_nopriv_somfy_pro_product_preview", array( $this, "somfy_must_login")); // not logged user

        
        /**
         * Global Search AJAX
         */

            add_action("wp_ajax_somfy_pro_global_search", array( $GlobalSearch, "globalSearch")); // logged user
            add_action("wp_ajax_nopriv_somfy_pro_global_search", array( $this, "somfy_must_login")); // not logged user


            add_action("wp_ajax_somfy_pro_full_search", array( $GlobalSearch, "fullSearch")); // logged user
            add_action("wp_ajax_nopriv_somfy_pro_full_search", array( $this, "somfy_must_login")); // not logged user


        
        
        /**
         * StockNotifictaion AJAX
         */
            add_action("wp_ajax_somfy_pro_stock_notifictaion_signup", array( $Products, "stockNotifictaionSignup")); // logged user
            add_action("wp_ajax_nopriv_somfy_pro_stock_notifictaion_signup", array( $this, "somfy_must_login")); // not logged user

                
        /**
         * Wishlist AJAX
         */
            // Add
            add_action("wp_ajax_somfy_pro_wishlist_add", array( $Wishlist, "addToWishlist")); // logged user
            add_action("wp_ajax_nopriv_somfy_pro_wishlist_add", array( $this, "somfy_must_login")); // not logged user

            // getByUser
            add_action("wp_ajax_somfy_pro_get_by_user", array( $Wishlist, "getByUser")); // logged user
            add_action("wp_ajax_nopriv_somfy_pro_get_by_user", array( $Wishlist, "getByUser")); // not logged user

            // removeFromWishlist
            add_action("wp_ajax_somfy_pro_wishlist_remove", array( $Wishlist, "removeFromWishlist")); // logged user
            add_action("wp_ajax_nopriv_somfy_pro_wishlist_remove", array( $this, "somfy_must_login")); // not logged user


        
        /**
         * Cart AJAX
         */

            // getCart
            add_action("wp_ajax_somfy_pro_get_cart", array( $Cart, "getCart")); // logged user
            add_action("wp_ajax_nopriv_somfy_pro_get_cart", array( $this, "somfy_must_login")); // not logged user

            // addToCart
            add_action("wp_ajax_somfy_pro_add_to_cart", array( $Cart, "addCartItem")); // logged user
            add_action("wp_ajax_nopriv_somfy_pro_add_to_cart", array( $this, "somfy_must_login")); // not logged user

            // getProductForCart
            add_action("wp_ajax_somfy_pro_get_cart_product_data", array( $Products, "getProductForCart")); // logged user
            add_action("wp_ajax_nopriv_somfy_pro_get_cart_product_data", array( $this, "somfy_must_login")); // not logged user

            // updateCart
            add_action("wp_ajax_somfy_pro_update_cart", array( $Cart, "updateCart")); // logged user
            add_action("wp_ajax_nopriv_somfy_pro_update_cart", array( $this, "somfy_must_login")); // not logged user

            // updateCart
            add_action("wp_ajax_somfy_pro_check_warranty_releated", array( $Products, "checkIfWarrantyReleated")); // logged user
            add_action("wp_ajax_nopriv_somfy_pro_check_warranty_releated", array( $this, "somfy_must_login")); // not logged user
      
        
        /**
         * Order AJAX
         */

            // newOrder
            add_action("wp_ajax_somfy_pro_new_order", array( $Order, "addNewOrder")); // logged user
            add_action("wp_ajax_nopriv_somfy_pro_new_order", array( $this, "somfy_must_login")); // not logged user
        
            // get user orders
            add_action("wp_ajax_somfy_pro_user_orders", array( $Order, "getUserOrders")); // logged user
            add_action("wp_ajax_nopriv_somfy_pro_user_orders", array( $this, "somfy_must_login")); // not logged user

            // sync user orders
            add_action("wp_ajax_somfy_pro_sync_user_orders", array( $Order, "syncUserOrders")); // logged user
            add_action("wp_ajax_nopriv_somfy_pro_sync_user_orders", array( $this, "somfy_must_login")); // not logged user

            // get user orders details
            add_action("wp_ajax_somfy_pro_order_details", array( $Order, "getOrderDetails")); // logged user
            add_action("wp_ajax_nopriv_somfy_pro_order_details", array( $this, "somfy_must_login")); // not logged user

        /**
         * Customer AJAX
         */
            
            // new registration request
            add_action("wp_ajax_somfy_pro_new_registration_request", array( $Customer, "new_registration_request")); // logged user
            add_action("wp_ajax_nopriv_somfy_pro_new_registration_request", array( $Customer, "new_registration_request")); // not logged user
        
            // new account update request
            add_action("wp_ajax_somfy_pro_new_account_update_request", array( $Customer, "new_account_update_request")); // logged user
            add_action("wp_ajax_nopriv_somfy_pro_new_account_update_request", array( $this, "somfy_must_login")); // not logged user
            
            // expert_signup
            add_action("wp_ajax_somfy_pro_expert_signup", array( $Customer, "expert_signup")); // logged user
            add_action("wp_ajax_nopriv_somfy_pro_expert_signup", array( $this, "somfy_must_login")); // not logged user
    
        
        /**
         * Balance Ajax 
         */
            // somfy_pro_new_event_registration
            add_action("wp_ajax_somfy_pro_new_event_registration", array( $Customer, "new_event_registration")); // logged user
            add_action("wp_ajax_nopriv_somfy_pro_new_event_registration", array( $Customer, "new_event_registration")); // not logged user

            // customer_balance
            add_action("wp_ajax_somfy_pro_new_balance_payment_request", array( $Balance, "new_balance_payment_request")); // logged user
            add_action("wp_ajax_nopriv_somfy_pro_new_balance_payment_request", array( $this, "somfy_must_login")); // not logged user


            // customer_balance
            add_action("wp_ajax_somfy_pro_customer_balance", array( $Balance, "get_customer_balance")); // logged user
            add_action("wp_ajax_nopriv_somfy_pro_customer_balance", array( $this, "somfy_must_login")); // not logged user

            // inv_autoload
            add_action("wp_ajax_somfy_pro_inv_autoload", array( $Balance, "get_customer_inv_autoload")); // logged user
            add_action("wp_ajax_nopriv_somfy_pro_inv_autoload", array( $this, "somfy_must_login")); // not logged user

            // get inv file
            add_action("wp_ajax_somfy_pro_get_inv_file", array( $Balance, "get_inv_file")); // logged user
            add_action("wp_ajax_nopriv_somfy_pro_get_inv_file", array( $this, "somfy_must_login")); // not logged user
        /**
         * RMA Ajax 
         */
            // somfy_pro_new_rma
            add_action("wp_ajax_somfy_pro_new_rma", array( $RMA, "new_rma_request")); // logged user
            add_action("wp_ajax_nopriv_somfy_pro_new_rma", array( $this, "somfy_must_login")); // not logged user


        /**
         * General Ajax 
         */
            // get_product_ids
            add_action("wp_ajax_somfy_pro_get_product_ids", array( $Products, "get_product_ids")); // logged user
            add_action("wp_ajax_nopriv_somfy_pro_get_product_ids", array( $this, "somfy_must_login")); // not logged user
            
            // Contact Page
            add_action("wp_ajax_somfy_pro_contact_form_submit", array( $Customer, "new_contact_form_submit")); // logged user
            add_action("wp_ajax_nopriv_somfy_pro_contact_form_submit", array( $this, "somfy_must_login")); // not logged user

        // check_user_logged
        add_action('wp_ajax_is_user_logged_in', array( $this, "ajax_check_user_logged_in"));
        add_action('wp_ajax_nopriv_is_user_logged_in', array( $this, "ajax_check_user_logged_in"));
    }

    function ajax_check_user_logged_in() {
        $isLoggedIn = is_user_logged_in()?'yes':'no';
        echo json_encode($isLoggedIn);
        die();
    }
    

    function somfy_must_login() {
        echo "You must be logged in!";
        die();
    }
}