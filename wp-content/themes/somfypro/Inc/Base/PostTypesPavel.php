<?php
/**
 * @package SomfyPRO
 */


add_action('init', 'news_post_types');


function news_post_types(){
    // Product
    register_post_type('costumer-updates',array(
        'supports' => array('title','thumbnail','editor','excerpt'),
        'rewrite' => array('slug' => 'costumer-updates'),
        'public' => true,
        'has_archive' => true,
        'labels' => array(
            'name' => 'Updates',
            'add_new_item' => 'Add New Item',
            'edit_item' => 'Edit Item',
            'all_items' => 'All Items',
            'singular_name' => 'Item',
            'show_ui' => true
        ),
        'menu_icon' => 'dashicons-products'
    ));

    register_post_type('studies',array(
        'supports' => array('title','thumbnail','editor','excerpt'),
        'rewrite' => array('slug' => 'studies'),
        'public' => true,
        'has_archive' => true,
        'labels' => array(
            'name' => 'Studies',
            'add_new_item' => 'Add New Item',
            'edit_item' => 'Edit Item',
            'all_items' => 'All Items',
            'singular_name' => 'Item',
            'show_ui' => true
        ),
        'menu_icon' => 'dashicons-products'
    ));
}
function more_post_ajax(){
    $ppp = (isset($_POST["ppp"])) ? $_POST["ppp"] : 3;
    $page = (isset($_POST['pageNumber'])) ? $_POST['pageNumber'] : 0;

    $postype = (isset($_POST['postype'])) ? $_POST['postype'] : '';

    // print_r($postype);

    header("Content-Type: text/html");

    $args = array(
        'post_type' => $postype,
        'posts_per_page' => $ppp,
        'paged'    => $page,
        'order' => 'ASC'
    );
    $loop = new WP_Query($args);
    if ($loop -> have_posts()) :  while ($loop -> have_posts()) : $loop -> the_post();

      if($postype == 'costumer-updates'){
        get_template_part('elements/update-post');
      } else if($postype == 'studies') {
        get_template_part('elements/studies-post');
      }
    endwhile;
    endif;
    wp_reset_postdata();
    die();
}

add_action('wp_ajax_nopriv_more_post_ajax', 'more_post_ajax');
add_action('wp_ajax_more_post_ajax', 'more_post_ajax');
