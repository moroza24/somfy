<?php
/**
 * @package SomfyPRO
 */


add_action('um_after_account_general', 'show_extra_fields', 100);


function show_extra_fields() {
	$custom_fields = [
		"custentity_il_mobile" => "מספר נייד",
		"phone" => "טלפון"
	];

	foreach ($custom_fields as $key => $value) {

		$fields[ $key ] = array(
				'title' => $value,
				'metakey' => $key,
				'type' => 'select',
				'label' => $value,
		);

		apply_filters('um_account_secure_fields', $fields, 'general' );

		$field_value = get_user_meta(um_user('ID'), $key, true) ? : '';

		$html = '<div class="um-field um-field-'.$key.'" data-key="'.$key.'">
			<div class="um-field-label">
				<label for="'.$key.'">'.$value.'</label>
				<div class="um-clear"></div>
			</div>
			<div class="um-field-area">
				<input class="um-form-field valid "
					type="text" name="'.$key.'"
					id="'.$key.'" value="'.$field_value.'"
					placeholder=""
					data-validate="" data-key="'.$key.'">
			</div>
		</div>';

		echo $html;

	}
}