<?php
/**
 * @package SomfyPRO
 */

use Inc\Base\SomfyAjaxPRO;

$somfyAjax = new SomfyAjaxPRO();
$somfyAjax->register();

if ( ! function_exists( 'somfypro_setup' ) ) :

    function somfypro_setup() {

        // Nav Register
        register_nav_menus(
                array(
                    'user' =>'User Menu' ,
                    'footer1' => 'Footer Menu 1',
                    'footer2' => 'Footer Menu 2'
                )
        );

        // Theme Features
        add_theme_support('title-tag');
        add_theme_support('post-thumbnails');
    }
endif;

add_action( 'after_setup_theme', 'somfypro_setup' );