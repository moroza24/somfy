<?php 
    if ( ! defined( 'ABSPATH' ) ) exit;
?>
<?php
  get_header();

  while(have_posts()) {

    the_post();
    $pageId = get_the_id();


    // Hero
    $fields = get_fields($pageId);

    if(isset($fields['hero'])){
      $hero = $fields['hero'];

      $backgroundImage = isset($hero['background_image']['url']) ? $hero['background_image']['url'] : NULL;
      $title = isset($hero['title']) ? $hero['title'] : NULL;
      $box_text = isset($hero['box_text']) ? $hero['box_text'] : NULL;

      $buttonLabel = isset($fields['button_label']) ? $fields['button_label'] : NULL;
      $buttonUrl = isset($fields['button_url']) ? $fields['button_url'] : NULL;

      if(isset($backgroundImage)){
        echo "<div class='page_hero' style='background-image: url(\"$backgroundImage\");'>";
        if(isset($title) || isset($box_text)){
          echo "<div class='hero__content'>
                  <h1 class='hero__title'>$title</h1>
                  <p class='hero__text'>$box_text</p>
                </div>";
        }
        echo "</div>";
      }
    }
     ?>
<?php
    $pageID = get_the_ID();
    $currentPagePermalink = get_the_permalink($pageID);
    $pageTitle = get_the_title($pageID);

?>
      
        <div class="generic-content">
          <ol class="breadcrumbs dark generic-breadcrumb">
            <li><a href="<?php echo home_url(); ?>" alt='דף בית'>דף בית ></a></li>
            <li><a href="<?php echo $currentPagePermalink; ?>" alt='<?php echo $pageTitle; ?>'><?php echo $pageTitle; ?></a></li>
          </ol>
            <?php 
              the_content(); 

              if(isset($buttonLabel) && isset($buttonUrl)){
                echo "<div class='generic-content__cta_container'>
                  <a href='$buttonUrl' class='generic-content__cta' alt='$buttonLabel'>$buttonLabel</a>
                </div>";
              }
              
            ?>


        </div>

<?php }
  get_footer();
?>