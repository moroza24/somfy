<div id="registration_popup" class="popup__overlay">
    
    <div class="popup__container">
        
        <div class="container_header">
            <a class="popup__overlay__close registration_popup__close" alt='סגור'>
                <i class="fas fa-times"></i>
            </a>
            <h2>בקשה להצטרפות</h2>
        </div>
        <div class="container_body">
            <h4 class="content_title">אנא מלא את כל השדות</h4>

            <form id="rgistration_request__form">
                <div class="mb-3 form-input__group">
                    <input type="text" class="form-control form-control-lg" id="rgistration_request__companyname" placeholder="שם החברה/עסק" minlength="3">
                    <p class="form_error" data-input="rgistration_request__companyname">יש להזין שם חברה או עסק תיקנים</p>
                </div>

                <div class="mb-3 form-input__group">
                    <input type="text" class="form-control form-control-lg" id="rgistration_request__city" placeholder="עיר" minlength="3">
                    <p class="form_error" data-input="rgistration_request__city">יש להזין עיר תיקנה</p>
                </div>

                <div class="mb-3 form-input__group">
                    <input type="text" class="form-control form-control-lg" id="rgistration_request__companyid" placeholder="ח.פ/ת.ז" minlength="6"
                        onkeydown="if(event.key==='.'){event.preventDefault();}"  oninput="event.target.value = event.target.value.replace(/[^0-9]*/g,'');">
                    <p class="form_error" data-input="rgistration_request__companyid">יש להזין ח.פ/ת.ז</p>
                </div>

                <div class="mb-3 form-input__group">
                    <input type="text" class="form-control form-control-lg" id="rgistration_request__contact_name" placeholder="איש קשר" minlength="2">
                    <p class="form_error" data-input="rgistration_request__contact_name">יש להזין שם איש קשר</p>
                </div>

                <div class="mb-3 form-input__group">
                    <input type="email" class="form-control form-control-lg" id="rgistration_request__email" placeholder='כתובת דוא"ל'>
                    <p class="form_error" data-input="rgistration_request__email">יש להזין כתובת דוא"ל</p>
                </div>

                <div class="mb-3 form-input__group">
                    <select class="form-control form-control-lg form-select" aria-label="תחום פעילות" id="rgistration_request__activitytype">
                        <option value="-1">תחום פעילות</option>
                        <option value="שלטים">שלטים</option>
                        <option value="ונציאניים">ונציאניים</option>
                        <option value="בית חכם">בית חכם</option>
                    </select>
                    <p class="form_error" data-input="rgistration_request__activitytype">יש לבחור תחום פעילות</p>
                </div>

                <div class="mb-3 form-input__group">
                    <input type="tel" class="form-control form-control-lg" id="rgistration_request__phone" placeholder="טלפון" minlength="8"  maxlength="10"
                        onkeydown="if(event.key==='.'){event.preventDefault();}"  oninput="event.target.value = event.target.value.replace(/[^0-9]*/g,'');">
                    <p class="form_error" data-input="rgistration_request__phone">יש להזין מספר טלפון</p>
                </div>

                <input class="sf_btn" type="submit" id="submit_rgistration_request" value="הרשם">
                <div class="loader" id="rgistration_request__form_loader"><div class="spinner-loader"></div></div>
                <p id="form_submit_success">הודעה נשלחה בהצלחה!</p>
                <p id="form_submit_error"></p>
            </form>
        </div>
    </div>

</div>