<?php 
    if ( ! defined( 'ABSPATH' ) ) exit;

    if ( ! is_user_logged_in() ) {
        um_reset_user();
    } 
    if (is_user_logged_in() ) {
        wp_redirect ('./home');
    }

    $pageThum = get_the_post_thumbnail_url();
    
    $links = get_field('links');
?>

<div id="login-page" class="container">

    <div class="login__container">
        <div class="log_form">
            <img src="<?php bloginfo('template_url'); ?>/assets/img/somfy_new_logo-1.png" height="50" class="somfy_logo" alt="Somfy Logo" loading="lazy">
            <?php 
                echo do_shortcode( '[ultimatemember form_id="284"]' ); 
                echo "<h4 class='register_title' style='text-align: center;'>לקוח חדש? לחץ כאן להרשמה</h4>
                    <a class='um-button register_btn' alt='להצטרפות'>להצטרפות</a>";
            ?>
        </div>

        <div class="login_graphics">
            <img src="<?php echo $pageThum;?>" alt="<?php the_title(); ?>">
        </div>
    </div>

    <!-- Links -->
    <?php 
        if(count($links)>0){
            echo "<div class='loginPage_links__container'>";
                
            foreach($links as $link){
                echo "<a class='loginPage_link' href='".$link['link']['url']."'  target='".$link['link']['target']."' alt='".$link['link']['title']."'>
                    <img src='".$link['link_image']['url']."' alt='".$link['link']['title']."'>
                    <span class='loginPage_link__title'>".$link['link']['title']."</span>
                </a>";
            }
            echo "</div>";
        }; 
    ?>
</div>
<?php 
    get_template_part('templates/account/registration_popup');
?>
