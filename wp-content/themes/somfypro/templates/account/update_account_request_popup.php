<?php 
global $current_user;
wp_get_current_user();

$userID = get_current_user_id();
$userData = array(
    'user_email' => $current_user->user_email,
    'companyname' => isset(get_user_meta($userID, 'companyname')[0]) ? get_user_meta($userID, 'companyname')[0] : '',
    'mobile' => isset(get_user_meta($userID, 'custentity_il_mobile')[0]) ? get_user_meta($userID, 'custentity_il_mobile')[0] : '',
    'phone' => isset(get_user_meta($userID, 'phone')[0]) ? get_user_meta($userID, 'phone')[0] : ''
);

?>


<div id="update_account_request_popup" class="popup__overlay">
    
    <div class="popup__container">
        
        <div class="container_header">
            <h2>בקשה לעדכון פרטים</h2>
            <a class="popup__overlay__close update_account_request_popup__close" alt='סגור'>
                <i class="fas fa-times"></i>
            </a>
        </div>
        <div class="container_body">
            <h4 class="content_title">אנא מלא את כל השדות</h4>

            <form id="update_account_request__form">
                <div class="mb-3 form-input__group">
                    <input type="text" 
                            class="form-control form-control-lg" 
                            id="update_account_request_companyname" 
                            placeholder="שם החברה/עסק" minlength="3" required>
                    <p class="form_error" data-input="update_account_request_companyname">יש להזין שם חברה או עסק תיקנים</p>
                </div>

                <div class="mb-3 form-input__group">
                    <input type="email" 
                        class="form-control form-control-lg" 
                        id="update_account_request__email" 
                        placeholder='כתובת דוא"ל' required>
                    <p class="form_error" data-input="update_account_request__email">יש להזין כתובת דוא"ל</p>
                </div>

                <div class="mb-3 form-input__group">
                    <input type="tel" 
                        class="form-control form-control-lg" 
                        id="update_account_request__phone" 
                        placeholder="טלפון" minlength="8"  maxlength="10" required
                        onkeydown="if(event.key==='.'){event.preventDefault();}"  oninput="event.target.value = event.target.value.replace(/[^0-9]*/g,'');">
                    <p class="form_error" data-input="update_account_request__phone">יש להזין מספר טלפון</p>
                </div>

                <div class="mb-3 form-input__group">
                    <textarea 
                        class="form-control form-control-lg" 
                        id="update_account_request__message" 
                        placeholder="פירוט בקשה לעדכון" rows="3" required></textarea>
                    <p class="form_error" data-input="update_account_request__message">יש להזין פירוט בקשה</p>
                </div>

                <input class="sf_btn" type="submit" id="submit_update_account_request" value="שלח">
                <div class="loader" id="update_account_request__form_loader"><div class="spinner-loader"></div></div>
                <p id="update_account_request__form_submit_success">הודעה נשלחה בהצלחה!</p>
                <p id="update_account_request__form_submit_error"></p>
            </form>
            <input type="hidden" id="defualt_user_email" value="<?php echo $userData['user_email'] ?>">
            <input type="hidden" id="defualt_companyname" value="<?php echo $userData['companyname'] ?>">
            <input type="hidden" id="defualt_phone" value="<?php echo $userData['mobile'] == '' ? $userData['phone'] : $userData['mobile'];?>">
        </div>
    </div>

</div>