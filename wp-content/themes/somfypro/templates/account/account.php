<?php 

    if ( ! defined( 'ABSPATH' ) ) exit;

    // get page 
    $pageID = get_the_ID();
    $pageTitle = get_the_title($pageID);
    $thumbnail = get_the_post_thumbnail_url($pageID);
    $currentPagePermalink = get_the_permalink($pageID);
    global $current_user;
    $userID = get_current_user_id();

    $userData = array(
        'email' =>  $current_user->user_email,
        'companyname' => isset(get_user_meta($userID, 'companyname')[0]) ? get_user_meta($userID, 'companyname')[0] : '',
        'first_name' => isset(get_user_meta($userID, 'first_name')[0]) ? get_user_meta($userID, 'first_name')[0] : '',
        'last_name' => isset(get_user_meta($userID, 'last_name')[0]) ? get_user_meta($userID, 'last_name')[0] : '',
        'phone' => isset(get_user_meta($userID, 'phone')[0]) ? get_user_meta($userID, 'phone')[0] : '',
        'custentity_il_mobile' => isset(get_user_meta($userID, 'custentity_il_mobile')[0]) ? get_user_meta($userID, 'custentity_il_mobile')[0] : '',
        'user_billaddressee' => isset(get_user_meta($userID, 'billaddressee')[0]) ? get_user_meta($userID, 'billaddressee')[0] : '',
        'user_billaddress1' => isset(get_user_meta($userID, 'billaddress1')[0]) ? get_user_meta($userID, 'billaddress1')[0] : '',
        'user_billcity' => isset(get_user_meta($userID, 'billcity')[0]) ? get_user_meta($userID, 'billcity')[0] : '',
        'user_billcountry' => isset(get_user_meta($userID, 'billcountry')[0]) ? get_user_meta($userID, 'billcountry')[0] : '',
        'user_shipaddressee' => isset(get_user_meta($userID, 'shipaddressee')[0]) ? get_user_meta($userID, 'shipaddressee')[0] : '',
        'user_shipaddress1' => isset(get_user_meta($userID, 'shipaddress1')[0]) ? get_user_meta($userID, 'shipaddress1')[0] : '',
        'user_shipcity' => isset(get_user_meta($userID, 'shipcity')[0]) ? get_user_meta($userID, 'shipcity')[0] : '',
        'user_shipcountry' => isset(get_user_meta($userID, 'shipcountry')[0]) ? get_user_meta($userID, 'shipcountry')[0] : '',
        'terms' => get_field('terms', "user_$userID" ,true)
    );
?>
<div class="main_content__with_header">
    <div class="page_header"
        style="background: linear-gradient(-90deg, rgba(29, 26, 52, .7), rgba(29, 26, 52, .7)), url('<?php echo $thumbnail; ?>');
            background-size: cover;
            background-position: center center;
            background-repeat: no-repeat;">
        <ol class="breadcrumbs">
            <li><a href="<?php echo home_url(); ?>" alt='דף בית'>דף בית ></a></li>
            <li><a href="<?php echo $currentPagePermalink; ?>" alt='<?php echo $pageTitle; ?>'><?php echo $pageTitle; ?></a></li>
        </ol>
        <h2 class="page_title"><?php echo $pageTitle; ?></h2>
    </div>
    <div id="account-page" class='page_content__container flex__sp_btwn'>
        <div class="sidenavs_col">
            <div class="account_tabs__triggers">
                <a class="tab_trigger active" data-target="account_details__tab" alt='פרטי פרופיל'>פרטי פרופיל <i class="fas fa-chevron-left"></i></a>
                <a class="tab_trigger" data-target="password__tab" alt='סיסמא'>סיסמא <i class="fas fa-chevron-left"></i></a>
                <a class="tab_trigger" data-target="orders__tab" alt='הזמנות שלי'>הזמנות שלי <i class="fas fa-chevron-left"></i></a>
            </div>

            <?php get_template_part('templates/features/somfy_side_menus'); ?>
        </div>

        <div class="tabs_wraper">
            <div class="account_tabs__triggers account_tabs__triggers_mobile">
                <a class="tab_trigger active" data-target="account_details__tab" alt='פרטי פרופיל'>פרטי פרופיל <i class="fas fa-chevron-left"></i></a>
                <a class="tab_trigger" data-target="password__tab" alt='סיסמא'>סיסמא <i class="fas fa-chevron-left"></i></a>
                <a class="tab_trigger" data-target="orders__tab" alt='הזמנות שלי'>הזמנות שלי <i class="fas fa-chevron-left"></i></a>
            </div>
            
            <div class="account_tabs__tabs_container">
                <!-- פרטי חשבון -->
                <div class="account_tab__content active" id="account_details__tab">
                    <div class="tab_header__bar">
                        <h3 class="tab_title">פרטים</h3>
                        <a class="update_details_cta" alt='בקשה לעדכון'><i class="far fa-edit"></i> בקשה לעדכון</a>
                    </div>
                    <hr>

                    <div class="customer_account_details">
                        <div class="billing_address_details">
                            <h4>כתובת העסק</h4>
                            <p class="address_details">
                                <?php echo $userData['user_billaddressee'] 
                                    ."<br>" .$userData['user_billaddress1'] 
                                    ."<br>" .$userData['user_billcity'] 
                                    .", " .$userData['user_billcountry'] ;
                                ?>
                            </p>
                        </div>

                        <?php if($userData['user_shipaddressee'] != '' 
                            && $userData['user_shipaddress1'] != ''
                            && $userData['user_shipcity'] != ''): ?>
                        <div class="shipping_address_details">
                            <h4>כתובת למשלוח</h4>
                            <p class="address_details">
                                <?php echo $userData['user_shipaddressee'] 
                                    ."<br>" .$userData['user_shipaddress1'] 
                                    ."<br>" .$userData['user_shipcity'] 
                                    .", " .$userData['user_shipcountry'] ;
                                ?>
                            </p>
                        </div>
                        <?php endif; ?>

                    </div>


                    <div class="tab_header__bar">
                        <h3 class="tab_title">פרטי איש קשר</h3>
                        <a class="update_details_cta" alt='בקשה לעדכון'><i class="far fa-edit"></i> בקשה לעדכון</a>
                    </div>
                    <hr>
                    <ul class="customer_contact_details">
                        <li>
                            <h6 class="detail_title">שם</h6>
                            <p class="detail_val">
                                <?php 
                                    echo $userData['first_name'] 
                                        . ' ' . $userData['last_name']; 
                                    if($userData['companyname'] != '') echo "<br>" .$userData['companyname'];
                                ?>
                            </p>
                        </li>
                        <?php if($userData['phone'] != '' || $userData['custentity_il_mobile'] != ''): ?>
                        <li>
                            <h6 class="detail_title">טלפון נייד</h6>
                            <p class="detail_val">
                                <?php 
                                    echo $userData['custentity_il_mobile'];
                                    if($userData['phone'] != '') echo "<br>" .$userData['phone']; 
                                ?>
                            </p>
                        </li>
                        <?php endif; ?>
                        
                        <li>
                            <h6 class="detail_title">דוא"ל</h6>
                            <p class="detail_val"><?php echo $userData['email']; ?></p>
                        </li>
                    </ul>


                    <!-- customer trems -->
                    <div class="customer_terms">
                        <div class="tab_header__bar">
                            <h3 class="tab_title">אפשרויות תשלום</h3>
                            <a class="update_details_cta" alt='בקשה לעדכון'><i class="far fa-edit"></i> בקשה לעדכון</a>
                        </div>
                        <hr>
                        <ul>
                            <li>
                                אשראי
                                <img src="<?php bloginfo('template_url'); ?>/assets/img/icons/credit.png" alt="credit" loading="lazy">
                            </li>
                            <?php 
                                if($userData['terms']['label'] != '' && $userData['terms']['value'] != '-1'): 
                                    echo " <li>". $userData['terms']['label'] ."</li>";
                                endif; 
                            ?>
                        </ul>        
                    </div>
                </div>

                <!-- בקשה לעדכון סיסמא -->
                <div class="account_tab__content" id="password__tab">
                    <div class="tab_header__bar">
                        <h3 class="tab_title">עדכון סיסמא</h3>
                    </div>
                    <hr>
                    <?php echo do_shortcode( '[ultimatemember_password]' ); ?>
                    <hr>
                </div>

                <div class="account_tab__content" id="orders__tab">
                    <?php get_template_part('templates/shop/account_shoping_history'); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_template_part('templates/account/update_account_request_popup'); ?>
