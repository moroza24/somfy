<div id="calculators_popup" class="popup__overlay">
    
    <div class="popup__container">
        
        <div class="container_header">
            <a class="popup__overlay__close" alt='סגור'>
                <i class="fas fa-times"></i>
            </a>
            <div class="calculator__title_icon"></div>
            <h2 id='calculator__title'></h2>
        </div>
        <div class="container_body">

            <!-- Calculators Selector -->
            <div id="calculators_selector__container">
                <ul>
                    <li>
                        <a class="calculators_selector__trigger" data-calculator='1' alt="מחשבון תריסים">מחשבון תריסים</a>
                    </li>
                    <li>
                        <a class="calculators_selector__trigger" data-calculator='2' alt="מחשבון סוככים">מחשבון סוככים</a>
                    </li>
                </ul>
            </div>

            <!-- Calculators -->
            <div id="calculators_steps__container">
                <!-- Calc 1 -->
                <div id="calculator_1" class="calculator__container">
                    <!-- Step 1 -->
                    <form class="step__form_container hidden" data-step="1">
                        <div class="step_number">1</div>
                        <div class="step_content">
                            <h3 class="step_title">מאפייני חלון:</h3>
                            
                            <!-- רוחב -->
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">
                                        <span class="label">רוחב במ"מ</span>
                                        <img src="<?php bloginfo('template_url'); ?>/assets/calc_icons/width.svg" alt="width">
                                    </span>
                                </div>
                                <input type="number" 
                                    class="form-control" 
                                    id="calc_1__window_width"
                                    placeholder='רוחב במ"מ' 
                                    aria-label='רוחב במ"מ' 
                                    aria-describedby="basic-addon1" 
                                    min="400" 
                                    max="6500"
                                    step="1"
                                    required
                                    onkeydown="if(event.key==='.'){event.preventDefault();}"  oninput="event.target.value = event.target.value.replace(/[^0-9]*/g,'');">
                            </div>
                            <p class="form_error" data-for='calc_1__window_width'>חובה לסמן רוחב חלון. מ-400מ"מ עד 6500 מ"מ</p>

                            <!-- גובה -->
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon2">
                                        <span class="label">גובה במ"מ</span>
                                        <img src="<?php bloginfo('template_url'); ?>/assets/calc_icons/height.svg" alt="height">
                                    </span>
                                </div>
                                <input type="number" 
                                    class="form-control" 
                                    id="calc_1__window_height"
                                    placeholder='גובה במ"מ' 
                                    aria-label='גובה במ"מ' 
                                    aria-describedby="basic-addon2" 
                                    min="600" 
                                    max="3400"
                                    step="1" 
                                    required
                                    onkeydown="if(event.key==='.'){event.preventDefault();}"  oninput="event.target.value = event.target.value.replace(/[^0-9]*/g,'');">
                            </div>
                            <p class="form_error" data-for='calc_1__window_height'>חובה לסמן גובה חלון. מ-600 מ"מ עד 3400 מ"מ</p>

                            <div class="step_actions">
                                <button class="sf_btn next_step" data-calculator='1' data-next='2'>הבא</button>
                            </div>
                        </div>
                    </form>

                    <!-- Step 2 -->
                    <form class="step__form_container hidden" data-step="2">
                        <div class="step_number">2</div>
                        <div class="step_content">
                            <h3 class="step_title">מאפייני התריס:</h3>
                            
                            <!-- סוג שלב -->
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="calc_1__blind_type">
                                        <img src="<?php bloginfo('template_url'); ?>/assets/calc_icons/step_height.svg" alt="step_height">
                                    </label>
                                </div>
                                <select class="custom-select custom-select-lg" id="calc_1__blind_type" required>
                                    <option value="-1">סוג שלב</option>
                                    <option value="1">משוך</option>
                                    <option value="2">מוקצף</option>
                                    <option value="3">סורג</option>
                                </select>
                            </div>
                            <p class="form_error" data-for='calc_1__blind_type'>חובה לבחור סוג שלב</p>

                            <!-- גובה שלב -->
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="calc_1__blind_height">
                                        <img src="<?php bloginfo('template_url'); ?>/assets/calc_icons/step_height.svg" alt="step_height">
                                    </label>
                                </div>
                                <select class="custom-select custom-select-lg" id="calc_1__blind_height" required>
                                    <option value="-1">גובה שלב</option>
                                </select>
                            </div>
                            <p class="form_error" data-for='calc_1__blind_height'>חובה לבחור גובה שלב תריס</p>

                            <!-- משקל -->
                            <div class="input-group mb-3 weights_inputs">
                                <div class="input-group-prepend prepend-full">
                                    <span class="input-group-text" id="basic-addon3">
                                        <img src="<?php bloginfo('template_url'); ?>/assets/calc_icons/weight.svg" alt="weight">
                                        משקל למ"ר/כללי
                                    </span>
                                </div>
                                <span id='calc_1__blinds_mr_weight__title'></span>
                                <span id='calc_1__blinds_weight__title'></span>
                                <input type="number" style="display: none;" 
                                    class="form-control" 
                                    id="calc_1__blinds_mr_weight"
                                    aplaceholder='משקל למ"ר' 
                                    aria-label='משקל למ"ר' 
                                    aria-describedby="basic-addon3"
                                    min="0"
                                    step="0.01"
                                    required
                                    disabled>
                                <input type="number"  style="display: none;"
                                    class="form-control" 
                                    id="calc_1__blinds_weight"
                                    aria-label='משקל כללי' 
                                    aria-describedby="basic-addon3"
                                    min="0"
                                    step="0.01"
                                    required
                                    disabled>
                            </div>

                            <div class="step_actions">
                                <button class="sf_btn prev_step" data-calculator='1' data-prev='1'>הקודם</button>
                                <button class="sf_btn next_step" data-calculator='1' data-next='3'>הבא</button>
                            </div>
                            <p class="note">* שים לב! משקל התריס ייקבע ע"פ נתוני ייצרן השלבים</p>
                        </div>
                    </form>

                    <!-- Step 3 -->
                    <form class="step__form_container hidden" data-step="3">
                        <div class="step_number">3</div>
                        <div class="step_content">
                            <h3 class="step_title">מאפייני המנוע:</h3>

                            <!-- קוטר צינור -->
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="calc_1__pipe_diameter">
                                        <img src="<?php bloginfo('template_url'); ?>/assets/calc_icons/pipe.svg" alt="pipe">
                                    </label>
                                </div>
                                <select class="custom-select custom-select-lg" id="calc_1__pipe_diameter" required>
                                    <option value="-1">קוטר צינור</option>
                                </select>
                            </div>
                            <p class="form_error" data-for='calc_1__pipe_diameter'>חובה לבחור קוטר צינור</p>

                            <!-- טכנולוגיה -->
                            <div class="input-group mb-3 input-group_radio">
                                <div class="input-group-prepend prepend-full">
                                    <span class="input-group-text input-group-radio">
                                        <img src="<?php bloginfo('template_url'); ?>/assets/calc_icons/wired_1.svg" alt="wired_1">
                                        טכנולוגיה
                                    </span>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" 
                                        name="calc_1__tech" id="calc_1__tech_wired" value="4" data-title='מחווט'  required>
                                    <label class="form-check-label" for="calc_1__tech_wired">מחווט</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" 
                                        name="calc_1__tech" id="calc_1__tech_wireless" data-title='רדיו'  value="2">
                                    <label class="form-check-label" for="calc_1__tech_wireless">רדיו</label>
                                </div>
                            </div>
                            <p class="form_error" data-for='calc_1__tech'>חובה לבחור טכנולוגיה</p>

                            <!-- גיבוי ידני -->
                            <div class="input-group mb-3 input-group_radio">
                                <div class="input-group-prepend prepend-full">
                                    <span class="input-group-text input-group-radio">
                                        <img src="<?php bloginfo('template_url'); ?>/assets/calc_icons/backup.svg" alt="backup">
                                        גיבוי ידני
                                    </span>
                                </div>
                                
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" id="calc_1__pipe_backup" value="true">
                                    <label class="form-check-label" for="calc_1__pipe_backup"></label>
                                </div>
                            </div>

                            <!-- סלד -->
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="calc_1__rpm">
                                        <img src="<?php bloginfo('template_url'); ?>/assets/calc_icons/rpm.svg" alt="rpm">
                                    </label>
                                </div>
                                <select class="custom-select custom-select-lg" id="calc_1__rpm" required>
                                    <option value="-1">סל"ד</option>
                                    <option value="5">12</option>
                                    <option value="4">17</option>
                                </select>
                            </div>
                            <p class="form_error" data-for='calc_1__rpm'>חובה לבחור סל"ד</p>

                            <div class="step_actions">
                                <button class="sf_btn prev_step" data-calculator='1' data-prev='2'>הקודם</button>
                                <button class="sf_btn submit_calc" data-calculator='1'>מצא לי מנוע</button>
                            </div>
                            <div class="loader" id="calc_1__loader"><div class="spinner-loader"></div></div>
                            <p id="calc_1__error"></p>
                        </div>
                    </form>
                </div>

                <!-- Calc 2 -->
                <div id="calculator_2" class="calculator__container">
                    <!-- Step 1 -->
                    <form class="step__form_container hidden" data-step="1">
                        <div class="step_number">1</div>
                        <div class="step_content">
                            <h3 class="step_title">מאפייני הסוכך:</h3>
                            
                            <!-- פתיחה -->
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="calc_2__screen_opening">
                                        <img src="<?php bloginfo('template_url'); ?>/assets/calc_icons/opening.svg" alt="opening">
                                    </label>
                                </div>
                                <select class="custom-select custom-select-lg" id="calc_2__screen_opening" required>
                                    <option value="-1">פתיחה</option>
                                    <option value="1">עד 1.5 מ'</option>
                                    <option value="2">1.5  מ' עד 2.5 מ'</option>
                                    <option value="3">2.5  מ' עד 3.5 מ'</option>
                                    <option value="4">3.5  מ' עד 5 מ'</option>
                                </select>
                            </div>
                            <p class="form_error" data-for='calc_2__screen_width'>חובה לבחור פתיחה</p>

                            <!-- רוחב -->
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="calc_2__screen_width">
                                        <img src="<?php bloginfo('template_url'); ?>/assets/calc_icons/width.svg" alt="width">
                                    </label>
                                </div>
                                <select class="custom-select custom-select-lg" id="calc_2__screen_width" required>
                                    <option value="-1">רוחב הסוכך</option>
                                    <option value="1" style="display: none;">0 - 6</option>
                                    <option value="2" style="display: none;">6 - 12</option>
                                    <option value="3" style="display: none;">12 - 15</option>
                                    <option value="4" style="display: none;">15 - 20</option>
                                    <option value="5" style="display: none;">20 - 25</option>
                                    <option value="6" style="display: none;">25 - 30</option>
                                </select>
                            </div>
                            <p class="form_error" data-for='calc_2__screen_width'>חובה לבחור רוחב הסוכך</p>


                            <!-- מס זרועות -->
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="calc_2__arms">
                                        <img src="<?php bloginfo('template_url'); ?>/assets/calc_icons/arms.svg" alt="arms">
                                    </label>
                                </div>
                                <select class="custom-select custom-select-lg" id="calc_2__arms" required>
                                    <option value="-1">מספר זרועות</option>
                                    <option value="2" style="display: none;">2</option>
                                    <option value="4" style="display: none;">4</option>
                                    <option value="6" style="display: none;">6</option>
                                    <option value="8" style="display: none;">8</option>
                                    <option value="10" style="display: none;">10</option>
                                    <option value="12" style="display: none;">12</option>
                                </select>
                            </div>
                            <p class="form_error" data-for='calc_2__arms'>חובה לבחור מספר זרועות</p>

                            <div class="step_actions">
                                <button class="sf_btn next_step" data-calculator='2' data-next='2'>הבא</button>
                            </div>
                            <p id="no_matching_pipes_error">לא נמצא התאמה לקוטר צינור</p>
                        </div>
                    </form>
                    
                    <!-- Step 3 -->
                    <form class="step__form_container hidden" data-step="2">
                        <div class="step_number">2</div>
                        <div class="step_content">
                            <h3 class="step_title">מאפייני המנוע:</h3>

                            <!-- קוטר צינור -->
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="calc_2__pipe_diameter">
                                        <img src="<?php bloginfo('template_url'); ?>/assets/calc_icons/pipe.svg" alt="pipe">
                                    </label>
                                </div>
                                <select class="custom-select custom-select-lg" id="calc_2__pipe_diameter" required>
                                    <option value="-1">קוטר צינור</option>
                                </select>
                            </div>
                            <p class="form_error" data-for='calc_2__pipe_diameter'>חובה לבחור קוטר צינור</p>

                            <!-- טכנולוגיה -->
                            <div class="input-group mb-3 input-group_radio">
                                <div class="input-group-prepend prepend-full">
                                    <span class="input-group-text input-group-radio">
                                        <img src="<?php bloginfo('template_url'); ?>/assets/calc_icons/wired_2.svg" alt="wired_2">
                                        טכנולוגיה
                                    </span>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" 
                                        name="calc_2__tech" id="calc_2__tech_wired" value="4" data-title='מחווט' required>
                                    <label class="form-check-label" for="calc_2__tech_wired">מחווט</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" 
                                        name="calc_2__tech" id="calc_2__tech_wireless" value="2" data-title='רדיו' >
                                    <label class="form-check-label" for="calc_2__tech_wireless">רדיו</label>
                                </div>
                            </div>
                            <p class="form_error" data-for='calc_2__tech'>חובה לבחור טכנולוגיה</p>

                            <!-- גיבוי ידני -->
                            <div class="input-group mb-3 input-group_radio">
                                <div class="input-group-prepend prepend-full">
                                    <span class="input-group-text input-group-radio">
                                        <img src="<?php bloginfo('template_url'); ?>/assets/calc_icons/backup.svg" alt="backup">
                                        גיבוי ידני
                                    </span>
                                </div>
                                
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" id="calc_2__pipe_backup" value="true">
                                    <label class="form-check-label" for="calc_2__pipe_backup"></label>
                                </div>
                            </div>


                            <div class="step_actions">
                                <button class="sf_btn prev_step" data-calculator='2' data-prev='1'>הקודם</button>
                                <button class="sf_btn submit_calc" data-calculator='2'>מצא לי מנוע</button>
                            </div>
                            <div class="loader" id="calc_2__loader"><div class="spinner-loader"></div></div>
                            <p id="calc_2__error"></p>
                        </div>
                    </form>
                </div>
            </div>


        </div>
    </div>
</div>