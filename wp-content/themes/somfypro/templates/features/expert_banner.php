<div id="expert_banner">
    <div class="expert_banner_container">
        <div class="expert_banner__content">
            <h3>הצטרפות לאקספרט</h3>
            <div>
                <p>
                    לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ
                </p>
                <div class="expert_banner_actions">
                    <a class="sf_btn sf_btn-inline" id='sign_to_expert_cta' alt='להצטרפות'>להצטרפות</a>
                    <div class="loader" id="expert_banner_loader"><div class="spinner-loader"></div></div>
                </div>
                <p class="expert_banner__message error" id="expert_banner_error"></p>
                <p class="expert_banner__message" id="expert_banner_success">לקוח יקר, פנייתך התקבלה ותענה בימים הקרובים</p>
                
            </div>
        </div>
        
    </div>
</div>