<?php 
    global $current_user;
    wp_get_current_user();

    $userID = get_current_user_id();

    $userData = array(
        'user_email' => $current_user->user_email,
        'user_nickname' => $current_user->nickname,
        'user_first_name' => isset(get_user_meta($userID, 'first_name')[0]) ? get_user_meta($userID, 'first_name')[0] : '',
        'user_last_name' => isset(get_user_meta($userID, 'last_name')[0]) ? get_user_meta($userID, 'last_name')[0] : '',
        'user_shipaddressee' => isset(get_user_meta($userID, 'shipaddressee')[0]) ? get_user_meta($userID, 'shipaddressee')[0] : '',
        'user_shipaddress1' => isset(get_user_meta($userID, 'shipaddress1')[0]) ? get_user_meta($userID, 'shipaddress1')[0] : '',
        'user_shipcity' => isset(get_user_meta($userID, 'shipcity')[0]) ? get_user_meta($userID, 'shipcity')[0] : '',
        'user_shipcountry' => isset(get_user_meta($userID, 'shipcountry')[0]) ? get_user_meta($userID, 'shipcountry')[0] : '',
        'vatregnumber' => isset(get_user_meta($userID, 'vatregnumber')[0]) ? get_user_meta($userID, 'vatregnumber')[0] : '',
        'phone' => isset(get_user_meta($userID, 'phone')[0]) ? get_user_meta($userID, 'phone')[0] : '',
        'custentity_il_mobile' => isset(get_user_meta($userID, 'custentity_il_mobile')[0]) ? get_user_meta($userID, 'custentity_il_mobile')[0] : '',
        'companyname' => isset(get_user_meta($userID, 'companyname')[0]) ? get_user_meta($userID, 'companyname')[0] : ''
    );

    if($userData['user_first_name'] != '' && $userData['user_last_name'] != ''){
        $contactname = $userData['user_first_name'] . ' ' . $userData['user_last_name'];
    } else if($userData['companyname'] != ''){
        $contactname = $userData['companyname'];
    } else {
        $contactname = $userData['user_nickname'];
    }

    $address = $userData['user_shipaddressee'] . ', ' .$userData['user_shipaddress1'] . ', ' . $userData['user_shipcity'] . ' ' . $userData['user_shipcountry'];

    $phone = $userData['phone'] != '' ? $userData['phone'] : $userData['custentity_il_mobile'];
?>


<div class="rma__container">
        <a class='sf_btn sf_btn-inline' id='rma_form_trigger' alt='להחזרת סחורה לחץ כאן'>להחזרת סחורה לחץ כאן</a>

        <div id="rma__form_container">
            <h3 class="form_title">טופס החזרת סחורה</h3>
            <p id="rma_id"></p>


            <!-- Customer Details -->
            <div class="rma_form__customer_details">
                <div class="col">
                    <div class="form-group">
                        <label for="customer_details__vatregnumber">מספר החברה (ח.פ)</label>
                        <input type="text" class="form-control" id="customer_details__vatregnumber" disabled value="<?php echo $userData['vatregnumber'];?>">
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="customer_details__companyname">שם החברה</label>
                        <input type="text" class="form-control" id="customer_details__companyname" disabled value="<?php echo $userData['companyname'];?>">
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="customer_details__address">כתובת</label>
                        <input type="text" class="form-control" id="customer_details__address" disabled value="<?php echo $address;?>">
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="customer_details__contactname">שם איש קשר</label>
                        <input type="text" class="form-control" id="customer_details__contactname"  value="<?php echo $contactname;?>">
                    </div>
                    <p class="form_error" data-input='customer_details__contactname'>יש להזין שם איש קשר</p>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="customer_details__email">כתובת דוא’’ל</label>
                        <input type="email" class="form-control" id="customer_details__email" disabled value="<?php echo $userData['user_email'];?>">
                    </div>
                    <p class="form_error" data-input='customer_details__email'>יש להזין כתובת מייל תיקנית</p>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="customer_details__phone">מספר טלפון</label>
                        <input type="tel" class="form-control" id="customer_details__phone" value="<?php echo $phone;?>" minlength="8"  maxlength="10"
                            onkeydown="if(event.key==='.'){event.preventDefault();}"  oninput="event.target.value = event.target.value.replace(/[^0-9]*/g,'');">
                    </div>
                    <p class="form_error" data-input='customer_details__phone'>יש להזין טלפון תיקני</p>
                </div>
            </div>


            <!-- RMA Types -->
            <h3 class="rma_types__title">סוגי החזרת מוצרים:</h3>
            <div class="rma_types__container">
                <span>א. המוצר אינו תקין ועומד בתנאי האחריות</span>
                <span>ב. טעות של סומפי במשלוח/חשבונית</span>
                <span>ג. טעות לקוח בעת ההזמנה/אחר</span>
            </div>

            <!-- RMA Items forms -->
            <form id="new_rma_item__form">
                <div class="row">
                    <div class="col">
                        <div class="form-group with_autoload">
                            <label for="new_rma_item__itemid">מק"ט</label>
                            <input type="text" class="form-control product_autoloader" 
                                data-product='' data-autoload="true" 
                                id="new_rma_item__itemid">
                        </div>
                        <p class="form_error" data-input='new_rma_item__itemid'>יש לבחור מק"ט מהרשימה</p>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="new_rma_item__title">תיאור מוצר</label>
                            <input type="text" class="form-control" data-source='new_rma_item__itemid' disabled id="new_rma_item__title">
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="new_rma_item__amount">כמות</label>
                            <input type="number" min="1" step='1' class="form-control" id="new_rma_item__amount">
                        </div>
                        <p class="form_error" data-input='new_rma_item__amount'>יש להזין כמות</p>
                    </div>
                    <div class="col rma_options-col">
                        <p class="rma_options-label">סיבת החזר</p>
                        <div class="btn-group btn-group-toggle rma_options" data-toggle="buttons">
                            <label class="btn btn-secondary active">
                                <input type="radio" name="new_rma_item__type_option" value="rma_type1" id="new_rma_item__type_option1" autocomplete="off" checked>א
                            </label>
                            <label class="btn btn-secondary">
                                <input type="radio" name="new_rma_item__type_option" value="rma_type2" id="new_rma_item__type_option2" autocomplete="off">ב
                            </label>
                            <label class="btn btn-secondary">
                                <input type="radio" name="new_rma_item__type_option" value="rma_type3" id="new_rma_item__type_option3" autocomplete="off">ג
                            </label>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group with_autoload">
                            <label for="new_rma_item__inv">חשבונית</label>
                            <input type="text" class="form-control inv-auto" 
                                data-tranid='' data-autoload="true" 
                                id="new_rma_item__inv">
                        </div>
                        <p class="form_error" data-input='new_rma_item__inv'>יש להזין חשבונית</p>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="new_rma_item__amount_2_pick">כמות לאיסוף</label>
                            <input type="number" min="1" step='1' class="form-control" id="new_rma_item__amount_2_pick">
                        </div>
                        <p class="form_error" data-input='new_rma_item__amount_2_pick'>יש להזין כמות לאיסוף</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <textarea class="form-control" id="new_rma_item__description" rows="5" placeholder="פרט את סיבת ההחזר"></textarea>
                        </div>
                        <p class="form_error" data-input='new_rma_item__description'>יש להזין פירוט סיבת החזרה</p>
                    </div>
                </div>
                <input type="hidden" class="form-control" id="new_rma_item__productid">
                <button class="sf_btn" id="new_rma_item__form_submit">
                    <i class="fas fa-plus"></i>הוסף שורת החזרה</button>
            </form>


            <!-- Added RMA Items -->
            <div class="added_rma_items__container">
                <table class="table" id="added_rma_items__table">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col" style="display: none;">productid</th>
                            <th scope="col">מק"ט</th>
                            <th scope="col">שם מוצר</th>
                            <th scope="col">כמות</th>
                            <th scope="col">כמות לאיסוף</th>
                            <th scope="col">חשבונית</th>
                            <th scope="col">סיבת החזרה</th>
                            <th scope="col">פירוט</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody data-for="added_rma_items__table">
                        
                    </tbody>
                </table>
            </div>
            
            <!-- Instructions -->
            <div class="rma_istructions">
                <ul>
                    <li>
                        <strong>לתשומת לבכם: </strong>מדיניות האחריות של סומפי מפורטת בתעודת האחריות אשר מגיעה עם המנוע וטופס זה אינו מגדיר את מדיניות
                        האחריות של סומפי. לפני החזרת מוצר אנא בדקו את תעודת האחריות וודאו שאכן התנאים מתקיימים. מנוע שאינו עומד באחריות
                        (שבר, נזק, חדירת מים, חריטה ועוד...) לא יטופל במעבדה. לצורך בירור עמידה באחריות אתם מוזמנים לפנות למעבדה בטל' ישיר
                        שמספרו  073-82354548 ונציגנו ישמחו לתת תשובה בנושא.                    
                    </li>
                    <li>
                        אין החזרה של מתאמים.
                    </li>
                    <li>
                        לשאלות נוספות ובירורים אתם מוזמנים לפנות לקשרי לקוחות בטל' 073-82354548.
                    </li>
                </ul>
            </div>


            <!-- Submit RMA form -->
            <div class="rma_actions">
                <button class='sf_btn sf_btn-inline' disabled id='rma_form_submit'>שלח</button>
            </div>
            <div class="loader" id="rma_form_submit__loader"><div class="spinner-loader"></div>המערכת שולחת את הטופס, אנא המתן</div>
            <p id="rma_form_submit__error"></p>
        </div>
        <p id="rma_form_submit__success">הטופס נשלח בהצלחה!</p>
</div>
