<?php
    $frontpage_id = get_option( 'page_on_front' );

    $sideNav1 = get_field('home_account_side_menu', $frontpage_id);
    $sideNav2 = get_field('home_somfy_actions_side_menu', $frontpage_id);

    global $post;
    $pageID = $post->ID;

?>
<!-- side_menus -->
<div class="somfy_side__menus">
    <!-- Account menu -->
    <?php if (isset($sideNav1) && $sideNav1['side_nav_links_list'] != ''): ?>
        <div class="somfy_side_nav" id="account__side_nav">
            <h3 class="side_nav__title"><?php echo $sideNav1['side_nav_title']; ?></h3>
            <hr>
            <ul class="side_nav__links_list">
                <?php 
                    foreach ($sideNav1['side_nav_links_list'] as $item) {
                        echo "<li class='side_nav__item'>
                            <a href='".$item['side_nav_link']['url']."' alt='".$item['side_nav_link']['title']."'>
                                <span class='side_nav__item_icon'><img src='".$item['side_nav_icon']."'></span>
                                <span class='side_nav__item_title'>
                                    ".$item['side_nav_link']['title']."
                                    <i class='fas fa-chevron-left'></i>
                                </span>
                            </a>
                        </li>";
                    }
                ?>
            </ul>
        </div>
    <?php endif; ?>


    <!-- Somy Actions menu -->
    <?php if (isset($sideNav2) && $sideNav2['side_nav_links_list'] != ''): ?>
        <div class="somfy_side_nav" id="somfy_actions__side_nav">
            <h3 class="side_nav__title"><?php echo $sideNav2['side_nav_title']; ?></h3>
            <hr>
            <ul class="side_nav__links_list">
                <?php 
                    foreach ($sideNav2['side_nav_links_list'] as $item) {
                        if (is_front_page() && $item['side_nav_link']['title'] == 'מעקב הזמנות') continue;
                        echo "<li class='side_nav__item'>
                            <a href='".$item['side_nav_link']['url']."' alt=' ".$item['side_nav_link']['title']."'>
                                <span class='side_nav__item_icon'><img src='".$item['side_nav_icon']."'></span>
                                <span class='side_nav__item_title'>
                                    ".$item['side_nav_link']['title']."
                                    <i class='fas fa-chevron-left'></i>
                                </span>
                            </a>
                        </li>";
                    }
                ?>
            </ul>
        </div>
    <?php endif; ?>
</div>