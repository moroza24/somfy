<div id="mobile-menu__overlay">
    <div class="mobile-menu__top">

        <a class="side_icon calculator_trigger calculator_trigger__mobile_nav" alt="calculator">
            <div class="icon"></div>
            <span>
                מחשבון<br>בחירת מנוע
            </span>   
        </a>

        <a class="mobile-menu__close" alt='סגור'>
            <i class="fas fa-times"></i>
        </a>
    </div>

    
    <?php
        use Inc\Classes\Shop\CustomerPRO;
        $CustomerPRO = new CustomerPRO();


        $customerTaxonomy = $CustomerPRO->getCustomerTaxonomy();
        $userCategories = $customerTaxonomy['categories'];
        $userSubgroup = $customerTaxonomy['sub_group'];
        $userSub2group = $customerTaxonomy['sub2group'];

        $shopUrl = home_url() .'/shop';

        // Get pro categories
        $categories = new WP_Query(array(
            'post_type' => 'product_category',
            'posts_per_page'   => -1,
            'meta_key' => 'sort_order',
            'orderby' => 'meta_value_num',
            'order' => 'ASC',
            'meta_query' => array(
                array(
                    'key' => 'show_on_site',
                    'compare' => '=',
                    'value' => TRUE
                )
            )
        )); 
        if($categories){

            $InactiveCategoriesHtmlArr = array();
            echo "<div class='accordion accordion-flush' id='mobile_category__accordion'>";
                    
            while ($categories->have_posts()) {
                $categories->the_post();

                $categoryTitle = get_the_title();
                $categoryID = get_the_ID();
                $groups = get_field('sub_groups');

                // mega menu banner
                $megaImg = get_field('mega_img');
                $megaLink = get_field('mega_link');
                
                $disabled = '';
                $activeCategory = true;
                $restrictionClass = '';
                if(!in_array($categoryID, $userCategories)){
                    $restrictionClass = 'accordion-button__inactive';
                    $disabled = 'disabled';
                    $activeCategory = false;
                }
                $categoryHtml = '';

                $categoryHtml .= "<div class='accordion-item'>";

                if($groups){
                    // category Trigger
                    $categoryHtml .= "<h2 class='accordion-header' id='flush-headingCategory$categoryID'>
                            <button class='accordion-button $restrictionClass collapsed' type='button' 
                                data-bs-toggle='collapse' 
                                $disabled
                                data-bs-target='#flush-collapseCategory$categoryID' 
                                aria-expanded='false' 
                                aria-controls='flush-collapseCategory$categoryID'>
                                $categoryTitle
                            </button>
                        </h2>";
                    $categoryHtml .= "<div id='flush-collapseCategory$categoryID' class='accordion-collapse collapse' 
                            aria-labelledby='flush-headingCategory$categoryID' 
                            data-bs-parent='#mobile_category__accordion'>
                            <div class='accordion-body'>";
                
                    // Groups
                    $categoryHtml .= "<div class='accordion accordion-flush' id='mobile_groups__accordion$categoryID'>";
                    foreach ($groups as $group){
                        $groupID = $group->ID;
                        $groupTitle = get_the_title($groupID);
                        $groupPermalink = "$shopUrl?category=$categoryID&subgroup=$groupID";
                        $subgroups = get_field('2_subgroups', $groupID);
                        
                        $groupRestrictionClass = '';
                        $groupDisabled = '';
                        if(!in_array($groupID, $userSubgroup)){
                            $groupPermalink = '';
                            $groupRestrictionClass = 'accordion-button__inactive';
                            $groupDisabled = 'disabled';
                        }
                        
                        $categoryHtml .= "<div class='accordion-item'>";
                        if($subgroups){
                            // category Trigger
                            $categoryHtml .= "<h2 class='accordion-header' id='flush-headingGroup$categoryID-$groupID'>
                                <button class='accordion-button $groupRestrictionClass collapsed' type='button' 
                                    $groupDisabled
                                    data-bs-toggle='collapse' 
                                    data-bs-target='#flush-collapseGroup$categoryID-$groupID' 
                                    aria-expanded='false' 
                                    aria-controls='flush-collapseGroup$categoryID-$groupID'>
                                    $groupTitle
                                </button>
                            </h2>";
                            $categoryHtml .= "<div id='flush-collapseGroup$categoryID-$groupID' class='accordion-collapse collapse' 
                                    aria-labelledby='flush-headingGroup$categoryID-$groupID' 
                                    data-bs-parent='#mobile_Group__accordion'>
                                    <div class='accordion-body'>";
                                    
                            // subgroups
                            foreach ($subgroups as $subgroup){
                                $sub_subgroupID = $subgroup->ID;
                                $subgroupTitle = get_the_title($subgroup->ID);
                                $subgroupPermalink = "$shopUrl?category=$categoryID&subgroup=$groupID&subsubgroup=$sub_subgroupID";

                                $sub_subgroupRestrictionClass = '';
                                if(!in_array($sub_subgroupID, $userSub2group)){
                                    $sub_subgroupRestrictionClass = 'subgroup_button__inactive';
                                    $subgroupPermalink = '';
                                }

                                $categoryHtml .= "<a class='subgroup_button $sub_subgroupRestrictionClass' href='$subgroupPermalink' alt='$subgroupTitle'>
                                        <i class='fas fa-angle-left'></i>
                                        $subgroupTitle
                                    </a>";
                            }

                            $categoryHtml .= "</div>
                            </div>";
                        } else {
                            $categoryHtml .= "<h2 class='accordion-header' id='flush-headingGroup$categoryID-$groupID'>
                                <a href='$groupPermalink' class='accordion-button $groupRestrictionClass group-button' alt='$groupTitle'>
                                    $groupTitle
                                </a>
                            </h2>";
                        }
                        $categoryHtml .= "</div>";
                        wp_reset_postdata();
                    }
                    $categoryHtml .= "</div>";
                    
                    // Print subgroups
                    if($megaLink && $megaImg):
                        $categoryHtml .= "<div class='megamenu_mobil__banner_container'>
                        <a href='".$megaLink['url']."' alt='".$megaLink['title']."' target='".$megaLink['target']."'>
                            <img src='".$megaImg['url']."' alt='".$megaLink['title']."'>
                        </a>
                        </div>";
                    endif;

                    $categoryHtml .= "</div>
                    </div>";
                } else {
                    $categoryHtml .= "<h2 class='accordion-header' id='flush-headingCategory$categoryID'>
                        <a href='$permalink' class='accordion-button category-button' alt='$categoryTitle'>
                            $categoryTitle
                        </a>
                    </h2>";
                }

                $categoryHtml .=  "</div>";
                wp_reset_postdata();
                
                if($activeCategory){
                    echo $categoryHtml;
                } else {
                    array_push($InactiveCategoriesHtmlArr, $categoryHtml);
                }
            }
            foreach ($InactiveCategoriesHtmlArr as $cat){
                echo $cat;
            }

            echo "</div>";
        }
        wp_reset_postdata();

    ?>


<div id="side_nav" >
    <div class="flex__cont flex__al_c flex__sp_btwn">

    <!-- Global Search -->
    <?php get_template_part('templates/features/global_search'); ?>

    <div class="side_icon " alt="תפריט משתמש">
        <a id="user-menu__trigger" class='user_trigger_mobile' alt='תפריט'>
            <?php
                $userID = get_current_user_id();
                if($userID){
                    $nickname = isset(get_user_meta($userID, 'nickname')[0]) ? get_user_meta($userID, 'nickname')[0] : '';
    
                    echo "<span>שלום, " . $nickname . "</span>";
                }
            ?>
            <img src="<?php bloginfo('template_url'); ?>/assets/svg/003-user-profile.svg" alt="user-menu" loading="lazy">
        </a>
        
        <?php get_template_part('templates/nav/user-menu_dropdown'); ?>
    </div>
    <a class="side_icon updates_link"  alt="עדכונים מסומפי" href="<?php echo home_url(); ?>/somfy-updates" style="line-height: 0px;">
        <i class="fas fa-globe"></i>
    </a>
    <a class="side_icon cart__trigger" alt="עגלת קניות" href="<?php echo home_url(); ?>/cart">
        <img class='not-active' src="<?php bloginfo('template_url'); ?>/assets/svg/002-shopping-cart.svg" alt="cart" loading="lazy">        
    </a>
    <a class="side_icon" id="wishlist__trigger" href="<?php echo home_url(); ?>/wishlist" alt="wishlist">
        <img src="<?php bloginfo('template_url'); ?>/assets/svg/001-heart-shape-outline.svg" alt="wishlist" loading="lazy">
    </a>

    </div>

</div>

</div>