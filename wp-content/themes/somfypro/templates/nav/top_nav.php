<?php
    if(is_user_logged_in()){

    $userID = get_current_user_id();
    $isExpert = false;
    if($userID){
        $hirarchy = get_field('custentity_hirarchy', "user_$userID", false);
        if($hirarchy == 1) $isExpert = true;
    }
?>
<div id="top-menu">
    <nav class="navbar navbar-light" id="top_nav">
        
        <!-- Logo -->
        <a class="navbar-brand" href="<?php echo home_url(); ?>" alt='בית'>
            <?php if($isExpert){ ?>
                <img src="<?php bloginfo('template_url'); ?>/assets/img/logo_expert-1.png" id="logo_expert" height="50" class="d-inline-block align-top" alt="Somfy Logo" loading="lazy">
                <img src="<?php bloginfo('template_url'); ?>/assets/img/somfy_new_logo-1.png" id="somfy_new_logo_with_expert" height="50" class="d-inline-block align-top" alt="Somfy Logo" loading="lazy">
            <?php } else { ?>
                <img src="<?php bloginfo('template_url'); ?>/assets/img/somfy_new_logo-1.png" id="somfy_new_logo"  height="50" class="d-inline-block align-top" alt="Somfy Logo" loading="lazy">
            <?php } ?>
        </a>
        
        <!-- side_nav -->
        <?php get_template_part('templates/nav/side_nav'); ?>
    </nav>


    <a class="side_icon calculator_trigger calculator_trigger__top_nav" alt="calculator">
        <div class="icon"></div>
        <span>
            מחשבון<br>בחירת מנוע
        </span>   
    </a>
</div>

<!-- Mobile menu overlay -->
<?php get_template_part('templates/nav/mobile-menu'); ?>

<!-- Mobile global search -->
<?php get_template_part('templates/features/mobile-global_search'); ?>

<?php
    };
?>