<!-- <div class="is_desktop">
</div> -->

<div id="side_nav" class='is_desktop'>
    <div class="flex__cont flex__al_c flex__sp_btwn">


        <!-- Global Search -->
        <?php get_template_part('templates/features/global_search'); ?>

        <div class="side_icon ">
            <a id="user-menu__trigger" alt="תפריט משתמש">
                <?php
                    $userID = get_current_user_id();
                    if($userID){
                        $nickname = isset(get_user_meta($userID, 'nickname')[0]) ? get_user_meta($userID, 'nickname')[0] : '';
        
                        echo "<span>שלום, " . $nickname . "</span>";
                    }
                ?>
                <img src="<?php bloginfo('template_url'); ?>/assets/svg/003-user-profile.svg" alt="user-menu" loading="lazy">
            </a>
            
            <?php get_template_part('templates/nav/user-menu_dropdown'); ?>
        </div>
        <a class="side_icon updates_link"  alt="עדכונים מסומפי" href="<?php echo home_url(); ?>/somfy-updates" style="line-height: 0px;">
            <i class="fas fa-globe"></i>
        </a>
        <a class="side_icon cart__trigger" alt="עגלת קניות" href="<?php echo home_url(); ?>/cart">
            <img class='not-active' src="<?php bloginfo('template_url'); ?>/assets/svg/002-shopping-cart.svg" alt="cart" loading="lazy">        
        </a>
        <a class="side_icon" id="wishlist__trigger" href="<?php echo home_url(); ?>/wishlist" alt="wishlist">
            <img src="<?php bloginfo('template_url'); ?>/assets/svg/001-heart-shape-outline.svg" alt="wishlist" loading="lazy">
        </a>


    </div>

    
</div>

<a id="mobile_menu__trigger" class="is_mobile" alt="mobile menu">
    <span></span><span></span><span></span>
</a>
