<div id="categories_nav">
    <?php
        use Inc\Classes\Shop\CustomerPRO;
        $CustomerPRO = new CustomerPRO();


        $customerTaxonomy = $CustomerPRO->getCustomerTaxonomy();

        // echo "<pre>";
        // print_r($customerTaxonomy);
        // echo "</pre>";
        $userCategories = $customerTaxonomy['categories'];
        $userSubgroup = $customerTaxonomy['sub_group'];
        $userSub2group = $customerTaxonomy['sub2group'];
        
        $frontpage_id = get_option( 'page_on_front' );
        $sideNav2 = get_field('home_somfy_actions_side_menu', $frontpage_id);

        $shopUrl = home_url() .'/shop';

        while(have_posts()){
            the_post();
            $currentUrl = get_permalink();
        }

        if(!isset($currentUrl)) $currentUrl = '';

        wp_reset_postdata();
        // Get EU categories
        $categories = new WP_Query(array(
            'post_type' => 'product_category',
            'posts_per_page'   => -1,
            'meta_key' => 'sort_order',
            'orderby' => 'meta_value_num',
            'order' => 'ASC',
            'meta_query' => array(
                array(
                    'key' => 'show_on_site',
                    'compare' => '=',
                    'value' => TRUE
                )
            )
        )); 

        if($categories){
            $InactiveCategoriesHtmlArr = array();

            echo "<ul class='categories-links'>";
                    
            while ($categories->have_posts()) {
                $categories->the_post();

                $categoryTitle = get_the_title();
                $categoryID = get_the_ID();

                $catIcon = get_field('icon', $categoryID);
                $catIconActive = get_field('active_icon', $categoryID);
                
                // get groups
                $sub_groups = get_field('sub_groups');

                // mega menu banner
                $megaImg = get_field('mega_img');
                $megaLink = get_field('mega_link');

                $restrictionClass = '';
                $activeCategory = true;
                if(!in_array($categoryID, $userCategories)){
                    $restrictionClass = 'category_link__inactive';
                    $activeCategory = false;
                }

                $categoryHtml = '';
                $categoryHtml .= "<li class='category_link  $restrictionClass'>
                    <a class='menu_link' alt='$categoryTitle'>
                        <img src='$catIcon' alt='$categoryTitle'>
                        <img src='$catIconActive' class='active' alt='$categoryTitle'>
                        <span>$categoryTitle</span>
                    </a>";
                
                if($sub_groups){
                    $subgroupsHtml = '';

                    // Show MEGA MENU
                    // mega menu start
                    $subgroupsHtml .= "<div class='megamenu__container'>";


                    // Print Groups
                    $subgroupsHtml .= "<div class='megamenu__subgroups_links_container'>";
                    
                    foreach ($sub_groups as $sub_group){
                        $subgroupID = $sub_group->ID;
                        $subgroupTitle = get_the_title($subgroupID);
                        $sub_subgroups = get_field('2_subgroups', $subgroupID);

                        $subgroupRestrictionClass = '';
                        if(!in_array($subgroupID, $userSubgroup)){
                            $subgroupRestrictionClass = 'subgroup_link__container__inactive';
                            $subgroupPermalink = '';
                        }

                        $subgroupsHtml .= "<div class='subgroup_link__container $subgroupRestrictionClass' data-target='$categoryID-group_tab_$subgroupID'>
                            <a class='subgroup_link' href='$shopUrl?category=$categoryID&subgroup=$subgroupID' alt='$subgroupTitle'>$subgroupTitle";
                        if($sub_subgroups) $subgroupsHtml .= "<i class='fas fa-angle-down'></i>";
                        $subgroupsHtml .= "</a></div>";

                        if($sub_subgroups){
                            $subgroupsHtml .= "<div class='subgroups_links__tab' id='$categoryID-group_tab_$subgroupID'>";
                            foreach ($sub_subgroups as $sub_subgroup){
                                $sub_subgroupID = $sub_subgroup->ID;
                                $sub_subgroupTitle = get_the_title($sub_subgroupID);
                                $sub_subgroupPermalink = "$shopUrl?category=$categoryID&subgroup=$subgroupID&subsubgroup=$sub_subgroupID";
                                $sub_subgroupRestrictionClass = '';
                                if(!in_array($sub_subgroupID, $userSub2group)){
                                    $sub_subgroupRestrictionClass = 'sub_subgroup_link__container__inactive';
                                    $sub_subgroupPermalink = '';
                                }

                                $subgroupsHtml .= "<div class='sub_subgroup_link__container $sub_subgroupRestrictionClass'>
                                    <a class='sub_subgroup_link' href='$sub_subgroupPermalink' alt='$sub_subgroupTitle'>
                                        $sub_subgroupTitle
                                    </a>
                                </div>";
                            }
                            $subgroupsHtml .= "</div>";
                        }
                    }
                    $subgroupsHtml .= "</div>";

                    if($megaLink && $megaImg):
                        // Print subgroups
                        $subgroupsHtml .= "<div class='megamenu__banner_container'>
                            <a href='".$megaLink['url']."' alt='".$megaLink['title']."' target='".$megaLink['target']."'>
                                <img src='".$megaImg['url']."' alt='".$megaLink['title']."'>
                            </a>
                            </div>";
                    endif;

                    // mega menu end
                    $subgroupsHtml .= "</div>";
                    // Print subgroups
                    $categoryHtml .= "<div class='megamenu__subgroups_container'>";
                    $categoryHtml .= $subgroupsHtml;
                    $categoryHtml .= "</div>";
                }

                $categoryHtml .= "</li>";

                if($activeCategory){
                    echo $categoryHtml;
                } else {
                    array_push($InactiveCategoriesHtmlArr, $categoryHtml);
                }
            }

            foreach ($InactiveCategoriesHtmlArr as $cat){
                echo $cat;
            }

            echo "</ul>";

            
        }
        wp_reset_postdata();
    
    ?>


<!-- Somy Actions menu -->
<?php if (!is_front_page() && isset($sideNav2) && $sideNav2['side_nav_links_list'] != '' && !is_account( get_the_ID() )): ?>
        <div class="somfy_side_nav" id="somfy_actions__side_nav">
            <h3>סומפי לשירותך</h3>
            <ul class="side_nav__links_list">
                <?php 
                    foreach ($sideNav2['side_nav_links_list'] as $item) {
                        echo "<li class='side_nav__item'>
                            <a href='".$item['side_nav_link']['url']."' alt=' ".$item['side_nav_link']['title']."'>
                                <span class='side_nav__item_icon'><img src='".$item['side_nav_icon_active']."'></span>
                                <span class='side_nav__item_title'>
                                    ".$item['side_nav_link']['title']."
                                </span>
                            </a>
                        </li>";
                    }
                ?>
            </ul>
        </div>
    <?php endif; ?>
</div>