<?php
    $menu_name = 'user';
    $locations = get_nav_menu_locations();
    $menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
    $menuitems = wp_get_nav_menu_items( $menu->term_id, array( 'order' => 'DESC' ) );
?>

<div class="user-menu__dropdown">
        <?php
            if(is_user_logged_in()){
                echo "<ul class='menu-list'>";
                foreach ($menuitems as $item) {
                    if($item->title == 'Logout'){
                        echo "<hr class='divider'>";
                        echo "<li class='nav-item'><a href='$item->url' class='logout-link' alt='$item->title'>$item->title</a></li>";
                    } else {
                        echo "<li class='nav-item'><a href='$item->url' alt='$item->title'>$item->title</a></li>";
                    }
                }
                echo "</ul>";
            } else {
                echo "<div class='not_logged'>
                    <h4 class='register_title'>להתחבר לחשבון</h4>";

                    
                echo do_shortcode( '[ultimatemember form_id="284"]' );


                echo "<hr class='divider'>
                    <h4 class='register_title'>עדיין לא רשום?</h4>
                    <p class='register_text'>
                        ביצירת חשבון בחנות זו, תוכל לבצע תשלום בצורה
                        מהירה יותר, להזין מספר כתובות למשלוח, לעקוב
                        אחר הזמנותיך וכו'
                    </p>
                    <a class='um-button register_btn' alt='ליצור חשבון' href='" .get_home_url() ."/register'>ליצור חשבון</a>
                </div>";
            }
        wp_reset_postdata();
            
        ?>
</div>