<div id="product_preview_popup" class="popup__overlay">
    
    <div class="popup__container">
        
        <div class="container_header">
            <a class="popup__overlay__close product_preview_popup__close" alt='סגור'>
                <i class="fas fa-times"></i>
            </a>
        </div>
        <div class="loader" id="product_preview_popup__loader"><div class="spinner-loader"></div></div>

        <div id="product_preview_popup__content"></div>
    </div>

</div>