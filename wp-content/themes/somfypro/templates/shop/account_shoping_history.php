<div class="account_orders_history__container">

<?php
    $pageID = get_the_ID();

    $active_orders_desc = get_field('active_orders_desc', $pageID);
    $closed_orders_desc = get_field('closed_orders_desc', $pageID);
    
    $userID = get_current_user_id();
    $userInternalid = get_user_meta($userID, 'internalid')[0];

    echo "<input type='hidden' id='user_internalid' value='$userInternalid'>";
?>
    <div class="loader" id="account_orders__loader"><div class="spinner-loader"></div>טוען הזמנות...</div>
    <div id="no_orders__message" class="hidden">תרם ביצעת הזמנה</div>
    <div id="error_getting_data" class="hidden">לא ניתן לסנכרן נתונים. <br>צור קשר עם סומפי</div>
    
    <!-- User Orders -->
    <div class="orders_resultes__container">
        <h2 class="main-title">הזמנות שלי</h2>
        <hr class="divider">

        <div id="active_orders__container" class="orders_container">
            <div class="results_header">
                <h3 class="title">הזמנות פעילות</h3>
                
                <div class="sorting_filters_container">
                    <p class="title">סדר לפי: </p>
                    <a class="sf_btn sf_btn__small sort_orders_btn"  alt='מחיר'
                        data-order='asc' data-for="active_orders_results" data-sorttype='price'>מחיר</a>
                    <a class="sf_btn sf_btn__small sort_orders_btn"  alt='תאריך'
                        data-order='asc' data-for="active_orders_results" data-sorttype='date'>תאריך</a>
                </div>
            </div>
            <?php
                if($active_orders_desc != ''){
                    echo "<p class='results_desc'>$active_orders_desc</p>";
                }
            ?>
            <div class="results" id="active_orders_results">
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">מס הזמנה</th>
                            <th scope="col">תאריך</th>
                            <th scope="col">מחיר</th>
                            <th scope="col" class='order_status'>סטטוס</th>
                            <th scope="col" class='order_track'>מספר מעקב</th>
                            <th scope="col" class='order_actions'>פעולות</th>
                            <th scope="col">תשלום</th>
                        </tr>
                    </thead>
                    <tbody data-for="active_orders_results">
                        
                    </tbody>
                </table>
            </div>
        </div>

        
        <div id="orders_history__container" class="orders_container">
            <div class="results_header">
                <h3 class="title">הזמנות סגורות</h3>
                
                <div class="sorting_filters_container">
                    <p class="title">סדר לפי: </p>
                    <a class="sf_btn sf_btn__small sort_orders_btn"  alt='מחיר'
                        data-order='asc' data-for="orders_history_results" data-sorttype='price'>מחיר</a>
                    <a class="sf_btn sf_btn__small sort_orders_btn"  alt='תאריך'
                        data-order='asc' data-for='orders_history_results' data-sorttype='date'>תאריך</a>
                </div>
            </div>
            <?php
                if($closed_orders_desc != ''){
                    echo "<p class='results_desc'>$closed_orders_desc</p>";
                }
            ?>
            <div class="results" id="orders_history_results">
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">מס הזמנה</th>
                            <th scope="col">תאריך</th>
                            <th scope="col">מחיר</th>
                            <th scope="col" class="order_status">סטטוס</th>
                            <th scope="col">מספר מעקב</th>
                            <th scope="col">פעולות</th>
                            <th scope="col">תשלום</th>
                        </tr>
                    </thead>
                    <tbody data-for="orders_history_results">
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>		

<?php get_template_part('templates/shop/order_details_popup'); ?>
