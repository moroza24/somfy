<?php
    while(have_posts()){

        the_post();
        $pageId = get_the_ID();
        $currentPagePermalink = get_the_permalink($pageId);
        $pageTitle = get_the_title($pageId);
        $payment_method_message = get_field('payment_method_message', $pageId);
        
        $pickup_map_img = get_field('pickup_map_img');
        
        if(!$pickup_map_img){
            $pickup_map_img_url = bloginfo('template_url') + "\assets\img\pages\shop_pickup.png";
        } else {
            $pickup_map_img_url = $pickup_map_img['url'];

        }
    } 

    // check if current user is logged and get his data
    global $current_user;
    wp_get_current_user();
    
    $userID = get_current_user_id();

    // set default billcountry
    $default_billcountry = get_field_object('field_5fc295450ae7f')['default_value'];
    $default_shipcountry = get_field_object('field_5fc296b60ae86')['default_value'];

    $userData = array(
        'user_email' => '',
        'user_first_name' => '',
        'user_last_name' => '',
        'user_billaddressee' => '',
        'user_billaddress1' => '',
        'user_billcity' => '',
        'user_billcountry' => $default_billcountry,
        'user_shipcountry' => $default_shipcountry
    );
    
    
    if($userID){
        $userData = array(
            'user_billaddressee' => isset(get_user_meta($userID, 'billaddressee')[0]) ? get_user_meta($userID, 'billaddressee')[0] : '',
            'user_billaddress1' => isset(get_user_meta($userID, 'billaddress1')[0]) ? get_user_meta($userID, 'billaddress1')[0] : '',
            'user_billcity' => isset(get_user_meta($userID, 'billcity')[0]) ? get_user_meta($userID, 'billcity')[0] : '',
            'user_billcountry' => isset(get_user_meta($userID, 'billcountry')[0]) ? get_user_meta($userID, 'billcountry')[0] : '',
            'user_shipcountry' => isset(get_user_meta($userID, 'shipcountry')[0]) ? get_user_meta($userID, 'shipcountry')[0] : '',
            'user_shipaddressee' => isset(get_user_meta($userID, 'shipaddressee')[0]) ? get_user_meta($userID, 'shipaddressee')[0] : '',
            'user_shipaddress1' => isset(get_user_meta($userID, 'shipaddress1')[0]) ? get_user_meta($userID, 'shipaddress1')[0] : '',
            'user_shipcity' => isset(get_user_meta($userID, 'shipcity')[0]) ? get_user_meta($userID, 'shipcity')[0] : '',
            'user_shipzip' => isset(get_user_meta($userID, 'shipzip')[0]) ? get_user_meta($userID, 'shipzip')[0] : '',
            'terms' => get_field('terms', "user_$userID" ,true)
        );
    } else {
        $userID = '';
    }
    $site_whatsapp_chat_number = get_option('site_whatsapp_chat_number');
?>


<div class="container checkout_page">
    <ol class="breadcrumbs dark">
        <li><a href="<?php echo home_url(); ?>" alt='דף בית'>דף בית ></a></li>
        <li><a href="<?php echo $currentPagePermalink; ?>" alt='<?php echo $pageTitle; ?>'><?php echo $pageTitle; ?></a></li>
    </ol>
    <h2 class="section_title section_title_full dark_title"><?php echo the_title(); ?></h2>
    
    <!-- order_submition_loader -->
    <div class="order_submition_loader_container__overlay overly_bg">
        <div class="order_submition_loader__container">
            <h4>אנא המתן...</h4>
            <div class="loader" id="order_submition_loader"><div class="spinner-loader"></div></div>
            <p>המערכת מכינה את הזמנתך, אנא המתן.</p>
            <p id="order_submition__success">ההזמנה נשלחה!</p>
            <p id="order_submition__error"></p>
            <div class="order_submition_loader__actions">
                <a href="<?php echo home_url();  ?>/contact" id="go_to_contact" alt='צור קשר' class="sf_btn sf_btn__small">צור קשר</a>
                <a id="order_submition_loader__container__close" alt='אישור' class="sf_btn sf_btn__small">אישור</a>
            </div>
        </div>
    </div>
    

    <div class="checkout_page_container">
        <!-- Checkout Form  -->
        <div class="checkout_form__container">
            <!-- Triggers -->
            <div class="checkout_tabs__triggers">
                <a class="tab_trig active" alt='משלוח' data-target="checkout_delivery_tab">משלוח</a>
                <a class="tab_trig" alt='תשלום' data-target="checkout_payment_tab">תשלום</a>
            </div>

            <!-- Tabs content -->
            <div class="checkout_tabs__content">
                <!-- Billing information -->
                
                <h3 class="form_title">פרטי חיוב</h3>
                <p class="delivery_details_content">
                    <?php 
                        echo $userData['user_billaddressee'] . "</br>" 
                            . $userData['user_billaddress1'] . "</br>" 
                            . $userData['user_billcity'] . ", " . $userData['user_billcountry'];
                    ?>
                </p>

                <!-- checkout_delivery_tab -->
                <div class="tab_content active" id="checkout_delivery_tab">
                    <!-- Delivery options trigger -->
                    <h3 class="delivery_options__title">בחר סוג משלוח</h3>
                    <div class="delivery_options__container">
                        <div class="delivery_option active">
                            <input type="radio" name="delivery_option" id="delivery_option__delivery" value="delivery" checked>
                            <label for="delivery_option__delivery">משלוח</label>
                        </div>
                        <div class="delivery_option">
                            <input type="radio" name="delivery_option" id="delivery_option__pickup" value="pickup">
                            <label for="delivery_option__pickup">איסוף עצמי</label>
                        </div>
                    </div>

                    <!-- delivery_subtab__pickup -->
                    <div id="delivery_subtab__pickup" class="delivery_option__subtab">
                        <img src="<?php echo $pickup_map_img_url;?>" alt="map" loading="lazy">
                        
                    </div>

                    <!-- delivery_subtab__delivery -->
                    <div id="delivery_subtab__delivery" class="delivery_option__subtab">
                        <!-- toggle__other_delivery_details_form -->
                        <div class="toggle__other_delivery_details_form">
                            <input type="checkbox" name="other_delivery_address" id="other_delivery_address__toggle">
                            <label for="other_delivery_address__toggle">כתובת שונה למשלוח</label>
                        </div>

                        <!-- other_delivery_details_form -->
                        <div class="request_address_change" style="display: none;">
                            <p>
                                במידה והנך מעוניין לעדכן כתובת למשלוח אחרת<br>
                                אלייך להגיש בקשה ונציגינו יצרו איתך קשר בהקדם
                                <a alt='הגש פניה' class="sf_btn sf_btn-inline update_details_cta">הגש פניה</a>

                            </p>
                            <hr>

                            <p>
                                לחילופין מוזמן ליצור עימנו קשר דרך הוואטסאפ
                                
                                <a class='whatsappbtn' href="https://wa.me/<?php echo $site_whatsapp_chat_number; ?>"  target="_blank" alt='whatsapp'>
                                    <img src="<?php bloginfo('template_url'); ?>\assets\img\whatsapp.png" loading="lazy" alt="whatsapp">
                                </a>
                            </p>
                        </div>
                        
                    </div>

                    <div class="checkout_form_cta">
                        <div class="form_user_check">
                            <input type="checkbox" name="user_updates_agreement" id="user_updates_agreement">
                            <label for="user_updates_agreement">אני מאשר/ת קבלת מידע שיווקי, עדכונים והטבות</label>
                        </div>
                        <div class="form_user_check">
                            <input type="checkbox" name="user_usage_agreement" id="user_usage_agreement">
                            <label for="user_usage_agreement">אני מסכים ומאשר את התנאים ומדיניות הפרטיות</label>
                            <p class="form_error" data-input="user_usage_agreement">יש להסמן "אני מסכים"</p>
                        </div>

                        <a class="sf_btn" alt='המשך לתשלום' id="go_to_payment">המשך לתשלום</a>
                    </div>
                    
                </div>

                <!-- checkout_payment_tab -->
                <div class="tab_content" id="checkout_payment_tab">
                    <!-- payment options trigger -->
                    <div class="payment_options__container">
                        <div class="payment_option active">
                            <input type="radio" name="payment_option" id="payment_option__credit" value="credit" checked>
                            <label for="payment_option__credit">
                                <img src="<?php bloginfo('template_url'); ?>\assets\img\icons\credit.png" loading="lazy" alt="paypal">
                            </label>
                        </div>

                        <?php 
                            if($userData['terms']['label'] != '' && $userData['terms']['value'] != '-1'): 
                                echo '
                                    <div class="payment_option">
                                        <input type="radio" name="payment_option" id="payment_option__'.$userData['terms']['value'].'" value="'.$userData['terms']['value'].'">
                                        <label for="payment_option__'.$userData['terms']['value'].'">
                                            '.$userData['terms']['label'].'
                                        </label>
                                    </div>';
                            endif; 
                        ?>
                        
                    </div>
                    <?php if($payment_method_message != '') echo "<p class='payment_method_message'>$payment_method_message</p>" ?>
                    <hr class="divider">

                    <div class="payment_cta">
                        <a class="sf_btn" alt='תשלום' id="submit_order">תשלום</a>
                        <a class="sf_btn sf_btn-light" alt='חזרה' id="go_to_delivery_tab">חזרה</a>
                    </div>
                </div>
            </div>
        </div>

        <!-- Cart Preview -->
        <div class="cart_preview__container">
            <div class="preview-header">
                <h5 class="header-title">סיכום</h5>
            </div>

            <div class="cart_items__container">

            </div>


            <div class="cart_summery">
                <hr class="divider">

                <div class="discount_summery">
                    הנחה
                    <div class="val">
                        <span class="discount_summery_val"></span>
                        <span class='currency_symbol'>₪</span>
                    </div>
                </div>


                <hr class="divider">
                <div class="total_summery">
                    סה"כ לתשלום
                    <div class="val">
                        <span class="total_summery_val"></span>
                        <span class='currency_symbol'>₪</span>
                    </div>
                </div>
                <div class="catalogy_summery">
                    סה"כ לפי מחיר קטלוגי
                    <div class="val">
                        <span class="catalogy_summery_val"></span>
                        <span class='currency_symbol'>₪</span>
                    </div>
                </div>
            </div>

            <div class="not_cart_items">
                <p>עוד לא הוספת מוצרים לסל הקניות</p>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id='current_user_id' value='<?php echo $userID;?>'>

<!-- Shipping data -->
<?php 
    $shop_free_shipping_threshold = get_option('shop_free_shipping_threshold');
    $shop_shipping_product_id = get_option('shop_shipping_product_id');
    $shipping_price = get_field('base_price', $shop_shipping_product_id);
?>
<input type="hidden" id='free_shipping_threshold' value='<?php echo $shop_free_shipping_threshold?>'>
<input type="hidden" id='shipping_product_id' value='<?php echo $shop_shipping_product_id;?>'>
<input type="hidden" id='shipping_price' value='<?php echo $shipping_price;?>'>



<?php get_template_part('templates/account/update_account_request_popup'); ?>
