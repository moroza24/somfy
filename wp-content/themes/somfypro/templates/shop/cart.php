<?php
    $pageID = get_the_ID();
    $currentPagePermalink = get_the_permalink($pageID);
    $pageTitle = get_the_title($pageID);
    $thumbnail = get_the_post_thumbnail_url($pageID);
?>
<div class="main_content__with_header cart_page">
    <div class="page_header"
        style="background: linear-gradient(-90deg, rgba(29, 26, 52, .7), rgba(29, 26, 52, .7)), url('<?php echo $thumbnail; ?>');
            background-size: cover;
            background-position: center center;
            background-repeat: no-repeat;">
        <ol class="breadcrumbs">
            <li><a href="<?php echo home_url(); ?>" alt='דף בית'>דף בית ></a></li>
            <li><a href="<?php echo $currentPagePermalink; ?>" alt='<?php echo $pageTitle; ?>'><?php echo $pageTitle; ?></a></li>
        </ol>
        <h2 class="page_title"><?php echo $pageTitle; ?></h2>
    </div>

    <div class="cart_page_container">
        <div class="cart_result__container">
            <div class="loader" id="cart_result__loader"><div class="spinner-loader"></div></div>

            <div class="cart_items__container"></div>
            <table class="table" id="cart_items__table">
                <thead>
                    <tr>
                        <th scope="col">תמונה</th>
                        <th scope="col">מק"ט ושם מוצר</th>
                        <th scope="col" class='th_hhide_mobile'></th>
                        <th scope="col" class='th_hhide_mobile'>מחיר ליחידה</th>
                        <th scope="col" class='th_hhide_mobile'>סה"כ מחיר</th>
                        <th scope="col" class='th_hhide_mobile'>אחוז הנחה</th>
                        <th scope="col" class='th_hhide_mobile'></th>
                    </tr>
                </thead>
                <tbody id="cart_items__tbody">
                    
                </tbody>
            </table>
            <div class="not_cart_items">
                <p>עוד לא הוספת מוצרים לסל הקניות</p>
            </div>
        </div>
    </div>
    

    <div class="cart_summery" id="cart_page_summery">
        <div class="total_discount_summery"></div>
        <div class="summaries">

            <div class="total_summery">
                סה"כ לתשלום
                <div class="val">
                    <span class="total_summery_val"></span>
                    <span class='currency_symbol'>₪</span>
                </div>
            </div>
            <div class="catalogy_summery">
                סה"כ לפי מחיר קטלוגי
                <div class="val">
                    <span class="catalogy_summery_val"></span>
                    <span class='currency_symbol'>₪</span>
                </div>
            </div>
        </div>

        <a href="<?php echo home_url(); ?>/checkout" alt='המשך לתשלום' class="go_to_checkout sf_btn sf_btn__big">המשך לתשלום</a>
    </div>

    
</div>