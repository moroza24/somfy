<?php
    use Inc\Classes\Shop\CustomerPRO;
    $CustomerPRO = new CustomerPRO();

    $comingFromCalculator = false;

    if (isset($_GET['calculator'])) {
        if($_GET['calculator'] == true){
            $comingFromCalculator = true;
        };
    }

    // check if the user can access this taxonomy 
    $customerTaxonomy = $CustomerPRO->getCustomerTaxonomy();
    $userCategories = $customerTaxonomy['categories'];
    $userSubproduct = $customerTaxonomy['sub_group'];
    $userSub2product = $customerTaxonomy['sub2group'];

    if (isset($_GET['category'])) {
        $category = $_GET['category'];
        $categoryPermalink = get_home_url()."/shop/?category=" .$category;
        $categoryTitle = get_the_title($category);
        if(!in_array($category, $userCategories)){
            wp_redirect ('./');
        }
    } else {
        $category = '';
    }

    if (isset($_GET['subgroup'])) {
        $subgroup = $_GET['subgroup'];
        $subgroupPermalink = get_home_url()."/shop/?category=" .$category ."&subgroup=". $subgroup;
        $subgroupTitle = get_the_title($subgroup);
        if(!in_array($subgroup, $userSubproduct)){
            wp_redirect ('./');
        }
    } else {
        $subgroup = '';
    }

    if (isset($_GET['subsubgroup']) && !$comingFromCalculator) {
        $subsubgroup = $_GET['subsubgroup'];
        $subsubgroupPermalink = get_home_url()."/shop/?category=" .$category ."&subgroup=". $subgroup ."&subsubgroup=". $subsubgroup;
        $subsubgroupTitle = get_the_title($subsubgroup);
        if(!in_array($subsubgroup, $userSub2product)){
            wp_redirect ('./');
        }
    } else {
        $subsubgroup = '';
    }


    if($subgroup != ''){
        $pageID = $subgroup;
    } else if($category != '') {
        $pageID = $category;
    } else {
        $pageID = get_the_ID();
    }
    
    $pageTitle = get_the_title($pageID);
    $pageContent = get_the_content($pageID);
    $fieldsForFilters = get_field('filters', $pageID);
    $showCalculator = get_field('show_calculator', $pageID);
?>

<div class="container somfy_shop" id="somfy_shop">
    <?php 
        echo "<input id='shop' type='hidden' value='$pageID'>"; 
        echo "<input id='category' type='hidden' value='$category'>"; 
        echo "<input id='subgroup' type='hidden' value='$subgroup'>"; 
        echo "<input id='subsubgroup' type='hidden' value='$subsubgroup'>"; 
    ?>

    <ol class="breadcrumbs dark">
        <li><a href="<?php echo home_url(); ?>" alt=''>דף בית ></a></li>
        <?php if(isset($categoryPermalink) && isset($categoryTitle)): ?>
            <li>
                <a href="<?php echo $categoryPermalink; ?>" alt='<?php echo $categoryTitle; ?>'><?php echo $categoryTitle; ?> ></a>
            </li>
        <?php endif; ?>
        <?php if(isset($subgroupPermalink) && isset($subgroupTitle)): ?>
            <li>
                <a href="<?php echo $subgroupPermalink; ?>" alt='<?php echo $subgroupTitle; ?>'><?php echo $subgroupTitle; ?> ></a>
            </li>
        <?php endif; ?>
        <?php if(isset($subsubgroupPermalink) && isset($subsubgroupTitle)): ?>
            <li>
                <a href="<?php echo $subsubgroupPermalink; ?>" alt='<?php echo $subsubgroupTitle; ?>'><?php echo $subsubgroupTitle; ?></a>
            </li>
        <?php endif; ?>
    </ol>

    <h2 class="section_title section_title_full dark_title"><?php echo $pageTitle; ?></h2>
    <div class="section_content_text">
        <?php echo $pageContent; ?>
    </div>
    
    <!-- calculator -->
    <?php if($showCalculator) get_template_part('templates/features/calculator_banner'); ?>
    

    <!-- Advanced Filters -->
    <div id="advanced_filters_accordion">
        <div class="card">
            <div class="card-header" id="headingOne">
                <h5 class="mb-0">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        סינון מתקדם
                        <i class="fas fa-chevron-up"></i>
                    </button>
                </h5>
            </div>

            <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#advanced_filters_accordion">
                <div class="card-body advanced_filters_container" id="advanced_filters_container">
              
                    <!-- Price Range -->
                    <div class='filter__col' data-filter-name='price_range_filter' style="width: 50%;">
                        <h5>טווח מחירים</h5>
                        <a class='clear_filter' alt='price_range_filter' data-filter-type='price_range_filter' data-filter-name='price_range_filter'><i class='fas fa-eraser'></i></a>
                        <hr class='divider'>
                        <input type="hidden" id='price_range_filter_start' value="0">
                        <input type="hidden" id='price_range_filter_end' value="10000">

                        <div class="range_slider" id="price_range_filter">
                            <div>
                                <?php 
                                    $minPriceFilter = 10;
                                    $maxPriceFilter = 10000;
                                    $startPriceFilter = 0;
                                    $endPriceFilter = 10000;

                                    $startLeft = 100*($startPriceFilter / $maxPriceFilter);
                                    $endLeft = 100*($endPriceFilter / $maxPriceFilter);
                                    $inverseRight = 100 - $endLeft;
                                ?>
                                <div class='inverse-left'  style="width:<?php echo $startLeft; ?>%;"></div>
                                <div class='inverse-right' style="width:<?php echo $inverseRight; ?>%;"></div>
                                <div class='range' style="left:0%; right:<?php echo $inverseRight; ?>%;"></div>
                                <span class='thumb' style="left:<?php echo $startLeft; ?>%;"></span>
                                <span class='thumb' style="left:<?php echo $endLeft; ?>%;"></span>
                                <div class='sign' style="left:<?php echo $startLeft; ?>%;">
                                    <span id="value"><?php echo $startPriceFilter; ?></span>
                                </div>
                                <div class='sign' style="left:<?php echo $endLeft; ?>%;">
                                    <span id="value"><?php echo $endPriceFilter; ?></span>
                                </div>
                            </div>
                            <input type="range" tabindex="0" value="<?php echo $startPriceFilter; ?>" max="<?php echo $maxPriceFilter; ?>" min="<?php echo $minPriceFilter; ?>" step="10"/>
                            <input type="range" tabindex="0" value="<?php echo $endPriceFilter; ?>" max="<?php echo $maxPriceFilter; ?>" min="<?php echo $minPriceFilter; ?>" step="10" />
                        </div>
                    </div>

                    <a alt='סנן' class="sf_btn advance_filters_submit">סנן</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Sort Filters Bar -->
    <div class="sort_filters__container">

        <div class="flex__cont flex__al_c flex__wrap">
            <h4 class="title">סדר לפי:</h4>
            <div class="button_container">
                <button class="sort_filter_btn" data-sort-type='post_title' data-sort-order='asc'>שם</button>
                <button class="sort_filter_btn" data-sort-type='base_price' data-sort-order='desc'>מחיר יורד</button>
                <button class="sort_filter_btn" data-sort-type='base_price' data-sort-order='asc'>מחיר עולה</button>
                <button class="sort_filter_btn" data-sort-type='on_sell' data-sort-order='desc'>מחיר מבצע</button>
            </div>

        </div>
        <!-- Results -->
        <h5 class="results_counter" id="total_shop_results"></h5>
    </div>



    <div class="results_container" id="shop_results_container">

        <!-- print results here -->
        <div class="results" id="shop_results"></div>

        <!-- no_results_msg -->
        <div class='shop_no_results_msg' id="shop_no_results_msg">לא נמצאו תוצאות</div>
        

        <!-- Pagination cta -->
        <div class='load_more_cta' id="load_more_cta">
            <a alt="טען עוד"><i class="fas fa-plus"></i></a>
        </div>

        <!-- loader -->
        <div class="loader results_loader" id="shop_loader"><div class="spinner-loader"></div>טוען מוצרים...</div>

    </div>
</div>

<?php 
    wp_reset_postdata(); 
    get_template_part('templates/shop/popup_product_preview');
?>

