<div id="wishlist_popup" class="popup__overlay">
    
    <div class="popup__container">
        
        <div class="container_header">
            <a class="popup__overlay__close">
                <i class="fas fa-times"></i>
            </a>
            <h2><?php echo get_option('wishlist__title'); ?></h2>
            <p>
                <?php echo get_option('wishlist__content'); ?>
                
            </p>
        </div>
        <div class="container_body">
            <?php echo "<a alt='ליצור חשבון' class='sf_btn' href='" .get_home_url() ."/register'>ליצור חשבון</a>"; ?>
        </div>
    </div>

</div>