<div id="add_to_cart__popup" class="popup__overlay">
    
    <div class="popup__container">
        
        <div class="container_header">
            <a class="popup__overlay__close add_to_cart__popup__close" alt='סגור'>
                <i class="fas fa-times"></i>
            </a>
            <h2>סל קניות</h2>
            <p class='add_to_cart__success'>
                מוצר נוסף לסל הקניות
            </p>
            
            <div class="loader" id="add_to_cart__loader"><div class="spinner-loader"></div></div>
        </div>
    </div>

</div>