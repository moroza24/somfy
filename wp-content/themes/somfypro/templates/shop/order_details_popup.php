
<div id="order_details_popup" class="popup__overlay">
    
    <div class="popup__container">
        
        <div class="container_header">
            <a class="popup__overlay__close" id="order_details_popup__close" alt='סגור'>
                <i class="fas fa-times"></i>
            </a>
            <h2>הזמנה מספר: <span id="order_details__order_tranid"></span></h2>
        </div>
        
        <div class="container_body">
            <div class="loader" id="order_details__loader"><div class="spinner-loader"></div>טוען פרטים...</div>
            <div id="no_ordersitems__message" class="hidden">לא נמצאו שורות להזמנה</div>

            <!-- Order details results -->
            <div id="order_details__results">
                <div class="order_status">סטטוס: 
                    <span id="order_details__order_status"></span>
                </div>
                <br>
                <h5 id="order_details__order_inv__title">חשבוניות להדפסה</h5>
                <div class="order_inv">
                    <ul id="order_details__order_inv"></ul>
                </div>
                <p class="inv_download_error"></p>
                
                <div class="table_wraper">
                    <table class="table" id="order_details__orderitems">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">מק"ט</th>
                                <th scope="col">שם מוצר</th>
                                <th scope="col">כמות</th>
                                <th scope="col">כמות שסופקה</th>
                                <th scope="col">מחיר ביניים</th>
                                <th scope="col">מחיר</th>
                            </tr>
                        </thead>
                        <tbody data-for="order_details__orderitems">
                            
                        </tbody>
                    </table>
                </div>
                
                <div class="order_details__summery">
                    <div class="order_details__subtotal">מחיר ביניים 
                        <span id="order_details__subtotal"></span>
                    </div>
                    <!-- 
                    <div class="order_details__deliverytotal">משלוח
                        <span id="order_details__deliverytotal"></span>
                    </div> -->

                    <hr>
                    <div class="order_details__cat_total">סכום סופי קטלוגי
                        <span id="order_details__cat_total"></span>
                    </div>
                    <div class="order_details__total">סכום סופי
                        <span id="order_details__total"></span>
                    </div>
                    <a class='sf_btn sf_btn__small order_again_btn' id="order_details__order_again_btn" alt='>להזמין חדש' data-order=''>להזמין חדש</a>
                </div>
            </div>
        </div>
    </div>

</div>