<div id="cart_popup__container">
    <div class="popup-header">
        <h5 class="header-title">סל קניות</h5>
        <a class="cart_popup__close" alt='סגור'>
            <i class="fas fa-times"></i>
        </a>
    </div>

    <div class="cart_items__container">

    </div>


    <div class="cart_summery">
        <hr class="divider">

        <div class="sub_summery">
            מחיר ביניים
            <div class="val">
                <span class="sub_summery_val"></span>
                <span class='currency_symbol'>₪</span>
            </div>
            
        </div>

        <div class="discount_summery">
            הנחה
            <div class="val">
                <span class="discount_summery_val"></span>
                <span class='currency_symbol'>₪</span>
            </div>
        </div>

        <hr class="divider">

        <div class="total_summery">
            סכום סופי
            <div class="val">
                <span class="total_summery_val"></span>
                <span class='currency_symbol'>₪</span>
            </div>
        </div>

        <a href="<?php echo home_url(); ?>/checkout" alt='המשך לתשלום' class="go_to_checkout sf_btn">המשך לתשלום</a>
        <a href="<?php echo home_url(); ?>/cart" alt='לסל קניות' class="go_to_checkout sf_btn sf_btn-dark">לסל קניות</a>
    </div>

    <div class="not_cart_items">
        <p>עוד לא הוספת מוצרים לסל הקניות</p>
    </div>
</div>