
<div id="express_order__container">
    <h3 class="express__title">הזמנת אקספרס</h3>
    <hr>
    <div class="express_order__content">
    
        <!-- added items -->
        <ol id="express_order__added_items">
        </ol>

        <!-- dummy added item -->
        <li class="added_item_form_semple" style='display:none;'><form>
            <div class="row">
                <p id="product_itemid__text"></p>
                <p id="product_itemtitle__text"></p>
                <p id="product_amount__text"></p>
                <div class="col col-input with_autoload" style='display:none;'>
                    <input type="text" class="form-control form-control-lg product_autoloader" name='product_itemid' data-product='' data-autoload="true" placeholder='מק"ט' required>
                </div>
                <div class="col col-input col-input-number" style='display:none;'>
                    <input type="number" value='1' min='1' name='product_amount' class="form-control form-control-lg" placeholder="כמות" required>
                </div>
                <div class="col col-actions">
                    <button type="submit" class='express_added_item__remove_submit' alt="הסר שורה"><i class="fas fa-plus"></i></button>
                </div>
            </div>
        </form></li>

        <!-- add item form -->
        <div class="row">
            <h3 class="form_title">הזן מק"ט/שם המוצר</h3>
        </div>
        <form id="express_add_item__form">
            <div class="row">
                <div class="col col-input with_autoload">
                    <input type="text" class="form-control form-control-lg product_autoloader" name='product_itemid' id="express_add_item__form_item" data-product='' data-autoload="true" placeholder='מק"ט/שם מוצר' required>
                </div>
                <div class="col col-input">
                    <input type="number" value='1' min='1' id="express_add_item__form_amount" name='product_amount' class="form-control form-control-lg" placeholder="כמות" required>
                </div>
                <div class="col col-actions">
                    <button type="submit" id='express_add_item__form_submit' alt="הוסף שורה"><i class="fas fa-plus"></i></button>
                </div>
            </div>
        </form>

        <p id="express_add_item__form_error"></p>


        <button class='sf_btn' id='add_express_items_to_cart'>
            <img 
                src="<?php bloginfo('template_url'); ?>/assets/svg/002-shopping-cart-light.svg" 
                alt="cart" 
                loading="lazy">
            הוסף לסל
        </button><br>

        <a  alt='נקה הכל' id="clean_express_items">נקה הכל</a>
        <p id="add_express_order_cart__error">יש להזין לפחות מוצר אחד</p>
        <hr class="divider">


        <!-- Add from file -->
        <div class="express_from_file__container">
            <h3 class="title">הוסף הזמנה מקובץ</h3>

            <p class="instructions">
                לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג ולתיעם גדדיש.
                קוויז דומור ליאמום בלינך רוגצה. לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, סחטיר בלובק.
                קרא עוד
            </p>

            <div class="actions">
                <input type="file" id="fileUpload" />
                <button class='sf_btn' id='load_express_items_file'>
                    <img 
                        src="<?php bloginfo('template_url'); ?>/assets/svg/file.svg" 
                        alt="cart" 
                        loading="lazy">
                    בחר קובץ לעלות
                </button>

                <a href="<?php bloginfo('template_url'); ?>/assets/expressOrder_sample.csv" alt='קובץ לדוגמא' download>קובץ לדוגמא</a>
            </div>
            <p id="file_items_error"></p>
        </div>
    </div>
</div>


<div class="file_loader__overlay">
    <div class="loader_conatiner">
        <div class="loader"><div class="spinner-loader"></div></div>
        <p>טוען קובץ...</p>
    </div>
</div>