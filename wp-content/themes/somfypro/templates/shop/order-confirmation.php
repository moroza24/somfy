<?php
    while(have_posts()){

        the_post();
        $pageId = get_the_ID();

        $show_discount = get_field('show_discount');
        $discount_amount = get_field('discount_amount');

        $complete_button = get_field('complete_button');

        $side_container_background = get_field('side_container_background');
        $side_container_title = get_field('side_container_title');
        $side_container_link = get_field('side_container_link');
    } 
?>
    
<div class="container order-confirmation_page">
    <h2 class="section_title section_title_full dark_title"><?php echo the_title(); ?></h2>

    <!-- Success -->
    <div class="order-confirmation_page_container">
        <div class="confirmation__content">
            <h3 class="title">תודה על הזמנתך</h3>
            <p class="subtitle">ההזמנה נקלטה והועברה לטיפול, ניתן לעקוב אחר סטטוס ההזמנה באיזור האישי שלך</p>
            <a class='order_link' href="<?php echo home_url(); ?>/account/#orders__tab" alt='צפייה בהזמנות'>צפייה בהזמנות</a>
            <?php if($show_discount){ ?>
                <hr class="divider">
                <div class="discount_coupon">
                    <img src="<?php bloginfo('template_url'); ?>/assets/img/icons/coupon.svg"  alt="coupon" loading="lazy">
                    <p>קוד קופון לקנייה הבאה מחכה לך במייל</p>
                    <p><span class="discount_amount"><?php echo $discount_amount; ?></span> הנחה</p>
                </div>
            <?php } ?>

            <a class='sf_btn' href="<?php echo $complete_button['url']; ?>" alt="<?php echo $complete_button['title']; ?>"><?php echo $complete_button['title']; ?></a>
        </div>

    </div>
</div>