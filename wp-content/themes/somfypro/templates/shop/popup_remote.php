
<div id="remote_popup" class="popup__overlay">
    
    <div class="popup__container">
        
        <img src="<?php bloginfo('template_url'); ?>/assets/img/gift-box.svg" alt="gift" loading="lazy" class="popup__img">
        
        <h2 class="popup__title">מגיעה לך מתנה!</h2>
        <p class="popup__desc">
            לקוח יקר, כחלק מהטבת מודל הרדיו, מגיע לך <span id='remote_title'></span> <span id='remote_price'></span> בכל רכישה של מנוע רדיו.
            בלחיצה על אישור, השלטים יתווספו לסל הקניות
        </p>

        <hr>

        <div id="remote_card__container">
        </div>


        <div class="popup__actions">
            <button class="sf_btn" id='add_remote_cta'
                data-product=''
                data-parent=''
                data-amount=''>
                אישור
            </button>

            <button class="sf_btn sf_btn-light" id='remote_popup__close'>
                לא מעוניין
            </button>
        </div>
    </div>

</div>