<?php 
    // get page 
    $pageID = get_the_ID();
    $pageTitle = get_the_title($pageID);
    $pageContent = get_the_content($pageID);
    $pageContentwithbreaks = wpautop( $pageContent, true );
    $currentPagePermalink = get_the_permalink($pageID);

    $thumbnail = get_the_post_thumbnail_url($pageID);
    // get question and answer array
    $support_link_data = get_field('support_link_data', $pageID);

?>

<div class="main_content__with_header support_page" id="technical-support_page">
    <div class="page_header"
        style="background: linear-gradient(-90deg, rgba(29, 26, 52, .7), rgba(29, 26, 52, .7)), url('<?php echo $thumbnail; ?>');
            background-size: cover;
            background-position: center center;
            background-repeat: no-repeat;">
        <ol class="breadcrumbs">
            <li><a href="<?php echo home_url(); ?>" alt='דף בית'>דף בית ></a></li>
            <li><a href="<?php echo $currentPagePermalink; ?>" alt='<?php echo $pageTitle; ?>'><?php echo $pageTitle; ?></a></li>
        </ol>
        <h2 class="page_title"><?php echo $pageTitle; ?></h2>
    </div>


    <div class="support_page_content flex__cont flex__sp_btwn">
        <div class="content">
            <?php echo $pageContentwithbreaks; ?>
        </div>

        <div class="link_container">
        <?php 
            echo "<a class='link_card' href='".$support_link_data['support_link']['url']."' alt='".$support_link_data['support_link']['title']."'>
                    <img src='".$support_link_data['support_image']."' alt='".$support_link_data['support_link']['title']."'>
                    <span class='link_card__title'>".$support_link_data['support_link']['title']."</span>
                </a>";
        ?>
        </div>
        
    </div>
</div>