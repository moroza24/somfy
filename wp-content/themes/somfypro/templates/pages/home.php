<?php
    $userID = get_current_user_id();
    $nickname = '';
    if($userID){
        $nickname = isset(get_user_meta($userID, 'display_name')[0]) ? get_user_meta($userID, 'display_name')[0] : '';
    }

    $homeLinksSection = get_field('home_links_section');
?>


<div id="home-page" class="container">
    <h2 class="section_title dark_title"><?php echo "<span>ברוך הבא, " . $nickname . "</span>"; ?></h2>
    
    <div class="home_content__container">
        <div class="sidenavs_col">
            <?php get_template_part('templates/features/somfy_side_menus'); ?>
        </div>

        <?php get_template_part('templates/shop/express_order'); ?>
    </div>

    <?php if(isset($homeLinksSection)): ?>
        <div class="home_links__container">
            <h2 class="section_title dark_title"><?php echo $homeLinksSection['title']; ?></h2>

            <!-- Links -->
            <?php 
                $home_links = $homeLinksSection['home_links'];
                if(count($home_links)>0){
                    echo "<div class='links__container'>";
                        
                    foreach($home_links as $link){
                        echo "<a class='loginPage_link' href='".$link['home_link']['url']."'  alt='".$link['home_link']['title']."'>
                            <img src='".$link['home_link_image']['url']."' alt='".$link['home_link']['title']."'>
                            <span class='loginPage_link__title'>".$link['home_link']['title']."</span>
                        </a>";
                    }
                    echo "</div>";
                }; 
            ?>
        </div>
    <?php endif; ?>
</div>