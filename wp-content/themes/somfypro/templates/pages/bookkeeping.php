<?php 
    if ( ! defined( 'ABSPATH' ) ) exit;

    // get page 
    $pageID = get_the_ID();
    $pageTitle = get_the_title($pageID);
    $pageContent = get_the_content($pageID);
    $pageContentwithbreaks = wpautop( $pageContent, true );
    $currentPagePermalink = get_the_permalink($pageID);

    $thumbnail = get_the_post_thumbnail_url($pageID);
    // get question and answer array


    global $current_user;
    $userID = get_current_user_id();
    $terms = get_field('terms', "user_$userID" ,true);
    $userData = array(
        'email' =>  $current_user->user_email,
        'companyname' => isset(get_user_meta($userID, 'companyname')[0]) ? get_user_meta($userID, 'companyname')[0] : '',
        'first_name' => isset(get_user_meta($userID, 'first_name')[0]) ? get_user_meta($userID, 'first_name')[0] : '',
        'last_name' => isset(get_user_meta($userID, 'last_name')[0]) ? get_user_meta($userID, 'last_name')[0] : '',
        'phone' => isset(get_user_meta($userID, 'phone')[0]) ? get_user_meta($userID, 'phone')[0] : '',
        'custentity_il_mobile' => isset(get_user_meta($userID, 'custentity_il_mobile')[0]) ? get_user_meta($userID, 'custentity_il_mobile')[0] : '',
        'terms' => get_field('terms', "user_$userID" ,true)
    );


    if($userData['first_name'] != "" || $userData['last_name'] !=''){
        $userData['contact'] = $userData['first_name'] . " " .$userData['last_name'];
    } else {
        $userData['contact'] = $userData['companyname'];
    }
?>


<div class="main_content__with_header" id="bookkeeping_page">
    <div class="page_header"
        style="background: linear-gradient(-90deg, rgba(29, 26, 52, .7), rgba(29, 26, 52, .7)), url('<?php echo $thumbnail; ?>');
            background-size: cover;
            background-position: center center;
            background-repeat: no-repeat;">
        <ol class="breadcrumbs">
            <li><a href="<?php echo home_url(); ?>" alt='דף בית'>דף בית ></a></li>
            <li><a href="<?php echo $currentPagePermalink; ?>" alt='<?php echo $pageTitle; ?>'><?php echo $pageTitle; ?></a></li>
        </ol>
        <h2 class="page_title"><?php echo $pageTitle; ?></h2>
    </div>


    <div class="page_content__container">
        
        <!-- customer_balance__container -->
        <div class="loader" id="customer_balance__loader"><div class="spinner-loader"></div></div>
        <p id="customer_balance__error"></p>
        <p id="customer_balance__all_clean">אין חובות פתוחים, כל הכבוד!</p>
        
        <div class="customer_balance__container">
            <p class="balance_form_instructions">
                לקוח יקר, <br>
                להסדרת תשלום, אנא סמן את ההזמנה שברצונך לשלם עבורה או חלק ממנה ע"י הקלדה ידנית.
            </p>
            <hr class="divider">

            <div class="customer_balance__table__actions">
                <p class="total_to_pay"></p>
                
                <div class="sorting_filter_container">
                    <p class="title">סדר לפי: </p>
                    <a class="sf_btn sf_btn_inline sf_btn__small sort_orders_btn"  alt='תאריך'
                        data-order='asc'>תאריך</a>
                </div>
            </div>

            <div class="table_wraper">
                <table class="table" id="customer_balance__table">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">מס חשבונית</th>
                            <th scope="col">הזמנה</th>
                            <th scope="col">תאריך</th>
                            <th scope="col">סכום מקורי</th>
                            <th scope="col">יתרה</th>
                            <th scope="col">פרטי הזמנה</th>
                            <th scope="col">סכום לתשלום</th>
                        </tr>
                    </thead>
                    <tbody id="customer_balance_tbody">
                        
                    </tbody>
                </table>
            </div>
            

            <div class="load_more_updates" id="load_more_balance">
               <a alt="טען עוד"><i class="fas fa-plus"></i></a>
             </div>

            <div class="balance_due">
                    <h4 class="balance_due__title">יתרה לתשלום</h4>
                    <p id="balance_due__val"></p>

                    <p class="total_to_pay"></p>

            </div>

            <div class="customer_balance__actions">
                <a class='sf_btn sf_btn-inline' alt='השאר פניה למחלקת הנה"ח' id="submit_customer_balance_form">השאר פניה למחלקת הנה"ח</a>
                <p id="customer_balance_form_error"></p>
            </div>
        </div>


        <div class="content">
            <?php echo $pageContentwithbreaks; ?>
        </div>
    </div>
</div>



<div id="balance_customerDetailes_popup" class="popup__overlay">
    
    <div class="popup__container">
        
        <div class="container_header">
            <h2>צור קשר</h2>
            <a class="popup__overlay__close balance_customerDetailes_popup__close" alt='סגור'>
                <i class="fas fa-times"></i>
            </a>
        </div>
        <div class="container_body">
            <h4 class="content_title">אנא מלא את כל השדות<br>ונציגנו יצרו איתך קשר בהקדם</h4>

            <form id="balance_customerDetailes__form">
                <div class="balance_payment_request__preview">
                    <div id="payment_request__preview__table"></div>
                    <div id="payment_request__preview__total"></div>
                </div>


                <div class="mb-3 form-input__group">
                    <input type="text" 
                        class="form-control form-control-lg" 
                        id="balance_customerDetailes__contact" 
                        placeholder='איש קשר' required>
                    <p class="form_error" data-input="balance_customerDetailes__contact">יש להזין שם איש קשר</p>
                </div>

                <div class="mb-3 form-input__group">
                    <input type="email" 
                        class="form-control form-control-lg" 
                        id="balance_customerDetailes__email" 
                        placeholder='כתובת דוא"ל' required>
                    <p class="form_error" data-input="balance_customerDetailes__email">יש להזין כתובת דוא"ל</p>
                </div>

                <div class="mb-3 form-input__group">
                    <input type="tel" 
                        class="form-control form-control-lg" 
                        id="balance_customerDetailes__phone" 
                        placeholder="טלפון" minlength="8"  maxlength="10"required
                        onkeydown="if(event.key==='.'){event.preventDefault();}"  oninput="event.target.value = event.target.value.replace(/[^0-9]*/g,'');">
                    <p class="form_error" data-input="balance_customerDetailes__phone">יש להזין מספר טלפון</p>
                </div>

                

                <input class="sf_btn" type="submit" id="submit_balance_customerDetailes" value="שלח">
                <div class="loader" id="balance_customerDetailes__form_loader"><div class="spinner-loader"></div>המערכת שולחת את הטופס, אנא המתן</div>
                <p id="balance_customerDetailes__form_submit_success">הודעה נשלחה בהצלחה!</p>
                <p id="balance_customerDetailes__form_submit_error"></p>
            </form>
            <input type="hidden" id="defualt_user_contact" value="<?php echo $userData['contact'] ?>">
            <input type="hidden" id="defualt_user_email" value="<?php echo $userData['email'] ?>">
            <input type="hidden" id="defualt_phone" value="<?php echo $userData['custentity_il_mobile'] == '' ? $userData['phone'] : $userData['custentity_il_mobile'];?>">
        </div>
    </div>

</div>
<?php get_template_part('templates/shop/order_details_popup'); ?>
