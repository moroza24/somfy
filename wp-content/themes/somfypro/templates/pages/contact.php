<?php 
    // get page 
    $pageID = get_the_ID();
    $currentPagePermalink = get_the_permalink($pageID);
    $pageTitle = get_the_title($pageID);


    // get question and answer array
    $qas = get_field('questions_and_answers', $pageID);
    $map_src = get_field('map_src', $pageID);

    $subj = get_field('subject_of_contact',$pageID);
    $departments = get_field('departments',$pageID);

    $uid = get_current_user_id();
    $sales_rep = get_field('salesrep', 'user_'.$uid);
    $available_agent = '';
    if( !empty($sales_rep) ):
        $available_agent = get_users(
            array(
                'role' => 'um_sales-rep',
                'meta_query' => array(
                    array(
                        'key' => 'internalid',
                        'value' => $sales_rep,
                        'compare' => 'IN'
                    ),
                )
            )
        );
        $agent = get_userdata( $available_agent[0] -> ID );
        
        $agent_name = $agent->first_name . ' ' . $agent->last_name;
    endif; 
?>
<div id="contact-page" class="container">
    <ol class="breadcrumbs dark">
        <li><a href="<?php echo home_url(); ?>" alt='דף בית'>דף בית ></a></li>
        <li><a href="<?php echo $currentPagePermalink; ?>" alt='<?php echo $pageTitle; ?>'><?php echo $pageTitle; ?></a></li>
    </ol>
	<h2 class="section_title dark_title"><?php the_title(); ?></h2>
    
    <!-- Contact Form -->
    <div class="contact-form__container">
       
        <div class="contact-form__form">
            <form id="pro_contact_form">
                <div class="form_row">
                    <div class="form_input">
                        <select class="form-select form-select-sm" id="pro_contact_form__subsub" name="pro_contact_form__subsub" required>
                            <option value="">מהות הפנייה</option>
                            <?php 
                                if( $subj ):
                                    foreach ($subj as $k => $sub) { ?>
                                        <option value="<?php echo 'subj-'.$k; ?>"><?php echo $sub['title']; ?></option>
                            <?php }
                                endif; 
                            ?>
                        </select>
                        <p class="form_error" data-input="pro_contact_form__subsub">יש לבחור את מהות הפנייה</p>
                    </div>
                </div>

                
                <div class="form_row">
                    <div class="form_input">
                        <select class="form-select form-select-sm" id="pro_contact_form__department" name="pro_contact_form__department" required>
                            <option value=""> מחלקה </option>
                            <?php 
                            
                                if( $departments ):
                                    foreach ($departments as $d) { ?>
                                        <option value="<?php echo $d['email']; ?>"><?php echo $d['title']; ?></option>
                            <?php }
                                endif; 
                            ?>
                            <?php if( $agent_name ) {?>
                                <option value="<?php echo $agent->user_email; ?>">שיחה עם סוכן (<?php echo $agent_name; ?>)</option>
                            <?php } ?>
                        </select>
                        <p class="form_error" data-input="pro_contact_form__department">יש לבחור מחקלה</p>
                    </div>
                </div>


                <div class="form_row">
                    <div class="form_input">
                        <textarea id='pro_contact_form__description' name='pro_contact_form__description' placeholder="טקסט חופשי לפירוט נוסף" required></textarea>
                    </div>
                    <p class="form_error" data-input="pro_contact_form__description">יש להזין פירוט נוסף לפניה</p>
                </div>

                <div class="form_row">
                    <div class="form_actions">
                        <a class="sf_btn sf_btn__big " alt='tel:0738797254' href="tel:0738797254">073-8797254 <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/Layer-2.jpg" alt=""></a>
                    </div>
                </div>

                <div class="form_row submit_row">
                    <div class="form_actions">
                        <button id='pro_contact_form__formsubmit' class='sf_btn sf_btn__big formsubmit zcwf_button'>שלח פנייה</button>
                        <div class="loader" id="pro_contact_form__loader"><div class="spinner-loader"></div></div>
                        <p id="pro_contact_form__submit_success">הודעה נשלחה בהצלחה!</p>
                        <p id="pro_contact_form__submit_error"></p>
                    </div>
                </div>
            </form>
        </div>
        <div class="thumb" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>');">

        </div>

    </div>
    
    <?php 
        wp_reset_postdata();
    ?>

</div>