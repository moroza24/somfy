<div class="studies">
    <div class="thumb" style='background-image: url(<?php the_post_thumbnail_url(); ?>);'></div>
    <div class="content">

      <div class="small_line">
        <span><?php echo get_the_date( 'M\' j, Y' ); ?> </span>
        <span class="divider">|</span>
        <span> <?php the_field('studies_small_line'); ?> </span>
      </div>
      <div class="title">
        <?php the_title(); ?>
      </div>

      <div class="iconz">
        <span>
          <img class='clock' src="<?php echo get_stylesheet_directory_uri(); ?>/assets/svg/clock.svg" alt="">
          <span><?php echo get_field('seminar_date'); ?></span>
        </span>
        <span class='divider'>|</span>
        <span>
          <img class='map_coords' src="<?php echo get_stylesheet_directory_uri(); ?>/assets/svg/map_coords.svg" alt="">
          <span><?php echo get_field('seminar_place'); ?></span>
        </span>
        <br/>
        <span>
          <img class='mic' src="<?php echo get_stylesheet_directory_uri(); ?>/assets/svg/mic.svg" alt="">
          <span><?php echo get_field('seminar_teacher'); ?></span>
        </span>
      </div>

      <a href="#" 
        alt='להרשם להדרכה'
        data-pid='<?php echo get_the_ID(); ?>' 
        data-subject='<?php echo get_the_title(); ?>'
        data-date='<?php echo get_field('seminar_date'); ?>'
        class='toggle-seminar-form'>
        להרשם להדרכה
      </a>
    </div>
</div>
