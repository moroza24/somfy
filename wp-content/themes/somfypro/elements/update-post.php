<div class="update">
  <a href="<?php echo esc_url( get_permalink() ); ?>">
    <div class="thumb" style='background-image: url(<?php the_post_thumbnail_url(); ?>)'></div>
    <div class="update_content">
      <div class="small_line">
        <span><?php echo get_the_date( 'M\' j, Y' ); ?> </span>
        <span class="divider">|</span>
        <span> <?php the_field('studies_small_line'); ?> </span>
      </div>
      <div class="title">
        <?php the_title(); ?>
      </div>
      <div class="content">
        <?php the_excerpt(); ?>
      </div>
    </div>
    </a>
</div>
