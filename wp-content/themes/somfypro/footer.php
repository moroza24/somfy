    
    </div> <!-- close main_content div-->
    </div> <!-- close content_wrapper div-->

<?php
    if(is_user_logged_in()){
    
    $userID = get_current_user_id();
    $isExpert = false;
    if($userID){
        $hirarchy = get_field('custentity_hirarchy', "user_$userID", false);
        if($hirarchy == 1) $isExpert = true;
    }

    // expert banner
    if(!$isExpert) get_template_part('templates/features/expert_banner');

    $site_whatsapp_chat_number = get_option('site_whatsapp_chat_number');
?>


<?php
    // footer 1
    $locations = get_nav_menu_locations();

    $menu_name = 'footer1';
    $footer1_menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
    $footer1_menuitems = wp_get_nav_menu_items( $footer1_menu->term_id, array( 'order' => 'DESC' ) );
    
    $menu_name = 'footer2';
    $footer2_menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
    $footer2_menuitems = wp_get_nav_menu_items( $footer2_menu->term_id, array( 'order' => 'DESC' ) );
    
?>
    <footer class="footer" id="footer">
        <div class="footer_container">

            <div class='footer_section'>
                <h3 class='footer_section__title'>צור קשר</h3>
                <p class="footer_section__content">
                    להזמנות באתר בלבד: <a href="tel:073-3245760">073-3245760</a><br>
                    לא תתאפשר רכישה במשרדי החברה!<br>
                    דוא"ל: <a href="mailto:eshop.il@somfy.com" alt='eshop.il@somfy.com'>eshop.il@somfy.com</a> 
                    <br>
                    <a href="<?php echo get_home_url(); ?>/צור-קשר">טופס צור קשר</a>
                </p>
            </div>

            <?php 
                if($footer1_menu){
                    echo "<div class='footer_section'>
                        <h3 class='footer_section__title'>$footer1_menu->name</h3>
                        <ul class='footer_menu'>
                    ";

                    foreach ($footer1_menuitems as $item) {
                        echo "<li class='footer_menu-item'><a href='$item->url' alt='$item->title'><span>></span>$item->title</a></li>";
                    }
                    echo "</ul></div>";
                }
            ?>

            <?php 
                if($footer2_menu){
                    echo "<div class='footer_section'>
                        <h3 class='footer_section__title'>$footer2_menu->name</h3>
                        <ul class='footer_menu'>
                    ";
                    foreach ($footer2_menuitems as $item) {
                        echo "<li class='footer_menu-item'><a href='$item->url' alt='$item->title'><span>></span>$item->title</a></li>";
                    }
                    echo "</ul></div>";
                }
            ?>
            
            <div class='footer_section'>
                <h3 class='footer_section__title'>אפשרויות תשלום</h3>
                <div class="paymeny_types">
                    <img src="<?php bloginfo('template_url'); ?>/assets/img/icons/visa.svg" alt="visa">
                    <img src="<?php bloginfo('template_url'); ?>/assets/img/icons/mastercard.svg" alt="mastercard">
                </div>
            </div>

            <div class='footer_section'>
                <h3 class='footer_section__title'>רשתות חברתיות</h3>
                <div class="social_links">
                    <a href="https://www.facebook.com/SomfyIsrael" class="social_link" alt="facebook" target="_blank">
                        <img src="<?php bloginfo('template_url'); ?>/assets/img/icons/facebook-circular-logo.svg" alt="facebook">
                    </a>
                    <a href="https://www.youtube.com/user/somfyisrael" class="social_link" alt="youtube" target="_blank">
                        <img src="<?php bloginfo('template_url'); ?>/assets/img/icons/youtube.svg" alt="youtube">
                    </a>
                    <a href="https://www.instagram.com/somfyfrance/" class="social_link" alt="instagram" target="_blank">
                        <img src="<?php bloginfo('template_url'); ?>/assets/img/icons/instagram.svg" alt="instagram">
                    </a>
                </div>
            </div>
        </div>
    </footer>
    
    <?php get_template_part('templates/shop/popup_stock_notifictaion'); ?>
    <?php get_template_part('templates/shop/popup_stock_notifictaion_maxamount'); ?>
    <?php get_template_part('templates/shop/popup_wishlist'); ?>
    <?php get_template_part('templates/shop/popup_add_to_cart'); ?>
    <?php get_template_part('templates/shop/popup_add_warranty'); ?>
    <?php get_template_part('templates/features/calculator_popup'); ?>
    <?php get_template_part('templates/shop/popup_remote'); ?>

    <?php if($site_whatsapp_chat_number != ''): ?>
        <a href="https://wa.me/<?php echo $site_whatsapp_chat_number; ?>" id='site_whatsapp_chat' target="_blank" alt='whatsapp chat'>
            <img src="<?php bloginfo('template_url'); ?>/assets/img/Chat.png" alt="whatsapp chat">
            <img class='mobile' src="<?php bloginfo('template_url'); ?>/assets/img/whatsapp.png" alt="whatsapp chat">
        </a>
    <?php endif; ?>

    <?php
        };
    ?>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
    
    <?php wp_footer(); ?>
    <!-- JavaScript Bundle with Popper -->

    </body>
</html>