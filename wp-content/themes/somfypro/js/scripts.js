// 3rd party packages from NPM
import $ from 'jquery';
import slick from 'slick-carousel';
import Cookies from 'js-cookie'

// Our modules / classes

// General js
import GeneralScripts from './modules/features/GeneralScripts';
import Inputs from './modules/features/Inputs';
import TopNav from './modules/TopNav';
import GlobalSearch from './modules/features/GlobalSearch';
import ExpressOrder from './modules/features/ExpressOrder';
import RMA from './modules/features/RMA';
import Calculator from './modules/features/Calculator';

// Pages
import Home from './modules/pages/home';
import SearchPage from './modules/pages/SearchPage';
import Account from './modules/pages/Account';
import Bookkeeping from './modules/pages/Bookkeeping';
import LoginPage from './modules/pages/LoginPage';
import News from './modules/pages/News';
import ContactPage from './modules/pages/ContactPage';


// Shop
import Cart from './modules/shop/Cart';
import ShopPage from './modules/shop/ShopPage';
import ProductPage from './modules/shop/ProductPage';
import CheckoutPage from './modules/shop/CheckoutPage';
import StockNotifictaion from './modules/shop/StockNotifictaion';
import Wishlist from './modules/shop/Wishlist';
import Category from './modules/shop/Category';
import OrderDetailsPopup from './modules/shop/OrderDetailsPopup';
import OrderAgain from './modules/shop/OrderAgain';

import OrderSync from './modules/features/OrderSync';

$.fn.extend({
    toggleText: function(a, b){
        return this.text(this.text() == b ? a : b);
    }
});


// Instantiate a new object using our modules/classes
$(document).ready(() => {
    var pathname = window.location.pathname;
    var generalScripts = new GeneralScripts();
    var inputs = new Inputs();
    var topNav = new TopNav();
    var globalSearch = new GlobalSearch();

    // Shop
    var cart = new Cart();
    var stockNotifictaion = new StockNotifictaion();
    var wishlist = new Wishlist(stockNotifictaion, cart);
    var category = new Category();
    var orderAgain = new OrderAgain(cart);
    var orderDetailsPopup = new OrderDetailsPopup(orderAgain);
    var calculator = new Calculator();

    var orderSync = new OrderSync();

    // Home page scripts
    if(pathname == '/' || pathname == '/somfy/pro/' || pathname == '/staging/pro/'){
        var expressOrder = new ExpressOrder(cart);
    }

    // Search page scripts
    if(pathname.includes('/search/')){
        var searchPage = new SearchPage(stockNotifictaion, wishlist, cart);
    }

    // Search page scripts
    if(pathname.includes('/wishlist/')){
        wishlist.printUsersWishlistPage();
    }

    // Account page scripts
    if(pathname.includes('/account/')){
        var account = new Account(orderDetailsPopup, orderAgain);
    }

    if(pathname.includes('/bookkeeping/')){
        var bookkeeping = new Bookkeeping(orderDetailsPopup);
    }

    if(pathname.includes('/product_subgroup/') || pathname.includes('/product_2_subgroup/') || pathname.includes('/shop/')){
        var shopPage = new ShopPage(stockNotifictaion, wishlist, cart, inputs);
    }

    if(pathname.includes('/product/')){
        var productPage = new ProductPage();
    }

    if(pathname.includes('/somfy-updates/')){
        var news = new News();
    }
  
    if(pathname.includes('/checkout/')){
        var checkoutPage = new CheckoutPage(cart);
    }

    
    if(pathname.includes('/customer-service/')){
        var rma = new RMA();
    }

    if($('#pro_contact_form').length > 0){
        var contactPage = new ContactPage();
    }


    // general small scripts
    
    
    // Set redirect after registration - to Shop || to Checkout
    if(pathname.includes('/login/')){
        var loginPage = new LoginPage();
        let checkoutURL = somfyData.root_url + "/checkout";
        $('#login-page').find('form').append(`
            <input type="hidden" name="redirect_to" value="${somfyData.root_url}">
        `)
    }
});
