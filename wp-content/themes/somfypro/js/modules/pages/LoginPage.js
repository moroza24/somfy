import $ from 'jquery';

class LoginPage {

    constructor() { 
        // register popup
        this.register_btn = $('.register_btn');
        this.registration_popup__close = $('.registration_popup__close');
        this.registration_popup = $('#registration_popup');
        this.submit_rgistration_request_btn = $('#submit_rgistration_request');
        this.rgistration_request__form_loader = $('#rgistration_request__form_loader');
        this.form_submit_error = $('#form_submit_error');
        this.form_submit_success = $('#form_submit_success');

        // registration form
        this.rgistration_request__form = $('#rgistration_request__form');
        this.initEvents();
    }

    initEvents() {
        this.register_btn.on('click',()=>{
            this.registration_popup.fadeIn();
        });

        this.registration_popup__close.on('click',()=>{
            this.closeRegPopup();
        });

        this.rgistration_request__form.on('submit', (e)=>{
            e.preventDefault();
            this.rgistration_request__form_loader.show();
            this.submit_rgistration_request_btn.hide();
            this.form_submit_error.hide();
            this.form_submit_success.hide();

            // validate form
            this.sendNewRequest(e);
        })
    }


    sendNewRequest(e){
        let companyname = $('#rgistration_request__companyname').val();
        let city = $('#rgistration_request__city').val();
        let companyid = $('#rgistration_request__companyid').val();
        let contact_name = $('#rgistration_request__contact_name').val();
        let customerEmail = $('#rgistration_request__email').val();
        let activitytype = $('#rgistration_request__activitytype').val();
        let phone = $('#rgistration_request__phone').val();

        let error = false;
        if(companyname.length < 3 || companyname == ''){
            $('p.form_error[data-input="rgistration_request__companyname"]').show();
            error = true;
        } else {
            $('p.form_error[data-input="rgistration_request__companyname"]').hide();
        }

        if(city.length < 3 || city == ''){
            $('p.form_error[data-input="rgistration_request__city"]').show();
            error = true;
        }else {
            $('p.form_error[data-input="rgistration_request__city"]').hide();
        }

        if(companyid.length < 6 || companyid == ''){
            $('p.form_error[data-input="rgistration_request__companyid"]').show();
            error = true;
        }else {
            $('p.form_error[data-input="rgistration_request__companyid"]').hide();
        }

        if(contact_name.length < 3 || contact_name == ''){
            $('p.form_error[data-input="rgistration_request__contact_name"]').show();
            error = true;
        }else {
            $('p.form_error[data-input="rgistration_request__contact_name"]').hide();
        }

        if(customerEmail.length < 3 || customerEmail == ''){
            $('p.form_error[data-input="rgistration_request__email"]').show();
            error = true;
        }else {
            $('p.form_error[data-input="rgistration_request__email"]').hide();
        }

        if(activitytype.length < 3 || activitytype == '' || activitytype == '-1'){
            $('p.form_error[data-input="rgistration_request__activitytype"]').show();
            error = true;
        }else {
            $('p.form_error[data-input="rgistration_request__activitytype"]').hide();
        }

        var regex = /^0(5[^7]|[2-4]|[8-9]|7[0-9])[0-9]{7}$/;
        if(phone.length < 8 || phone == '' || !regex.test(phone.replace("-", ""))){
            $('p.form_error[data-input="rgistration_request__phone"]').show();
            error = true;
        } else {
            $('p.form_error[data-input="rgistration_request__phone"]').hide();
        }

        if(error){
            this.rgistration_request__form_loader.hide();
            this.submit_rgistration_request_btn.show();
            return;
        }


        $.ajax({
            url : somfyData.ajax_url,
            type : 'POST',
            data : {
                action : 'somfy_pro_new_registration_request',
                companyname: companyname,
                city: city,
                companyid: companyid,
                contact_name: contact_name,
                customerEmail: customerEmail,
                activitytype: activitytype,
                phone: phone,
                nonce: somfyData.ajax_nonce
            },
            dataType: "json",
            success: (response) =>{
                if(response.status){
                    this.rgistration_request__form_loader.hide();
                    this.form_submit_success.fadeIn();
                    let closePopupTimer = setTimeout(this.closeRegPopup.bind(this), 2000); 
                } else {
                    // SHOW ERROR
                    this.submit_rgistration_request_btn.show();
                    this.rgistration_request__form_loader.hide();
                    this.form_submit_error.html(response.message).fadeIn();
                }
            },
            error:  (jqXHR, status) => {
                console.log(jqXHR);
                console.log(status);
            }
        });
    }

    closeRegPopup(){
        this.registration_popup.fadeOut();
        this.rgistration_request__form_loader.hide();
        this.submit_rgistration_request_btn.show();
        this.form_submit_error.html('').hide();
        this.form_submit_success.hide();
    }
}

export default LoginPage;
