
import $ from 'jquery';
import AccountUpdatePopup from '../features/AccountUpdatePopup';

class Account {

    constructor(orderDetailsPopup, orderAgain) {
        this.orderDetailsPopup = orderDetailsPopup;
        this.orderAgain = orderAgain;
        this.accountUpdatePopup = new AccountUpdatePopup();

        // account tabs
        this.tab_trigger = $('.tab_trigger');
        
        // account orders
        this.accountOrders__loader = $('#account_orders__loader');
        this.no_orders__message = $('#no_orders__message');
        this.error_getting_data = $('#error_getting_data');
        this.orders_resultes__container = $('.orders_resultes__container');
        
        // order details popup
        this.sort_orders_btn = $('.sort_orders_btn');

        this.getUserOrders();


        this.initEvents();
    }

    initEvents() {
        // account tabs
        this.tab_trigger.on('click', (e)=>{
            let target = e.currentTarget.dataset.target;
            let isActive = $(e.currentTarget).hasClass('active');

            if(!isActive){
                $('.tab_trigger.active').removeClass('active');
                $('.account_tab__content.active').removeClass('active');

                $(e.currentTarget).addClass('active');
                $('#'+target).addClass('active');
                window.location.hash = '#' + target;
                this.scrollToTop();
            }
        });


        // show tab from hash
        this.setTabFromHash();

        this.sort_orders_btn.on('click', (e) =>{
            let target = e.currentTarget.dataset.for;
            let order = e.currentTarget.dataset.order;
            let sorttype = e.currentTarget.dataset.sorttype;

            let newOrder = order == 'asc' ? 'desc' : 'asc';
            $(e.currentTarget).attr('data-order', newOrder);
            
            this.sortByDate(target, sorttype, order);
        })
    }


    setTabFromHash(){
        let url = window.location.href;
        if(url.indexOf("#") > 0){
            let hash = url.substring(url.indexOf("#")+1);
            if(hash != ''){
                $('.tab_trigger.active').removeClass('active');
                $('.account_tab__content.active').removeClass('active');
    
                $('.tab_trigger[data-target=' + hash + ']').addClass('active');
                $('#'+hash).addClass('active');
                this.scrollToTop();
            } 
        }
    }


    getUserOrders(){
        let userInternalid = $('#user_internalid').val();
        
        this.accountOrders__loader.show();

        $.ajax({
            url : somfyData.ajax_url,
            type : 'POST',
            data : {
                action : 'somfy_pro_user_orders',
                user_internalid: userInternalid,
                nonce: somfyData.ajax_nonce
            },
            dataType: "json",
            success: (response) =>{
                if(response.status){
                    if(response.userOrders && response.userOrders.length > 0){
                        // print active orders
                        // print old orders
                        this.printUserOrders(response.userOrders);
                        // show results
                        this.orders_resultes__container.show();
                    } else {
                        // show no results msg
                        this.no_orders__message.removeClass('hidden');
                    }
                } else {
                    // SHOW ERROR
                    this.error_getting_data.removeClass('hidden');
                }
                
                this.accountOrders__loader.hide();

            },
            error:  (jqXHR, status) => {
                console.log(jqXHR);
                console.log(status);
            }
        });
    }


    printUserOrders(orders) {
        let activeOrdersTbody = $('tbody[data-for="active_orders_results"]');
        let oldOrdersTbody = $('tbody[data-for="orders_history_results"]');
        for (let order of orders) {
            if(!order.tranid || order.tranid == null){
                continue;
            } 
            let orderStatus = '';

            if(order.status == 'Cancelled'){
                orderStatus = 'בוטלה';
            } else if(order.status == 'Closed'){
                orderStatus = 'סגורה';
            } else {
                if(order.delivery_Data.length == 0){ // new order with no tracking data
                    orderStatus = 'הזמנה נקלטה והועברה לטיפול';
                } else {
                    let newestDeliveryStatus = '131';
                    order.delivery_Data.forEach(del =>{
                        if(del.custrecord_fc_dn_status = "132"){ // 132 = Shipped
                            newestDeliveryStatus = "132";
                        }
                    });
                    
                    if(order.pickup == 'T'){
                        if(newestDeliveryStatus == "131"){// 131 = packed
                            orderStatus = 'הזמנה נקלטה והועברה לליקוט';
                        } else {// 132 = Shipped
                            orderStatus = 'ההזמנה מוכנה לאיסוף במחסן';
                        }
                    } else {
                        if(newestDeliveryStatus == "131"){// 131 = packed
                            orderStatus = 'הזמנה לוקטה והועברה לשילוח';
                        } else {// 132 = Shipped
                            orderStatus = 'הזמנה בהפצה';
                        }
                    }
                    
                } 
            }

            let deliveryHtml = '';
            if(order.delivery_Data.length > 0){ // new order with no tracking data
                order.delivery_Data.forEach(del =>{
                    if(del.custrecord_fc_dn_status = "132"){ // 132 = Shipped
                        deliveryHtml += `<a href='http://fcx.co.il/he/Track/BoxitSearch' target='_blank' class='delivery_rec' alt='${del.name}'>${del.name}</a>`;
                    }

                });

            } 
            
            let orderTrHtml = `<tr>
                    <th scope='row' class='order_num order_cell'>
                        <a class='show_order_items' data-order='${order.tranid}' alt='${order.tranid}'>${order.tranid}</a></td>
                    </th>
                    <td class='nowrap order_cell order_date'>${order.saleseffectivedate}</td>
                    <td class='nowrap order_cell order_total'>${this.numberWithCommas(order.total)} ₪</td>
                    <td class='order_status order_cell'>${orderStatus}</td>
                    <td class='nowrap order_tracking order_cell'>${deliveryHtml}</td>
                    <td class='order_actions'>
                        <a class='sf_btn sf_btn__small show_order_items' data-order='${order.tranid}' alt='פרטי הזמנה'>פרטי הזמנה</a>
                        <a class='sf_btn sf_btn__small order_again_btn' data-order='${order.tranid}' alt='להזמין חדש'>להזמין חדש</a>
                    </td>
                    <td class='nowrap order_paid order_cell'>
                        ${order.status == 'Billed' ? '<span class="paid_lb">שולם</span>' : '<span class="not_paid_lb">לא שולם</span>'}
                    </td>
                </tr>`;

            if(order.status == 'Closed' || order.status == 'Billed' || order.status == 'Cancelled'){
                oldOrdersTbody.append(orderTrHtml);
            } else {
                activeOrdersTbody.append(orderTrHtml);
            }
        };
        this.scrollToTop();

        this.orderDetailsPopup.initShowOrderItemsEvent();
        this.orderAgain.initOrderAgianEvents();
    }

    convertDate(d) {
        var p = d.split("/");
        return +(p[2]+p[1]+p[0]);
    }
      
    sortByDate(target, sorttype, order = 'asc') {
        var tbody = document.querySelector("tbody[data-for='" + target + "']");
        // get trs as array for ease of use
        var rows = [].slice.call(tbody.querySelectorAll("tr"));
        
        if(sorttype == 'date'){
            rows.sort(function(a,b) {
                let aArr = a.cells[1].innerHTML.split("/");
                let aVal = 0 + (aArr[2]+aArr[1]+aArr[0]);
                let bArr = b.cells[1].innerHTML.split("/");
                let bVal = 0 + (bArr[2]+bArr[1]+bArr[0]);
                
                if(order == 'asc'){
                    return bVal - aVal;
                } else {
                    return aVal - bVal;
                }
            });
        } else if(sorttype == 'price'){
            rows.sort(function(a,b) {
                let av = parseFloat(a.cells[2].innerHTML.replace(" ₪", "").replace(",", ""));
                let bv = parseFloat(b.cells[2].innerHTML.replace(" ₪", "").replace(",", ""));
                return order == 'asc' ? bv - av : av - bv;
            });
        }
        
        
        rows.forEach(function(v) {
          tbody.appendChild(v); // note that .appendChild() *moves* elements
        });
      }
      
    
    numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    scrollToTop(){
        $('html, body').animate({
            scrollTop: $("body").offset().top - 400
        }, 250);
    }
}

export default Account;
    