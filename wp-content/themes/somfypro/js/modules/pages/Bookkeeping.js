
import $ from 'jquery';
class Bookkeeping {

    constructor(orderDetailsPopup) {
        this.orderDetailsPopup = orderDetailsPopup;
        
        this.customer_balance__loader = $('#customer_balance__loader');
        this.customer_balance__container = $('.customer_balance__container');
        
        this.customer_balance_tbody = $('#customer_balance_tbody');
        this.balance_due__val = $('#balance_due__val');

        this.submit_customer_balance_form = $('#submit_customer_balance_form');
        this.customer_balance__error = $('#customer_balance__error');
        this.customer_balance__all_clean = $('#customer_balance__all_clean');
        
        
        // balance form
        this.balanceResultsFromERP = [];
        this.balanceFormData = [];
        this.total_to_pay_elm = $('.total_to_pay');
        this.totalToPay = 0;
        this.sort_orders_btn = $('.sort_orders_btn');
        this.customer_balance_form_error = $('#customer_balance_form_error');
        this.customer_balance_form__loader = $('#customer_balance_form__loader');

        // balance_customerDetailes_popup
        this.balance_customerDetailes_popup = $('#balance_customerDetailes_popup');
        this.balance_customerDetailes__form = $('#balance_customerDetailes__form');
        this.balance_customerDetailes_popup__close = $('.balance_customerDetailes_popup__close');
        this.submit_balance_customerDetailes = $('#submit_balance_customerDetailes');
        this.balance_customerDetailes__form_loader = $('#balance_customerDetailes__form_loader');
        this.balance_customerDetailes__form_submit_success = $('#balance_customerDetailes__form_submit_success');
        this.balance_customerDetailes__form_submit_error = $('#balance_customerDetailes__form_submit_error');
        this.finalItems = [];

        // pagination
        this.balance_due_total = 0;
        this.loadMore = false;
        this.loadMoreBtn = $('#load_more_balance');
        this.maxPages = 1;
        this.paginators = {
            page: 1,
            limit: 10
        }

        this.getCustomerBalance();

        this.initEvents();
    }

    initEvents(){
        this.balance_customerDetailes_popup__close.on('click',()=>{
            this.close_balance_customerDetailes_popup();
        });


        this.balance_customerDetailes__form.on('submit', (e)=>{
            e.preventDefault();
            this.balance_customerDetailes__form_loader.show();
            this.submit_balance_customerDetailes.hide();
            // validate and send form
            this.send_form__balance_customerDetailes();
        })

        this.loadMoreBtn.on('click', (e)=>{
            this.loadMoreBtn.addClass('spin');

            this.paginators.page += 1;
            this.loadMore = true;
            this.getCustomerBalance();
        })
    }


    send_form__balance_customerDetailes(){
        let user_contact = $('#balance_customerDetailes__contact').val();
        let user_email = $('#balance_customerDetailes__email').val();
        let phone = $('#balance_customerDetailes__phone').val(); 

        let error = false;
        if(user_contact.length < 3 || user_contact == ''){
            $('p.form_error[data-input="balance_customerDetailes__contact"]').show();
            error = true;
        } else {
            $('p.form_error[data-input="balance_customerDetailes__contact"]').hide();
        }


        if(user_email.length < 3 || user_email == ''){
            $('p.form_error[data-input="balance_customerDetailes__email"]').show();
            error = true;
        }else {
            $('p.form_error[data-input="balance_customerDetailes__email"]').hide();
        }

        var regex = /^0(5[^7]|[2-4]|[8-9]|7[0-9])[0-9]{7}$/;
        if(phone.length < 8 || phone == '' || !regex.test(phone.replace("-", ""))){
            $('p.form_error[data-input="balance_customerDetailes__phone"]').show();
            error = true;
        } else {
            $('p.form_error[data-input="balance_customerDetailes__phone"]').hide();
        }

        if(error){
            this.balance_customerDetailes__form_loader.hide();
            this.submit_balance_customerDetailes.show();
            return;
        }

        let items = [];
        for (const [key, value] of Object.entries(this.balanceFormData)) {
            let tranid = key;
            let amount = value;

            for(let i = 0; i < this.balanceResultsFromERP.length; i++){
                if(this.balanceResultsFromERP[i].tranid == key){
                    items.push({
                        tranid: tranid,
                        amount: amount,
                        itemData: this.balanceResultsFromERP[i]
                    })
                    break;
                }
            }
        }

        $.ajax({
            url : somfyData.ajax_url,
            type : 'POST',
            data : {
                action : 'somfy_pro_new_balance_payment_request',
                user_contact: user_contact,
                user_email: user_email,
                phone: phone,
                items: items,
                nonce: somfyData.ajax_nonce
            },
            dataType: "json",
            success: (response) =>{
                if(response.status){
                    this.balance_customerDetailes__form_loader.hide();
                    this.balance_customerDetailes__form_submit_success.fadeIn();
                    let closePopupTimer = setTimeout(this.close_balance_customerDetailes_popup.bind(this), 2000); 
                } else {
                    // SHOW ERROR
                    this.submit_balance_customerDetailes.show();
                    this.balance_customerDetailes__form_loader.hide();
                    this.balance_customerDetailes__form_submit_error.html(response.message).fadeIn();
                }
            },
            error:  (jqXHR, status) => {
                console.log(jqXHR);
                console.log(status);
            }
        });
    }

    initBalanceFormEvents(){
        let cbInv = $('#customer_balance__table').find('.select_filter__option_checkbox');
        let balanceAmountInputs = $('#customer_balance__table').find('.balance_amount__input');
        
        // checkbox click
        cbInv.on('change', (e)=>{
            let isChecked = e.currentTarget.checked;
            let tranid = e.currentTarget.id;
            
            if(isChecked){
                let val = $('.balance_amount__input[data-tranid="' + tranid + '"]').val();
                this.balanceFormData[tranid] = val == '' ? 0 : val;
            } else {
                delete this.balanceFormData[tranid];
            }

            // update total to pay
            this.updateTotalToPay();
        });

        // balance input change
        balanceAmountInputs.off('change').on('change', (e)=>{
            let tranid = e.currentTarget.dataset.tranid;
            let val = parseFloat(e.currentTarget.value);
            let maxVal = parseFloat(e.currentTarget.max);

            if(val > maxVal){
                $(e.currentTarget).val(maxVal);
                val = maxVal;
            } 

            this.balanceFormData[tranid] = val == '' ? 0 : val;

            let lb = $('#customer_balance__table').find('.select_filter_btn[for="'+ tranid +'"]');
            let cb = $('.select_filter__option_checkbox[data-tranid="' + tranid + '"]');
            if(!cb[0].checked){
                $(lb).trigger('click');
            }

            // update total to pay
            this.updateTotalToPay();
        })

        // sort
        this.sort_orders_btn.off('click').on('click', (e) =>{
            let target = e.currentTarget.dataset.for;
            let order = e.currentTarget.dataset.order;

            let newOrder = order == 'asc' ? 'desc' : 'asc';
            $(e.currentTarget).attr('data-order', newOrder);
            
            this.sortByDate(target, order);
        })

        // submit
        this.submit_customer_balance_form.off('click').on('click', (e)=>{
            if(this.totalToPay == 0){
                // show nothing selected message
                this.customer_balance_form_error.html('לא נבחרו חשבוניות לתשלום או לא הוזן סכום').show();
            } else {
                // open popup
                this.customer_balance_form_error.hide();
                this.show_balance_customerDetailes_popup()
            }
        })
    }

    show_balance_customerDetailes_popup(){
        let defualt_user_contact = $('#defualt_user_contact').val();
        let defualt_user_email = $('#defualt_user_email').val();
        let defualt_phone = $('#defualt_phone').val();
        
        // user fields
        $('#balance_customerDetailes__contact').val(defualt_user_contact)
        $('#balance_customerDetailes__email').val(defualt_user_email)
        $('#balance_customerDetailes__phone').val(defualt_phone)


        this.finalItems = [];
        let previewTableHtml = '<ul>';

        for (const [key, value] of Object.entries(this.balanceFormData)) {
            let tranid = key;
            let amount = value;

            for(let i = 0; i < this.balanceResultsFromERP.length; i++){
                if(this.balanceResultsFromERP[i].tranid == key){
                    this.finalItems.push({
                        tranid: tranid,
                        amount: amount,
                        itemData: this.balanceResultsFromERP[i]
                    });

                    previewTableHtml += `<li>
                        <span class="inv">${tranid}</span>
                        <span class="amount">${this.numberWithCommas(amount)} ₪</span>
                    </li>`;
                    break;
                }
            }
        }
        previewTableHtml += '<ul>';

        $('#payment_request__preview__total').html('סה"כ לתשלום: ' + this.numberWithCommas(this.totalToPay) + '  ₪');
        $('#payment_request__preview__table').html(previewTableHtml);
        
        this.submit_balance_customerDetailes.show();
        this.balance_customerDetailes__form_submit_success.hide();
        this.balance_customerDetailes__form_submit_error.hide();

        this.balance_customerDetailes__form_loader.hide()
        this.balance_customerDetailes_popup.fadeIn();
    }

    close_balance_customerDetailes_popup(){
        this.balance_customerDetailes_popup.fadeOut();
    }

    updateTotalToPay(){
        let total = 0;
        if(Object.keys(this.balanceFormData).length > 0){

            for (const [key, value] of Object.entries(this.balanceFormData)) {
                total += parseFloat(value);
            }
            this.total_to_pay_elm.html('סה"כ לתשלום: ' + this.numberWithCommas(total.toFixed(2)) + " ₪").show();
        } else {
            this.total_to_pay_elm.html('').hide();
        }
        this.totalToPay = total;
    }


    getCustomerBalance(){
        
        $.ajax({
            url : somfyData.ajax_url,
            type : 'POST',
            data : {
                action : 'somfy_pro_customer_balance',
                paginators: this.paginators,
                nonce: somfyData.ajax_nonce
            },
            dataType: "json",
            success: (response) =>{
                if(response.status){
                    let balanceResults = response.balanceResultsData;
                    let termsLimit = response.termDaysLimit;

                    if(balanceResults.length <= 0){
                        // all clean
                        this.customer_balance__all_clean.show();
                    } else {
                        // printBalanceTable
                        this.balance_due_total = response.totalOverdue;
                        this.balanceResultsFromERP = balanceResults.concat(this.balanceResultsFromERP);
                        this.printBalanceTable(balanceResults, termsLimit);
                    }

                    if(response.maxPages > 1 && this.paginators.page < response.maxPages){
                        // show load more btn
                        this.loadMoreBtn.show();
                    } else {
                        this.loadMoreBtn.hide();
                    }
                } else {
                    // SHOW ERROR
                    this.customer_balance__error.html(response.message).show();
                }
                this.loadMoreBtn.removeClass('spin');

                this.customer_balance__loader.hide();
            },
            error:  (jqXHR, status) => {
                console.log(jqXHR);
                console.log(status);
            }
        });
    }


    printBalanceTable(balanceResults, termsLimit){
        if(!this.loadMore){
            this.customer_balance_tbody.html('');
        }

        
        balanceResults.forEach(result => {
            let isOverDueClass = '';
            if(termsLimit && termsLimit > 0){
                if(result.daysopen > termsLimit){
                    isOverDueClass = 'overdue';
                } else if(result.daysopen == termsLimit){
                    isOverDueClass = 'due';
                }
            }
            let balanceResultsHtml = `<tr class='${isOverDueClass}'>
                    <td>
                        <a class='select_filter__option'>
                            <input type='checkbox' 
                                class='select_filter__option_checkbox' 
                                data-tranid='${result.tranid}'
                                id='${result.tranid}'>
                            <label class='select_filter_btn' for='${result.tranid}'>${result.tranid}</label>
                        </a>
                    </td>
                    <td>${result.createdfrom == '' ? '' : result.orderData.tranid}</td>
                    <td>${result.trandate}</td>
                    <td>${this.numberWithCommas(result.amount)} ₪</td>
                    <td>${this.numberWithCommas(result.fxamount)} ₪</td>
                    <td>
                        ${result.createdfrom == '' ? '' : `<a class='sf_btn sf_btn__small show_order_items' data-order='${result.orderData.tranid}'>פרטים</a>`}
                    </td>
                    <td class='actions_td'>
                        <div class="mb-3 form-input__group">
                            <input 
                                type="number" 
                                class="form-control form-control-sm balance_amount__input" 
                                data-tranid='${result.tranid}' min='0' max='${result.fxamount}' placeholder="הקלד סכום">
                        </div>
                    </td>
                </tr>`;

            this.customer_balance_tbody.append(balanceResultsHtml);
        });

        this.balance_due__val.html(`${this.numberWithCommas(this.balance_due_total.toFixed(2))} ₪`);
        this.customer_balance__container.show();

        this.initBalanceFormEvents();
        this.orderDetailsPopup.initShowOrderItemsEvent();
    }

    numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    sortByDate(target, order = 'asc') {
        var tbody = document.querySelector("#customer_balance_tbody");
        // get trs as array for ease of use
        var rows = [].slice.call(tbody.querySelectorAll("tr"));
        rows.sort(function(a,b) {
            let aArr = a.cells[1].innerHTML.split("/");
            let aVal = 0 + (aArr[2]+aArr[1]+aArr[0]);
            let bArr = b.cells[1].innerHTML.split("/");
            let bVal = 0 + (bArr[2]+bArr[1]+bArr[0]);
            
            if(order == 'asc'){
                return bVal - aVal;
            } else {
                return aVal - bVal;
            }
        });
        
        rows.forEach(function(v) {
          tbody.appendChild(v); // note that .appendChild() *moves* elements
        });
    }
}

export default Bookkeeping;
