
import $ from 'jquery';
class News {

  constructor() {

    this.initNews();
    
  }

  initNews(){

    var tabnum;
    $('.news_tab').on('click' , function(e){
      $('.news_tab').removeClass('active');
      $(this).addClass('active');
      var tabnum = $(this).attr('data-tab');
      $('.tabs-content').hide();
      $('.tabs-content[data-tab="'+tabnum+'"]').show();
    });


    function load_studies(postype,tabnum){
      let ppp = 3; // Post per page
      let pageNumber = 1;
      pageNumber++;
      var max_updates = $('.tabs-content[data-tab="'+tabnum+'"]').attr('data-maxpages');
        $.ajax({
            type: "POST",
            dataType: "html",
            url: somfyData.ajax_url,
            data: {
                'pageNumber' : pageNumber,
                'ppp' : ppp,
                'action' : 'more_post_ajax',
                'postype' : postype
            },
            beforeSend: function() {
                $(".load_more_updates[data-tab='"+tabnum+"']").addClass('loading');
            },
            success: function(data){
                var $data = $(data);
                if($data.length ){
                    $(".tabs-content[data-tab='"+tabnum+"'] .tab-posts").append($data);
                }
                if( pageNumber >=  max_updates ) {
                  $(".load_more_updates[data-tab='"+tabnum+"']").css({
                    'visibility' : 'hidden'
                  });
              }
              $('.load_more_updates').removeClass('loading')
            },
            error : function(jqXHR, textStatus, errorThrown) {
                $loader.html(jqXHR + " :: " + textStatus + " :: " + errorThrown);
            }
        });
        return false;
    }




    function load_updates(postype,tabnum){
    let ppp = 3; // Post per page
    let pageNumber = 1;
    pageNumber++;
    var max_updates = $('.tabs-content[data-tab="'+tabnum+'"]').attr('data-maxpages');
      $.ajax({
          type: "POST",
          dataType: "html",
          url: somfyData.ajax_url,
          data: {
              'pageNumber' : pageNumber,
              'ppp' : ppp,
              'action' : 'more_post_ajax',
              'postype' : postype
          },
          beforeSend: function() {
              $(".load_more_updates[data-tab='"+tabnum+"']").addClass('loading');
          },
          success: function(data){
              var $data = $(data);
              if($data.length ){
                  $(".tabs-content[data-tab='"+tabnum+"'] .tab-posts").append($data);
              }
              if( pageNumber >=  max_updates ) {
                  $(".load_more_updates[data-tab='"+tabnum+"']").css({
                    'visibility' : 'hidden'
                  });
              }
              $('.load_more_updates').removeClass('loading')

          },
          error : function(jqXHR, textStatus, errorThrown) {
              $loader.html(jqXHR + " :: " + textStatus + " :: " + errorThrown);
          }
      });

      return false;
    }


    $(document).on("click",".load_more_updates",function(){ // When btn is pressed.
        let tabnum = $(this).attr('data-tab');
        let post_type = $(this).attr('data-postype');
        if(post_type === 'costumer-updates') {
          load_updates(post_type,tabnum);
        } else if(post_type === 'studies'){
          load_studies(post_type,tabnum);
        }
    });


    $('#rgistration_request__form').on('submit', (e)=>{
      e.preventDefault();
      $('#rgistration_request__form_loader').show();
      $('#submit_rgistration_request').hide();
      $('#rgistration_request__form_submit_success').hide();
      $('#rgistration_request__form_submit_error').html('').hide();

      // validated form
      let eventID = $('#hidden_post').val();
      let contact_name = $('#rgistration_request__contact_name').val();
      let user_email = $('#rgistration_request__email').val();
      let phone = $('#rgistration_request__phone').val();
      let participants = $('#rgistration_request__participants').val();
      let accept_terms = $('#accept_terms')[0].checked;

      let error = false;
      if(participants.length == 0 || participants == ''){
        $('p.form_error[data-input="rgistration_request__participants"]').show();
        error = true;
      } else {
          $('p.form_error[data-input="rgistration_request__participants"]').hide();
      }


      if(contact_name.length < 3 || contact_name == ''){
          $('p.form_error[data-input="rgistration_request__contact_name"]').show();
          error = true;
      } else {
          $('p.form_error[data-input="rgistration_request__contact_name"]').hide();
      }


      if(user_email.length < 3 || user_email == ''){
          $('p.form_error[data-input="balance_customerDetailes__email"]').show();
          error = true;
      }else {
          $('p.form_error[data-input="balance_customerDetailes__email"]').hide();
      }

      var regex = /^0(5[^7]|[2-4]|[8-9]|7[0-9])[0-9]{7}$/;
      if(phone.length < 8 || phone == '' || !regex.test(phone.replace("-", ""))){
          $('p.form_error[data-input="rgistration_request__phone"]').show();
          error = true;
      } else {
          $('p.form_error[data-input="rgistration_request__phone"]').hide();
      }

      if(error){
          $('#rgistration_request__form_loader').hide();
          $('#submit_rgistration_request').show();
          return;
      }


      $.ajax({
        url : somfyData.ajax_url,
        type : 'POST',
        data : {
            action : 'somfy_pro_new_event_registration',
            eventID: eventID,
            contact_name: contact_name,
            user_email: user_email,
            phone: phone,
            participants: participants,
            accept_terms: accept_terms,
            nonce: somfyData.ajax_nonce
        },
        dataType: "json",
        success: (response) =>{
            if(response.status){
                $('#rgistration_request__form_loader').hide();
                $('#rgistration_request__form_submit_success').fadeIn();
                let closePopupTimer = setTimeout(()=>{
                  $('.studies_popup').fadeOut();
                }, 2000); 
            } else {
                // SHOW ERROR
                $('#submit_rgistration_request').show();
                $('#rgistration_request__form_loader').hide();
                $('#rgistration_request__form_submit_error').html(response.message).fadeIn();
            }
        },
        error:  (jqXHR, status) => {
            console.log(jqXHR);
            console.log(status);
        }
      });
    })

    $(document).on('click', '.toggle-seminar-form', function(){
      var pid = $(this).attr('data-pid');
      var date = $(this).attr('data-date');
      var subject = $(this).attr('data-subject');

      $('#hidden_post').val(pid);
      $('#studie__subject').html(subject);
      $('#studie__date').html(date);

      $('#rgistration_request__form_loader').hide();
      $('#submit_rgistration_request').show();
      $('#rgistration_request__form')[0].reset();

      $('.studies_popup').fadeIn();
    })

    function close_rgistration_request_popup(){
      $('.studies_popup').fadeOut();
    }


    $(document).on('click' , '.files_download_popup .file_downloads_popup__close' , function(){
      $('.studies_popup').fadeOut();
    })
    
    $(document).on('click' , '.studies_popup .studies_popup__close' , function(){
      $('.studies_popup').fadeOut();
    })

      

    $('.file_download').click(function(){

      if( $.trim($(this).find('.content').html()).length ) {
        $('.files_download_popup').fadeIn();
        var list_of_files = $(this).find('.list_of_files').clone();
        $('.files_download_popup .popup__container').html(list_of_files);
      }
      $('.files_download_popup .file_downloads_popup__close').click(function(){
        $(this).parents().closest('.files_download_popup').fadeOut();
      })
    });


      


    function getSliderSettings() {
          return {
            rtl: true,
            arrows: true,
            prevArrow:'<i class="slickleft"> <img src="http://localhost/somfy/pro/wp-content/themes/somfypro/assets/svg/arrow.svg"> </i>',
            nextArrow:'<i class="slickright"><img src="http://localhost/somfy/pro/wp-content/themes/somfypro/assets/svg/arrow.svg"> </i>'
          }
    }

    $('.gal_files a').click(function(e){
      e.preventDefault();
      $('.popup__overlay.gal_events').fadeIn();
      var galcontents = $(this).parent().find('.gal_events_slider').clone();
      $('.popup__overlay.gal_events .inner').html(galcontents);
      $('.gal_events_slider').not('.slick-initialized').slick(getSliderSettings());
    })

    $('.popup__overlay.gal_events').click(function(e){
      $(this).fadeOut(function(){
        $('.gal_events_slider').slick("unslick");
      });
    })

    $('.gal_events_popup__close').on('click' , ()=>{
      $('.gal_events .inner').trigger('click');
    })

    $('.gal_events .inner').click(function(e){
      e.stopImmediatePropagation();
      return false;
    })

  }




}

export default News;
