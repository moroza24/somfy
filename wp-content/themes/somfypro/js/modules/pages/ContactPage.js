import $ from 'jquery';

class ContactPage {
    constructor() {
        this.pro_contact_form__loader = $('#pro_contact_form__loader');
        this.pro_contact_form__formsubmit = $('#pro_contact_form__formsubmit');
        this.pro_contact_form__submit_error = $('#pro_contact_form__submit_error');
        this.pro_contact_form__submit_success = $('#pro_contact_form__submit_success');
        this.initEvents();
    }

    initEvents() {
            $('#pro_contact_form__formsubmit').on('click', (e)=>{
            e.preventDefault();
            this.validateForm();
        })
        $('#pro_contact_form').on('submit', (e)=>{
            e.preventDefault();
            this.validateForm();
        })
    }

    validateForm(){
        this.pro_contact_form__loader.show();
        this.pro_contact_form__formsubmit.hide();
        
        // validate form
        let subsub = $('#pro_contact_form__description').val();
        let department = $('#pro_contact_form__department').val();
        let departmentName = $('#pro_contact_form__department')[0].selectedOptions[0].innerHTML;
        let description = $('#pro_contact_form__description').val();
        
        let error = false;
        if(subsub.length < 3 || subsub == ''){
            $('p.form_error[data-input="pro_contact_form__subsub"]').show();
            error = true;
        } else {
            $('p.form_error[data-input="pro_contact_form__subsub"]').hide();
        }

        if(department.length < 3 || department == ''){
            $('p.form_error[data-input="pro_contact_form__department"]').show();
            error = true;
        } else {
            $('p.form_error[data-input="pro_contact_form__department"]').hide();
        }

        if(description.length < 3 || description == ''){
            $('p.form_error[data-input="pro_contact_form__description"]').show();
            error = true;
        } else {
            $('p.form_error[data-input="pro_contact_form__description"]').hide();
        }

        if(error){
            this.pro_contact_form__loader.hide();
            this.pro_contact_form__formsubmit.show();
            return;
        }


        // send form data
        $.ajax({
            url : somfyData.ajax_url,
            type : 'POST',
            data : {
                action : 'somfy_pro_contact_form_submit',
                subsub: subsub,
                department: department,
                departmentName: departmentName,
                description: description,
                nonce: somfyData.ajax_nonce
            },
            dataType: "json",
            success: (response) =>{
                console.log(response);
                if(response.status){
                    this.pro_contact_form__loader.hide();
                    this.pro_contact_form__submit_success.fadeIn();
                } else {
                    // SHOW ERROR
                    this.pro_contact_form__formsubmit.show();
                    this.pro_contact_form__loader.hide();
                    this.pro_contact_form__submit_error.html(response.message).fadeIn();
                }
            },
            error:  (jqXHR, status) => {
                console.log(jqXHR);
                console.log(status);
            }
        });

    }
    
}

export default ContactPage;
