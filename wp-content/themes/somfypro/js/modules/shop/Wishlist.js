import $ from 'jquery';

class Wishlist {
    
    constructor(stockNotifictaion, cart) {
        this.stockNotifictaion = stockNotifictaion;
        this.cart = cart;

        this.usersWishlist;
        this.currentClickedCTA;
        this.wishlistPage = false;

        this.initEvents();
    }

    initEvents() {
        this.getUsersWishlist();

        this.addClickEvent();
    }


    printUsersWishlistPage(){
        this.wishlistPage = true;
        $('#wishlist_results').html('');
        $('#wishlist_loader').show();
        $('#total_wishlist_results').html('').hide();
        $('#wishlist_no_results_msg').hide();

        $.ajax({
            url : somfyData.ajax_url,
            type : 'POST',
            data : {
                action : 'somfy_pro_get_by_user',
                getFullData: true,
                nonce: somfyData.ajax_nonce
            },
            dataType: "json",
            success: (response) =>{
                if(response.status){
                    let total_wishlist_results = response.usersWishlist.length;

                    if(total_wishlist_results > 0){
                        // update ctas by users wishlist
                        this.printProducts(response.usersWishlist);
                        $('#total_wishlist_results').html('סה"כ תוצאות: ' + total_wishlist_results).show();
                        this.addClickEvent();
                    } else {
                        $('#wishlist_no_results_msg').show();
                    }
                } else {
                    // SHOW ERROR
                    console.warn(response);
                }

                $('#wishlist_loader').hide();

            },
            error:  (jqXHR, status) => {
                console.log(jqXHR);
                console.log(status);
            }
        });
    }

    printProducts(products){
        let resultsHtml = '';

        products.forEach((product) => {

            // print product thumbnail & tags
            resultsHtml += `
                <div class='product-card'>
                    <div class='product-thumbnail-col'>`;
            
            // print discount tag
            if(product.meta.discount_percentage && product.meta.discount_percentage != ''){
                resultsHtml += `
                    <span class="dicount_tag">
                        <span class='tag__title'>הנחה</span><br>
                        ${product.meta.discount_percentage}%
                    </span>`;
            }

            // wishlistCTA
            resultsHtml += `<a class='wishlist_cta active' data-product='${product.product_id}' alt='מועדפים'>
                <i class="fas fa-heart"></i>
            </a>`;

            resultsHtml += `
                    <a href='${product.permalink}' alt='${product.post_title}'>
                        <img 
                            src='${product.thumbnail_url}' 
                            alt='${product.post_title}' 
                            class='product-thumbnail'>
                    </a>
                </div>`;


            // print product title
            resultsHtml += `
                <div class='product-meta-container'>
                    <a href="${product.permalink}" class='product-title' alt='${product.post_title}'>${product.post_title}</a>
                </div>`;

            // print product CTA & prices
            resultsHtml += `
                <hr class="divider">
                <div class="product-cta-container">
                    <div class='product-cta'>
                        <button class='add_to_cart__cta' 
                            data-product='${product.product_id}' 
                            data-add_warranty='false'
                            data-warranty_applicable='${product.warranty_applicable}'>
                                <img 
                                    src="${somfyData.template_url}/assets/svg/002-shopping-cart-light.svg" 
                                    alt="cart" 
                                    loading="lazy">
                                הוסף לסל
                        </button>
                    </div>`;

            
            resultsHtml += `<div class='product-price'>`;
            if(product.meta.on_sell == true){
                resultsHtml += `
                    <div class='top_price'>
                        <span class='old_price'>
                            <span class='currency_symbol'>₪</span>${this.numberWithCommas(product.pricelist.base_price_after_discountcode)}
                        </span>
                        <div class="vl"></div>
                        <span class='discount_price'>
                            <span class='currency_symbol'>₪</span>${this.numberWithCommas(product.meta.discount_price)}
                        </span>
                    </div>`;
                resultsHtml += `<div class='bottomline_price'>מחיר ברוטו: <span class='currency_symbol'>₪</span>${this.numberWithCommas(product.pricelist.base_price)}</div>`;
            } else {
                resultsHtml += `
                    <div class='top_price'>
                        <span class='base_price'>
                            <span class='currency_symbol'>₪</span>${this.numberWithCommas(product.pricelist.base_price_after_discountcode)}
                        </span>
                    </div>`;
                resultsHtml += `<div class='bottomline_price'>מחיר ברוטו: <span class='currency_symbol'>₪</span>${this.numberWithCommas(product.pricelist.base_price)}</div>`;
            }
            resultsHtml += `</div>`;
            resultsHtml += `</div></div>`;
        });
        
        $('#wishlist_results').html(resultsHtml);

        this.stockNotifictaion.initCtaEvents();
        this.addClickEvent();
        this.getUsersWishlist();
        this.cart.initAddToCartEvent();
    }


    getUsersWishlist(){
        $.ajax({
            url : somfyData.ajax_url,
            type : 'POST',
            data : {
                action : 'somfy_pro_get_by_user',
                nonce: somfyData.ajax_nonce
            },
            dataType: "json",
            success: (response) =>{

                if(response.status){
                    // update ctas by users wishlist
                    this.usersWishlist = response.usersWishlist;

                    if(this.usersWishlist.length > 0){
                        this.updateByUsersWishlist();
                        $('#wishlist__trigger').addClass('with-items');
                    } else {
                        $('#wishlist__trigger').removeClass('with-items');
                    }
                } else {
                    // SHOW ERROR
                    console.warn(response);
                }
            },
            error:  (jqXHR, status) => {
                console.log(jqXHR);
                console.log(status);
            }
        });
    }


    updateByUsersWishlist(){
        let usersItems = this.usersWishlist;

        usersItems.forEach(item => {
            $(`.wishlist_cta[data-product="${item.product_id}"]`).addClass('active').html('<i class="fas fa-heart"></i>');
        });
    }


    addClickEvent(){
        let addToWishlist_ctas = $('.wishlist_cta');

        addToWishlist_ctas.off('click').on('click', (e) => {
            this.currentClickedCTA = e.currentTarget;
            let isActive = $(e.currentTarget).hasClass('active');
            let wishlistAction = !isActive ? 'somfy_pro_wishlist_add' : 'somfy_pro_wishlist_remove';
            let productID = e.currentTarget.dataset.product;

            $.ajax({
                url : somfyData.ajax_url,
                type : 'POST',
                data : {
                    action : wishlistAction,
                    productID: productID,
                    nonce: somfyData.ajax_nonce
                },
                dataType: "json",
                success: (response) =>{
    
                    if(response.status){
                        $(this.currentClickedCTA).toggleClass('active');
                        
                        if($(this.currentClickedCTA).hasClass('active')){
                            $(this.currentClickedCTA).html('<i class="fas fa-heart"></i>');
                        } else {
                            $(this.currentClickedCTA).html('<i class="far fa-heart"></i>');
                        }

                        if(this.wishlistPage) this.printUsersWishlistPage();
                        this.getUsersWishlist();
                    } else {
                        // SHOW ERROR
                        if(!response.user_logged){
                            // show popup - need to be registered user to use wishlist
                            $('#wishlist_popup').fadeIn('fast');
                            $('.popup__overlay__close').on('click', (e) => {
                                $('#wishlist_popup').fadeOut('fast');
                            })
                        }
                    }
                },
                error:  (jqXHR, status) => {
                    console.log(jqXHR);
                    console.log(status);
                }
            });
        })
    }

    numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
}

export default Wishlist;