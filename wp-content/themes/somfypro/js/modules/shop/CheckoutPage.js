import $ from 'jquery';
import AccountUpdatePopup from '../features/AccountUpdatePopup';

class CheckoutPage {

    constructor(cartObj) {
        this.cartObj = cartObj;
        this.accountUpdatePopup = new AccountUpdatePopup();

        // checkout_tabs
        this.checkout_delivery_tab = $('#checkout_delivery_tab');
        this.checkout_payment_tab = $('#checkout_payment_tab');
        this.tab_trig = $('.tab_trig');

        // delivery options toggle
        this.delivery_option_trigger = $('input[name="delivery_option"]');
        this.delivery_subtab__delivery = $('#delivery_subtab__delivery');
        this.delivery_subtab__pickup = $('#delivery_subtab__pickup');
        
        // other delivery address form
        this.otherDeliveryAddress__toggle = $('#other_delivery_address__toggle');

        
        // go_to_payment
        this.goToPaymentBtn = $('#go_to_payment');
        this.go_to_deliveryBtn = $('#go_to_delivery_tab');


        // Paymeny options
        this.payment_option_trigger = $('input[name="payment_option"]');


        // Submit order
        this.submitOrderBtn = $('#submit_order');
        this.order_submition_loader = $('.order_submition_loader_container__overlay');
        this.order_submition_loader__container__close = $('#order_submition_loader__container__close');

        // OrderObj
        this.newOrder = {
            orderItems: null,
            order_subSummary: 0,
            order_totalDiscount: 0,
            order_totalPrice: 0,
            paymentmethod: 'credit',
            shipmethod : 'delivery',
            useOtherDeliveryAddress : false,
            user_usage_agreement : false,
            user_updates_agreement : false
        };

        this.deliveryDetails_valid = false;

        this.initEvents();
    }


    initEvents() {
        // delivery options toggle
        this.delivery_option_trigger.on('change' , (e)=>{
            let target = $(e.currentTarget).val();
            
            if(target == 'delivery'){
                this.delivery_subtab__pickup.hide();
                this.delivery_subtab__delivery.show();
                this.newOrder.shipmethod = 'delivery';
                this.newOrder.useOtherDeliveryAddress = $('#other_delivery_address__toggle')[0].checked;
            } else if(target == 'pickup'){
                this.delivery_subtab__delivery.hide();
                this.delivery_subtab__pickup.show();
                this.newOrder.shipmethod = 'pickup';
                this.newOrder.useOtherDeliveryAddress = false;
            }
        })

        // tab_trigs
        this.tab_trig.on('click', (e)=>{
            let target = e.currentTarget.dataset.target;

            if(this.deliveryDetails_valid && target == 'checkout_payment_tab'){
                this.validate_deliveryDetails();
            } else if(!this.deliveryDetails_valid && target == 'checkout_payment_tab'){
                this.validate_deliveryDetails();
            } else if(target == 'checkout_delivery_tab'){
                this.goToDeliveryTab();
            }
        })


        // other delivery address form
        this.otherDeliveryAddress__toggle.on('change', (e)=>{
            let checked = $(e.currentTarget)[0].checked;
            this.newOrder.useOtherDeliveryAddress = checked;

            if(checked){
                $('.request_address_change').show();
            } else {
                $('.request_address_change').hide();
            }
        });


        // go_to_payment
        this.goToPaymentBtn.on('click', ()=>{
            this.validate_deliveryDetails();
        });

        // go_to_delivery
        this.go_to_deliveryBtn.on('click', ()=>{
            this.goToDeliveryTab();
        });

       
        // payment options toggle
        this.payment_option_trigger.on('change' , (e)=>{
            let paymenyOption = $(e.currentTarget).val();
            this.newOrder.paymentmethod = paymenyOption;
        })


        // Submit order
        this.submitOrderBtn.on('click', ()=>{
            this.submitOrder();
        });

        this.order_submition_loader__container__close.on('click', ()=>{
            this.order_submition_loader.hide();
        });
    }


    // Submit order
    submitOrder(){
        // Show loader
        $('#order_submition_loader').show();
        $('#order_submition__success').hide();
        $('#order_submition__error').hide();
        $('.order_submition_loader__actions').hide();
        this.order_submition_loader.fadeIn('fast');

        // get calculator data if exists
        let calcItems = JSON.parse(localStorage.getItem('pro_cart_calcItems'));

        // ajax call - add new order
        $.ajax({
            url : somfyData.ajax_url,
            type : 'POST',
            data : {
                action : 'somfy_pro_new_order',
                newOrder: this.newOrder,
                calcItems: calcItems,
                nonce: somfyData.ajax_nonce
            },
            dataType: "json",
            success: (response) =>{
                if(response.status){
                    // show success message
                    $('#order_submition_loader').hide();
                    $('#order_submition__success').fadeIn('fast');

                    // cleare cart
                    this.cartObj.clearCart(true);


                    // // redirect to thanks page after few seconds
                    let redirectToThanksTimer = setTimeout(this.redirectToThanks.bind(this), 2000); 
                    
                } else {
                    // SHOW ERROR message
                    $('#order_submition_loader').hide();
                    $('.order_submition_loader__actions').fadeIn('fast');
                    $('#order_submition__error').html(response.message).fadeIn('fast');
                }
            },
            error:  (jqXHR, status) => {
                console.log(jqXHR);
                console.log(status);
            }
        });
    }


    redirectToThanks(){
        window.location.replace(somfyData.root_url + '/order-confirmation');
    }

    // Delivery Details
    validate_deliveryDetails(){
        let isValid = true;
        // check delivery option

        // validate user agreements
        let user_updates_agreement = $('#user_updates_agreement');
        let user_usage_agreement = $('#user_usage_agreement');

        if(user_updates_agreement[0].checked){
            this.newOrder.user_updates_agreement = true;
        }

        if(user_usage_agreement[0].checked){
            this.newOrder.user_usage_agreement = true;
            $('.form_error[data-input="user_usage_agreement"]').fadeOut('fast');
        } else {
            // show error message
            $('.form_error[data-input="user_usage_agreement"]').fadeIn('fast');
            isValid = false;
        }

        if(isValid){
            this.goToPaymentTab();
            this.deliveryDetails_valid = true;
        } else {
            this.deliveryDetails_valid = false;
        }

    }

    goToDeliveryTab(){
        // go to payment tab
        this.cartObj.totalSummeryVal.html(this.cartObj.numberWithCommas(this.cartObj.order_totalPrice));

        // this.deliveryDetails_form.show();
        this.checkout_payment_tab.hide();

        $('.tab_trig[data-target="checkout_payment_tab"]').removeClass('active');
        this.checkout_delivery_tab.show();
        $('.tab_trig[data-target="checkout_delivery_tab"]').addClass('active');
        
        $('.cart_preview__container .cart_items__container').show();
        this.newOrder.orderItems = null;
    }

    goToPaymentTab(){
        // go to payment tab
        // this.deliveryDetails_form.hide();
        this.checkout_delivery_tab.hide();
        $('.tab_trig[data-target="checkout_delivery_tab"]').removeClass('active');
        this.checkout_payment_tab.show();
        $('.tab_trig[data-target="checkout_payment_tab"]').addClass('active');

        $('.cart_preview__container .cart_items__container').hide();

        // set final cart items and prices
        //  add shiping pricing - to summary and to newOrder object
        // if shipping method is delivery
        this.newOrder.order_subSummary =  parseFloat(this.cartObj.order_subSummary);
        this.newOrder.order_totalDiscount = parseFloat(this.cartObj.order_totalDiscount);
        this.newOrder.order_totalPrice = parseFloat(this.cartObj.order_totalPrice);
        this.newOrder.orderItems = JSON.parse(localStorage.getItem('pro_cartItems'));

        let free_shipping_threshold = $('#free_shipping_threshold').val();
        let shipping_price = parseInt($('#shipping_price').val());
        let shippingproductID = $('#shipping_product_id').val();

        if((this.newOrder.shipmethod == 'delivery' && this.newOrder.order_totalPrice >= free_shipping_threshold)){
            // dont add shipping to orderItems
            // dont add shipping price to totalPrice
            // hide shipping price
            $('.delivery_summery_val').html('0');
        } else if(this.newOrder.shipmethod == 'delivery' && this.newOrder.order_totalPrice < free_shipping_threshold){
            // add shipping to orderItems
            let shipItemData = {
                productID: shippingproductID,
                amount: 1,
                parentProduct: '',
                childeProduct : ''
            };

            this.newOrder.orderItems.push(shipItemData);

            // add shipping price to totalPrice
            this.newOrder.order_totalPrice += shipping_price;
            this.newOrder.order_subSummary += shipping_price;
            this.cartObj.totalSummeryVal.html(this.cartObj.numberWithCommas(this.newOrder.order_totalPrice));

            // show shipping price
            $('.delivery_summery_val').html(shipping_price);
        } else {
            $('.delivery_summery_val').html('0');
        }

        // scroll to top
        $('html, body').animate({
            scrollTop: $(".checkout_tabs__triggers").offset().top - 400
        }, 250);
    }

    
}

export default CheckoutPage;
