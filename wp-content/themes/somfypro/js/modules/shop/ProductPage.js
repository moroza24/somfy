import $ from 'jquery';

class ProductPage {

    constructor() {
        // Tabs
        this.tabsTrigger = $('.trigger-link');
        this.releatedProducts__slider = $(".releated_products__slider");
        this.productImages__slider = $(".products_images__slider");

        this.stock_quantity_input = $('#stock_quantity_input');
        this.add_warranty_to_product__checkbox = $('#add_warranty_to_product__checkbox');
        this.product_cta__addToCart = $('.product_cta-add-to-cart .add_to_cart__cta');
        this.initEvents();
    }


    initEvents() {
      this.tabsTrigger.on('click', (e)=>{
          let currentTriggert = $(e.currentTarget);
          let target = e.currentTarget.dataset.toggle;

          if(!$('#'+target).hasClass('active')){
              $('.tab-content__container.active').removeClass('active');
              $('#'+target).addClass('active');

              $('.trigger-link.active').removeClass('active');
              currentTriggert.addClass('active');

          }
      })

      
      if(this.releatedProducts__slider.length > 0){
        this.initReleatedProductsSlider(this.releatedProducts__slider.length);
      }
      this.initProductImagesSlider();

      this.stock_quantity_input.on('change', (e)=>{
        let newVal = $(e.currentTarget).val();
        $('#add_warranty_to_product__quantity_input').attr('max', newVal);
      })

      this.add_warranty_to_product__checkbox.on('change', (e)=>{
        let isChecked = e.currentTarget.checked;

        if(isChecked){
            $(this.product_cta__addToCart).attr('data-add_warranty', "true");
        } else {
            $(this.product_cta__addToCart).attr('data-add_warranty', "false");
        }
      })
    }

    initReleatedProductsSlider(slides){
      slides = slides > 4 ? 4 : slides;
      let args = {
        autoplay: false,
        arrows: true,
        dots: true,
        infinite: false,
        slidesToShow: 4,
        swipeToSlide: true,
        prevArrow:"<a class='slick-prev pull-left' alt='prev'><i class='fas fa-chevron-left' aria-hidden='true'></i></a>",
        nextArrow:"<a class='slick-next pull-right' alt='next'><i class='fas fa-chevron-right' aria-hidden='true'></i></a>",
        responsive: [
            {
              breakpoint: 1920,
              settings: {
                slidesToShow: 4,
              }
            },
            {
              breakpoint: 1366,
              settings: {
                slidesToShow: 3,
              }
            },
            {
              breakpoint: 950,
              settings: {
                slidesToShow: 2,
              }
            },
            {
              breakpoint: 630,
              settings: {
                slidesToShow: 1,
              }
            }
        ]
      };
      this.releatedProducts__slider.not('.slick-initialized').slick(args);
    }

    initProductImagesSlider(){
      let args = {
        dots: true,
        arrows: false
      };
      $(".products_images__slider").slick(args);
    }
}

export default ProductPage;
