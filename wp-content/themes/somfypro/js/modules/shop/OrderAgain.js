
import $ from 'jquery';
class OrderAgain {
    constructor(cart) {
        this.cartObj = cart;
    }

    initOrderAgianEvents(){
        let orderAgainBtn= $('.order_again_btn');

        orderAgainBtn.off('click').on('click', (e)=>{
            let OrderTranid = e.currentTarget.dataset.order;
            this.reorderItems(OrderTranid);
        })
    }

    reorderItems(OrderTranid){
        this.cartObj.addToCartPopupLoader.show();
        this.cartObj.addToCartPopupSuccess.hide();
        this.cartObj.addToCartPopup.fadeIn('fast');

        $.ajax({
            url : somfyData.ajax_url,
            type : 'POST',
            data : {
                action : 'somfy_pro_order_details',
                order_tranid: OrderTranid,
                nonce: somfyData.ajax_nonce
            },
            dataType: "json",
            success: (response) =>{
                if(response.status){
                    if(response.items && response.items.length > 0){
                        // reorder
                        
                        for(let i=0; i<response.items.length; i++){
                            let product = response.items[i].productID;
                            let amount = response.items[i].quantity;

                            // add to cart
                            this.cartObj.addToCart(product, amount, '', '', false);
                        }
                        this.cartObj.updateCartDB(this.cartObj.cartItems);
                        this.cartObj.updateCartPopup();
                        this.cartObj.showSuccessAddToCart();

                        // let redirectToThanksTimer = setTimeout(this.redirectTo.bind(this), 2000); 
                    } else {
                        // show no results found
                        console.warn(response);
                    }
                } else {
                    // SHOW ERROR
                    console.warn(response);
                }
            },
            error:  (jqXHR, status) => {
                console.log(jqXHR);
                console.log(status);
            }
        });
    }

    
    redirectTo(){
        window.location.href = somfyData.root_url + "/cart";
    }                        

}

export default OrderAgain;
    