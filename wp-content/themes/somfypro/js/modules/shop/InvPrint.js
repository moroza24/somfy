import $ from 'jquery';
class InvPrint {

    constructor() {
        this.currentLoader;
        this.isLoading = false;
    }
    initInvDownloadEvent(){
        $('.inv_file_download').off('click').on('click', (e)=>{
            if(!this.isLoading){
                this.isLoading = true;

                this.currentLoader = $(e.currentTarget).siblings('.loader');

                this.currentLoader.css('visibility', 'visible');
                $('.inv_download_error').html('').hide();

                let recordid = e.currentTarget.dataset.recordid;

                this.getInv(recordid);
            }
        })
    }

    getInv(recordid){
        $.ajax({
            url : somfyData.ajax_url,
            type : 'POST',
            data : {
                action : 'somfy_pro_get_inv_file',
                recordid: recordid,
                nonce: somfyData.ajax_nonce
            },
            dataType: "json",
            success: (response) =>{
                if(response.status){;
                    this.downloadPDF(response.invFileData.content, response.invFileData.documentNumber);
                } else {
                    $('.inv_download_error').html(response.message).fadeIn();
                }

                this.currentLoader.css('visibility', 'hidden');
                this.currentLoader = null;
                this.isLoading = false;
            },
            error:  (jqXHR, status) => {
                console.log(jqXHR);
                console.log(status);
            }
        });
    }

    downloadPDF(pdf, fileName) {
        let downloadLink = document.createElement("a");
    
        downloadLink.href = `data:application/pdf;base64,${pdf}`;
        downloadLink.download = `${fileName}.pdf`;
        downloadLink.click();
    }
}
export default InvPrint;
