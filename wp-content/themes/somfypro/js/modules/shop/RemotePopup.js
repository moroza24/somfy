import $ from 'jquery';

class RemotePopup {
    constructor(cart) {

        this.cartObj = cart;

        this.popupContainer = $('#remote_popup');
        this.remote__content = $('#remote_card__container')
        this.remote_popup__close = $('#remote_popup__close');

        this.add_remote_cta = $('#add_remote_cta');
        this.remote_title = $('#remote_title');
        this.remote_price = $('#remote_price');

        this.initEvents();
    }


    initEvents() {
        this.remote_popup__close.on('click', ()=>{
            this.popupContainer.fadeOut('fast');
        })
    }

    showRemotePopup(productID, amount) {
        let itemData = JSON.parse(localStorage.getItem('pro_cust_radio_remote'));

        this.remote_title.html('');
        this.remote_price.html('');
        this.add_remote_cta.attr({
            'data-product': '',
            'data-parent': '',
            'data-amount': '',
            'data-originalamount': ''
        });

        if(itemData){
            this.remote_title.html(itemData.title);
            
            let priceText = '';
            if(parseInt(itemData.price_list.base_price_after_discountcode) == 0){
                priceText = 'בחינם';
            } else {
                priceText = `בעלות של ${itemData.price_list.base_price_after_discountcode} ₪`;
            }
            this.remote_price.html(priceText);

            this.add_remote_cta.attr({
                'data-product': itemData.productID,
                'data-parent': productID,
                'data-amount': amount,
                'data-originalamount': amount
            });

            this.printItemData(itemData, amount);
            this.popupContainer.fadeIn('fast');
        }
    }

    printItemData(itemData, amount){
        // print product thumbnail & tags
        let resultsHtml = `
            <div class='product-thumbnail-col'>
                <a href='${itemData.permalink}${this.isCalcItem ? '?calculator=true' : ''}' alt='${itemData.title}'>
                    <img 
                        src='${itemData.thumbnail_url}' 
                        alt='${itemData.title}' 
                        class='product-thumbnail'>
                </a>
            </div>`;
        // end - product thumbnail & tags


        // print content
        resultsHtml += `<div class='product_info_container'>`


        // print product title
        resultsHtml += `
            <div class='product-meta-container'>
                <p>מק"ט: ${itemData.itemid}</p>
                <a href="${itemData.permalink}${this.isCalcItem ? '?calculator=true' : ''}" class='product-title' alt='${itemData.title}'>${itemData.title}</a>
            </div>`;

        // amount input
        resultsHtml += `<div class="item_amount">
            <label for="item_amount__${itemData.productID}">כמות:</label>
            <input type="number" class="item_amount_input form-control form-control-sm" 
                id="radio_remote_amount"
                data-product="${itemData.productID}" 
                min="1" max="${amount}" 
                value="1">
        </div>`;

        // end - content
        resultsHtml += `</div>`;


        this.remote__content.html(resultsHtml);

        this.initAddRemoteAmountChange();
        this.initAddRemoteCTA();
    }


    initAddRemoteAmountChange(){
        let radio_remote_amount = $('#radio_remote_amount');

        radio_remote_amount.off('change').on('change', (e)=>{
            $('#add_remote_cta').attr('data-amount', radio_remote_amount.val());
        })
    }

    initAddRemoteCTA(){
        let remoteCta = $('#add_remote_cta');

        remoteCta.off('click').on('click', (e)=>{
            let product = e.currentTarget.dataset.product;
            let parent = e.currentTarget.dataset.parent;
            let amount = e.currentTarget.dataset.amount;
            let originalamount = e.currentTarget.dataset.originalamount;


            this.popupContainer.fadeOut('fast');
            this.cartObj.addToCartPopup.fadeIn('fast');
            this.cartObj.addToCart(product, amount, parent, "", true);
            // this.cartObj.updateCartItem(parent, originalamount, '', product, e.currentTarget);

            this.cartObj.updateCartDB(this.cartObj.cartItems);
            this.cartObj.updateCartPopup();
        })
    }
}


export default RemotePopup;
    