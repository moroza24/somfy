import $ from 'jquery';
import Cookies from 'js-cookie'
import StockNotifictaionMax from './StockNotifictaionMax';
import RemotePopup from './RemotePopup';

class Cart {

    constructor() {
        this.stockNotifictaionMax = new StockNotifictaionMax();
        this.remotePopup = new RemotePopup(this);

        // cart popup
        this.cartPopupTrigger = $('.cart__trigger');
        this.cartPopupClose = $('.cart_popup__close');
        this.cartPopup = $('#cart_popup__container');

        // add to cart flow
        this.addToCartPopupClose = $('.add_to_cart__popup__close');
        this.addToCartPopup = $('#add_to_cart__popup');
        this.addToCartPopupLoader = $('#add_to_cart__loader');
        this.addToCartPopupSuccess = $('.add_to_cart__success');
        this.oldAmountInputVal = null;

        this.userIsLoggedIn = false;

        // cart results 
        this.cartItemsContainer = $('.cart_items__container');
        this.cartitemsTable = $('#cart_items__table');
        this.cartItemsTbody = $('#cart_items__tbody');
        this.cartPage_summery = $('#cart_page_summery');
        this.cartSummeryContainer = $('.cart_summery');
        this.subSummeryVal = $('.sub_summery_val');
        this.discountSummeryVal = $('.discount_summery_val');
        this.totalSummeryVal = $('.total_summery_val');
        this.catalogySummeryVal = $('.catalogy_summery_val');
        this.noCartItemsMsg = $('.not_cart_items');

        this.cart_result__loader = $('#cart_result__loader');

        this.cartItems;
        this.order_subSummary;
        this.order_totalDiscount;
        this.order_totalPrice;

        // add warranty popup
        this.add_warranty_popup = $('#add_warranty_popup');
        this.add_warranty_popup__close = $('#add_warranty_popup .popup__overlay__close');
        this.add_warranty__quantity_input = $('#add_warranty__quantity_input');
        this.add_warranty__submit = $('#add_warranty__submit');
        this.add_warranty_checkbox = $('#add_warranty_checkbox');

        this.warrantyProduct;
        this.remoteItemData = null;

        this.initEvents();
        this.initAddToCartEvent();   


    }

    initEvents() {
        // get current cart
        this.setInitCartItemsData();

        this.cartPopupClose.on('click', ()=>{
            this.cartPopup.fadeOut('fast');
            this.cartPopupTrigger.removeClass('active');
        });

        this.addToCartPopupClose.on('click', ()=>{
            this.addToCartPopup.fadeOut('fast');
            this.addToCartPopupLoader.show();
            this.addToCartPopupSuccess.hide();
        });

        
        this.add_warranty_popup__close.on('click', (e) => {
            this.add_warranty_popup.fadeOut('fast');
        })

        this.add_warranty__submit.on('click', (e) => {
            this.addToCartFromWarranty(e);
        });

        this.add_warranty_checkbox.on('change', (e)=>{
            let isChecked = e.currentTarget.checked;

            if(isChecked){
                $(add_warranty__submit).attr('data-add_warranty', "true");
            } else {
                $(add_warranty__submit).attr('data-add_warranty', "false");
            }
        })
    }


    setInitCartItemsData(){
        if(window.location.pathname.includes('/cart/')){
            this.cart_result__loader.show();
        } 
        $.ajax({
            url : somfyData.ajax_url,
            type : 'POST',
            data : {
                action : 'somfy_pro_get_cart',
                nonce: somfyData.ajax_nonce
            },
            dataType: "json",
            success: (response) =>{

                // set customer remote item data to local storage
                if(response.cust_radiomodel_qualified && response.cust_radiomodel_qualified.status){
                    localStorage.setItem('pro_cust_radio_remote', JSON.stringify(response.remoteitem));
                    this.remoteItemData = response.remoteitem;
                } else {
                    localStorage.removeItem('pro_cust_radio_remote');
                    this.remoteItemData = null;
                }


                if(response.status && !response.user_logged){
                    // user not logged in - get cartItems from localStorage
                    this.cartItems = JSON.parse(localStorage.getItem('pro_cartItems'));
                    this.userIsLoggedIn = false;
                } else if(response.status && response.user_logged){
                    // user is logged in - get cartItems from response
                    /* if user just registered from the checkout page
                    * check localStorage 'checkout_registration' 
                    * and set cartItems from localStorage
                    * and update cartItems in DB
                    */
                    let checkout_registration = localStorage.getItem('checkout_registration');
                    if(checkout_registration == 'true'){
                        this.cartItems = JSON.parse(localStorage.getItem('pro_cartItems'));
                        this.updateCartDB(this.cartItems);
                    } else {
                        this.cartItems = response.cart_items;
                        localStorage.setItem('pro_cartItems', JSON.stringify(this.cartItems));
                    }

                    this.userIsLoggedIn = true;
                } else {
                    // SHOW ERROR
                    console.warn(response);
                }

                this.updateCartPopup();
            },
            error:  (jqXHR, status) => {
                console.log(jqXHR);
                console.log(status);
            }
        });
    }



    updateCartPopup(updateFromCheckout = false){
        if(this.cartItems && this.cartItems.length > 0){
            this.cartPopupTrigger.addClass('with-items');

            // get full products data 
            this.printCartPopupContent();

            this.cartSummeryContainer.show();
            this.noCartItemsMsg.hide();
        } else {
            let pathname = window.location.pathname;
            if(pathname.includes('/checkout/') && !updateFromCheckout) window.location.replace(somfyData.root_url + '/shop');

            this.cartItemsContainer.html('');

            this.cartPopupTrigger.removeClass('with-items');
            this.cartSummeryContainer.hide();
            this.cart_result__loader.hide();
            this.noCartItemsMsg.show();

            if(pathname.includes('/cart/')){
                this.cartitemsTable.hide();
                this.cartPage_summery.css('visibility' , 'hidden');
                this.cartItemsTbody.html('');
            }
        }
    }


    printCartPopupContent(){
        $.ajax({
            url : somfyData.ajax_url,
            type : 'POST',
            data : {
                action : 'somfy_pro_get_cart_product_data',
                cartItems: this.cartItems,
                nonce: somfyData.ajax_nonce
            },
            dataType: "json",
            success: (response) =>{
                // set customer remote item data to local storage
                if(response.cust_radiomodel_qualified && response.cust_radiomodel_qualified.status){
                    localStorage.setItem('pro_cust_radio_remote', JSON.stringify(response.remoteitem));
                    this.remoteItemData = response.remoteitem;
                } else {
                    localStorage.removeItem('pro_cust_radio_remote');
                    this.remoteItemData = null;
                }

                if(response.status && response.productDataResults.length > 0){
                    this.warrantyProduct = response.warrantyProduct;

                    if(window.location.pathname.includes('/cart/')){
                        this.printCartForPage(response);
                    } else {
                        this.printCartForCheckout(response);
                    }
                } else {
                    // SHOW ERROR
                    this.warrantyProduct = null;
                    console.warn(response);
                }
            },
            error:  (jqXHR, status) => {
                console.log(jqXHR);
                console.log(status);
            }
        });
    }

    // cart page
    printCartForPage(response){
        let subSummary = 0, totalDiscount = 0, totalPrice = 0, catlogyPrice = 0;

        let productsTableHtml = '';
        for(let product of response.productDataResults){
            subSummary += parseFloat(product.pricelist.base_price_after_discountcode) * parseInt(product.amount);
            let finalPrice = parseFloat(product.pricelist.base_price_after_discountcode) * parseInt(product.amount);
            let discount_percent = parseFloat(product.pricelist.discount_percent);

            if(product.parentProduct != ''){
                catlogyPrice += parseFloat(product.base_price) * parseInt(product.amount);
            } else {
                catlogyPrice += parseFloat(product.pricelist.base_price) * parseInt(product.amount);
            }

            if(product.on_sell){
                finalPrice = parseFloat(product.discount_price) * parseInt(product.amount);
                discount_percent = product.discount_percentage;
            } else if(product.parentProduct != '' && product.productID == this.warrantyProduct.productID){
                finalPrice = parseFloat(product.base_price) * parseInt(product.amount);
            }

            totalPrice += finalPrice;

            let kidsHtml = '';

            // print warrany kid
            if((product.childeProduct == '') && product.warranty_applicable == true){
                kidsHtml += this.getWarrantyKidHtmlForPage(product.productID, product.amount);
            };
            kidsHtml += this.getCartItemKidsHTMLForPage(product.productID, response.productDataResults, product.amount);


            if(product.parentProduct != '') continue;

            productsTableHtml += `
                <tr class='cartItem__tr cartItem__tr_desktop'>
                    <td>
                        <div class="item_tumbnail">
                            <img src="${product.thumbnail}" alt="${product.title}">
                        </div>
                    </td>
                    <td>
                        <div class="item_titles">
                            <a href="${product.permalink}" alt="${product.itemid}" class="item_itemid">מק"ט: ${product.itemid}</a>
                            <a href="${product.permalink}" alt="${product.title}" class="item_title">${product.title}</a>
                        </div>
                    </td>
                    <td>
                        <div class="item_amount">
                            <label for="item_amount__${product.productID}">כמות:</label>
                            <input type="number" class="item_amount_input form-control form-control-sm" 
                                id="item_amount__${product.productID}"
                                data-product="${product.productID}" 
                                data-parent="${product.parentProduct}" 
                                data-child="${product.childeProduct}" 
                                min="1" max="100000" 
                                value="${product.amount}">
                            <div class="loader" id="item_${product.productID}__loader"><div class="spinner-loader"></div></div>
                        </div>
                    </td>

                    <td>
                        <div class="item_prices">
                            ${product.on_sell ? `
                                <div class='top_price'>
                                    <span class='old_price'>
                                        <span class='currency_symbol'>₪</span>${this.numberWithCommas(product.pricelist.base_price_after_discountcode)}
                                    </span>
                                    <div class="vl"></div>
                                    <span class='discount_price'>
                                        <span class='currency_symbol'>₪</span>${this.numberWithCommas(product.discount_price)}
                                    </span>
                                </div>
                                <div class='bottomline_price'>מחיר ברוטו: <span class='currency_symbol'>₪</span>${this.numberWithCommas(product.pricelist.base_price)}</div>
                                ` : `
                                <div class="top_price">
                                    <span class="base_price">
                                        <span class="currency_symbol">₪</span>${this.numberWithCommas(product.pricelist.base_price_after_discountcode)}
                                    </span>
                                </div>
                                <div class='bottomline_price'>מחיר ברוטו: <span class='currency_symbol'>₪</span>${this.numberWithCommas(product.pricelist.base_price)}</div>
                                `}   
                        </div>
                    </td>
                    
                    <td>
                        <div class="item_prices">
                            ${product.on_sell ? `
                                <div class="top_price">
                                    <span class="base_price">
                                        <span class="currency_symbol">₪</span>${this.numberWithCommas((product.discount_price * product.amount).toFixed(2))}
                                    </span>
                                </div>
                                ` : `
                                <div class="top_price">
                                    <span class="base_price">
                                        <span class="currency_symbol">₪</span>${this.numberWithCommas((product.pricelist.base_price_after_discountcode * product.amount).toFixed(2))}
                                    </span>
                                </div>
                                `}   
                        </div>
                    </td>

                    <td>
                        ${discount_percent != '' ? 
                        `
                            הנחה ${discount_percent}%
                        `:``}
                    </td>

                    <td>
                        <a class="remove_cart_item"  alt='הסר'
                            data-product="${product.productID}"
                            data-child="${product.childeProduct}"
                            data-parent="${product.parentProduct}">הסר</a>
                    </td>
                </tr>

                
                <tr class='cartItem__tr cartItem__tr_mobile'>
                    <td>
                        <div class="item_tumbnail">
                            <img src="${product.thumbnail}" alt="${product.title}">
                        </div>
                    </td>
                    <td>
                        <div class="item_titles">
                            <a href="${product.permalink}" alt="${product.itemid}" class="item_itemid">מק"ט: ${product.itemid}</a>
                            <a href="${product.permalink}" alt="${product.title}" class="item_title">${product.title}</a>
                        </div>
                    </td>
                </tr>
                <tr class='cartItem__tr cartItem__tr_mobile'>
                    <td colspan='2'>
                        <div class="item_actions_td">

                            <div class="item_amount">
                                <label for="item_amount__${product.productID}">כמות:</label>
                                <input type="number" class="item_amount_input form-control form-control-sm" 
                                    id="item_amount__${product.productID}"
                                    data-product="${product.productID}" 
                                    data-parent="${product.parentProduct}" 
                                    data-child="${product.childeProduct}" 
                                    min="1" max="100000" 
                                    value="${product.amount}">
                                <div class="loader" id="item_${product.productID}__loader"><div class="spinner-loader"></div></div>
                            </div>
                            ${discount_percent != '' ? 
                            `
                                הנחה ${discount_percent}%
                            `:``}
                            <a class="remove_cart_item"  alt='הסר'
                                data-product="${product.productID}"
                                data-child="${product.childeProduct}"
                                data-parent="${product.parentProduct}">הסר</a>
                        </div>
                    </td>
                </tr>
                <tr class='cartItem__tr cartItem__tr_mobile' style='border-bottom: 1px solid;'>
                    <td colspan='2'>
                        <div class='item_prices_td'>
                            <div class="item_prices">
                                <h5>מחיר ליחידה</h5>
                                ${product.on_sell ? `
                                    <div class='top_price'>
                                        <span class='old_price'>
                                            <span class='currency_symbol'>₪</span>${this.numberWithCommas(product.pricelist.base_price_after_discountcode)}
                                        </span>
                                        <div class="vl"></div>
                                        <span class='discount_price'>
                                            <span class='currency_symbol'>₪</span>${this.numberWithCommas(product.discount_price)}
                                        </span>
                                    </div>
                                    <div class='bottomline_price'>מחיר ברוטו: <span class='currency_symbol'>₪</span>${this.numberWithCommas(product.pricelist.base_price)}</div>
                                    ` : `
                                    <div class="top_price">
                                        <span class="base_price">
                                            <span class="currency_symbol">₪</span>${this.numberWithCommas(product.pricelist.base_price_after_discountcode)}
                                        </span>
                                    </div>
                                    <div class='bottomline_price'>מחיר ברוטו: <span class='currency_symbol'>₪</span>${this.numberWithCommas(product.pricelist.base_price)}</div>
                                    `}   
                            </div>
                            <div class="item_prices">
                                <h5>סה"כ מחיר</h5>
                                ${product.on_sell ? `
                                    <div class="top_price">
                                        <span class="base_price">
                                            <span class="currency_symbol">₪</span>${this.numberWithCommas((product.discount_price * product.amount).toFixed(2))}
                                        </span>
                                    </div>
                                    ` : `
                                    <div class="top_price">
                                        <span class="base_price">
                                            <span class="currency_symbol">₪</span>${this.numberWithCommas((product.pricelist.base_price_after_discountcode * product.amount).toFixed(2))}
                                        </span>
                                    </div>
                                    `}   
                            </div>
                        </div>
                    </td>
                </tr>
                

                ${kidsHtml}
            `;
        }

        this.order_subSummary = subSummary.toFixed(2);
        this.order_totalDiscount = (100*(catlogyPrice - subSummary)/catlogyPrice).toFixed(0);
        this.order_totalPrice = totalPrice.toFixed(2);
        
        this.subSummeryVal.html(this.numberWithCommas(this.order_subSummary));
        $('.total_discount_summery').html(`<span>הנחה</span><span>${this.order_totalDiscount}%</span>`);
        this.totalSummeryVal.html(this.numberWithCommas(this.order_totalPrice));
        this.catalogySummeryVal.html(this.numberWithCommas(catlogyPrice.toFixed(2)));

        // this.cartItemsContainer.html(productsHtml);
        this.cartItemsTbody.html(productsTableHtml);
        this.cartPage_summery.css('visibility', 'visible');
        this.cartitemsTable.show();
        this.cart_result__loader.hide();


        this.initRemoveItemEvent();
        this.initWarrantyCheckbox();
        this.initAmountInputEvent();
    }


    getWarrantyKidHtmlForPage(productID, amount){
        let product = this.warrantyProduct;
        let html = `
            <tr class='cartItem__tr childe_tr cartItem__tr_desktop'>
                <td></td>
                <td class='warrantyProduct_td'>
                    <div class="item_titles">
                        <a alt="${product.title}" class="item_title">${product.title}</a>
                    </div>
                </td>
                <td>
                    <div class="item_amount">
                        <label for="item_amount__${product.productID}">כמות:</label>
                        <input type="number" class="item_amount_input form-control form-control-sm" 
                            id="item_amount__${product.productID}"
                            data-product="${product.productID}" 
                            data-parent="${productID}" 
                            data-child="" 
                            data-new="true"
                            min="1" max="${amount}" 
                            value="0">
                        <div class="loader" id="item_${product.productID}__loader"><div class="spinner-loader"></div></div>
                    </div>
                </td>

                <td>
                    <div class="item_prices">
                        <div class="top_price">
                            <span class="base_price">
                                <span class="currency_symbol">₪</span>${this.numberWithCommas((product.base_price * 1.00).toFixed(2))}
                            </span>
                        </div>
                    </div>
                </td>
                
                <td>
                    <div class="item_prices">
                        <div class="top_price">
                            <span class="base_price">
                                <span class="currency_symbol">₪</span>0
                            </span>
                        </div>
                    </div>
                </td>
                <td></td>

                <td>
                    <a class="remove_cart_item"  alt='הסר'
                        data-product="${product.productID}"
                        data-child=""
                        data-parent="${productID}">הסר</a>
                </td>
            </tr>
            
            <tr class='cartItem__tr cartItem__tr_mobile childe_tr'>
                <td class='warrantyProduct_td' colspan='2'>
                    <div class="item_titles">
                        <a alt="${product.title}" class="item_title">${product.title}</a>
                    </div>
                </td>
            </tr>
            
            <tr class='cartItem__tr cartItem__tr_mobile childe_tr'>
                <td colspan='2'>
                    <div class="item_actions_td">
                        <div class="item_amount">
                            <label for="item_amount__${product.productID}">כמות:</label>
                            <input type="number" class="item_amount_input form-control form-control-sm" 
                                id="item_amount__${product.productID}"
                                data-product="${product.productID}" 
                                data-parent="${productID}" 
                                data-child="" 
                                data-new="true"
                                min="1" max="${amount}" 
                                value="0">
                            <div class="loader" id="item_${product.productID}__loader"><div class="spinner-loader"></div></div>
                        </div>
                        <a class="remove_cart_item"  alt='הסר'
                            data-product="${product.productID}"
                            data-child=""
                            data-parent="${productID}">הסר</a>
                    </div>
                </td>
            </tr>
            <tr class='cartItem__tr cartItem__tr_mobile childe_tr' style='border-bottom: 1px solid;'>
                <td colspan='2'>
                    <div class="item_prices_td">

                        <div class="item_prices">
                            <h5>מחיר ליחידה</h5>
                            <div class="top_price">
                                <span class="base_price">
                                    <span class="currency_symbol">₪</span>${this.numberWithCommas((product.base_price * 1.00).toFixed(2))}
                                </span>
                            </div>
                        </div>
                        <div class="item_prices">
                            <h5>סה"כ מחיר</h5>
                            <div class="top_price">
                                <span class="base_price">
                                    <span class="currency_symbol">₪</span>0
                                </span>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
            `;

        return html;
    }

    getCartItemKidsHTMLForPage(parentProduct, productsArr, maxAmount){
        let kidsHtml = '';
        for(let product of productsArr){
            if(product.parentProduct != parentProduct) continue;

            if(product.productID == this.warrantyProduct.productID){
                kidsHtml += `
                    <tr class='cartItem__tr childe_tr cartItem__tr_desktop'>
                        <td></td>
                        <td class='warrantyProduct_td'>
                            <div class="item_titles">
                                <a href="${product.permalink}" alt="${product.title}" class="item_title">${product.title}</a>
                            </div>
                        </td>
                        <td>
                            <div class="item_amount">
                                <label for="item_amount__${product.productID}">כמות:</label>
                                <input type="number" class="item_amount_input form-control form-control-sm" 
                                    id="item_amount__${product.productID}"
                                    data-product="${product.productID}" 
                                    data-parent="${product.parentProduct}" 
                                    data-child="${product.childeProduct}" 
                                    min="1" max="${maxAmount}" 
                                    value="${product.amount}">
                                <div class="loader" id="item_${product.productID}__loader"><div class="spinner-loader"></div></div>
                            </div>
                        </td>

                        <td>
                            <div class="item_prices">
                                <div class="top_price">
                                    <span class="base_price">
                                        <span class="currency_symbol">₪</span>${this.numberWithCommas((product.base_price * 1.00).toFixed(2))}
                                    </span>
                                </div>
                            </div>
                        </td>
                        
                        <td>
                            <div class="item_prices">
                                <div class="top_price">
                                    <span class="base_price">
                                        <span class="currency_symbol">₪</span>${this.numberWithCommas((product.base_price * product.amount).toFixed(2))}
                                    </span>
                                </div>
                            </div>
                        </td>
                        <td></td>

                        <td>
                            <a class="remove_cart_item"  alt='הסר'
                                data-product="${product.productID}"
                                data-child="${product.childeProduct}"
                                data-parent="${product.parentProduct}">הסר</a>
                        </td>
                    </tr>

                    <tr class='cartItem__tr cartItem__tr_mobile'>
                        <td colspan='2'>
                            <div class="item_titles">
                                <a href="${product.permalink}" alt="${product.title}" class="item_title">${product.title}</a>
                            </div>
                        </td>
                    </tr>
                    
                    <tr class='cartItem__tr cartItem__tr_mobile'>
                        <td colspan='2'>
                            <div class="item_actions_td">
                                <div class="item_amount">
                                    <label for="item_amount__${product.productID}">כמות:</label>
                                    <input type="number" class="item_amount_input form-control form-control-sm" 
                                        id="item_amount__${product.productID}"
                                        data-product="${product.productID}" 
                                        data-parent="${product.parentProduct}" 
                                        data-child="${product.childeProduct}" 
                                        min="1" max="${maxAmount}" 
                                        value="${product.amount}">
                                    <div class="loader" id="item_${product.productID}__loader"><div class="spinner-loader"></div></div>
                                </div>
                                <a class="remove_cart_item"  alt='הסר'
                                    data-product="${product.productID}"
                                    data-child="${product.childeProduct}"
                                    data-parent="${product.parentProduct}">הסר</a>
                            </div>
                        </td>
                    </tr>
                    <tr class='cartItem__tr cartItem__tr_mobile' style='border-bottom: 1px solid;'>
                        <td colspan='2'>
                            <div class="item_prices_td">
                                <div class="item_prices">
                                    <h5>מחיר ליחידה</h5>
                                    <div class="top_price">
                                        <span class="base_price">
                                            <span class="currency_symbol">₪</span>${this.numberWithCommas((product.base_price * 1.00).toFixed(2))}
                                        </span>
                                    </div>
                                </div>
                                <div class="item_prices">
                                    <h5>סה"כ מחיר</h5>
                                    <div class="top_price">
                                        <span class="base_price">
                                            <span class="currency_symbol">₪</span>${this.numberWithCommas((product.base_price * product.amount).toFixed(2))}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                `;
            } else {
                let finalPrice = parseFloat(product.pricelist.base_price_after_discountcode) * parseInt(product.amount);
                let discount_percent = parseFloat(product.pricelist.discount_percent);


                if(product.on_sell){
                    totalDiscount += (product.pricelist.base_price_after_discountcode - product.discount_price) * parseInt(product.amount);
                    finalPrice = parseFloat(product.discount_price) * parseInt(product.amount);
                    discount_percent = product.discount_percentage;
                } else if(product.parentProduct != ''){
                    finalPrice = parseFloat(product.base_price) * parseInt(product.amount);
                }

                kidsHtml += `
                    <tr class='cartItem__tr cartItem__tr_desktop'>
                        <td>
                            <div class="item_tumbnail">
                                <img src="${product.thumbnail}" alt="${product.title}">
                            </div>
                        </td>
                        <td>
                            <div class="item_titles">
                                <a href="${product.permalink}" alt="${product.itemid}" class="item_itemid">מק"ט: ${product.itemid}</a>
                                <a href="${product.permalink}" alt="${product.title}" class="item_title">${product.title}</a>
                            </div>
                        </td>
                        <td>
                            <div class="item_amount">
                                <label for="item_amount__${product.productID}">כמות:</label>
                                <input type="number" class="item_amount_input form-control form-control-sm" 
                                    id="item_amount__${product.productID}"
                                    data-product="${product.productID}" 
                                    data-parent="${product.parentProduct}" 
                                    data-child="${product.childeProduct}" 
                                    min="1" max="100000" 
                                    value="${product.amount}">
                                <div class="loader" id="item_${product.productID}__loader"><div class="spinner-loader"></div></div>
                            </div>
                        </td>

                        <td>
                            <div class="item_prices">
                                ${product.on_sell ? `
                                    <div class='top_price'>
                                        <span class='old_price'>
                                            <span class='currency_symbol'>₪</span>${this.numberWithCommas(product.pricelist.base_price_after_discountcode)}
                                        </span>
                                        <div class="vl"></div>
                                        <span class='discount_price'>
                                            <span class='currency_symbol'>₪</span>${this.numberWithCommas(product.discount_price)}
                                        </span>
                                    </div>
                                    <div class='bottomline_price'>מחיר ברוטו: <span class='currency_symbol'>₪</span>${this.numberWithCommas(product.pricelist.base_price)}</div>
                                    ` : `
                                    <div class="top_price">
                                        <span class="base_price">
                                            <span class="currency_symbol">₪</span>${this.numberWithCommas(product.pricelist.base_price_after_discountcode)}
                                        </span>
                                    </div>
                                    <div class='bottomline_price'>מחיר ברוטו: <span class='currency_symbol'>₪</span>${this.numberWithCommas(product.pricelist.base_price)}</div>
                                    `}   
                            </div>
                        </td>
                        
                        <td>
                            <div class="item_prices">
                                ${product.on_sell ? `
                                    <div class="top_price">
                                        <span class="base_price">
                                            <span class="currency_symbol">₪</span>${this.numberWithCommas((product.discount_price * product.amount).toFixed(2))}
                                        </span>
                                    </div>
                                    ` : `
                                    <div class="top_price">
                                        <span class="base_price">
                                            <span class="currency_symbol">₪</span>${this.numberWithCommas((product.pricelist.base_price_after_discountcode * product.amount).toFixed(2))}
                                        </span>
                                    </div>
                                    `}   
                            </div>
                        </td>

                        <td>
                            ${discount_percent != '' ? 
                            `
                                הנחה ${discount_percent}%
                            `:``}
                        </td>

                        <td>
                            <a class="remove_cart_item"  alt='הסר'
                                data-product="${product.productID}"
                                data-child="${product.childeProduct}"
                                data-parent="${product.parentProduct}">הסר</a>
                        </td>
                    </tr>

                    <tr class='cartItem__tr cartItem__tr_mobile'>
                        <td>
                            <div class="item_tumbnail">
                                <img src="${product.thumbnail}" alt="${product.title}">
                            </div>
                        </td>
                        <td>
                            <div class="item_titles">
                                <a href="${product.permalink}" alt="${product.itemid}" class="item_itemid">מק"ט: ${product.itemid}</a>
                                <a href="${product.permalink}" alt="${product.title}" class="item_title">${product.title}</a>
                            </div>
                        </td>
                    </tr>
                    
                    <tr class='cartItem__tr cartItem__tr_mobile'>
                        <td colspan='2'>
                            <div class="item_actions_td">
                                <div class="item_amount">
                                    <label for="item_amount__${product.productID}">כמות:</label>
                                    <input type="number" class="item_amount_input form-control form-control-sm" 
                                        id="item_amount__${product.productID}"
                                        data-product="${product.productID}" 
                                        data-parent="${product.parentProduct}" 
                                        data-child="${product.childeProduct}" 
                                        min="1" max="100000" 
                                        value="${product.amount}">
                                    <div class="loader" id="item_${product.productID}__loader"><div class="spinner-loader"></div></div>
                                </div>
                                <a class="remove_cart_item"  alt='הסר'
                                    data-product="${product.productID}"
                                    data-child="${product.childeProduct}"
                                    data-parent="${product.parentProduct}">הסר</a>
                            </div>
                        </td>
                    </tr>
                    <tr class='cartItem__tr cartItem__tr_mobile' style='border-bottom: 1px solid;'>
                        <td colspan='2'>
                            <div class="item_prices_td">
                                <div class="item_prices">
                                    <h5>מחיר ליחידה</h5>
                                    ${product.on_sell ? `
                                        <div class='top_price'>
                                            <span class='old_price'>
                                                <span class='currency_symbol'>₪</span>${this.numberWithCommas(product.pricelist.base_price_after_discountcode)}
                                            </span>
                                            <div class="vl"></div>
                                            <span class='discount_price'>
                                                <span class='currency_symbol'>₪</span>${this.numberWithCommas(product.discount_price)}
                                            </span>
                                        </div>
                                        <div class='bottomline_price'>מחיר ברוטו: <span class='currency_symbol'>₪</span>${this.numberWithCommas(product.pricelist.base_price)}</div>
                                        ` : `
                                        <div class="top_price">
                                            <span class="base_price">
                                                <span class="currency_symbol">₪</span>${this.numberWithCommas(product.pricelist.base_price_after_discountcode)}
                                            </span>
                                        </div>
                                        <div class='bottomline_price'>מחיר ברוטו: <span class='currency_symbol'>₪</span>${this.numberWithCommas(product.pricelist.base_price)}</div>
                                        `}   
                                </div>
                                <div class="item_prices">
                                    <h5>סה"כ מחיר</h5>
                                    ${product.on_sell ? `
                                        <div class="top_price">
                                            <span class="base_price">
                                                <span class="currency_symbol">₪</span>${this.numberWithCommas((product.discount_price * product.amount).toFixed(2))}
                                            </span>
                                        </div>
                                        ` : `
                                        <div class="top_price">
                                            <span class="base_price">
                                                <span class="currency_symbol">₪</span>${this.numberWithCommas((product.pricelist.base_price_after_discountcode * product.amount).toFixed(2))}
                                            </span>
                                        </div>
                                        `}   
                                </div>
                            </div>
                        </td>
                    </tr>
                `;
            }
            
        }
        return kidsHtml;
    }


    // checkout page
    printCartForCheckout(response){
        let subSummary = 0, totalDiscount = 0, totalPrice = 0, catlogyPrice = 0;
        let productsHtml = '';
        
        for(let product of response.productDataResults){
            subSummary += parseFloat(product.pricelist.base_price_after_discountcode) * parseInt(product.amount);
            let finalPrice = parseFloat(product.pricelist.base_price_after_discountcode) * parseInt(product.amount);
            
            if(product.parentProduct != ''){
                catlogyPrice += parseFloat(product.base_price) * parseInt(product.amount);
            } else {
                catlogyPrice += parseFloat(product.pricelist.base_price) * parseInt(product.amount);
            }

            if(product.on_sell){
                totalDiscount += (product.pricelist.base_price_after_discountcode - product.discount_price) * parseInt(product.amount);
                finalPrice = parseFloat(product.discount_price) * parseInt(product.amount);
            } else if(product.parentProduct != '' && product.productID == this.warrantyProduct.productID){
                finalPrice = parseFloat(product.base_price) * parseInt(product.amount);
            }

            totalPrice += finalPrice;

            let kidsHtml = '';
            
            // print warrany kid
            // if(product.childeProduct == '' && product.warranty_applicable == true){
            //     kidsHtml += this.getWarrantyKidHtmlForCheckout(product.productID, product.amount);
            // };
            kidsHtml += this.getCartItemKidsHTMLForCheckout(product.productID, response.productDataResults, product.amount);

            if(product.parentProduct != '') continue;

            productsHtml += `<div class="cart_item_box" data-product="${product.productID}">
                <div class="parent_cart_item_box">
                    
                    <div class="item_info">
                        <div class="item_titles">
                            <a href="${product.permalink}" alt="${product.itemid}" class="item_itemid">מק"ט: ${product.itemid}</a>
                            <a href="${product.permalink}" alt="${product.title}" class="item_title">${product.title}</a>
                        </div>
                        
                        <div class="item_amount">
                            <label for="item_amount__${product.productID}">כמות:</label>
                            <input type="number" class="item_amount_input form-control form-control-sm" 
                                id="item_amount__${product.productID}"
                                data-product="${product.productID}" 
                                data-parent="${product.parentProduct}" 
                                data-child="${product.childeProduct}" 
                                min="1"
                                value="${product.amount}">
                            <div class="loader" id="item_${product.productID}__loader"><div class="spinner-loader"></div></div>
                        </div>
                        <a class="remove_cart_item"  alt='הסר'
                            data-product="${product.productID}"
                            data-child="${product.childeProduct}"
                            data-parent="${product.parentProduct}">הסר</a>
                        <div class="item_prices">
                            ${product.on_sell ? `
                                <div class="top_price">
                                    <span class="base_price">
                                        <span class="currency_symbol">₪</span>${this.numberWithCommas((product.discount_price * product.amount).toFixed(2))}
                                    </span>
                                </div>
                                ` : `
                                <div class="top_price">
                                    <span class="base_price">
                                        <span class="currency_symbol">₪</span>${this.numberWithCommas((product.pricelist.base_price_after_discountcode * product.amount).toFixed(2))}
                                    </span>
                                </div>
                                `}   
                        </div>
                        
                    </div>
                </div>
                ${kidsHtml}
            </div>`;
        }

        this.order_subSummary = subSummary.toFixed(2);
        // this.order_totalDiscount = totalDiscount.toFixed(2);
        this.order_totalPrice = totalPrice.toFixed(2);
        this.order_totalDiscount = (catlogyPrice - subSummary).toFixed(2);

        this.subSummeryVal.html(this.numberWithCommas(this.order_subSummary));
        this.discountSummeryVal.html(this.numberWithCommas(this.order_totalDiscount) + '-');
        this.totalSummeryVal.html(this.numberWithCommas(this.order_totalPrice));
        this.catalogySummeryVal.html(this.numberWithCommas(catlogyPrice.toFixed(2)));

        this.cartItemsContainer.html(productsHtml);
        
        this.initRemoveItemEvent();
        this.initWarrantyCheckbox();
        this.initAmountInputEvent();
    }

    getCartItemKidsHTMLForCheckout(parentProduct, productsArr, maxAmount){
        let kidsHtml = '';
        for(let product of productsArr){
            if(product.parentProduct != parentProduct){
                continue;
            }

            if(product.productID == this.warrantyProduct.productID){
                kidsHtml += `<div class="kid_cart_item_box warranty_kid" data-product="${product.productID}" data-parent="${product.parentProduct}">
                    <div class="item_info">
                        <div class="item_titles">
                            <a class='warranty_checkbox_group item_title'>
                                <input type='checkbox' checked
                                    class='warranty_checkbox' 
                                    data-product='${product.productID}'
                                    data-max='${maxAmount}'
                                    data-parent='${parentProduct}'
                                    id='warranty_${product.productID}'>
                                <label class='select_filter_btn' for='warranty_${product.productID}'>${product.title}</label>
                            </a>
                        </div>
                        
                        <div class="item_amount">
                            <label for="item_amount__${product.productID}">כמות:</label>
                            <input type="number" class="item_amount_input form-control form-control-sm" 
                                id="item_amount__${product.productID}"
                                data-product="${product.productID}" 
                                data-parent="${product.parentProduct}" 
                                data-child="${product.childeProduct}" 
                                min="1" max="${maxAmount}" 
                                value="${product.amount}">
                            <div class="loader" id="item_${product.productID}__loader"><div class="spinner-loader"></div></div>
                        </div>
                        <div class="item_prices">
                            <div class="top_price">
                                <span class="base_price">
                                    <span class="currency_symbol">₪</span>${this.numberWithCommas((product.base_price * product.amount).toFixed(2))}
                                </span>
                            </div>
                        </div>
                        <a class="remove_cart_item"  alt='הסר'
                            data-product="${product.productID}"
                            data-child="${product.childeProduct}"
                            data-parent="${product.parentProduct}">הסר</a>
                    </div>
                </div>`;
            } else {
                kidsHtml += `<div class="cart_item_box kid_item" data-product="${product.productID}">
                    <div class="parent_cart_item_box">
                        ${this.remoteItemData ? `<div class='discount_tag'>
                                הנחה<br>
                                <span class='discount_tag_num'>${product.pricelist.discount_percent}%</span>
                            </div>` : ''}
                        
                        <div class="item_info">
                            <div class="item_titles">
                                <a href="${product.permalink}" alt="${product.itemid}" class="item_itemid">מק"ט: ${product.itemid}</a>
                                <a href="${product.permalink}" alt="${product.title}" class="item_title">${product.title}</a>
                            </div>
                            
                            <div class="item_amount">
                                <label for="item_amount__${product.productID}">כמות:</label>
                                <input type="number" class="item_amount_input form-control form-control-sm" 
                                    id="item_amount__${product.productID}"
                                    data-product="${product.productID}" 
                                    data-parent="${product.parentProduct}" 
                                    data-child="${product.childeProduct}" 
                                    min="1"
                                    value="${product.amount}">
                                <div class="loader" id="item_${product.productID}__loader"><div class="spinner-loader"></div></div>
                            </div>
                            <a class="remove_cart_item"  alt='הסר'
                                data-product="${product.productID}"
                                data-child="${product.childeProduct}"
                                data-parent="${product.parentProduct}">הסר</a>
                            <div class="item_prices">
                                ${product.on_sell ? `
                                    <div class="top_price">
                                        <span class="base_price">
                                            <span class="currency_symbol">₪</span>${this.numberWithCommas((product.discount_price * product.amount).toFixed(2))}
                                        </span>
                                    </div>
                                    ` : `
                                    <div class="top_price">
                                        <span class="base_price">
                                            <span class="currency_symbol">₪</span>${this.numberWithCommas((product.pricelist.base_price_after_discountcode * product.amount).toFixed(2))}
                                        </span>
                                    </div>
                                    `}   
                            </div>
                            ${this.remoteItemData ? `<img src="${somfyData.template_url}/assets/img/gift-box.svg" alt="gift" loading="lazy" class="gift__img">` : ''}
                        </div>
                    </div>
                </div>`;
            }
        }
        return kidsHtml;
    }

    getWarrantyKidHtmlForCheckout(productID, amount){
        let product = this.warrantyProduct;
        let html = `<div class="kid_cart_item_box warranty_kid" data-product="${product.productID}" data-parent="${productID}">
                <div class="item_info">
                    <div class="item_titles">
                        <a class='warranty_checkbox_group item_title'>
                            <input type='checkbox'
                                class='warranty_checkbox' 
                                data-product='${product.productID}'
                                data-max='${amount}'
                                data-parent='${productID}'
                                id='warranty_${product.productID}'>
                            <label class='select_filter_btn' for='warranty_${product.productID}'>${product.title}</label>
                        </a>
                    </div>
                    
                    <div class="item_amount">
                        <label for="item_amount__${product.productID}">כמות:</label>
                        <input type="number" class="item_amount_input form-control form-control-sm" 
                            id="item_amount__${product.productID}"
                            data-product="${product.productID}" 
                            data-parent="${productID}" 
                            data-child="" 
                            data-new="true"
                            min="1" max="${amount}" 
                            value="0">
                        <div class="loader" id="item_${product.productID}__loader"><div class="spinner-loader"></div></div>
                    </div>
                    <div class="item_prices">
                        <div class="top_price">
                            <span class="base_price">
                                <span class="currency_symbol">₪</span>${this.numberWithCommas((product.base_price * 1.00).toFixed(2))}
                            </span>
                        </div>
                    </div>
                    <a class="remove_cart_item"  alt='הסר'
                        data-product="${product.productID}"
                        data-child=""
                        data-parent="${productID}">הסר</a>
                </div>
            </div>`;

        return html;
    }


    // Init inputs & triggers events
    initAddToCartEvent(){
        let addToCartCTA = $('.add_to_cart__cta');

        addToCartCTA.off('click').on('click', (e)=>{
            let productID = e.currentTarget.dataset.product;
            let amount = null;

            // check if warranty can be added
            let shop_warranty_product_id = somfyData.shop_warranty_product_id;
            let add_warranty = e.currentTarget.dataset.add_warranty;
            let warranty_applicable = e.currentTarget.dataset.warranty_applicable;
            let radiodiscount = e.currentTarget.dataset.radiodiscount;

            // set amount
            let ppAmount = $('#product_preview_popup').find('.item_amount_input');
            let pageAmount = $('#stock_quantity_input[data-product="'+productID+'"]');

            if(ppAmount.length > 0){ // coming from product preview
                amount = $(ppAmount).val();
            } else if(pageAmount.length > 0){ // coming from product preview
                amount = $(pageAmount).val();
            } else {
                amount = 1;
            }

            // check if calc item
            let isCalcItem = e.currentTarget.dataset.calcitem == 'true';
            if(isCalcItem){
                this.updateCalcItem(productID);
            };

            
            let showWarrantyPopup = false;
            // check if on shop page
            var preview_popup = document.getElementById("product_preview_popup");
            if(preview_popup){
                if(!(window.getComputedStyle(preview_popup).display === "none")) showWarrantyPopup = true;
            }


            if((warranty_applicable == 'true' || warranty_applicable == '1') && add_warranty == 'true'){ // coming from warranty popup
                // add product and warranty to cart
                if(ppAmount.length > 0) $('#product_preview_popup').hide();

                let showSuccessPopup = true;
                if(radiodiscount == 'true'){
                    showSuccessPopup = false;
                }

                this.addToCart(shop_warranty_product_id, amount, productID, "", showSuccessPopup);
                this.addToCart(productID, amount, "", shop_warranty_product_id), showSuccessPopup;
        
                
                // show popup
                if(radiodiscount == 'true') {
                    this.addToCartPopup.hide();
                    this.remotePopup.showRemotePopup(productID, amount);
                } else {
                    this.addToCartPopup.fadeIn('fast');
                }

                // update cart items popup
            } else if((warranty_applicable == 'true' || warranty_applicable == '1') && add_warranty == 'false' && showWarrantyPopup){ // show the warranty popup
                // show add_warrany popup
                this.show_addWarrantyPopup(productID, amount, radiodiscount);
            } else { // just add the product

                if(ppAmount.length > 0) $('#product_preview_popup').hide();
                

                let showSuccessPopup = true;
                if(radiodiscount == 'true'){
                    showSuccessPopup = false;
                }

                this.addToCart(productID, amount, "", "", showSuccessPopup);
                
                // show popup
                if(radiodiscount == 'true') {
                    this.addToCartPopup.hide();
                    this.remotePopup.showRemotePopup(productID, amount);
                } else {
                    this.addToCartPopup.fadeIn('fast');
                }

            }
            this.updateCartDB(this.cartItems);
            this.updateCartPopup();
        });
    }


    // clac items
    updateCalcItem(productID){
        let curCalcItems = JSON.parse(localStorage.getItem('pro_cart_calcItems'));
        let calc_userInput = JSON.parse(localStorage.getItem('pro_calc_userInputs'));

        if(!curCalcItems || curCalcItems.length == 0){
            curCalcItems = [{'productID':productID, 'calc_userInput': calc_userInput}];
        } else {
            let itemFound = false;

            for(let item of curCalcItems){
                if(item.productID == productID) {
                    item.calc_userInput = calc_userInput;
                    itemFound = true;
                }
            }
            if(!itemFound) curCalcItems.push({'productID':productID, 'calc_userInput': calc_userInput});
        }
        localStorage.setItem('pro_cart_calcItems', JSON.stringify(curCalcItems));
    }

    removeFromCalcItems(productID){
        let curCalcItems = JSON.parse(localStorage.getItem('pro_cart_calcItems'));

        if(curCalcItems && curCalcItems.length > 0){
            let newCalcItems = curCalcItems.filter((item) =>{
                return item.productID != productID;
            });
            localStorage.setItem('pro_cart_calcItems', JSON.stringify(newCalcItems));
        }
    }

    initRemoveItemEvent(){
        let removeCartItemTrigger = $('.remove_cart_item');

        removeCartItemTrigger.on('click', (e) => {
            let productID = e.currentTarget.dataset.product;
            let parentProduct = e.currentTarget.dataset.parent;
            let childeProduct = e.currentTarget.dataset.child;

            let itemLoader = $('#item_' + productID + '__loader');
            itemLoader.fadeIn('fast');

            this.removeCartItem(productID, parentProduct, childeProduct);
            this.removeFromCalcItems(productID);
            // update cart items popup
            this.updateCartPopup();
        });
    }

    initWarrantyCheckbox(){
        let warrantyCB = $('.warranty_checkbox');

        warrantyCB.off('change').on('change', (e)=> {

            let productID = e.currentTarget.dataset.product;
            let parentProduct = e.currentTarget.dataset.parent;
            let max = e.currentTarget.dataset.max;
            let checked = e.currentTarget.checked;

            if(checked){
                // add warranty
                // update cart items popup
                this.addToCart(productID, 1, parentProduct, "");
                this.updateCartItem(parentProduct, max, '', productID, '');
            } else {
                // remove warranty
                this.removeCartItem(productID, parentProduct, "");
            }
            if(this.userIsLoggedIn) this.updateCartDB(this.cartItems);
            this.updateCartPopup();
        })
    }


    initAmountInputEvent(){
        let itemsAmountInput = $('.item_amount_input');
        let shop_warranty_product_id = somfyData.shop_warranty_product_id;

        itemsAmountInput.on('focusin', (e)=>{
            this.oldAmountInputVal = $(e.currentTarget).val();
        });
        itemsAmountInput.on('focusout', (e)=>{
            this.oldAmountInputVal = null;
        });

        itemsAmountInput.on('change', (e) => {
            let productID = e.currentTarget.dataset.product;
            let parentProduct = e.currentTarget.dataset.parent;
            let childeProduct = e.currentTarget.dataset.child;
            let newAmount = $(e.currentTarget).val();
            let max = e.currentTarget.max;
            let isNew = e.currentTarget.dataset.new;

            if(parseInt(newAmount) > parseInt(max)){
                newAmount = max;
                $(e.currentTarget).val(newAmount);
            } 

            let itemLoader = $('#item_' + productID + '__loader');
            itemLoader.fadeIn('fast');
            $(e.currentTarget).prop('disabled', true);

            if(isNew == 'true'){
                this.addToCart(shop_warranty_product_id, newAmount, parentProduct, "");

                this.updateCartItem(parentProduct, max, '', shop_warranty_product_id, e.currentTarget);
            } else {
                this.updateCartItem(productID, newAmount, parentProduct, childeProduct, e.currentTarget);
            }
            // update cart items popup
            if(this.userIsLoggedIn) this.updateCartDB(this.cartItems);
            this.updateCartPopup();
        });
    }

    // Add to cart
    addToCartFromWarranty(e){
        let productID = e.currentTarget.dataset.product;
        let amount = e.currentTarget.dataset.amount;

        // check if warranty can be added
        let shop_warranty_product_id = somfyData.shop_warranty_product_id;
        let add_warranty = e.currentTarget.dataset.add_warranty;
        let warranty_applicable = e.currentTarget.dataset.warranty_applicable;
        let radiodiscount = e.currentTarget.dataset.radiodiscount;
         
        this.add_warranty_popup.hide();

        if(warranty_applicable && add_warranty == 'true'){
            // add product and warranty to cart
            let showSuccessPopup = true;
            if(radiodiscount == 'true'){
                showSuccessPopup = false;
            }

            this.addToCart(shop_warranty_product_id, amount, productID, "", showSuccessPopup);
            this.addToCart(productID, amount, "", shop_warranty_product_id, showSuccessPopup);
            if(radiodiscount == 'true') this.remotePopup.showRemotePopup(productID, amount);
        } else {
            // just add the product
            let showSuccessPopup = true;
            if(radiodiscount == 'true'){
                showSuccessPopup = false;
            }
            this.addToCart(productID, amount, "", "", showSuccessPopup);
            if(radiodiscount == 'true') this.remotePopup.showRemotePopup(productID, amount);
        }

        if(this.userIsLoggedIn) this.updateCartDB(this.cartItems);

        // update cart items popup
        this.updateCartPopup();
        if(radiodiscount == 'false'){
            this.addToCartPopup.fadeIn('fast');  
        } else {
            this.addToCartPopup.hide();
        }
    }


    addToCart(productID, amount, parentProduct, childeProduct, showSuccessPopup = true) {
        let cartItemData = {
            productID: productID,
            amount: parseInt(amount),
            parentProduct: parentProduct,
            childeProduct : childeProduct,
            status: true
        };

        // get current cart items from localStorage
        let cartItems = localStorage.getItem('pro_cartItems');
        cartItems = JSON.parse(cartItems);

        if(!cartItems){
            // new cart - just add the items
            cartItems = [cartItemData];
        } else if(cartItems && cartItems.length > 0){
            // not new cart - check if product allready exists
            let itemFound = false;

            cartItems.map(item => {
                if(item.productID == cartItemData.productID 
                    && item.parentProduct == cartItemData.parentProduct){

                    let newAmount = parseInt(item.amount) + parseInt(cartItemData.amount);
                    
                    // disrigard warrany
                    if(newAmount > 1 || productID == somfyData.shop_warranty_product_id){
                        item.amount = newAmount;
                        if(childeProduct != ''){
                            item.childeProduct = childeProduct;
                        }
                        itemFound = true;
                        return item;
                    } 
                }
            });

            
            if(!itemFound){
                cartItems.push(cartItemData);
            }
        } else {
            // empty cart
            cartItems = [cartItemData];
        }
        localStorage.setItem('pro_cartItems', JSON.stringify(cartItems));
        this.cartItems = cartItems;

        // show added successfully message
        if(showSuccessPopup) this.showSuccessAddToCart();
    }


    updateCartItem(productID, newAmount, parentProduct, childeProduct, currentTarget = false) {
        let cartItemData = {
            productID: productID,
            amount: parseInt(newAmount),
            parentProduct : parentProduct,
            childeProduct : childeProduct
        };

        // get current cart items from localStorage
        let cartItems = localStorage.getItem('pro_cartItems');
        cartItems = JSON.parse(cartItems);
        
        if(!cartItems){
            // new cart - just add the items
            cartItems = [cartItemData];
        } else if(cartItems && cartItems.length > 0){
            // not new cart - check if product allready exists
            let itemFound = false;

            cartItems.map(item => {
                if(item.productID == cartItemData.productID 
                    && item.parentProduct == cartItemData.parentProduct){
                        item.amount = parseInt(cartItemData.amount);
                        item.childeProduct = cartItemData.childeProduct;
                        itemFound = true;

                        return item;
                }
            });

            if(!itemFound){
                cartItems.push(cartItemData);
            }

        } else {
            // empty cart
            cartItems = [cartItemData];
        }

        localStorage.setItem('pro_cartItems', JSON.stringify(cartItems));
        this.cartItems = cartItems;


        // show added successfully message
        if(currentTarget){
            let itemLoader = $('#item_' + productID + '__loader');
            itemLoader.fadeOut('fast');
            $(currentTarget).prop('disabled', false);
        }
    }

    removeCartItem(productID, parentProduct, childeProduct){
        // get current cart items from localStorage
        let cartItems = localStorage.getItem('pro_cartItems');
        cartItems = JSON.parse(cartItems);
        
        if(cartItems && cartItems.length > 0){
            // not new cart - check if product  exists
            let newCartItems = [];

            for(let item of cartItems){
                if(item.productID == productID 
                    && item.parentProduct == parentProduct) continue;
                newCartItems.push(item);
            }
            cartItems = newCartItems;
        }

        // update parent if childe is removed
        if(parentProduct != ''){
            for(let item of cartItems){
                if(item.productID == parentProduct 
                    && item.childeProduct == productID) {
                        item.childeProduct = '';
                    }
            }
        }

        cartItems = this.removeChildeProducts(productID, cartItems);

        // update localStorage
        localStorage.setItem('pro_cartItems', JSON.stringify(cartItems));
        this.cartItems = cartItems;

        this.updateCartDB(this.cartItems);

         // remove warranty if attached
        // if(childeProduct != ''){
        //     this.removeCartItem(childeProduct, productID, "");
        // } else {
        //     this.updateCartDB(this.cartItems);
        // }

    }


    removeChildeProducts(productID, cartItems){
        let newCartItems = [];
        if(cartItems && cartItems.length > 0){
            // not new cart - check if product  exists

            for(let item of cartItems){
                if(item.parentProduct == productID) continue;
                newCartItems.push(item);
            }
        }             
        return newCartItems;
    }

    // DB Update
    updateCartDB(cartItems){
        $.ajax({
            url : somfyData.ajax_url,
            type : 'POST',
            data : {
                action : 'somfy_pro_update_cart',
                cartItems : cartItems.length == 0 ? '' : cartItems,
                clear : cartItems.length == 0 ? true : false,
                nonce: somfyData.ajax_nonce
            },
            dataType: "json",
            success: (response) =>{
                if(!response.status){
                    // SHOW ERROR
                    console.warn(response);
                } else {
                    let checkout_registration = localStorage.getItem('checkout_registration');

                    if(checkout_registration == 'true'){
                        localStorage.removeItem('checkout_registration');
                    }
                }

            },
            error:  (jqXHR, status) => {
                console.log(jqXHR);
                console.log(status);
            }
          });
    }


    // Popups

    showSuccessAddToCart() {
        this.addToCartPopupLoader.hide();
        
        this.addToCartPopupSuccess.show();
        let closeAddToCartPopupTimer = setTimeout(this.closeAddToCartPopup.bind(this), 2000); 

    }

    closeAddToCartPopup(){
        this.addToCartPopupClose.trigger('click');
    }

    show_addWarrantyPopup(productID, amount, radiodiscount){
        $('#product_preview_popup').hide();

        this.add_warranty__quantity_input
            .attr('max', amount)
            .val(amount);

        this.add_warranty__submit
            .attr({
                'data-product' : productID,
                'data-amount' : amount,
                'data-radiodiscount' : radiodiscount
            });

        this.add_warranty_popup.fadeIn('fast');
    }

    numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    clearCart(cleareFromCheckout){
        localStorage.removeItem('pro_cartItems');
        this.cartItems = null;
        this.updateCartDB('');
        this.updateCartPopup(cleareFromCheckout);
    }
}


export default Cart;
