import $ from 'jquery';

class ProductPreview {
    constructor(stockNotifictaion, wishlist, cart, inputsObj) {
        this.wishlist = wishlist;
        this.cart = cart;

        this.popupContainer = $('#product_preview_popup');
        this.popupContainerLoader = $('#product_preview_popup__loader');
        this.popupContainerClose = $('.product_preview_popup__close');
        this.popup__content = $('#product_preview_popup__content');

        this.isCalcItem = false;
        this.initProductPreviewEvent();   

    }

    initProductPreviewEvent(){
        $('.product_preview_cta').off('click').on('click', (e)=>{
            let productID = e.currentTarget.dataset.product;
            let calcitem = e.currentTarget.dataset.calcitem;

            if(calcitem == 'true') this.isCalcItem = true;
            // open popup with loader
            this.popup__content.html('');

            this.popupContainerLoader.show();
            this.popupContainer.fadeIn('fast');

            // get product data
            $.ajax({
                url : somfyData.ajax_url,
                type : 'POST',
                data : {
                    action : 'somfy_pro_product_preview',
                    productID: productID,
                    nonce: somfyData.ajax_nonce
                },
                dataType: "json",
                success: (response) =>{
                    if(response.status){
                        // print result
                        this.printProductPreviewData(response.productData);
                        
                        // set customer remote item data to local storage
                        if(response.cust_radiomodel_qualified.status){
                            localStorage.setItem('pro_cust_radio_remote', JSON.stringify(response.remoteitem));
                        } else {
                            localStorage.removeItem('pro_cust_radio_remote');
                        }

                        // init click events: add cart, wishlist
                        this.wishlist.addClickEvent();
                        this.wishlist.getUsersWishlist();
                        this.cart.initAddToCartEvent();
                    } else {
                        // show error message
                    }
                    this.popupContainerLoader.hide();
                },
                error:  (jqXHR, status) => {
                    console.log(jqXHR);
                    console.log(status);
                }
            });

            
        });

        this.popupContainerClose.off('click').on('click', ()=>{
            this.popupContainer.fadeOut('fast');
        })
    }


    printProductPreviewData(productData){
        // print product thumbnail & tags
        let resultsHtml = `
            <div class='product-thumbnail-col'>`;
    
        // print discount tag
        if(productData.discount_percentage && productData.discount_percentage != ''){
            resultsHtml += `
                <span class="dicount_tag">
                    <span class='tag__title'>הנחה</span><br>
                    ${productData.discount_percentage}%
                </span>`;
        }

        // wishlistCTA
        resultsHtml += `<a class='wishlist_cta' data-product='${productData.productID}' alt='מועדפים'>
            <i class="far fa-heart"></i>
        </a>`;

        resultsHtml += `
                <a href='${productData.permalink}${this.isCalcItem ? '?calculator=true' : ''}' alt='${productData.title}'>
                    <img 
                        src='${productData.thumbnail_url}' 
                        alt='${productData.title}' 
                        class='product-thumbnail'>
                </a>
            </div>`;
        // end - product thumbnail & tags


        // print content
        resultsHtml += `<div class='product_info_container'>`


        // print product title
        resultsHtml += `
            <div class='product-meta-container'>
                <a href="${productData.permalink}${this.isCalcItem ? '?calculator=true' : ''}" class='product-title' alt='${productData.title}'>${productData.title}</a>
                <p>${productData.itemid}</p>
                <hr class="divider">
            </div>`;

        // print description
        resultsHtml += productData.short_desc != '' ? `
            <div class='product-description-container'>
                <p>${productData.short_desc}</p>
                <hr class="divider">
            </div>`:'';


        // print product CTA & prices
        resultsHtml += `
            <div class="product-cta-container">
            
                <div class='product-cta'>
                    <button class='add_to_cart__cta' 
                        data-calcitem='${this.isCalcItem ? 'true' : 'false'}'
                        data-product='${productData.productID}'
                        data-add_warranty='false'
                        data-radiodiscount='${productData.radiomodel_qualified}'
                        data-warranty_applicable='${productData.warranty_applicable}'>
                        <img 
                            src="${somfyData.template_url}/assets/svg/002-shopping-cart-light.svg" 
                            alt="cart" 
                            loading="lazy">
                        הוסף לסל
                    </button>
                </div>`;
        
        
        resultsHtml += `<div class='product-price'>`;
        if(productData.on_sell == true){
            resultsHtml += `
                <div class='top_price'>
                    <span class='old_price'>
                        <span class='currency_symbol'>₪</span>${productData.base_price}
                    </span>
                    <div class="vl"></div>
                    <span class='discount_price'>
                        <span class='currency_symbol'>₪</span>${productData.discount_price}
                    </span>
                </div>
                
                <div class='bottomline_price'>מחיר ברוטו: <span class='currency_symbol'>₪</span>${productData.base_price}</div>`;
        } else {
            resultsHtml += `
                <div class='top_price'>
                    <span class='base_price'>
                        <span class='currency_symbol'>₪</span>${productData.base_price_after_discountcode}
                    </span>
                </div>
                
                <div class='bottomline_price'>מחיר ברוטו: <span class='currency_symbol'>₪</span>${productData.base_price}</div>`;
        }
        // end - prices
        resultsHtml += `</div>`;

        // amount input
        resultsHtml += `<div class="item_amount">
            <label for="item_amount__${productData.productID}">כמות:</label>
            <input type="number" class="item_amount_input form-control form-control-sm" 
                id="item_amount__${productData.productID}"
                data-product="${productData.productID}" 
                min="1" max="100000" 
                value="1">
        </div>`;


        // end - product CTA & prices
        resultsHtml += `</div>`;

        // end - content
        resultsHtml += `</div>`;


        this.popup__content.html(resultsHtml);
    }
}

export default ProductPreview;
