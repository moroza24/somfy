import $ from 'jquery';
import ProductPreview from './ProductPreview';

class ShopPage {

    constructor(stockNotifictaion, wishlist, cart, inputsObj) {

        this.stockNotifictaion = stockNotifictaion;
        this.wishlist = wishlist;
        this.cart = cart;
        this.inputsObj = inputsObj;

        this.productPreviewObj = new ProductPreview(stockNotifictaion, wishlist, cart, inputsObj);
        this.isLoading = false;
        this.firstLoad = true

        // Advanced Filters
        this.selectFilterBtns = $('.select_filter_btn');
        this.numberFilterBtns = $('.number_filter input[type=range]');
        this.priceRangeFilterBtns = $('#price_range_filter input[type=range]');
        this.trueFalseFilterBtns = $('.true_false_filter input[type=radio]');
        this.advancedFilters_containerBaseHtml = $('#advanced_filters_container').html();
        
        this.advanceFilters_submit = $('.advance_filters_submit');
        this.clearFilter = $('.clear_filter');
        this.multiChoiceFilter = $('.multi_choice_filter');
        
        this.advancedFilters = [];
        this.filters = {
            taxonomy_post_type: null,
            taxonomy_value: null
        };
        this.filtersPageId;

        // Sorting
        this.sortFiltersBtn = $('.sort_filter_btn');
        this.sort_filters__container = $('#somfy_shop .sort_filters__container');
        
        this.sortFilter = {
            sortType : '',
            sortOrder : ''
        };


        // Results
        this.shopResults = $('#shop_results');
        this.noResults_msg = $('#shop_no_results_msg');
        this.shopLoader = $('#shop_loader');
        this.totalShopResults = $('#total_shop_results');
        

        // Pagination
        this.loadMoreCTA = $('#load_more_cta');
        this.loadingMore = false;
        
        this.paginators = {
            limit: 12,
            page: 1,
            totalPages: 0
        };

        // calculater data
        this.calcFilters;
        this.comingFromCalculator = false;
        this.calcNameQuery;

        this.initEvents();
        
        // init getting results after setting the filters
        this.getPageFilters();
    }


    initEvents(){

        // Advanced Filters
        this.selectFilterBtns = $('.select_filter_btn');
        this.numberFilterBtns = $('.number_filter input[type=range]');
        this.priceRangeFilterBtns = $('#price_range_filter input[type=range]');
        this.trueFalseFilterBtns = $('.true_false_filter input[type=radio]');

        this.advanceFilters_submit = $('.advance_filters_submit');
        this.clearFilter = $('.clear_filter');
        this.multiChoiceFilter = $('.multi_choice_filter');

        // Sorting
        this.sortFiltersBtn = $('.sort_filter_btn');

        // Sort filters
        this.sortFiltersBtn.off('click').on('click', (e)=>{
            this.setSortFilters(e);
        });


        // Pagination
        this.loadMoreCTA = $('#load_more_cta');


        // Advanced Filters
        this.selectFilterBtns.off('click').on('click', (e) => {
            // update select filter
            e.preventDefault();
            this.toggleSelectFilter(e);
        });


        // on back/forword button click
        window.onpopstate = (e)=>{
            this.advancedFilters = [];
            this.firstLoad = true;
            if(e.state){
                this.advancedFilters = e.state;
                this.getResults();
            } else {
                this.getShopFilters();
            }
        };

        // multi choice filter
        this.multiChoiceFilter.off('click').on('click' , (e) => {
            this.toggleMultiChoice(e);
        })
        
        // clear filters
        this.clearFilter.off('click').on('click' , (e) => {
            let filterType = e.currentTarget.dataset.filterType;
            let filterName = e.currentTarget.dataset.filterName;

            if(filterType == 'select' || filterType == 'relationship'){
                this.clearSelecteFilter(e, filterType);
            } else if(filterType == 'number'){
                $('#'+filterName).val(0).trigger('input');
            } else if(filterType == 'true_false'){
                $('input[name='+filterName).prop('checked', false);
            } else if(filterType == 'price_range_filter'){
                let startPriceFilter = $('#price_range_filter_start').val();
                let endPriceFilter = $('#price_range_filter_end').val();

                $(this.priceRangeFilterBtns[0]).val(startPriceFilter).trigger('input');
                $(this.priceRangeFilterBtns[1]).val(endPriceFilter).trigger('input');
            }
            
            let updatedAdvancedFilters = [];
            this.advancedFilters.map(a => {
                if(a.filterName != filterName) updatedAdvancedFilters.push(a);
            });
            this.advancedFilters = updatedAdvancedFilters;

            $(e.currentTarget).fadeOut('fast');
        })
        
        // Filter buttons - set values
        this.numberFilterBtns.off('change').on('change', (e) =>{
            this.setNumberFilterValue(e.currentTarget.id);
        });

        this.priceRangeFilterBtns.off('change').on('change', (e) =>{
            this.setPriceRangeFilterValue();
        })

        this.trueFalseFilterBtns.off('click').on('click', (e) =>{
            this.setTrueFalseFilterValue(e.currentTarget.name);
        })


        // submit filters
        this.advanceFilters_submit.off('click').on('click', () => {
            this.loadingMore = false;
            
            this.reset_calc();
            this.getResults();

            $('#headingOne .btn-link').trigger('click');
        })

        
        // Pagination
        this.loadMoreCTA.off('click').on('click', ()=>{
            this.loadMoreCTA.hide();
            this.paginators.page += 1;
            this.loadingMore = true;

            this.getResults();
        });

        if(this.comingFromCalculator){
            $(window).bind('beforeunload', function(){
                localStorage.removeItem('pro_calc_products');
              });
        }
    }

    
    // Get results
    getResults(){
        this.totalShopResults.html('');
        this.loadMoreCTA.hide();
        this.shopLoader.fadeIn('slow');
        this.isLoading = true;
        this.noResults_msg.hide();
        if(!this.loadingMore) this.shopResults.html('');

        localStorage.removeItem('pro_calc_products');
        
        this.setWindowHistory();
        this.firstLoad = this.firstLoad ? false : false;

        console.log(this.advancedFilters);
        $.ajax({
            url : somfyData.ajax_url,
            type : 'POST',
            data : {
                action : 'somfy_pro_shop_products',
                advancedFilters : this.advancedFilters,
                sortFilter : this.sortFilter,
                paginators: this.paginators,
                filtersPageId: this.filtersPageId,
                comingFromCalculator: this.comingFromCalculator,
                calcNameQuery: this.calcNameQuery,
                nonce: somfyData.ajax_nonce
            },
            dataType: "json",
            success: (response) =>{
                console.log(response);
                if(response.status){
                    let products_results = response.products;
                    let filtersObjects = response.filtersObjects;
                    if(response.total == 0){
                        // show "nothing found msg"
                        this.noResults_msg.fadeIn('fast');
                        this.sort_filters__container.hide();

                        this.shopResults.html('');
                        this.shopResults.hide();
                    } else {
                        // print results
                        
                        this.noResults_msg.hide();

                        // prints products
                        this.printProductsResults(products_results);
                        
                        this.totalShopResults.html('סה"כ תוצאות: ' + response.total);

                        // print #load_more_cta
                        this.paginators.totalPages = Math.ceil(response.total / this.paginators.limit);
                        
                        if(this.paginators.totalPages > this.paginators.page){
                            this.loadMoreCTA.fadeIn('fast');
                            this.paginators.totalPages = Math.ceil(response.total / this.paginators.limit);
                        } else if(this.paginators.totalPages == this.paginators.page){
                            this.loadMoreCTA.hide();
                        }


                        // set customer remote item data to local storage
                        if(response.cust_radiomodel_qualified.status){
                            localStorage.setItem('pro_cust_radio_remote', JSON.stringify(response.remoteitem));
                        } else {
                            localStorage.removeItem('pro_cust_radio_remote');
                        }

                        
                        this.stockNotifictaion.initCtaEvents();
                        this.wishlist.addClickEvent();
                        this.wishlist.getUsersWishlist();
                        this.cart.initAddToCartEvent();
                        this.productPreviewObj.initProductPreviewEvent();
                        
                        this.sort_filters__container.show();
                        this.shopResults.fadeIn('fast');
                        
                    }
                    // prints advanced filters
                    this.printAdvancedFilters(filtersObjects);
                    
                    this.initEvents();
                    this.activateCurrentPageFilter();
                    this.inputsObj.initRangeSlider();
                    this.inputsObj.initSingleRange();
                    
                    if(!this.loadingMore){
                        // scroll to top
                        $('html, body').animate({
                            scrollTop: $("#advanced_filters_accordion").offset().top - 400
                        }, 250);
                    }
                } else {
                    this.noResults_msg.show();
                }
                this.isLoading = false;
                this.shopLoader.fadeOut('slow');
            },
            error:  (jqXHR, status) => {
                console.log(jqXHR);
                console.log(status);
                this.isLoading = false;
                this.shopLoader.fadeOut('slow');
            }
            });
        

    }

    
    // print the results
    printProductsResults(products){
        let resultsHtml = this.loadingMore ? this.shopResults.html() : '';
        let calcProducts = JSON.parse(localStorage.getItem('pro_calc_products'));
        if(!calcProducts) calcProducts = [];
        products.forEach((product) => {
            if(this.comingFromCalculator && !calcProducts.includes(product.ID)){
                calcProducts.push(product.ID);
            }

            // print product thumbnail & tags
            resultsHtml += `
                <div class='product-card'>
                    <div class='product-thumbnail-col'>`;
            
            // print discount tag
            if(product.discount_percentage && product.discount_percentage != ''){
                resultsHtml += `
                    <span class="dicount_tag">
                        <span class='tag__title'>הנחה</span><br>
                        ${product.discount_percentage}%
                    </span>`;
            }

            // wishlistCTA
            resultsHtml += `<a class='wishlist_cta' data-product='${product.ID}' alt='מועדפים'>
                <i class="far fa-heart"></i>
            </a>`;

            // product preview
            resultsHtml += `<a class='product_preview_cta' data-product='${product.ID}' data-calcitem='${this.comingFromCalculator ? 'true' : 'false'}' alt='תצוגה מקדימה'>
                <i class="fas fa-search-plus"></i>
            </a>`;
            resultsHtml += `
                    <a href='${product.permalink}${this.comingFromCalculator ? "?calculator=true" : ""}' alt='${product.post_title}'>
                        <img 
                            src='${product.thumbnail_url}' 
                            alt='${product.post_title}' 
                            class='product-thumbnail'>
                    </a>
                </div>`;


            // print product title
            resultsHtml += `
                <div class='product-meta-container'>
                    <a href="${product.permalink}${this.comingFromCalculator ? '?calculator=true' : ''}" class='product-title' alt='${product.post_title}'>${product.post_title}</a>
                </div>`;

            // print product CTA & prices
            resultsHtml += `
                <hr class="divider">
                <div class="product-cta-container">
                    <div class='product-cta'>
                    
                        <button class='add_to_cart__cta' 
                            data-calcitem='${this.comingFromCalculator ? 'true' : 'false'}'
                            data-product='${product.ID}'
                            data-add_warranty='false'
                            data-radiodiscount='${product.radiomodel_qualified}'
                            data-warranty_applicable='${product.warranty_applicable}'>
                            <img 
                                src="${somfyData.template_url}/assets/svg/002-shopping-cart-light.svg" 
                                alt="cart" 
                                loading="lazy">
                            הוסף לסל
                        </button>
                    </div>`;
            
            
            resultsHtml += `<div class='product-price'>`;
            if(product.on_sell == true){
                resultsHtml += `
                    <div class='top_price'>
                        <span class='old_price'>
                            <span class='currency_symbol'>₪</span>${product.pricelist.base_price}
                        </span>
                        <div class="vl"></div>
                        <span class='discount_price'>
                            <span class='currency_symbol'>₪</span>${product.discount_price}
                        </span>
                    </div>`;
                resultsHtml += `<div class='bottomline_price'>מחיר ברוטו: <span class='currency_symbol'>₪</span>${product.pricelist.base_price}</div>`;
            } else {
                resultsHtml += `
                    <div class='top_price'>
                        <span class='base_price'>
                            <span class='currency_symbol'>₪</span>${product.pricelist.base_price_after_discountcode}
                        </span>
                    </div>`;
                resultsHtml += `<div class='bottomline_price'>מחיר ברוטו: <span class='currency_symbol'>₪</span>${product.pricelist.base_price}</div>`;
            }
            resultsHtml += `</div>`;
            resultsHtml += `</div></div>`;
        });
        
        this.shopResults.html(resultsHtml);

        localStorage.setItem('pro_calc_products', JSON.stringify(calcProducts));
    }

    // print advanced filters
    printAdvancedFilters(filtersObjects){
        let advanced_filters_container = $('#advanced_filters_container');
        let afHtml = '';

        filtersObjects.forEach((filter) => {
            let filterName = filter.name;
            let filterLabel = filter.label;
            let filterType = filter.type;
            let selectHtml = '';

            if((filterType == 'relationship' && filter.haveChoices > 1) || filterType == 'select'){
            // if(filterType == 'relationship' || filterType == 'select'){
                selectHtml += `<div class='filter__col' data-filter-name='${filterName}'>
                    <h5>${filterLabel}</h5>
                    <a class='clear_filter' 
                        data-filter-type='${filterType}' 
                        alt='${filterLabel}' 
                        data-filter-name='${filterName}'><i class='fas fa-eraser'></i></a>
                    <hr class='divider'>`;

                selectHtml += `<div class='select_filter'>`;
                let filterChoices = Object.values(filter.choices);
                let totalResults = 0;

                filterChoices.forEach((option) =>{
                    let title = option.title;
                    let value = option.value;
                    let results = option.results;
                    
                    if(value == '-1' || !value) return;
                    if(results == '0') return;

                    totalResults += 1;
                    selectHtml += `<a class='select_filter__option' data-filter-name='${filterName}' data-id='${value}' data-filter-type='relationship'>
                            <input type='checkbox' class='select_filter__option_checkbox' data-filter-name='${filterName}' id='${filterName}-option_${value}' name='${filterName}-option_${value}' value='${value}'>
                            <label class='select_filter_btn' for='${filterName}-option_${value}'>${title}</label>
                            (${results})
                        </a>`;
                })

                selectHtml += "</div>";
                // multichoice btn
                if(filterType == 'select'){
                    selectHtml += `<a class='multi_choice_filter' data-active='false' data-filter-name='${filterName}' data-filter-type='${filterType}'>בחירה מרובה</a>`;
                }
                if(totalResults > 0){
                    afHtml += selectHtml;
                }
            } else if(filterType == 'number'){
                let min = filter.min;
                let max = filter.max;
                let step = filter.step;
                afHtml += `<div class='filter__col' data-filter-name='${filterName}'>
                    <h5>${filterLabel}</h5>
                    <a class='clear_filter' 
                        data-filter-type='${filterType}' 
                        alt='${filterLabel}' 
                        data-filter-name='${filterName}'><i class='fas fa-eraser'></i></a>
                    <hr class='divider'>`;
                    
                afHtml +=  `<div class='number_filter single-range-wrap'>
                        <div class='range-value single-range__label' style='right: calc(0% + 10px);'>
                            <span>0</span>
                        </div>
                        <input id='${filterName}' type='range' min='${min}' max='${max}' value='0' step='${step}'>
                    </div>`;
            } else if(filterType == 'true_false'){
                afHtml += `<div class='filter__col' data-filter-name='${filterName}'>
                    <h5>${filterLabel}</h5>
                    <a class='clear_filter' 
                        data-filter-type='${filterType}' 
                        alt='${filterLabel}' 
                        data-filter-name='${filterName}'><i class='fas fa-eraser'></i></a>
                    <hr class='divider'>`;
                afHtml += `<div class='true_false_filter'>
                    <div class='option_group'>
                        <input type='radio' class='btn-check' name='${filterName}' id='${filterName}-true' autocomplete='off' value='true' >
                        <label class='true_false_filter-true' for='${filterName}-true'>כן</label>
                    </div>
                    
                    <div class='option_group'>
                        <input type='radio' class='btn-check' name='${filterName}' id='${filterName}-false' autocomplete='off' value='false'>
                        <label class='true_false_filter-false' for='${filterName}-false'>לא</label>
                    </div>
                </div>`;
            }

            afHtml += "</div>";
        });
        advanced_filters_container.html(afHtml).append(this.advancedFilters_containerBaseHtml);
    }

    // set active filters from result
    activateCurrentPageFilter(){
        this.advancedFilters.forEach(filter => {
            if(filter.filterType == 'relationship'){
                $(`label[for="${filter.filterName}-option_${filter.value[0]}"]`).trigger('click'); 
            } else if(filter.filterType == 'select'){
                for (let i = 0; i < filter.value.length; i++){
                    $('.multi_choice_filter[data-filter-name='+filter.filterName+']').trigger('click');
                    $(`label[for="${filter.filterName}-option_${filter.value[i]}"]`).trigger('click'); 
                    $('.multi_choice_filter[data-filter-name='+filter.filterName+']').trigger('click');
                }  
            }else if(filter.filterType == "range"){
                $(this.priceRangeFilterBtns[0]).val(filter.min).trigger('input');
                $(this.priceRangeFilterBtns[1]).val(filter.max).trigger('input');
            }
        })
        let category = $('#category').val();
        let subgroup = $('#subgroup').val();
        let subsubgroup = $('#subsubgroup').val();

        $('label[for="custitem_somfy_web_product_category-option_' + category + '"]').trigger('click'); 
        $('label[for="custitem_sub_product_group-option_' + subgroup + '"]').trigger('click'); 
        $('label[for="-option_custitem_sub2productgroup' + subsubgroup + '"]').trigger('click'); 
    }


    // get initial filtes from url and page
    // in case of: Calculator or Shop page
    getPageFilters(){
        this.loadingMore = false;
        this.paginators.page = 1;
        this.calcFilters = JSON.parse(localStorage.getItem('pro_calc_finalFilters'));
        
        // check if coming from calculator
        if(this.getAllUrlParams().calculator == "true" && this.calcFilters){
            this.comingFromCalculator = true;
            this.getCalculatorFilters();
        } else {
            this.comingFromCalculator = false;
            this.getShopFilters();
        };         
    }


    // set calculator filters and results
    getCalculatorFilters(){
        this.calcNameQuery = this.calcFilters.nameQuery;
        let category_filterData = {
            'filterName' : 'custitem_somfy_web_product_category',
            'value' : this.calcFilters.category,
            'compare_type' : 'IN',
            'filterType' : 'relationship'
        };

        let subgroup_filterData = {
            'filterName' : 'custitem_sub_product_group',
            'value' : this.calcFilters.subgroup,
            'compare_type' : 'IN',
            'filterType' : 'relationship'
        };

        let subsubgroup_filterData = {
            'filterName' : 'custitem_sub2productgroup',
            'value' : this.calcFilters.subsubgroup,
            'compare_type' : 'IN',
            'filterType' : 'relationship'
        };

        let custitem_wireradio = {
            'filterName' : 'custitem_wireradio',
            'value' : this.calcFilters.tech,
            'compare_type' : 'IN',
            'filterType' : 'select'
        };
        let custitem_diameter = {
            'filterName' : 'custitem_diameter',
            'value' : this.calcFilters.pipeDiameterCode,
            'compare_type' : 'IN',
            'filterType' : 'select'
        };
        let custitem_torque = {
            'filterName' : 'custitem_torque',
            'value' : this.calcFilters.torque,
            'compare_type' : '>=',
            'filterType' : 'number'
        };
        let custitem_csi = {
            'filterName' : 'custitem_csi',
            'value' : this.calcFilters.backup,
            'compare_type' : '=',
            'filterType' : 'boolen'
        };

        this.updateFilterView(category_filterData);
        this.updateFilterView(subgroup_filterData);
        this.updateFilterView(subsubgroup_filterData, false);

        this.updateFilterView(custitem_wireradio);
        this.updateFilterView(custitem_diameter);
        this.updateFilterView(custitem_torque);
        this.updateFilterView(custitem_csi);

        this.filtersPageId = this.calcFilters.subgroup[0];

        this.getResults();
    }


    // set shop filters
    getShopFilters(){
        let urlParams = this.getAllUrlParams();

        let shop = $('#shop').val();
        let category,subgroup, subsubgroup;
        
        let isSelectFilter = new RegExp('^s__');
        let isNumberFilter = new RegExp('^n__');
        let isBoolFilter = new RegExp('^b__');
        
        let rangeFilterData = {
            'filterName' : 'price_range_filter', 
            'compare_type' : 'BETWEEN',
            'filterType' : 'range',
            'min' : 0,
            'max' : 0,
        };

        for (const [key, value] of Object.entries(urlParams)) {
            let filterData;
            if(value == '') continue;

            if(isNumberFilter.test(key)){ // number
                filterData = {
                    'filterName' : key.replace("n__", ""),
                    'value' : value,
                    'compare_type' : '=',
                    'filterType' : 'number'
                };
            } else if(isBoolFilter.test(key)){ // bool
                filterData = {
                    'filterName' : key.replace("b__", ""),
                    'value' : value,
                    'compare_type' : '=',
                    'filterType' : 'boolean'
                };
            } else if(isSelectFilter.test(key)){ // select field
                filterData = {
                    'filterName' : key.replace("s__", ""),
                    'value' : [value],
                    'compare_type' : 'IN',
                    'filterType' : 'relationship'
                };
            } else if(key == 'category' || key == 'subgroup' || key == 'subsubgroup'){
                if(key == 'category') {
                    key = 'custitem_somfy_web_product_category';
                } else if(key == 'subgroup') {
                    key = 'custitem_sub_product_group';
                }else if(key == 'subsubgroup') {
                    key = 'custitem_sub2productgroup';
                }

                filterData = {
                    'filterName' : key,
                    'value' : [value],
                    'compare_type' : 'IN',
                    'filterType' : 'relationship'
                };
            } else if(key == "price_range_filter__min"){
                rangeFilterData.min = value;
            } else if(key == "price_range_filter__max"){
                rangeFilterData.max = value;
            } 
            
            if(filterData != undefined) this.updateFilterData(filterData);
        }   

        if(rangeFilterData.min && rangeFilterData.max) this.updateFilterData(rangeFilterData);
        
        category = urlParams['category'];
        subgroup = urlParams['subgroup'];
        subsubgroup = urlParams['subsubgroup'];
        
        if(subgroup != '' && subgroup != undefined){
            this.filtersPageId = subgroup;
        } else if(category != '' && category != undefined){
            this.filtersPageId = category;
        } else {
            this.filtersPageId = shop;
        }

        this.getResults();
    }


    // updateFilterView
    updateFilterView(filterData, updateOptionsView = true) {
        if(filterData.value.length == 0) return;

        if(filterData.value.length == 1 && (filterData.value[0] == '' || filterData.value[0] == undefined)) {
            let updatedAdvancedFilters = [];
            this.advancedFilters.map(a => {
                if(a.filterName != filterData['filterName']) updatedAdvancedFilters.push(a);
            });
            this.advancedFilters = updatedAdvancedFilters;

        } else if(updateOptionsView){
            for(let k = 0; k < filterData.value.length; k++){
                $('label[for="' + filterData.filterName + '-option_' + filterData.value[k] + '"]').trigger('click'); // activate only page filter
            }
            this.updateFilterData(filterData);
        }
    }


    // Sort filters
    setSortFilters(e){
        this.loadingMore = false;
        this.paginators.page = 1;

        if($(e.currentTarget).hasClass('active')){
            $(e.currentTarget).removeClass('active');
            this.sortFilter.sortType = '';
            this.sortFilter.sortOrder = '';
        } else {
            $('.sort_filter_btn.active').removeClass('active');
            $(e.currentTarget).addClass('active');
            this.sortFilter.sortType = e.currentTarget.dataset.sortType;
            this.sortFilter.sortOrder = e.currentTarget.dataset.sortOrder;
        }
        this.getResults();
    }


    // Advanced Filters - Select filter
    toggleMultiChoice(e){
        let filterName = e.currentTarget.dataset.filterName;
        let filterType = e.currentTarget.dataset.filterType;
        let isActive = e.currentTarget.dataset.active;
        let sfo = $('a.select_filter__option[data-filter-name=' + filterName + ']');
        let cb = $('a.select_filter__option[data-filter-name=' + filterName + '] .select_filter__option_checkbox');
        
        if(isActive == "false"){
            sfo.addClass('multiple');
            $(e.currentTarget)
                .html('בחר')
                .addClass('action-submit')
                .attr('data-active', true)
                .attr('data-action', 'submit');
        } else {
            // submit filter to get results
            this.setSelectFilterValues(filterName, filterType);


            sfo.removeClass('multiple');
            $(e.currentTarget)
                .html('בחירה מרובה')
                .removeClass('action-submit')
                .attr('data-active', false)
                .attr('data-action', 'toggle');
        }
    }

    toggleSelectFilter(e){
        let parent = $(e.currentTarget.parentElement);
        let cb = $(parent).find('.select_filter__option_checkbox');
        let filterName = parent[0].dataset.filterName;
        let filterType = parent[0].dataset.filterType;

        let cbAll = $('.select_filter__option[data-filter-name='+filterName+']').find('.select_filter__option_checkbox');
        let parentAll = $('.select_filter__option[data-filter-name='+filterName+']');

        if(parent.hasClass('multiple')){
            if(!cb.is(':checked')){
                parent.addClass('active');
                cb.prop( "checked", true );
            } else {
                parent.removeClass('active');
                cb.prop( "checked", false );
                
            }
        } else {
            if(!cb.is(':checked')){
                cbAll.prop( "checked", false );
                parentAll.removeClass('active');
                parent.addClass('active');
                cb.prop( "checked", true );
            } else {
                cbAll.prop( "checked", false );
                parentAll.removeClass('active');
            }
            this.setSelectFilterValues(filterName, filterType);
        }

        let cbChecked = $('.select_filter__option[data-filter-name='+filterName+']').find('.select_filter__option_checkbox:checked');
        if (cbChecked.length > 0){
            $('.clear_filter[data-filter-name='+filterName+']').fadeIn('fast');
        }else {
            $('.clear_filter[data-filter-name='+filterName+']').fadeOut('fast');
        }

    }


    setSelectFilterValues(filterName, filterType){
        let cbAll = $('.select_filter__option[data-filter-name='+filterName+']').find('.select_filter__option_checkbox:checked');
        
        let filterValues = [];

        for (let i = 0; i < cbAll.length; i++){
            filterValues.push($(cbAll[i]).val())
        }

        let filterData = {
            'filterName' : filterName,
            'value' : filterValues,
            'compare_type' : 'IN',
            'filterType' : filterType
        };
        
        if(!this.isLoading) this.updateFilterData(filterData);

        if(filterName == 'custitem_sub_product_group' && filterValues.length > 0) this.filtersPageId = filterValues[0];
        if(filterName == 'custitem_sub_product_group' && filterValues.length == 0) this.filtersPageId = $('#category').val();
        
    }

    clearSelecteFilter(e, filterType){
        let filterName = e.currentTarget.dataset.filterName;

        $('.select_filter__option[data-filter-name='+filterName+']').removeClass('active');
        $('.select_filter__option_checkbox[data-filter-name='+filterName+']').prop('checked', false);
        this.setSelectFilterValues(filterName, filterType);
    }


    // Advanced Filters - Number filter
    setNumberFilterValue(filterName){
        let rangInput = $('input[type=range]#' + filterName);
        
        let filterValue = rangInput.val();

        let filterData = {
            'filterName' : filterName,
            'value' : filterValue,
            'compare_type' : '=',
            'filterType' : 'number'
        };
        
        if(!this.isLoading) this.updateFilterData(filterData);
        
        if(filterValue > 0) {
            // show erase
            $('.clear_filter[data-filter-name='+filterName+']').fadeIn('fast');
        }
    }

    // Advanced Filters - True/False filter
    setTrueFalseFilterValue(filterName){
        
        let filterValue = $('input[name='+filterName+']:checked').val();

        let filterData = {
            'filterName' : filterName,
            'value' : filterValue,
            'compare_type' : '=',
            'filterType' : 'boolean'
        };

        if(!this.isLoading) this.updateFilterData(filterData);
        
        $('.clear_filter[data-filter-name='+filterName+']').fadeIn('fast');
    }

    // Advanced Filters - Price Range filter
    setPriceRangeFilterValue(){
        let filterName = 'price_range_filter';

        let val1 = $(this.priceRangeFilterBtns[0]).val();
        let val2 = $(this.priceRangeFilterBtns[1]).val();

        let filterData = {
            'filterName' : filterName,
            'min' : Math.min(val1, val2),
            'max' : Math.max(val1, val2),
            'compare_type' : 'BETWEEN',
            'filterType' : 'range'
        };

        if(!this.isLoading) this.updateFilterData(filterData);

        $('.clear_filter[data-filter-name='+filterName+']').fadeIn('fast');
    }

    updateFilterData(filterData){
        let filterUpdated = false;
        for (let i = 0; i < this.advancedFilters.length; i++) {
            if(this.advancedFilters[i].filterName == filterData.filterName) {
                this.advancedFilters[i] = filterData;
                filterUpdated = true;
                break;
            }
        }
        if(!filterUpdated) this.advancedFilters.push(filterData);
    }

    getAllUrlParams(url) {
        // get query string from url (optional) or window
        var queryString = url ? url.split('?')[1] : window.location.search.slice(1);
        
        // we'll store the parameters here
        var obj = {};
        
        // if query string exists
        if (queryString) {
        
            // stuff after # is not part of query string, so get rid of it
            queryString = queryString.split('#')[0];
        
            // split our query string into its component parts
            var arr = queryString.split('&');
        
            for (var i = 0; i < arr.length; i++) {
                // separate the keys and the values
                var a = arr[i].split('=');
        
                // set parameter name and value (use 'true' if empty)
                var paramName = a[0];
                var paramValue = typeof (a[1]) === 'undefined' ? true : a[1];
        
                // (optional) keep case consistent
                paramName = paramName.toLowerCase();
                if (typeof paramValue === 'string') paramValue = paramValue.toLowerCase();
        
                // if the paramName ends with square brackets, e.g. colors[] or colors[2]
                if (paramName.match(/\[(\d+)?\]$/)) {
        
                    // create key if it doesn't exist
                    var key = paramName.replace(/\[(\d+)?\]/, '');
                    if (!obj[key]) obj[key] = [];
        
                    // if it's an indexed array e.g. colors[2]
                    if (paramName.match(/\[\d+\]$/)) {
                        // get the index value and add the entry at the appropriate position
                        var index = /\[(\d+)\]/.exec(paramName)[1];
                        obj[key][index] = paramValue;
                    } else {
                        // otherwise add the value to the end of the array
                        obj[key].push(paramValue);
                    }
                } else {
                    // we're dealing with a string
                    if (!obj[paramName]) {
                        // if it doesn't exist, create property
                        obj[paramName] = paramValue;
                    } else if (obj[paramName] && typeof obj[paramName] === 'string'){
                        // if property does exist and it's a string, convert it to an array
                        obj[paramName] = [obj[paramName]];
                        obj[paramName].push(paramValue);
                    } else {
                        // otherwise add the property
                        obj[paramName].push(paramValue);
                    }
                }
            }
        }
        return obj;
    }

    reset_calc(){
        this.comingFromCalculator = false;
        localStorage.removeItem('pro_calc_finalFilters');
        localStorage.removeItem('pro_calc_products');
        localStorage.removeItem('pro_calc_userInputs');
    }


    setWindowHistory(){
        if(this.firstLoad || this.loadingMore) return;

        let newUrlParams = this.insertParam();
        let newUrl = document.location.origin + document.location.pathname + newUrlParams;
        history.pushState(this.advancedFilters, document.title, newUrl);
    }

    insertParam() {
        let filtersUrlParams = '?';
        for (let i = 0; i < this.advancedFilters.length; i++) {
            let filter = this.advancedFilters[i];
            
            // range
            if(filter['filterName'] == 'price_range_filter'){
                let keyMin = encodeURIComponent(filter['filterName'] + '__min');
                let valueMin = encodeURIComponent(filter['min']);
                let keyMax = encodeURIComponent(filter['filterName'] + '__max');
                let valueMax = encodeURIComponent(filter['max']);
    
                if(keyMin != '' && valueMax != '') {
                    filtersUrlParams += '&' + [keyMin,valueMin].join('=');
                    filtersUrlParams += '&' + [keyMax,valueMax].join('=');
                }
            // select/relationship
            } else if(filter['filterType'] == "relationship" || filter['filterType'] == 'select') {
                let key = '';
                if(filter['filterName'] == "custitem_somfy_web_product_category"){
                    key = encodeURIComponent('category');
                } else if(filter['filterName'] == "custitem_sub_product_group"){
                    key = encodeURIComponent('subgroup');
                } else if(filter['filterName'] == "custitem_sub2productgroup"){
                    key = encodeURIComponent('subsubgroup');
                } else {
                    key = encodeURIComponent('s__' + filter['filterName']);
                }

                let value = encodeURIComponent(filter['value']);
    
                if(value != '') filtersUrlParams += '&' + [key,value].join('=');
            // boolean
            } else if(filter['filterType'] == "boolean"){
                let key = encodeURIComponent('b__' + filter['filterName']);
                let value = encodeURIComponent(filter['value']);
                if(value != '') filtersUrlParams += '&' + [key,value].join('=');
            // number
            } else if(filter['filterType'] == "number"){
                let key = encodeURIComponent('n__' + filter['filterName']);
                let value = encodeURIComponent(filter['value']);
                if(value != '') filtersUrlParams += '&' + [key,value].join('=');
            };

        }

        return filtersUrlParams;
    }
}

export default ShopPage;
