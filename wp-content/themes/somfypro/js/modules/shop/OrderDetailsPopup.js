
import $ from 'jquery';
import InvPrint from './InvPrint';

class OrderDetailsPopup {

    constructor(orderAgain) {
        this.orderAgain = orderAgain;

        this.orderDetails__loader = $('#order_details__loader');
        this.no_ordersitems__message = $('#no_ordersitems__message');
        this.order_details__results = $('#order_details__results');
        this.orderDetails_popup = $('#order_details_popup');
        this.orderDetails_popup__close = $('#order_details_popup__close');

        this.InvPrintObj = new InvPrint();
    }

    initShowOrderItemsEvent(){
        let showOrderItemsBtn = $('.show_order_items');

        showOrderItemsBtn.off('click').on('click', (e)=>{
            let orderTranid = e.currentTarget.dataset.order;
            
            this.resetOrderInfo();
            this.orderDetails__loader.show();
            this.no_ordersitems__message.hide();
            this.order_details__results.hide();
            this.orderDetails_popup.fadeIn('fast');

            $.ajax({
                url : somfyData.ajax_url,
                type : 'POST',
                data : {
                    action : 'somfy_pro_order_details',
                    order_tranid: orderTranid,
                    nonce: somfyData.ajax_nonce
                },
                dataType: "json",
                success: (response) =>{
                    if(response.status){
                        if(response.items && response.items.length > 0){
                            this.printOrderItems(response.items);
                            this.printOrderInfo(response.orderInfo);
                            this.order_details__results.show();
                        } else {
                            // show no results found
                            this.no_ordersitems__message.show();
                        }
                        // hide loader
                        this.orderDetails__loader.hide();
                    } else {
                        // SHOW ERROR
                        console.warn(response);
                    }
    
                },
                error:  (jqXHR, status) => {
                    console.log(jqXHR);
                    console.log(status);
                }
            });
        });

        this.orderDetails_popup__close.off('click').on('click', () =>{
            this.orderDetails_popup.fadeOut('fast');
        })

        
    }


    resetOrderInfo(){
        $('#order_details__subtotal').html("");
        $('#order_details__total').html("");
        $('#order_details__order_tranid').html("");
        $('#order_details__order_inv').html("");
        $('#order_details__order_status').html("");
    }

    printOrderItems(items){
        let orderDetails__orderitems = $('tbody[data-for="order_details__orderitems"]');
        orderDetails__orderitems.html('');
        let cat_total = 0;

        items.forEach(item => {
            let orderItemTrHtml = `<tr>
                    <th scope='row'>
                        <a href='${item.permalink}' alt='${item.itemid}'> ${item.itemid}</a>
                    </th>
                    <td>${item.post_title}</td>
                    <td>${item.quantity}</td>
                    <td>${item.quantityshiprecv}</td>
                    <td>${this.numberWithCommas(item.amount)} ₪</td>
                    <td>${this.numberWithCommas(item.grossamount)} ₪</td>
                </tr>`;

            cat_total += parseFloat( item.custcol_originalitemrate);
            orderDetails__orderitems.append(orderItemTrHtml);
        });

        $('#order_details__cat_total').html("₪" + this.numberWithCommas(cat_total));
        
    }
    

    printOrderInfo(orderInfo){
        let invHtml = '';
        let loaderHtml = '<div class="loader"><div class="spinner-loader"></div></div>';
        
        let orderStatus = '';
            
        if(orderInfo.acf.status == 'Cancelled'){
            orderStatus = 'בוטלה';
        } else if(orderInfo.acf.status == 'Closed'){
            orderStatus = 'סגורה';
        } else {
            if(orderInfo.delivery_Data.length == 0){ // new order with no tracking data
                orderStatus = 'הזמנה נקלטה והועברה לטיפול';
            } else {
                let newestDeliveryStatus = '131';
                orderInfo.delivery_Data.forEach(del =>{
                    if(del.custrecord_fc_dn_status = "132"){ // 132 = Shipped
                        newestDeliveryStatus = "132";
                    }
                });

                if(newestDeliveryStatus == "131"){// 131 = packed
                    orderStatus = 'הזמנה לוקטה והועברה לשילוח';
                } else {
                    orderStatus = 'הזמנה בהפצה';
                }
            } 
        }
        
        if(orderInfo.inv.length > 0){
            orderInfo.inv.forEach(inv => {
                invHtml += `<li class='inv'><a class='inv_file_download' alt='להורדה' data-recordid='${inv.internalid}'>${inv.tranid} <i class="fas fa-print"></i></a>
                    ${loaderHtml}
                </li>`;
            });
            $('#order_details__order_inv__title').show();
            $('#order_details__order_inv').html(invHtml);
            $('#order_details__order_inv').parent().show();
        } else {
            $('#order_details__order_inv__title').hide();
            $('#order_details__order_inv').parent().hide();
        }

        $('#order_details__subtotal').html("₪" + this.numberWithCommas(orderInfo.acf.subtotal));
        $('#order_details__total').html("₪" + this.numberWithCommas(orderInfo.acf.total));

        $('#order_details__order_tranid').html(orderInfo.acf.tranid);
        $('#order_details__order_again_btn').attr('data-order', orderInfo.acf.tranid);
        $('#order_details__order_status').html(orderStatus);

        this.InvPrintObj.initInvDownloadEvent();
        this.orderAgain.initOrderAgianEvents();
    }

    numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
}

export default OrderDetailsPopup;
