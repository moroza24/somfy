import $ from 'jquery';


class TopNav {

    constructor() {
        // User dropdown
        this.userMenuTrigger = $('#user-menu__trigger');
        this.userMenuTrigger_mobile = $('#mobile-menu__overlay .user_trigger_mobile');


        
        this.userMenuDropdown = $('.user-menu__dropdown');
        this.userMenuDropdown_mobile = $('.user-menu__dropdown');
        

        // Mobile Nav
        this.mobileMenuTrigger = $('#mobile_menu__trigger');
        this.mobileSubmenuTrigger = $('.link__container');
        this.mobileMenuClose = $('.mobile-menu__close');
        this.mobileMenuOverlay = $('#mobile-menu__overlay');

        // megamenu
        this.submenuTrigger = $('.category_link');
        this.subgroupTrigger = $('.group_link__container');

        this.initEvents();


        // categories_nav
        this.categoriesLinks = $('ul.categories-links');
        this.categoryLinks = $('#categories_nav .category_link');
        this.subgroupLinks = $('#categories_nav .subgroup_link__container');
        this.megamenu__subgroups_container = $('#categories_nav .megamenu__subgroups_container');
        this.initCategoriesNavEvents();
    }

    initEvents(){

        this.userMenuTrigger.on('click', (e)=>{

            this.userMenuDropdown.fadeToggle('fast');
            $(e.currentTarget).toggleClass('side_icon__active');
        })


        this.userMenuTrigger_mobile.on('click', (e)=>{
            this.userMenuDropdown_mobile.fadeToggle('fast');
            $(e.currentTarget).toggleClass('side_icon__active');
            
        })

        

        this.mobileMenuTrigger.on('click', (e)=>{
            this.mobileMenuOverlay.fadeIn('fast');
        });

        this.mobileMenuClose.on('click', (e)=>{
            this.mobileMenuOverlay.fadeOut('fast');
        });

        // show megamenu
        this.submenuTrigger.on('mouseenter', (e)=>{
            $(e.currentTarget).find('.megamenu__container').show();
        });

        this.submenuTrigger.on('mouseleave', (e)=>{
            $(e.currentTarget).find('.megamenu__container').hide();
        });

        // show megamenu subgroups
        this.subgroupTrigger.on('mouseenter', (e)=>{
            let target = e.currentTarget.dataset.target;

            let currentActive = $('.group_link__container.active');
            if(currentActive.length > 0){
                let currentActiveTarget = $(currentActive)[0].dataset.target;
                currentActive.removeClass('active');
                $('#'+currentActiveTarget).slideUp('fast');
            }

            $(e.currentTarget).addClass('active');
            $('#'+target).slideDown('fast');
        });

        this.mobileSubmenuTrigger.on('click', (e)=>{
            $(e.currentTarget).find('.mobile-submenu__container').slideToggle('fast');
            $(e.currentTarget).find('.mobile-link__icon').toggleClass('mobile-link__icon--reverse');
        })
    }

    initCategoriesNavEvents(){
        this.categoryLinks.on('mouseenter', (e)=>{
            if($(e.currentTarget).hasClass('category_link__inactive')){
                return;
            }
            this.categoryLinks.removeClass('active').find('.megamenu__subgroups_container').hide();
            $(e.currentTarget).addClass('active').find('.megamenu__subgroups_container').show();
        })

        this.categoriesLinks.on('mouseleave', (e)=>{
            if($(e.currentTarget).hasClass('category_link__inactive')){
                return;
            }
            this.categoryLinks.removeClass('active').find('.megamenu__subgroups_container').hide();
        })

        this.subgroupLinks.on('mouseenter', (e)=>{
            let target = e.currentTarget.dataset.target;
            if($(e.currentTarget).hasClass('subgroup_link__container__inactive')){
                return;
            }
            if($('#' + target).length > 0){
                this.subgroupLinks.removeClass('active');
                $('.subgroups_links__tab').hide();

                $(e.currentTarget).addClass('active');
                $('#' + target).addClass('active').show();
            }
            
        })
        
        this.megamenu__subgroups_container.on('mouseleave', (e)=>{
            this.subgroupLinks.removeClass('active');
            $('.subgroups_links__tab').hide();
        })

        $('.category_link__inactive').on('click', (e)=>{
            e.preventDefault();
        });
        $('.subgroup_link__container__inactive').on('click', (e)=>{
            e.preventDefault();
        })
        $('.sub_subgroup_link__container__inactive').on('click', (e)=>{
            e.preventDefault();
        })
    }
}

export default TopNav;
