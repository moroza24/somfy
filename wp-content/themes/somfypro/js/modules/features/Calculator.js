import $ from 'jquery';

class Calculator {

    constructor() {
        // calculator popup controls
        this.calculator_trigger = $('.calculator_trigger ');
        this.calculators_popup = $('#calculators_popup');
        this.calculator_popup__close = $('#calculators_popup .popup__overlay__close');
        this.calculator__title = $('#calculator__title');

        // calculator selector
        this.calculators_selector__container = $('#calculators_selector__container');
        this.calculators_selector__trigger = $('.calculators_selector__trigger');
        
        // calculator step controls
        this.calculators_steps__container = $('#calculators_steps__container');
        this.next_step = $('.next_step');
        this.prev_step = $('.prev_step');


        // calc 1 - temp data holder
        this.calc1_blindWeightsData;
        this.calc1_pipeDiameterData;

        // calc 1 - process elements
        this.calc_1__loader = $('#calc_1__loader');
        this.calc_1__error = $('#calc_1__error');

        // calc 1 - data matrix
        this.calc1_blined_matrix;


        // calc 2 - data matrix
        this.calc2__matrix;

        // calc 2 - process elements
        this.calc_2__loader = $('#calc_2__loader');
        this.calc_2__error = $('#calc_2__error');


        // calculator user inputs and filters objects
        this.CalcInputs = {};

        this.finalFilters = {};
        this.userInputs = {};


        this.initEvents();
    }

    initEvents() {
        // calculator popup controls
        this.calculator_trigger.on('click', (e)=>{
            this.showCalculatorPopup(e);
        })

        this.calculator_popup__close.on('click', (e)=>{
            this.hideCalculatorPopup();
        });

        // calculator selector
        this.calculators_selector__trigger.on('click', (e)=>{
            let target = e.currentTarget.dataset.calculator;
            let calculator_name = $(e.currentTarget).text();
            this.showCalculator(target, calculator_name);

            this.CalcInputs = {};
            this.finalFilters = {};
            this.userInputs = {};
        });


        // calculator step controls
        this.calculators_steps__container.find('form').on('submit', (e)=>{
            e.preventDefault();
        });

        // next prev btn
        this.next_step.on('click', (e)=>{
            this.nextStep(e);
        });

        this.prev_step.on('click', (e)=>{
            this.prevStep(e);
        });


        // Calc 1 - calculate blinds step weight 
        $('#calc_1__blind_type').on('change', (e)=>{
            this.calc1_filterBlindHeights();
            this.calc1_calculateWeight();
        })

        $('#calc_1__blind_height').on('change', (e)=>{
            this.calc1_calculateWeight();
        })

        // Calc 1 - Submit
        $('.submit_calc[data-calculator="1"]').on('click', (e)=>{
            this.calc1_submitCalc(e);
        });


        // Calc 2 - filter torque options
        $('#calc_2__screen_opening').on('change', (e)=>{
            this.calc2_filterPipeDiameter();
            this.calc2_filter_step1_inputs('screen_opening');
        });

        $('#calc_2__screen_width').on('change', (e)=>{
            this.calc2_filterPipeDiameter();
            this.calc2_filter_step1_inputs('screen_width');
        })

        $('#calc_2__arms').on('change', (e)=>{
            this.calc2_filterPipeDiameter();
        })

        // Calc 2 - Submit
        $('.submit_calc[data-calculator="2"]').on('click', (e)=>{
            this.calc2_submitCalc(e);
        });
    }


    /**
     * Calc 1 
     */
    // calc weight
    calc1_calculateWeight() {
        let selectedBlindHeight = $('#calc_1__blind_height option:selected');
        let windowHeight = parseInt($('#calc_1__window_height').val());
        let windowWidth = parseInt($('#calc_1__window_width').val());

        // round windowWidth to 100
        let hDef = windowHeight % 200 < 100 ? (0 - windowHeight % 200) : (200 - windowHeight % 200);
        let wDef = windowWidth % 100 < 50 ? (0 - windowWidth % 100) : (100 - windowWidth % 100);

        windowWidth = windowWidth + wDef;
        windowHeight = windowHeight + hDef;
        
        // round windowHeight to 200
        if(selectedBlindHeight.length == 1){
            let mWeight = selectedBlindHeight[0].dataset.weight;
            console.log({mWeight});

            let totalWeight = ((windowHeight * windowWidth * mWeight) / 1000000).toFixed(2);
            console.log({totalWeight});

            $('#calc_1__blinds_mr_weight').val(mWeight);
            $('#calc_1__blinds_mr_weight__title').text(mWeight);
            $('#calc_1__blinds_weight').val(totalWeight);
            $('#calc_1__blinds_weight__title').text(totalWeight);

            this.CalcInputs = {
                'windowHeight' : windowHeight,
                'windowWidth' : windowWidth,
                'selectedBlindHeight' : Object.assign({blind_height : $(selectedBlindHeight).val()}, selectedBlindHeight[0].dataset),
                'mWeight' : mWeight,
                'totalWeight' : totalWeight
            }
            this.calc1_filterPipeDiameter();
        } else {
            $('#calc_1__blinds_mr_weight').val('');
            $('#calc_1__blinds_weight').val('');
            this.CalcInputs = {};
            this.finalFilters = {};
            this.userInputs = {};
        }
    }

    // calc1 filter BlindHeights input
    calc1_filterBlindHeights(){
        let selectedBlindType = $('#calc_1__blind_type option:selected').val();
        $('#calc_1__blind_height').html('');
        let items = '<option value="-1">גובה שלב</option>';

        for(let i = 0; i < this.calc1_blindWeightsData.length; i++) {
            let op = this.calc1_blindWeightsData[i];
            if(op.typecode == selectedBlindType){
                items += `
                <option value='${op.height}' 
                    data-weight='${op.kgm2}' 
                    data-typecode='${op.typecode}' 
                    data-lighning='${op.lighning}' 
                    data-forced='${op.forced}'
                    data-category='${op.category}'>${op.title}</option>`;
            }
        }
        $('#calc_1__blind_height').html(items);
    }


    // calc1 filter PipeDiameter input
    calc1_filterPipeDiameter(){
        $('#calc_1__pipe_diameter').html('');

        $.getJSON( somfyData.template_url + "/assets/calc_json/calc1_pipe_diameter.json", (data) => {
            $('#calc_1__pipe_diameter').html('<option value="-1">קוטר צינור</option>');
            for(let i = 0; i < data.length; i++) {
                let op = data[i];
                
                if(op.typecode == this.CalcInputs.selectedBlindHeight.typecode 
                    && op.type.toLowerCase() == this.CalcInputs.selectedBlindHeight.category.toLowerCase()
                    && op.forced.toString() == this.CalcInputs.selectedBlindHeight.forced.toString()
                    && op.lighting.toString() == this.CalcInputs.selectedBlindHeight.lighning.toString()
                    && parseInt(op.shelf) == parseInt(this.CalcInputs.selectedBlindHeight.blind_height)
                    && parseInt(op.height) == parseInt(this.CalcInputs.windowHeight)){
                    
                    // add matching options to dom
                    $('#calc_1__pipe_diameter').append( `
                        <option value='${op.diampipe}' 
                            data-diamtotal='${op.diamtotal}' 
                            data-diamcode='${op.diamcode}' 
                            data-height='${op.height}' 
                            data-lighning='${op.lighting}' 
                            data-forced='${op.forced}'
                            data-type='${op.type}'
                            data-typecode='${op.typecode}'
                            data-shelf='${op.shelf}'>${op.diampipe}</option>`);
                }
            }
        });
        
    }


    // calc1 get NameQuery results from matrix
    calc1_getNameQuery(){
        let results = {
            name: [],
            torque: []
        };

        // loop matrix get the - torque and names for query
        for(let i = 0; i < this.calc1_blined_matrix.length; i++) {
            let op = this.calc1_blined_matrix[i];
            let opTech = op.wired ? "4" : "2";

            if(op.shelftype.toLowerCase() == this.CalcInputs.selectedBlindHeight.category.toLowerCase() 
                && parseInt(op.shelfheight) == parseInt(this.CalcInputs.selectedBlindHeight.blind_height)
                && parseInt(this.CalcInputs.windowHeight) > parseInt(op.windowheightmin)
                && parseInt(this.CalcInputs.windowHeight) <= parseInt(op.windowheightmax)
                && parseInt(this.CalcInputs.totalWeight) > parseInt(op.totalweightmin)
                && parseInt(this.CalcInputs.totalWeight) <= parseInt(op.totalweightmax)
                && this.CalcInputs.pipeDiameter.includes(`${op.pipediamter}`)
                && this.CalcInputs.productFilters.tech.includes(opTech)){
                    results.name.push(op.name);
                    if(op.torque) results.torque.push(op.torque);
            }
        }
        this.CalcInputs.nameQuery = [...new Set(results.name)];
        this.CalcInputs.torque = [...new Set(results.torque)];

        return results.name.length > 0 ? true : false;;
    }


    // Calc 1 - Submit
    calc1_submitCalc(e){
        // show loader
        this.calc_1__error.hide().html('');
        this.calc_1__loader.fadeIn('fast');

        
        // validate last step form params
        let errors = 0;
        let techValues = [];
        let techTexts = [];
        let rpmText = '';

        let selectedPipeDiameter = $('#calc_1__pipe_diameter option:selected');
        let tech = $('input[name="calc_1__tech"]:checked');
        let backup = $('#calc_1__pipe_backup:checked');
        let rpm = $('#calc_1__rpm option:selected').val();

        if(!$(selectedPipeDiameter).val() || $(selectedPipeDiameter).val() == -1){
            $('p.form_error[data-for="calc_1__pipe_diameter"]').css('visibility', 'visible');
            errors++;
        } else {
            $('p.form_error[data-for="calc_1__pipe_diameter"]').css('visibility', 'hidden');
        }

        if(tech.length == 0){
            $('p.form_error[data-for="calc_1__tech"]').css('visibility', 'visible');
            errors++;
        } else {
            $('p.form_error[data-for="calc_1__tech"]').css('visibility', 'hidden');
            for(let i = 0; i < tech.length; i++){
                techValues.push($(tech[i]).val());
                techTexts.push($(tech[i]).attr('data-title'));
            }
        }

        if(!rpm || rpm == -1){
            $('p.form_error[data-for="calc_1__rpm"]').css('visibility', 'visible');
            errors++;
        } else {
            rpmText = $('#calc_1__rpm option:selected').text();;
            $('p.form_error[data-for="calc_1__rpm"]').css('visibility', 'hidden');
        }

        if(errors > 0) {
            this.finalFilters = {};
            this.userInputs = {};

            localStorage.removeItem('pro_calc_userInputs');
            localStorage.removeItem('pro_calc_finalFilters');

            // hide loader
            this.calc_1__loader.fadeOut('fast');
            return;
        }
        console.log(backup);

        // set pipeDiameter values & product Filters
        this.CalcInputs.selectedPipeDiameter = Object.assign({diampipe : $(selectedPipeDiameter).val()}, selectedPipeDiameter[0].dataset);
        this.CalcInputs.pipeDiameter = [this.CalcInputs.selectedPipeDiameter.diampipe];
        this.CalcInputs.pipeDiameterCode = [this.CalcInputs.selectedPipeDiameter.diamcode];
        this.CalcInputs.productFilters = {
            tech: techValues,
            backup: backup.length > 0 ? true : false,
            rpm: rpm
        };

        // set the nameQuery and torque from the calc_1 matrix
        let nameQueryFound = this.calc1_getNameQuery();
        console.log({nameQueryFound});
        if(nameQueryFound == false){
            this.finalFilters = {};
            this.userInputs = {};
            
            localStorage.removeItem('pro_calc_userInputs');
            localStorage.removeItem('pro_calc_finalFilters');

            // show not found error
            this.calc_1__error.html('לא נמצאו תוצאות').slideDown('fast');

            // hide loader
            this.calc_1__loader.fadeOut('fast');

            return;
        }

        // save user inputs for log and adding to order confirmation
        this.userInputs = {
            'clac' : 1,
            'windowHeight' : this.CalcInputs.windowHeight,
            'windowWidth' :this.CalcInputs.windowWidth,
            'blind_height' : $('#calc_1__blind_height option:selected').text(),
            'blind_type' : $('#calc_1__blind_type option:selected').text(),
            'tech' : techValues,
            'techTexts' : techTexts.toString(),
            'backup': this.CalcInputs.productFilters.backup,
            'mWeight': this.CalcInputs.mWeight,
            'totalWeight': this.CalcInputs.totalWeight,
            'rpm': rpmText,
            'pipeDiameter' : this.CalcInputs.pipeDiameter
        }

        // create final filters obj
        this.finalFilters = {
            'category' : ['1279'], //תריסי חשמליים
            'subgroup' : ['1291'], // תריסי גלילה
            'subsubgroup' : ['3444', '3443'], // מנועה מחווט RTS/WT
            'tech' : techValues,
            'pipeDiameter' : this.CalcInputs.pipeDiameter,
            'pipeDiameterCode' : this.CalcInputs.pipeDiameterCode,
            'nameQuery' : this.CalcInputs.nameQuery,
            'torque' : this.CalcInputs.torque,
            'backup': this.CalcInputs.productFilters.backup,
            'rpm': this.CalcInputs.productFilters.rpm
        }

        // save to localStorage
        localStorage.setItem('pro_calc_userInputs', JSON.stringify(this.userInputs));
        localStorage.setItem('pro_calc_finalFilters', JSON.stringify(this.finalFilters));

        // build url params
        let encodedParams = this.encodeData({'calculator' : true, 'category' : ['1279'], 'subgroup' : ['1291']});
        let redirectUrl = somfyData.root_url + "/shop?" + encodedParams;

        // redirect to shop with filters
        window.location.href = redirectUrl;
    }


    /**
     * Calc 2
     */
    calc2_filter_step1_inputs(changedInput){
        let selectedOpening = parseInt($('#calc_2__screen_opening option:selected').val());
        let selectedWidth = parseInt($('#calc_2__screen_width option:selected').val());
        let res = [];
        let targetInput;

        console.log({changedInput});

        if(selectedOpening && selectedOpening != "-1" && changedInput == 'screen_opening'){
            targetInput = 'calc_2__screen_width';
            for(let i = 0; i < this.calc2__matrix.length; i++) {
                let op = this.calc2__matrix[i];
                if(selectedOpening == parseInt(op.openingcode)){
                    if(!res.includes(op.widthcode)) res.push(op.widthcode);
                    console.log(op);
                }
            }
        } else if(selectedWidth && selectedWidth != "-1" && selectedOpening && selectedOpening != "-1" && changedInput == 'screen_width'){
            targetInput = 'calc_2__arms';
            for(let i = 0; i < this.calc2__matrix.length; i++) {
                let op = this.calc2__matrix[i];
                if(selectedOpening == parseInt(op.openingcode) && selectedWidth == parseInt(op.widthcode)){
                    console.log(op);
                    if(!res.includes(op.arms)) res.push(op.arms);
                }
            }   
        }

        if(res.length > 0){
            $(`#${targetInput} option`).hide();
            $(`#${targetInput} option[value="-1"]`).show();
            res.forEach(r => {
                $(`#${targetInput} option[value="${r}"]`).show();
            });
            $(`#${targetInput}`).prop('selectedIndex', 0);

            if(changedInput == 'screen_opening') {
                $(`#calc_2__arms option`).hide();
                $(`#calc_2__arms option[value="-1"]`).show();
                $(`#calc_2__arms`).prop('selectedIndex', 0);
            }
        }
    }

    // Filter torque options
    calc2_filterPipeDiameter(){
        let selectedOpening = parseInt($('#calc_2__screen_opening option:selected').val());
        let selectedWidth = parseInt($('#calc_2__screen_width option:selected').val());
        let selectedArms = parseInt($('#calc_2__arms option:selected').text());
        
        if(!selectedOpening || selectedOpening == "-1"
            || !selectedWidth || selectedWidth == "-1"
            || !selectedArms || selectedArms == "-1"){

                this.CalcInputs.selectedOpening = '';
                this.CalcInputs.selectedWidth = '';
                this.CalcInputs.selectedArms = '';
                this.CalcInputs.selectedOpeningText = '';
                this.CalcInputs.selectedWidthText = '';
                this.CalcInputs.selectedArmsText = '';

                return;
            } 

            
        this.CalcInputs.selectedOpening = selectedOpening;
        this.CalcInputs.selectedWidth = selectedWidth;
        this.CalcInputs.selectedArms = selectedArms;
        this.CalcInputs.selectedOpeningText = $('#calc_2__screen_opening option:selected').text();
        this.CalcInputs.selectedWidthText = $('#calc_2__screen_width option:selected').text();
        this.CalcInputs.selectedArmsText = $('#calc_2__arms option:selected').text();

        $('#calc_2__pipe_diameter').html('');
        let items = '<option value="-1">קוטר צינור</option>';

        let pipeDiameterOptions = [];

        for(let i = 0; i < this.calc2__matrix.length; i++) {
            let op = this.calc2__matrix[i];
            if(parseInt(selectedOpening) == parseInt(op.openingcode)
                && parseInt(selectedWidth) == parseInt(op.widthcode)
                && parseInt(selectedArms) == parseInt(op.arms) ){
                    if(!pipeDiameterOptions.includes(op.pipediamter)){
                        pipeDiameterOptions.push(op.pipediamter);
                        console.log(op.pipediamter);
                        items += `<option value='${op.pipediamter}'
                            data-internalid='${op.pipediamterinternalid}'>${op.pipediamter}</option>`;
                    } 
                }
        }

        if(pipeDiameterOptions.length > 0){
            $('.next_step[data-calculator="2"]').prop('disabled', false);
            $('#no_matching_pipes_error').hide();
            $('#calc_2__pipe_diameter').html(items);
        } else {
            // show no matching results found
            $('#no_matching_pipes_error').show();
            $('.next_step[data-calculator="2"]').prop('disabled', true);
        }
    }

    // calc2 get NameQuery results from matrix
    calc2_getNameQuery(){
        let results = {
            name: [],
            torque: []
        };

        this.CalcInputs.nameQuery = [];
        this.CalcInputs.torque = [];

        console.log(this.CalcInputs);

        // loop matrix get the - torque and names for query
        for(let i = 0; i < this.calc2__matrix.length; i++) {
            let op = this.calc2__matrix[i];
            let opTech = op.wired ? "4" : "2";

            if(parseInt(op.openingcode) == parseInt(this.CalcInputs.selectedOpening)
                && parseInt(op.arms) == parseInt(this.CalcInputs.selectedArms)
                && parseInt(op.widthcode) == parseInt(this.CalcInputs.selectedWidth)
                && op.csi == this.CalcInputs.productFilters.backup
                && this.CalcInputs.pipeDiameter.includes(op.pipediamter)
                && this.CalcInputs.productFilters.tech.includes(opTech)){
                    results.name.push(op.name);
                    if(op.torque) results.torque.push(op.torque);
            }
        }
        this.CalcInputs.nameQuery = [...new Set(results.name)];
        this.CalcInputs.torque = [...new Set(results.torque)];

        return results.name.length > 0 ? true : false;;
    }

    // Calc 2 - Submit
    calc2_submitCalc(e){
        // show loader
        this.calc_2__error.hide().html('');
        this.calc_2__loader.fadeIn('fast');

        
        // validate last step form params
        let errors = 0;
        let techValues = [];
        let techTexts = [];
        let selectedPipeDiameter = $('#calc_2__pipe_diameter option:selected');
        let tech = $('input[name="calc_2__tech"]:checked');
        let backup = $('#calc_2__pipe_backup:checked');

        if(!$(selectedPipeDiameter).val() || $(selectedPipeDiameter).val() == -1){
            $('p.form_error[data-for="calc_2__pipe_diameter"]').css('visibility', 'visible');
            errors++;
        } else {
            $('p.form_error[data-for="calc_2__pipe_diameter"]').css('visibility', 'hidden');
        }

        if(tech.length == 0){
            $('p.form_error[data-for="calc_2__tech"]').css('visibility', 'visible');
            errors++;
        } else {
            $('p.form_error[data-for="calc_2__tech"]').css('visibility', 'hidden');
            for(let i = 0; i < tech.length; i++){
                techValues.push($(tech[i]).val());
                techTexts.push($(tech[i]).attr('data-title'));
            }
        }

        backup = backup && backup.length > 0 ? true : false;
        
        if(errors > 0) {
            this.finalFilters = {};
            this.userInputs = {};

            localStorage.removeItem('pro_calc_userInputs');
            localStorage.removeItem('pro_calc_finalFilters');

            // hide loader
            this.calc_2__loader.fadeOut('fast');
            return;
        }
        
        // set pipeDiameter values & product Filters
        this.CalcInputs.selectedPipeDiameter = Object.assign({diampipe : $(selectedPipeDiameter).val()}, selectedPipeDiameter[0].dataset);
        this.CalcInputs.pipeDiameter = [parseInt(this.CalcInputs.selectedPipeDiameter.diampipe)];
        this.CalcInputs.productFilters = {
            tech: techValues,
            backup: backup ? true : false
        };

        // set the nameQuery and torque from the calc_1 matrix
        let nameQueryFound = this.calc2_getNameQuery();
        if(nameQueryFound == false){
            this.finalFilters = {};
            this.userInputs = {};
            
            localStorage.removeItem('pro_calc_userInputs');
            localStorage.removeItem('pro_calc_finalFilters');

            // show not found error
            this.calc_2__error.html('לא נמצאו תוצאות').slideDown('fast');

            // hide loader
            this.calc_2__loader.fadeOut('fast');

            return;
        }

        // save user inputs for log and adding to order confirmation
        this.userInputs = {
            'clac' : 2,
            'arms' : this.CalcInputs.selectedArmsText,
            'opening' :this.CalcInputs.selectedOpeningText,
            'width' :this.CalcInputs.selectedWidthText,
            'tech' : techValues,
            'techTexts' : techTexts.toString(),
            'backup': this.CalcInputs.productFilters.backup,
            'pipeDiameter' : this.CalcInputs.pipeDiameter
        }

        // // create final filters obj
        this.finalFilters = {
            'category' : ['1278'], // פתרונות הצללה
            'subgroup' : ['1289'], // סוככים ומרקיזה
            'subsubgroup' : ['3444', '3443'], // מנועה מחווט RTS/WT
            'tech' : techValues,
            'pipeDiameterCode' : [this.CalcInputs.selectedPipeDiameter.internalid],
            'nameQuery' : this.CalcInputs.nameQuery,
            'torque' : this.CalcInputs.torque,
            'backup': this.CalcInputs.productFilters.backup
        }

        // save to localStorage
        localStorage.setItem('pro_calc_userInputs', JSON.stringify(this.userInputs));
        localStorage.setItem('pro_calc_finalFilters', JSON.stringify(this.finalFilters));

        // build url params
        let encodedParams = this.encodeData({'calculator' : true, 'category' : ['1278'], 'subgroup' : ['1289']});
        let redirectUrl = somfyData.root_url + "/shop?" + encodedParams;

        // redirect to shop with filters
        window.location.href = redirectUrl;
    }


    // calculator popup controls
    showCalculatorPopup(e){
        let calculator = e.currentTarget.dataset.calculator;
        let calculatorTitle = e.currentTarget.dataset.calculatortitle;

        if(!calculator){
            this.calculator__title.html('בחר מחשבון');
            this.calculators_selector__container.show();
            this.calculators_steps__container.hide();
        } else {
            this.calculator__title.html(calculatorTitle);
            this.calculators_selector__container.hide();
            this.calculators_steps__container.show();
        }

        $('.step__form_container').addClass('hidden');
        $('.form_error').css('visibility', 'hidden');

        let forms = $('.step__form_container');
        for(let i = 0; i < forms.length; i++){
            $(forms[i]).trigger('reset');
        };

        this.calculators_popup.fadeIn('fast');
    }
    
    hideCalculatorPopup(){
        this.calculators_popup.hide();
        this.calculators_selector__container.hide();
        this.calculator__title.html('');
    }


    // calculator selector
    showCalculator(target, calculator_name) {
        this.calculator__title.html(calculator_name);

        if(target == 1){
            this.clac1_preLoadData();
        } else if(target == 2){
            this.clac2_preLoadData();
        }

        this.calculators_selector__container.hide();
        $('.calculator__container').hide();
        $('#calculator_'+target).show();
        $('#calculator_'+target).find('.step__form_container[data-step="1"]').removeClass('hidden');
        
        this.calculators_steps__container.show();
    }

    
    // calculator step controls
    nextStep(e){
        let formElm = $(e.currentTarget).closest('form.step__form_container')[0];
        let isValid = this.validateStepFrom(formElm);
        let next_step = e.currentTarget.dataset.next;
        let calculator = e.currentTarget.dataset.calculator;

        if(isValid){
            $(formElm).addClass('hidden');
            $(`#calculator_${calculator}`).find(`.step__form_container[data-step="${next_step}"`).removeClass('hidden');
        }
    }

    prevStep(e){
        let formElm = $(e.currentTarget).closest('form.step__form_container')[0];
        let prev_step = e.currentTarget.dataset.prev;
        let calculator = e.currentTarget.dataset.calculator;

        $(formElm).addClass('hidden');
        $(`#calculator_${calculator}`).find(`.step__form_container[data-step="${prev_step}"`).removeClass('hidden');
    }


    // step form validation
    validateStepFrom(formElm){
        let formInputElms = formElm.elements;
        let errors = 0;

        for(let i = 0; i < formElm.elements.length; i++){
            let inp = $(formElm.elements[i]);
            let inpId = inp[0].id;
            
            if(inp.is('input[type="number"')){
                if(inp[0].validity.valid){
                    $('p.form_error[data-for="'+inpId+'"]').css('visibility', 'hidden');
                } else {
                    $('p.form_error[data-for="'+inpId+'"]').css('visibility', 'visible');
                    errors++;
                }
            } else if(inp.is('select')){
                let selected = $(inp).find('option:selected').val();
                
                if(selected == "-1"){
                    $('p.form_error[data-for="'+inpId+'"]').css('visibility', 'visible');
                    errors++;
                } else {
                    $('p.form_error[data-for="'+inpId+'"]').css('visibility', 'hidden');
                }
            }
        }

        return errors > 0 ? false : true;
    }


    /** preload data */
    // Calc 1
    clac1_preLoadData(){
        // preload calc1 blind weight data
        $.getJSON( somfyData.template_url + "/assets/calc_json/calc1_bline_weights.json", (data) => {
            this.calc1_blindWeightsData = data;
        });

        // preload calc1 matrix
        $.getJSON( somfyData.template_url + "/assets/calc_json/calc1_blined_matrix.json", (data) => {
            this.calc1_blined_matrix = data;
        });
    }


     // Calc 2
     clac2_preLoadData(){
        // preload calc2 matrix
        $.getJSON( somfyData.template_url + "/assets/calc_json/calc2__matrix.json", (data) => {
            this.calc2__matrix = data;
        });
    }


    // Helper functions
    encodeData(data) {
        return Object.keys(data).map(function(key) {
            return [key, data[key]].map(encodeURIComponent).join("=");
        }).join("&");
    }  
}
export default Calculator;
