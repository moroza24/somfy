import $ from 'jquery';
import AutoLoadProducts from './AutoLoadProducts';
import AutoLoadInv from './AutoLoadInv';

class RMA {

    constructor() {
        this.autoLoadProducts = new AutoLoadProducts();
        this.autoLoadInv = new AutoLoadInv();

        this.rma_form_trigger = $('#rma_form_trigger');
        this.rma__form_container = $('#rma__form_container');

        // item form
        this.new_rma_item__form = $('#new_rma_item__form');
        this.new_rma_item__form_submit = $('#new_rma_item__form_submit');

        // added items table
        this.added_rma_items__table = $('#added_rma_items__table');
        this.added_rma_items__tbody = $('tbody[data-for="added_rma_items__table"]');

        // rma submit
        this.rma_form_submit = $('#rma_form_submit');
        this.rma_form_submit__loader = $('#rma_form_submit__loader');
        this.rma_form_submit__success = $('#rma_form_submit__success');
        this.rma_form_submit__error = $('#rma_form_submit__error');

        this.initEvents();
    }

    initEvents() {
        this.rma_form_trigger.on('click', (e)=>{
            $(e.currentTarget).slideUp();
            this.rma_form_submit__success.hide();
            this.rma__form_container.slideToggle();
        });


        this.new_rma_item__form.on('submit', (e)=>{
            e.preventDefault();
            this.validateItemForm();
        });

        this.new_rma_item__form_submit.on('click', (e)=>{
            e.preventDefault();
            this.validateItemForm();
        });

        $('#new_rma_item__amount').on('change', (e)=>{
            if(e.currentTarget.value > 0){
                $('#new_rma_item__amount_2_pick').attr('max', e.currentTarget.value);
            }
        })

        this.rma_form_submit.on('click', (e)=>{
            this.sendRMAform();
        })
    }


    sendRMAform(){
        let userData = this.getUserDetailes();
        let rmaItems = this.getRMAItems();
        this.rma_form_submit__loader.show();
        $.ajax({
            url : somfyData.ajax_url,
            type : 'POST',
            data : {
                action : 'somfy_pro_new_rma',
                userData: userData,
                rmaItems: rmaItems,
                nonce: somfyData.ajax_nonce
            },
            dataType: "json",
            success: (response) =>{
                if(response.status){
                    this.rma__form_container.slideUp();
                    this.rma_form_submit__success.fadeIn();
                    let closeForm = setTimeout(this.resetRMAForm.bind(this), 2000); 
                } else {
                    this.rma_form_submit__error.html(response.message).fadeIn();
                }
                this.rma_form_submit__loader.hide();
                this.rma_form_trigger.show();
            },
            error:  (jqXHR, status) => {
                console.log(jqXHR);
                console.log(status);
            }
        });
    }

    getRMAItems(){
        let rows = this.added_rma_items__tbody.find('tr');
        let rmaItemsArr = [];

        for(let r of rows){
            let cells = r.cells;
            let rmaItem = {};

            for(let c of cells){
                let key = c.dataset.key;
                
                if(key){
                    if(key == 'type'){
                        if(c.innerHTML == 'א'){
                            rmaItem[key] = 1;
                        } else if(c.innerHTML == 'ב'){
                            rmaItem[key] = 2;
                        } else if(c.innerHTML == 'ג'){
                            rmaItem[key] = 3;
                        }
                    } else {
                        rmaItem[key] = c.innerHTML;
                    }
                }
            }

            rmaItemsArr.push(rmaItem);
        }
        return rmaItemsArr;
    }


    getUserDetailes(){
        let errors = false;
        // validate user details
        let vatregnumber = $('#customer_details__vatregnumber').val();
        let companyname = $('#customer_details__companyname').val();
        let contactname = $('#customer_details__contactname').val();
        let email = $('#customer_details__email').val();
        let phone = $('#customer_details__phone').val();

        var regex = /^0(5[^7]|[2-4]|[8-9]|7[0-9])[0-9]{7}$/;
        if(phone.length < 3 || phone == '' || !regex.test(phone.replace("-", ""))){
            $('p.form_error[data-input="customer_details__phone"]').show();
            errors = true;
        }else {
            $('p.form_error[data-input="customer_details__phone"]').hide();
        }

        if(contactname.length < 3 || contactname == ''){
            $('p.form_error[data-input="customer_details__contactname"]').show();
            errors = true;
        }else {
            $('p.form_error[data-input="customer_details__contactname"]').hide();
        }

        if(email.length < 3 || email == ''){
            $('p.form_error[data-input="customer_details__email"]').show();
            errors = true;
        }else {
            $('p.form_error[data-input="customer_details__email"]').hide();
        }

        if(errors) {
            $('html, body').animate({
                scrollTop: $(".rma_form__customer_details").offset().top - 400
            }, 250);
            return false;
        }

        return {
            vatregnumber : vatregnumber,
            companyname : companyname,
            contactname : contactname,
            email : email,
            phone : phone
        }
    }

    validateItemForm(){
        let errors = false;

        let title = $('#new_rma_item__title').val();
        let itemid = $('#new_rma_item__itemid').val();
        let productid = $('#new_rma_item__itemid').attr('data-product');
        let productInternalid = $('#new_rma_item__itemid').attr('data-internalid');
        let amount = $('#new_rma_item__amount').val();
        let type_option = $('input[name="new_rma_item__type_option"]:checked').val();
        let inv = $('#new_rma_item__inv').val();
        let invTranid = $('#new_rma_item__inv').attr('data-tranid');
        let invInternalid = $('#new_rma_item__inv').attr('data-internalid');
        let amount_2_pick = $('#new_rma_item__amount_2_pick').val();
        let description = $('#new_rma_item__description').val();

        if(!itemid || itemid.length <= 3 || !productid || productid.length <= 3 || !productInternalid || productInternalid.length < 3){
            errors = true;
            $('p.form_error[data-input="new_rma_item__itemid"]').show();
        } else {
            $('p.form_error[data-input="new_rma_item__itemid"]').hide();
        }

        if(!amount || amount == 0){
            errors = true;
            $('p.form_error[data-input="new_rma_item__amount"]').show();
        } else {
            $('p.form_error[data-input="new_rma_item__amount"]').hide();
        }

        if(!amount_2_pick || amount_2_pick == 0){
            errors = true;
            $('p.form_error[data-input="new_rma_item__amount_2_pick"]').show();
        } else {
            $('p.form_error[data-input="new_rma_item__amount_2_pick"]').hide();
        }

        if(!description || description.length <= 3){
            errors = true;
            $('p.form_error[data-input="new_rma_item__description"]').show();
        } else {
            $('p.form_error[data-input="new_rma_item__description"]').hide();
        }

        if(type_option != "rma_type1" && (!invTranid || invTranid.length <= 3)  && (!invInternalid || invInternalid.length <= 3)){
            errors = true;
            $('p.form_error[data-input="new_rma_item__inv"]').show();
        } else {
            $('p.form_error[data-input="new_rma_item__inv"]').hide();
        }
        if(errors) return;

        this.addItemToTable(itemid,
            productid,
            productInternalid,
            title,
            amount,
            amount_2_pick,
            inv,
            invInternalid,
            type_option,
            description);

    }

    addItemToTable(itemid, productid, productInternalid, title, amount, amount_2_pick, inv, invInternalid, type_option, description){
        let type;
        if(type_option == 'rma_type1') {
            type = 'א';
        } else if(type_option == 'rma_type2') {
            type = 'ב';
        } else if(type_option == 'rma_type3') {
            type = 'ג';
        }

        this.added_rma_items__tbody.append(`
        <tr class='added_rma_line'>
            <td style="display: none;" data-key='productid'>${productid}</td>
            <td style="display: none;" data-key='product_internalid'>${productInternalid}</td>
            <td data-key='itemid'>${itemid}</td>
            <td data-key='title'>${title}</td>
            <td data-key='amount'>${amount}</td>
            <td data-key='amount_2_pick'>${amount_2_pick}</td>
            <td data-key='inv'>${inv}</td>
            <td data-key='invInternalid' style='display: none'>${invInternalid}</td>
            <td data-key='type'>${type}</td>
            <td data-key='description'>${description}</td>
            <td>
                <a class='remove_rma_item' alt='מחק שורה'><i class="fas fa-plus"></i></a>
            </td>
        </tr>`);

        this.added_rma_items__table.show();
        $('#new_rma_item__itemid').attr({'data-product': '', 
            'data-title': '', 
            'data-internalid': '', 
            'data-item': ''});

        $('#new_rma_item__inv').attr({'data-tranid': '', 
            'data-internalid': ''});
        
        $('#new_rma_item__form')[0].reset();
        this.rma_form_submit.prop('disabled', false);

        this.initRemoveItemEvent();
    }
    
    initRemoveItemEvent(){
        let remove_rma_item = $('.remove_rma_item');

        remove_rma_item.off('click').on('click', (e)=>{
            $(e.currentTarget).closest('.added_rma_line').remove();

            if($('.added_rma_line').length == 0){
                this.added_rma_items__table.hide();
                this.rma_form_submit.prop('disabled', true);
            }
        })
    }

    resetRMAForm(){
        this.added_rma_items__tbody.html('');
        $('#new_rma_item__itemid').attr({'data-product': '', 
            'data-title': '', 
            'data-internalid': '', 
            'data-item': ''});

        $('#new_rma_item__inv').attr({'data-tranid': '', 
            'data-internalid': ''});
            
        $('#new_rma_item__form')[0].reset();
    }
}




export default RMA;
    