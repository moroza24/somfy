import $ from 'jquery';

class AutoLoadInv {

    constructor() {
        this.initAutoloader();
        this.previousValue = '';
        this.isLoading = false;
        this.currentInput;
    }

    initAutoloader(){
        let autoloadInputs = $('input.inv-auto[data-autoload="true"]');

        // Attach loader, and resultsbox
        autoloadInputs.each(function(){
            $(`<div class="loader"><div class="spinner-loader"></div></div>
                <div class="results_preview"></div>
                `).insertAfter(this);
        })

        autoloadInputs.off('keyup').on('keyup', (e)=>{
            let input = $(e.currentTarget);
            this.currentInput = input;
            this.currentInputID = input[0].id;
            $(input).attr({'data-tranid': ''});
            this.typingLogic();
        });

        $(document).on('click', (e)=>{
            if($(e.currentTarget).hasClass('inv_autoloader') || $(e.currentTarget).hasClass('results_preview')){
                return;
            } 
            $(this.currentInput).siblings('.results_preview').hide().html('');
            this.currentInput = null;
            this.currentInputID = null;
        })
    }

    typingLogic(){
        let inputVal = this.currentInput.val();
        if(inputVal != this.previousValue && inputVal.length >= 3){
            clearTimeout(this.typingTimer);

            if(!this.isLoading){
                $(this.currentInput).siblings('.loader').show();
                this.isLoading = true;  
            } 
            $(this.currentInput).siblings('.results_preview').hide();
            this.typingTimer = setTimeout(this.getResults.bind(this), 1000); 
        } else {
            $(this.currentInput).siblings('.loader').hide();
            this.isLoading = false;
        }
        this.previousValue = this.currentInput.val();
    }

    getResults(){
        let term = this.currentInput.val();

        $.ajax({
            url : somfyData.ajax_url,
            type : 'POST',
            data : {
                action : 'somfy_pro_inv_autoload',
                term : term,
                nonce: somfyData.ajax_nonce
            },
            dataType: "json",
            success: (response) =>{
                if(response.status){
                    let inv_results = response.results;

                    if(inv_results.length == 0){
                        $(this.currentInput).siblings('.results_preview').hide();
                    } else {
                        this.printProductsResults(inv_results);
                        this.initAutoloadOptionEvents();
                        $(this.currentInput).siblings('.results_preview').show();
                    }
                }
                this.isLoading = false;
                $(this.currentInput).siblings('.loader').hide();
            },
            error:  (jqXHR, status) => {
                console.log(jqXHR);
                console.log(status);
                this.isLoading = false;
                $(this.currentInput).siblings('.loader').hide();
            }
          });       
    }

    printProductsResults(invs){
        let resultsHtml = '<ul class="inv_list">';

        invs.forEach(inv => {
            resultsHtml += `<li>
                <a class='autoloader_option' 
                    data-target='${this.currentInputID}' 
                    data-internalid='${inv.internalid}' 
                    data-tranid='${inv.tranid}' alt="${inv.tranid}">
                    ${inv.tranid}
                </a>
            </li>`
        })

        resultsHtml += '</ul>';
        $(this.currentInput).siblings('.results_preview').html(resultsHtml);
    }

    initAutoloadOptionEvents(){
        let autoloader_option = $('.autoloader_option');

        autoloader_option.off('click').on('click', (e)=>{
            let target = e.currentTarget.dataset.target;
            let tranid = e.currentTarget.dataset.tranid;
            let internalid = e.currentTarget.dataset.internalid;

            $('#'+target).val(tranid).attr({'data-tranid': tranid, 'data-internalid': internalid});
            $(this.currentInput).siblings('.results_preview').hide().html('');

            this.currentInput = null;
            this.currentInputID = null;
            this.previousValue = '';
        })
    }
}



export default AutoLoadInv;
    