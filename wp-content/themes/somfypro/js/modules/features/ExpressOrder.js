import $ from 'jquery';
import AutoLoadProducts from './AutoLoadProducts';

class ExpressOrder {

    constructor(cartObj) {
        this.cartObj = cartObj;
        this.express_add_item__form_submit = $('#express_add_item__form_submit');
        this.express_add_item__form = $('#express_add_item__form');
        this.express_add_item__form_error = $('#express_add_item__form_error');
        this.clean_express_items = $('#clean_express_items');
        
        this.add_express_items_to_cart = $('#add_express_items_to_cart');
        this.add_express_order_cart__error = $('#add_express_order_cart__error');

        this.file_loader__overlay = $('.file_loader__overlay');
        this.autoLoadProducts = new AutoLoadProducts();

        this.fileUploadedData;
        this.itemidsArr = [];
        this.initEvents();
    }


    initEvents(){
        this.express_add_item__form.on('submit',(e)=>{
            e.preventDefault();
            this.addNewExpressLine();
        });
        this.express_add_item__form_submit.on('click',(e)=>{
            e.preventDefault();
            this.addNewExpressLine();
        });

        this.clean_express_items.on('click',(e)=>{
            if(confirm("למחוק את כל המוצרים מההזמנה?")){
                $('li.added_item_form').remove();
                $('#file_items_error').html('').hide();
                this.clean_express_items.hide();
            } 
        });

        this.add_express_items_to_cart.on('click',(e)=>{
            this.addExpressItemsToCart();
        })

        this.initFileUplate();
    }


    addExpressItemsToCart(){
        let itemForms = $('li.added_item_form form');

        if(itemForms.length > 0){
            this.add_express_order_cart__error.hide();

            this.cartObj.addToCartPopupLoader.show();
            this.cartObj.addToCartPopupSuccess.hide();
            this.cartObj.addToCartPopup.fadeIn('fast');

            for(let i=0; i<itemForms.length; i++){
                let product = $(itemForms[i]).find('input[name="product_itemid"]').attr('data-product');
                let amount = $(itemForms[i]).find('input[name="product_amount"]').val();

                // add to cart
                this.cartObj.addToCart(product, amount, '', '', false);

                $(itemForms[i]).closest('li.added_item_form').remove();
            }

            this.cartObj.updateCartDB(this.cartObj.cartItems);
            this.cartObj.updateCartPopup();
            this.cartObj.showSuccessAddToCart();
    
            // clear all forms
            // $('li.added_item_form').remove();
            $('#file_items_error').html('').hide();
            this.clean_express_items.hide();

            // scroll to top
            $('html, body').animate({
                scrollTop: $("#top-menu").offset().top - 400
            }, 250);
            window.location.replace(somfyData.root_url + '/checkout');
        } else {
            // show no items entered
            this.add_express_order_cart__error.show();
        }
    }


    initFileUplate(){

        $("#load_express_items_file").on("click", function () {
            $('#fileUpload').trigger('click');
        });

        $('#fileUpload').on('input', ()=>{
            // Show loader
            this.file_loader__overlay.show();
            $('html, body').animate({
                scrollTop: $(".file_loader__overlay").offset().top - 400
            }, 250);

            var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.csv|.txt)$/;
            if (regex.test($("#fileUpload").val().toLowerCase())) {
                if (typeof (FileReader) != "undefined") {
                    var reader = new FileReader();
                    reader.onload = (e)=>{
                        let resultCSVString = e.target.result;
                        let fileUploadedData = this.convertCSVToJSON(resultCSVString, ',');
                        if(fileUploadedData){
                            this.fileUploadedData = fileUploadedData;
                            this.validateAndAddFileData();
                        } else {
                            this.file_loader__overlay.hide();
                            alert("שים לב לקבוע כותרות לעמודות: \n itemid, amount");
                        }
                        
                    }
                    reader.readAsText($("#fileUpload")[0].files[0]);
                } else {
                    this.file_loader__overlay.hide();
                    alert("דפדפן זה לא תומך באפשרות זאת");
                }
            } else {
                this.file_loader__overlay.hide();
                alert("יש להעלות קובץ עם סיומת scv או txt");
            }
        })
    }


    validateAndAddFileData(){
        // validate itemid with backend

        $.ajax({
            url : somfyData.ajax_url,
            type : 'POST',
            data : {
                action : 'somfy_pro_get_product_ids',
                itemidsArr : this.itemidsArr,
                fileUploadedData : this.fileUploadedData,
                nonce: somfyData.ajax_nonce
            },
            dataType: "json",
            success: (response) =>{
                if(response.status){
                    // add lines to preview 
                    let errors = '';
                    for(let i = 0; i < response.finalFileUploadedData.length; i++){
                        let item = response.finalFileUploadedData[i];
                        if(item.product_data && item.file_data.amount && item.file_data.amount != '' && item.file_data.itemid != ''){
                            this.insertNewItemElement(item.file_data.itemid, 
                                item.product_data.ID, 
                                item.file_data.amount,
                                item.product_data.post_title);
                        } else if(item.product_data && ((!item.file_data.amount || item.file_data.amount != '') || item.file_data.itemid != '')){
                            errors += `<p class='file_item_error'>שורה ${i} - מקט: ${item.file_data.itemid}, כמות: ${item.file_data.amount}<p>`
                        }
                    }

                    if(errors != ''){
                        $('#file_items_error').html(errors).show();
                        // scroll to error
                        $('html, body').animate({
                            scrollTop: $("#file_items_error").offset().top - 400
                        }, 250);
                    } else {
                        $('#file_items_error').html('').hide();
                        // scroll to top
                        $('html, body').animate({
                            scrollTop: $("#express_order__container").offset().top - 400
                        }, 250);
                    }
                } else {
                    // show errors
                    $('#file_items_error').html(response.message).show();
                    $('html, body').animate({
                        scrollTop: $("#file_items_error").offset().top - 400
                    }, 250);
                }
                $('#fileUpload').val('');
                this.file_loader__overlay.hide();
            },
            error:  (jqXHR, status) => {
                console.log(jqXHR);
                console.log(status);
                this.file_loader__overlay.hide();
            }
        });

    }


    convertCSVToJSON(str, delimiter = ',') {
        const titles = str.slice(0, str.indexOf('\n')).split(delimiter);
        if(titles[0].trim() != 'itemid' || titles[1].trim() != 'amount'){
            return null
        }

        const rows = str.slice(str.indexOf('\n') + 1).split('\n');
        return rows.map(row => {
            const values = row.split(delimiter);
            if(values[0] != '') this.itemidsArr.push(values[0]);
            
            return titles.reduce((object, curr, i) => (object[curr.trim()] = values[i], object), {})
        });
    };

    addNewExpressLine(){
        // validate form
        let express_add_item__form_item = $('#express_add_item__form_item')[0].dataset.product;
        let express_add_item__form_item_title = $('#express_add_item__form_item')[0].dataset.title;
        let express_add_item__form_itemid = $('#express_add_item__form_item').val();
        let express_add_item__form_amount = $('#express_add_item__form_amount').val();

        let errors = false;
        let errorMsg = '';

        if(express_add_item__form_item == ''){
            errors = true;
            errorMsg = 'יש לבחור מוצר מהרשימה';
        }
        
        if(errors){
            this.express_add_item__form_error.html(errorMsg).show();
            return;
        } else {
            this.express_add_item__form_error.html('').hide();
        }

        this.insertNewItemElement(express_add_item__form_itemid, 
            express_add_item__form_item, 
            express_add_item__form_amount, 
            express_add_item__form_item_title);

        $('#express_add_item__form')[0].reset();
        $('#express_add_item__form_item').attr({'data-product': '', 'data-title': ''});
    }

    insertNewItemElement(itemid, productID, amount, itemTitle = '') {
        let formClone = $('.added_item_form_semple').clone();

        // set inputs
        formClone.find('input[name="product_itemid"]').val(itemid).attr('disabled' , 'disabled');
        formClone.find('input[name="product_itemid"]').attr('data-product', productID);
        formClone.find('input[name="product_amount"]').val(parseInt(amount));
        formClone.appendTo('#express_order__added_items').show();
        formClone.addClass('added_item_form').removeClass('added_item_form_semple');
        
        // set text
        formClone.find('#product_itemid__text').text(itemid);
        formClone.find('#product_itemtitle__text').text(itemTitle);
        formClone.find('#product_amount__text').text('כמות: ' + amount);

        this.clean_express_items.show();
        this.add_express_order_cart__error.hide();

        this.initRemoveItemEvent();
    }

    initRemoveItemEvent(){
        $('.express_added_item__remove_submit').off('click').on('click', (e)=>{
            e.preventDefault();
            this.removeItem(e);
        })

        $('.added_item_form').off('submit').on('submit', (e)=>{
            e.preventDefault();
            this.removeItem(e);
        })
    }

    removeItem(e){
        $(e.currentTarget).closest('li.added_item_form').remove();
        if($('li.added_item_form').length == 0) this.clean_express_items.hide();
    }
}



export default ExpressOrder;
    