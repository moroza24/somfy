import $ from 'jquery';

class GeneralScripts {

    constructor() {
        // expert cta
        this.sign_to_expert_cta = $('#sign_to_expert_cta');
        this.expert_banner_loader = $('#expert_banner_loader');
        this.expert_banner_error = $('#expert_banner_error');
        this.expert_banner_success = $('#expert_banner_success');

        this.initEvents();

    }

    initEvents(){
        // expert cta
        this.sign_to_expert_cta.on('click', ()=>{
            this.sign_to_expert_cta.hide();
            this.expert_banner_loader.show();

            $.ajax({
                url : somfyData.ajax_url,
                type : 'POST',
                data : {
                    action : 'somfy_pro_expert_signup',
                    nonce: somfyData.ajax_nonce
                },
                dataType: "json",
                success: (response) =>{
                    if(response.status){
                        this.expert_banner_loader.hide();
                        this.expert_banner_success.fadeIn();
                    } else {
                        // SHOW ERROR
                        this.sign_to_expert_cta.show();
                        this.expert_banner_loader.hide();
                        this.expert_banner_error.html(response.message).fadeIn();
                    }
                },
                error:  (jqXHR, status) => {
                    console.log(jqXHR);
                    console.log(status);
                }
            })
        })
    }
}



export default GeneralScripts;
    