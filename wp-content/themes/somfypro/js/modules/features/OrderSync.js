import $ from 'jquery';

class OrderSync {

    constructor() {   

        setInterval(()=>{this.checkeIfSyncNeeded();}, 60000);
        this.isRunning = false;

        this.checkeIfSyncNeeded();
    }  

    checkeIfSyncNeeded(){
        let lastOrderSync = localStorage.getItem('pro_last_order_sync');
        let current = Date.now();

        if(!lastOrderSync){
            localStorage.setItem('pro_last_order_sync', current);       
            
            // call to sync orders
            this.syncOrders();
        } else {
            if(current - lastOrderSync >= 300000){
                localStorage.setItem('pro_last_order_sync', current);       
                
                // call to sync orders
                this.syncOrders();
            }
        }
    }

    syncOrders(){
        if(this.isRunning) return;

        this.isRunning = true;
        $.ajax({
            url : somfyData.ajax_url,
            type : 'POST',
            data : {
                action : 'somfy_pro_sync_user_orders',
                nonce: somfyData.ajax_nonce
            },
            dataType: "json",
            success: (response) =>{
                console.log('order synced');
                this.isRunning = false;
            },
            error:  (jqXHR, status) => {
                console.log(jqXHR);
                console.log(status);
            }
        });
    }
}
export default OrderSync;
