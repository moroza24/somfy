
import $ from 'jquery';
class AccountUpdatePopup {

    constructor() {
        // update_account_request
        this.update_details_cta = $('.update_details_cta');
        this.update_account_request_popup = $('#update_account_request_popup');
        this.update_account_request_popup__close = $('.update_account_request_popup__close');
        this.update_account_request__form = $('#update_account_request__form');
        this.update_account_request__form_submit_error = $('#update_account_request__form_submit_error');
        this.update_account_request__form_submit_success = $('#update_account_request__form_submit_success');
        this.update_account_request__form_loader = $('#update_account_request__form_loader');
        this.submit_update_account_request = $('#submit_update_account_request');
        
        this.initEvents();
    }

    initEvents() {
        // update_account_request
        this.update_details_cta.on('click', ()=>{
            this.openUpdateAccountRequest_popup();
        })

        this.update_account_request_popup__close.on('click', ()=>{
            this.closeUpdateAccountRequest_popup();
        })

        this.update_account_request__form.on('submit', (e)=>{
            e.preventDefault();
            // validate form
            this.update_account_request__form_loader.show();
            this.submit_update_account_request.hide();

            this.sendNewRequest(e);
        })

    }


    sendNewRequest(e){
        let companyname = $('#update_account_request_companyname').val();
        let customerEmail = $('#update_account_request__email').val();
        let message = $('#update_account_request__message').val();
        let phone = $('#update_account_request__phone').val();

        let error = false;
        if(companyname.length < 3 || companyname == ''){
            $('p.form_error[data-input="update_account_request_companyname"]').show();
            error = true;
        } else {
            $('p.form_error[data-input="update_account_request_companyname"]').hide();
        }


        if(customerEmail.length < 3 || customerEmail == ''){
            $('p.form_error[data-input="update_account_request__email"]').show();
            error = true;
        }else {
            $('p.form_error[data-input="update_account_request__email"]').hide();
        }

        
        if(message.length < 3 || message == ''){
            $('p.form_error[data-input="update_account_request__message"]').show();
            error = true;
        }else {
            $('p.form_error[data-input="update_account_request__message"]').hide();
        }

        var regex = /^0(5[^7]|[2-4]|[8-9]|7[0-9])[0-9]{7}$/;
        if(phone.length < 8 || phone == '' || !regex.test(phone.replace("-", ""))){
            $('p.form_error[data-input="rgistration_request__phone"]').show();
            error = true;
        } else {
            $('p.form_error[data-input="rgistration_request__phone"]').hide();
        }

        if(error){
            this.update_account_request__form_loader.hide();
            this.submit_update_account_request.show();
            return;
        }


        $.ajax({
            url : somfyData.ajax_url,
            type : 'POST',
            data : {
                action : 'somfy_pro_new_account_update_request',
                companyname: companyname,
                customerEmail: customerEmail,
                message: message,
                phone: phone,
                nonce: somfyData.ajax_nonce
            },
            dataType: "json",
            success: (response) =>{
                if(response.status){
                    this.update_account_request__form_loader.hide();
                    this.update_account_request__form_submit_success.fadeIn();
                    let closePopupTimer = setTimeout(this.closeUpdateAccountRequest_popup.bind(this), 2000); 
                } else {
                    // SHOW ERROR
                    this.submit_update_account_request.show();
                    this.update_account_request__form_loader.hide();
                    this.update_account_request__form_submit_error.html(response.message).fadeIn();
                }
            },
            error:  (jqXHR, status) => {
                console.log(jqXHR);
                console.log(status);
            }
        });
    }

    closeUpdateAccountRequest_popup(){
        this.update_account_request_popup.fadeOut();
        this.update_account_request__form_submit_error.hide();
        this.update_account_request__form_submit_success.hide();
        this.update_account_request__form_loader.hide();
        this.submit_update_account_request.show();
    }

    openUpdateAccountRequest_popup(){
        let defualt_user_email = $('#defualt_user_email').val();
        let defualt_companyname = $('#defualt_companyname').val();
        let defualt_phone = $('#defualt_phone').val();

        $('#update_account_request__email').val(defualt_user_email);
        $('#update_account_request_companyname').val(defualt_companyname);
        $('#update_account_request__phone').val(defualt_phone);
        $('#update_account_request__message').val('');
        this.update_account_request_popup.fadeIn();
    }
      
}

export default AccountUpdatePopup;
    