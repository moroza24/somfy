
import $ from 'jquery';

class Inputs {

    constructor() {
        this.initRangeSlider()
        this.initSingleRange()
    }

    initSingleRange() {
        let rangeInput = $('.single-range-wrap input[type=range]');

        rangeInput.off('input').on('input', (e) =>{
            this.setSingleRangeValue($(e.currentTarget)[0], $(e.currentTarget.previousElementSibling)[0]);
        });
        
    }

    setSingleRangeValue(range, rangeLabel){
        let newValue = Number( (range.value - range.min) * 100 / (range.max - range.min) );
        let newPosition = 10 - (newValue * 0.2);

        rangeLabel.innerHTML = `<span>${range.value}</span>`;
        rangeLabel.style.right = `calc(${newValue}% + (${newPosition}px))`;
    }

    initRangeSlider() {
        $('.range_slider input[type=range]').first().off('input').on('input', (e) => {
            let thisRange = $(e.currentTarget)[0];
            thisRange.value = Math.min( thisRange.value, thisRange.parentNode.childNodes[5].value-1 );
            var value = (100/( parseInt(thisRange.max)-parseInt(thisRange.min)))*parseInt(thisRange.value)-(100/(parseInt(thisRange.max)-parseInt(thisRange.min)))*parseInt(thisRange.min);
            var children = thisRange.parentNode.childNodes[1].childNodes;
            children[1].style.width=value+'%';
            children[5].style.left=value+'%';
            children[7].style.left=value+'%';
            children[11].style.left=value+'%';
            children[11].childNodes[1].innerHTML=thisRange.value;
        })

        $('.range_slider input[type=range]').last().off('input').on('input', (e) => {
            let thisRange = $(e.currentTarget)[0];

            thisRange.value=Math.max(thisRange.value,thisRange.parentNode.childNodes[3].value-(-1));
            var value=(100/(parseInt(thisRange.max)-parseInt(thisRange.min)))*parseInt(thisRange.value)-(100/(parseInt(thisRange.max)-parseInt(thisRange.min)))*parseInt(thisRange.min);
            var children = thisRange.parentNode.childNodes[1].childNodes;
            children[3].style.width=(100-value)+'%';
            children[5].style.right=(100-value)+'%';
            children[9].style.left=value+'%';
            
            children[13].style.left=value+'%';
            children[13].childNodes[1].innerHTML=thisRange.value;

        })
    }
}

export default Inputs;