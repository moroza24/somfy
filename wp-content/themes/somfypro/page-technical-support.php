<?php /* Template Name: Somfy Technical Support */ ?>
<?php 
    if ( ! defined( 'ABSPATH' ) ) exit;
?>
<?php
    // redirect not logged user
    if(!is_user_logged_in()){
        wp_redirect ('./login');
    };
?>
<?php 
    get_header(); 

    get_template_part('templates/pages/technical-support');

    get_footer(); 
?>