<?php
  get_header();

  // get page 
  $pageID = get_the_ID();
  $pageTitle = get_the_title($pageID);
  $pageContent = get_the_content($pageID);
  $pageContentwithbreaks = wpautop( $pageContent, true );
  $currentPagePermalink = get_the_permalink($pageID);

  $thumbnail = get_the_post_thumbnail_url($pageID);
  // get question and answer array
  $support_link_data = get_field('support_link_data', $pageID);
?>
<?php 
    if(!is_user_logged_in()){
        $fixStyle_header = 'top: 0; right: 0;'; 
        $fixStyle_news_updates = 'margin-top: 85px; margin-right: 20px; margin-left: 20px;'; 
    } else {
        $fixStyle_header ='';
        $fixStyle_news_updates ='';
    }
?>
<div class="main_content__with_header"id="single-customer-updates_page">
    <div class="page_header"
        style="background: linear-gradient(-90deg, rgba(29, 26, 52, .7), rgba(29, 26, 52, .7)), url('<?php echo $thumbnail; ?>');
            background-size: cover;
            background-position: center center;
            background-repeat: no-repeat;
            <?php echo $fixStyle_header; ?>">
          <ol class="breadcrumbs">
            <li><a href="<?php echo home_url(); ?>" alt='דף בית'>דף בית ></a></li>
            <li><a href="<?php echo get_home_url(); ?>/somfy-updates" alt='עדכונים מסומפי'>עדכונים מסומפי ></a></li>
            <li><a href="<?php echo $currentPagePermalink; ?>" alt='><?php echo $pageTitle; ?>'><?php echo $pageTitle; ?></a></li>
          </ol>
        <h2 class="page_title"><?php echo $pageTitle; ?></h2>
    </div>

    <div class="content_container"  style="<?php echo $fixStyle_news_updates; ?>">
        <?php echo $pageContentwithbreaks; ?>
    </div>
  </div>
<?php
get_footer()
?>
