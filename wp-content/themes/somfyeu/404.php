<?php

get_header(); ?>

	<div id="not_found" class="content-area">
		<div class="query_header" style="direction: rtl;">
			<h1 class="section-title">מצטערים,</h1>
			<h4 class="subtitle">העמוד לא נמצא</h4>


			<div class="imghead">
				<img class="image_head" src="<?php bloginfo('template_url'); ?>/assets/img/pages/404.png" alt="header_image">
			</div>
		</div>
	</div><!-- #primary -->

<?php get_footer(); ?>