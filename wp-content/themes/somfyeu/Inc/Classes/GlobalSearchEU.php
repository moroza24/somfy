<?php
/**
 * @package SomfyEU
 */

namespace Inc\Classes;

use Inc\Classes\Shop\ProductsEU;

class GlobalSearchEU {
     /** 
     * Description: 
     * @param
     * @return
     */
    public function globalSearch($request){
        if ( !wp_verify_nonce( $_REQUEST['nonce'], NONCE_SECRET)) {
            exit("No naughty business please");
         }  

        //  validate params
        $term = isset($_REQUEST['term']) ? $_REQUEST['term'] : null;
        if(!isset($term)){
            $result['status'] = false;
            $result['message'] = 'חלק מהנתונים חסרים';
            echo json_encode($result);
            die();
        }

        $searchResults = array(
            'products'    => array(),
            'taxonomy'   => array()
        );


        // prep term
        
        // serach for products, limit 5
        $Products = new ProductsEU();
        $productsResults = $Products->searchProducts($term, null, 5, null, true);
        $totalProductsResults = 0;
        
        if($productsResults['total_count'] > 0){
            $searchResults['products'] = $productsResults['res'];
            $totalProductsResults = $productsResults['total_count'];
        }
        


        // search for taxonomy
        $taxonomyResults = $this->searchTaxonomy($term);
        $searchResults['taxonomy'] = $taxonomyResults;

        $result['status'] = true;
        $result['results'] = $searchResults;
        $result['total_products'] = $totalProductsResults;
        
        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            echo json_encode($result);
        }else {
            header("Location: ".$_SERVER["HTTP_REFERER"]);
        }
      
        die();
    }


    /** 
     * Description: 
     * @param
     * @return
     */
    public function fullSearch($request){
        if ( !wp_verify_nonce( $_REQUEST['nonce'], NONCE_SECRET)) {
            exit("No naughty business please");
         }  

        //  validate params
        $term = isset($_REQUEST['term']) ? $_REQUEST['term'] : null;
        $filters = isset($_REQUEST['filters']) ? $_REQUEST['filters'] : null;
        $paginators = isset($_REQUEST['paginators']) ? $_REQUEST['paginators'] : null;

        if(!isset($term) || !isset($paginators)){
            $result['status'] = false;
            $result['message'] = 'חלק מהנתונים חסרים';
            echo json_encode($result);
            die();
        }

        $totalProductsResults = 0;
        $searchResults = array(
            'products'    => array()
        );


        // prep term
        
        // serach for products, with filters
        $Products = new ProductsEU();

        $productsResults = $Products->searchProducts(
            $term, 
            $filters, 
            $paginators['limit'], 
            $paginators['page']);
        
        if($productsResults['total_count'] > 0){
            $searchResults['products'] = $productsResults['res'];
            $totalProductsResults = $productsResults['total_count'];
        }

        $result['status'] = true;
        $result['results'] = $searchResults;
        $result['total'] = $totalProductsResults;
        

        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            echo json_encode($result);
        }else {
            header("Location: ".$_SERVER["HTTP_REFERER"]);
        }
      
        die();
    }

    public function searchTaxonomy($term){
        global $wpdb;
        $dbPosts = EU_SITE_PREFIX . "posts";

        // Prep term and WHERE query
        $prepedTerm = str_replace(' ', '%', $term);

        $titleWhere = " p.post_title LIKE '%$prepedTerm%'";

        $whereQuery = " AND ($titleWhere)";

        $query = "SELECT p.ID, 
                p.post_title, 
                REPLACE(p.post_type, 'product_', '')  as post_type   
            FROM $dbPosts p
            WHERE (p.post_type IN ('product_category' ,'product_group', 'product_subgroup') AND p.post_status = 'publish')
            $whereQuery
            ORDER BY p.post_type
        ";

        $res = $wpdb->get_results($query);

        if($res){
            // add permalink
            for($i = 0; $i < count($res); $i++){
                $res[$i]->permalink = get_the_permalink($res[$i]->ID);
            }
        }

        return $res;
    }
}