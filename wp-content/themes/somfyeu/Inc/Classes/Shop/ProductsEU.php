<?php
/**
 * @package SomfyEU
 */

namespace Inc\Classes\Shop;


class ProductsEU {
    public $dbPosts;
    public $dbPostmeta;
    public $dbStockNotification;
 
    function __construct(){
        global $wpdb;
        $this->dbPosts = EU_SITE_PREFIX . "posts";
        $this->dbPostmeta = EU_SITE_PREFIX . "postmeta";
        $this->dbStockNotification = EU_SITE_PREFIX . "stock_notification";

        $this->templateURL = get_template_directory_uri();
    }


    
    /** 
     * Description: getShopProducts with lots of filters
     * @param 
     *  $limit - pagination
     *  $page - pagination
     * @return products array 
     *  if $shortResults = true - return without meta data
     */
    public function getShopProducts($request){
        global $wpdb;
        if ( !wp_verify_nonce( $_REQUEST['nonce'], NONCE_SECRET)) {
            exit("No naughty business please");
        }  

        //  validate params
        $advancedFilters = isset($_REQUEST['advancedFilters']) ? $_REQUEST['advancedFilters'] : null;
        $sortFilter = isset($_REQUEST['sortFilter']) ? $_REQUEST['sortFilter'] : null;
        $paginators = isset($_REQUEST['paginators']) ? $_REQUEST['paginators'] : null;
        $filtersPageId = isset($_REQUEST['filtersPageId']) ? $_REQUEST['filtersPageId'] : null;
        
        $comingFromCalculator = isset($_REQUEST['comingFromCalculator']) ? $_REQUEST['comingFromCalculator'] : null;
        // $calcNameQuery = isset($_REQUEST['calcNameQuery']) ? $_REQUEST['calcNameQuery'] : null;
        
        if(!isset($paginators) || !isset($filtersPageId)){
            $result['status'] = false;
            $result['paginators'] = $paginators;
            $result['filtersPageId'] = $filtersPageId;
            $result['message'] = 'חלק מהנתונים חסרים';
            echo json_encode($result);
            die();
        }

        // if comingFromCalculator add name query to the mix
        // $calcNameWhereQuery = '';

        // if($comingFromCalculator && $comingFromCalculator == 'true'){
        //     // if(!$calcNameQuery || !is_array($calcNameQuery) || count($calcNameQuery) == 0){
        //     //     $result['status'] = false;
        //     //     $result['message'] = 'חלק מהנתונים חסרים למציאת תוצאות מחשבון';
        //     //     echo json_encode($result);
        //     //     die();
        //     // }

        //     // $calcNameWhereQuery .= " AND ((";

        //     // $relWhere = '';
        //     // for ($i = 0; $i < count($calcNameQuery); $i++){
        //     //     if($i > 0){
        //     //         $relWhere .= ' OR ';
        //     //     }

        //     //     $relWhere .= " (p.post_title LIKE '%" .$calcNameQuery[$i] . '%\')';
        //     // }
        //     // $calcNameWhereQuery .= $relWhere;
        //     // $calcNameWhereQuery .= ")) ";
        // }

        // dont include internal products
        $warrantyProductID = get_option('shop_warranty_product_id');
        $shippingProductID = get_option('shop_shipping_product_id');
        $internalProducts = "('$warrantyProductID', '$shippingProductID')";

        // set limit and offset
        $limit = intval($paginators['limit']);
        $page = intval($paginators['page']);

        $startAfter = ($page - 1) * $limit;


        // prep order filters
        $orderByQuery = '';
        $sortJoin = '';

        if(isset($sortFilter) && $sortFilter['sortType'] != '' && $sortFilter['sortOrder'] != ''){
            $sortOrder = $sortFilter['sortOrder'];
            $sortType = $sortFilter['sortType'];


            switch($sortType){
                case 'post_title':
                    $orderByQuery = "ORDER BY p.post_title $sortOrder";
                    break;
                case 'base_price' : 
                    $sortJoin = "
                        LEFT JOIN $this->dbPostmeta pm2
                            ON pm2.meta_key = '$sortType' AND pm2.post_id = p.ID";
                    $orderByQuery = "ORDER BY pm2.meta_value+0 $sortOrder";
                    break;
                case 'on_sell' : 
                    $sortJoin = "
                        LEFT JOIN $this->dbPostmeta pm2
                            ON pm2.meta_key = '$sortType' AND pm2.post_id = p.ID";
                    $orderByQuery = "ORDER BY pm2.meta_value+0 $sortOrder";
                    break;
            }
        }


        // // prep advancedFilters query
        $afJoin = '';
        $afWhere = '';

        if(isset($advancedFilters) && count($advancedFilters) > 0){
            foreach ($advancedFilters as $af){
                $filterName = $af['filterName'];
                if(is_array($af['value']) && $af['value'][0] == '') continue;

                switch($af['filterType']){
                    case 'select':
                        $values = " ('".implode("','",$af['value'])."') ";

                        $afJoin .= "
                            LEFT JOIN $this->dbPostmeta pm_$filterName
                                ON pm_$filterName.meta_key = '$filterName' AND pm_$filterName.post_id = p.ID ";
                        $afWhere .= "
                            AND pm_$filterName.meta_value IN $values ";
                        break;
                    
                    case 'relationship':
                        $relWhere = '';
                        for ($i = 0; $i < count($af['value']); $i++){
                            if($i == 0){
                                $relWhere .= '
                                    AND ( ';
                            }

                            if($i > 0){
                                $relWhere .= ' OR ';
                            }

                            $relWhere .= " (pm_$filterName.meta_value LIKE '%s:" .strlen($af['value'][$i]) . ':"' . $af['value'][$i] . '"%\''
                                ." OR pm_$filterName.meta_value = '" . $af['value'][$i] . "') ";

                            if($i == count($af['value']) - 1){
                                $relWhere .= ' ) ';
                            }
                        }

                        $afJoin .= "
                            LEFT JOIN $this->dbPostmeta pm_$filterName
                                ON pm_$filterName.meta_key = '$filterName' AND pm_$filterName.post_id = p.ID ";

                        $afWhere .= $relWhere;
                        break;
                    
                    case 'range':
                        $min = $af['min'];
                        $max = $af['max'];
                        $afJoin .= "
                            LEFT JOIN $this->dbPostmeta pm_$filterName
                                ON pm_$filterName.meta_key = 'base_price' AND pm_$filterName.post_id = p.ID";
                        $afWhere .= "
                            AND (cast(pm_$filterName.meta_value as int) >= cast('$min' as int) AND cast(pm_$filterName.meta_value as int) <= cast('$max' as int))";
                        break;

                    case 'number':
                        $relWhere = '';
                        $compare_type = $af['compare_type'] != '' ? $af['compare_type'] : '=';

                        if(!is_array($af['value'])){
                            $values = " ('".implode("','",$af['value'])."') ";
                        } else {
                            $values = $af['value'];
                        }

                        for ($i = 0; $i < count($af['value']); $i++){
                            if($i == 0){
                                $relWhere .= '
                                    AND ( ';
                            }

                            if($i > 0){
                                $relWhere .= ' OR ';
                            }

                            $relWhere .= " 
                                CAST(pm_$filterName.meta_value AS CHAR) $compare_type " .intval($af['value'][$i]);                            

                            if($i == count($af['value']) - 1){
                                $relWhere .= ' ) ';
                            }
                        }

                        $afWhere .= $relWhere;
                        $afJoin .= "
                            LEFT JOIN $this->dbPostmeta pm_$filterName
                                ON pm_$filterName.meta_key = '$filterName' AND pm_$filterName.post_id = p.ID ";
                        break;
                
                    case "boolen":
                        $val = $af['value'] == 'true' ? 1 : 0;
                        $afJoin .= "
                            LEFT JOIN $this->dbPostmeta pm_$filterName
                                ON pm_$filterName.meta_key = '$filterName' AND pm_$filterName.post_id = p.ID ";
                        $afWhere .= "
                            AND (pm_$filterName.meta_value = $val)";
                        break;
                    }
            }
        }


        // // FINAL QUERY
        $query = "SELECT p.ID, p.post_title
            FROM $this->dbPosts p
            $sortJoin
            $afJoin
            WHERE (p.post_type = 'product' AND p.post_status = 'publish' AND p.ID NOT IN $internalProducts)
            $afWhere
            $orderByQuery
        ";
        $result['query'] = $query;

        
        $res = $wpdb->get_results($query);

        $total_count = 0;
        $final_products_results = array();
        $final_filtersObjects = array();

        $fieldsForFilters = get_field('filters', $filtersPageId);
        $filtersObjects = $this->getProductFilters($fieldsForFilters);

        if($res){
            // prep advancedFilter return obj
            if(count($filtersObjects) > 0){
                for($i = 0; $i < count($res); $i++){
                    $resID = $res[$i]->ID;
                    // set advanced filters totals
                    for($j = 0 ; $j < count($filtersObjects) ; $j++){
                        if($filtersObjects[$j]['type'] == "relationship"){
                            $productVal = get_field($filtersObjects[$j]['key'], $resID);
                            $filtersObjects[$j]['productVal'] = $productVal;
                            if(!isset($productVal)) $productVal = array();
                            foreach($productVal as $product){
                                $filtersObjects[$j]['choices']["$product->ID"]['results'] += 1;
                            }
                        } else {
                            if($filtersObjects[$j]['type'] == "select"){
                                $productVal = get_field($filtersObjects[$j]['key'], $resID);
                                
                                if(isset($productVal['value'])){
                                    $productVal = $productVal['value'];
                                }

                                if($productVal == '') continue;
                                $filtersObjects[$j]['productVal'] = $productVal;

                                $filtersObjects[$j]['choices']["$productVal"]['results'] += 1;
                            }
                        }
                    }
                    
                    // add final products
                    if(count($final_products_results) < $limit && $i >= $startAfter){
                        $res[$i]->meta = get_fields($resID);
                        $res[$i]->permalink = get_the_permalink($resID);

                        $res[$i]->thumbnail_url = get_products_files($resID, 'thumbnail')['url'];
                        $res[$i]->warranty_applicable = $this->checkIfWarrantyReleated($resID);
                        $stock = get_field('stock_quantity', $resID);
                        $res[$i]->stock_quantity = $stock == '' ? 0 : $stock;

                        array_push($final_products_results, $res[$i]);
                    }
                }

                $total_count = count($res);
                $result['status'] = true;
                $result['filtersObjects'] = $filtersObjects;
                $result['products'] = $final_products_results;
                $result['total'] = $total_count;
                
                echo json_encode($result);
                die();
            }
        } 
        
        $result['status'] = false;
        $result['res'] = $res;
        $result['message'] = 'לא נמצאו תוצאות';
        echo json_encode($result);
        die();   
    }



    /** 
     * Description: searchProducts for search Page
     * @param $term - search term
     *  $shortResults - for short results (for global serach results preivew)
     *  $limit - pagination
     *  $page - pagination
     * @return products array 
     *  if $shortResults = true - return without meta data
     */
    public function searchProducts($term, $filters, $limit = NULL, $page = NULL, $shortResults = false){
        global $wpdb;
        // dont include internal products
        $warrantyProductID = get_option('shop_warranty_product_id');
        $shippingProductID = get_option('shop_shipping_product_id');

        $internalProducts = "('$warrantyProductID', '$shippingProductID')";

        // set limit and offset
        $queryLimit = isset($limit) ? " LIMIT ".$limit : " ";
        $queryOffset = isset($page) && isset($limit) ? " OFFSET " .($page - 1) * $limit : "";

        // Prep term and WHERE query
        $prepedTerm = str_replace(' ', '%', $term);

        $titleWhere = " p.post_title LIKE '%$prepedTerm%'";
        $itemidWhere = "pm1.meta_value LIKE '%$prepedTerm%'";

        $whereQuery = " AND (($titleWhere) OR ($itemidWhere))";

        // prep order filters
        $orderByQuery = '';
        $sortJoin = '';

        if(isset($filters) && $filters['sortType'] != '' && $filters['sortOrder'] != ''){
            $sortOrder = $filters['sortOrder'];
            $sortType = $filters['sortType'];

            switch($sortType){
                case 'post_title':
                    $orderByQuery = "ORDER BY p.post_title $sortOrder";
                    break;
                case 'base_price' : 
                    $sortOrder = $filters['sortOrder'];
                    $sortJoin = "LEFT JOIN $this->dbPostmeta pm2
                        ON pm2.meta_key = '$sortType' AND pm2.post_id = p.ID";
                    $orderByQuery = "ORDER BY pm2.meta_value $sortOrder";
                    break;
                case 'on_sell' : 
                    $sortJoin = "LEFT JOIN $this->dbPostmeta pm2
                        ON pm2.meta_key = '$sortType' AND pm2.post_id = p.ID";
                    $orderByQuery = "ORDER BY pm2.meta_value $sortOrder";
                    break;
            }
        }

        $query = "SELECT p.ID, p.post_title,
                pm1.meta_value AS itemid
            FROM $this->dbPosts p
            LEFT JOIN $this->dbPostmeta pm1 
                ON pm1.meta_key = 'itemid' AND pm1.post_id = p.ID
            $sortJoin
            WHERE (p.post_type = 'product' AND p.post_status = 'publish' AND p.ID NOT IN $internalProducts)
            $whereQuery

            $orderByQuery

            $queryLimit
            $queryOffset
        ";
        
        $res = $wpdb->get_results($query);

        // $autoload = false => return full product data
        if($res && !$autoload){
            for($i = 0; $i < count($res); $i++){
                $res[$i]->meta = get_fields($res[$i]->ID);
                $res[$i]->permalink = get_the_permalink($res[$i]->ID);

                $res[$i]->thumbnail_url = get_products_files($res[$i]->ID, 'thumbnail')['url'];

                $res[$i]->warranty_applicable = $this->checkIfWarrantyReleated($res[$i]->ID);
            }
        } else if($res && $autoload){
            // $autoload = true => return for preview only
            // add permalink
            for($i = 0; $i < count($res); $i++){
                $res[$i]->permalink = get_the_permalink($res[$i]->ID);
            }
        }

        // get total count with no limit
        if($res){
            $count_query = "SELECT COUNT(p.ID) AS total
                FROM $this->dbPosts p
                LEFT JOIN $this->dbPostmeta pm1 
                    ON pm1.meta_key = 'itemid' AND pm1.post_id = p.ID
                $sortJoin
                WHERE (p.post_type = 'product' AND p.post_status = 'publish' AND p.ID NOT IN $internalProducts)
                $whereQuery
            ";

            $total_count = $wpdb->get_results($count_query);
        } else {
            $total_count = 0;
        }

        return array(
            'res' => $res,
            'total_count' => $total_count[0]->total
        );
    }


    /** 
     * Description: getByID
     * @param $productID
     * @return
     */
    public function getByID($productID){

        $res = get_post($productID, ARRAY_A);
        $res['meta'] = get_fields($productID);

        return $res;
    }

    /** 
     * Description: getByID
     * @param $productID
     * @return
     */
    public function getProductForCart($request){
        if ( !wp_verify_nonce( $_REQUEST['nonce'], NONCE_SECRET)) {
            exit("No naughty business please");
        }  

        //  validate params
        $cartItems = isset($_REQUEST['cartItems']) ? $_REQUEST['cartItems'] : null;

        if(!isset($cartItems)){
            $result['status'] = false;
            $result['message'] = 'חלק מהנתונים חסרים';
            echo json_encode($result);
            die();
        }

        $productDataResults = array();

        if(count($cartItems) > 0){
            foreach ($cartItems as $item){
                $productID = $item['productID'];

                $product = array();
        
                $product['productID'] = $productID;
                $product['itemid'] = get_field('itemid', $productID);
                $product['title'] = get_the_title($productID);
                $product['permalink'] = get_the_permalink($productID);
                $product['thumbnail'] = get_products_files($productID, 'thumbnail')['url'];
                $product['base_price'] = get_field('base_price', $productID);
                $product['on_sell'] = get_field('on_sell', $productID);
                $product['discount_price'] = get_field('discount_price', $productID);
                $product['discount_percentage'] = get_field('discount_percentage', $productID);
                $product['amount'] = $item['amount'];
                $product['stock_quantity'] = get_field('stock_quantity', $productID);
                
                $product['parentProduct'] = $item['parentProduct'];
                $product['childeProduct'] = $item['childeProduct'];
                array_push($productDataResults, $product);
            }
        }
        $result['status'] = true;
        $result['productDataResults'] = $productDataResults;
        echo json_encode($result);
        die();
       
        // return $product;
    }

        /** 
     * Description: checkIfWarrantyReleated
     * @param
     * @return
     */
    public function checkIfWarrantyReleated($productID){
        //  validate params
        if(!isset($productID)){
            return false;
        }

        $warranty_categories_list = explode(',', get_option('warranty_categories_list'));
        $warranty_subgroups_list = explode(',', get_option('warranty_subgroups_list'));
        $warranty_sub2groups_list = explode(',', get_option('warranty_sub2groups_list'));

        $productTax = array(
            'category' => get_field('custitem_somfy_web_product_category',$productID, false),
            'sub_product_group' => get_field('custitem_sub_product_group',$productID, false),
            'sub_2product_group' => get_field('custitem_sub2productgroup',$productID, false),
        );

        if(count(array_intersect($productTax['category'],$warranty_categories_list)) >= 1 
            && count(array_intersect($productTax['sub_product_group'],$warranty_subgroups_list)) >= 1 
            && count(array_intersect($productTax['sub_2product_group'],$warranty_sub2groups_list)) >=1){
                return true;
            }
        return false;
    }

    /** 
     * Description: stockNotifictaionSignup
     * @param
     * @return
     */
    public function stockNotifictaionSignup($request){
        if ( !wp_verify_nonce( $_REQUEST['nonce'], NONCE_SECRET)) {
            exit("No naughty business please");
         }  

        //  validate params
        $userEmail = isset($_REQUEST['userEmail']) ? $_REQUEST['userEmail'] : null;
        $userFullname = isset($_REQUEST['userFullname']) ? $_REQUEST['userFullname'] : null;
        $userPhone = isset($_REQUEST['userPhone']) ? $_REQUEST['userPhone'] : null;
        $productID = isset($_REQUEST['productID']) ? $_REQUEST['productID'] : null;

        $requiredAmount = isset($_REQUEST['requiredAmount']) ? $_REQUEST['requiredAmount'] : 0;

        if(!isset($userEmail) || !isset($productID) || !isset($userFullname) || !isset($userPhone)){
            $result['status'] = false;
            $result['message'] = 'חלק מהנתונים חסרים';
            echo json_encode($result);
            die();
        }

        
        // logged user
        // update profile field - newsletter_subscribed
        $userID = null;

        $userExists = get_user_by( 'email', $userEmail );
        if($userExists){
            $userID = $userExists->data->ID;
        }


        // add stock notification db entry
        $notificationAdded = $this->addStockNotification($productID, $userEmail, $userFullname, $userPhone, $userID, $requiredAmount);

        if($notificationAdded['status']){
            $result['status'] = true;
            $result['notificationAdded'] = $notificationAdded;
            $result['message'] = 'פרטיך נשמרו, תקבל הודעה ברגע שהמלאי יהיה זמין';
        } else {
            $result['status'] = false;
            $result['messageSent'] = $notificationAdded;
            $result['message'] = 'קרתה שגיאה, נסה שנית';
        }

        echo json_encode($result);
        die();
    }

    /** 
     * Description: addStockNotification
     * @param
     * @return
     */
    public function addStockNotification($productID, $userEmail, $userFullname, $userPhone, $userID = null, $requiredStock = 0){
        global $wpdb;
		$createDate = current_time( "Y-m-d H:i:s", 0 );

        $query = "INSERT INTO $this->dbStockNotification
			(
                `ID`, 
                `site`, 
                `user_email`, 
                `user_fullname`, 
                `user_phone`, 
                `user_id`, 
                `product_id`, 
                `required_amount`, 
                `user_notified`, 
                `create_date`,
                `update_date`
            ) VALUES (
                NULL,
                'eu',
                '$userEmail', 
                '$userFullname', 
                '$userPhone', 
                '$userID', 
                '$productID', 
                '$requiredStock', 
                0, 
                '$createDate',
                '$createDate'
            )";

        $res = $wpdb->query($query);
        
        if($res){
            return array('status' => true, 'result' => $res);
        } else {
            return array('status' => false, 'result' => $res);
        }
    }



    /**
     * Description: getProductFilters
     */
    public function getProductFilters($fieldsForFilters = null){

        if(!$fieldsForFilters){
            $fieldChoises = get_field_object('field_5fe9d888b6f4d');
            $fieldsForFilters = $fieldChoises['choices'];
        }

        $groups = acf_get_field_groups(array('post_type' => 'product'));
                        
        $fields = array();

        foreach ($groups as $group){

            $groupFields = acf_get_fields($group['key']);
            
            foreach ($groupFields as $field){
                if(!in_array($field['name'], $fieldsForFilters)) continue;

                $field_type = $field['type'];
                $field_name = $field['name'];

                $fieldData = array(
                    'key' => $field['key'],
                    'label' => $field['label'],
                    'name' => $field_name,
                    'type' => $field['type'],
                );
                
                if($field_type == 'relationship'){
                    $postType = $field['post_type'][0];
                    wp_reset_postdata();

                    // get post types
                    $posts = new \WP_Query(array(
                        'post_type' => $postType,
                        'posts_per_page'   => -1,
                        'meta_key' => 'id',
                        'orderby' => 'meta_value_num',
                        'order' => 'ASC',
                        'meta_query' => array(
                            array(
                                'key' => 'show_on_site',
                                'compare' => '=',
                                'value' => TRUE
                            )
                        )
                    ));
                    
                    $postsChoices = array();

                    if($posts){
                        while ($posts->have_posts()) {
                            $posts->the_post();
                            $choiseID = get_the_ID();

                            $postsChoices["$choiseID"] = array(
                                'value' => $choiseID,
                                'title' => get_the_title(),
                                'results' => 0
                            );
                        }
                    }
                    wp_reset_postdata();

                    $fieldData['post_type'] = $field['post_type'][0];
                    $fieldData['choices'] = $postsChoices;
                } else if($field_type == 'select'){
                    $selectChoices = array();
                    foreach ($field['choices'] as $key => $value) {
                        $selectChoices["$key"] = array(
                            'value' => $key,
                            'title' => $value,
                            'results' => 0
                        );
                    }
                    $fieldData['choices'] = $selectChoices;
                } else if($field_type == 'number'){
                    $fieldData['min'] = $field['min'];
                    $fieldData['max'] = $field['max'];
                    $fieldData['step'] = $field['step'];
                } else {
                    $fieldData = $field;
                }
                array_push($fields, $fieldData);
                // $fields["$field_name"] = $fieldData;
            }
        }

        return $fields;
    }

}