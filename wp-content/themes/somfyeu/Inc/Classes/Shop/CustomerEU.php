<?php
/**
 * @package  SomfySiteManager
 */

namespace Inc\Classes\Shop;

use Inc\Classes\Shop\NetSuiteApiEU;
use Inc\Classes\SomfyMessaging;

class CustomerEU{

    function __construct(){
        $this->userDefaults = array(
            "custentity_customertype" => "8",
            "taxitem" => "9",
            "pricelevel" => "2",
            "custentity_financialstatus" => "1",
            "custentity_codeification" => "9",
            "custentity_geocust" => "2",
            "custentity_hirarchy" => "2",
            "receivablesaccount" => "6",
            "custentity_customeractivity" => "4",
            "custentity_customertypegroup" => "5",
            "currencyPrim" => "1",
            "currency" => "1",
            "subsidiary" => "2",
            "salesrep" => "329"
        );
    }

    
    public function syncNewRegisteredUser($user){
        $NetSuiteApi = new NetSuiteApiEU();
        
        $userData = array(
            'user_id' => $user->ID,
            'email' => $user->user_email,
            'companyname' => $user->nickname,
            'isperson' => 'T',
            'first_name' => isset(get_user_meta($user->ID, 'first_name')[0]) ? get_user_meta($user->ID, 'first_name')[0] : '',
            'last_name' => isset(get_user_meta($user->ID, 'last_name')[0]) ? get_user_meta($user->ID, 'last_name')[0] : '',
            'phone' => isset(get_user_meta($user->ID, 'phone')[0]) ? get_user_meta($user->ID, 'phone')[0] : '',
            'custentity_il_mobile' => isset(get_user_meta($user->ID, 'custentity_il_mobile')[0]) ? get_user_meta($user->ID, 'custentity_il_mobile')[0] : '',
            'custentity_customertype' => isset(get_user_meta($user->ID, 'custentity_customertype')[0]) ? get_user_meta($user->ID, 'custentity_customertype')[0] : '',
            'taxitem' => isset(get_user_meta($user->ID, 'taxitem')[0]) ? get_user_meta($user->ID, 'taxitem')[0] : '',
            'pricelevel' => isset(get_user_meta($user->ID, 'pricelevel')[0]) ? get_user_meta($user->ID, 'pricelevel')[0] : '',
            'custentity_financialstatus' => isset(get_user_meta($user->ID, 'custentity_financialstatus')[0]) ? get_user_meta($user->ID, 'custentity_financialstatus')[0] : '',
            'custentity_codeification' => isset(get_user_meta($user->ID, 'custentity_codeification')[0]) ? get_user_meta($user->ID, 'custentity_codeification')[0] : '',
            'custentity_geocust' => isset(get_user_meta($user->ID, 'custentity_geocust')[0]) ? get_user_meta($user->ID, 'custentity_geocust')[0] : '',
            'custentity_hirarchy' => isset(get_user_meta($user->ID, 'custentity_hirarchy')[0]) ? get_user_meta($user->ID, 'custentity_hirarchy')[0] : '',
            'receivablesaccount' => isset(get_user_meta($user->ID, 'receivablesaccount')[0]) ? get_user_meta($user->ID, 'receivablesaccount')[0] : '',
            'currencyPrim' => isset(get_user_meta($user->ID, 'currencyPrim')[0]) ? get_user_meta($user->ID, 'currencyPrim')[0] : '',
            'currency' => isset(get_user_meta($user->ID, 'currency')[0]) ? get_user_meta($user->ID, 'currency')[0] : '',
            'subsidiary' => isset(get_user_meta($user->ID, 'subsidiary')[0]) ? get_user_meta($user->ID, 'subsidiary')[0] : ''
        );
        
        $userSynced = $NetSuiteApi->newCustomer($userData);

        if($userSynced['status']){
            $internalid = $userSynced['createdRecordid'];
            update_user_meta( $user->ID, 'internalid', $internalid);
        } 

        // sign user to newsletter
        $isUserSubscribed = get_field('newsletter_subscribed', $user->ID, true);
        if(strlen($isUserSubscribed) > 0){
            // send email to somfy
            $SomfyMessaging = new SomfyMessaging();

            $sendTo = get_option('newsletter_sub__to');
            $subject = get_option('newsletter_sub__subject');
            $template = get_option('newsletter_sub__template');
            $htmlBody = $SomfyMessaging->getTemplate($template);
            
            $data = array(
                'loggedUser' => true,
                'userEmail' => $user->user_email
            );

            $messageSent = $SomfyMessaging->sendMail($sendTo, $subject, $htmlBody, $data);

            // send to customer
            $subject_cust = get_option('newsletter_sub__customer_subject');
            $template_cust = get_option('newsletter_sub__customer_template');
            $htmlBody_cust = $SomfyMessaging->getTemplate($template_cust);
            $messageSent_cust = $SomfyMessaging->sendMail($userEmail, $subject_cust, $htmlBody_cust, $data);

            // add client to InWise
            $clientAdded = $SomfyMessaging->addClient($userEmail, $userID);
        }

        $NetSuiteApi->somfylog('syncNewUser', array(
            'userSynced' => $userSynced
        ));
    }


    public function syncNewOrderUser($user){
        $NetSuiteApi = new NetSuiteApiEU();
        
        $userData = array(
            'user_id' => $user['ID'],
            'email' => $user['user_email'],
            'companyname' => $user['nickname'],
            'isperson' => 'T',
            'first_name' => isset(get_user_meta($user['ID'], 'first_name')[0]) ? get_user_meta($user['ID'], 'first_name')[0] : '',
            'last_name' => isset(get_user_meta($user['ID'], 'last_name')[0]) ? get_user_meta($user['ID'], 'last_name')[0] : '',
            'phone' => isset(get_user_meta($user['ID'], 'phone')[0]) ? get_user_meta($user['ID'], 'phone')[0] : '',
            'custentity_il_mobile' => isset(get_user_meta($user['ID'], 'custentity_il_mobile')[0]) ? get_user_meta($user['ID'], 'custentity_il_mobile')[0] : '',
            'custentity_customertype' => isset(get_user_meta($user['ID'], 'custentity_customertype')[0]) ? get_user_meta($user['ID'], 'custentity_customertype')[0] : '',
            'taxitem' => isset(get_user_meta($user['ID'], 'taxitem')[0]) ? get_user_meta($user['ID'], 'taxitem')[0] : '',
            'pricelevel' => isset(get_user_meta($user['ID'], 'pricelevel')[0]) ? get_user_meta($user['ID'], 'pricelevel')[0] : '',
            'custentity_financialstatus' => isset(get_user_meta($user['ID'], 'custentity_financialstatus')[0]) ? get_user_meta($user['ID'], 'custentity_financialstatus')[0] : '',
            'custentity_codeification' => isset(get_user_meta($user['ID'], 'custentity_codeification')[0]) ? get_user_meta($user['ID'], 'custentity_codeification')[0] : '',
            'custentity_geocust' => isset(get_user_meta($user['ID'], 'custentity_geocust')[0]) ? get_user_meta($user['ID'], 'custentity_geocust')[0] : '',
            'custentity_hirarchy' => isset(get_user_meta($user['ID'], 'custentity_hirarchy')[0]) ? get_user_meta($user['ID'], 'custentity_hirarchy')[0] : '',
            'receivablesaccount' => isset(get_user_meta($user['ID'], 'receivablesaccount')[0]) ? get_user_meta($user['ID'], 'receivablesaccount')[0] : '',
            'currencyPrim' => isset(get_user_meta($user['ID'], 'currencyPrim')[0]) ? get_user_meta($user['ID'], 'currencyPrim')[0] : '',
            'currency' => isset(get_user_meta($user['ID'], 'currency')[0]) ? get_user_meta($user['ID'], 'currency')[0] : '',
            'subsidiary' => isset(get_user_meta($user['ID'], 'subsidiary')[0]) ? get_user_meta($user['ID'], 'subsidiary')[0] : ''
        );
        
        $userSynced = $NetSuiteApi->newCustomer($userData);

        if($userSynced['status']){
            $internalid = $userSynced['createdRecordid'];
            update_user_meta( $user['ID'], 'internalid', $internalid);

            $NetSuiteApi->somfylog('syncNewUser', array(
                'userSynced' => $userSynced
            ));
            return array('status' => true, 'internalid' => $internalid, 'userSynced' => $userSynced);
        } else {
            return array('status' => false, 'userSynced' => $userSynced);
        }
    }

    public function updateCustomer($user_id){
        $NetSuiteApi = new NetSuiteApiEU();
        $user_info = get_userdata($user_id);

        $userData = array(
            'user_id' => $user_id,
            'email' => $user_info->user_email,
            'companyname' => $user_info->nickname,
            'internalid' => isset(get_user_meta($user_id, 'internalid')[0]) ? get_user_meta($user_id, 'internalid')[0] : '',
            'first_name' => isset(get_user_meta($user_id, 'first_name')[0]) ? get_user_meta($user_id, 'first_name')[0] : '',
            'last_name' => isset(get_user_meta($user_id, 'last_name')[0]) ? get_user_meta($user_id, 'last_name')[0] : '',
            'phone' => isset(get_user_meta($user_id, 'phone')[0]) ? get_user_meta($user_id, 'phone')[0] : '',
            'custentity_il_mobile' => isset(get_user_meta($user_id, 'custentity_il_mobile')[0]) ? get_user_meta($user_id, 'custentity_il_mobile')[0] : ''
        );
        
        $userSynced = $NetSuiteApi->updateCustomer($userData);

        $NetSuiteApi->somfylog('update user eu', array(
            'userData' => $userData,
            'userSynced' => $userSynced
        ));
    }

    public function getUserByInternalid($internalid){

        $my_user = new \WP_User_Query(
            array(
                'posts_per_page' => 1,
                'meta_query' => array(
                    array(
                        'key'     => 'internalid',
                        'value'   => $internalid,
                        'compare' => '='
                    )
                )
            ));
        if(isset($my_user->get_results()[0])) return $my_user->get_results()[0];
        return false;
    } 



    /** 
     * Description: createNewOrderCustomer in case of order without registration
     * @param
     * @return 
     */
    public function createNewOrderCustomer($customer){
        $customerRole = get_option('site_customer_role');

        $userData = array(
            'user_pass' => wp_generate_password( 10, true, false ), 
            'user_login' => $customer['email'],
            'show_admin_bar_front' => 'false',
            'role' => $customerRole,
            'first_name' => $customer['first_name'],
            'last_name' => $customer['last_name']
        );

        $user_id = wp_insert_user($userData);

        // On success.
        if ( ! is_wp_error( $user_id ) ) {
            update_user_meta( $user_id, 'account_status', 'awaiting_admin_review');
            update_user_meta( $user_id, 'primary_blog', 1);

            $userData['ID'] = $user_id;
            $userData['user_email'] = $customer['email'];
            $userData['nickname'] = $customer['first_name'] ." " .$customer['last_name'];

            $customerUpdated = $this->updateCustomerDate($user_id, $customer);

            // sync to erp
            $userSynced = $this->syncNewOrderUser($userData);

            if(!$userSynced['status']){
                return array(
                    'status' => false,
                    'error' => $user_id,
                    'userSynced' => $userSynced,
                    'message' => 'Could not sync new order user'
                );
            }

            return array(
                'status' => true,
                'user_id' => $user_id,
                'internalid' => $userSynced['internalid'],
                'customerUpdated' => $customerUpdated
            );
        } else {
            return array(
                'status' => false,
                'error' => $user_id,
                'message' => 'Could not create new order user'
            );
        }
    }

    /** 
     * Description: updateCustomerDate
     * @param
     * @return 
     */
    public function updateCustomerDate($customerID, $customer){
        
        // update userDefaults
        foreach($this->userDefaults as $key => $value){
            update_field($key, $value, 'user_'.$customerID);
        }

        $nickname = $customer['first_name'] ." " .$customer['last_name'];
        update_field('first_name', $customer['first_name'], 'user_'.$customerID);
        update_field('last_name', $customer['last_name'], 'user_'.$customerID);
        update_field('nickname', $nickname, 'user_'.$customerID);
        update_field('display_name', $nickname, 'user_'.$customerID);
        update_field('user_email', $customer['email'], 'user_'.$customerID);
        $args = array(
            'ID'         => $customerID,
            'user_email' => esc_attr( $customer['email'] )
        );
        wp_update_user( $args );
        return true;
    }

}