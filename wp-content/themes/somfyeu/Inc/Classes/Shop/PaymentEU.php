<?php
/**
 * @package SomfyEU
 */

namespace Inc\Classes\Shop;

class PaymentEU {
    public $dbPaymentLog;
 
    function __construct(){
        $this->dbPaymentLog = EU_SITE_PREFIX . "payment_log";
    }
        

    /** 
     * Description: newCartItem
     * @param
     * @return
     */
    public function newPaymentLog($userID, $orderid, $status, $method, $data){
        global $wpdb;
		$createDate = current_time( "Y-m-d H:i:s", 0 );
        $dataJson = json_encode($data);

        $query = "INSERT INTO $this->dbPaymentLog
			(
                `ID`, 
                `user_id`, 
                `internalid`, 
                `orderid`,
                `status`,
                `method`,
                `data`,
                `create_date`,
                `update_date`
            ) VALUES (
                NULL,
                '$userID', 
                NULL,
                '$orderid',
                '$status',
                '$method',
                '$dataJson',
                '$createDate',
                '$createDate'
            )";

        $res = $wpdb->query($query);
        return $wpdb->insert_id;
    }


    /** 
     * Description: updateCartItem
     * @param
     * @return
     */
    public function updatePaymentLog($logID, $status, $internalid){
        global $wpdb;
        
        $updateDate = current_time( "Y-m-d H:i:s", 0 );

        $query = "UPDATE $this->dbPaymentLog
            SET status = '$status',
                internalid = '$internalid',
                update_date = '$updateDate'
            WHERE ID = $logID";

        return $wpdb->query($query);
    }

}

