<?php
/**
 * @package SomfyEU
 */

namespace Inc\Classes\Shop;

use Inc\Classes\Shop\CouponEU;

class CartEU {
    public $dbCartItem;
 
    function __construct(){
        $this->dbCartItem = EU_SITE_PREFIX . "cartitem";
    }
    
    /** 
     * Description: getCartByUserID
     * @param $userID
     * @return cart and cart items
     */
    public function getCart($request){
        if ( !wp_verify_nonce( $_REQUEST['nonce'], NONCE_SECRET)) {
            exit("No naughty business please");
        }  

        // logged user
        $loggedUser = false;
        $userID = get_current_user_id();

        if(!$userID){
            $result['status'] = true;
            $result['user_logged'] = false;
            $result['message'] = 'משתמש לא מחובר';
            echo json_encode($result);
            die();
        }

        $userCart = $this->getCartByUser($userID);

        $userCartItems = array();
        if($userCart){
            $userCartItems = json_decode($userCart->cart_items);
        }

        $result['status'] = true;
        $result['user_logged'] = true;
        $result['cart_items'] = $userCartItems;
        echo json_encode($result);
        die();

    }


    /** 
     * Description: updateCart
     * @param
     * @return
     */
    public function updateCart($request){
        if ( !wp_verify_nonce( $_REQUEST['nonce'], NONCE_SECRET)) {
            exit("No naughty business please");
        }  

        //  validate params
        $cartItems = isset($_REQUEST['cartItems']) ? $_REQUEST['cartItems'] : null;
        $clear = isset($_REQUEST['clear']) ? $_REQUEST['clear'] : null;

        if(!isset($cartItems) || !isset($clear)){
            $result['status'] = false;
            $result['cartItems'] = $cartItems;
            $result['message'] = 'חלק מהנתונים חסרים';
            echo json_encode($result);
            die();
        }

        if($clear == "true"){
            $cartItems = '';
        }

        // logged user
        $loggedUser = false;
        $userID = get_current_user_id();

        if(!$userID){
            $result['status'] = false;
            $result['user_logged'] = false;
            $result['message'] = 'משתמש לא מחובר';
            echo json_encode($result);
            die();
        }
   
        // check if user have a cartItems record
        $userCart = $this->getCartByUser($userID);

        if($userCart){
            $cartUpdated = $this->updateCartItems($userID, $cartItems);
        } else {
            $cartUpdated = $this->newCartItem($userID, $cartItems);
        }
        
        $result['status'] = true;
        $result['user_logged'] = true;
        $result['cartItems'] = $cartItems;
        $result['cartUpdated'] = $cartUpdated;
        echo json_encode($result);
        die();
    }



    /** 
     * Description: getCartByUser
     * @param
     * @return
     */
    public function getCartByUser($userID){
        global $wpdb;

        $query = "SELECT * 
            FROM $this->dbCartItem
            WHERE user_id = '$userID'";

        return $wpdb->get_row($query);
    }



    /** 
     * Description: addCartItem
     * @param
     * @return
     */
    public function addCartItem($request){
        if ( !wp_verify_nonce( $_REQUEST['nonce'], NONCE_SECRET)) {
            exit("No naughty business please");
        }  

        //  validate params
        $productID = isset($_REQUEST['productID']) ? $_REQUEST['productID'] : null;
        $amount = isset($_REQUEST['amount']) ? $_REQUEST['amount'] : null;

        if(!isset($productID) || !isset($amount)){
            $result['status'] = false;
            $result['message'] = 'חלק מהנתונים חסרים';
            echo json_encode($result);
            die();
        }

        // logged user
        $loggedUser = false;
        $userID = get_current_user_id();

        if(!$userID){
            $result['status'] = false;
            $result['user_logged'] = false;
            $result['message'] = 'משתמש לא מחובר';
            echo json_encode($result);
            die();
        }

        $cartItemData = array(
            'productID' => $productID,
            'amount' => intval($amount)
        );

        // get current cart items from DB
        $userCartItemsRecord = $this->getCartByUser($userID);
        
        if(count($userCartItemsRecord) > 0){

            $userCartItems = json_decode($userCartItemsRecord[0]->cart_items);

            // cart not empty
            // check if cart item already exists
            $itemFound = false;

            if(!$userCartItems || count($userCartItems) == 0){
                $userCartItems = array();
            } else {
                for($i = 0; $i < count($userCartItems); $i++){
                    if($userCartItems[$i]->productID == $productID){
                        $itemFound = true;
                        // update found item
                        $newAmount = $userCartItems[$i]->amount + $amount;
                        $userCartItems[$i]->amount = $newAmount;
                        break;
                    }
                }
            }
            

            if(!$itemFound){
                // just add the item
                array_push($userCartItems, $cartItemData);
            }
           

            // update user cart items record
            $cartItems = $userCartItems;
            $cartUpdated = $this->updateCartItem($userID, $cartItems);
            $result['cartUpdated'] = $cartUpdated;
        } else {
            // no cart item exists for user
            // add new record
            $cartItems = array($cartItemData);
            $addedToCart = $this->newCartItem($userID, $cartItems);
            $result['addedToCart'] = $addedToCart;
        }

        $result['status'] = true;
        $result['user_logged'] = true;
        $result['cartItems'] = $cartItems;

        echo json_encode($result);
        die();

    }

    

    /** 
     * Description: newCartItem
     * @param
     * @return
     */
    public function newCartItem($userID, $cartItems){
        global $wpdb;
		$createDate = current_time( "Y-m-d H:i:s", 0 );

        $cartItemsJson = json_encode($cartItems);

        $query = "INSERT INTO $this->dbCartItem
			(
                `ID`, 
                `user_id`, 
                `cart_items`, 
                `create_date`, 
                `update_date`
            ) VALUES (
                NULL,
                '$userID', 
                '$cartItemsJson', 
                '$createDate',
                '$createDate'
            )";

        $res = $wpdb->query($query);
        return $wpdb->insert_id;
    }

    
    /** 
     * Description: updateCartItem
     * @param
     * @return
     */
    public function updateCartItems($userID, $cartItems){
        global $wpdb;
        
        $updateDate = current_time( "Y-m-d H:i:s", 0 );
        $cartItemsJson = json_encode($cartItems);

        $query = "UPDATE $this->dbCartItem
            SET cart_items = '$cartItemsJson', 
                update_date = '$updateDate'
            WHERE user_id = $userID";

        return $wpdb->query($query);
    }

}