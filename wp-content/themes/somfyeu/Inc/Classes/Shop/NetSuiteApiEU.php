<?php
/**
 * @package  SomfySiteManager
 */

namespace Inc\Classes\Shop;


class NetSuiteApiEU{

    /**
     * PRODUCTS END POINTS
     */
    
    /**
     * Description: Get Product stock
     * @param
     * @return
     */
    public function getProductStock($productID, $internalid){
        $script_ID = '672';
        $deploy_ID = '6';

        $auth_header = $this->getGETAuthHeaders($deploy_ID, $script_ID, $internalid);

        $ch = curl_init();

        curl_setopt($ch,CURLOPT_URL, NETSUITE_URL . '?&script=' . $script_ID . '&deploy=' . $deploy_ID . '&id=' . $internalid);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: ' . $auth_header,
            'Content-Type: application/json'
        ]);

        $res = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $response = json_decode($res, true);

        curl_close($ch);
        
        if(isset($response['error'])){
            return array(
                'status' => false,
                'http_code' => $http_code,
                'error' => $response['error']
            );
        } else {
            $currentStockQuantity = isset($response[0]['locationquantityavailable']) ? $response[0]['locationquantityavailable'] : 0;
            // update product stock
            update_field('stock_quantity', $currentStockQuantity, $productID);

            return array(
                'status' => true,
                'http_code' => $http_code,
                'currentStockQuantity' => $currentStockQuantity,
                'data' => $response
            );
        }
    }

/**
     * Description: getProductsPriceEU
     * @param
     * @return
     */
    public function getProductsPriceEU($productArray){
        $script_ID = '672';
        $deploy_ID = '11';

        $currency = 1;
        $qty = 0;
        $itemsString = implode(",",$productArray);

        $auth_header = $this->getGETAuthHeadersPriceEU($deploy_ID, $script_ID, $currency, $qty, $itemsString);
        $ch = curl_init();

        curl_setopt($ch,CURLOPT_URL, NETSUITE_URL . '?&script=' . $script_ID . '&deploy=' . $deploy_ID . '&currency=' . $currency . '&qty=' . $qty . '&item=' . $itemsString);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: ' . $auth_header,
            'Content-Type: application/json'
        ]);

        $res = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $response = json_decode($res, true);

        curl_close($ch);
        
        if(isset($response['error'])){
            return array(
                'status' => false,
                'http_code' => $http_code,
                'error' => $response['error']
            );
        } else {
            return array(
                'status' => true,
                'http_code' => $http_code,
                'data' => $response
            );
        }
    }
    /**
     * CUSTOMERS END POINTS
     */


    /**
     * Description: Get Customer by ID
     * @param
     * @return
     */
    public function getCustomerByID($internalid){
        $script_ID = '672';
        $deploy_ID = '3';

        $auth_header = $this->getGETAuthHeaders($deploy_ID, $script_ID, $internalid);

        $ch = curl_init();

        curl_setopt($ch,CURLOPT_URL, NETSUITE_URL . '?&script=' . $script_ID . '&deploy=' . $deploy_ID . '&id=' . $internalid);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: ' . $auth_header,
            'Content-Type: application/json'
        ]);

        $res = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $response = json_decode($res, true);

        curl_close($ch);
        
        if(isset($response['error'])){
            return array(
                'status' => false,
                'http_code' => $http_code,
                'error' => $response['error']
            );
        } else {
            return array(
                'status' => true,
                'http_code' => $http_code,
                'data' => $response
            );
        }
    }

    /**
     * Description: New Customer
     * @param
     * @return
     */
    public function newCustomer($newCustomerData){
        $script_ID = '671';
        $deploy_ID = '1';
  
        // get data from user
        $custJSON = '[{
            "recordtype": "customer",
            "externalid":"EU#' . $newCustomerData['user_id'] .'",
            "custentity_customertype":"' . $newCustomerData['custentity_customertype'] .'",
            "subsidiary":"' . $newCustomerData['subsidiary'] .'",
            "taxitem":"' . $newCustomerData['taxitem'] .'",
            "currencyPrim":"' . $newCustomerData['currencyPrim'] .'",
            "currency": "' . $newCustomerData['currency'] .'",
            "custentity_codeification":"' . $newCustomerData['custentity_codeification'] .'",
            "custentity_geocust":"' . $newCustomerData['custentity_geocust'] .'",
            "custentity_hirarchy":"' . $newCustomerData['custentity_hirarchy'] .'",
            "receivablesaccount":"' . $newCustomerData['receivablesaccount'] .'",
            "isperson" : "' . $newCustomerData['isperson'] .'",
            "custentity_customeractivity" : "4",
            "custentity_customertypegroup" : "5",
            "firstname" : "' .$newCustomerData['first_name'] .'",
            "lastname" : "' .$newCustomerData['last_name'] .'",
            "companyname":"' .$newCustomerData['companyname'] .'",
            "custentity_il_mobile":"' .$newCustomerData['custentity_il_mobile'] .'",
            "email":"' .$newCustomerData['email'] .'",
            "phone":"' .$newCustomerData['phone'] .'"
        }]';
        
        $auth_header = $this->getPOSTAuthHeaders($deploy_ID, $script_ID);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, NETSUITE_URL . '?&script=' . $script_ID . '&deploy=' . $deploy_ID . '&realm=' . NETSUITE_ACCOUNT);
        curl_setopt($ch, CURLOPT_POST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $custJSON);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: ' . $auth_header,
            'Content-Type: application/json',
            'Content-Length: ' . strlen($custJSON)
        ]);

        $res = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $response = json_decode($res, true);

        curl_close($ch);
        
        if(isset($response['error']) || $response[0]['isSuccess'] == false){
            $this->somfylog('error user sync', array(
                'http_code' => $http_code,
                'response' => $response,
                'error' => isset($response['error']) ? $response['error'] : $response[0]['message']
            ));
            return array(
                'status' => false,
                'http_code' => $http_code,
                'error' => isset($response['error']) ? $response['error'] : $response[0]['message']
            );
        } else {
            return array(
                'status' => true,
                'http_code' => $http_code,
                'data' => $response,
                'createdRecordid' => $response[0]['createdRecordid']
            );
        }
    }

    /**
     * Description: Update Customer data
     * @param
     * @return
     */
    public function updateCustomer($userData){
        $script_ID = '671';
        $deploy_ID = '1';
  
        // get data from user
        $custJSON = '[{
            "recordtype": "customer",
            "recordid":"' . $userData['internalid'] .'",
            "firstname" : "' .$userData['first_name'] .'",
            "lastname" : "' .$userData['last_name'] .'",
            "companyname":"' .$userData['companyname'] .'",
            "custentity_il_mobile":"' .$userData['custentity_il_mobile'] .'",
            "email":"' .$userData['email'] .'",
            "phone":"' .$userData['phone'] .'",
            "isperson" : "T"
        }]';
        $auth_header = $this->getPOSTAuthHeaders($deploy_ID, $script_ID);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, NETSUITE_URL . '?&script=' . $script_ID . '&deploy=' . $deploy_ID . '&realm=' . NETSUITE_ACCOUNT);
        curl_setopt($ch, CURLOPT_POST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $custJSON);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: ' . $auth_header,
            'Content-Type: application/json',
            'Content-Length: ' . strlen($custJSON)
        ]);

        $res = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $response = json_decode($res, true);

        curl_close($ch);
        
        if(isset($response['error'])){
            return array(
                'status' => false,
                'http_code' => $http_code,
                'error' => $response['error']
            );
        } else {
            return array(
                'status' => true,
                'http_code' => $http_code,
                'data' => $response
            );
        }
    }




    /**
     * ORDERS END POINTS
     */

    /**
     * Description: Get Order by ID
     * @param
     * @return
     */
    public function getOrderById($order_id){
        $script_ID = '672';
        $deploy_ID = '7';

        $auth_header = $this->getGETAuthHeaders($deploy_ID, $script_ID, $order_id);

        $ch = curl_init();

        curl_setopt($ch,CURLOPT_URL, NETSUITE_URL . '?&script=' . $script_ID . '&deploy=' . $deploy_ID . '&id=' . $order_id);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: ' . $auth_header,
            'Content-Type: application/json'
        ]);

        $res = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $response = json_decode($res, true);

        curl_close($ch);
        
        if(isset($response['error'])){
            return array(
                'status' => false,
                'http_code' => $http_code,
                'error' => $response['error']
            );
        } else {
            return array(
                'status' => true,
                'http_code' => $http_code,
                'data' => $response
            );
        }
    }

    /**
     * Description: Get Orders by entityid
     * @param
     * @return
     */
    public function getOrdersByEntityid($entityid){
        $script_ID = '672';
        $deploy_ID = '7';

        $auth_header = $this->getGETAuthHeadersByEntityid($deploy_ID, $script_ID, $entityid);

        $ch = curl_init();

        curl_setopt($ch,CURLOPT_URL, NETSUITE_URL . '?&script=' . $script_ID . '&deploy=' . $deploy_ID . '&entityid=' . $entityid);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: ' . $auth_header,
            'Content-Type: application/json'
        ]);

        $res = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $response = json_decode($res, true);

        curl_close($ch);
        
        if(isset($response['error'])){
            return array(
                'status' => false,
                'http_code' => $http_code,
                'error' => $response['error']
            );
        } else {
            return array(
                'status' => true,
                'http_code' => $http_code,
                'data' => $response
            );
        }
    }

    /**
     * Description: New Order
     * @param
     * @return
     */
    public function newOrder($orderCreated, $order_args, $entity, $orderDefaults){
        $script_ID = '671';
        $deploy_ID = '1';

        // prep order data
        $newOrderID = $orderCreated['newOrderID'];

        $itemsArr = array();

        $pricelevel = '2'; // EU Base Price
        $rate = get_option('shop_rate');

        $orderItems = $orderCreated['orderTotals']['products'];

        foreach($orderItems as $product){
            $quantity = intval($product['quantity']);
            
            array_push($itemsArr, array(
                "item" => $product['internalid'],
                "quantity" => $quantity,
                "pricelevel" => $pricelevel, 
                "custcol_originalitemrate" => $product['custcol_originalitemrate'],
                "rate" => $product['custcol_rate'],
                'tax1amt' => $product['custcol_tax1amt'],
                "amount" => $product['custcol_amount'],
                "custcol_discountpercentage" => $product['custcol_discountpercentage']
            ));
        }
         
        if($order_args['shipmethod'] == 'delivery'){
            $isPickup = 'F';
        } else {
            $isPickup = 'T';
        }

        // get data from user
        $orderPayload = array(
            array(
                "recordtype" => "salesorder",
                "custbody_pickup" => $isPickup,
                "custbody_websiteorder" => "T",
                "entity" => $entity,
                "externalid" => $orderCreated['externalid'],
                "memo" => "EU Site - test",
                "currencyPrim" => $orderDefaults['currencyPrim'],
                "subtotal" => $orderCreated['orderTotals']['subtotal'],
                "discounttotal" => $orderCreated['orderTotals']['discountamount'],
                "total" => $orderCreated['orderTotals']['total'],
                "terms" => $orderDefaults['terms'],
                "salesrep"  => $orderDefaults['salesrep'],
                "item" => $itemsArr,
                "billaddr1" => $order_args['billaddress1'],
                "billaddressee" => $order_args['billaddressee'],
                "billcity" => $order_args['billcity'],
                "billcountry" => $order_args['billcountry']
            )
        );


        if($order_args['shipmethod'] == 'delivery'){
            $orderPayload[0]['shipaddr1'] = $order_args['shipaddress1'];
            $orderPayload[0]['shipaddressee'] = $order_args['shipaddressee'];
            $orderPayload[0]['shipcity'] = $order_args['shipcity'];
            $orderPayload[0]['shipcountry'] = $order_args['shipcountry'];
        } else {
            $orderPayload[0]['shipaddr1'] = $order_args['billaddress1'];
            $orderPayload[0]['shipaddressee'] = $order_args['billaddressee'];
            $orderPayload[0]['shipcity'] = $order_args['billcity'];
            $orderPayload[0]['shipcountry'] = $order_args['billcountry'];
        }

        $orderJSON = json_encode($orderPayload);
        $this->somfylog('new order',  $orderJSON );
        $auth_header = $this->getPOSTAuthHeaders($deploy_ID, $script_ID);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, NETSUITE_URL . '?&script=' . $script_ID . '&deploy=' . $deploy_ID . '&realm=' . NETSUITE_ACCOUNT);
        curl_setopt($ch, CURLOPT_POST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $orderJSON);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: ' . $auth_header,
            'Content-Type: application/json',
            'Content-Length: ' . strlen($orderJSON)
        ]);

        $res = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $response = json_decode($res, true);

        curl_close($ch);
        
        if(isset($response['error']) && isset($response[0]['createdRecordid'])){
            return array(
                'status' => false,
                'http_code' => $http_code,
                'orderPayload' => $orderPayload,
                'error' => $response['error']
            );
        } else {
            // update local order title and internal ids
            update_field('internalid', $response[0]['createdRecordid'], $newOrderID);


            return array(
                'status' => true,
                'http_code' => $http_code,
                'data' => $response,
                'orderPayload' => $orderPayload,
                'createdRecordid' => $response[0]['createdRecordid']
            );
        }
    }



    /**
     * Description: New Deposit
     * @param
     * @return
     */
    public function newDeposit($entity, $salesorder, $payment, $paymentMethode, $paymentData, $isCredit){
        $script_ID = '673';
        $deploy_ID = '1';

        $depositPayload = array(
            array(
                "recordtype" => "salesorder",
                "torecord" => "customerdeposit",
                "recordid"=> $salesorder,
                "memo"=> "EU Test Deposit - $salesorder . ",
                "payment" => $payment,
                "paymentmethod" => $paymentMethode
        ));

        if($isCredit){
            $depositPayload[0]["ccnumber"] =  $paymentData['ResultData']['CreditCardNumber'];
            $depositPayload[0]["ccexpiredate"] =  substr($paymentData['ResultData']['CreditCardExpDate'],0,2) .'/'. substr($paymentData['ResultData']['CreditCardExpDate'],-2) ;
            // $depositPayload[0]["cczipcode"] =  "????";
            $depositPayload[0]["custbody_4_digits"] =  substr($paymentData['ResultData']['CreditCardNumber'], -4);
            // $depositPayload[0]["custbody_som_due_date"] =  "????";
            // $depositPayload[0]["custbody_id_number"] =  "????";
            $depositPayload[0]["custbody_payments_number"] =  $paymentData['ResultData']['TotalPayments'];
            // $depositPayload[0]["custbody_approve_number"] =  "????";
            $depositPayload[0]["custbody_pelecard_transaction_id"] =  $paymentData['ResultData']['TransactionPelecardId'];
            $depositPayload[0]["custbody_voucher_id"] =  $paymentData['ResultData']['VoucherId'];
            $depositPayload[0]["custbody_shva_file_number"] =  $paymentData['ResultData']['ShvaFileNumber'];
            // $depositPayload[0]["ccapproved"] =  "????";
        } else {
            if(isset($paymentData['purchase_units'][0]['payments']['captures'][0]['id'])){
                $captureID = $paymentData['purchase_units'][0]['payments']['captures'][0]['id'];
                $depositPayload[0]["memo"] = "EU Test Deposit - $salesorder . 
                    payment capture id: $captureID";
            }
        }

        $depositJSON = json_encode($depositPayload);
        
        $auth_header = $this->getPOSTAuthHeaders($deploy_ID, $script_ID);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, NETSUITE_URL . '?&script=' . $script_ID . '&deploy=' . $deploy_ID . '&realm=' . NETSUITE_ACCOUNT);
        curl_setopt($ch, CURLOPT_POST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $depositJSON);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: ' . $auth_header,
            'Content-Type: application/json',
            'Content-Length: ' . strlen($depositJSON)
        ]);

        $res = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $response = json_decode($res, true);

        curl_close($ch);
        
        if(isset($response['error']) && isset($response[0]['createdRecordid'])){
            return array(
                'status' => false,
                'http_code' => $http_code,
                'data' => $response,
                'error' => $response['error']
            );
        } else {

            return array(
                'status' => true,
                'http_code' => $http_code,
                'data' => $response,
                'depositPayload' => $depositPayload
            );
        }
    }

    // Restlet auth headers
    // Auth headers - GET with Modified Date
    protected function getGETAuthHeadersByModifiedDate($deploy_ID, $script_ID, $last_modified__arr){
        $oauth_nonce = md5(mt_rand());
        $oauth_timestamp = time();
        $oauth_signature_method = 'HMAC-SHA1';
        $oauth_version = "1.0";

        $base_string =
            "GET&" . urlencode(NETSUITE_URL) . "&" .
            urlencode(
                "deploy=" . $deploy_ID
                . "&edt=" . $last_modified__arr['edt']
                . "&eh=" . $last_modified__arr['eh']
                . "&em=" . $last_modified__arr['em']
                . "&endday=" . $last_modified__arr['endday']
                . "&oauth_consumer_key=" . NETSUITE_CONSUMER_KEY
                . "&oauth_nonce=" . $oauth_nonce
                . "&oauth_signature_method=" . $oauth_signature_method
                . "&oauth_timestamp=" . $oauth_timestamp
                . "&oauth_token=" . NETSUITE_TOKEN_ID
                . "&oauth_version=" . $oauth_version
                . "&script=" . $script_ID
                . "&sdt=" . $last_modified__arr['sdt']
                . "&sh=" . $last_modified__arr['sh']
                . "&sm=" . $last_modified__arr['sm']
                . "&startday=" . $last_modified__arr['startday']
            );

        $sig_string = urlencode(NETSUITE_CONSUMER_SECRET) . '&' . urlencode(NETSUITE_TOKEN_SECRET);
        $signature = base64_encode(hash_hmac("sha1", $base_string, $sig_string, true));

        $auth_header = "OAuth "
            . 'oauth_signature="' . rawurlencode($signature) . '", '
            . 'oauth_version="' . rawurlencode($oauth_version) . '", '
            . 'oauth_nonce="' . rawurlencode($oauth_nonce) . '", '
            . 'oauth_signature_method="' . rawurlencode($oauth_signature_method) . '", '
            . 'oauth_consumer_key="' . rawurlencode(NETSUITE_CONSUMER_KEY) . '", '
            . 'oauth_token="' . rawurlencode(NETSUITE_TOKEN_ID) . '", '  
            . 'oauth_timestamp="' . rawurlencode($oauth_timestamp) . '", '
            . 'realm="' . rawurlencode(NETSUITE_ACCOUNT) .'"';

        return $auth_header;
    }


    // Auth headers - GET with id 
    protected function getGETAuthHeadersByEntityid($deploy_ID, $script_ID, $entityid){
        $oauth_nonce = md5(mt_rand());
        $oauth_timestamp = time();
        $oauth_signature_method = 'HMAC-SHA1';
        $oauth_version = "1.0";

        $base_string =
            "GET&" . urlencode(NETSUITE_URL) . "&" .
            urlencode(
                "deploy=" . $deploy_ID
                . "&entityid=" . $entityid
                . "&oauth_consumer_key=" . NETSUITE_CONSUMER_KEY
                . "&oauth_nonce=" . $oauth_nonce
                . "&oauth_signature_method=" . $oauth_signature_method
                . "&oauth_timestamp=" . $oauth_timestamp
                . "&oauth_token=" . NETSUITE_TOKEN_ID
                . "&oauth_version=" . $oauth_version
                . "&script=" . $script_ID
            );

        $sig_string = urlencode(NETSUITE_CONSUMER_SECRET) . '&' . urlencode(NETSUITE_TOKEN_SECRET);
        $signature = base64_encode(hash_hmac("sha1", $base_string, $sig_string, true));

        $auth_header = "OAuth "
            . 'oauth_signature="' . rawurlencode($signature) . '", '
            . 'oauth_version="' . rawurlencode($oauth_version) . '", '
            . 'oauth_nonce="' . rawurlencode($oauth_nonce) . '", '
            . 'oauth_signature_method="' . rawurlencode($oauth_signature_method) . '", '
            . 'oauth_consumer_key="' . rawurlencode(NETSUITE_CONSUMER_KEY) . '", '
            . 'oauth_token="' . rawurlencode(NETSUITE_TOKEN_ID) . '", '  
            . 'oauth_timestamp="' . rawurlencode($oauth_timestamp) . '", '
            . 'realm="' . rawurlencode(NETSUITE_ACCOUNT) .'"';

        return $auth_header;
    }

    // Auth headers - GET with id 
    protected function getGETAuthHeaders($deploy_ID, $script_ID, $internalid){
        $oauth_nonce = md5(mt_rand());
        $oauth_timestamp = time();
        $oauth_signature_method = 'HMAC-SHA1';
        $oauth_version = "1.0";

        $base_string =
            "GET&" . urlencode(NETSUITE_URL) . "&" .
            urlencode(
                "deploy=" . $deploy_ID
                . "&id=" . $internalid
                . "&oauth_consumer_key=" . NETSUITE_CONSUMER_KEY
                . "&oauth_nonce=" . $oauth_nonce
                . "&oauth_signature_method=" . $oauth_signature_method
                . "&oauth_timestamp=" . $oauth_timestamp
                . "&oauth_token=" . NETSUITE_TOKEN_ID
                . "&oauth_version=" . $oauth_version
                . "&script=" . $script_ID
            );

        $sig_string = urlencode(NETSUITE_CONSUMER_SECRET) . '&' . urlencode(NETSUITE_TOKEN_SECRET);
        $signature = base64_encode(hash_hmac("sha1", $base_string, $sig_string, true));

        $auth_header = "OAuth "
            . 'oauth_signature="' . rawurlencode($signature) . '", '
            . 'oauth_version="' . rawurlencode($oauth_version) . '", '
            . 'oauth_nonce="' . rawurlencode($oauth_nonce) . '", '
            . 'oauth_signature_method="' . rawurlencode($oauth_signature_method) . '", '
            . 'oauth_consumer_key="' . rawurlencode(NETSUITE_CONSUMER_KEY) . '", '
            . 'oauth_token="' . rawurlencode(NETSUITE_TOKEN_ID) . '", '  
            . 'oauth_timestamp="' . rawurlencode($oauth_timestamp) . '", '
            . 'realm="' . rawurlencode(NETSUITE_ACCOUNT) .'"';

        return $auth_header;
    }


    // param - currency, qty, itemsString
    protected function getGETAuthHeadersPriceEU($deploy_ID, $script_ID, $currency, $qty, $itemsString){
        $oauth_nonce = md5(mt_rand());
        $oauth_timestamp = time();
        $oauth_signature_method = 'HMAC-SHA1';
        $oauth_version = "1.0";

        $base_string =
            "GET&" . urlencode(NETSUITE_URL) . "&" .
            urlencode(
                "currency=" . $currency
                . "&deploy=" . $deploy_ID
                . "&item=" . urlencode($itemsString)
                . "&oauth_consumer_key=" . NETSUITE_CONSUMER_KEY
                . "&oauth_nonce=" . $oauth_nonce
                . "&oauth_signature_method=" . $oauth_signature_method
                . "&oauth_timestamp=" . $oauth_timestamp
                . "&oauth_token=" . NETSUITE_TOKEN_ID
                . "&oauth_version=" . $oauth_version
                . "&qty=" . $qty
                . "&script=" . $script_ID
            );

        $sig_string = urlencode(NETSUITE_CONSUMER_SECRET) . '&' . urlencode(NETSUITE_TOKEN_SECRET);
        $signature = base64_encode(hash_hmac("sha1", $base_string, $sig_string, true));

        $auth_header = "OAuth "
            . 'oauth_signature="' . rawurlencode($signature) . '", '
            . 'oauth_version="' . rawurlencode($oauth_version) . '", '
            . 'oauth_nonce="' . rawurlencode($oauth_nonce) . '", '
            . 'oauth_signature_method="' . rawurlencode($oauth_signature_method) . '", '
            . 'oauth_consumer_key="' . rawurlencode(NETSUITE_CONSUMER_KEY) . '", '
            . 'oauth_token="' . rawurlencode(NETSUITE_TOKEN_ID) . '", '  
            . 'oauth_timestamp="' . rawurlencode($oauth_timestamp) . '", '
            . 'realm="' . rawurlencode(NETSUITE_ACCOUNT) .'"';

        return $auth_header;
    }


    protected function getPOSTAuthHeaders($deploy_ID, $script_ID){
        $oauth_nonce = md5(mt_rand());
        $oauth_timestamp = time();
        $oauth_signature_method = 'HMAC-SHA1';
        $oauth_version = "1.0";

        $base_string =
            "POST&" . urlencode(NETSUITE_URL) . "&" .
            urlencode(
                "deploy=" . $deploy_ID
            . "&oauth_consumer_key=" . NETSUITE_CONSUMER_KEY
            . "&oauth_nonce=" . $oauth_nonce
            . "&oauth_signature_method=" . $oauth_signature_method
            . "&oauth_timestamp=" . $oauth_timestamp
            . "&oauth_token=" . NETSUITE_TOKEN_ID
            . "&oauth_version=" . $oauth_version
            . "&realm=" . NETSUITE_ACCOUNT
            . "&script=" . $script_ID
            );

        $sig_string = urlencode(NETSUITE_CONSUMER_SECRET) . '&' . urlencode(NETSUITE_TOKEN_SECRET);
        $signature = base64_encode(hash_hmac("sha1", $base_string, $sig_string, true));

        $auth_header = "OAuth "
            . 'oauth_signature="' . rawurlencode($signature) . '", '
            . 'oauth_version="' . rawurlencode($oauth_version) . '", '
            . 'oauth_nonce="' . rawurlencode($oauth_nonce) . '", '
            . 'oauth_signature_method="' . rawurlencode($oauth_signature_method) . '", '
            . 'oauth_consumer_key="' . rawurlencode(NETSUITE_CONSUMER_KEY) . '", '
            . 'oauth_token="' . rawurlencode(NETSUITE_TOKEN_ID) . '", '  
            . 'oauth_timestamp="' . rawurlencode($oauth_timestamp) . '", '
            . 'realm="' . rawurlencode(NETSUITE_ACCOUNT) .'"';

        return $auth_header;
    }


    public function somfylog($title, $message){
		global $wpdb;
        $dbSomfylog = EU_SITE_PREFIX . "somfylog";

		$createDate = current_time( "Y-m-d H:i:s", 0 );
		$message = json_encode($message);
		
		$query = "INSERT INTO $dbSomfylog
			(
                `ID`, 
                `title`, 
                `message`, 
                `create_date`
            ) VALUES (
                NULL,
				'$title',
				'$message',
				'$createDate'
		)";

        $wpdb->query($query);
	}


}

