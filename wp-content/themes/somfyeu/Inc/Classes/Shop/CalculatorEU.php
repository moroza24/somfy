<?php
/**
 * @package SomfyEU
 */

namespace Inc\Classes\Shop;

class CalculatorEU {
    

    public function saveCalcInputs($orderID, $userID, $calcInputs){
        global $wpdb;
        $calc_db = EU_SITE_PREFIX . "calculator_inputs";
        $createDate = current_time( "Y-m-d H:i:s", 0 );

        $deleteOlderQuery = "DELETE FROM `sm_calculator_inputs` WHERE orderID = '".$orderID."' AND userID = '".$userID."'; ";
        $wpdb->query($deleteOlderQuery);


		$query = "
            INSERT INTO $calc_db
                (
                    `ID`, 
                    `orderID`, 
                    `userID`, 
                    `productID`, 
                    `userInput`, 
                    `create_date`
                ) VALUES ";

        for($i = 0; $i < count($calcInputs); $i++){
            $row = $calcInputs[$i];
            $userInput = json_encode($row['calc_userInput']);
            $query .= " (
                NULL,
				'".$orderID."',
				'".$userID."',
				'".$row['productID']."',
				'".$userInput."',
                '". $createDate."'
            ) ";

            if($i < count($calcInputs) - 1) $query .= " ,";
        }
        return $wpdb->query($query);
    }
}