<?php
/**
 * @package SomfyEU
 */

namespace Inc\Classes\Shop;

use Inc\Classes\Shop\ProductsEU;

class WishListEU {


    public $dbWishlist;
    public $templateURL;
 
    function __construct(){
        global $wpdb;
        $this->dbWishlist = EU_SITE_PREFIX . "wishlist";
        $this->templateURL = get_template_directory_uri();
    }


    /** 
     * Description: getByUser    
     * @param
     * @return
     */
    public function getByUser($request){
        if ( !wp_verify_nonce( $_REQUEST['nonce'], NONCE_SECRET)) {
            exit("No naughty business please");
         }  

        //  validate params
        $getFullData = isset($_REQUEST['getFullData']) ? $_REQUEST['getFullData'] : null;

        // logged user
        $loggedUser = false;
        $userID = get_current_user_id();

        if($userID){
            $loggedUser = true;
        } else {
            $result['status'] = false;
            $result['user_logged'] = false;
            $result['message'] = 'משתמש לא מחובר';
            echo json_encode($result);
            die();
        }

        $usersWishlist = $this->getByUserID($userID);

        if($usersWishlist && $getFullData){
            $Products = new ProductsEU();

            for($i = 0; $i < count($usersWishlist); $i++){
                $productID = $usersWishlist[$i]->product_id;
                $usersWishlist[$i]->post_title = get_the_title($productID);
                $usersWishlist[$i]->meta = get_fields($productID);
                $usersWishlist[$i]->permalink = get_the_permalink($productID);

                $usersWishlist[$i]->thumbnail_url = get_products_files($productID, 'thumbnail')['url'];
                $usersWishlist[$i]->warranty_applicable = $Products->checkIfWarrantyReleated($productID);
            }
        }

        $result['status'] = true;
        $result['usersWishlist'] = $usersWishlist;
        echo json_encode($result);
        die();

    }


    /** 
     * Description: addToWishlist    
     * @param
     * @return
     */
    public function addToWishlist($request){
        if ( !wp_verify_nonce( $_REQUEST['nonce'], NONCE_SECRET)) {
            exit("No naughty business please");
         }  

        //  validate params
        $productID = isset($_REQUEST['productID']) ? $_REQUEST['productID'] : null;

        if(!isset($productID)){
            $result['status'] = false;
            $result['message'] = 'חלק מהנתונים חסרים';
            echo json_encode($result);
            die();
        }

        // logged user
        $loggedUser = false;
        $userID = get_current_user_id();

        if($userID){
            $loggedUser = true;
        } else {
            $result['status'] = false;
            $result['user_logged'] = false;
            $result['message'] = 'משתמש לא מחובר';
            echo json_encode($result);
            die();
        }

        $addedToWishlist = $this->new($productID, $userID);

        $result['status'] = true;
        $result['addedToWishlist'] = $addedToWishlist;
        
        echo json_encode($result);
        die();
    }


    /** 
     * Description: removeFromWishlist    
     * @param
     * @return
     */
    public function removeFromWishlist($request){
        if ( !wp_verify_nonce( $_REQUEST['nonce'], NONCE_SECRET)) {
            exit("No naughty business please");
         }  

        //  validate params
        $productID = isset($_REQUEST['productID']) ? $_REQUEST['productID'] : null;

        if(!isset($productID)){
            $result['status'] = false;
            $result['message'] = 'חלק מהנתונים חסרים';
            echo json_encode($result);
            die();
        }


        // logged user
        $loggedUser = false;
        $userID = get_current_user_id();

        if($userID){
            $loggedUser = true;
        } else {
            $result['status'] = false;
            $result['user_logged'] = false;
            $result['message'] = 'משתמש לא מחובר';
            echo json_encode($result);
            die();
        }

        $removedToWishlist = $this->remove($productID, $userID);

        $result['status'] = true;
        $result['removedToWishlist'] = $removedToWishlist;
        
        echo json_encode($result);
        die();
    }


    /** 
     * Description: new    
     * @param
     * @return
     */
    public function new($productID, $userID){
        global $wpdb;
		$createDate = current_time( "Y-m-d H:i:s", 0 );

        $query = "INSERT INTO $this->dbWishlist
			(
                `ID`, 
                `user_id`, 
                `product_id`, 
                `create_date`
            ) VALUES (
                NULL,
                '$userID', 
                '$productID', 
                '$createDate'
            )";

        $res = $wpdb->query($query);
        return $wpdb->insert_id;
    }

 
    /** 
     * Description: getByUserID    
     * @param
     * @return
     */
    public function getByUserID($userID){
        global $wpdb;

        $query = "SELECT * 
            FROM $this->dbWishlist
            WHERE user_id = '$userID'";

        return $wpdb->get_results($query);
    }

 
    /** 
     * Description: remove    
     * @param
     * @return
     */
    public function remove($productID, $userID){
        global $wpdb;

        $sql = "DELETE FROM $this->dbWishlist
            WHERE product_id = '$productID' AND user_id = '$userID'";

        return $wpdb->query($wpdb->prepare($sql));
    }
}