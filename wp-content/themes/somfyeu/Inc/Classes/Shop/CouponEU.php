<?php
/**
 * @package SomfyEU
 */

namespace Inc\Classes\Shop;

use Inc\Classes\Shop\CartEU;

class CouponEU {
    
    public $dbPosts;
    public $dbPostmeta;
    public $dbCouponUsage;

    function __construct(){
        global $wpdb;
        $this->dbPosts = EU_SITE_PREFIX . "posts";
        $this->dbPostmeta = EU_SITE_PREFIX . "postmeta";
        $this->dbCouponUsage = EU_SITE_PREFIX . "coupon_usage";
    }


    public function getByID($coupon_id){

        $res = get_post($coupon_id, ARRAY_A);

        if($res){
            $res['meta'] = get_fields($coupon_id);
        }
        return $res;
    }


    /** 
     * Description: getByCode
     * @param $code
     * @return $coupon obj
     */
    public function getByCode($code){
        global $wpdb;

        $queryWhere = " AND LOWER(pm.meta_value) = LOWER('$code')";

        $query = "SELECT * 
            FROM $this->dbPosts p
            LEFT JOIN $this->dbPostmeta pm 
                ON pm.meta_key = 'coupon_code' AND pm.post_id = p.ID
            WHERE p.post_status = 'publish' 
                AND p.post_type = 'coupon'
                $queryWhere    
            ";

        $res = $wpdb->get_row($query);
        if($res){
            $res->meta = get_fields($res->ID);
        } 

        return $res;
    }


    /** 
     * Description: checkCouponValidity
     * @param
     * @return
     */
    public function checkCouponValidity($coupon_id, $user_id){
        $Cart = new Cart();
        $coupon = $this->getByID($coupon_id);
        // $cart = $Cart->getCartByUserID($user_id);

        // expiry date
        $expiryDate = $coupon['meta']['coupon_expiry_date'];
		$currentDate = current_time( "Y-m-d H:i:s", 0 );

        // Usage limit per coupon + Usage Counter
        $usageLimitPerCoupon = $coupon['meta']['usage_roles'][0]['usage_limit_per_coupon'];
        $usageCounter = $coupon['meta']['usage_counter'];

        if($usageCounter >= $usageLimitPerCoupon){
            return array(
                'isValid' => false,
                'errorMsg' => "Coupon reached his use limit"
            );
        }

        // Usage limit per user + coupon_usage (db)
        $usageLimitPerUser = $coupon['meta']['usage_roles'][0]['usage_limit_per_user'];
        $userCouponUsage = $this->getUserCouponUsage($coupon_id, $user_id);

        if($userCouponUsage >= $usageLimitPerUser){
            return array(
                'isValid' => false,
                'errorMsg' => "User reached his individual coupon use limit"
            );
        }

        // Allowed Users
        $allowedUsers = $coupon['meta']['usage_roles'][1]['allowed_users'];
        
        if (!$this->checkAllowedUser($user_id, $allowedUsers)) {
            return array(
                'isValid' => false,
                'errorMsg' => "User not alowed to use this coupon"
            );
        }
        
        // Minimum spend
        $minimumSpend = $coupon['meta']['usage_roles'][1]['minimum_spend'];

        // Maximum spend
        $maximumSpend = $coupon['meta']['usage_roles'][1]['maximum_spend'];

        return array(
            'isValid' => true
        );
    }

    
    /** 
     * Description: apllayCoupon
     * @param
     * @return
     */
    public function apllayCoupon(){
        /**
         * Discount Type
         * Coupon amount
         * Exclude sale items
         * Products
         * Exclude products
         * Product categories
         * Exclude categories
         */
    }


    /** 
     * Description: getUserCouponUsage
     * @param
     * @return
     */
    public function getUserCouponUsage($coupon_id, $user_id){
        global $wpdb;

        $query = "SELECT COUNT(ID) AS coupon_usage
            FROM $this->dbCouponUsage
            WHERE user_id = '$user_id'
                AND coupon_id = '$coupon_id'";

        $res = $wpdb->get_row($query);
        return $res->coupon_usage;
    }


    
    /** 
     * Description: updateCouponUsageLimit update the 
     * @param
     * @return
     */
    public function updateCouponUsageLimitCounter($coupon_id){
        $currentCount = get_field('usage_counter', $coupon_id);
        $currentCount = !$currentCount ? 0 : $currentCount;
        $newCount = $currentCount + 1 ;

        return update_field('usage_counter', $newCount, $coupon_id);
    }
    

     /** 
     * Description: updateCouponUserUsage
     * @param
     * @return
     */
    public function updateCouponUserUsage($user_id, $coupon_id, $order_id){
        global $wpdb;
		$createDate = current_time( "Y-m-d H:i:s", 0 );

        $query = "INSERT INTO $this->dbCouponUsage
			(
                `ID`, 
                `user_id`, 
                `coupon_id`, 
                `order_id`, 
                `create_date`
            ) VALUES (
                NULL,
                '$user_id', 
                '$coupon_id', 
                '$order_id', 
                '$createDate'
            )";

        $res = $wpdb->query($query);
        return $wpdb->insert_id;
    }


    /** 
     * Description: updateCouponUserUsage
     * @param
     * @return
     */
    public function checkAllowedUser($user_id, $allowedUsers){
        for($i=0; $i<count($allowedUsers); $i++){
            if($allowedUsers[$i] == $user_id){
                return TRUE;
            }
        }
        return FALSE;
    }
}