<?php
/**
 * @package SomfyEU
 */

namespace Inc\Classes\Shop;

use Inc\Classes\Shop\NetSuiteApiEU;
use Inc\Classes\Shop\CustomerEU;
use Inc\Classes\Shop\PaymentEU;
use Inc\Classes\Shop\PeleCardEU;
use Inc\Classes\Shop\CalculatorEU;
use Inc\Classes\SomfyMessaging;
use \Inc\Classes\ProductPricing;

class OrderEU {
    
    public $dbOrderItem;
    public $dbPosts;
    public $dbPostmeta;
    public $NetSuiteApiEU;

    function __construct(){
        global $wpdb;
        $this->dbOrderItem = EU_SITE_PREFIX . "orderitem";
        $this->dbPosts = EU_SITE_PREFIX . "posts";
        $this->dbPostmeta = EU_SITE_PREFIX . "postmeta";

        $this->NetSuiteApiEU = new NetSuiteApiEU();

        $this->orderDefaults = array(
            'custentity_customertype' => '8',
            'orderstatus' => 'A',
            'currency' => '1',
            'currencyPrim' => 'ILS',
            'terms' => '4',
            "salesrep" => "329"
        );
    }

    /******     AJAX     ******/

    /** 
     * Description: syncUserOrders
     * @param internalid
     * @return
     */
    public function syncUserOrders($request){
        if ( !wp_verify_nonce( $_REQUEST['nonce'], NONCE_SECRET)) {
            exit("No naughty business please");
         }  

        // validate logged user
        $userID = get_current_user_id();

        if(!$userID){
            $result['status'] = false;
            $result['user_logged'] = false;
            $result['message'] = 'משתמש לא מחובר';
            echo json_encode($result);
            die();
        }

        if( user_can( $userID, 'editor') || user_can( $userID, 'administrator') ) {  
            // Stuff here for administrators or editors
            $result['status'] = true;
            echo json_encode($result);
            die();
        }

        $user_internalid = isset(get_user_meta($userID, 'internalid')[0]) ? get_user_meta($userID, 'internalid')[0] : '';
        
        if($user_internalid == ""){
            if(!$userID){
                $result['status'] = false;
                $result['user_logged'] = true;
                $result['message'] = 'משתמש לא מסונכרן';
                echo json_encode($result);
                die();
            }
        }

        // get order from ERP
        $this->NetSuiteApiEU->somfylog('start - order sync: ' . $user_internalid, array($userID, $user_internalid));

        $userOrdersERP = $this->NetSuiteApiEU->getOrdersByEntityid($user_internalid);

        if($userOrdersERP['status']){
            if(count($userOrdersERP['data']) > 0 && is_array($userOrdersERP['data'])){
                // loop results and update orders and order items
                $ordersSynced = $this->updateOrdersFromERP($userOrdersERP['data'], $user_internalid);

                if($ordersSynced['status']){
                    // all updated
                    $this->NetSuiteApiEU->somfylog('end - order sync: ' . $user_internalid, array($userID, $user_internalid));

                    $result['status'] = true;
                    $result['userOrdersERP'] = $userOrdersERP;
                    $result['ordersSynced'] = $ordersSynced;
                    echo json_encode($result);
                    die();
                } else {
                    $this->NetSuiteApiEU->somfylog('error - order sync: ' . $user_internalid, array($userID, $user_internalid, $userOrdersERP, $ordersSynced));
                    $result['status'] = false;
                    $result['message'] = 'לא ניתן לעדכן את ההזמנה. ' . $ordersSynced['message'];
                    $result['userOrdersERP'] = $userOrdersERP;
                    $result['ordersSynced'] = $ordersSynced;
                    echo json_encode($result);
                    die();
                }
            } else {
                $this->NetSuiteApiEU->somfylog('end - order sync: ' . $user_internalid, array($userID, $user_internalid));
                $result['status'] = true;
                $result['message'] = 'לא נמצאו הזמנות';
                $result['userOrders'] = array();
                echo json_encode($result);
                die();
            }
        } else {
            $this->NetSuiteApiEU->somfylog('error - order sync: ' . $user_internalid, array($userID, $user_internalid, $userOrdersERP));
            $result['status'] = false;
            $result['message'] = 'לא ניתן לשלוף הזמנות מהמערכת';
            $result['error'] = $userOrdersERP;
            echo json_encode($result);
            die();
        }
        
    }



    /** 
     * Description: getOrdersByUser
     * @param internalid
     * @return
     */
    public function getUserOrders($request){
        if ( !wp_verify_nonce( $_REQUEST['nonce'], NONCE_SECRET)) {
            exit("No naughty business please");
         }  

        //  validate params
        $user_internalid = isset($_REQUEST['user_internalid']) ? $_REQUEST['user_internalid'] : null;
        if(!isset($user_internalid) || $user_internalid == ''){
            $result['status'] = false;
            $result['message'] = 'חלק מהנתונים חסרים';
            $result['user_internalid'] = $user_internalid;
            echo json_encode($result);
            die();
        }

        // validate logged user
        $userID = get_current_user_id();

        if(!$userID){
            $result['status'] = false;
            $result['user_logged'] = false;
            $result['message'] = 'משתמש לא מחובר';
            echo json_encode($result);
            die();
        }

        $userOrders = $this->getLocalUserOrders($user_internalid);
        $result['userOrders'] = $userOrders;

        if(count($userOrders) > 0){
            $result['status'] = true;
        } else {
            $result['status'] = false;
            $result['message'] = 'לא נמצאו הזמנות';
        }
        echo json_encode($result);
        die();
    }



    /** 
     * Description: getOrderDetails
     * @param internalid
     * @return
     */
    public function getOrderDetails($request){
        if ( !wp_verify_nonce( $_REQUEST['nonce'], NONCE_SECRET)) {
            exit("No naughty business please");
         }  

        //  validate params
        $order_tranid = isset($_REQUEST['order_tranid']) ? $_REQUEST['order_tranid'] : null;
        if(!isset($order_tranid)){
            $result['status'] = false;
            $result['message'] = 'חלק מהנתונים חסרים';
            echo json_encode($result);
            die();
        }

        // validate logged user
        $userID = get_current_user_id();

        if(!$userID){
            $result['status'] = false;
            $result['message'] = 'משתמש לא מחובר';
            echo json_encode($result);
            die();
        }

        // get items
        $orderItems = $this->getOrderItemsData($order_tranid);
        $orderInfo = $this->getOrderInfo($order_tranid, null);

        $result['status'] = true;
        $result['items'] = $orderItems;
        $result['orderInfo'] = $orderInfo;
        echo json_encode($result);
        die();
    }


    /** 
     * Description: newOrder
     * @param
     * @return
     */
    public function addNewOrder($request){
        if ( !wp_verify_nonce( $_REQUEST['nonce'], NONCE_SECRET)) {
            exit("No naughty business please");
        }  

        //  validate params
        $order_args = isset($_REQUEST['newOrder']) ? $_REQUEST['newOrder'] : null;
        
        $orderItems = isset($order_args['orderItems']) ? $order_args['orderItems'] : null;
        $customer_email = isset($order_args['customer_email']) ? $order_args['customer_email'] : null;
        $customer_firstname = isset($order_args['customer_firstname']) ? $order_args['customer_firstname'] : null;
        $customer_lastname = isset($order_args['customer_lastname']) ? $order_args['customer_lastname'] : null;
        $user_usage_agreement = isset($order_args['user_usage_agreement']) ? $order_args['user_usage_agreement'] : null;

        $calcItems = isset($_REQUEST['calcItems']) ? $_REQUEST['calcItems'] : null;
        
        if(!$order_args || !$orderItems || !$customer_email || !$customer_firstname || !$customer_lastname 
            || (!$user_usage_agreement || $user_usage_agreement == 'false')){
                $result['status'] = false;
                $result['order_args'] = $order_args;
                $result['message'] = 'חלק מהנתונים חסרים';
                echo json_encode($result);
                die();
        }
      
        // logged user
        $userID = get_current_user_id();
        $entity = '';
        $CustomerObj = new CustomerEU();
        $customerFormData = array(
            'email' => $customer_email,
            'first_name' => $customer_firstname,
            'last_name' => $customer_lastname,
            'nickname' => $customer_firstname . " " . $customer_lastname,
            'user_email' => $customer_email
        );
        
        if(!$userID){
            // check if user email exists
            $userEmailExists = email_exists($customer_email);
            
            if($userEmailExists){
                $userID = $userEmailExists;
            } else {
                // if not: create a new user
                $CustomerObj = new CustomerEU();
                $customerCreated = $CustomerObj->createNewOrderCustomer($customerFormData); 
                
                if(!$customerCreated['status']){
                    $result['status'] = false;
                    $result['message'] = 'לא ניתן ליצור לקוח חדש.' .$customerCreated['message'];
                    $result['customerCreated'] = $customerCreated;
                    echo json_encode($result);
                    die();
                } else {
                    $userID = $customerCreated['user_id'];
                }
            }
        }

        $entity = isset(get_user_meta($userID, 'internalid')[0]) ? get_user_meta($userID, 'internalid')[0] : '';
        
        if(is_null($entity) || $entity == ""){
            // user need to be synced
            $customerFormData['ID'] = $userID;
            $userSynced = $CustomerObj->syncNewOrderUser($customerFormData);

            if(!$userSynced['status']){
                $result['status'] = false;
                $result['message'] = 'לא ניתן לסנכרן לקוח.';
                $result['order_args'] = $order_args;
                $result['userSynced'] = $userSynced;
                $result['userID'] = $userID;
                $result['entity'] = $entity;
                echo json_encode($result);
                die();
            } else {
                $entity = $userSynced['internalid'];
            }
        }

        // check calcItems
        $haveCalcItems = false;
        if(is_array($calcItems) && count($calcItems) > 0){
            $haveCalcItems = true;
        }
        
        $orderCleared = $this->clearOlderOrders($entity);
        $orderCreated = $this->newOrderFromSite($order_args, $entity);
        $calcInputsSaved= '';

        if($orderCreated['status']){

            // save calc inputs
            if($haveCalcItems){
                $CalculatorEu = new CalculatorEU();
                $calcInputsSaved = $CalculatorEu->saveCalcInputs($orderCreated['newOrderID'], $userID, $calcItems);
            }

            $result['status'] = true;
        } else {
            $result['status'] = false;
            $result['message'] = 'לא ניתן ליצור הזמנה חדשה. ' .$orderCreated['message'];
        }

        $result['haveCalcItems'] = $haveCalcItems;
        $result['calcInputsSaved'] = $calcInputsSaved;
        $result['order_args'] = $order_args;
        $result['orderCleared'] = $orderCleared;
        $result['orderCreated'] = $orderCreated;
        $result['userID'] = $userID;
        $result['entity'] = $entity;
        echo json_encode($result);
        die();

    }

/** 
     * Description: newOrderPayment and sync to ERP
     * @param
     * @return
     */
    public function newOrderPayment($request){
        if ( !wp_verify_nonce( $_REQUEST['nonce'], NONCE_SECRET)) {
            exit("No naughty business please");
        }  

        //  validate params
        $order_args = isset($_REQUEST['order_args']) ? $_REQUEST['order_args'] : null;
        $orderCreated = isset($_REQUEST['orderCreated']) ? $_REQUEST['orderCreated'] : null;
        $userID = isset($_REQUEST['userID']) ? $_REQUEST['userID'] : null;
        $entity = isset($_REQUEST['entity']) ? $_REQUEST['entity'] : null;
        
        $payment_method = isset($_REQUEST['payment_method']) ? $_REQUEST['payment_method'] : null;
        $payment_details = isset($_REQUEST['payment_details']) ? $_REQUEST['payment_details'] : null;
        $pele_transactionId =  isset($_REQUEST['pele_transactionId']) ? $_REQUEST['pele_transactionId'] : null;

        // check calcItems
        $calcItems = isset($_REQUEST['calcItems']) ? $_REQUEST['calcItems'] : null;
        $haveCalcItems = false;
        if(is_array($calcItems) && count($calcItems) > 0){
            $haveCalcItems = true;
        }

        $customer_email = isset($order_args['customer_email']) ? $order_args['customer_email'] : null;
        $customer_firstname = isset($order_args['customer_firstname']) ? $order_args['customer_firstname'] : null;
        $customer_lastname = isset($order_args['customer_lastname']) ? $order_args['customer_lastname'] : null;
        $user_usage_agreement = isset($order_args['user_usage_agreement']) ? $order_args['user_usage_agreement'] : null;
        
        if(!$order_args || !$orderCreated || !$entity || !$payment_method  || !$userID || !$customer_email || !$customer_firstname || !$customer_lastname 
            || (!$user_usage_agreement || $user_usage_agreement == 'false')){
                $result['status'] = false;
                $result['req'] = $_REQUEST;
                $result['message'] = 'חלק מהנתונים חסרים';
                echo json_encode($result);
                die();
        }
      

        // Log payment data
        $PaymentEU = new PaymentEU();

        if($payment_method == 'paypal' && (is_null($payment_details) || $payment_details == '')){
            $result['status'] = false;
            $result['req'] = $_REQUEST;
            $result['message'] = 'חסרים פרטי תשלום - paypal';
            echo json_encode($result);
            die();
        } else if($payment_method == 'paypal' && !is_null($payment_details)){
            $isCredit = false;
        } else if($payment_method == 'credit'  && (is_null($pele_transactionId) || $pele_transactionId == '')){
            $result['status'] = false;
            $result['req'] = $_REQUEST;
            $result['message'] = 'חסרים פרטי תשלום - credit transactionId';
            echo json_encode($result);
            die();
        } else if($payment_method == 'credit' && $pele_transactionId != ''){
            // get credit payment data
            $PeleCard = new PeleCardEU();
            $payment_details = $PeleCard->pelecard_get_transaction($pele_transactionId);
            $isCredit = true;
        }
        
        // log payment
        $paymentLoged = $PaymentEU->newPaymentLog($userID, $orderCreated['newOrderID'], 'payment_recived', $payment_method, $payment_details);

        // update order is_payed
        update_field('is_payed', 1, $orderCreated['newOrderID']);


        // Send the order to ERP
        $ERPOrderCreated = $this->NetSuiteApiEU->newOrder(
            $orderCreated, 
            $order_args,
            $entity,
            $this->orderDefaults);

        // error_log(json_encode($ERPOrderCreated));

        if($ERPOrderCreated['status'] && !is_null($ERPOrderCreated['createdRecordid'])){
            // Log payment data
            $paymentLoged2 = $PaymentEU->updatePaymentLog($paymentLoged, 'order_synced', $ERPOrderCreated['createdRecordid']);
            
            // sync the new order
            $newOrderData = $this->NetSuiteApiEU->getOrderById($ERPOrderCreated['createdRecordid']);
                
            if($newOrderData['status']){
                $ordersSynced = $this->updateOrdersFromERP($newOrderData['data'], $entity);
            }

           
            // send deposit to ERP
            $paymentMethode = $this->getPaymentMethodID($payment_method, $payment_details);
            if(!$paymentMethode){
                // somfy log
                $this->NetSuiteApiEU->somfylog('getPaymentMethodID error', array(
                    'entity' => $entity, 
                    'createdRecordid' => $ERPOrderCreated['createdRecordid'], 
                    'total' => $orderCreated['orderTotals']['total'], 
                    'paymentMethode' => $paymentMethode, 
                    'payment_method' => $payment_method, 
                    'payment_details' => $payment_details,
                    'isCredit' => $isCredit
                ));

                // send notification to manager about error
                $this->sendOrderErrorEmail(array(
                    'message' => 'לא נמצא שיטת תשלום מתאימה',
                    'payment_method' => $payment_method, 
                    'paymentMethode' => $paymentMethode, 
                    'payment_details' => $payment_details,
                    'new_order_data' => $newOrderData
                ));
            } else {
                $depositSent = $this->NetSuiteApiEU->newDeposit(
                    $entity, 
                    $ERPOrderCreated['createdRecordid'], 
                    $orderCreated['orderTotals']['total'], 
                    $paymentMethode, 
                    $payment_details,
                    $isCredit
                );
    
                if($depositSent['status']){
                    $paymentLoged3 = $PaymentEU->updatePaymentLog($paymentLoged, 'payment_synced', $ERPOrderCreated['createdRecordid']);
                } else {
                    // error in deposit sync
                    // somfy log
                    $this->NetSuiteApiEU->somfylog('newDeposit error', array(
                        'entity' => $entity, 
                        'createdRecordid' => $ERPOrderCreated['createdRecordid'], 
                        'total' => $orderCreated['orderTotals']['total'], 
                        'paymentMethode' => $paymentMethode, 
                        'payment_details' => $payment_details,
                        'isCredit' => $isCredit,
                        'depositSent' => $depositSent
                    ));
    
                    // send notification to manager about error
                    $this->sendOrderErrorEmail(array(
                        'message' => 'שגיאה בשליחת תשלום ל-ERP',
                        'depositSent' => $depositSent,
                        'paymentMethode' => $paymentMethode, 
                        'payment_details' => $payment_details,
                        'new_order_data' => $newOrderData
                    ));
                }
    
            }

            
            // send order confirmation to customer
            $confirmationSent = $this->sendOrderConfirmationEmail($orderCreated['newOrderID'], $calcItems);

            $result['status'] = true;
            $result['newOrderTranid'] = $newOrderData['data'][0]['tranid'];
            $result['confirmationSent'] = $confirmationSent;
            $result['ERPOrderCreated'] = $ERPOrderCreated;
            $result['ordersSynced'] = $ordersSynced;
            $result['paymentMethode'] = $paymentMethode;
            $result['payment_method'] = $payment_method;
            $result['depositSent'] = $depositSent;
        } else {
            // send notification to manager about error
            $this->sendOrderErrorEmail(array(
                'message' => 'שגיאה בסנכרון הזמנה',
                'order_args' => $order_args, 
                'ERPOrderCreated' => $ERPOrderCreated, 
                'payment_details' => $payment_details,
                'payment_method' => $payment_method
            ));

            $result['status'] = false;
            $result['ERPOrderCreated'] = $ERPOrderCreated;
            $result['message'] = 'לא ניתן לסנכרן הזמנה.';
        }
        

        $result['order_args'] = $order_args;
        $result['orderCreated'] = $orderCreated;
        $result['entity'] = $entity;
        echo json_encode($result);
        die();

    }


    /****** order functions ******/

    /** 
     * Description: newOrderFromSite
     * @param
     * @return
     */
    public function newOrderFromSite($order, $entity = null){
        // error_log(json_encode($order));

        // validate shipping data
        if($order['shipmethod'] == 'delivery' && 
            ($order['shipaddressee'] == '' 
                || $order['shipaddress1'] == ''
                || $order['shipcity'] == ''
                || $order['shipcountry'] == '')){
            // error
            return array('status' => false, 'message' => 'Shipping data is missing');
        }


        // validate billing data
        if($order['paymentmethod'] == '' || 
            $order['billaddressee'] == '' 
            || $order['billaddress1'] == ''
            || $order['billcity'] == ''
            || $order['billcountry'] == ''){
            // error
            return array('status' => false, 'message' => 'Payment data is incomplate');
        }

        // validate customer data
        if($order['customer_email'] == ''){
            // error
            return array('status' => false, 'message' => 'Customer data is incomplate');
        }

        // validate order total and items
        $orderTotals = $this->getOrderSummeryAndStock($order['orderItems']);

        if($orderTotals['status']){
            // validate order summery
            if(intval($orderTotals['subtotal']) != intval($order['order_subSummary'])
                || intval($orderTotals['discountamount']) != intval($order['order_totalDiscount'])
                || intval($orderTotals['total']) != intval($order['order_totalPrice'])){
                // error
                return array('status' => false, 'message' => 'Order Total data is wrong', 'orderTotals' => $orderTotals);
            }
        } else {
            // error
            return array('status' => false, 'message' => $orderTotals['message']);
        }
        


        /** All Good continue **/
        
        // create new order post 
        $newOrderID = wp_insert_post(array(
            'post_type' => 'somfy_order'
        ));

        if($newOrderID){
            $externalid = 'EUSO#' . date("Y") . $newOrderID;
            $titleUpdateArr = array(
                'ID'           => $newOrderID,
                'post_title'   => $externalid,
            );
           
            // Update the post_title
            wp_update_post( $titleUpdateArr );
            update_field('externalid', $externalid, $newOrderID);
            update_field('is_payed', 0, $newOrderID);
            update_field('process_complated', 0, $newOrderID);
            update_field('is_synced', 0, $newOrderID);


            // update order ACF - shipping data
            update_field('shipmethod', $order['shipmethod'], $newOrderID);
            
            if($order['shipmethod'] == 'delivery'){
                $isPickup = 'F';
            } else {
                $isPickup = 'T';
            }
            update_field('custbody_pickup',$isPickup, $newOrderID);
            
            if($order['shipmethod'] == 'delivery'){
                update_field('shipaddressee', $order['shipaddressee'], $newOrderID);
                update_field('shipaddress1', $order['shipaddress1'], $newOrderID);
                update_field('shipcity', $order['shipcity'], $newOrderID);
                update_field('shipcountry', $order['shipcountry'], $newOrderID);
            } 


            // update order ACF - billing data
            update_field('billaddressee', $order['billaddressee'], $newOrderID);
            update_field('billaddress1', $order['billaddress1'], $newOrderID);
            update_field('billcity', $order['billcity'], $newOrderID);
            update_field('billcountry', $order['billcountry'], $newOrderID);


            // update customer data
            if($entity) update_field('entity', $entity, $newOrderID);
            update_field('customer_email', $order['customer_email'], $newOrderID);

            foreach($this->orderDefaults as $key => $value){
                update_field($key , $value, $newOrderID);
            }


            update_field('subtotal', $orderTotals['subtotal'], $newOrderID);
            update_field('discountamount', $orderTotals['discountamount'], $newOrderID);
            update_field('taxtotal', $orderTotals['taxtotal'], $newOrderID);
            update_field('total', $orderTotals['total'], $newOrderID);
        
            // create order items
            $itemsCreated = $this->addItemsToOrder($newOrderID, $orderTotals['products']);

            if(!$itemsCreated){
                wp_delete_post($newOrderID);
                return array(
                    'status' => false, 
                    'message' => 'Could not create order items',
                    'itemsCreated' => $itemsCreated,
                    'orderTotals' => $orderTotals
                );
            }

            // Finally we are done!
            return array(
                'status' => true, 
                'externalid' => $externalid,
                'newOrderID' => $newOrderID,
                'orderTotals' => $orderTotals,
                'itemsCreated' => $itemsCreated
            );
        } else {
            return array(
                'status' => false, 
                'message' => 'Canot create new order');
        }
    }

    

    /** 
     * Description: updateOrderFromERP for entity
     * @param orders:array
     * @param internalid:string
     * @return
     */
    public function updateOrdersFromERP($orders, $user_internalid){
        $errors = false;

        // error_log(json_encode($orders));
        // for loop to go throw the orders as $key => $value
        foreach ($orders as $order){
            $internalid = $order['internalid'];
           
            // check by internalid if order already exists
            $existingOrderID = $this->isOrderExists($internalid);
            
            if($existingOrderID){
                /** if exists:
                * update all ACF by $key => $value
                */
                $updatedOrder = $this->updateFromERP($order, $existingOrderID, $user_internalid);
                
                if(!$updatedOrder['status']){
                    return array(
                        'status' => false, 
                        'message' => 'somthing went wrong in creating the order from ERP ' . $updatedOrder['message']
                    );
                }
                /* update order items
                    * get array of the current order items ids
                    * create order items (new ones)
                        * if TRUE - remove previuse order items
                        * if FALSE - ERROR
                */
            } else {
                /** if not: 
                * create order post
                * update all ACF by $key => $value
                * create order items
                */ 
                $createdOrder = $this->newFromERP($order, $user_internalid);
                
                if(!$createdOrder['status']){
                    return array(
                        'status' => false, 
                        'message' => 'somthing went wrong in creating the order from ERP ' . $createdOrder['message']
                    );
                }
            }
        }
        return array(
            'status' => true,
            'message' => 'All Synced!'
        );       
    }


    /** 
     * Description: newFromERP order
     * @param
     * @return
     */
    public function newFromERP($order, $entity){
        // create new order post 
        $newOrderID = wp_insert_post(array(
            'post_type' => 'somfy_order'
        ));

        if($newOrderID){
            $titleUpdateArr = array(
                'ID'           => $newOrderID,
                'post_title'   => $order['tranid'],
            );
           
            // Update the order post_title
            wp_update_post( $titleUpdateArr );


            // Update ACF
            $fieldsToExclude = array('item', 'currencyPrim', 'discountamount', 'isSuccess', 'taxcode'); 
            foreach($order as $key => $value){
                if(in_array($key, $fieldsToExclude)) continue;

                update_field($key, $value, $newOrderID);
            }
            

            // update customer data
            $CustomerObj = new CustomerEU();
            $userData = $CustomerObj->getUserByInternalid($entity);
            $userEmail = $userData->data->user_email;
            update_field('customer_email', $userEmail, $newOrderID);
        
      

            // create order items
            $itemsCreated = $this->addOrderItemsFromERP($newOrderID,  $order['item']);

            if(!$itemsCreated){
                wp_delete_post($newOrderID);
                return array(
                    'status' => false, 
                    'message' => 'Could not create order items. ' . $itemsCreated['message'],
                    'itemsCreated' => $itemsCreated
                );
            }
            update_field('is_synced', 1, $newOrderID);

            // Finally we are done!
            return array(
                'status' => true, 
                'newOrderID' => $newOrderID,
                'itemsCreated' => $itemsCreated
            );
        } else {
            return array('status' => false, 'message' => 'Cannot create new order');
        }
    }


    /** 
     * Description: updateFromERP order
     * @param
     * @return
     */
    public function updateFromERP($order, $existingOrderID, $user_internalid){
       
        $titleUpdateArr = array(
            'ID'           => $existingOrderID,
            'post_title'   => $order['tranid'],
        );
        
        // Update the order post_title
        wp_update_post( $titleUpdateArr );


        // Update ACF
        $fieldsToExclude = array('item', 'currencyPrim', 'isSuccess', 'taxcode', 'shipmethod'); 
        foreach($order as $key => $value){
            if(in_array($key, $fieldsToExclude)) continue;

            update_field($key, $value, $existingOrderID);
        }
        

        // update customer data
        $CustomerObj = new CustomerEU();
        $userData = $CustomerObj->getUserByInternalid($user_internalid);

        $userEmail = $userData->data->user_email;
        update_field('customer_email', $userEmail, $existingOrderID);

        // create order items
        // get current order items
        $currentOrderItems = $this->getOrderItemsArray($existingOrderID, null);
        $itemsCreated = $this->addOrderItemsFromERP($existingOrderID,  $order['item']);

        if(!$itemsCreated){
            wp_delete_post($existingOrderID);
            return array(
                'status' => false, 
                'message' => 'Could not create order items. ' . $itemsCreated['message'],
                'itemsCreated' => $itemsCreated
            );
        } else {
            // delete current order items
            $this->deleteOldOrderItems($currentOrderItems);
        }
        update_field('is_synced', 1, $existingOrderID);

        // Finally we are done!
        return array(
            'status' => true, 
            'existingOrderID' => $existingOrderID,
            'itemsCreated' => $itemsCreated
        );
        
    }


    /** 
     * Description: getLocalUserOrders
     * @param
     * @return
     */
    public function getLocalUserOrders($user_internalid){
        // get post types
        $orders = new \WP_Query(array(
            'post_type' => 'somfy_order',
            'posts_per_page'   => -1,
            'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash'),  
            'order' => 'DESC',
            'orderby' => 'title',
            'meta_query' => array(
                array(
                    'key' => 'entity',
                    'compare' => '=',
                    'value' => $user_internalid
                )
            )
        ));

        $results = array();
        
        if($orders){
            while ($orders->have_posts()) {
                $orders->the_post();
                $orderID =  get_the_ID();
                array_push($results, array(
                    'ID' => $orderID,
                    'tranid' => get_field('tranid', $orderID),
                    'saleseffectivedate' => get_field('saleseffectivedate', $orderID),
                    'status' => get_field('status', $orderID)
                ));
            }
        }

        return $results;
    }


    /** 
     * Description: getOrderItemsArray
     * @param
     * @return
     */
    public function getOrderItemsArray($orderid, $tranid){
        global $wpdb;

        $whereQuery = 'WHERE 1=1';

        if($tranid) $whereQuery .= " AND tranid = '$tranid'";
        if($orderid) $whereQuery .= " AND orderid = '$orderid'";

        $query = "SELECT ID 
            FROM $this->dbOrderItem
            $whereQuery";

        $results = $wpdb->get_results($query);
        $finaleResults = array();

        if(count($results) >0){
            for($i = 0; $i < count($results); $i++){
                array_push($finaleResults, $results[$i]->ID);
            }
        }

        return $finaleResults;
    }

    /** 
     * Description: getOrderItemsData
     * @param
     * @return
     */
    public function getOrderItemsData($tranid){
        global $wpdb;

        $query = "SELECT 
                oi.*, 
                pd.ID AS productID, 
                pd.post_title, 
                pd.internalid
            FROM $this->dbOrderItem oi
            LEFT JOIN (
                SELECT p.ID, p.post_title, pm.meta_value AS internalid
                FROM $this->dbPosts p
                JOIN $this->dbPostmeta pm
                    ON pm.meta_key = 'internalid' AND pm.post_id = p.ID
                WHERE p.post_type = 'product'
            ) pd 
                ON pd.internalid = oi.item
            WHERE oi.tranid = '$tranid'";

        $results = $wpdb->get_results($query);
        
        if(count($results) > 0){
            for($i = 0; $i < count($results); $i++){
                $pid = $results[$i]->productID;
                $results[$i]->itemid = get_field('itemid', $pid);
                $results[$i]->permalink = get_the_permalink($pid);
                $results[$i]->isInternalItem = get_field('is_internal_item', $pid);
            }
        }
        return $results;
    }
    

    /** 
     * Description: getOrderInfo
     * @param
     * @return
     */
    public function getOrderInfo($tranid = null, $orderID = null){
        if(is_null($orderID)){
            $args = array(
                'post_type' => 'somfy_order',
                'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash'),  
                'posts_per_page'   => 1,
                'meta_query' => array(
                    array(
                        'key' => 'tranid',
                        'compare' => '=',
                        'value' => $tranid
                    )
                )
            );
        } else {
            $args = array(
                'p' => $orderID,
                'post_type' => 'somfy_order',
                'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash'),  
                'posts_per_page'   => 1
            );
        }
        $orderInfo = new \WP_Query($args);

        if(count($orderInfo->posts) > 0){
            $result = $orderInfo->posts[0];
            $result->acf = get_fields($result->ID, true);
            return $result;
        } else {
            return false;
        }
    }



    /** 
     * Description: deleteOldOrderItems
     * @param
     * @return
     */
    public function deleteOldOrderItems($orderItems){
        global $wpdb;

        $values = "('".implode("','",$orderItems)."')";


        $sql = "DELETE FROM $this->dbOrderItem
            WHERE ID IN $values";

        return $wpdb->query($sql);
    }



    /** 
     * Description: isOrderExists
     * @param
     * @return
     */
    public function isOrderExists($internalid){
        $userOrder = new \WP_Query(array(
            'post_type' => 'somfy_order',
            'posts_per_page'   => 1,
            'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash'),  
            'meta_query' => array(
                array(
                    'key' => 'internalid',
                    'compare' => '=',
                    'value' => $internalid
                )
            )
        ));
        return $userOrder->have_posts() ? $userOrder->post->ID : false;
    }

    
    /** 
     * Description: addItemsToOrder
     * @param
     * @return
     */
    public function addItemsToOrder($newOrderID, $products){
        global $wpdb;
        $createDate = current_time( "Y-m-d H:i:s", 0 );

        $query = "INSERT INTO $this->dbOrderItem
			(
                `ID`,
                `internalid`,
                `tranid`,
                `orderid`,
                `lineuniquekey`,
                `item`,
                `quantity`,
                `quantityshiprecv`,
                `quantitypicked`,
                `quantityfulfilled`,
                `quantitybilled`,
                `pricelevel`,
                `custcol_originalitemrate`,
                `rate`,
                `custcol_discountcode`,
                `custcol_discountpercentage`,
                `amount`,
                `taxamount`,
                `grossamount`,
                `closed`,
                `custbody_is_upgrade_required`,
                `custcol_awaitingcompletion`
            ) VALUES ";
        
        for ($i = 0; $i < count($products); $i++){
            $product = $products[$i];
            $item = $product['internalid'];
            $productID = $product['productID'];
            $pricelevel = '2'; // EU Base Price

            $custcol_originalitemrate = $product['custcol_originalitemrate'];
            $rate = $product['custcol_rate'];
            $quantity = $product['quantity'];

            $amount = $product['custcol_amount'];
            $taxamount = $product['custcol_tax1amt'];
            $grossamount = $amount + $taxamount;
            $discountpercentage = $product['custcol_discountpercentage'];
            
            if($i > 0) $query .= ",";
            $query .= "(
                NULL,
                NULL,
                NULL,
                '$newOrderID',
                '',
                '$item',
                '$quantity',
                '0',
                '0',
                '0',
                '0',
                '$pricelevel',
                '$custcol_originalitemrate',
                '$rate',
                '',
                '$discountpercentage',
                '$amount',
                '$taxamount',
                '$grossamount',
                false,
                false,
                false
            )";
        }

        $res = $wpdb->query($query);
        return $res;
    }
    

    /** 
     * Description: addOrderItemsFromERP
     * @param
     * @return
     */
    public function addOrderItemsFromERP($newOrderID, $products){
        global $wpdb;
        $shippingProductInternalID = get_option('shop_shipping_product_internalid');
        $createDate = current_time( "Y-m-d H:i:s", 0 );

        $query = "INSERT INTO $this->dbOrderItem
			(
                `ID`,
                `internalid`,
                `tranid`,
                `orderid`,
                `lineuniquekey`,
                `item`,
                `quantity`,
                `quantityshiprecv`,
                `quantitypicked`,
                `quantityfulfilled`,
                `quantitybilled`,
                `pricelevel`,
                `custcol_originalitemrate`,
                `rate`,
                `custcol_discountcode`,
                `custcol_discountpercentage`,
                `amount`,
                `taxamount`,
                `grossamount`,
                `closed`,
                `custbody_is_upgrade_required`,
                `custcol_awaitingcompletion`
            ) VALUES ";
        
        for ($i = 0; $i < count($products); $i++){
            if($i > 0) $query .= ",";

            $closed = $products[$i]['closed'] == "F" ? false : true;
            $custbody_is_upgrade_required = $products[$i]['custbody_is_upgrade_required'] == "F" ? false : true;
            $custcol_awaitingcompletion = $products[$i]['custcol_awaitingcompletion'] == "F" ? false : true;

            $quantityshiprecv = isset($products[$i]['quantityshiprecv']) ? $products[$i]['quantityshiprecv'] : 0;
            $quantitypicked = isset($products[$i]['quantitypicked']) ? $products[$i]['quantitypicked'] : 0;
            $quantityfulfilled = isset($products[$i]['quantityfulfilled']) ? $products[$i]['quantityfulfilled'] : 0;
            $quantitybilled = isset($products[$i]['quantitybilled']) ? $products[$i]['quantitybilled'] : 0;

            if($products[$i]['item'] == $shippingProductInternalID){
                $query .= "(
                    NULL,
                    '". $products[$i]['internalid'] ."',
                    '". $products[$i]['tranid'] ."',
                    '$newOrderID',
                    '". $products[$i]['lineuniquekey'] ."',
                    '". $products[$i]['item'] ."',
                    '". abs($products[$i]['quantity']) ."',
                    '". $quantityshiprecv ."',
                    '". $quantitypicked ."',
                    '". $quantityfulfilled ."',
                    '". $quantitybilled ."',
                    '". $products[$i]['pricelevel'] ."',
                    '". $products[$i]['custcol_originalitemrate'] ."',
                    '". $products[$i]['rate'] ."',
                    '". $products[$i]['custcol_discountcode'] ."',
                    '". $products[$i]['custcol_discountpercentage'] ."',
                    '". abs($products[$i]['amount']) ."',
                    '". $products[$i]['taxamount'] ."',
                    '". intval(abs($products[$i]['amount']) + intval($products[$i]['taxamount'])) ."',
                    '$closed',
                    '$custbody_is_upgrade_required',
                    '$custcol_awaitingcompletion'
                )";
            } else {
                $query .= "(
                    NULL,
                    '". $products[$i]['internalid'] ."',
                    '". $products[$i]['tranid'] ."',
                    '$newOrderID',
                    '". $products[$i]['lineuniquekey'] ."',
                    '". $products[$i]['item'] ."',
                    '". $products[$i]['quantity'] ."',
                    '". $quantityshiprecv ."',
                    '". $quantitypicked ."',
                    '". $quantityfulfilled ."',
                    '". $quantitybilled ."',
                    '". $products[$i]['pricelevel'] ."',
                    '". $products[$i]['custcol_originalitemrate'] ."',
                    '". $products[$i]['rate'] ."',
                    '". $products[$i]['custcol_discountcode'] ."',
                    '". $products[$i]['custcol_discountpercentage'] ."',
                    '". $products[$i]['amount'] ."',
                    '". $products[$i]['taxamount'] ."',
                    '". $products[$i]['Amount (Gross)'] ."',
                    '$closed',
                    '$custbody_is_upgrade_required',
                    '$custcol_awaitingcompletion'
                )";
            }

            
        }

        $res = $wpdb->query($query);
        return $res;
    }


    /** 
     * Description: getOrderSummeryAndStock
     * @param
     * @return
     */
    public function getOrderSummeryAndStock($orderItems){
        $rate = intval(get_option('shop_rate'));
        $response = array(
            'status' => false,
            'message' => '',
            'subtotal' => 0,
            'discountamount' => 0,
            'taxtotal' => 0,
            'total' => 0,
            'products' => array(),
            'errors' => array()
        );
        $internals = array();
        
        foreach($orderItems as $item){
            array_push($internals, get_field('internalid', $item['productID']));
        }

        // get updated prices
        $productPricesERP = $this->NetSuiteApiEU->getProductsPriceEU($internals);
        if($productPricesERP['status'] && is_array($productPricesERP['data'])){
            $productPricesData = $productPricesERP['data'];
            $updatePrices = false;
        } else {
            $response['status'] = false;
            $response['message'] = 'לא ניתן לוודא מחיר מוצר';
            return $response;
        }

        foreach($orderItems as $item){
            $productID = $item['productID'];
            $internalid = get_field('internalid', $productID);
            $isInternalItem = get_field('is_internal_item', $productID);

            // validate stock
            $currentStock = null;
            
            $currentStockQuantity = null;

            if(!$isInternalItem){
                $currentStock = $this->NetSuiteApiEU->getProductStock($productID, $internalid);

                if($currentStock['status']){
                    $currentStockQuantity = $currentStock['currentStockQuantity'];
                
                    if($item['amount'] > $currentStockQuantity){
                        array_push($response['errors'], array('title' => 'אין מספיק מלאי', 'message' => $currentStock));
                        $response['message'] .= "$productID - אין מספיק מלאי. ";
                        continue;
                    }
                } else {
                    array_push($response['errors'], array('title' => 'שגיאה בבדיקת מלאי', 'message' => $currentStock));
                    $response['message'] .= "$productID - שגיאה בבדיקת מלאי. ";
                    continue;
                }
            } 
            
            // calc totals
            $base_price = get_field('base_price', $productID);
            $base_price_no_rate = get_field('base_price_no_rate', $productID);
            $taxamount = get_field('taxamount', $productID);
            
            $on_sell = get_field('on_sell', $productID);
            $discount_price = intval(get_field('discount_price', $productID));

            // validate price
            $validePrice = true;
            foreach ($productPricesData as $pp){
                if($pp['item'] == $internalid){
                    $unitprice = $pp['unitprice'];
                    $taxAmount = round(($rate / 100) * $unitprice);
                    $productPrice = round($unitprice + $taxAmount);

                    if($productPrice != $base_price){
                        $validePrice = false;
                        break;
                    }
                }
            }

            if(!$validePrice){
                array_push($response['errors'], array('title' => 'invalid price', 'message' => "$internalid - invalid price. site=$base_price, erp=$productPrice"));
                $response['message'] .= "$internalid - invalid price. site=$base_price, erp=$productPrice";
                continue;
            }


            $custcol_originalitemrate = $base_price_no_rate;
            $custcol_rate = $base_price_no_rate;

            $finalPrice = intval($base_price) * intval($item['amount']);

            $response['subtotal'] += $finalPrice;

            $custcol_discountpercentage = 0.00;

            if($on_sell){
                $response['discountamount'] += (intval($base_price) - intval($discount_price)) * intval($item['amount']);
                
                $taxamount = round($discount_price - $discount_price / (1 + $rate / 100));
                $custcol_rate = $discount_price - $taxamount;

                $finalPrice = intval($discount_price) * intval($item['amount']);

                $custcol_discountpercentage = round(floatval(1 - $discount_price / $base_price) * 100, 2);
            
            }

            $custcol_tax1amt = $taxamount;
            $custcol_amount = intval($custcol_rate) * intval($item['amount']);

            $response['total'] += $finalPrice;


            array_push($response['products'], array(
                'productID' => $productID,
                'internalid' => $internalid,
                'quantity' => $item['amount'],
                'base_price' => $base_price,
                'base_price_no_rate' => $base_price_no_rate,
                'on_sell' => $on_sell,
                'discount_price' => $discount_price,
                'currentStockQuantity' => $currentStockQuantity,

                'custcol_originalitemrate' => $custcol_originalitemrate,
                'custcol_rate' => $custcol_rate,
                'custcol_tax1amt' => $custcol_tax1amt,
                'custcol_amount' => $custcol_amount,
                'custcol_discountpercentage' => $custcol_discountpercentage

            ));
        }


        if(count($response['errors']) == 0) $response['status'] = true;
        return $response;
    }
  

    /** 
     * Description: getItemsTableHtml
     * @param
     * @return
     */
    public function getItemsTableHtml($orderData, $itemsData){
        $html = "<style>
            th, td{
                border: 1px solid #ddd;
                padding: 8px;
                text-align: right;
            }
            thead{
                
            }
        </style>";

        $html .= "<table style='background: #fff; width: 100%; direction: rtl;border-collapse: collapse;'>
            <thead style='background: #000; color: #fff; padding: 8px;'>
                <tr>
                    <th>פריט</th>
                    <th>מק\"ט</th>
                    <th>כמות</th>
                    <th>סך ביניים</th>
                </tr>
            </thead>
            <tbody>";
            
        // print lines
        $haveShipping = false;
        $shippingTotal = 0;
        $shippingProductID = get_option('shop_shipping_product_id');

        foreach($itemsData as $item){
            if($item->productID == $shippingProductID){
                $haveShipping = true;
                $shippingTotal = $item->grossamount;
                continue;
            }
            $html .= "<tr>
                <td>$item->post_title</td>
                <td>$item->itemid</td>
                <td>$item->quantity</td>
                <td>$item->grossamount ₪</td>
            </tr>";
        }

        // print totals row
        $html .= "<tr style='background: #fef4dd;'>
                <td colspan='4'>
                    <span>סך ביניים: " .number_format($orderData['subtotal'],2) ." ₪</span><br>
                    <span>טיפול ומשלוח: " .number_format($shippingTotal, 2) ." ₪</span><br>
                    <span>הנחה: " .number_format($orderData['discounttotal'], 2) ." ₪</span><br>
                    <span>סה\"כ: " .number_format($orderData['total'],2) ." ₪</span>
                </td>
            </tr>";
        $html .= "</tbody>
        </table>";

        return $html;
    }



    /** 
     * Description: getCalcInputsHtml($calcItems)
     * @param
     * @return
     */
    public function getCalcInputsHtml($calcItems){
        $html = "<style>
        </style>";

        $html .= "<ul style='list-style: none; margin: 20px 0; padding: 0;'>";
            
        // print lines
        foreach($calcItems as $item){
            $productTitle = get_the_title($item['productID']);
            $html .= "<li>";

            $html .= "<span style='font-weight:bold;'>קלט מחשבון למוצר: " .$productTitle . "</span></br>";
            $html .= "<p style='margin-top: 0; padding-right: 5px;'>
                רוחב חלון: ". intval($item['calc_userInput']['windowWidth'] / 10) . " ס\"מ. " ."
                גובה חלון: ". intval($item['calc_userInput']['windowHeight'] / 10) . " ס\"מ. " ."
                סוג תריס: ". $item['calc_userInput']['blind_type'] . ". " ."
                גובה שלב: ". $item['calc_userInput']['blind_height'] . ". " ."
                טכנולוגיה: ".$item['calc_userInput']['tech'] . ". " ."
                </p>";

            $html .= "</li>";
        }

        // print totals row
        $html .= "</ul>";

        return $html;
    }


    /** 
     * Description: clearOlderOrders
     * @param
     * @return
     */
    public function clearOlderOrders($entity){
        $orders = new \WP_Query(array(
            'post_type' => 'somfy_order',
            'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash'),  
            'meta_query' => array(
                'relation' => 'AND',
                array(
                    'key' => 'entity',
                    'compare' => '=',
                    'value' => $entity
                ),
                array(
                    'key' => 'is_payed',
                    'compare' => '=',
                    'value' => 0
                ),
                array(
                    'key' => 'is_synced',
                    'compare' => '=',
                    'value' => 0
                )
            )
        ));

        if(count($orders->posts) > 0){
            $results = array();
            while ($orders->have_posts()) {
                $orders->the_post();
                $orderID =  get_the_ID();
                wp_trash_post($orderID);
                wp_delete_post($orderID);
                array_push($results, $orderID);
            }
            return $results;
        } else {
            return false;
        }
    }


    public function getPaymentMethodID($payment_method, $payment_details){
        $ppm_CreditCardBrand = array(
            '1' => '4',// Master Card
            '2' => '10'// VISA
        );

        $ppm_CreditCardCompanyIssuer = array(
            '2' => '10', // Master Card
            '4' => '6', // American Express
            '3' => '12' // DINERS
        );

        if($payment_method == 'paypal'){
            $res = '11';
        } else {
            // check for - Credit Card Brand
            $ccb = intval($payment_details['ResultData']['CreditCardBrand']);
            $res = isset($ppm_CreditCardBrand[$ccb]) ? $ppm_CreditCardBrand[$ccb] : false;
            
            // check for - Credit Card Company Issuer
            $ccci = intval($payment_details['ResultData']['CreditCardCompanyIssuer']);
            if(!$res){
                $res = isset($ppm_CreditCardCompanyIssuer[$ccci]) ? $ppm_CreditCardCompanyIssuer[$ccci] : false;
            }
        }
        return $res;
    }

    public function updateOrderProcessCompleted($tranid){
        $userOrder = new \WP_Query(array(
            'post_type' => 'somfy_order',
            'posts_per_page'   => 1,
            'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash'),  
            'meta_query' => array(
                array(
                    'key' => 'tranid',
                    'compare' => '=',
                    'value' => $tranid
                )
            )
        ));
        
        if($userOrder->have_posts()){
            $orderID = $userOrder->post->ID;
            $orderProcessCompleted = get_field('process_completed', $orderID);

            if($orderProcessCompleted == 1){
                return false;
            } else {
                update_field('process_completed', 1, $orderID);
                return true;
            }
        } else {
            return false;
        }
    }


    
    /** 
     * Description: sendOrderConfirmationEmail
     * @param
     * @return
     */
    public function sendOrderConfirmationEmail($order_id, $calcItems = null){
        date_default_timezone_set('Israel');
        
        $CustomerObj = new CustomerEU();

        // get order info
        $orderData = get_fields($order_id, true);

        // error_log(json_encode($orderData));

        // get customer data
        $entity = $orderData['entity'];
        $tranid = $orderData['tranid'];

        $customer = $CustomerObj->getUserByInternalid($entity);
        $userID = $customer->ID;
        $user_email = $customer->data->user_email;

        $customerData = array(
            'ID' => $userID,
            'user_email' => $user_email,
            'first_name' => isset(get_user_meta($userID, 'first_name')[0]) ? get_user_meta($userID, 'first_name')[0] : '',
            'last_name' => isset(get_user_meta($userID, 'last_name')[0]) ? get_user_meta($userID, 'last_name')[0] : '',
            'phone' => isset(get_user_meta($userID, 'phone')[0]) ? get_user_meta($userID, 'phone')[0] : '',
            'custentity_il_mobile' => isset(get_user_meta($userID, 'custentity_il_mobile')[0]) ? get_user_meta($userID, 'custentity_il_mobile')[0] : '',
            'companyname' => isset(get_user_meta($userID, 'companyname')[0]) ? get_user_meta($userID, 'companyname')[0] : '',
        );
        
        // get items
        $itemsData = $this->getOrderItemsData($tranid);

        $SomfyMessaging = new SomfyMessaging();

        // get html template and email subject
        $emailSubjectTemplate = get_option('order-confirmation__subject');
        $template = get_option('order-confirmation__template');
        $emailContentTemplate = $SomfyMessaging->getTemplate($template);

        // create items table html
        $items_table = $this->getItemsTableHtml($orderData, $itemsData);

        // prep calc inputs html if exists
        $calcHtml = '';
        if($calcItems && count($calcItems) > 0){
            $calcHtml = $this->getCalcInputsHtml($calcItems);
        }

        // replace placeholder with data
        $emailContentData = array(
            'order_tranid' => $tranid,
            'order_date' => date('d/m/Y H:i'),
            'items_table' => $items_table,
            'calcHtml' => $calcHtml,
            'first_name' => $customerData['first_name'],
            'last_name' => $customerData['last_name'],
            'customer_email' => $customerData['user_email'],
            'phone' => $customerData['phone'] != '' ? $customerData['phone'] : $customerData['custentity_il_mobile'],
            'billaddressee' => $orderData['billaddressee'],
            'billaddress1' => $orderData['billaddress1'],
            'billcity' => $orderData['billcity'],
            'billcountry' => $orderData['billcountry']['label'],
        );

        foreach($emailContentData as $key => $value){
            $emailContentTemplate = str_replace("{{".$key."}}", $value, $emailContentTemplate);
        }

        $emailSubjectTemplate = str_replace("{{order_tranid}}", $tranid, $emailSubjectTemplate);
        
        // send to customer
        $sendTo = $customerData['user_email'];
        $subject = $emailSubjectTemplate;
        $htmlBody = $emailContentTemplate;

        $messageSent = $SomfyMessaging->sendMail($sendTo, $subject, $htmlBody);


        // somfy log
        $this->NetSuiteApiEU->somfylog('sendOrderConfirmationEmail', array(
            'order_id' => $order_id,
            'customerData' => $customerData
        ));

        return $messageSent;
    }


/** 
     * Description: sendOrderErrorEmail
     * @param
     * @return
     */
    public function sendOrderErrorEmail($data){
        date_default_timezone_set('Israel');
        
        $SomfyMessaging = new SomfyMessaging();

        $to = get_option('somfy_error_mail_to');
        $sendTo = $to != '' ? $to : 'alex@interjet.co.il';
        $subject = "שגיאה בתהליך הזמנה";
        $htmlBody = "
            <h3>".$data['message']."</h3>
            <ul>
                <li>הזמנה: ".$data['new_order_data']['data'][0]['tranid']."</li>
                <li>אופן תשלום: ".$data['payment_method']."</li>
                <li>תשובה ממערכת תשלום: </li>
            </ul>
            <pre style='direction: ltr; background: #d4d4d4; text-align: left;'>". json_encode($data['payment_details'], JSON_PRETTY_PRINT)."</pre>";
        $messageSent = $SomfyMessaging->sendMail($sendTo, $subject, $htmlBody, null);


        // somfy log
        $this->NetSuiteApiEU->somfylog('sendOrderConfirmationEmail', array(
            'order_id' => $order_id,
            'orderData' => $orderData,
            'customerData' => $customerData,
            'itemsData' => $itemsData
        ));

        return $messageSent;
    }
}