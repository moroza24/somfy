<?php
/**
 * @package SomfyEU
 */

namespace Inc\Classes\Shop;

class PeleCardEU {
    
    /** 
     * Description: newCartItem
     * @param
     * @return
     */
    public function getIframeURL($request){
        if ( !wp_verify_nonce( $_REQUEST['nonce'], NONCE_SECRET)) {
            exit("No naughty business please");
         }  

        //  validate params
        $orderid = isset($_REQUEST['orderid']) ? $_REQUEST['orderid'] : null;
        $orderTotal = isset($_REQUEST['orderTotal']) ? $_REQUEST['orderTotal'] : null;

        if(is_null($orderid) || $orderid == '' || is_null($orderTotal) || $orderTotal == ''){
            $result['status'] = false;
            $result['message'] = 'חלק מהנתונים חסרים';
            $result['orderid'] = $orderid;
            $result['orderTotal'] = $orderTotal;
            echo json_encode($result);
            die();
        }
        
        // validate order totals
        $orderTotalFromDB = get_field('total' ,$orderid);
        $orderExternalID = get_field('externalid' ,$orderid);

        $result['orderid'] = $orderid;
        $result['orderTotal'] = $orderTotal;
        $result['orderTotalFromDB'] = $orderTotalFromDB;

        if(intval($orderTotal) != intval($orderTotalFromDB) || $orderExternalID == ''){
            $result['status'] = false;
            $result['message'] = 'בעיה ביצירת הזמנה, הזמנה לא נמצאה';
            echo json_encode($result);
            die();
        }

        $userKey = str_replace('EUSO#', '', $orderExternalID);
        $totalX100 = intval($orderTotal) * 100;
        $pele_res = $this->pelecard_get_iframe($totalX100, $orderExternalID);

        if($pele_res == false){
            $result['status'] = false;
            $result['message'] = 'לא ניתן לקבל קישור לתשלום';
            $result['pele_res'] = $pele_res;
            echo json_encode($result);
            die();
        } else {
            $result['status'] = true;
            $result['confirmationKey'] = $pele_res['ConfirmationKey'];
            $result['userKey'] = $orderExternalID;
            $result['totalX100'] = $totalX100;
            $result['iframeUrl'] = $pele_res['URL'];
            echo json_encode($result);
            die();
        }
        
    }


    /** 
     * Description: newCartItem
     * @param
     * @return
     */
    public function validatePayment($request){
        if ( !wp_verify_nonce( $_REQUEST['nonce'], NONCE_SECRET)) {
            exit("No naughty business please");
        }  
        
        //  validate params
        $transactionId = isset($_REQUEST['transactionId']) ? $_REQUEST['transactionId'] : null;

        if(is_null($transactionId) || $transactionId == ''){
            $result['status'] = false;
            $result['message'] = 'חלק מהנתונים חסרים';
            $result['transactionId'] = $transactionId;
            echo json_encode($result);
            die();
        }
        

        $pele_res = $this->pelecard_get_transaction($transactionId);
        $result['pele_res'] = $pele_res;

        if($pele_res['StatusCode'] == '000'){
            $result['status'] = true;
        } else if(in_array($pele_res['StatusCode'], array('555','510'))){
            $result['status'] = false;
            $result['message'] = 'תשלום בוטל';
        } else {
            $result['status'] = false;
            $result['message'] = 'תשלום לא אושר. נסה שוב';

        }

        echo json_encode($result);
        die();
    }

    /** 
     * Description: pelecard_get_iframe
     * @param
     * @return
     */
    public function pelecard_get_iframe($total, $userKey){
        $endPoint = 'https://gateway20.pelecard.biz/PaymentGW/init';

        $goodUrl = get_home_url() . "/checkout?credit_loading=true";
        
        $to = get_option('somfy_error_mail_to');
        $sendTo = $to != '' ? $to : 'alex@interjet.co.il';

        if($total / 100 <= 1000){
            $maxPayments = 3;
        } else if($total / 100 <= 2000){
            $maxPayments = 6;
        } else if($total / 100 > 2000){
            $maxPayments = 12;
        }

        $args = array(
            "terminal" => PELE_TERMINAL,
            "user" => PELE_USER,
            "password" => PELE_PASSWORD,
            "GoodURL" => $goodUrl,
            "ErrorURL" => $goodUrl,
            "CancelURL" => $goodUrl,
            "ActionType"=> "J4",
            "Currency" => "1",
            "Total" => $total,
            "UserKey" => $userKey,
            "CreateToken" => "True",
            "Language" => "HE",
            "CustomerIdField" => "must",
            "Cvv2Field" => "must",
            "MaxPayments" => $maxPayments,
            "MinPayments" => "1",
            "FirstPayment" => "auto",
            "ShopNo" => "001", // TODO:
            "ParamX" => $userKey, 
            "LogoURL" => get_bloginfo('template_url') ."/assets/img/somfy-logo.png",
            "NotificationGoodMail" => $sendTo,
            "NotificationErrorMail" => $sendTo,
            "NotificationFailMail" => $sendTo,
        );


        $curl = curl_init();


        curl_setopt_array($curl, array(
            CURLOPT_URL => $endPoint,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($args),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
            ),
            ));

        $response = curl_exec($curl);
		$info = curl_getinfo($curl);

		// check for errors
		if(!$info["http_code"] || $info["http_code"]!=200){
            return array(
                'status' => false, 
                'error' => 'Http Error', 
                'args' => $args, 
                'response' => $response
            );
        } 
        $res = json_decode($response, true);
        
        curl_close($curl);

        if($res['ConfirmationKey'] != '' && $res['Error']['ErrCode'] == 0){
            return $res;
        } else {
            return false;
        }
    }



    /** 
     * Description: pelecard_get_transaction
     * @param
     * @return
     */
    public function pelecard_get_transaction($transactionId){

        $endPoint = 'https://gateway20.pelecard.biz/PaymentGW/GetTransaction';

        $args = array(
            "terminal" => PELE_TERMINAL,
            "user" => PELE_USER,
            "password" => PELE_PASSWORD,
            "TransactionId" => $transactionId
        );

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $endPoint,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($args),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
            ),
            ));

        $response = curl_exec($curl);
		$info = curl_getinfo($curl);

		// check for errors
		if(!$info["http_code"] || $info["http_code"]!=200){
            return array(
                'status' => false, 
                'error' => 'Http Error', 
                'args' => $args, 
                'response' => $response
            );
        } 
        $res = json_decode($response, true);
        
        curl_close($curl);

        return $res;
    }
}

