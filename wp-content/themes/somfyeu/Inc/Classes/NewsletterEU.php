<?php
/**
 * @package SomfyEU
 */

namespace Inc\Classes;
use Inc\Classes\SomfyMessaging;

class NewsletterEU {
     /** 
     * Description: signUser to newsletter
     * @param
     * @return
     */
    public function signUser($request){
        if ( !wp_verify_nonce( $_REQUEST['nonce'], NONCE_SECRET)) {
            exit("No naughty business please");
         }  

        //  validate params
        $userEmail = isset($_REQUEST['userEmail']) ? $_REQUEST['userEmail'] : null;

        if(!isset($userEmail)){
            $result['status'] = false;
            $result['message'] = 'חלק מהנתונים חסרים';
            echo json_encode($result);
            die();
        }

        // logged user
        // update profile field - newsletter_subscribed
        $loggedUser = false;
        $userID = '';

        $userExists = get_user_by( 'email', $userEmail );
        if($userExists){
            $userID = $userExists->data->ID;

            update_field('newsletter_subscribed', true, $userID);
            $loggedUser = true;
        }

        // send email to somfy
        $SomfyMessaging = new SomfyMessaging();

        $sendTo = get_option('newsletter_sub__to');
        $subject = get_option('newsletter_sub__subject');
        $template = get_option('newsletter_sub__template');
        $htmlBody = $SomfyMessaging->getTemplate($template);

        $data = array(
            'loggedUser' => $loggedUser,
            'userEmail' => $userEmail
        );

        $messageSent = $SomfyMessaging->sendMail($sendTo, $subject, $htmlBody, $data);

        // send to customer
        $subject_cust = get_option('newsletter_sub__customer_subject');
        $template_cust = get_option('newsletter_sub__customer_template');
        $htmlBody_cust = $SomfyMessaging->getTemplate($template_cust);
        $messageSent_cust = $SomfyMessaging->sendMail($userEmail, $subject_cust, $htmlBody_cust, $data);

        // add client to InWise
        $clientAdded = $SomfyMessaging->addClient($userEmail, $userID);

        if($messageSent[0]['status'] == 'sent' && $messageSent_cust[0]['status'] == 'sent'){
            $result['status'] = true;
            $result['messageSent'] = $messageSent[0];
            $result['clientAdded'] = $clientAdded;
            $result['message'] = 'הרשמה עברה בהצלחה';
        } else {
            $result['status'] = false;
            $result['messageSent'] = $messageSent[0];
            $result['clientAdded'] = $clientAdded;
            $result['message'] = 'קרתה שגיאה, נסה שנית';
        }

        echo json_encode($result);
        die();
    }
}