<?php
/**
 * @package SomfyEU
 */

namespace Inc\Base;

use Inc\Classes\GlobalSearchEU;
use Inc\Classes\NewsletterEU;

use Inc\Classes\Shop\CartEU;
use Inc\Classes\Shop\OrderEU;
use Inc\Classes\Shop\PeleCardEU;
use Inc\Classes\Shop\ProductsEU;
use Inc\Classes\Shop\WishListEU;

class SomfyAjaxEU {
    public function register() {
        $GlobalSearch = new GlobalSearchEU();
        $Newsletter = new NewsletterEU();
        $Cart = new CartEU();
        $Order = new OrderEU();
        $PeleCard = new PeleCardEU();
        $Products = new ProductsEU();
        $Wishlist = new WishListEU();
        

        /**
         * Shop AJAX
         */
            add_action("wp_ajax_somfy_eu_shop_products", array( $Products, "getShopProducts")); // logged user
            add_action("wp_ajax_nopriv_somfy_eu_shop_products", array( $Products, "getShopProducts")); // not logged user

        
        /**
         * Global Search AJAX
         */

            add_action("wp_ajax_somfy_eu_global_search", array( $GlobalSearch, "globalSearch")); // logged user
            add_action("wp_ajax_nopriv_somfy_eu_global_search", array( $GlobalSearch, "globalSearch")); // not logged user


            add_action("wp_ajax_somfy_eu_full_search", array( $GlobalSearch, "fullSearch")); // logged user
            add_action("wp_ajax_nopriv_somfy_eu_full_search", array( $GlobalSearch, "fullSearch")); // not logged user


        /**
         * Newsletter AJAX
         */
            add_action("wp_ajax_somfy_eu_newsletter_signup", array( $Newsletter, "signUser")); // logged user
            add_action("wp_ajax_nopriv_somfy_eu_newsletter_signup", array( $Newsletter, "signUser")); // not logged user

        
        /**
         * StockNotifictaion AJAX
         */
            add_action("wp_ajax_somfy_eu_stock_notifictaion_signup", array( $Products, "stockNotifictaionSignup")); // logged user
            add_action("wp_ajax_nopriv_somfy_eu_stock_notifictaion_signup", array( $Products, "stockNotifictaionSignup")); // not logged user

                
        /**
         * Wishlist AJAX
         */
            // Add
            add_action("wp_ajax_somfy_eu_wishlist_add", array( $Wishlist, "addToWishlist")); // logged user
            add_action("wp_ajax_nopriv_somfy_eu_wishlist_add", array( $Wishlist, "addToWishlist")); // not logged user

            // getByUser
            add_action("wp_ajax_somfy_eu_get_by_user", array( $Wishlist, "getByUser")); // logged user
            add_action("wp_ajax_nopriv_somfy_eu_get_by_user", array( $Wishlist, "getByUser")); // not logged user

            // removeFromWishlist
            add_action("wp_ajax_somfy_eu_wishlist_remove", array( $Wishlist, "removeFromWishlist")); // logged user
            add_action("wp_ajax_nopriv_somfy_eu_wishlist_remove", array( $Wishlist, "removeFromWishlist")); // not logged user


        
        /**
         * Cart AJAX
         */

            // getCart
            add_action("wp_ajax_somfy_eu_get_cart", array( $Cart, "getCart")); // logged user
            add_action("wp_ajax_nopriv_somfy_eu_get_cart", array( $Cart, "getCart")); // not logged user

            // addToCart
            add_action("wp_ajax_somfy_eu_add_to_cart", array( $Cart, "addCartItem")); // logged user
            add_action("wp_ajax_nopriv_somfy_eu_add_to_cart", array( $Cart, "addCartItem")); // not logged user

            // getProductForCart
            add_action("wp_ajax_somfy_eu_get_cart_product_data", array( $Products, "getProductForCart")); // logged user
            add_action("wp_ajax_nopriv_somfy_eu_get_cart_product_data", array( $Products, "getProductForCart")); // not logged user

            // updateCart
            add_action("wp_ajax_somfy_eu_update_cart", array( $Cart, "updateCart")); // logged user
            add_action("wp_ajax_nopriv_somfy_eu_update_cart", array( $Cart, "updateCart")); // not logged user

            // updateCart
            add_action("wp_ajax_somfy_eu_check_warranty_releated", array( $Products, "checkIfWarrantyReleated")); // logged user
            add_action("wp_ajax_nopriv_somfy_eu_check_warranty_releated", array( $Products, "checkIfWarrantyReleated")); // not logged user
      
        
        /**
         * Order AJAX
         */

            // newOrder
            add_action("wp_ajax_somfy_eu_new_order", array( $Order, "addNewOrder")); // logged user
            add_action("wp_ajax_nopriv_somfy_eu_new_order", array( $Order, "addNewOrder")); // not logged user
        
            // newOrder payment
            add_action("wp_ajax_somfy_eu_new_order_payment", array( $Order, "newOrderPayment")); // logged user
            add_action("wp_ajax_nopriv_somfy_eu_new_order_payment", array( $Order, "newOrderPayment")); // not logged user
        
            // get credit iframe url
            add_action("wp_ajax_somfy_eu_get_iframe_url", array( $PeleCard, "getIframeURL")); // logged user
            add_action("wp_ajax_nopriv_somfy_eu_get_iframe_url", array( $PeleCard, "getIframeURL")); // not logged user
            
            // validate cart payment
            add_action("wp_ajax_somfy_eu_validate_card_payment", array( $PeleCard, "validatePayment")); // logged user
            add_action("wp_ajax_nopriv_somfy_eu_validate_card_payment", array( $PeleCard, "validatePayment")); // not logged user

            // get user orders
            add_action("wp_ajax_somfy_eu_user_orders", array( $Order, "getUserOrders")); // logged user
            add_action("wp_ajax_nopriv_somfy_eu_user_orders", array( $this, "somfy_must_login")); // not logged user

            // sync user orders
            add_action("wp_ajax_somfy_eu_sync_user_orders", array( $Order, "syncUserOrders")); // logged user
            add_action("wp_ajax_nopriv_somfy_eu_sync_user_orders", array( $this, "somfy_must_login")); // not logged user


            // get user orders_details
            add_action("wp_ajax_somfy_eu_order_details", array( $Order, "getOrderDetails")); // logged user
            add_action("wp_ajax_nopriv_somfy_eu_order_details", array( $this, "somfy_must_login")); // not logged user

        // check_user_logged
        add_action('wp_ajax_is_user_logged_in', array( $this, "ajax_check_user_logged_in"));
        add_action('wp_ajax_nopriv_is_user_logged_in', array( $this, "ajax_check_user_logged_in"));
    }

    function ajax_check_user_logged_in() {
        $isLoggedIn = is_user_logged_in()?'yes':'no';
        echo json_encode($isLoggedIn);
        die();
    }
    

    function somfy_must_login() {
        echo "You must be logged in!";
        die();
    }
}