<?php
/**
 * @package SomfyEU
 */

/* add new tab called "היסטוריית קניות" */
add_filter('um_account_page_default_tabs_hook', 'my_custom_tab_in_um', 100 );


// um

function my_custom_tab_in_um( $tabs ) {
	$tabs[800]['myProducts']['icon'] = 'fas fa-history';
	$tabs[800]['myProducts']['title'] = 'היסטוריית קניות';
	$tabs[800]['myProducts']['custom'] = true;
	$tabs[800]['myProducts']['show_button'] = false;
	
	return $tabs;
}
	
/* make our new tab hookable */
add_action('um_account_tab__myProducts', 'um_account_tab__myProducts');
function um_account_tab__myProducts( $info ) {
	global $ultimatemember;
	extract( $info );

	$output = $ultimatemember->account->get_tab_output('myProducts');
	if($output) { 
		echo $output; 
	}
}

/* Finally we add some content in the tab */

add_filter('um_account_content_hook_myProducts', 'um_account_content_hook_myProducts');
function um_account_content_hook_myProducts( $output ){
	ob_start();
	get_template_part('templates/shop/account_shoping_history');
		
	$output .= ob_get_contents();
	ob_end_clean();
	return $output;
}


// UM custom field - was-ambassador flag

add_action('um_after_register_fields', 'add_a_hidden_field_to_register');

function add_a_hidden_field_to_register( $args ) {
	$userDefaults = array(
		"custentity_customertype" => "8",
		"taxitem" => "9",
		"pricelevel" => "2",
		"custentity_financialstatus" => "1",
		"custentity_codeification" => "9",
		"custentity_geocust" => "2",
		"custentity_hirarchy" => "2",
		"receivablesaccount" => "6",
		"currencyPrim" => "1",
		"currency" => "1",
		"subsidiary" => "2",
		"salesrep" => "329"
	);

	foreach($userDefaults as $key => $value){
		echo "<input type='hidden' name='$key' id='$key' value='$value' />";
	}
}

add_action('um_after_account_general', 'show_extra_fields', 100);


function show_extra_fields() {
	$custom_fields = [
		"custentity_il_mobile" => "מספר נייד",
		"phone" => "טלפון"
	];

	foreach ($custom_fields as $key => $value) {

		$fields[ $key ] = array(
				'title' => $value,
				'metakey' => $key,
				'type' => 'select',
				'label' => $value,
		);

		apply_filters('um_account_secure_fields', $fields, 'general' );

		$field_value = get_user_meta(um_user('ID'), $key, true) ? : '';

		$html = '<div class="um-field um-field-'.$key.'" data-key="'.$key.'">
			<div class="um-field-label">
				<label for="'.$key.'">'.$value.'</label>
				<div class="um-clear"></div>
			</div>
			<div class="um-field-area">
				<input class="um-form-field valid "
					type="text" name="'.$key.'"
					id="'.$key.'" value="'.$field_value.'"
					placeholder=""
					data-validate="" data-key="'.$key.'">
			</div>
		</div>';

		echo $html;

	}
}