<?php
/**
 * @package SomfyEU
 */

use Inc\Base\SomfyAjaxEU;

$somfyAjax = new SomfyAjaxEU();
$somfyAjax->register();

if ( ! function_exists( 'somfyeu_setup' ) ) :

    function somfyeu_setup() {

        // Nav Register
        register_nav_menus(
                array(
                    'user' =>'User Menu' ,
                    'footer1' => 'Footer Menu 1',
                    'footer2' => 'Footer Menu 1'
                )
        );

        // Theme Features
        add_theme_support('title-tag');
        add_theme_support('post-thumbnails');
    }
endif;

add_action( 'after_setup_theme', 'somfyeu_setup' );