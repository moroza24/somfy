<?php
/**
 * @package SomfyEU
 */

flush_rewrite_rules( false );

// 
// Redirect Subscribers to root url
// 
add_action('admin_init', 'redirectSubs');


function redirectSubs(){
  $currentUser = wp_get_current_user();
  if(count($currentUser->roles) == 1 && $currentUser->roles[0] == 'subscriber'){
    wp_redirect(site_url('/'));
    exit;
  }
};



// 
// Hide admin bar from not admins
// 
add_action('wp_loaded', 'noAdminBar');


function noAdminBar(){
  $currentUser = wp_get_current_user();
  if(count($currentUser->roles) == 1 && ($currentUser->roles[0] == 'subscriber' || $currentUser->roles[0] == 'member')){
    show_admin_bar(false);
  }
};


// logout
function somfy_logout( $user_logout ) {
  set_transient( 'somfy_logout' , '1', 0 );
  wp_redirect ('./');
  exit();
}

add_action( 'wp_logout', 'somfy_logout' );


// localStorage
function somfy_footer() {
  global $current_user;
  wp_get_current_user();

  if ( ! get_transient('somfy_logout'))
      return;
  
  if ( ! is_user_logged_in() ){
    ?>
  <script type="text/javascript">
    localStorage.clear();
    console.log('logout');
  </script>
<?php
  } 
  delete_transient( 'somfy_logout' );
}

add_action( 'wp_footer', 'somfy_footer',20 );

