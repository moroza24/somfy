<?php
/**
 * @package SomfyEU
 */


add_action('init', 'custom_post_types');


function custom_post_types(){
    // Product
    register_post_type('product',array(
        'supports' => array('title','thumbnail'),
        'rewrite' => array('slug' => 'product'),
        'public' => true,
        'has_archive' => true,
        'labels' => array(
            'name' => 'Products',
            'add_new_item' => 'Add New Product',
            'edit_item' => 'Edit Product',
            'all_items' => 'All Products',
            'singular_name' => 'Product',
            'show_ui' => true
        ),
        'menu_icon' => 'dashicons-products'
    ));

    // Order
    register_post_type('somfy_order',array(
        'supports' => array('title'),
        'rewrite' => array('slug' => 'somfy_order'),
        'public' => true,
        'has_archive' => true,
        'labels' => array(
            'name' => 'Orders',
            'add_new_item' => 'Add New Order',
            'edit_item' => 'Edit Order',
            'all_items' => 'All Orders',
            'singular_name' => 'Order',
            'show_ui' => true
        ),
        'menu_icon' => 'dashicons-clipboard'
    ));

    // Coupon
    register_post_type('coupon',array(
        'supports' => array('title', 'editor'),
        'rewrite' => array('slug' => 'coupon'),
        'public' => true,
        'has_archive' => true,
        'labels' => array(
            'name' => 'Coupons',
            'add_new_item' => 'Add New Coupon',
            'edit_item' => 'Edit Coupon',
            'all_items' => 'All Coupons',
            'singular_name' => 'Coupon',
            'show_ui' => true
        ),
        'menu_icon' => 'dashicons-money-alt'
    ));

    // Product Category
    register_post_type('product_category',array(
        'show_in_menu' => 'edit.php?post_type=product',
        'supports' => array('title', 'thumbnail'),
        'rewrite' => array('slug' => 'product_category'),
        'public' => true,
        'has_archive' => true,
        'labels' => array(
            'name' => 'Product Categories',
            'add_new_item' => 'Add New Category',
            'edit_item' => 'Edit Category',
            'all_items' => 'All Categories',
            'singular_name' => 'Product Category',
            'show_ui' => true
        ),
        'menu_icon' => 'dashicons-money-alt'
    ));

    // Product Category
    register_post_type('product_subgroup',array(
        'show_in_menu' => 'edit.php?post_type=product',
        'supports' => array('title', 'editor', 'thumbnail'),
        'rewrite' => array('slug' => 'product_subgroup'),
        'public' => true,
        'has_archive' => true,
        'labels' => array(
            'name' => 'Product Subgroups',
            'add_new_item' => 'Add New Subgroup',
            'edit_item' => 'Edit Subgroup',
            'all_items' => 'All Subgroups',
            'singular_name' => 'Product Subgroup',
            'show_ui' => true
        ),
        'menu_icon' => 'dashicons-money-alt'
    ));

    //  product_2_subgroup
    register_post_type('product_2_subgroup',array(
        'show_in_menu' => 'edit.php?post_type=product',
        'supports' => array('title', 'editor', 'thumbnail'),
        'rewrite' => array('slug' => 'product_2_subgroup'),
        'public' => true,
        'has_archive' => true,
        'labels' => array(
            'name' => 'Product 2 Subgroups',
            'add_new_item' => 'Add New  2 Subgroup',
            'edit_item' => 'Edit  2 Subgroup',
            'all_items' => 'All  2 Subgroups',
            'singular_name' => 'Product  2 Subgroups',
            'show_ui' => true
        ),
        'menu_icon' => 'dashicons-money-alt'
    ));
}




// remove orderitems on order delete
function remove_order_items( $pid ) {
    global $wpdb;
    $dbOrderItem = EU_SITE_PREFIX . "orderitem";
    $dbPosts = EU_SITE_PREFIX . "posts";
   
    if(get_post_type($pid) != 'somfy_order') return;
    
    $getQuery = "SELECT ID FROM $dbPosts WHERE ID = '$pid'";
    $var = $wpdb->get_var( $getQuery );
  
    if ( $var ) {
        $query2 = "DELETE FROM $dbOrderItem WHERE orderid = '$pid'";
        $wpdb->query( $query2 );
    }
  
}
add_action( 'delete_post', 'remove_order_items', 10 );
  
add_filter('manage_product_posts_columns',  'product_columns_head');
add_action('manage_product_posts_custom_column',  'product_columns_content', 10, 2);
add_filter('manage_edit-product_sortable_columns',  'product_sortable');
add_action( 'pre_get_posts', 'product_orderby');



// ADD NEW COLUMN
function product_columns_head($defaults) {
    $defaults['itemid'] = 'itemid';
    $defaults['internalid'] = 'internalid';
    unset($defaults['date']);
    $defaults['date'] = 'date';
    return $defaults;
}

// SHOW THE itemid
function product_columns_content($column_name, $post_ID) {
    if ($column_name == 'itemid') {
        $post_itemid = get_post_meta($post_ID, 'itemid')[0];
        if ($post_itemid) {
            echo $post_itemid;
        }
    } else if ($column_name == 'internalid') {
        $post_internalid = get_post_meta($post_ID, 'internalid')[0];
        if ($post_internalid) {
            echo $post_internalid;
        }
    }
}

function product_sortable( $cols ) {
    $cols['itemid'] = 'itemid';
    $cols['internalid'] = 'internalid';
    return $cols;
}

function product_orderby( $query ) {
    if( ! is_admin() )
        return;

    $orderby = $query->get( 'orderby');

    if( 'itemid' == $orderby ) {
        $query->set('meta_key','itemid');
        $query->set('orderby','meta_value');
    } else if( 'internalid' == $orderby ) {
        $query->set('meta_key','internalid');
        $query->set('orderby','meta_value');
    }
}