<?php 
/**
 * @package SomfyEU
 */

use \Inc\Classes\Shop\CustomerEU;

add_action('wp_login','my_function', 10, 3);

function my_function($user_login, $user){
    $internalid = get_user_meta($user->ID, 'internalid');
    $role = $user->roles[0];
    $customerRole = get_option('site_customer_role');

    if($role == $customerRole){
        if(!isset($internalid) || (count($internalid) > 0 && $internalid[0] == '') || count($internalid) == 0){
            $CustomerEU = new CustomerEU();
            $CustomerEU->syncNewRegisteredUser($user);
        }
    }
}

add_action( 'profile_update', 'my_profile_update', 10, 2 );
 
function my_profile_update( $user_id, $old_user_data ) {
    $CustomerEU = new CustomerEU();
    $CustomerEU->updateCustomer($user_id);
}


// upda user custome fields
add_action('um_account_pre_update_profile', 'getUMFormData', 100);


function getUMFormData(){
  $id = um_user('ID');
  $names = array('custentity_il_mobile', 'phone');

  foreach( $names as $name )
    update_user_meta( $id, $name, $_POST[$name] );
}