<?php
/**
 * @package SomfyEU
 */


// 
// Import JS, CSS files into theme
// 
add_action('wp_enqueue_scripts', 'theme_files');

 
function theme_files() {
  // Bootstrap
  // wp_enqueue_style( 'bootstrapcss', 'https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css' );
  // wp_enqueue_script( 'jquery', 'https://code.jquery.com/jquery-3.3.1.slim.min.js', array(), '1.0.0', true);
  wp_enqueue_script( 'popper', 'https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js', array(), '1.0.0', true);
  wp_enqueue_script( 'bootstrapscript', 'https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js', array(), '1.0.0', true);
  
  // fontawesome
  wp_enqueue_style( 'fontawesome', 'https://use.fontawesome.com/releases/v5.7.1/css/all.css' ,array(), '1.0');	

  // CSS
  wp_enqueue_style('somfyeu-styles', get_stylesheet_uri());

  // JS
  wp_enqueue_script('somfyeu-scripts', get_theme_file_uri('/js/scripts-bundled.js'), NULL, '1.0', true);

  wp_localize_script('somfyeu-scripts', 'somfyData', array(

    'root_url' => get_site_url(),
    'template_url' => get_template_directory_uri(),
    'ajax_url' => admin_url('admin-ajax.php'),
    'ajax_nonce' => wp_create_nonce(NONCE_SECRET),
    'shop_warranty_product_id' => get_option('shop_warranty_product_id'),

  ));
}

