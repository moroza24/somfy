<?php /* Template Name: Somfy Contact */ ?>

<?php 
    get_header(); 
    wp_reset_postdata();
    get_template_part('templates/pages/contact');

    get_footer(); 
?>
