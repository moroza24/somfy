<?php if ( ! defined( 'ABSPATH' ) ) exit; ?>

<div class="um <?php echo esc_attr( $this->get_class( $mode ) ); ?> um-<?php echo esc_attr( $form_id ); ?>">

	<h2 class="section_title dark_title">עריכת חשבון</h1>

	<div class="um-form">
	
		<form method="post" action="">
			
			<?php
			/**
			 * UM hook
			 *
			 * @type action
			 * @title um_account_page_hidden_fields
			 * @description Show hidden fields on account form
			 * @input_vars
			 * [{"var":"$args","type":"array","desc":"Account shortcode arguments"}]
			 * @change_log
			 * ["Since: 2.0"]
			 * @usage add_action( 'um_account_page_hidden_fields', 'function_name', 10, 1 );
			 * @example
			 * <?php
			 * add_action( 'um_account_page_hidden_fields', 'my_account_page_hidden_fields', 10, 1 );
			 * function my_account_page_hidden_fields( $args ) {
			 *     // your code here
			 * }
			 * ?>
			 */
			do_action( 'um_account_page_hidden_fields', $args ); 


			$accountTabs = array();

			$tempTabs = UM()->account()->tabs;
			unset($tempTabs['privacy']); 
			
			foreach ( $tempTabs as $id => $info ) {
				if($id == 'general'){
					$info['title'] = 'חשבון';
					$info['submit_title'] = 'עדכן';
				} else if($id == 'delete'){
					$info['title'] = 'מחק חשבון';
					$info['submit_title'] = 'מחק';
				} else if($id == 'password'){
					$info['title'] = 'עדכן סיסמה';
					$info['submit_title'] = 'עדכן';
				}

				$accountTabs[$id] = $info;
			}
			?>

			
			<div class="um-account-side uimob340-hide uimob500-hide">
				<ul>
					<?php foreach ( $accountTabs as $id => $info ) {
						if ( isset( $info['custom'] ) || UM()->options()->get( "account_tab_{$id}" ) == 1 || $id == 'general' ) { ?>

							<li class="<?php if ( $id == UM()->account()->current_tab ) echo 'current_tab'; ?>">
								<a data-tab="<?php echo esc_attr( $id )?>" 
                                    href="<?php echo esc_url( UM()->account()->tab_link( $id ) ); ?>" 
                                    class="um-account-link <?php if ( $id == UM()->account()->current_tab ) echo 'current'; ?>">

									<span class="um-account-title uimob800-hide"><?php echo esc_html( $info['title'] ); ?></span>
									<span class="um-account-arrow uimob800-hide">
										<i class="<?php if ( is_rtl() ) { ?>um-faicon-angle-left<?php } else { ?>um-faicon-angle-right<?php } ?>"></i>
									</span>
								</a>
							</li>
						<?php }
					} ?>
				</ul>
			</div>
			
			<div class="um-account-main" data-current_tab="<?php echo esc_attr( UM()->account()->current_tab ); ?>">
			
				<?php
				
				do_action( 'um_before_form', $args );

				foreach ( $accountTabs as $id => $info ) {
					$current_tab = UM()->account()->current_tab;

					

					if ( isset( $info['custom'] ) || UM()->options()->get( 'account_tab_' . $id ) == 1 || $id == 'general' ) { ?>

						<div class="um-account-nav uimob340-show uimob500-show">
							<a href="javascript:void(0);" data-tab="<?php echo esc_attr( $id ); ?>" class="<?php if ( $id == $current_tab ) echo 'current'; ?>">
								<?php echo esc_html( $info['title'] ); ?>
								<span class="ico"><i class="<?php echo esc_attr( $info['icon'] ); ?>"></i></span>
								<span class="arr"><i class="um-faicon-angle-down"></i></span>
							</a>
						</div>

						<div class="um-account-tab um-account-tab-<?php echo esc_attr( $id ); ?>" data-tab="<?php echo esc_attr( $id  )?>">
							<?php $info['with_header'] = false;
							UM()->account()->render_account_tab( $id, $info, $args ); ?>
						</div>

					<?php }
				} ?>
				
			</div>
			<div class="um-clear"></div>
		</form>
		
		<?php
		/**
		 * UM hook
		 *
		 * @type action
		 * @title um_after_account_page_load
		 * @description After account form
		 * @change_log
		 * ["Since: 2.0"]
		 * @usage add_action( 'um_after_account_page_load', 'function_name', 10 );
		 * @example
		 * <?php
		 * add_action( 'um_after_account_page_load', 'my_after_account_page_load', 10 );
		 * function my_after_account_page_load() {
		 *     // your code here
		 * }
		 * ?>
		 */
        do_action( 'um_after_account_page_load' ); 
        ?>


	</div>

</div>