<?php /* Template Name: Somfy Wishlist */ ?>

<?php
    // redirect not logged user
    if(!is_user_logged_in()){
        wp_redirect (wp_login_url($redirect_uri));
    };
?>
<?php 
    get_header(); 

    get_template_part('templates/shop/wishlist');

    get_footer(); 
?>