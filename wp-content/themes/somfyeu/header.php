<?php
    // $somfyBannerText = get_option('site_top_banner_text');
    // $somfyBannerUrl = get_option('site_top_banner_url');
    // $show_somfy_banner = false;
    $htmlStyle = '';

    // if(isset($somfyBannerText) && $somfyBannerText != ''){
    //     $show_somfy_banner = true;
    //     $htmlStyle = "style='margin-top: 36px !important;'";
    // }
?>

<!DOCTYPE html>

<html <?php language_attributes();?> <?php echo $htmlStyle;?> >

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-site-verification" content="yc1DHLpvOUVCM4NUGB7QbCqpqdeJLr3eIduyVeYnJiA" />
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>

    
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo('template_url'); ?>/assets/favicon.ico">
    <?php wp_head(); ?>
    
</head>


<body <?php body_class(); ?>> 

    <!-- Top Menu -->
    <?php get_template_part('templates/nav/top_nav'); ?>

    <a class="side_icon calculator_trigger calculator_trigger__mobile" alt="בחירת מנוע">
        <div class="icon"></div>
        <span>
            מחשבון<br>בחירת מנוע
        </span>   
    </a>
    
    <div class="main_content">