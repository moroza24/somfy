<?php /* Template Name: Somfy Checkout */ ?>
<?php
if(isset($_GET['credit_loading'])){
    $credit_loading = $_GET['credit_loading'];
    if($credit_loading == 'true'){
            echo "<div class='loader' id='pele_loader'><div class='spinner-loader'></div></div>";
            exit();
    }
}
?>
<?php 
    get_header(); 

    get_template_part('templates/shop/checkout');

    get_footer(); 
?>
