<div id="add_to_cart__popup" class="popup__overlay">
    
    <div class="popup__container">
        <a class="popup__overlay__close add_to_cart__popup__close" alt='סגור'>
            <i class="fas fa-times"></i>
        </a>
        <div class="container_header">
            <h2>סל קניות</h2>
            <p class='add_to_cart__success'>
                מוצר נוסף לסל הקניות
            </p>
            <p class='max_amount_reached'>
                המלאי מוגבל ל- <span id="max_amount_reached_qty"></span>
                <button data-product='' id="stock_notifictaion_maxamount__trigger" class="sf_btn">תודיעו לי כשיש מספיק מלאי</button>
            </p>
            <div class="loader" id="add_to_cart__loader"><div class="spinner-loader"></div></div>
        </div>
    </div>

</div>