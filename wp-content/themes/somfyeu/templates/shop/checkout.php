

<script src="https://www.paypal.com/sdk/js?client-id=AU2w4ut4FrZgYnfvpKTlApwcXI_CaD4d7MOLywTdzFDL2Rvd023Duyhs3Ug3EUPAsgI0nTw422xDsJAN&currency=ILS&intent=capture"> // Replace YOUR_CLIENT_ID with your sandbox client ID
    </script>
<?php

    while(have_posts()){

        the_post();
        $pageId = get_the_ID();
        $pageID = get_the_ID();
        $currentPagePermalink = get_the_permalink($pageID);
        $pageTitle = get_the_title($pageID);
    
        $pickup_map_img = get_field('pickup_map_img');
       

        if(!$pickup_map_img){
            $pickup_map_img_url = bloginfo('template_url') + "\assets\img\pages\shop_pickup.png";
        } else {
            $pickup_map_img_url = $pickup_map_img['url'];

        }
    } 

    // check if current user is logged and get his data
    global $current_user;
    wp_get_current_user();
    
    $userID = get_current_user_id();

    // set default billcountry
    $default_billcountry = get_field_object('field_5fc295450ae7f')['default_value'];
    $default_shipcountry = get_field_object('field_5fc296b60ae86')['default_value'];

    $userData = array(
        'user_email' => '',
        'user_first_name' => '',
        'user_last_name' => '',
        'user_billaddressee' => '',
        'user_billaddress1' => '',
        'user_billcity' => '',
        'user_billcountry' => $default_billcountry,
        'user_shipcountry' => $default_shipcountry
    );
    
    
    if($userID){
        $userData = array(
            'user_email' => $current_user->user_email,
            'user_first_name' => isset(get_user_meta($userID, 'first_name')[0]) ? get_user_meta($userID, 'first_name')[0] : '',
            'user_last_name' => isset(get_user_meta($userID, 'last_name')[0]) ? get_user_meta($userID, 'last_name')[0] : '',
            'user_billaddressee' => isset(get_user_meta($userID, 'billaddressee')[0]) ? get_user_meta($userID, 'billaddressee')[0] : '',
            'user_billaddress1' => isset(get_user_meta($userID, 'billaddress1')[0]) ? get_user_meta($userID, 'billaddress1')[0] : '',
            'user_billcity' => isset(get_user_meta($userID, 'billcity')[0]) ? get_user_meta($userID, 'billcity')[0] : '',
            'user_billcountry' => $default_billcountry,
            'user_shipcountry' => $default_shipcountry
        );

        if($userData['user_billaddressee'] == '') $userData['user_billaddressee'] = $userData['user_first_name'] . " " . $userData['user_last_name'];
    } else {
        $userID = '';
    }

?>


<div class="container checkout_page">
    <ol class="breadcrumbs dark">
        <li><a href="<?php echo home_url(); ?>" alt='דף בית'>דף בית ></a></li>
        <li><a href="<?php echo $currentPagePermalink; ?>" alt='<?php echo $pageTitle; ?>'><?php echo $pageTitle; ?></a></li>
    </ol>
    <h2 class="section_title section_title_full dark_title"><?php echo the_title(); ?></h2>
    
    <!-- order_submition_loader -->
    <div class="order_submition_loader_container__overlay overly_bg">
        <div class="order_submition_loader__container">
            <h4>אנא המתן...</h4>
            <div class="loader" id="order_submition_loader"><div class="spinner-loader"></div></div>
            <p id="order_submition__success">ההזמנה נשלחה!</p>
            <p id="order_submition__payment_recived">תשלום התקבל<br>שולח הזמנה...</p>
            <p id="order_submition__error"></p>
            <div class="order_submition_loader__actions">
                <a href="<?php echo home_url();  ?>/contact" id="go_to_contact" class="sf_btn sf_btn__small" alt='צור קשר'>צור קשר</a>
                <a id="order_submition_loader__container__close" class="sf_btn sf_btn__small" alt='אישור'>אישור</a>
            </div>
        </div>
    </div>
    

    <div class="checkout_page_container">
        <!-- Checkout Form  -->
        <div class="checkout_form__container">
            <!-- Triggers -->
            <div class="checkout_tabs__triggers">
                <a class="tab_trig active" data-target="checkout_delivery_tab" alt='משלוח'>משלוח</a>
                <a class="tab_trig" data-target="checkout_payment_tab" alt='תשלום'>תשלום</a>
            </div>

            <!-- Tabs content -->
            <div class="checkout_tabs__content">
                <!-- Billing information -->
                <from id="delivery_details_form">
                    <h3 class="form_title">פרטי חיוב</h3>

                    <div class="mb-3 form-input__group">
                        <input type="email" class="form-control form-control-lg" id="delivery_details__email" placeholder="כתובת מייל" required
                            value="<?php echo $userData['user_email'];?>">
                        <p class="form_error" data-input="delivery_details__email">יש להזין כתובת מייל תיקנית</p>
                    </div>

                    <div class="row g-3">
                        <div class="col">
                            <div class="mb-3 form-input__group">
                                <input type="text" class="form-control form-control-lg" id="delivery_details__firstname" placeholder="שם פרטי" required
                                    value="<?php echo $userData['user_first_name'];?>">
                                <p class="form_error" data-input="delivery_details__firstname">יש להזין שם פרטי</p>
                            </div>
                        </div>
                        <div class="col">
                            <div class="mb-3 form-input__group">
                                <input type="text" class="form-control form-control-lg" id="delivery_details__lastname" placeholder="שם משפחה" required
                                    value="<?php echo $userData['user_last_name'];?>">
                                <p class="form_error" data-input="delivery_details__lastname">יש להזין שם משפחה</p>
                            </div>
                        </div>
                    </div>

                    <div class="mb-3 form-input__group">
                        <input type="text" class="form-control form-control-lg" id="delivery_details__billaddressee" placeholder="נמען" required
                            value="<?php echo $userData['user_billaddressee'];?>">
                        <p class="form_error" data-input="delivery_details__billaddressee">יש להזין נמען</p>
                    </div>

                    <div class="mb-3 form-input__group">
                        <input type="text" class="form-control form-control-lg" id="delivery_details__billaddress1" placeholder="רחוב, מספר בית" required
                            value="<?php echo $userData['user_billaddress1'];?>">
                        <p class="form_error" data-input="delivery_details__billaddress1">יש להזין רחוב, מספר בית</p>
                    </div>

                    <div class="mb-3 form-input__group">
                        <input type="text" class="form-control form-control-lg" id="delivery_details__billcity" placeholder="יישוב" required
                            value="<?php echo $userData['user_billcity'];?>">
                        <p class="form_error" data-input="delivery_details__billcity">יש להזין ישוב</p>
                    </div>
                    <input type="hidden" id="delivery_details__billcountry"
                        value="<?php echo $userData['user_billcountry'];?>">
                </from>
                
                <!-- checkout_delivery_tab -->
                <div class="tab_content active" id="checkout_delivery_tab">
                    <!-- Delivery options trigger -->
                    <h3 class="delivery_options__title">בחר סוג משלוח</h3>
                    <div class="delivery_options__container">
                        <div class="delivery_option active">
                            <input type="radio" name="delivery_option" id="delivery_option__delivery" value="delivery" checked>
                            <label for="delivery_option__delivery">משלוח</label>
                        </div>
                        <div class="delivery_option">
                            <input type="radio" name="delivery_option" id="delivery_option__pickup" value="pickup">
                            <label for="delivery_option__pickup">איסוף עצמי</label>
                        </div>
                    </div>

                    <!-- delivery_subtab__pickup -->
                    <div id="delivery_subtab__pickup" class="delivery_option__subtab">
                        <img src="<?php echo $pickup_map_img_url;?>" alt="map" loading="lazy">
                    </div>

                    <!-- delivery_subtab__delivery -->
                    <div id="delivery_subtab__delivery" class="delivery_option__subtab">
                        <!-- toggle__other_delivery_details_form -->
                        <div class="toggle__other_delivery_details_form">
                            <input type="checkbox" name="other_delivery_address" id="other_delivery_address__toggle">
                            <label for="other_delivery_address__toggle">כתובת שונה למשלוח</label>
                        </div>

                        <!-- other_delivery_details_form -->
                        <from id="other_delivery_details_form">
                            <h3 class="form_title">כתובת שונה למשלוח</h3>

                            <div class="row g-3">
                                <div class="col">
                                    <div class="mb-3 form-input__group">
                                        <input type="text" class="form-control form-control-lg" id="other_delivery_details__firstname" placeholder="שם פרטי" required>
                                        <p class="form_error" data-input="other_delivery_details__firstname">יש להזין שם פרטי</p>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="mb-3 form-input__group">
                                        <input type="text" class="form-control form-control-lg" id="other_delivery_details__lastname" placeholder="שם משפחה" required>
                                        <p class="form_error" data-input="other_delivery_details__lastname">יש להזין שם משפחה</p>
                                    </div>
                                </div>
                            </div>

                            <div class="mb-3 form-input__group">
                                <input type="text" class="form-control form-control-lg" id="other_delivery_details__shipaddressee" placeholder="נמען" required>
                                <p class="form_error" data-input="other_delivery_details__shipaddressee">יש להזין נמען</p>
                            </div>

                            <div class="mb-3 form-input__group">
                                <input type="text" class="form-control form-control-lg" id="other_delivery_details__shipaddress1" placeholder="רחוב, מספר בית" required>
                                <p class="form_error" data-input="other_delivery_details__shipaddress1">יש להזין רחוב, מספר בית</p>
                            </div>

                            <div class="mb-3 form-input__group">
                                <input type="text" class="form-control form-control-lg" id="other_delivery_details__shipcity" placeholder="יישוב" required>
                                <p class="form_error" data-input="other_delivery_details__shipcity">יש להזין ישוב</p>
                            </div>
                            <input type="hidden" id="other_delivery_details__shipcountry"
                                value="<?php echo $userData['user_shipcountry'];?>">
                        </from>
                    </div>
                    
                    <?php if(!is_user_logged_in()){ ?>
                        <!-- not registered user -->
                        <div class="register_user">
                            <p>בהרשמה ניתן לקבל קוד קופון <strong>15% הנחה</strong> לקנייה הבאה</p>
                            <hr class="divider">
                            <a class="register_user_from_checkout" alt='צור חשבון'>צור חשבון</a>
                        </div>
                    <?php }; ?>


                    <div class="checkout_form_cta">
                        <div class="form_user_check">
                            <input type="checkbox" name="user_updates_agreement" id="user_updates_agreement">
                            <label for="user_updates_agreement">אני מאשר/ת קבלת מידע שיווקי, עדכונים והטבות</label>
                        </div>
                        <div class="form_user_check">
                            <input type="checkbox" name="user_usage_agreement" id="user_usage_agreement">
                            <label for="user_usage_agreement">אני מסכים ומאשר את התנאים ומדיניות הפרטיות</label>
                            <p class="form_error" data-input="user_usage_agreement">יש להסמן "אני מסכים"</p>
                        </div>

                        <a class="sf_btn" id="go_to_payment">המשך לתשלום</a>
                    </div>
                    
                </div>

                <!-- checkout_payment_tab -->
                <div class="tab_content" id="checkout_payment_tab">
                    <!-- payment options trigger -->
                    <div class="payment_options__container">
                        <div class="payment_option active">
                            <input type="radio" name="payment_option" id="payment_option__credit" value="credit" checked>
                            <label for="payment_option__credit">
                                <img src="<?php bloginfo('template_url'); ?>\assets\img\icons\credit.png" loading="lazy" alt="paypal">
                            </label>
                        </div>
                        <div class="payment_option">
                            <input type="radio" name="payment_option" id="payment_option__paypal" value="paypal">
                            <label for="payment_option__paypal">
                                <img src="<?php bloginfo('template_url'); ?>\assets\img\icons\paypal.png" loading="lazy" alt="paypal">
                            </label>
                        </div>
                    </div>

                    <div id="paypal-button-container"></div>

                    <hr class="divider">

                    <div class="payment_cta">
                        <a class="sf_btn" id="submit_order" alt='תשלום'>תשלום</a>
                        <a class="sf_btn sf_btn-light" id="go_to_delivery_tab" alt='חזרה'>חזרה</a>
                    </div>
                </div>
            </div>
        </div>

        <!-- Cart Preview -->
        <div class="cart_preview__container">
            <div class="preview-header">
                <h5 class="header-title">סיכום</h5>
            </div>

            <div class="cart_items__container">

            </div>


            <div class="cart_summery">
                <hr class="divider">

                <div class="sub_summery">
                    מחיר ביניים
                    <div class="val">
                        <span class="sub_summery_val"></span>
                        <span class='currency_symbol'>₪</span>
                    </div>
                    
                </div>

                <div class="discount_summery">
                    הנחה
                    <div class="val">
                        <span class="discount_summery_val"></span>
                        <span class='currency_symbol'>₪</span>
                    </div>
                </div>

                <div class="delivery_summery">
                    משלוח
                    <div class="val">
                        <span class="delivery_summery_val">0</span>
                        <span class='currency_symbol'>₪</span>
                    </div>
                </div>

                <hr class="divider">

                <div class="total_summery">
                    סכום סופי
                    <div class="val">
                        <span class="total_summery_val"></span>
                        <span class='currency_symbol'>₪</span>
                    </div>
                </div>
            </div>

            <div class="not_cart_items">
                <p>עוד לא הוספת מוצרים לסל הקניות</p>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id='current_user_id' value='<?php echo $userID;?>'>

<!-- Shipping data -->
<?php 
    $shop_free_shipping_threshold = get_option('shop_free_shipping_threshold');
    $shop_shipping_product_id = get_option('shop_shipping_product_id');
    $shipping_price = get_field('base_price', $shop_shipping_product_id);
?>
<input type="hidden" id='free_shipping_threshold' value='<?php echo $shop_free_shipping_threshold?>'>
<input type="hidden" id='shipping_product_id' value='<?php echo $shop_shipping_product_id;?>'>
<input type="hidden" id='shipping_price' value='<?php echo $shipping_price;?>'>


<div id="card_iframe_popup" class="popup__overlay">
    
    <div class="popup__container">
        <a class="popup__overlay__close card_iframe_popup__close" alt='סגור'>
            <i class="fas fa-times"></i>
        </a>
        <div class="container_header">
            <h2>תשלום באשראי</h2>
            <iframe id="pele_iframe" src="" title="תשלום באשראי - פלאקארד"></iframe>
        </div>
    </div>

</div>