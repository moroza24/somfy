<?php
    // Get EU categories
    $pageID = get_option( 'page_on_front' );

    $sectionData = get_field('top_seller_section', $pageID);

    if($sectionData){
        $sectionTitle = $sectionData['title'];
        $poducts = $sectionData['poducts'];
?>
    <h2 class="section_title section_title_full dark_title top_sales_title"><?php echo $sectionTitle; ?></h2>

    <div class=" selected_products">
            <div class="col selected_products_slider">

                <?php
                   
                    if(count($poducts) > 0) {
                        foreach ($poducts as $poduct) {
                    
                            $productID = $poduct->ID;
                            $permalink = get_the_permalink($poduct->ID);
                            $thumbnail_url = get_products_files($productID, 'thumbnail')['url'];
                            
                            $product_title = get_the_title($productID);
                            
                            $base_price = floatval(get_field('base_price', $productID));
                            $stock_quantity = get_field('stock_quantity', $productID);
                            
                            $on_sell = get_field('on_sell', $productID);
                            $discount_price = floatval(get_field('discount_price', $productID));
                            
                            if($on_sell){
                                $discount_percentage = get_field('discount_percentage', $productID);
                    
                                $on_sell_class = 'onsell'; 
                            } else {
                                $discount_percentage = '';
                                $discount_price = '';
                                $on_sell_class = ''; 
                            }

                            // check warranty extension in custitem_complementary_product
                            $custitem_complementary_product = get_field('custitem_complementary_product', $productID);
                            $warranty_applicable = false;
                            
                            if($custitem_complementary_product && count($custitem_complementary_product) > 0){
                                $warrantyItemID = get_option('shop_warranty_product_id');

                                foreach ($custitem_complementary_product as $comp_product) {
                                    if($comp_product->ID == $warrantyItemID){
                                        $warranty_applicable = true;
                                        break;
                                    }
                                }
                            }
                            
                ?>
                                <div class='product-slide_container'>
                                    <div class='product-slide'>
                                        <div class='product-thumbnail-col'>
                                            <?php if($discount_percentage != ''){ ?>
                                                <span class="dicount_tag">
                                                    <span class='tag__title'>הנחה</span><br>
                                                    <?php echo $discount_percentage. "%"; ?>
                                                </span>
                                            <?php };?>
                                            <?php if($stock_quantity < 3){ ?>
                                                <span class="out_of_stock_tag">אזל במלאי</span>
                                            <?php };?>

                                            <!-- wishlist -->
                                            <a class='wishlist_cta' data-product='<?php echo $productID; ?>' alt='מועדפים'>
                                                <i class="far fa-heart"></i>
                                            </a>

                                            <a href='<?php echo $permalink; ?>' alt='<?php echo $product_title; ?>'>
                                                <div class="product-thumbnail">
                                                    <img 
                                                        src='<?php echo $thumbnail_url;?>' 
                                                        alt='<?php echo $product_title; ?>' 
                                                        class=''>
                                                </div>
                                            </a>
                                        </div>

                                        <div class='product-meta-container'>
                                            <a href="<?php echo $permalink; ?>" class='product-title' alt='<?php echo $product_title; ?>'><?php echo $product_title; ?></a>
                                        </div>
                                        <hr class="divider">

                                        <div class="product-cta-container">
                                            <div class='product-cta'>
                                                <?php if($stock_quantity < 3){ ?>
                                                    <button class='get_stock_reminder' data-product='<?php echo $productID; ?>'>
                                                        תודיעו לי שהמוצר חוזר
                                                    </button>
                                                <?php } else { ?>
                                                    <button class='add_to_cart__cta add_to_cart__cta_big' 
                                                        data-product='<?php echo $productID; ?>'  
                                                        data-stock='<?php echo $stock_quantity; ?>'
                                                        data-warranty_applicable='<?php echo $warranty_applicable; ?>'
                                                        data-add_warranty='false'>
                                                        <img 
                                                            src="<?php bloginfo('template_url'); ?>/assets/svg/002-shopping-cart.svg" 
                                                            alt="cart" 
                                                            loading="lazy">
                                                        הוסף לסל
                                                    </button>
                                                <?php } ?>
                                            </div>

                                            <div class='product-price'>
                                                <?php if($on_sell){ ?>
                                                    <span class='old_price'>
                                                        <span class='currency_symbol'>₪</span><?php echo $base_price; ?>
                                                    </span>

                                                    <span class='discount_price'>
                                                        <span class='currency_symbol'>₪</span><?php echo $discount_price; ?>
                                                    </span>
                                                <?php } else { ?>
                                                    <span class='base_price'>
                                                        <span class='currency_symbol'>₪</span><?php echo $base_price; ?>
                                                    </span>
                                                <?php } ?>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                <?php        
                        }
                    }
                ?>
            </div>
    </div>
<?php
    }
    wp_reset_postdata();
?>
