<div class="um-field account_orders_history__container" style="direction: ltr;text-align: left;">

<?php
    $userID = get_current_user_id();
    $userInternalid = get_user_meta($userID, 'internalid')[0];

    echo "<input type='hidden' id='user_internalid' value='$userInternalid'>";
?>
    <div class="loader" id="account_orders__loader"><div class="spinner-loader"></div>טוען הזמנות...</div>
    <div id="no_orders__message" class="hidden">תרם ביצעת הזמנה</div>
    
    <!-- User Orders -->
    <div class="orders_resultes__container">
        <h2 class="main-title">הזמנות שלי</h2>
        <hr class="divider">

        <div id="active_orders__container" class="orders_container">
            <div class="results_header">
                <h3 class="title">הזמנות פעילות</h3>

                <div class="sorting_filters_container">
                    <p class="title">סדר לפי: </p>
                    <a class="sf_btn sf_btn__small sort_orders_btn"  alt='תאריך'
                        data-order='asc' data-for="active_orders_results">תאריך</a>
                </div>
            </div>

            <div class="results" id="active_orders_results">
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">מס הזמנה</th>
                            <th scope="col">תאריך</th>
                            <th scope="col">פעולות</th>
                        </tr>
                    </thead>
                    <tbody data-for="active_orders_results">
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>		


<div id="order_details_popup" class="popup__overlay">
    
    <div class="popup__container">
        <a class="popup__overlay__close" id="order_details_popup__close" alt='סגור'>
            <i class="fas fa-times"></i>
        </a>
        <div class="container_header">
            <h2>הזמנה מספר: <span id="order_details__order_tranid"></span></h2>
        </div>
        
        <div class="container_body">
            <div class="loader" id="order_details__loader"><div class="spinner-loader"></div>טוען פרטים...</div>
            <div id="no_ordersitems__message" class="hidden">לא נמצאו שורות להזמנה</div>

            <!-- Order details results -->
            <div id="order_details__results">
               
                <table class="table" id="order_details__orderitems">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">מק"ט</th>
                            <th scope="col">שם מוצר</th>
                            <th scope="col">כמות</th>
                            <th scope="col">כמות שסופקה</th>
                            <th scope="col">מחיר</th>
                        </tr>
                    </thead>
                    <tbody data-for="order_details__orderitems">
                        
                    </tbody>
                </table>

                <div class="order_details__summery">
                    <div class="order_details__subtotal">מחיר ביניים 
                        <span id="order_details__subtotal"></span>
                    </div>

                    <div class="order_details__shipping">משלוח
                        <span id="order_details__shipping"></span>
                    </div>
                    <hr>

                    <div class="order_details__total">סכום סופי
                        <span id="order_details__total"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<?php 
    $shop_shipping_product_id = get_option('shop_shipping_product_id');
?>
    <input type="hidden" id='shipping_product_id' value='<?php echo $shop_shipping_product_id;?>'>
