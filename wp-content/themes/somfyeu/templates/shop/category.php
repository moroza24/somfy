<?php
    while(have_posts()){

        the_post();

        $categoryId = get_the_id();

        $categoryTitle = get_the_title();
        $categoryPermalink = get_the_permalink($categoryId);
        
        $groups = get_field('sub_groups');
        $relatedProducts = get_field('related_products');

    }
?>

<div class="container product_category">
    <div class="row">
        <div class="col">
            <ol class="breadcrumbs dark">
                <li><a href="<?php echo home_url(); ?>" alt='דף בית'>דף בית ></a></li>
                <li><a href="<?php echo $categoryPermalink; ?>" alt='<?php echo $categoryTitle; ?>'><?php echo $categoryTitle; ?></a></li>
            </ol>
            <h2 class="section_title dark_title"><?php echo $categoryTitle; ?></h2>

            <!-- Subgroups -->
            <?php 
                if($groups){
                    echo "<div class='subgroups_container'>";
                    foreach ($groups as $group){
                        $groupTitle = get_the_title($group->ID);
                        $groupPermalink = get_home_url()."/shop/?category=" .$categoryId ."&subgroup=". $group->ID;
                        $thumbnail = get_the_post_thumbnail_url($group->ID);

                        echo "<div class='subgroup__card' style='background-image: url(\"$thumbnail\");'>
                                <a class='subgroup_link' href='$groupPermalink' alt='$groupTitle'>$groupTitle</a>
                            </div>";
                    }
                    echo "</div>";
                }
            ?>
        </div>
    </div>   
</div>

<div class="product_category_related_products">
<!-- relatedProducts -->
<?php 
    if($relatedProducts){
        echo "<h2 class='section_title'>מוצרים משלימים</h2>";
        echo "<div class='releated_products__slider'>";

        foreach ($relatedProducts as $product){
            $productID = $product->ID;
            $thumbnail_url = get_the_post_thumbnail_url($productID);
            $permalink = get_the_permalink($productID);

            if($thumbnail_url == ''){
                $thumbnail_url = get_bloginfo('template_url') .'/assets/img/placeholder-image.png';
            }
            
            $product_title = get_the_title($productID);
            
            $base_price = floatval(get_field('base_price', $productID));
            $stock_quantity = get_field('stock_quantity', $productID);
            
            $on_sell = get_field('on_sell', $productID);
            $discount_price = floatval(get_field('discount_price', $productID));
            
            if($on_sell){
                $discount_percentage = get_field('discount_percentage', $productID);
                $on_sell_class = 'onsell'; 
            } else {
                $discount_percentage = '';
                $discount_price = '';
    
                $on_sell_class = ''; 
            }

            // check warranty extension in custitem_complementary_product
            $custitem_complementary_product = get_field('custitem_complementary_product', $productID);
            $warranty_applicable = false;
            
            if($custitem_complementary_product && count($custitem_complementary_product) > 0){
                $warrantyItemID = get_option('shop_warranty_product_id');

                foreach ($custitem_complementary_product as $comp_product) {
                    if($comp_product->ID == $warrantyItemID){
                        $warranty_applicable = true;
                        break;
                    }
                }
            }
    ?>
            <div class='product-slide'>
                <div class='product-thumbnail-col'>
                    <?php if($discount_percentage != ''){ ?>
                        <span class="dicount_tag">
                            <span class='tag__title'>הנחה</span><br>
                            <?php echo $discount_percentage . "%"; ?>
                        </span>
                    <?php };?>
                    <?php if($stock_quantity < 3){ ?>
                        <span class="out_of_stock_tag">אזל במלאי</span>
                    <?php };?>

                    <!-- wishlist -->
                    <a class='wishlist_cta' data-product='<?php echo $productID; ?>' alt='מועדפים'>
                        <i class="far fa-heart"></i>
                    </a>
                    
                    <a href='<?php echo $permalink; ?>' alt='<?php echo $product_title; ?>'>
                        <img 
                            src='<?php echo $thumbnail_url;?>' 
                            alt='<?php echo $product_title; ?>' 
                            class='product-thumbnail'>
                    </a>
                </div>
                <div class='product-meta-container'>
                    <a href="<?php echo $permalink; ?>" class='product-title' alt='<?php echo $product_title; ?>'><?php echo $product_title; ?></a>
                </div>

                <hr class="divider">
                <div class="product-cta-container">
                    <div class='product-cta'>
                        <?php if($stock_quantity < 3){ ?>
                            <button class='get_stock_reminder' data-product='<?php echo $productID; ?>'>
                                תודיעו לי שהמוצר חוזר
                            </button>
                        <?php } else { ?>
                            <button class='add_to_cart__cta' 
                                data-product='<?php echo $productID; ?>' 
                                data-stock='<?php echo $stock_quantity; ?>'
                                data-warranty_applicable='<?php echo $warranty_applicable; ?>'
                                data-add_warranty='false'>
                                <img 
                                    src="<?php bloginfo('template_url'); ?>/assets/svg/002-shopping-cart.svg" 
                                    alt="cart" 
                                    loading="lazy">
                                הוסף לסל
                            </button>
                        <?php } ?>
                    </div>

                    <div class='product-price'>
                        <?php if($on_sell){ ?>
                            <span class='old_price'>
                                <span class='currency_symbol'>₪</span><?php echo $base_price; ?>
                            </span>

                            <span class='discount_price'>
                                <span class='currency_symbol'>₪</span><?php echo $discount_price; ?>
                            </span>
                        <?php } else { ?>
                            <span class='base_price'>
                                <span class='currency_symbol'>₪</span><?php echo $base_price; ?>
                            </span>
                        <?php } ?>
                    </div>
                    
                </div>
            </div>
            <?php        
        }
        echo "</div>";
    }
    wp_reset_postdata();
?>
</div>