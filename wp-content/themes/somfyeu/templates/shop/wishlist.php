<?php
    $pageID = get_the_ID();
    $currentPagePermalink = get_the_permalink($pageID);
    $pageTitle = get_the_title($pageID);

?>
<div class="container wishlist">
    <ol class="breadcrumbs dark">
        <li><a href="<?php echo home_url(); ?>" alt='דף בית'>דף בית ></a></li>
        <li><a href="<?php echo $currentPagePermalink; ?>" alt='<?php echo $pageTitle; ?>'><?php echo $pageTitle; ?></a></li>
    </ol>
    <h2 class="section_title section_title_full dark_title"><?php echo the_title(); ?></h2>

    <h5 id="total_wishlist_results"></h5>
    <div id="wishlist_results_container">
        <div id="wishlist_results"></div>
        <div id="wishlist_no_results_msg">לא נמצאו תוצאות</div>
        <div class="loader" id="wishlist_loader"><div class="spinner-loader"></div></div>
    </div>
</div>
