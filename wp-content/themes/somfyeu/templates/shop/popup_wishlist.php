<div id="wishlist_popup" class="popup__overlay">
    
    <div class="popup__container">
        <a class="popup__overlay__close" alt='סגור'>
            <i class="fas fa-times"></i>
        </a>
        <div class="container_header">
            <h2><?php echo get_option('wishlist__title'); ?></h2>
            <p>
                <?php echo get_option('wishlist__content'); ?>
                
            </p>
        </div>
        <div class="container_body">
            <?php echo "<a class='sf_btn' href='" .get_home_url() ."/register' alt='ליצור חשבון'>ליצור חשבון</a>"; ?>
        </div>
    </div>

</div>