<?php 
    // get warranty product data
    $warrantyProductId = get_option('shop_warranty_product_id');
    $warrantyThumb = get_products_files($warrantyProductId, 'thumbnail')['url'];
    $warrantyTitle = get_the_title($warrantyProductId);
    $warrantyShortDesc = get_field('storedescription', $warrantyProductId);
    $warrantyPrice = floatval(get_field('base_price', $warrantyProductId));
?>

<div id="add_warranty_popup" class="popup__overlay">
    
    <div class="popup__container">
        <a class="popup__overlay__close" alt='סגור'>
            <i class="fas fa-times"></i>
        </a>

        <div class="add_warranty_input_container">
            <p class="warrany_price">
                <?php echo "<span class='currency_symbol'>₪</span>". $warrantyPrice; ?>
            </p>
            <div class="warranty_input_group">
                <input type="checkbox" name="add_warranty_checkbox" id="add_warranty_checkbox">
                <label for="add_warranty_checkbox">הוסף למוצר</label>
            </div>
        </div>
        
        <div class="warranty_desc">
            <div class="warranty_thumb">
                <img src="<?php echo $warrantyThumb?>" alt="<?php echo $warrantyTitle; ?>">
            </div>
            <div class="desc">
                <h5 class='add-warranty__title'><?php echo $warrantyTitle; ?></h5>
                <p class='add-warranty__text'><?php echo $warrantyShortDesc; ?></p>
            </div>

            <div class="warranty_cta">
                <div class="product_cta-amount">
                    <label for='add_warranty__quantity_input'>כמות:</label>
                    <input type='number' 
                        min='1' 
                        max='' 
                        class='form-control' 
                        id='add_warranty__quantity_input' 
                        value='1'>
                </div>

                <button class="sf_btn" id='add_warranty__submit' 
                    data-product=''
                    data-stock='' 
                    data-warranty_applicable='1' 
                    data-add_warranty='false'>
                    הוסף לסל
                </button>
            </div>
            
        </div>


        <!-- <div class="container_header">
            <h2><?php echo get_option('add_warrany_popup__title'); ?></h2>
            <p>
                <?php echo get_option('add_warranty_popup__content'); ?>
                
            </p>
        </div>
        <div class="container_body">
            <button class="sf_btn" id='add_to_cart__with_warranty' 
                data-product=''
                data-stock='' 
                data-amount=''
                data-warranty_applicable='1' 
                data-add_warranty='true'>
                הוסף לסל
            </button>
            <button class="sf_btn sf_btn_light" id='add_to_cart__no_warranty' 
                data-product=''
                data-stock='' 
                data-warranty_applicable='1' 
                data-add_warranty='false'>
                לא מעוניין
            </button>
        </div> -->
    </div>

</div>