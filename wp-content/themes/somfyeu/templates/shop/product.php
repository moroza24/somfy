<?php
    use Inc\Classes\Shop\ProductsEU;
    $ProductsEU = new ProductsEU();

    $warrantyProductID = get_option('shop_warranty_product_id');
    $shop_product_details_fields = get_option('shop_product_details_fields');
    
    $comingFromCalculator = false;

    if (isset($_GET['calculator'])) {
        if($_GET['calculator'] == true){
            $comingFromCalculator = true;
        };
    }

    while(have_posts()){
        the_post();

        $productID = get_the_ID();     

        $internal_item = get_field('internal_item', $productID);
        if($internal_item == 1) {
            wp_redirect(home_url());
        }

        $productTitle = get_the_title();
        $dateils_tab_fields = acf_get_fields('group_6015649c2469d');

        $productAllFiles = get_products_files($productID);
        $productImages = $productAllFiles['images'];
        $productTechFiles = $productAllFiles['files'];
        $productData = get_fields($productID);

        $promotional_video = get_field('promotional_video', $productID);
        $training_video = get_field('training_video', $productID);

        // check warranty extension in productComplementaryProducts
        $productComplementaryProducts = $productData['custitem_complementary_product'];
        $warranty_applicable = $ProductsEU->checkIfWarrantyReleated($productID);
        
        // Stock
        $minStockQuantity = get_option('shop_min_stock_quantity');
        $notInStock = false;
        $stock_quantity = get_field('stock_quantity', $productID);
        
        if($stock_quantity < $minStockQuantity){
            $notInStock = true;
        }

        // Prices
        $base_price = floatval(get_field('base_price', $productID));

        $on_sell = get_field('on_sell', $productID);
        $discount_price = floatval(get_field('discount_price', $productID));
        
        if($on_sell){
            $discount_percentage = get_field('discount_percentage', $productID);
            $on_sell_class = 'onsell'; 
        } else {
            $discount_percentage = '';
            $discount_price = '';

            $on_sell_class = ''; 
        }
        
    }
    wp_reset_postdata();

    $homePageID = get_option('page_on_front');
    $homeData = get_field('about_section', $homePageID);
    $const_prod_files = get_field('const_prod_files', $homePageID);
    $productIcons = $homeData['icons'];
?>


<div class="somfy-product__container">
    <!-- Info -->
    <div class="product_info__container">

        <!-- Thumbnail -->
        <div class="thumbnail__container">
            <?php if($discount_percentage != ''){ ?>
                <span class="dicount_tag">
                    <span class='tag__title'>הנחה</span><br>
                    <?php echo $discount_percentage ."%"; ?>
                </span>
            <?php };?>
            <?php if($notInStock){ ?>
                <span class="out_of_stock_tag">אזל במלאי</span>
            <?php };?>

            <!-- wishlist -->
            <a class='wishlist_cta' data-product='<?php echo $productID; ?>' alt='מועדפים'>
                <i class="far fa-heart"></i>
            </a>
            
            <div class="products_images__slider">
                <?php  foreach ($productImages as $image){ ?>
                    <div><div class="image_slide_container">
                        <img class='product_slider_image' src="<?php echo $image['url']; ?>" alt="<?php echo $image['filename']; ?>">
                    </div></div>
                <?php } ?>
            </div>
        </div>
        
        

        <!-- Meta Info -->
        <div class="product_meta__container">
            <p class="product_sku">מק"ט: <?php echo $productData['itemid']; ?></p>
            <h2 class="product_title"><?php echo $productTitle; ?></h2>

            <!-- Stock tag -->
            <?php 
                if($notInStock){
                    echo "<span class='product_not-in-stock get_stock_reminder' data-product='$productID'>אזל המלאי</span>";
                } else {
                    echo "<span class='product_in-stock'>במלאי</span>";
                }
            ?>
            
            <p class="product_short-desc">
                <?php echo htmlspecialchars_decode($productData['storedescription']); ?>
            </p>

            
            <div class="product_icons-container">
                <?php 
                    if(count($productIcons) > 0) {
                        foreach ($productIcons as $icon) {

                            $title = $icon['title'];
                            $sub_title = $icon['sub_title'];
                            $iconUrl = $icon['icon']['url'];

                            echo "<div class='product_icon'>
                                    <div class='icon_box'>
                                        <img class='icon_img' src='$iconUrl' alt='$title'>
                                    </div>
                                    <div class='icon_titles'>
                                        <h3 class='icon_title'>$title</h3>
                                        <p class='icon_subtitle'>$sub_title</p>
                                    </div>
                                </div>";
                        }
                    }
                ?>
                
            </div>

            <!-- CTA -->
            <div class="product_cta-container">
                <div class="product_mobile-preview">
                    <img src="<?php echo $productImages[0]['url']; ?>" alt="<?php echo $productTitle; ?>">
                </div>
                <div class="product_cta-amount">
                    <?php 
                        if(!$notInStock){
                            echo "<label for='stock_quantity_input'>כמות:</label>
                                <input type='number' 
                                    min='1' 
                                    max='".$productData['stock_quantity']."' 
                                    class='form-control' 
                                    id='stock_quantity_input' 
                                    data-product='$productID'
                                    value='1'>";
                        }
                    ?>
                    
                </div>

                <div class="product_cta-price">
                    <?php if($on_sell){ ?>
                        <span class='discount_price'>
                            <span class='currency_symbol'>₪</span><?php echo $discount_price; ?>

                            <span class='old_price'>
                                <span class='currency_symbol'>₪</span><?php echo $base_price; ?>
                            </span>
                        </span>
                    <?php } else { ?>
                        <span class='base_price'>
                            <span class='currency_symbol'>₪</span><?php echo $base_price; ?>
                        </span>
                    <?php } ?>
                </div>

                <div class="product_cta-add-to-cart">
                    <?php if($notInStock){ ?>
                        <button class='get_stock_reminder' data-product='<?php echo $productID; ?>'>
                            תודיעו לי שהמוצר חוזר
                        </button>
                    <?php } else { ?>
                        <button class='add_to_cart__cta' 
                            data-calcitem = '<?php if($comingFromCalculator) echo 'true'; ?>'
                            data-product='<?php echo $productID; ?>'
                            data-stock='<?php echo $stock_quantity; ?>' 
                            data-warranty_applicable='<?php echo $warranty_applicable; ?>' 
                            data-add_warranty='false'>
                            <img 
                                src="<?php bloginfo('template_url'); ?>/assets/svg/002-shopping-cart.svg" 
                                alt="cart" 
                                loading="lazy">
                            הוסף לסל
                        </button>
                    <?php } ?>
                    
                </div>
            </div>
        </div>
    </div>


    <!-- Tabs -->
    <!-- Nav tabs -->
    <div class="product-tabs__container">
        <ul class="product-tabs__triggers">
            <li class="tab-trigger__box">
                <a class="trigger-link active" data-toggle="tab_description" role="tab" alt='תיאור'>תיאור</a>
            </li>
            <li class="tab-trigger__box">
                <a class="trigger-link" data-toggle="tab_dateils" role="tab" alt='מאפיינים'>מאפיינים</a>
            </li>
            <li class="tab-trigger__box">
                <a class="trigger-link" data-toggle="tab_support" role="tab" alt='תמיכה טכנית'>תמיכה טכנית</a>
            </li>
        </ul>

        <!-- Tab panes -->
        <div class="product-tabs__content">
            <div class="tab-content__container active" id="tab_description" role="tabpanel">
                <?php 
                    // print promotional_video
                    if(is_array($promotional_video) && count($promotional_video) > 0){
                        echo "<div class='product_promotional_video__container'>";
                        foreach ($promotional_video as $vid){
                            $vidID = $vid['youtube_id'];
                            echo "<iframe width='560' height='315' src='https://www.youtube.com/embed/$vidID' frameborder='0' allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>";
                        }
                        echo "</div>";
                    }

                    echo htmlspecialchars_decode($productData['storedetaileddescription']);
                    
                ?>
            </div>

            <div class="tab-content__container" id="tab_dateils" role="tabpanel">
                <?php echo htmlspecialchars_decode($productData['featureddescription']);?>
                <?php if(count($dateils_tab_fields) > 0): ?>
                <ul>
                    <?php 
                        foreach($dateils_tab_fields as $field){
                            $label = $field['label'];
                            $name = $field['name'];
                            $type = $field['type'];

                            if($type == 'select'){
                                $value = isset($productData[$name]['label']) ? $productData[$name]['label'] : '';
                            } else {
                                $value = isset($productData[$name]) ? $productData[$name] : '';
                            }
                            if($value == '') continue;
                            echo "<li><span>$label: </span>$value</li>";
                        }
                    ?>
                </ul>
                <?php endif; ?>
            </div>

            <div class="tab-content__container" id="tab_support" role="tabpanel">
                
                <!--  -->
                <div class="tab_section product_files__container">
                    <?php 
                        // print training_video
                        if(is_array($training_video) && count($training_video) > 0){
                    
                            echo "<h3 class='title'>סרטוני הדרכה</h3>
                                <div class='product_training_video__container'>";
                            foreach ($training_video as $vid){
                                $vidID = $vid['youtube_id'];
                                echo "<iframe width='560' height='315' src='https://www.youtube.com/embed/$vidID' frameborder='0' allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>";
                            }
                            echo "</div>";
                        }
                    ?>    
                </div>

                <!-- Files -->
                <?php if(count($productTechFiles) > 0 || count($const_prod_files) > 0): ?>
                    <div class="tab_section product_files__container">
                        <h3 class='title'>קבצי הדרכה/הסבר</h3>
                        
                        <div class="files__container">
                        <?php 
                            $pdfFileIcon = get_bloginfo('template_url'). "/assets/img/icons/pdf.svg";
                            foreach ($const_prod_files as $file){
                                $fileName = $file['prod_file']['title'];
                                $url = $file['prod_file']['url'];
                                echo "<a href='$url' alt='$fileName' class='file__button' download>
                                    <img src='$pdfFileIcon' alt='$fileName'>
                                    $fileName
                                </a>";
                            }
                            foreach ($productTechFiles as $file){
                                $fileName = $file['filename'];
                                $url = $file['url'];
                                echo "<a href='$url' alt='$fileName' class='file__button' download>
                                    <img src='$pdfFileIcon' alt='$fileName'>
                                    $fileName
                                </a>";
                            }
                        ?>
                        </div>
                        
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
        

    <!-- Realeated products -->
    <?php
        // Get EU categories
        if($productComplementaryProducts){
    ?>
        <h2 class="section_title section_title_mini dark_title">
            מוצרים משלימים
        </h2>

        <div class="releated_products__container">
            <div class="releated_products__slider">
                <?php
                
                    if(count($productComplementaryProducts) > 0) {
                        foreach ($productComplementaryProducts as $poduct) {
                            $productID = $poduct->ID;
                            
                            // excloude internal products
                            $internal_item = get_field('internal_item', $productID);
                            if($internal_item == 1) continue;

                            $permalink = get_the_permalink($poduct->ID);
                            $thumbnail_url = get_products_files($productID, 'thumbnail')['url'];
                            
                            $product_title = get_the_title($productID);
                            
                            $base_price = floatval(get_field('base_price', $productID));
                            $stock_quantity = get_field('stock_quantity', $productID);
                            
                            $on_sell = get_field('on_sell', $productID);
                            $discount_price = floatval(get_field('discount_price', $productID));
                            
                            if($on_sell){
                                $discount_percentage = get_field('discount_percentage', $productID);
                    
                                $on_sell_class = 'onsell'; 
                            } else {
                                $discount_percentage = '';
                                $discount_price = '';
                    
                                $on_sell_class = ''; 
                            }

                            // check warranty extension in custitem_complementary_product
                            $custitem_complementary_product = get_field('custitem_complementary_product', $productID);
                            $warranty_applicable = false;
                            
                            if($custitem_complementary_product && count($custitem_complementary_product) > 0){
                                foreach ($custitem_complementary_product as $comp_product) {
                                    if($comp_product->ID == $warrantyProductID){
                                        $warranty_applicable = true;
                                        break;
                                    }
                                }
                            }
                            
                ?>
                                <div class='product-slide_container'>
                                    <div class='product-slide'>
                                        <div class='product-thumbnail-col'>
                                            <?php if($discount_percentage != ''){ ?>
                                                <span class="dicount_tag">
                                                    <span class='tag__title'>הנחה</span><br>
                                                    <?php echo $discount_percentage ."%"; ?>
                                                </span>
                                            <?php };?>
                                            <?php if($stock_quantity < 3){ ?>
                                                <span class="out_of_stock_tag">אזל במלאי</span>
                                            <?php };?>

                                            <!-- wishlist -->
                                            <a class='wishlist_cta' data-product='<?php echo $productID; ?>' alt='מועדפים'>
                                                <i class="far fa-heart"></i>
                                            </a>
                                                
                                            <a href='<?php echo $permalink; ?>' alt='<?php echo $product_title; ?>'>
                                                <img 
                                                    src='<?php echo $thumbnail_url;?>' 
                                                    alt='<?php echo $product_title; ?>' 
                                                    class='product-thumbnail'>
                                            </a>
                                        </div>

                                        <div class='product-meta-container'>
                                            <a href="<?php echo $permalink; ?>" class='product-title' alt='<?php echo $product_title; ?>'><?php echo $product_title; ?></a>
                                        </div>
                                        <hr class="divider">

                                        <div class="product-cta-container">
                                            <div class='product-cta'>
                                                <?php if($stock_quantity < 3){ ?>
                                                    <button class='get_stock_reminder' data-product='<?php echo $productID; ?>'>
                                                        תודיעו לי שהמוצר חוזר
                                                    </button>
                                                <?php } else { ?>
                                                    <button class='add_to_cart__cta' 
                                                        data-product='<?php echo $productID; ?>'  
                                                        data-stock='<?php echo $stock_quantity; ?>'
                                                        data-warranty_applicable='<?php echo $warranty_applicable; ?>'
                                                        data-add_warranty='false'>
                                                        <img 
                                                            src="<?php bloginfo('template_url'); ?>/assets/svg/002-shopping-cart.svg" 
                                                            alt="cart" 
                                                            loading="lazy">
                                                        הוסף לסל
                                                    </button>
                                                <?php } ?>
                                            </div>

                                            <div class='product-price'>
                                                <?php if($on_sell){ ?>
                                                    <span class='old_price'>
                                                        <span class='currency_symbol'>₪</span><?php echo $base_price; ?>
                                                    </span>

                                                    <span class='discount_price'>
                                                        <span class='currency_symbol'>₪</span><?php echo $discount_price; ?>
                                                    </span>
                                                <?php } else { ?>
                                                    <span class='base_price'>
                                                        <span class='currency_symbol'>₪</span><?php echo $base_price; ?>
                                                    </span>
                                                <?php } ?>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                <?php        
                        }
                    }
                ?>
            </div>
        </div>
    <?php
        }
        wp_reset_postdata();
    ?>

</div>