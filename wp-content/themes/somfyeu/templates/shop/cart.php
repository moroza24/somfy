<?php
    $pageID = get_the_ID();
    $currentPagePermalink = get_the_permalink($pageID);
    $pageTitle = get_the_title($pageID);

?>

<div class="container cart_page">
    <ol class="breadcrumbs dark">
        <li><a href="<?php echo home_url(); ?>" alt='דף בית'>דף בית ></a></li>
        <li><a href="<?php echo $currentPagePermalink; ?>" alt='<?php echo $pageTitle; ?>'><?php echo $pageTitle; ?></a></li>
    </ol>
    <h2 class="section_title section_title_full dark_title"><?php echo the_title(); ?></h2>

    <div class="cart_page_container">

        <!-- Coupon -->
        <!-- <div class="coupon_input__container">
            <img src="<?php bloginfo('template_url'); ?>/assets/img/coupon_input.png" alt="קופון" loading="lazy">
            <div class="form-floating form-floating--rtl">
                <input type="text" class="form-control form-control-sm" id="coupon_input" placeholder="קוד קופון">
                <label for="coupon_input">קוד קופון</label>
            </div>
            <a class="sf_btn submit_coupon">אימות קופון</a>

        </div> -->

        <div class="cart_result__container">
            <div class="cart_items__container"></div>
            <div class="not_cart_items">
                <p>עוד לא הוספת מוצרים לסל הקניות</p>
            </div>
        </div>
        
    </div>
    

    <div class="cart_summery">
        <div class="summaries">
            <div class="sub_summery">
                מחיר ביניים
                <div class="val">
                    <span class="sub_summery_val"></span>
                    <span class='currency_symbol'>₪</span>
                </div>
            </div>

            <div class="discount_summery">
                הנחה
                <div class="val">
                    <span class="discount_summery_val"></span>
                    <span class='currency_symbol'>₪</span>
                </div>
            </div>

            <hr class="divider">

            <div class="total_summery">
                סכום סופי
                <div class="val">
                    <span class="total_summery_val"></span>
                    <span class='currency_symbol'>₪</span>
                </div>
            </div>
        </div>

        <a href="<?php echo home_url(); ?>/checkout" class="go_to_checkout sf_btn sf_btn__big" alt='המשך לתשלום'>המשך לתשלום</a>
    </div>

    
</div>