<?php
    use Inc\Classes\Shop\OrderEU;

    if(isset($_GET['tranid'])){
        $tranid = $_GET['tranid'];
        $Order = new OrderEU();
        $orderProcessCompleted = $Order->updateOrderProcessCompleted('SO#'.$tranid);

        if(!$orderProcessCompleted){
            // wp_redirect(home_url());
        }
    } else {
        // wp_redirect(home_url());
    }

    while(have_posts()){

        the_post();
        $pageId = get_the_ID();

        $show_discount = get_field('show_discount');
        $discount_amount = get_field('discount_amount');

        $complete_button = get_field('complete_button');

        $side_container_background = get_field('side_container_background');
        $side_container_title = get_field('side_container_title');
        $side_container_link = get_field('side_container_link');
    } 
?>
    
<div class="container order-confirmation_page">
    <h2 class="section_title section_title_full dark_title"><?php echo the_title(); ?></h2>

    <!-- Success -->
    <div class="order-confirmation_page_container">
        <div class="confirmation__content">
            <h3 class="title">תודה על קנייתך</h3>
            <p class="subtitle">חשבונית יחד עם קבלה תשלח למייל שציינת</p>
            <hr class="divider">

            <?php if($show_discount){ ?>
            <div class="discount_coupon">
                <img src="<?php bloginfo('template_url'); ?>/assets/img/icons/coupon.svg"  alt="coupon" loading="lazy">
                <p>קוד קופון לקנייה הבאה מחכה לך במייל</p>
                <p><span class="discount_amount"><?php echo $discount_amount; ?></span> הנחה</p>
            </div>
            <?php } ?>

            <a class='sf_btn' href="<?php echo $complete_button['url']; ?>" alt="<?php echo $complete_button['title']; ?>"><?php echo $complete_button['title']; ?></a>
        </div>

        <div class="tech_suppurt__content" style="background-image: url('<?php echo $side_container_background['url']; ?>')">
            <h3 class="title"><?php echo $side_container_title; ?></h3>
            <a class='sf_btn'
                href="<?php echo $side_container_link['url']; ?>" 
                target="<?php echo $side_container_link['target']; ?>" 
                alt="<?php echo $side_container_link['title']; ?>">
                
                <?php echo $side_container_link['title']; ?>
            </a>
        </div>
    </div>
</div>