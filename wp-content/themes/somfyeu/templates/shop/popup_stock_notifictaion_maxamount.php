<?php
    if(is_user_logged_in()){
        global $current_user;
        wp_get_current_user();
        $userEmail = $current_user->user_email;
        $userFullname = $current_user->display_name;
        $userPhone = get_field('custentity_il_mobile', $current_user->ID);
    } else {
        $userEmail = '';
        $userFullname = '';
        $userPhone = '';
    }
?>
<div id="stock_notifictaion_maxamount_popup" class="popup__overlay">
    
    <div class="popup__container">
        <a class="popup__overlay__close" alt='סגור'>
            <i class="fas fa-times"></i>
        </a>
        <div class="container_header">
            <h2><?php echo get_option('stocknotification_maxamount__title'); ?></h2>
            <p>
                <?php echo get_option('stocknotification_maxamount__content'); ?>
            </p>
        </div>
        <div class="container_body" id="stock_notifictaion_maxamount_signup">
            <form id="stock_notifictaion_maxamount_signup__form">
                <input type="text" value="<?php echo $userFullname; ?>" required name="fullname" id="stock_notifictaion_maxamount_fullname" placeholder="שם מלא">
                <input type="tel" value="<?php echo $userPhone; ?>" required name="phone" id="stock_notifictaion_maxamount_phone" placeholder="טלפון" minlength="8"  maxlength="10"
                    onkeydown="if(event.key==='.'){event.preventDefault();}"  oninput="event.target.value = event.target.value.replace(/[^0-9]*/g,'');">
                <input type="email" value="<?php echo $userEmail; ?>" required name="email" id="stock_notifictaion_maxamount_email" placeholder="כתובת מייל">
                <input type="number" value="" required name="required_amount" id="stock_notifictaion_maxamount_required_amount" placeholder="מלאי מבוקש">

                <input type="submit" value="הרשם" id="stock_notifictaion_maxamount_submit">
                <div class="loader" id="stock_notifictaion_maxamount__form_loader"><div class="spinner-loader"></div></div>
            </form>
            <div id="stock_notifictaion_maxamount__submit_msg"></div>
        </div>
    </div>

</div>