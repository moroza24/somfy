<?php 
    // get page thumbnail
    $pageID = get_the_ID();
    $currentPagePermalink = get_the_permalink($pageID);
    $pageTitle = get_the_title($pageID);


    // get question and answer array
    $qas = get_field('questions_and_answers', $pageID);
    $map_src = get_field('map_src', $pageID);
?>
<div id="contact-page" class="container">
    <ol class="breadcrumbs dark">
        <li><a href="<?php echo home_url(); ?>" alt='דף בית'>דף בית ></a></li>
        <li><a href="<?php echo $currentPagePermalink; ?>" alt='<?php echo $pageTitle; ?>'><?php echo $pageTitle; ?></a></li>
    </ol>
	<h2 class="section_title dark_title"><?php the_title(); ?></h2>
    
    <!-- Contact Form -->
    <div class="contact-form__container">
        <div class="contact_map">
            <iframe src="<?php echo $map_src; ?>" 
                width="100%" height="100%" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            
        </div>
        <div class="contact-form__form">
            <form action='https://crm.zoho.com/crm/WebToLeadForm' name=WebToLeads3118520000027797003 method='POST' onSubmit='javascript:document.charset="UTF-8"; return checkMandatory3118520000027797003()' accept-charset='UTF-8'>
                <!-- Do not remove this code. -->
                <input type='text' style='display:none;' name='xnQsjsdp' value='17d057a202396f477ebd88d41924ec75a044a5dda8fe800733e245d4d6f8fa1a'></input> 
                <input type='hidden' name='zc_gad' id='zc_gad' value=''></input> 
                <input type='text' style='display:none;' name='xmIwtLD' value='341bce4dd3c2383b6934b0e1fac0947bdf0ee4820919e84b0ee2b8f2278f8911'></input> 
                <input type='text'  style='display:none;' name='actionType' value='TGVhZHM='></input>
                <input type='text' style='display:none;' name='returnURL' value='https://somfy.co.il' > </input>
                <!-- Do not remove this code. -->
            

                <div class="form_row">
                    <div class="form_label">
                        <label for='Last Name'>שם
                            <span style='color:red;'>*</span>
                        </label>
                    </div>
                    <div class="form_input">
                        <input type='text' id='Last Name' name='Last Name' maxlength='80'></input>
                    </div>
                </div>


                <div class="form_row">
                    <div class="form_label">
                        <label for='Email'>דוא"ל</label>
                    </div>
                    <div class="form_input">
                        <input type='text' ftype='email' id='Email' name='Email' maxlength='100'></input>
                    </div>
                </div>


                <div class="form_row">
                    <div class="form_label">
                        <label for='Mobile'>מספר נייד
                            <span style='color:red;'>*</span>
                        </label>
                    </div>
                    <div class="form_input">
                        <input type='text' id='Mobile' name='Mobile' maxlength='30'></input>
                    </div>
                </div>

                
                <div class="form_row">
                    <div class="form_label">
                        <label for='LEADCF2'>קטגוריה
                            <span style='color:red;'>*</span>
                        </label>
                    </div>
                    <div class="form_input">
                        <select class="form-select form-select-sm" id="LEADCF2" name="LEADCF2">
                            <option value="-None-">-None-</option>
                            <option value="תריס">תריס</option>
                            <option value="וילון">וילון</option>
                            <option value="סוכך">סוכך</option>
                            <option value="שער חשמלי">שער חשמלי</option>
                            <option value="בית חכם">בית חכם</option>
                            <option value="מערכת אזעקה">מערכת אזעקה</option>
                            <option value="מערכת אינטרקום">מערכת אינטרקום</option>
                            <option value="מצלמה ביתית">מצלמה ביתית</option>
                            <option value="שלט">שלט</option>
                        </select>
                    </div>
                </div>


                <div class="form_row">
                    <div class="form_label">
                        <label for='LEADCF12'>תת קטגוריה</label>
                    </div>
                    <div class="form_input">
                        <select class="form-select form-select-sm" id="LEADCF12" name="LEADCF12">
                            <option value="-None-">-None-</option>
                            <option value="תריס גלילה">תריס גלילה</option>
                            <option value="מנוע לתריס גלילה">מנוע לתריס גלילה</option>
                            <option value="תריס ונציאני">תריס ונציאני</option>
                            <option value="מנוע לתריס ונציאני">מנוע לתריס ונציאני</option>
                            <option value="וילון גלילה">וילון גלילה</option>
                            <option value="וילון ונציאני">וילון ונציאני</option>
                            <option value="וילון הסטה">וילון הסטה</option>
                            <option value="מנוע לוילון גלילה">מנוע לוילון גלילה</option>
                            <option value="סוכך זרועות">סוכך זרועות</option>
                            <option value="סוכך אחר">סוכך אחר</option>
                            <option value="מנוע לסוכך">מנוע לסוכך</option>
                            <option value="שער נגרר">שער נגרר</option>
                            <option value="מנוע לשער נגרר">מנוע לשער נגרר</option>
                            <option value="שער הרמה">שער הרמה</option>
                            <option value="מנוע לשער הרמה">מנוע לשער הרמה</option>
                            <option value="שער כנף">שער כנף</option>
                            <option value="מנוע לשער כנף">מנוע לשער כנף</option>
                            <option value="שער גלילה">שער גלילה</option>
                            <option value="מנוע לשער גלילה">מנוע לשער גלילה</option>
                            <option value="בית חכם">בית חכם</option>
                        </select>
                    </div>
                </div>


                <div class="form_row">
                    <div class="form_label">
                        <label for='Description'>הודעה</label>
                    </div>
                    <div class="form_input">
                        <textarea id='Description' name='Description'></textarea>
                    </div>
                </div>


                <div class="form_row">
                    <div class="form_actions">
                        <input type='submit' id='formsubmit' class='formsubmit zcwf_button' value='שלח' title='Submit'>
                        <!-- <input type='reset' class='zcwf_button' name='reset' value='Reset' title='Reset'> -->
                    </div>
                </div>
                
                
                <!-- not displayed -->
                <div style='display: none;'>
                    <div class='form_label'>
                        <label for='LEADCF1'>Lead Type</label>
                    </div>
                    <div class='form_input'>
                        <select class='zcwf_col_fld_slt' id='LEADCF1' name='LEADCF1'  >
                            <option value='-None-'>-None-</option>
                            <option value='Fixes'>Fixes</option>
                            <option selected value='Project'>Project</option>
                        </select>
                    </div>
                </div>
                


                <script>
                    var mndFileds=new Array('Last Name','Mobile','LEADCF2');
                    var fldLangVal=new Array('שם','Mobile','Category'); 
                    var name='';
                    var email='';
                    
                    function validateEmail(){
                        var emailFld = document.querySelectorAll('[ftype=email]');
                        var i;
                        for (i = 0; i < emailFld.length; i++){
                            var emailVal = emailFld[i].value;
                            if((emailVal.replace(/^\s+|\s+$/g, '')).length!=0 ){
                                var atpos=emailVal.indexOf('@');
                                var dotpos=emailVal.lastIndexOf('.');
                                if (atpos<1 || dotpos<atpos+2 || dotpos+2>=emailVal.length){
                                    alert('Please enter a valid email address. ');
                                    emailFld[i].focus();
                                    return false;
                                }
                            }
                        }
                        return true;
                    }

                    function checkMandatory3118520000027797003() {
                        for(i=0;i<mndFileds.length;i++) {
                            var fieldObj=document.forms['WebToLeads3118520000027797003'][mndFileds[i]];
                            if(fieldObj) {
                                if (((fieldObj.value).replace(/^\s+|\s+$/g, '')).length==0) {
                                    if(fieldObj.type =='file'){ 
                                        alert('Please select a file to upload.'); 
                                        fieldObj.focus(); 
                                        return false;
                                    } 
                                    alert(fldLangVal[i] +' cannot be empty.'); 
                                    fieldObj.focus();
                                    return false;
                                }  else if(fieldObj.nodeName=='SELECT') {
                                    if(fieldObj.options[fieldObj.selectedIndex].value=='-None-') {
                                        alert(fldLangVal[i] +' cannot be none.'); 
                                        fieldObj.focus();
                                        return false;
                                    }
                                } else if(fieldObj.type =='checkbox'){
                                    if(fieldObj.checked == false){
                                        alert('Please accept  '+fldLangVal[i]);
                                        fieldObj.focus();
                                        return false;
                                    } 
                                } 
                                try {
                                    if(fieldObj.name == 'Last Name') {
                                        name = fieldObj.value;
                                    }
                                } catch (e) {}
                            }
                        }
                        
                        if(!validateEmail()){
                            return false;
                        }
                    
                    }

                    function tooltipShow(el){
                        var tooltip = el.nextElementSibling;
                        var tooltipDisplay = tooltip.style.display;
                        if(tooltipDisplay == 'none'){
                            var allTooltip = document.getElementsByClassName('zcwf_tooltip_over');
                            for(i=0; i<allTooltip.length; i++){
                                allTooltip[i].style.display='none';
                            }
                            tooltip.style.display = 'block';
                        }else{
                            tooltip.style.display='none';
                        }
                    }
                </script>
                
                <!-- Do not remove this --- Analytics Tracking code starts -->
                    <script id='wf_anal' src='https://crm.zohopublic.com/crm/WebFormAnalyticsServeServlet?rid=341bce4dd3c2383b6934b0e1fac0947bdf0ee4820919e84b0ee2b8f2278f8911gid17d057a202396f477ebd88d41924ec75a044a5dda8fe800733e245d4d6f8fa1agid885e3c1045bd9bdcc91bdf30f82b5696gid14f4ec16431e0686150daa43f3210513'></script>
                <!-- Do not remove this --- Analytics Tracking code ends. -->
            </form>
        </div>
        
    </div>
    
    <!-- questions_and_answers -->
    <?php if($qas){ ?>
	    <h2 class="section_title dark_title">שאלות ותשובות</h1>
        <div id="qa_accordion">
            <?php for($i=0; $i<count($qas); $i++){ ?>
                <div class="card">
                    <div class="card-header" id="heading<?php echo $i; ?>">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse<?php echo $i; ?>" aria-expanded="true" aria-controls="collapse<?php echo $i; ?>">
                                <?php echo $qas[$i]['question']; ?>
                            </button>
                        </h5>
                    </div>

                    <div id="collapse<?php echo $i; ?>" class="collapse" aria-labelledby="heading<?php echo $i; ?>" data-parent="#qa_accordion">
                        <div class="card-body">
                            <?php echo $qas[$i]['answer']; ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
            
        </div>
    <?php } 
        wp_reset_postdata();
    ?>

</div>