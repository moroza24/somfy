<?php
    // Get EU categories
    $pageID = get_the_ID();
    $sectionData = get_field('about_section', $pageID);

    if($sectionData){
        $sectionTitle = $sectionData['title'];
        $sectionContent = $sectionData['content'];
        $sectionIcons = $sectionData['icons'];
        $readMoreUrl = $sectionData['read_more_url']['url'];

?>
    <h2 class="section_title section_title_full"><?php echo $sectionTitle; ?></h2>

    <div class="container home_about__container">
        <div class="row">
            <div class="col">

                <?php
                    echo "<div class='home_about__content'>
                        $sectionContent
                        <a class='home_about__read_more' href='$readMoreUrl' alt='קרא עוד'>קרא עוד</a>    
                    </div>";

                    if(count($sectionIcons) > 0) {
                        echo '<div class="container home_about_icons">
                            <div class="row">';
                        foreach ($sectionIcons as $icon) {

                            $title = $icon['title'];
                            $sub_title = $icon['sub_title'];
                            $iconUrl = $icon['icon']['url'];
    
                            echo "<div class='col home_about_icon'>
                                    <div class='icon_box'>
                                        <img class='category_icon' src='$iconUrl' alt='$title'>
                                    </div>
                                    <h3 class='about_icon_title'>$title</h3>
                                    <p class='about_icon_subtitle'>$sub_title</p>
                                </div>";
                        }
                        echo '</div></div>';
                    }
                    
                ?>
            </div>
        </div>
    </div>
<?php
    }
    wp_reset_postdata();
?>