<?php
    // Get EU categories
    $pageID = get_the_ID();
    $sectionData = get_field('selected_categories_section', $pageID);

    if($sectionData){
        $sectionTitle = $sectionData['title'];
        $categories = $sectionData['selected_categories'];

?>
    <div class="container-fluid home_categories_slider__container">
        <div class="row">
            <div class="col-12">
                <h2 class="section_title section_title_full"><?php echo $sectionTitle; ?></h2>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <div class="categories_slider">
                    <?php
                        foreach ($categories as $category) {

                            $categoryTitle = $category['title'];
                            $linkUrl = $category['link']['url'];

                            $bgImg = $category['background_image']['url'];
                            $icon = $category['background_icon']['url'];

                            echo "<div class='category_slide'>
                            
                                    <a href='$linkUrl' alt='$categoryTitle'>
                                        <div class='category_card' style='background: url(\"$bgImg\");'>
                                            <div class='category_icon_link' href='$linkUrl'>
                                                <img class='category_icon' src='$icon' alt='$categoryTitle'>
                                            </div>
                                            <span class='category_title' href='$linkUrl'>$categoryTitle</span>
                                        </div>
                                    </a>

                                </div>";
                        }
                    ?>
                </div>
            </div>
        </div>
        
    </div>
    
<?php
    }
    wp_reset_postdata();
?>