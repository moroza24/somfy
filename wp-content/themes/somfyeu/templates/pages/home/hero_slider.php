<div id="hero_slider" class="hero_slider">
<?php 
    $pageID = get_the_ID();
    $heroSlides = get_field('hero_slider', $pageID);

    if(count($heroSlides) > 0){
        foreach($heroSlides as $slide){
            $backgroundImage = $slide['background_image']['url'];
            $title = $slide['title'];
            $box_text = $slide['box_text'];
            $button_label = $slide['button_label'];
            $button_url = $slide['button_url'];

            echo "<div class='hero_slide' style='background-image: url(\"$backgroundImage\");'>
                <div class='slide__content'>
                    <h1 class='slide__title'>$title</h1>
                    <p class='slide__text'>$box_text</p>
                    <div class='slide_cta__container'>
                        <a href='$button_url' class='sf_btn sf_btn-inline' alt='$button_label'>$button_label</a>
                    </div>
                </div>
            </div>";
        }
    }
    wp_reset_postdata();
?>
</div>