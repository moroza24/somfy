<?php
    $pageID = get_the_ID();
    $currentPagePermalink = get_the_permalink($pageID);
    $pageTitle = get_the_title($pageID);
    

    if (isset($_GET['term'])) {
        $term = $_GET['term'];
    } else {
        //Handle the case where there is no parameter
        $term = '';
    }
?>
<div class="container search_page somfy_shop">
    <ol class="breadcrumbs dark">
        <li><a href="<?php echo home_url(); ?>" alt='דף בית'>דף בית ></a></li>
        <li><a href="<?php echo $currentPagePermalink; ?>" alt='<?php echo $pageTitle; ?>'><?php echo $pageTitle; ?></a></li>
    </ol>
    <?php echo "<input id='search_term' type='hidden' value='$term'>"; ?>
    
    
    <h2 class="section_title section_title_full dark_title"><?php echo the_title(); ?></h2>

    <!-- Sort Filters Bar -->
    <div class="sort_filters__container">
        <h4 class="title">סדר לפי:</h4>

        <button class="sort_filter_btn" data-sort-type='post_title' data-sort-order='asc'>שם</button>
        <button class="sort_filter_btn" data-sort-type='base_price' data-sort-order='asc'>מחיר יורד</button>
        <button class="sort_filter_btn" data-sort-type='base_price' data-sort-order='desc'>מחיר עולה</button>
        <button class="sort_filter_btn" data-sort-type='on_sell' data-sort-order='desc'>מחיר מבצע</button>
    </div>
    <h5 class="results_counter" id="total_search_results"></h5>
    <div class="results_container" id="search_results_container">

        <!-- print results here -->
        <div class="results" id="search_results"></div>

        <!-- no_results_msg -->
        <div class='no_results_msg' id="search_page_no_results_msg">לא נמצאו תוצאות</div>
        

        <!-- Pagination cta -->
        <div class='load_more_cta' id="load_more_cta">
            <a alt="טען עוד"><i class="fas fa-plus"></i></a>
        </div>

        <!-- loader -->
        <div class="loader results_loader" id="search_loader"><div class="spinner-loader"></div></div>

    </div>
</div>