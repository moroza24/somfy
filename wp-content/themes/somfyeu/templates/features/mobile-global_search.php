
<div id="mobile_search__container">
    <i class="fas fa-times" id="close_mobile_search_container"></i>

    <div class="content">
        <div class="serach_input">
            <input type="text" name="global_search__term" id="mobile_search__term" placeholder="חפש אחר מוצר">
        </div>

        <div class="mobile_serach-results_container">
            <div class="loader" id="mobile_global_search_loader"><div class="spinner-loader"></div></div>
            <div id="mobile_no_results_msg">
                <p>לא נמצאו תוצאות</p>
            </div>
            <div id="mobile_serach-results">
                <ul class="results_list" id="mobile_products_results">
                    
                </ul>
                <ul class="results_list" id="mobile_taxonomy_results">
                    
                </ul>
                <a href="<?php echo home_url(); ?>/search" id="mobile_show_more_results" alt='הצג תוצאות נוספות'>הצג תוצאות נוספות</a>
            </div>
        </div>
    </div>
</div>