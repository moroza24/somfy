<div id="global_search">
    
    <div class="search_input_container">
        <a id="global_search__trigger" alt='חיפוש'>
            <img 
                src="<?php bloginfo('template_url'); ?>/assets/svg/004-loupe.svg" 
                alt="wishlist" loading="lazy">
        </a>

        <input type="text" name="global_search__term" id="global_search__term" placeholder="חפש אחר מוצר">
    </div>

    <div class="serach-results_container">
        <i class="fas fa-times" id="close_serach-results_container"></i>
        <div class="loader" id="global_search_loader"><div class="spinner-loader"></div></div>
        <div id="no_results_msg">
            <p>לא נמצאו תוצאות</p>
        </div>
        <div id="serach-results">
            <ul class="results_list" id="products_results">
                
            </ul>
            <ul class="results_list" id="taxonomy_results">
                
            </ul>
            <a href="<?php echo home_url(); ?>/search" id="show_more_results"  alt='הצג תוצאות נוספות'>הצג תוצאות נוספות</a>
        </div>
    </div>

    
</div>

