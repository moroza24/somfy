<div id="calculators_popup" class="popup__overlay">
    
    <div class="popup__container">
        
        <div class="container_header">
            <div class="calculator__title_icon"></div>
            <h2 id='calculator__title'></h2>
            <a class="popup__overlay__close" alt='סגור'>>
                <i class="fas fa-times"></i>
            </a>
        </div>
        <div class="container_body">

            <!-- Calculators -->
            <div id="calculators_steps__container">
                <!-- Calc 1 -->
                <div id="calculator_1" class="calculator__container">
                    
                    <!-- Step 1 -->
                    <form class="step__form_container hidden" data-step="1">
                        <div class="step_number">1</div>
                        <div class="step_content">
                            <h3 class="step_title">מאפייני חלון:</h3>
                            
                            <!-- רוחב -->
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">
                                        <span class="label">רוחב בס"מ</span>
                                        <img src="<?php bloginfo('template_url'); ?>/assets/calc_icons/width.svg" alt="width">
                                    </span>
                                </div>
                                <input type="number" 
                                    class="form-control" 
                                    id="calc_1__window_width"
                                    placeholder='רוחב בס"מ'
                                    aria-label='רוחב בס"מ' 
                                    aria-describedby="basic-addon1" 
                                    min="40" 
                                    max="650"
                                    step="1"
                                    required
                                    onkeydown="if(event.key==='.'){event.preventDefault();}"  oninput="event.target.value = event.target.value.replace(/[^0-9]*/g,'');">
                            </div>
                            <p class="form_error" data-for='calc_1__window_width'>חובה לסמן רוחב חלון</p>

                            <!-- גובה -->
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon2">
                                        <span class="label">גובה בס"מ</span>
                                        <img src="<?php bloginfo('template_url'); ?>/assets/calc_icons/height.svg" alt="height">
                                    </span>
                                </div>
                                <input type="number" 
                                    class="form-control" 
                                    id="calc_1__window_height"
                                    placeholder='גובה בס"מ' 
                                    aria-label='גובה בס"מ'
                                    aria-describedby="basic-addon2" 
                                    min="60" 
                                    max="340"
                                    step="1"
                                    required
                                    onkeydown="if(event.key==='.'){event.preventDefault();}"  oninput="event.target.value = event.target.value.replace(/[^0-9]*/g,'');">
                            </div>
                            <p class="form_error" data-for='calc_1__window_height'>חובה לסמן גובה חלון</p>

                            
                            <div class="step_actions">
                                <button class="sf_btn next_step" data-calculator='1' data-next='2'>הבא</button>
                            </div>
                        </div>
                    </form>

                    <!-- Step 2 -->
                    <form class="step__form_container hidden" data-step="2">
                        <div class="step_number">2</div>
                        <div class="step_content">
                            <h3 class="step_title">מאפייני התריס:</h3>
                            
                            <!-- סוג תריס -->
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="calc_1__blind_type">
                                        <img src="<?php bloginfo('template_url'); ?>/assets/calc_icons/step_height.svg" alt="step_height">
                                    </label>
                                </div>
                                <select class="custom-select custom-select-lg" id="calc_1__blind_type" required>
                                    <option value="-1">סוג תריס</option>
                                    <option value="1">משוך</option>
                                    <option value="2">מוקצף</option>
                                    <option value="3">סורג</option>
                                </select>
                            </div>
                            <p class="form_error" data-for='calc_1__blind_type'>חובה לבחור סוג שלב</p>

                            <!-- גובה שלב -->
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="calc_1__blind_height">
                                        <img src="<?php bloginfo('template_url'); ?>/assets/calc_icons/step_height.svg" alt="step_height">
                                    </label>
                                </div>
                                <select class="custom-select custom-select-lg" id="calc_1__blind_height" required>
                                    <option value="-1">גובה שלב</option>
                                </select>
                            </div>
                            <p class="form_error" data-for='calc_1__blind_height'>חובה לבחור גובה שלב תריס</p>

                            <!-- סוג מנוע -->
                            <div class="input-group mb-3 input-group_radio">
                                <div class="input-group-prepend prepend-full">
                                    <span class="input-group-text input-group-radio">
                                        <img src="<?php bloginfo('template_url'); ?>/assets/calc_icons/wired_1.svg" alt="wired_1">
                                        סוג מנוע
                                    </span>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" 
                                        name="calc_1__tech" id="calc_1__tech_wired" value="4" data-title='מחווט' required>
                                    <label class="form-check-label" for="calc_1__tech_wired">מחווט</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" 
                                        name="calc_1__tech" id="calc_1__tech_wireless" data-title='ידני' value="2">
                                    <label class="form-check-label" for="calc_1__tech_wireless">אלחוטי</label>
                                </div>
                            </div>
                            <p class="form_error" data-for='calc_1__tech'>חובה לבחור סוג מנוע</p>


                            <!-- משקל -->
                            <!-- <div class="input-group mb-3">
                                <div class="input-group-prepend prepend-full">
                                    <span class="input-group-text" id="basic-addon3">
                                        <img src="<?php bloginfo('template_url'); ?>/assets/calc_icons/weight.svg" alt="weight">
                                        משקל למ"ר/כללי
                                    </span>
                                </div>
                                <input type="number" 
                                    class="form-control" 
                                    id="calc_1__blinds_mr_weight"
                                    aplaceholder='משקל למ"ר' 
                                    aria-label='משקל למ"ר' 
                                    aria-describedby="basic-addon3"
                                    min="0"
                                    step="0.01"
                                    required
                                    disabled>
                                <input type="number" 
                                    class="form-control" 
                                    id="calc_1__blinds_weight"
                                    aria-label='משקל כללי' 
                                    aria-describedby="basic-addon3"
                                    min="0"
                                    step="0.01"
                                    required
                                    disabled>
                            </div> -->

                            <div class="step_actions">
                                <button class="sf_btn prev_step" data-calculator='1' data-prev='1'>הקודם</button>
                                <button class="sf_btn submit_calc" data-calculator='1'>מצא לי מנוע</button>
                            </div>
                            <div class="loader" id="calc_1__loader"><div class="spinner-loader"></div></div>
                            <p id="calc_1__error"></p>
                        </div>
                    </form>
                </div>


            </div>
        </div>
    </div>
</div>