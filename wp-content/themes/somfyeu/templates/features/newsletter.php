<div id="newsletter">
    <div class="newsletter_container">
        <div class="newsletter__content">
            <h3>הרשמה לניוזלטר</h3>
            <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Obcaecati ullam dolores assumenda est explicabo magnam quas sit laborum! Provident non dicta quibusdam nihil incidunt minima quo dolorem illum ea excepturi.
            </p>
        </div>
        <div class="newsletter__form">
            <form id="newsletter_signup__form">
                <input type="email" required name="email" id="newsletter_email_input" placeholder="כתובת מייל">
                <input type="submit" value="הרשם" id="newsletter_submit">
                <div class="loader" id="newsletter__form_loader"><div class="spinner-loader"></div></div>
                
            </form>
            <div id="form_submit_msg">
                
            </div>
        </div>
    </div>

</div>
