<?php if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! is_user_logged_in() ) {
	um_reset_user();
} 
$pageThum = get_the_post_thumbnail_url();

?>
<?php
    $pageID = get_the_ID();
    $currentPagePermalink = get_the_permalink($pageID);
    $pageTitle = get_the_title($pageID);

?>
<div id="registretion-page" class="container">
    <ol class="breadcrumbs dark">
        <li><a href="<?php echo home_url(); ?>" alt='דף בית'>דף בית ></a></li>
        <li><a href="<?php echo $currentPagePermalink; ?>" alt='<?php echo $pageTitle; ?>'><?php echo $pageTitle; ?></a></li>
    </ol>
	<h2 class="section_title dark_title"><?php the_title(); ?></h2>

    <div class="registration__container">
        <div class="reg_form">
            <?php echo do_shortcode( '[ultimatemember form_id="41"]' ); ?>
        </div>

        <div class="graphics">
            <img src="<?php echo $pageThum;?>" alt="<?php the_title(); ?>">
        </div>
    </div>
</div>