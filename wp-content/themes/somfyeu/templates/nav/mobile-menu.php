<div id="mobile-menu__overlay">
    <div class="mobile-menu__top">
        <a class="side_icon calculator_trigger calculator_trigger__mobile_nav" alt="בחירת מנוע">
            <div class="icon"></div>
            <span>
                מחשבון<br>בחירת מנוע
            </span>   
        </a>
        <a class="mobile-menu__close" alt='סגור'>
            <i class="fas fa-times"></i>
        </a>
    </div>

    
    <?php
        $shopUrl = home_url() .'/shop';
        // Get EU categories
        $categories = new WP_Query(array(
            'post_type' => 'product_category',
            'posts_per_page'   => -1,
            'meta_key' => 'sort_order',
            'orderby' => 'meta_value_num',
            'order' => 'ASC',
            'meta_query' => array(
                array(
                    'key' => 'show_on_site',
                    'compare' => '=',
                    'value' => TRUE
                )
            )
        )); 
        if($categories){

            echo "<div class='accordion accordion-flush' id='mobile_category__accordion'>";
                    
            while ($categories->have_posts()) {
                $categories->the_post();

                $categoryTitle = get_the_title();
                $categoryID = get_the_ID();
                $permalink = get_permalink();
                $groups = get_field('sub_groups');

                // mega menu banner
                $megaImg = get_field('mega_img');
                $megaLink = get_field('mega_link');
                
                echo "<div class='accordion-item'>";

                if($groups){
                    // category Trigger
                    echo "<h2 class='accordion-header' id='flush-headingCategory$categoryID'>
                            <button class='accordion-button collapsed' type='button' 
                                data-bs-toggle='collapse' 
                                data-bs-target='#flush-collapseCategory$categoryID' 
                                aria-expanded='false' 
                                aria-controls='flush-collapseCategory$categoryID'>
                                $categoryTitle
                            </button>
                        </h2>";
                    echo "<div id='flush-collapseCategory$categoryID' class='accordion-collapse collapse' 
                            aria-labelledby='flush-headingCategory$categoryID' 
                            data-bs-parent='#mobile_category__accordion'>
                            <div class='accordion-body'>";
                
                    // Groups
                    echo "<div class='accordion accordion-flush' id='mobile_groups__accordion$categoryID'>";
                    foreach ($groups as $group){
                        $groupID = $group->ID;
                        $groupTitle = get_the_title($groupID);
                        $subgroups = get_field('2_subgroups', $groupID);
                        $groupPermalink = "$shopUrl?category=$categoryID&subgroup=$groupID";
                        
                        echo "<div class='accordion-item'>";
                        if($subgroups){
                            // category Trigger
                            echo "<h2 class='accordion-header' id='flush-headingGroup$categoryID-$groupID'>
                                <button class='accordion-button collapsed' type='button' 
                                    data-bs-toggle='collapse' 
                                    data-bs-target='#flush-collapseGroup$categoryID-$groupID' 
                                    aria-expanded='false' 
                                    aria-controls='flush-collapseGroup$categoryID-$groupID'>
                                    $groupTitle
                                </button>
                            </h2>";
                            echo "<div id='flush-collapseGroup$categoryID-$groupID' class='accordion-collapse collapse' 
                                    aria-labelledby='flush-headingGroup$categoryID-$groupID' 
                                    data-bs-parent='#mobile_Group__accordion'>
                                    <div class='accordion-body'>";
                                    
                            // subgroups
                            foreach ($subgroups as $subgroup){
                                $subgroupTitle = get_the_title($subgroup->ID);
                                $sub_subgroupID = $subgroup->ID;
                                $subgroupPermalink = "$shopUrl?category=$categoryID&subgroup=$groupID&subsubgroup=$sub_subgroupID";
                                
                                echo "<a class='subgroup_button' href='$subgroupPermalink' alt='$subgroupTitle'>
                                        <i class='fas fa-angle-left'></i>
                                        $subgroupTitle
                                    </a>";
                            }

                            echo "</div>
                            </div>";
                        } else {
                            echo "<h2 class='accordion-header' id='flush-headingGroup$categoryID-$groupID'>
                                <a href='$groupPermalink' class='accordion-button group-button' alt='$groupTitle'>
                                    $groupTitle
                                </a>
                            </h2>";
                        }
                        echo "</div>";
                        wp_reset_postdata();
                    }
                    echo "</div>";
                    
                    // Print subgroups
                    if($megaLink && $megaImg):
                        echo "<div class='megamenu_mobil__banner_container'>
                            <a href='".$megaLink['url']."' alt='".$megaLink['title']."' target='".$megaLink['target']."'>
                                <img src='".$megaImg['url']."' alt='".$megaLink['title']."'>
                            </a>
                            </div>";
                    endif;


                    echo "</div>
                    </div>";
                } else {
                    echo "<h2 class='accordion-header' id='flush-headingCategory$categoryID'>
                        <a href='$permalink' class='accordion-button category-button' alt='$categoryTitle'>
                            $categoryTitle
                        </a>
                    </h2>";
                }

                echo "</div>";
                wp_reset_postdata();
            }

            echo "</div>";
        }
        wp_reset_postdata();

    ?>

</div>