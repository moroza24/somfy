<div id="categories_nav">
    <?php
        $shopUrl = home_url() .'/shop';

        while(have_posts()){
            the_post();
            $currentUrl = get_permalink();
        }

        if(!isset($currentUrl)) $currentUrl = '';

        wp_reset_postdata();
        // Get EU categories
        $categories = new WP_Query(array(
            'post_type' => 'product_category',
            'posts_per_page'   => -1,
            'meta_key' => 'sort_order',
            'orderby' => 'meta_value_num',
            'order' => 'ASC',
            'meta_query' => array(
                array(
                    'key' => 'show_on_site',
                    'compare' => '=',
                    'value' => TRUE
                )
            )
        )); 

        if($categories){

            echo "<ul class='categories-links'>";
                    
            while ($categories->have_posts()) {
                $categories->the_post();

                $categoryTitle = get_the_title();
                $permalink = get_permalink();
                $categoryID = get_the_ID();
                
                $currentClass = $currentUrl == $permalink ? 'active' : '';

                // get groups
                $sub_groups = get_field('sub_groups');

                // mega menu banner
                $megaImg = get_field('mega_img');
                $megaLink = get_field('mega_link');

                echo "<li class='category_link $currentClass'>
                    <a href='$permalink' alt='$categoryTitle'>
                        $categoryTitle
                    </a>";
                
                if($sub_groups){
                    $subgroupsHtml = '';

                    // Show MEGA MENU
                    // mega menu start
                    echo "<div class='megamenu__container'><div class='container'>";


                    // Print Groups
                    echo "<div class='megamenu__groups_container'>";
                    
                    foreach ($sub_groups as $sub_group){
                        $groupID = $sub_group->ID;
                        $groupTitle = get_the_title($groupID);
                        $groupPermalink = get_permalink($groupID);
                        $sub_subgroups = get_field('2_subgroups', $groupID);

                        echo "<div class='group_link__container' data-target='$categoryID-group_tab_$groupID'>
                            <a class='group_link' href='$shopUrl?category=$categoryID&subgroup=$groupID'  alt='$groupTitle'>$groupTitle";
                        if($sub_subgroups) echo "<i class='fas fa-angle-left'></i>";
                        echo "</a></div>";

                        if($sub_subgroups){
                            $subgroupsHtml .= "<div class='subgroups_links__tab' id='$categoryID-group_tab_$groupID'>";
                            foreach ($sub_subgroups as $sub_subgroup){
                                $subgroupTitle = get_the_title($sub_subgroup->ID);
                                $sub_subgroupID = $sub_subgroup->ID;
                                $subgroupPermalink = "$shopUrl?category=$categoryID&subgroup=$groupID&subsubgroup=$sub_subgroupID";
                                
                                $subgroupsHtml .= "<div class='subgroup_link__container'>
                                    <a class='subgroup_link' href='$subgroupPermalink' alt='$subgroupTitle'>
                                        <i class='fas fa-angle-left'></i>
                                        $subgroupTitle
                                    </a>
                                </div>";
                            }
                            $subgroupsHtml .= "</div>";
                        }
                    }
                    echo "</div>";


                    // Print subgroups
                    echo "<div class='megamenu__subgroups_container'>";
                    echo $subgroupsHtml;
                    echo "</div>";


                    // Print subgroups
                    if($megaLink && $megaImg):
                        echo "<div class='megamenu__banner_container'>
                            <a href='".$megaLink['url']."' alt='".$megaLink['title']."' target='".$megaLink['target']."'>
                                <img src='".$megaImg['url']."' alt='".$megaLink['title']."'>
                            </a>
                            </div>";
                    endif;


                    // mega menu end
                    echo "</div></div>";
                }

                echo "</li>";
            }
            echo "</ul>";
        }
        wp_reset_postdata();
    
    ?>
</div>