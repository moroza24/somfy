<div id="top-menu">
    <nav class="navbar navbar-light" id="top_nav">
        
        <!-- Logo -->
        <a class="navbar-brand" href="<?php echo home_url(); ?>" alt='בית'>
            <img src="<?php bloginfo('template_url'); ?>/assets/img/somfy-logo.png" height="50" class="d-inline-block align-top" alt="Somfy Logo" loading="lazy">
        </a>
        
        <!-- side_nav -->
        <?php get_template_part('templates/nav/side_nav'); ?>
    </nav>

    <!-- Categories Menu -->
    <?php get_template_part('templates/nav/categories_nav'); ?>

    <a class="side_icon calculator_trigger calculator_trigger__top_nav" alt="בחירת מנוע">
        <div class="icon"></div>
        <span>
            מחשבון<br>בחירת מנוע
        </span>   
    </a>
</div>
<!-- Mobile menu overlay -->
<?php get_template_part('templates/nav/mobile-menu'); ?>

<!-- Mobile global search -->
<?php get_template_part('templates/features/mobile-global_search'); ?>
