
<div id="side_nav">

    <!-- Global Search -->
    <?php get_template_part('templates/features/global_search'); ?>

    <div class="side_icon user_menu">
        <a id="user-menu__trigger" alt="תפריט משתמש">
            <img src="<?php bloginfo('template_url'); ?>/assets/svg/003-user-profile.svg" alt="user-menu" loading="lazy">
        </a>
        <?php get_template_part('templates/nav/user-menu_dropdown'); ?>
    </div>

    <div class="side_icon">
        <a class="" id="cart__trigger" alt="עגלת קניות">
            <img class='not-active' src="<?php bloginfo('template_url'); ?>/assets/svg/002-shopping-cart.svg" alt="cart" loading="lazy">        
        </a>
    </div>

    <?php
        if(is_user_logged_in()){
    ?>
    <div class="side_icon">
        <a class="side_icon" id="wishlist__trigger" href="<?php echo home_url(); ?>/wishlist" alt="מועדפים">
            <img src="<?php bloginfo('template_url'); ?>/assets/svg/001-heart-shape-outline.svg" alt="מועדפים" loading="lazy">
        </a>
    </div>
    <?php
        };
    ?>
    <a id="mobile_menu__trigger" alt="mobile menu">
        <span></span><span></span><span></span>
    </a>

    <?php get_template_part('templates/shop/popup_cart'); ?>
    
</div>
