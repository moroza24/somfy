<?php 

    get_header(); 


    while(have_posts()) {

        the_post();
        $hero_shortcode = get_field('hero_shortcode');
         ?>

        <div id="home-page">
            <!-- hero_slider -->
            <?php get_template_part('templates/pages/home/hero_slider'); ?>

            <!-- product_categories_links -->
            <?php get_template_part('templates/pages/home/product_categories_links'); ?>

            <!-- about -->
            <?php get_template_part('templates/pages/home/about'); ?>

            <!-- selected_products_products -->
            <?php get_template_part('templates/shop/selected_products_products'); ?>

        </div>
        
    <?php }
    get_footer(); 
?>