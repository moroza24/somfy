// 3rd party packages from NPM
import $ from 'jquery';
import slick from 'slick-carousel';
import Cookies from 'js-cookie'

// Our modules / classes

// General js
import Inputs from './modules/features/Inputs';
import TopNav from './modules/TopNav';
import GlobalSearch from './modules/features/GlobalSearch';
import Newsletter from './modules/features/Newsletter';
import Calculator from './modules/features/Calculator';
 
// Pages
import Home from './modules/pages/home';
import SearchPage from './modules/pages/SearchPage';
import Account from './modules/pages/Account';


// Shop
import Cart from './modules/shop/Cart';
import ShopPage from './modules/shop/ShopPage';
import ProductPage from './modules/shop/ProductPage';
import CheckoutPage from './modules/shop/CheckoutPage';
import StockNotifictaion from './modules/shop/StockNotifictaion';
import Wishlist from './modules/shop/Wishlist';
import Category from './modules/shop/Category';

import OrderSync from './modules/features/OrderSync';

$.fn.extend({
    toggleText: function(a, b){
        return this.text(this.text() == b ? a : b);
    }
});


// Instantiate a new object using our modules/classes
$(document).ready(() => {
    var pathname = window.location.pathname;

    var inputs = new Inputs();
    var topNav = new TopNav();
    var globalSearch = new GlobalSearch();
    var newsletter = new Newsletter();

    // Shop
    var cart = new Cart();
    var stockNotifictaion = new StockNotifictaion();
    var wishlist = new Wishlist(stockNotifictaion, cart);
    var category = new Category();
    var calculator = new Calculator();

    var orderSync = new OrderSync();

    // Home page scripts
    if(pathname == '/' || pathname == '/somfy/' || pathname == '/staging/'){
        var home = new Home(stockNotifictaion, wishlist, cart);
    }

    // Search page scripts
    if(pathname == '/search/' || pathname == '/somfy/search/' || pathname == '/staging/search/'){
        var searchPage = new SearchPage(stockNotifictaion, wishlist, cart);
    }

    // Search page scripts
    if(pathname == '/wishlist/' || pathname == '/somfy/wishlist/' || pathname == '/staging/wishlist/'){
        wishlist.printUsersWishlistPage();
    }
    
    // Account page scripts
    if(pathname.includes('/account/')){
        var account = new Account();
    }

    if(pathname.includes('/product_subgroup/') || pathname.includes('/product_2_subgroup/') || pathname.includes('/shop/')){
        var shopPage = new ShopPage(stockNotifictaion, wishlist, cart, inputs);
    }

    if(pathname.includes('/product/')){
        var productPage = new ProductPage();
    }

    if(pathname.includes('/checkout/')){
        var checkoutPage = new CheckoutPage(cart);
    }
    


    // general small scripts


    // $('.somfy_top_banner__close').on('click', ()=>{
    //     $('#somfy_top_banner').fadeOut('fast');
    //     $('html').css('margin-top' , 0);
    // })

    
    // Set redirect after registration - to Shop || to Checkout
    if(pathname.includes('/register/')){
        let checkout_registration_redirect = localStorage.getItem('checkout_registration_redirect');
        
        if(checkout_registration_redirect == 'true'){
            let checkoutURL = somfyData.root_url + "/checkout";
            $('.um-form[data-mode="register"').find('form').append(`
                <input type="hidden" name="redirect_to" value="${checkoutURL}">
            `)

            localStorage.removeItem('checkout_registration_redirect');
        } else {
            let shopURL = somfyData.root_url + "/shop";
            $('.um-form[data-mode="register"').find('form').append(`
                <input type="hidden" name="redirect_to" value="${shopURL}">
            `)
        }
    }
});