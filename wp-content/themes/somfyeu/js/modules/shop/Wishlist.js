import $ from 'jquery';

class Wishlist {
    
    constructor(stockNotifictaion, cart) {
        this.stockNotifictaion = stockNotifictaion;
        this.cart = cart;

        this.usersWishlist;
        this.currentClickedCTA;
        this.wishlistPage = false;

        this.initEvents();
    }

    initEvents() {
        this.getUsersWishlist();

        this.addClickEvent();
    }


    printUsersWishlistPage(){
        this.wishlistPage = true;
        $('#wishlist_results').html('');
        $('#wishlist_loader').show();
        $('#total_wishlist_results').html('').hide();
        $('#wishlist_no_results_msg').hide();

        $.ajax({
            url : somfyData.ajax_url,
            type : 'POST',
            data : {
                action : 'somfy_eu_get_by_user',
                getFullData: true,
                nonce: somfyData.ajax_nonce
            },
            dataType: "json",
            success: (response) =>{
                if(response.status){
                    let total_wishlist_results = response.usersWishlist.length;

                    if(total_wishlist_results > 0){
                        // update ctas by users wishlist
                        this.printProducts(response.usersWishlist);
                        $('#total_wishlist_results').html('סה"כ תוצאות: ' + total_wishlist_results).show();
                        this.addClickEvent();
                    } else {
                        $('#wishlist_no_results_msg').show();
                    }
                } else {
                    // SHOW ERROR
                    if(response.user_logged) console.warn(response);
                }

                $('#wishlist_loader').hide();

            },
            error:  (jqXHR, status) => {
                console.warn(jqXHR);
                console.warn(status);
            }
        });
    }

    printProducts(products){
        let resultsHtml = '';

        products.forEach((product) => {

            // print product thumbnail & tags
            resultsHtml += `
                <div class='product-card'>
                    <div class='product-thumbnail-col'>`;
            
            // print discount tag
            if(product.meta.discount_percentage && product.meta.discount_percentage != ''){
                resultsHtml += `
                    <span class="dicount_tag">
                        <span class='tag__title'>הנחה</span><br>
                        ${product.meta.discount_percentage}%
                    </span>`;
            }

            // print out_of_stock tag
            if(product.meta.stock_quantity < 3){
                resultsHtml += `<span class="out_of_stock_tag">אזל במלאי</span>`;
            }

            // wishlistCTA
            resultsHtml += `<a class='wishlist_cta active' alt='מועדפים' data-product='${product.product_id}'>
                <i class="fas fa-heart"></i>
            </a>`;

            resultsHtml += `
                    <a href='${product.permalink}' alt='${product.post_title}'>
                        <img 
                            src='${product.thumbnail_url}' 
                            alt='${product.post_title}' 
                            class='product-thumbnail'>
                    </a>
                </div>`;


            // print product title
            resultsHtml += `
                <div class='product-meta-container'>
                    <a href="${product.permalink}" class='product-title' alt='${product.post_title}'>${product.post_title}</a>
                </div>`;

            // print product CTA & prices
            resultsHtml += `
                <hr class="divider">
                <div class="product-cta-container">
                    <div class='product-cta'>`;
                if(product.meta.stock_quantity < 3){
                    resultsHtml += `<button class='get_stock_reminder get_stock_reminder_small' data-product='${product.product_id}'>
                            תודיעו לי שהמוצר חוזר
                        </button>`;
                } else {
                    resultsHtml += `<button class='add_to_cart__cta add_to_cart__cta_small' 
                        data-product='${product.product_id}' 
                        data-stock='${product.meta.stock_quantity}'
                        data-add_warranty='false'
                        data-warranty_applicable='${product.warranty_applicable}'>
                            <img 
                                src="${somfyData.template_url}/assets/svg/002-shopping-cart.svg" 
                                alt="cart" 
                                loading="lazy">
                            הוסף לסל
                        </button>`;
                }
                resultsHtml += `</div>
                    <div class='product-price'>`;
            
            if(product.meta.on_sell == true){
                resultsHtml += `
                            <span class='old_price'>
                                <span class='currency_symbol'>₪</span>${product.meta.base_price}
                            </span>

                            <span class='discount_price'>
                                <span class='currency_symbol'>₪</span>${product.meta.discount_price}
                            </span>`;
            } else {
                resultsHtml += `
                    <span class='base_price'>
                                <span class='currency_symbol'>₪</span>${product.meta.base_price}
                            </span>`;
            }
            resultsHtml += `
                </div></div></div>`;
        });
        
        $('#wishlist_results').html(resultsHtml);

        this.stockNotifictaion.initCtaEvents();
        this.addClickEvent();
        this.getUsersWishlist();
        this.cart.initAddToCartEvent();
    }


    getUsersWishlist(){
        $.ajax({
            url : somfyData.ajax_url,
            type : 'POST',
            data : {
                action : 'somfy_eu_get_by_user',
                nonce: somfyData.ajax_nonce
            },
            dataType: "json",
            success: (response) =>{

                if(response.status){
                    // update ctas by users wishlist
                    this.usersWishlist = response.usersWishlist;

                    if(this.usersWishlist.length > 0){
                        this.updateByUsersWishlist();
                        $('#wishlist__trigger').addClass('with-items');
                    } else {
                        $('#wishlist__trigger').removeClass('with-items');
                    }
                } else {
                    // SHOW ERROR
                    if(response.user_logged) console.warn(response);
                }
            },
            error:  (jqXHR, status) => {
                console.warn(jqXHR);
                console.warn(status);
            }
        });
    }


    updateByUsersWishlist(){
        let usersItems = this.usersWishlist;

        usersItems.forEach(item => {
            $(`.wishlist_cta[data-product="${item.product_id}"]`).addClass('active').html('<i class="fas fa-heart"></i>');
        });
    }


    addClickEvent(){
        let addToWishlist_ctas = $('.wishlist_cta');

        addToWishlist_ctas.off('click').on('click', (e) => {
            this.currentClickedCTA = e.currentTarget;
            let isActive = $(e.currentTarget).hasClass('active');
            let wishlistAction = !isActive ? 'somfy_eu_wishlist_add' : 'somfy_eu_wishlist_remove';
            let productID = e.currentTarget.dataset.product;

            $.ajax({
                url : somfyData.ajax_url,
                type : 'POST',
                data : {
                    action : wishlistAction,
                    productID: productID,
                    nonce: somfyData.ajax_nonce
                },
                dataType: "json",
                success: (response) =>{
    
                    if(response.status){
                        $(this.currentClickedCTA).toggleClass('active');
                        
                        if($(this.currentClickedCTA).hasClass('active')){
                            $(this.currentClickedCTA).html('<i class="fas fa-heart"></i>');
                        } else {
                            $(this.currentClickedCTA).html('<i class="far fa-heart"></i>');
                        }

                        if(this.wishlistPage) this.printUsersWishlistPage();
                        this.getUsersWishlist();
                    } else {
                        // SHOW ERROR
                        if(!response.user_logged){
                            // show popup - need to be registered user to use wishlist
                            $('#wishlist_popup').fadeIn('fast');
                            $('.popup__overlay__close').on('click', (e) => {
                                $('#wishlist_popup').fadeOut('fast');
                            })
                        }
                    }
                },
                error:  (jqXHR, status) => {
                    console.warn(jqXHR);
                    console.warn(status);
                }
            });
        })
    }
}

export default Wishlist;