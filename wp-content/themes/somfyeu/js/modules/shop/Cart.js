import $ from 'jquery';
import Cookies from 'js-cookie'
import StockNotifictaionMax from './StockNotifictaionMax';

class Cart {

    constructor() {
        this.stockNotifictaionMax = new StockNotifictaionMax();

        // cart popup
        this.cartPopupTrigger = $('#cart__trigger');
        this.cartPopupClose = $('.cart_popup__close');
        this.cartPopup = $('#cart_popup__container');
        this.userMenuDropdown = $('#user-menu__dropdown');

        // add to cart flow
        this.addToCartPopupClose = $('.add_to_cart__popup__close');
        this.addToCartPopup = $('#add_to_cart__popup');
        this.addToCartPopupLoader = $('#add_to_cart__loader');
        this.addToCartPopupSuccess = $('.add_to_cart__success');
        this.maxAmountReachedMsg = $('.max_amount_reached');
        this.maxAmountReachedMsgQty = $('#max_amount_reached_qty');
        this.stockNotifictaion_maxamount__trigger = $('#stock_notifictaion_maxamount__trigger');
        this.oldAmountInputVal = null;

        this.userIsLoggedIn = false;

        // cart results 
        this.cartItemsContainer = $('.cart_items__container');
        this.cartSummeryContainer = $('.cart_summery');
        this.subSummeryVal = $('.sub_summery_val');
        this.discountSummeryVal = $('.discount_summery_val');
        this.totalSummeryVal = $('.total_summery_val');
        this.noCartItemsMsg = $('.not_cart_items');

        this.cartItems;
        this.order_subSummary;
        this.order_totalDiscount;
        this.order_totalPrice;

        // add warranty popup
        this.add_warranty_popup = $('#add_warranty_popup');
        this.add_warranty_popup__close = $('#add_warranty_popup .popup__overlay__close');
        this.add_warranty__quantity_input = $('#add_warranty__quantity_input');
        this.add_warranty__submit = $('#add_warranty__submit');
        this.add_warranty_checkbox = $('#add_warranty_checkbox');

        this.productDataResults;
        
        this.initEvents();
        this.initAddToCartEvent();   


    }

    initEvents() {
        // get current cart
        this.setInitCartItemsData();

        this.cartPopupTrigger.on('click', ()=>{
            this.cartPopup.fadeToggle('fast');
            this.cartPopupTrigger.toggleClass('active');
            
            this.userMenuDropdown.fadeOut();
        });

        this.cartPopupClose.on('click', ()=>{
            this.cartPopup.fadeOut('fast');
            this.cartPopupTrigger.removeClass('active');
        });

        this.addToCartPopupClose.on('click', ()=>{
            this.addToCartPopup.fadeOut('fast');
            this.addToCartPopupLoader.show();
            this.addToCartPopupSuccess.hide();
            this.maxAmountReachedMsg.hide();
            this.maxAmountReachedMsgQty.html('');
            this.stockNotifictaion_maxamount__trigger.attr('data-product', '');
        });

        this.stockNotifictaion_maxamount__trigger.on('click', (e)=>{
            let productID = e.currentTarget.dataset.product;
            this.addToCartPopupClose.trigger('click');
            this.stockNotifictaionMax.openPopup(productID);
        })

        this.add_warranty_popup__close.on('click', (e) => {
            this.add_warranty_popup.fadeOut('fast');
        })

        this.add_warranty__submit.on('click', (e) => {
            this.addToCartFromWarranty(e);
        });

        this.add_warranty_checkbox.on('change', (e)=>{
            let isChecked = e.currentTarget.checked;

            if(isChecked){
                $(add_warranty__submit).attr('data-add_warranty', "true");
            } else {
                $(add_warranty__submit).attr('data-add_warranty', "false");
            }
        })
    }


    setInitCartItemsData(){
        $.ajax({
            url : somfyData.ajax_url,
            type : 'POST',
            data : {
                action : 'somfy_eu_get_cart',
                nonce: somfyData.ajax_nonce
            },
            dataType: "json",
            success: (response) =>{
                if(response.status && !response.user_logged){
                    // user not logged in - get cartItems from localStorage
                    this.cartItems = JSON.parse(localStorage.getItem('eu_cartItems'));
                    this.userIsLoggedIn = false;
                } else if(response.status && response.user_logged){
                    // user is logged in - get cartItems from response
                    /* if user just registered from the checkout page
                    * check localStorage 'checkout_registration' 
                    * and set cartItems from localStorage
                    * and update cartItems in DB
                    */
                    let checkout_registration = localStorage.getItem('eu_checkout_registration');
                    if(checkout_registration == 'true'){
                        this.cartItems = JSON.parse(localStorage.getItem('eu_cartItems'));
                        this.updateCartDB(this.cartItems);
                    } else {
                        this.cartItems = response.cart_items;
                        localStorage.setItem('eu_cartItems', JSON.stringify(this.cartItems));
                    }

                    this.userIsLoggedIn = true;
                } else {
                    // SHOW ERROR
                    console.warn(response);
                }

                this.updateCartPopup();
            },
            error:  (jqXHR, status) => {
                console.warn(jqXHR);
                console.warn(status);
            }
        });
    }



    updateCartPopup(updateFromCheckout = false){
        if(this.cartItems && this.cartItems.length > 0){
            this.cartPopupTrigger.addClass('with-items');

            // get full products data 
            this.printCartPopupContent();

            this.cartSummeryContainer.show();
            this.noCartItemsMsg.hide();
        } else {
            let pathname = window.location.pathname;
            if(pathname.includes('/checkout/') && !updateFromCheckout) window.location.replace(somfyData.root_url + '/shop');
            this.cartItemsContainer.html('');

            this.cartPopupTrigger.removeClass('with-items');
            this.cartSummeryContainer.hide();
            this.noCartItemsMsg.show();
        }
    }


    printCartPopupContent(){
        $.ajax({
            url : somfyData.ajax_url,
            type : 'POST',
            data : {
                action : 'somfy_eu_get_cart_product_data',
                cartItems: this.cartItems,
                nonce: somfyData.ajax_nonce
            },
            dataType: "json",
            success: (response) =>{
                if(response.status && response.productDataResults.length > 0){
                    let subSummary = 0, totalDiscount = 0, totalPrice = 0;

                    let productsHtml = '';
                    let kidsArray = [];

                    this.productDataResults = response.productDataResults;

                    for(let product of response.productDataResults){
                        subSummary += parseInt(product.base_price) * parseInt(product.amount);
                        let finalPrice = parseInt(product.base_price) * parseInt(product.amount);
                        if(product.on_sell){
                            totalDiscount += (product.base_price - product.discount_price) * parseInt(product.amount);
                            finalPrice = parseInt(product.discount_price) * parseInt(product.amount);
                        }
                        totalPrice += finalPrice;

                        let kidsHtml = '';
                        if(product.childeProduct != ''){
                            kidsHtml = this.getCartItemKidsHTML(product.productID, response.productDataResults, product.amount);
                        } ;
                        if(product.parentProduct != '') continue;

                        productsHtml += `<div class="cart_item_box" data-product="${product.productID}">
                            <div class="parent_cart_item_box">
                                
                                <div class="item_tumbnail">
                                    <img src="${product.thumbnail}" alt="${product.title}">
                                </div>
                                <div class="item_info">
                                    <div class="item_titles">
                                        <a href="${product.permalink}" alt="${product.itemid}" class="item_itemid">מק"ט: ${product.itemid}</a>
                                        <a href="${product.permalink}" alt="${product.title}" class="item_title">${product.title}</a>
                                    </div>
                                    
                                    <div class="item_amount">
                                        <label for="item_amount__${product.productID}">כמות:</label>
                                        <input type="number" class="item_amount_input form-control form-control-sm" 
                                            id="item_amount__${product.productID}"
                                            data-product="${product.productID}" 
                                            data-parent="${product.parentProduct}" 
                                            data-child="${product.childeProduct}" 
                                            min="1" max="${product.stock_quantity}" 
                                            value="${product.amount}">
                                        <div class="loader" id="item_${product.productID}__loader"><div class="spinner-loader"></div></div>
                                    </div>
                                    <div class="item_prices">
                                        ${product.on_sell ? `
                                            <span class="discount_price">
                                                <span class="currency_symbol">₪</span>${product.discount_price}
                    
                                                <span class="old_price">
                                                    <span class="currency_symbol">₪</span>${product.base_price}
                                                </span>
                                            </span>
                                            ` : `
                                            <span class="base_price">
                                                <span class="currency_symbol">₪</span>${product.base_price}
                                            </span>
                                            `}   
                                    </div>
                                    <a class="remove_cart_item" 
                                        alt='הסר'
                                        data-product="${product.productID}"
                                        data-child="${product.childeProduct}"
                                        data-parent="${product.parentProduct}">הסר</a>
                                </div>
                            </div>
                            ${kidsHtml}
                        </div>`;
                    }

                    this.order_subSummary = subSummary;
                    this.order_totalDiscount = totalDiscount;
                    this.order_totalPrice = totalPrice;
                    
                    this.subSummeryVal.html(this.numberWithCommas(this.order_subSummary));
                    this.discountSummeryVal.html(this.numberWithCommas(this.order_totalDiscount));
                    this.totalSummeryVal.html(this.numberWithCommas(this.order_totalPrice));

                    this.cartItemsContainer.html(productsHtml);
                    
                    this.initRemoveItemEvent();
                    this.initAmountInputEvent();
                } else {
                    // SHOW ERROR
                    console.warn('error');
                }
            },
            error:  (jqXHR, status) => {
                console.warn(jqXHR);
                console.warn(status);
            }
        });
    }


    getCartItemKidsHTML(parentProduct, productsArr, maxAmount){
        let kidsHtml = '';
        for(let product of productsArr){
            if(product.parentProduct != parentProduct){
                continue;
            }
            kidsHtml += `<div class="kid_cart_item_box" data-product="${product.productID}" data-parent="${product.parentProduct}">
                <div class="item_tumbnail">
                    <img src="${product.thumbnail}" alt="${product.title}">
                </div>
                <div class="item_info">
                    <div class="item_titles">
                        <a href="${product.permalink}" alt="${product.itemid}" class="item_itemid">מק"ט: ${product.itemid}</a>
                        <a href="${product.permalink}" alt="${product.title}" class="item_title">${product.title}</a>
                    </div>
                    
                    <div class="item_amount">
                        <label for="item_amount__${product.productID}">כמות:</label>
                        <input type="number" class="item_amount_input form-control form-control-sm" 
                            id="item_amount__${product.productID}"
                            data-product="${product.productID}" 
                            data-parent="${product.parentProduct}" 
                            data-child="${product.childeProduct}" 
                            min="1" max="${maxAmount}" 
                            value="${product.amount}">
                        <div class="loader" id="item_${product.productID}__loader"><div class="spinner-loader"></div></div>
                    </div>
                    <div class="item_prices">
                        ${product.on_sell ? `
                            <span class="discount_price">
                                <span class="currency_symbol">₪</span>${product.discount_price}
    
                                <span class="old_price">
                                    <span class="currency_symbol">₪</span>${product.base_price}
                                </span>
                            </span>
                            ` : `
                            <span class="base_price">
                                <span class="currency_symbol">₪</span>${product.base_price}
                            </span>
                            `}   
                    </div>
                    <a class="remove_cart_item"  alt='הסר'
                        data-product="${product.productID}"
                        data-child="${product.childeProduct}"
                        data-parent="${product.parentProduct}">הסר</a>
                </div>
            </div>`;
        }
        return kidsHtml;
    }


    // Init inputs & triggers events
    initAddToCartEvent(){
        let addToCartCTA = $('.add_to_cart__cta');

        addToCartCTA.off('click').on('click', (e)=>{
            let productID = e.currentTarget.dataset.product;
            let amount = e.currentTarget.dataset.amount;
            let maxStock = e.currentTarget.dataset.stock;

            // check if warranty can be added
            let shop_warranty_product_id = somfyData.shop_warranty_product_id;
            let add_warranty = e.currentTarget.dataset.add_warranty;
            let warranty_applicable = e.currentTarget.dataset.warranty_applicable;

            // add to cart from product page
            // Get the amount from input
            if(!amount){
                amount = $('#stock_quantity_input[data-product="'+productID+'"]').val();
            }

            // defualt = 1
            if(!amount){
                amount = 1;
            }

            // check if calc item
            let isCalcItem = e.currentTarget.dataset.calcitem == 'true';
            if(isCalcItem){
                this.updateCalcItem(productID);
            };

            if((warranty_applicable == 'true' || warranty_applicable == '1') && add_warranty == 'true'){
                // add product and warranty to cart
                
                // show popup
                this.addToCartPopup.fadeIn('fast');

                this.addToCart(shop_warranty_product_id, amount, productID, "", null);
                this.addToCart(productID, amount, "", shop_warranty_product_id, maxStock);

                // update cart items popup
                this.updateCartPopup();
            } else if((warranty_applicable == 'true' || warranty_applicable == '1') && add_warranty == 'false'){
                // show add_warrany popup
                this.show_addWarrantyPopup(productID, amount, maxStock);
            } else {
                // just add the product

                // show popup
                this.addToCartPopup.fadeIn('fast');

                this.addToCart(productID, amount, "", "", maxStock);

                // update cart items popup
                this.updateCartPopup();
            }

            if(this.userIsLoggedIn) this.updateCartDB(this.cartItems);

        });
    }

    initRemoveItemEvent(){
        let removeCartItemTrigger = $('.remove_cart_item');

        removeCartItemTrigger.on('click', (e) => {
            let productID = e.currentTarget.dataset.product;
            let parentProduct = e.currentTarget.dataset.parent;
            let childeProduct = e.currentTarget.dataset.child;

            let itemLoader = $('#item_' + productID + '__loader');
            itemLoader.fadeIn('fast');

            this.removeCartItem(productID, parentProduct, childeProduct);

            this.removeFromCalcItems(productID);
            
            // update cart items popup
            this.updateCartPopup();
        });
    }

    initAmountInputEvent(){
        let itemsAmountInput = $('.item_amount_input');

        itemsAmountInput.on('focusin', (e)=>{
            this.oldAmountInputVal = $(e.currentTarget).val();
        });
        itemsAmountInput.on('focusout', (e)=>{
            this.oldAmountInputVal = null;
        });

        itemsAmountInput.on('change', (e) => {
            let productID = e.currentTarget.dataset.product;
            let parentProduct = e.currentTarget.dataset.parent;
            let childeProduct = e.currentTarget.dataset.child;
            let newAmount = $(e.currentTarget).val();
            let maxAmount = $(e.currentTarget).attr('max');

            if(parseInt(newAmount) > parseInt(maxAmount)){
                $(e.currentTarget).val(this.oldAmountInputVal);
                this.showSuccessAddToCart(maxAmount, productID);
            } else {
                let itemLoader = $('#item_' + productID + '__loader');
                itemLoader.fadeIn('fast');
                $(e.currentTarget).prop('disabled', true);
    
                this.updateCartItem(productID, newAmount, parentProduct, childeProduct, e.currentTarget);
            }
        });
    }

    // Add to cart
    addToCartFromWarranty(e){
        let productID = e.currentTarget.dataset.product;
        let amount = e.currentTarget.dataset.amount;
        let maxStock = e.currentTarget.dataset.stock;

        // check if warranty can be added
        let shop_warranty_product_id = somfyData.shop_warranty_product_id;
        let add_warranty = e.currentTarget.dataset.add_warranty;
        let warranty_applicable = e.currentTarget.dataset.warranty_applicable;

         
        // show popup
        this.add_warranty_popup.hide();
        this.addToCartPopup.fadeIn('fast');

        if(warranty_applicable && add_warranty == 'true'){
            // add product and warranty to cart
           

            this.addToCart(shop_warranty_product_id, amount, productID, "", null);
            this.addToCart(productID, amount, "", shop_warranty_product_id, maxStock);

            // update cart items popup
            this.updateCartPopup();
        } else {
            // just add the product
            this.addToCart(productID, amount, "", "", maxStock);

            // update cart items popup
            this.updateCartPopup();
        }
        
        if(this.userIsLoggedIn) this.updateCartDB(this.cartItems);

    }


    addToCart(productID, amount, parentProduct, childeProduct, maxStock){
        let cartItemData = {
            productID: productID,
            amount: parseInt(amount),
            parentProduct: parentProduct,
            childeProduct : childeProduct
        };
        // get current cart items from localStorage
        let cartItems = localStorage.getItem('eu_cartItems');
        cartItems = JSON.parse(cartItems);

        if(!cartItems){
            // new cart - just add the items
            cartItems = [cartItemData];
        } else if(cartItems && cartItems.length > 0){
            // not new cart - check if product allready exists
            let itemFound = false;
            let maxAmountReached = false;
            let maxAmountProductID = '';

            cartItems.map(item => {
                if(item.productID == cartItemData.productID 
                    && item.parentProduct == cartItemData.parentProduct){

                    let newAmount = parseInt(item.amount) + parseInt(cartItemData.amount);
                    
                    // disrigard warrany
                    if(newAmount <= maxStock || productID == somfyData.shop_warranty_product_id){
                        item.amount = newAmount;
                        if(childeProduct != ''){
                            item.childeProduct = childeProduct;
                        }
                        itemFound = true;
                        return item;
                    } else {
                        maxAmountReached = true;
                        maxAmountProductID = item.productID;
                    }
                }
            });

            if(maxAmountReached){
                // show popup for axstra stock notification
                this.showSuccessAddToCart(maxStock, maxAmountProductID);
                return;
            } else {
                if(!itemFound){
                    cartItems.push(cartItemData);
                }
            }
        } else {
            // empty cart
            cartItems = [cartItemData];
        }

        localStorage.setItem('eu_cartItems', JSON.stringify(cartItems));
        this.cartItems = cartItems;

        // show added successfully message
        this.showSuccessAddToCart();
    }


    updateCartItem(productID, newAmount, parentProduct, childeProduct, currentTarget){
        let cartItemData = {
            productID: productID,
            amount: parseInt(newAmount),
            parentProduct : parentProduct,
            childeProduct : childeProduct
        };
        
        // get current cart items from localStorage
        let cartItems = localStorage.getItem('eu_cartItems');
        cartItems = JSON.parse(cartItems);
        
        if(!cartItems){
            // new cart - just add the items
            cartItems = [cartItemData];
        } else if(cartItems && cartItems.length > 0){
            // not new cart - check if product allready exists
            let itemFound = false;

            cartItems.map(item => {
                if(item.productID == cartItemData.productID 
                    && item.parentProduct == cartItemData.parentProduct){
                        item.amount = parseInt(cartItemData.amount);
                        itemFound = true;

                        return item;
                }
            });

            if(!itemFound){
                cartItems.push(cartItemData);
            }

        } else {
            // empty cart
            cartItems = [cartItemData];
        }

        localStorage.setItem('eu_cartItems', JSON.stringify(cartItems));
        this.cartItems = cartItems;

        if(this.userIsLoggedIn) this.updateCartDB(cartItems);

        // show added successfully message
        let itemLoader = $('#item_' + productID + '__loader');
        itemLoader.fadeOut('fast');
        $(currentTarget).prop('disabled', false);
        
        // update cart items popup
        this.updateCartPopup();
    }

    removeCartItem(productID, parentProduct, childeProduct){
        
        // get current cart items from localStorage
        let cartItems = localStorage.getItem('eu_cartItems');
        cartItems = JSON.parse(cartItems);
        
        if(cartItems && cartItems.length > 0){
            // not new cart - check if product  exists
            let newCartItems = [];

            for(let item of cartItems){
                if(item.productID == productID 
                    && item.parentProduct == parentProduct) continue;
                newCartItems.push(item);
            }
            cartItems = newCartItems;
        }

       
        localStorage.setItem('eu_cartItems', JSON.stringify(cartItems));
        this.cartItems = cartItems;

        if(this.userIsLoggedIn) this.updateCartDB(cartItems);

        // show added successfully message
        let itemLoader = $('#item_' + productID + '__loader');

         // remove warranty if attached
        if(childeProduct != '') this.removeCartItem(childeProduct, productID, "");

    }

    // DB Update
    updateCartDB(cartItems){
        $.ajax({
            url : somfyData.ajax_url,
            type : 'POST',
            data : {
                action : 'somfy_eu_update_cart',
                cartItems : cartItems.length == 0 ? '' : cartItems,
                clear : cartItems.length == 0 ? true : false,
                nonce: somfyData.ajax_nonce
            },
            dataType: "json",
            success: (response) =>{
                if(!response.status){
                    // SHOW ERROR
                    console.warn(response);
                } else {
                    let checkout_registration = localStorage.getItem('eu_checkout_registration');

                    if(checkout_registration == 'true'){
                        localStorage.removeItem('eu_checkout_registration');
                    }
                }

            },
            error:  (jqXHR, status) => {
                console.warn(jqXHR);
                console.warn(status);
            }
          });
    }


    clearCart(cleareFromCheckout){
        localStorage.removeItem('eu_cartItems');
        this.cartItems = null;
        if(this.userIsLoggedIn) this.updateCartDB('');
        this.updateCartPopup(cleareFromCheckout);
    }

    // Popups

    showSuccessAddToCart(maxAmountReached = null, productID = null) {
        this.addToCartPopupLoader.hide();
        if(maxAmountReached && productID){
            this.addToCartPopupSuccess.hide();
            this.maxAmountReachedMsg.show();
            this.maxAmountReachedMsgQty.html(maxAmountReached);
            this.stockNotifictaion_maxamount__trigger.attr('data-product', productID);
            this.addToCartPopup.fadeIn('fast');
        } else {
            this.maxAmountReachedMsg.hide();
            this.maxAmountReachedMsgQty.html('');
            this.stockNotifictaion_maxamount__trigger.attr('data-product', '');

            this.addToCartPopupSuccess.show();
            let closeAddToCartPopupTimer = setTimeout(this.closeAddToCartPopup.bind(this), 2000); 
        }

    }

    closeAddToCartPopup(){
        this.addToCartPopupClose.trigger('click');
    }

    show_addWarrantyPopup(productID, amount, maxStock){
        this.add_warranty__quantity_input
            .attr('max', amount)
            .val(amount);

        this.add_warranty__submit
            .attr({
                'data-product' : productID,
                'data-stock' : maxStock,
                'data-amount' : amount
            });

        this.add_warranty_popup.fadeIn('fast');
    }

    numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }


    // clac items
    updateCalcItem(productID){
        let curCalcItems = JSON.parse(localStorage.getItem('eu_cart_calcItems'));
        let calc_userInput = JSON.parse(localStorage.getItem('eu_calc_userInputs'));

        if(!curCalcItems || curCalcItems.length == 0){
            curCalcItems = [{'productID':productID, 'calc_userInput': calc_userInput}];
        } else {
            let itemFound = false;

            for(let item of curCalcItems){
                if(item.productID == productID) {
                    item.calc_userInput = calc_userInput;
                    itemFound = true;
                }
            }
            if(!itemFound) curCalcItems.push({'productID':productID, 'calc_userInput': calc_userInput});
        }
        localStorage.setItem('eu_cart_calcItems', JSON.stringify(curCalcItems));
    }
    
    removeFromCalcItems(productID){
        let curCalcItems = JSON.parse(localStorage.getItem('eu_cart_calcItems'));

        if(curCalcItems && curCalcItems.length > 0){
            let newCalcItems = curCalcItems.filter((item) =>{
                return item.productID != productID;
            });
            localStorage.setItem('eu_cart_calcItems', JSON.stringify(newCalcItems));
        }
    }
}


export default Cart;
