import $ from 'jquery';

class CheckoutPage {

    constructor(cartObj) {
        this.cartObj = cartObj;

        // checkout_tabs
        this.checkout_delivery_tab = $('#checkout_delivery_tab');
        this.checkout_payment_tab = $('#checkout_payment_tab');
        this.tab_trig = $('.tab_trig');

        // Forms
        this.deliveryDetails_form = $('#delivery_details_form');
        this.otherDeliveryDetails_form = $('#other_delivery_details_form');

        // delivery options toggle
        this.delivery_option_trigger = $('input[name="delivery_option"]');
        this.delivery_subtab__delivery = $('#delivery_subtab__delivery');
        this.delivery_subtab__pickup = $('#delivery_subtab__pickup');
        
        // other delivery address form
        this.otherDeliveryAddress__toggle = $('#other_delivery_address__toggle');

        // not registered user
        this.registerUserFromCheckoutBtn = $('.register_user_from_checkout');
        
        // go_to_payment
        this.goToPaymentBtn = $('#go_to_payment');
        this.go_to_deliveryBtn = $('#go_to_delivery_tab');


        // Paymeny options
        this.payment_option_trigger = $('input[name="payment_option"]');


        // Submit order
        this.submitOrderBtn = $('#submit_order');
        this.order_submition_loader = $('.order_submition_loader_container__overlay');
        this.order_submition_loader__container__close = $('#order_submition_loader__container__close');

        // OrderObj
        this.newOrder = {
            customer_email : null,
            customer_firstname : null,
            customer_lastname : null,
            orderItems: null,
            order_subSummary: 0,
            order_totalDiscount: 0,
            order_totalPrice: 0,
            paymentmethod: 'credit',
            billaddressee : null,
            billaddress1 : null,
            billcity : null,
            billcountry : null,
            shipaddressee : null,
            shipaddress1 : null,
            shipcity : null,
            shipcountry : null,
            shipmethod : 'delivery',
            useOtherDeliveryAddress : false,
            user_usage_agreement : false,
            user_updates_agreement : false
        };

        this.deliveryDetails_valid = false;
        this.orderWithShipping = false;

        // new local order data
        this.userID = null;
        this.entity = null;
        this.orderCreated = null;
        this.order_args = null;

        // Pelecard data
        this.iframeRedirectCounter = 0;
        this.peleConfirmationData = null;
        this.pele_transactionId = null;
        this.newOrderTranid = null;

        this.calcItems;

        this.initEvents();
    }


    initEvents() {
        // delivery options toggle
        this.delivery_option_trigger.on('change' , (e)=>{
            let target = $(e.currentTarget).val();
            
            if(target == 'delivery'){
                this.delivery_subtab__pickup.hide();
                this.delivery_subtab__delivery.show();
                this.newOrder.shipmethod = 'delivery';
            } else if(target == 'pickup'){
                this.delivery_subtab__delivery.hide();
                this.delivery_subtab__pickup.show();
                this.newOrder.shipmethod = 'pickup';
            }
        })

        // tab_trigs
        this.tab_trig.on('click', (e)=>{
            let target = e.currentTarget.dataset.target;

            if(this.deliveryDetails_valid && target == 'checkout_payment_tab'){
                this.deliveryDetails_form.trigger('submit');
            } else if(!this.deliveryDetails_valid && target == 'checkout_payment_tab'){
                this.deliveryDetails_form.trigger('submit');
            } else if(target == 'checkout_delivery_tab'){
                this.goToDeliveryTab();
            }
        })

        // not registered user
        this.registerUserFromCheckoutBtn.on('click', ()=>{
            localStorage.setItem('checkout_registration', true);
            localStorage.setItem('checkout_registration_redirect', true);
            window.location.href = somfyData.root_url + "/register";
        });


        // other delivery address form
        this.otherDeliveryAddress__toggle.on('change', (e)=>{
            if($(e.currentTarget)[0].checked){
                this.otherDeliveryDetails_form.addClass('active');
                this.newOrder.useOtherDeliveryAddress = true;
            } else {
                this.otherDeliveryDetails_form.removeClass('active');
                this.newOrder.useOtherDeliveryAddress = false;
            }
        });


        // go_to_payment
        this.goToPaymentBtn.on('click', ()=>{
            this.deliveryDetails_form.trigger('submit');
        });

        // go_to_delivery
        this.go_to_deliveryBtn.on('click', ()=>{
            this.goToDeliveryTab();
        });

        // form submit
        this.deliveryDetails_form.on('submit', (e)=>{
            e.preventDefault();
            this.validate_deliveryDetails();
        })


        // payment options toggle
        this.payment_option_trigger.on('change' , (e)=>{
            let paymenyOption = $(e.currentTarget).val();
            this.newOrder.paymentmethod = paymenyOption;

            if(paymenyOption == 'paypal'){
                this.initPayPalButton();
            } else if(paymenyOption == 'credit') {
                $('#paypal-button-container').html('');
                this.initCreditIframe();
            }
        })


        // Submit order
        this.submitOrderBtn.on('click', ()=>{
            this.showCardIframePopup();
        });

        this.order_submition_loader__container__close.on('click', ()=>{
            this.order_submition_loader.hide();
        });
    }

    initPayPalButton(){
        let ppItemsDate = this.get_ppItemsDate();
        this.submitOrderBtn.hide();
        
        // set card vets to null
        this.iframeRedirectCounter = 0;
        this.peleConfirmationData = null;
        this.pele_transactionId = null;

        paypal.Buttons({
            style: {
                layout: 'horizontal'
            },
            commit: true,
            onClick: () => {
                // Show loader
                this.showOrderLoader();
            },
            createOrder: (data, actions) => {
                return actions.order.create({
                    purchase_units: [{
                        amount: {
                            "currency_code":'ILS',
                            "value": parseInt(this.newOrder.order_totalPrice),
                            "breakdown" : {
                                "item_total": {
                                    "currency_code":'ILS',
                                    "value": parseInt(this.newOrder.order_totalPrice)
                                }
                            }
                        },
                        items: ppItemsDate
                    }]
                });
            },
            onApprove: (data, actions) => {
                return actions.order.capture().then(
                    (details) => {
                        // send deposit info to ERP
                        console.log('Transaction completed by ' + details.payer.name.given_name + '!');

                        this.complateOrderPayment(details, 'paypal');
                    }
                );
            },
            onCancel:  (data, actions) => {
                // Show a cancel page or return to cart
                this.showOrderLoaderError('תהליך תשלום בוטל');
            },
            onError: (err) => {
                this.showOrderLoaderError(err);
            }
        }).render('#paypal-button-container');
    }


    initCreditIframe(firstLoad){
        // set card vets to null
        this.iframeRedirectCounter = 0;
        this.peleConfirmationData = null;
        this.pele_transactionId = null;

        $.ajax({
            url : somfyData.ajax_url,
            type : 'POST',
            data : {
                action : 'somfy_eu_get_iframe_url',
                orderid: this.orderCreated.newOrderID,
                orderTotal: this.orderCreated.orderTotals.total,
                nonce: somfyData.ajax_nonce
            },
            dataType: "json",
            success: (response) =>{
                if(response.status){
                    this.submitOrderBtn.show();

                    // set iframe url
                    this.peleConfirmationData = {
                        confirmationKey : response.confirmationKey,
                        userKey : response.userKey,
                        totalX100 : response.totalX100
                    }

                    this.setPele_transactionId(response.iframeUrl)
                    $('#pele_iframe').attr({'src': response.iframeUrl, 'data-origin': response.iframeUrl});
                    this.iframeRedirectCounter = 0;

                    if(firstLoad){
                        this.showPaymentTab();
                        this.order_submition_loader.fadeOut('fast');
                    }
                } else {
                    // SHOW ERROR message
                    this.peleConfirmationData = null;
                    this.showOrderLoaderError(response.message);
                }
            },
            error:  (jqXHR, status) => {
                console.warn(jqXHR);
                console.warn(status);
            }
        });

    }

    // Submit order
    submitOrder(){
        // Show loader
        this.showOrderLoader();
        
        this.userID = null;
        this.entity = null;
        this.orderCreated = null;
        this.order_args = null;

         // get calculator data if exists
        this.calcItems = JSON.parse(localStorage.getItem('eu_cart_calcItems'));

        // ajax call - add new order
        $.ajax({
            url : somfyData.ajax_url,
            type : 'POST',
            data : {
                action : 'somfy_eu_new_order',
                newOrder: this.newOrder,
                calcItems: this.calcItems,
                nonce: somfyData.ajax_nonce
            },
            dataType: "json",
            success: (response) =>{

                if(response.status){
                    $('#order_submition_loader').hide();
                
                    this.userID = response.userID;
                    this.entity = response.entity;
                    this.orderCreated = response.orderCreated;
                    this.order_args = response.order_args;

                    this.initCreditIframe(true);
                    
                } else {
                    // SHOW ERROR message
                    this.showOrderLoaderError(response.message);

                    this.userID = null;
                    this.entity = null;
                    this.orderCreated = null;
                    this.order_args = null;
                }
            },
            error:  (jqXHR, status) => {
                console.warn(jqXHR);
                console.warn(status);
            }
        });
    }

    complateOrderPayment(payment_details = null, payment_method){
        $('#order_submition__payment_recived').show();
        // ajax call - add new order
        $.ajax({
            url : somfyData.ajax_url,
            type : 'POST',
            data : {
                action : 'somfy_eu_new_order_payment',
                payment_details: payment_details,
                pele_transactionId : this.pele_transactionId,
                payment_method: payment_method,
                userID: this.userID,
                entity: this.entity,
                orderCreated: this.orderCreated,
                order_args: this.order_args,
                calcItems: this.calcItems,
                nonce: somfyData.ajax_nonce
            },
            dataType: "json",
            success: (response) =>{
                if(response.status){
                    // show success message
                    $('#order_submition_loader').hide();
                    this.newOrderTranid = response.newOrderTranid;
                    // cleare cart
                    this.cartObj.clearCart(true);
                    localStorage.removeItem('eu_calc_finalFilters');
                    localStorage.removeItem('eu_cart_calcItems');
                    localStorage.removeItem('eu_calc_userInputs');

                    // redirect to thanks page after few seconds
                    let redirectToThanksTimer = setTimeout(this.redirectToThanks.bind(this), 2000); 
                } else {
                    // SHOW ERROR message
                    this.showOrderLoaderError(response.message);
                }
            },
            error:  (jqXHR, status) => {
                console.warn(jqXHR);
                console.warn(status);
            }
        });
    }

    validateCardPayment(){
        $.ajax({
            url : somfyData.ajax_url,
            type : 'POST',
            data : {
                action : 'somfy_eu_validate_card_payment',
                transactionId : this.pele_transactionId,
                nonce: somfyData.ajax_nonce
            },
            dataType: "json",
            success: (response) =>{
                if(response.status){
                    this.complateOrderPayment(null, 'credit');
                    console.log('Transaction completed by card!');
                    
                } else {
                    // SHOW ERROR message
                    this.showOrderLoaderError(response.message);
                }
            },
            error:  (jqXHR, status) => {
                console.warn(jqXHR);
                console.warn(status);
            }
        });

    }


    get_ppItemsDate(){
        let ppItems = [];

        let items = this.cartObj.productDataResults;
        for (let item of items) {
            let finalPrice = item.on_sell ? item.discount_price : item.base_price;

            ppItems.push({
                "name":`${item.title}`,
                "quantity": parseInt(item.amount),
                "sku":`${item.productID}`,
                "unit_amount": {
                    "currency_code":'ILS',
                    "value": parseInt(finalPrice)
                }
            });
        }

        if(this.orderWithShipping){
            let shipping_price = parseInt($('#shipping_price').val());
            let shippingproductID = $('#shipping_product_id').val();
            ppItems.push({
                "name":`משלוח`,
                "sku":`${shippingproductID}`,
                "quantity": 1,
                "unit_amount": {
                    "currency_code":'ILS',
                    "value": shipping_price
                }
            });
        }
        return ppItems;
    }


    redirectToThanks(){
        let tranid = this.newOrderTranid.substring(3);
        window.location.replace(somfyData.root_url + '/order-confirmation?tranid=' + tranid);
    }

    // Delivery Details
    validate_deliveryDetails(){
        let isValid = true;
        // check delivery option
        if(this.newOrder.shipmethod == 'delivery'){
            // validate deliveryDetails_form
            this.newOrder.shipmethod = 'delivery';
            let formFields = [
                {id : 'delivery_details__email', validator : 'email'},
                {id : 'delivery_details__firstname', validator : 'text', min : 2},
                {id : 'delivery_details__lastname', validator : 'text', min : 2},
                {id : 'delivery_details__billaddressee', validator : 'text', min : 2},
                {id : 'delivery_details__billaddress1', validator : 'text', min : 2},
                {id : 'delivery_details__billcity', validator : 'text', min : 2},
                {id : 'delivery_details__billcountry', validator : 'text', min : 2},
            ];
            let isValid_deliveryDetails_form = this.validate_deliveryDetails_form(formFields);

            if(isValid_deliveryDetails_form){
                // set newOrder data
                this.newOrder.customer_email = $('#delivery_details__email').val();
                this.newOrder.customer_firstname = $('#delivery_details__firstname').val();
                this.newOrder.customer_lastname = $('#delivery_details__lastname').val();
                this.newOrder.billaddressee = $('#delivery_details__billaddressee').val() + ', ' + $('#delivery_details__firstname').val() + ' ' + $('#delivery_details__lastname').val();
                this.newOrder.billaddress1 = $('#delivery_details__billaddress1').val();
                this.newOrder.billcity = $('#delivery_details__billcity').val();
                this.newOrder.billcountry = $('#delivery_details__billcountry').val();
            } else {
                isValid = false;
            }

            // check for other delivery address
            if(this.newOrder.useOtherDeliveryAddress){
                // validate otherDeliveryDetails_form
                let otherFormFields = [
                    {id : 'other_delivery_details__firstname', validator : 'text', min : 2},
                    {id : 'other_delivery_details__lastname', validator : 'text', min : 2},
                    {id : 'other_delivery_details__shipaddressee', validator : 'text', min : 2},
                    {id : 'other_delivery_details__shipaddress1', validator : 'text', min : 2},
                    {id : 'other_delivery_details__shipcity', validator : 'text', min : 2},
                    {id : 'other_delivery_details__shipcountry', validator : 'text', min : 2},
                ];

                let isValid_otherDeliveryDetails_form = this.validate_deliveryDetails_form(otherFormFields);

                if(isValid_otherDeliveryDetails_form){
                    // set newOrder data
                    this.newOrder.shipaddressee = $('#delivery_details__shipaddressee').val() + ',' + $('#other_delivery_details__firstname').val() + ' ' + $('#other_delivery_details__lastname').val();
                    this.newOrder.shipaddress1 = $('#other_delivery_details__shipaddress1').val();
                    this.newOrder.shipcity = $('#other_delivery_details__shipcity').val();
                    this.newOrder.shipcountry = $('#other_delivery_details__shipcountry').val();
                } else {
                    isValid = false;
                }
            } else {
                // set shippingAddress same as billingAddress
                this.newOrder.shipaddressee = this.newOrder.billaddressee;
                this.newOrder.shipaddress1 = this.newOrder.billaddress1;
                this.newOrder.shipcity = this.newOrder.billcity;
                this.newOrder.shipcountry = this.newOrder.billcountry;
            }
        } else {

            this.newOrder.shipmethod = 'pickup';

            let formFields = [
                {id : 'delivery_details__email', validator : 'email'},
                {id : 'delivery_details__firstname', validator : 'text', min : 2},
                {id : 'delivery_details__lastname', validator : 'text', min : 2},
                {id : 'delivery_details__billaddressee', validator : 'text', min : 2},
                {id : 'delivery_details__billaddress1', validator : 'text', min : 2},
                {id : 'delivery_details__billcity', validator : 'text', min : 2},
                {id : 'delivery_details__billcountry', validator : 'text', min : 2},
            ];
            let isValid_deliveryDetails_form = this.validate_deliveryDetails_form(formFields);

            if(isValid_deliveryDetails_form){
                // set newOrder data
                this.newOrder.customer_email = $('#delivery_details__email').val();
                this.newOrder.customer_firstname = $('#delivery_details__firstname').val();
                this.newOrder.customer_lastname = $('#delivery_details__lastname').val();
                this.newOrder.billaddressee = $('#delivery_details__billaddressee').val() + ', ' + $('#delivery_details__firstname').val() + ' ' + $('#delivery_details__lastname').val();
                this.newOrder.billaddress1 = $('#delivery_details__billaddress1').val();
                this.newOrder.billcity = $('#delivery_details__billcity').val();
                this.newOrder.billcountry = $('#delivery_details__billcountry').val();
            } else {
                isValid = false;
            }
            
            // clear addresses
            this.newOrder.shipaddressee = null;
            this.newOrder.shipaddress1 = null;
            this.newOrder.shipcity = null;
            this.newOrder.shipcountry = null;
        }

        // validate user agreements
        let user_updates_agreement = $('#user_updates_agreement');
        let user_usage_agreement = $('#user_usage_agreement');

        if(user_updates_agreement[0].checked){
            this.newOrder.user_updates_agreement = true;
        }

        if(user_usage_agreement[0].checked){
            this.newOrder.user_usage_agreement = true;
            $('.form_error[data-input="user_usage_agreement"]').fadeOut('fast');
        } else {
            // show error message
            $('.form_error[data-input="user_usage_agreement"]').fadeIn('fast');
            isValid = false;
        }

        if(isValid){
            this.goToPaymentTab();
            this.deliveryDetails_valid = true;
        } else {
            this.deliveryDetails_valid = false;
        }

    }

    goToDeliveryTab(){
        // go to payment tab
        this.cartObj.totalSummeryVal.html(this.cartObj.numberWithCommas(this.cartObj.order_totalPrice));

        this.deliveryDetails_form.show();
        this.checkout_payment_tab.hide();

        $('.tab_trig[data-target="checkout_payment_tab"]').removeClass('active');
        this.checkout_delivery_tab.show();
        $('.tab_trig[data-target="checkout_delivery_tab"]').addClass('active');
        
        $('.cart_preview__container .cart_items__container').show();
        this.newOrder.orderItems = null;
    }

    goToPaymentTab(){
        
        // set final cart items and prices
        //  add shiping pricing - to summary and to newOrder object
        // if shipping method is delivery
        this.newOrder.order_subSummary =  parseInt(this.cartObj.order_subSummary);
        this.newOrder.order_totalDiscount = parseInt(this.cartObj.order_totalDiscount);
        this.newOrder.order_totalPrice = parseInt(this.cartObj.order_totalPrice);
        this.newOrder.orderItems = JSON.parse(localStorage.getItem('eu_cartItems'));

        let free_shipping_threshold = $('#free_shipping_threshold').val();
        let shipping_price = parseInt($('#shipping_price').val());
        let shippingproductID = $('#shipping_product_id').val();

        if((this.newOrder.shipmethod == 'delivery' && this.newOrder.order_totalPrice >= free_shipping_threshold)){
            // dont add shipping to orderItems
            // dont add shipping price to totalPrice
            // hide shipping price
            $('.delivery_summery_val').html('0');
            this.orderWithShipping = false;

        } else if(this.newOrder.shipmethod == 'delivery' && this.newOrder.order_totalPrice < free_shipping_threshold){
            // add shipping to orderItems
            let shipItemData = {
                productID: shippingproductID,
                amount: 1,
                parentProduct: '',
                childeProduct : ''
            };

            this.orderWithShipping = true;

            this.newOrder.orderItems.push(shipItemData);

            // add shipping price to totalPrice
            this.newOrder.order_totalPrice += shipping_price;
            this.newOrder.order_subSummary += shipping_price;
            this.cartObj.totalSummeryVal.html(this.cartObj.numberWithCommas(this.newOrder.order_totalPrice));

            // show shipping price
            $('.delivery_summery_val').html(shipping_price);
        } else {
            $('.delivery_summery_val').html('0');
        }

        this.submitOrder();
    }

    showPaymentTab(){
        // go to payment tab
        this.deliveryDetails_form.hide();
        this.checkout_delivery_tab.hide();
        $('.tab_trig[data-target="checkout_delivery_tab"]').removeClass('active');
        this.checkout_payment_tab.show();
        $('.tab_trig[data-target="checkout_payment_tab"]').addClass('active');

        $('.cart_preview__container .cart_items__container').hide();

        // scroll to top
        $('html, body').animate({
            scrollTop: $(".checkout_tabs__triggers").offset().top - 400
        }, 250);
    }

    // Validation
    validate_deliveryDetails_form(formFields){
        let errors = 0;
        
        for (let field of formFields) {
            let fieldVal = $('#' + field.id).val();

            if(field.validator == 'email'){
                if(!this.validateEmail(fieldVal)){
                    $('.form_error[data-input="' + field.id + '"]').fadeIn('fast');
                    errors++;
                } else {
                    $('.form_error[data-input="' + field.id + '"]').fadeOut('fast');
                }
            } else if(field.validator == 'text'){
                if(!this.validateText(fieldVal, field.min)){
                    $('.form_error[data-input="' + field.id + '"]').fadeIn('fast');
                    errors++;
                } else {
                    $('.form_error[data-input="' + field.id + '"]').fadeOut('fast');
                }
            }

        }
        
        return errors > 0 ? false : true;
    }

    validateText(val, min){
        return val.length >= min && val != '' ? true : false;
    }

    validateEmail(sEmail) {
		let filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
		return filter.test(sEmail) ? true : false;
	}


    showOrderLoader(){
        // Show loader
        $('#order_submition_loader').show();
        $('#order_submition__success').hide();
        $('#order_submition__error').hide();
        $('.order_submition_loader__actions').hide();
        this.order_submition_loader.fadeIn('fast');
    }

    showOrderLoaderError(message){
        $('#order_submition_loader').hide();
        $('.order_submition_loader__actions').fadeIn('fast');
        $('#order_submition__error').html(message).fadeIn('fast');
    }

    showCardIframePopup(){
        
        this.iframeRedirectCounter = -1;
        $('#card_iframe_popup').fadeIn('fast');
        
        
        $('#pele_iframe').off('load').attr('src', $('#pele_iframe').attr('data-origin')).on('load',()=>{
            this.iframeRedirectCounter += 1;

            if(this.iframeRedirectCounter == 1){
                // show loader
                $('#card_iframe_popup').hide();
                this.iframeRedirectCounter = 0;
                this.showOrderLoader();

                // validate payment
                this.validateCardPayment();
            }
        });
        $('.card_iframe_popup__close').off('click').on('click', ()=>{
            $('#card_iframe_popup').fadeOut('fast');
            $('#pele_iframe').attr('src', '');
            this.iframeRedirectCounter = 0;
        })
    }

    setPele_transactionId(url){
        let ps = "transactionId=";
        let si = url.indexOf(ps) + ps.length;
        let ei = url.indexOf('&', si);
        let transactionId = ei != -1 ? url.substring(si,ei) : url.substring(si);

        this.pele_transactionId = transactionId;
    }
}

export default CheckoutPage;
