import $ from 'jquery';

class ProductPage {

    constructor() {
        // Tabs
        this.tabsTrigger = $('.trigger-link');
        this.releatedProducts__slider = $(".releated_products__slider");
        this.productImages__slider = $(".products_images__slider");

        this.initEvents();
    }


    initEvents() {
        this.tabsTrigger.on('click', (e)=>{
            let currentTriggert = $(e.currentTarget);
            let target = e.currentTarget.dataset.toggle;

            if(!$('#'+target).hasClass('active')){
                $('.tab-content__container.active').removeClass('active');
                $('#'+target).addClass('active');

                $('.trigger-link.active').removeClass('active');
                currentTriggert.addClass('active');

            }
        })

        this.initReleatedProductsSlider();
        this.initProductImagesSlider();
    }

    initReleatedProductsSlider(){
        let args = {
            autoplay: false,
            arrows: true,
            dots: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            infinite: false,
            swipeToSlide: true,
            prevArrow:"<a class='slick-prev pull-left' alt='prev'><i class='fas fa-chevron-left' aria-hidden='true'></i></a>",
            nextArrow:"<a class='slick-next pull-right' alt='next'><i class='fas fa-chevron-right' aria-hidden='true'></i></a>",
            responsive: [
                {
                  breakpoint: 1920,
                  settings: {
                    slidesToShow: 4,
                  }
                },
                {
                    breakpoint: 1600,
                    settings: {
                      slidesToShow: 2,
                    }
                },
                {
                  breakpoint: 800,
                  settings: {
                    slidesToShow: 1,
                  }
                }
            ]
        };
        this.releatedProducts__slider.slick(args);
    }

    initProductImagesSlider(){
      let args = {
        dots: true,
        arrows: false
      };
    this.productImages__slider.slick(args);
    }
}

export default ProductPage;
