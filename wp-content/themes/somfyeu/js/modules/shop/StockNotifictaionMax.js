import $ from 'jquery';

class StockNotifictaionMax {
    
    constructor() {
        this.stockNotifictaion_maxamount__popup = $('#stock_notifictaion_maxamount_popup');
        this.popup__overlay__close = $('#stock_notifictaion_maxamount_popup .popup__overlay__close');

        this.productID;
        // form elements
        
        this.stockNotifictaionSignup_form = $('#stock_notifictaion_maxamount_signup');
        this.stockNotifictaion_email = $('#stock_notifictaion_maxamount_email');
        this.stockNotifictaion_fullname = $('#stock_notifictaion_maxamount_fullname');
        this.stockNotifictaion_phone = $('#stock_notifictaion_maxamount_phone');
        this.stockNotifictaion_required_amount = $('#stock_notifictaion_maxamount_required_amount');

        this.stockNotifictaion_submit = $('#stock_notifictaion_maxamount_submit');

        this.stockNotifictaion_form_loader = $('#stock_notifictaion_maxamount__form_loader');
        this.stockNotifictaion_submit_msg = $('#stock_notifictaion_maxamount__submit_msg');

        this.successCloseTimer;
        this.initEvents();   
    }

    initEvents(){
        // form submit
        this.stockNotifictaionSignup_form.on('submit', (e) => {
            e.preventDefault();
            this.stockNotifictaion_submit.prop('disabled', true).hide();
            this.stockNotifictaion_form_loader.show();

            this.sendForm();
        });

        // close overlay
        this.popup__overlay__close.on('click', this.closePopup.bind(this));
    }

    openPopup(productID){
        this.productID = productID;
            
        // show popup
        this.stockNotifictaion_maxamount__popup.fadeIn('fast');
    }

    sendForm(){
        let userEmail = this.stockNotifictaion_email.val();
        let userFullname = this.stockNotifictaion_fullname.val();
        let userPhone = this.stockNotifictaion_phone.val();
        let requiredAmount = this.stockNotifictaion_required_amount.val();

        $.ajax({
            url : somfyData.ajax_url,
            type : 'POST',
            data : {
                action : 'somfy_eu_stock_notifictaion_signup',
                userEmail : userEmail,
                userFullname : userFullname,
                userPhone : userPhone,
                productID: this.productID,
                requiredAmount: requiredAmount,
                nonce: somfyData.ajax_nonce
            },
            dataType: "json",
            success: (response) =>{

                if(response.status){
                    // hide form
                    this.stockNotifictaion_email.val('');
                    this.stockNotifictaion_fullname.val('');
                    this.stockNotifictaion_phone.val('');
                    this.stockNotifictaion_required_amount.val('');

                    this.stockNotifictaionSignup_form.hide();

                    // show success message
                    this.stockNotifictaion_submit_msg
                        .removeClass('error_msg')
                        .addClass('success_msg')
                        .html(response.message).fadeIn();
                    
                    // automaticly close popup
                    clearTimeout(this.successCloseTimer);
                    this.successCloseTimer = setTimeout(this.closePopup.bind(this), 2000); 

                } else {
                    // show error message
                    this.stockNotifictaion_submit_msg
                        .removeClass('success_msg')
                        .addClass('error_msg')
                        .html(response.message).fadeIn();
                    
                    // show the form
                    this.stockNotifictaion_email.fadeIn();
                    this.stockNotifictaion_phone.fadeIn();
                    this.stockNotifictaion_fullname.fadeIn();
                    this.stockNotifictaion_submit.prop('disabled', false).show();
                }
                this.stockNotifictaion_form_loader.hide();
            },
            error:  (jqXHR, status) => {
                console.warn(jqXHR);
                console.warn(status);
            }
          });
    }

    closePopup(){
        this.stockNotifictaion_maxamount__popup.fadeOut('fast');

        this.stockNotifictaion_email.val();
        this.stockNotifictaion_fullname.val();
        this.stockNotifictaion_phone.val();
        this.stockNotifictaion_required_amount.val();
        
        this.stockNotifictaion_submit.prop('disabled', false).show();

        this.stockNotifictaionSignup_form.show();
        this.stockNotifictaion_form_loader.hide();
        this.stockNotifictaion_submit_msg.removeClass('error_msg').removeClass('success_msg').html('').hide();
    }
}

export default StockNotifictaionMax;
