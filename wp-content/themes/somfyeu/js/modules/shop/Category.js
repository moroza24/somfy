
import $ from 'jquery';
class Category {

    constructor() {
        this.relatedProductsSlider = $(".releated_products__slider");

        if(this.relatedProductsSlider.length > 0) this.initSelectedProductsSlider();
    }

    // init Categories slider
    initSelectedProductsSlider(){
        let args = {
            autoplay: false,
            arrows: true,
            dots: true,
            infinite: false,
            slidesToShow: 4,
            swipeToSlide: true,
            prevArrow:"<a class='slick-prev pull-left' alt='prev'><i class='fas fa-chevron-left' aria-hidden='true'></i></a>",
            nextArrow:"<a class='slick-next pull-right' alt='next'><i class='fas fa-chevron-right' aria-hidden='true'></i></a>",
            responsive: [
                {
                  breakpoint: 1920,
                  settings: {
                    slidesToShow: 4,
                  }
                },
                {
                  breakpoint: 1366,
                  settings: {
                    slidesToShow: 3,
                  }
                },
                {
                  breakpoint: 950,
                  settings: {
                    slidesToShow: 2,
                  }
                },
                {
                  breakpoint: 630,
                  settings: {
                    slidesToShow: 1,
                  }
                }
            ]
        };
        this.relatedProductsSlider.slick(args);
  }

}

export default Category;
    