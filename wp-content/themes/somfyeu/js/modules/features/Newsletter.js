
import $ from 'jquery';


class Newsletter {

    constructor() {
        this.newsletterForm = $('#newsletter_signup__form');

        this.newsletterEmailInput = $('#newsletter_email_input');

        this.newsletterSubmit = $('#newsletter_submit');
        this.newsletterLoader = $('#newsletter__form_loader');
        this.formSubmitMsg = $('#form_submit_msg');

        this.initEvents();
    }

    initEvents(){
        this.newsletterForm.on('submit' , (e)=>{
            e.preventDefault();
            this.newsletterSubmit.prop('disabled', true).hide();
            this.newsletterLoader.show();

            this.signUser();
        })
    }

    signUser(){
        let userEmail = this.newsletterEmailInput.val();

        $.ajax({
            url : somfyData.ajax_url,
            type : 'POST',
            data : {
                action : 'somfy_eu_newsletter_signup',
                userEmail : userEmail,
                nonce: somfyData.ajax_nonce
            },
            dataType: "json",
            success: (response) =>{

                if(response.status){
                    // hide form
                    this.newsletterEmailInput.val('').fadeOut();
                    this.newsletterForm.hide();
                    // show success message
                    this.formSubmitMsg
                    .removeClass('error_msg')
                    .addClass('success_msg')
                    .html(response.message).fadeIn();
                } else {
                    // show error message
                    this.formSubmitMsg
                        .removeClass('success_msg')
                        .addClass('error_msg')
                        .html(response.message).fadeIn();

                    // show the form
                    this.newsletterEmailInput.fadeIn();
                    this.newsletterSubmit.prop('disabled', false).show();
                }
                this.newsletterLoader.hide();
            },
            error:  (jqXHR, status) => {
                console.warn(jqXHR);
                console.warn(status);
            }
          });
    }
}

export default Newsletter;
