import $ from 'jquery';

class Calculator {

    constructor() {
        this.calculator_trigger = $('.calculator_trigger ');
        this.calculatorBanner_trigger = $('.calculator_cta ');
        this.calculators_popup = $('#calculators_popup');
        this.calculator_popup__close = $('#calculators_popup .popup__overlay__close');
        this.calculator__title = $('#calculator__title');

        this.calculators_selector__container = $('#calculators_selector__container');
        this.calculators_selector__trigger = $('.calculators_selector__trigger');
        
        this.calc_1__loader = $('#calc_1__loader');
        this.calc_1__error = $('#calc_1__error');

        this.calculators_steps__container = $('#calculators_steps__container');
        this.next_step = $('.next_step');
        this.prev_step = $('.prev_step');

        this.CalcInputs = {};

        this.finalFilters = {};
        this.userInputs = {};

        // calc 1
        this.calc1_blindWeightsData;
        this.calc1_pipeDiameterData;

        this.calc1_blined_matrix;

        this.initEvents();
    }

    initEvents() {
        // calc popup trigger
        this.calculator_trigger.on('click', (e)=>{
            this.showCalculatorPopup(e);
        })
        this.calculatorBanner_trigger.on('click', (e)=>{
            this.showCalculatorPopup(e);
        })
        this.calculator_popup__close.on('click', (e)=>{
            this.hideCalculatorPopup();
        });

        this.calculators_steps__container.find('form').on('submit', (e)=>{
            e.preventDefault();
        });

        // next prev btn
        this.next_step.on('click', (e)=>{
            this.nextStep(e);
            this.calc1_calculateWeight();
        });

        this.prev_step.on('click', (e)=>{
            this.prevStep(e);
        });

        // Calc 1 - calculate blinds step weight 
        $('#calc_1__blind_type').on('change', (e)=>{
            this.calc1_filterBlindHeights();
            this.calc1_calculateWeight();
        })

        $('#calc_1__blind_height').on('change', (e)=>{
            this.calc1_calculateWeight();
        })

        $('.submit_calc[data-calculator="1"]').on('click', (e)=>{
            this.calc1_submitCalc(e);
        })

        $.getJSON( somfyData.template_url + "/assets/calc_json/calc1_blined_matrix.json", (data) => {
            this.calc1_blined_matrix = data;
        });
    }


    // calc weight
    calc1_calculateWeight() {
        let selectedBlindHeight = $('#calc_1__blind_height option:selected');

        let windowHeight = $('#calc_1__window_height').val() * 10;
        let windowWidth = $('#calc_1__window_width').val() * 10;
        
        // round windowWidth to 100
        let hDef = windowHeight % 20 < 10 ? (0 - windowHeight % 20) : (20 - windowHeight % 20);
        let wDef = windowWidth % 10 < 5 ? (0 - windowWidth % 10) : (10 - windowWidth % 10);

        windowWidth = windowWidth + wDef;
        windowHeight = windowHeight + hDef;

        if(selectedBlindHeight.length == 1){
            let mWeight = selectedBlindHeight[0].dataset.weight;

            let totalWeight = ((windowHeight * windowWidth * mWeight) / 1000000).toFixed(2);

            $('#calc_1__blinds_mr_weight').val(mWeight);
            $('#calc_1__blinds_weight').val(totalWeight);

            this.CalcInputs = {
                'windowHeight' : windowHeight,
                'windowWidth' : windowWidth,
                'selectedBlindHeight' : Object.assign({blind_height : $(selectedBlindHeight).val()}, selectedBlindHeight[0].dataset),
                'pipeDiameter' : [],
                'mWeight' : mWeight,
                'totalWeight' : totalWeight
            }

            this.calc1_filterPipeDiameter();
        } else {
            $('#calc_1__blinds_mr_weight').val('');
            $('#calc_1__blinds_weight').val('');
        }
    }

    // calc1_filterBlindHeights
    calc1_filterBlindHeights(){
        let selectedBlindType = $('#calc_1__blind_type option:selected').val();

        $('#calc_1__blind_height').html('');
        let items = '<option value="-1">גובה שלב</option>';

        for(let i = 0; i < this.calc1_blindWeightsData.length; i++) {
            let op = this.calc1_blindWeightsData[i];
            if(op.typecode == selectedBlindType){
                items += `
                <option value='${op.height}' 
                    data-weight='${op.kgm2}' 
                    data-typecode='${op.typecode}' 
                    data-lighning='${op.lighning}' 
                    data-forced='${op.forced}'
                    data-category='${op.category}'>${op.title}</option>`;
            }
        }
        $('#calc_1__blind_height').html(items);
    }


    calc1_filterPipeDiameter(){
        $.getJSON( somfyData.template_url + "/assets/calc_json/calc1_pipe_diameter.json", (data) => {
            let results = [];
            let resultsCode = [];
            for(let i = 0; i < data.length; i++) {
                let op = data[i];
                
                if(op.typecode == this.CalcInputs.selectedBlindHeight.typecode 
                    && op.type.toLowerCase() == this.CalcInputs.selectedBlindHeight.category.toLowerCase()
                    && op.forced.toString() == this.CalcInputs.selectedBlindHeight.forced.toString()
                    && op.lighting.toString() == this.CalcInputs.selectedBlindHeight.lighning.toString()
                    && parseInt(op.shelf) == parseInt(this.CalcInputs.selectedBlindHeight.blind_height)
                    && parseInt(op.height) == parseInt(this.CalcInputs.windowHeight)){
                        results.push(op.diampipe);
                        resultsCode.push(op.diamcode);
                }
            }
            this.CalcInputs.pipeDiameter = results;
            this.CalcInputs.pipeDiameterCode = resultsCode;
        });
        
    }


    calc1_getNameQuery(){
        let results = {
            name: [],
            torque: []
        };

        for(let i = 0; i < this.calc1_blined_matrix.length; i++) {
            let op = this.calc1_blined_matrix[i];
            let opTech = op.wired ? "4" : "2";

            if(op.shelftype.toLowerCase() == this.CalcInputs.selectedBlindHeight.category.toLowerCase() 
                && parseInt(op.shelfheight) == parseInt(this.CalcInputs.selectedBlindHeight.blind_height)
                && parseInt(this.CalcInputs.windowHeight) > parseInt(op.windowheightmin)
                && parseInt(this.CalcInputs.windowHeight) <= parseInt(op.windowheightmax)
                && parseFloat(this.CalcInputs.totalWeight) > parseFloat(op.totalweightmin)
                && parseFloat(this.CalcInputs.totalWeight) <= parseFloat(op.totalweightmax)
                && this.CalcInputs.pipeDiameter.includes(parseInt(op.pipediamter))
                && this.CalcInputs.productFilters.tech.includes(opTech)){
                    results.name.push(op.name);
                    if(op.torque) results.torque.push(op.torque);
            }
        }
        this.CalcInputs.nameQuery = [...new Set(results.name)];
        this.CalcInputs.torque = [...new Set(results.torque)];

        return results.name.length > 0 ? true : false;;
    }

    calc1_submitCalc(e){
        // show loader
        this.calc_1__error.hide().html('');
        this.calc_1__loader.fadeIn('fast');

        let errors = 0;
        let techValues = [];
        let techTexts = [];
        let rpmText = '';
        
        let blind_type = $('#calc_1__blind_type option:selected');
        let blind_height = $('#calc_1__blind_height option:selected');
        let tech = $('input[name="calc_1__tech"]:checked');

        if(!$(blind_type).val() || $(blind_type).val() == -1){
            $('p.form_error[data-for="calc_1__blind_type"]').css('visibility', 'visible');
            errors++;
        } else {
            $('p.form_error[data-for="calc_1__blind_type"]').css('visibility', 'hidden');
        }

        if(!$(blind_height).val() || $(blind_height).val() == -1){
            $('p.form_error[data-for="calc_1__blind_height"]').css('visibility', 'visible');
            errors++;
        } else {
            $('p.form_error[data-for="calc_1__blind_height"]').css('visibility', 'hidden');
        }

        if(tech.length == 0){
            $('p.form_error[data-for="calc_1__tech"]').css('visibility', 'visible');
            errors++;
        } else {
            $('p.form_error[data-for="calc_1__tech"]').css('visibility', 'hidden');
            for(let i = 0; i < tech.length; i++){
                techValues.push($(tech[i]).val());
                techTexts.push($(tech[i]).attr('data-title'));
            }
        }


        if(errors > 0) {
            this.finalFilters = {};
            this.userInputs = {};

            localStorage.removeItem('eu_calc_userInputs');
            localStorage.removeItem('eu_calc_finalFilters');

            // hide loader
            this.calc_1__loader.fadeOut('fast');
            return;
        }
        
        this.CalcInputs.productFilters = {
            tech: techValues
        };

        let nameQueryFound = this.calc1_getNameQuery();
        if(nameQueryFound == false){
            this.finalFilters = {};
            this.userInputs = {};
            
            localStorage.removeItem('eu_calc_userInputs');
            localStorage.removeItem('eu_calc_finalFilters');

            // show not found error
            this.calc_1__error.html('לא נמצאו תוצאות').slideDown('fast');

            // hide loader
            this.calc_1__loader.fadeOut('fast');

            return;
        }
        
        this.userInputs = {
            'windowHeight' : this.CalcInputs.windowHeight,
            'windowWidth' :this.CalcInputs.windowWidth,
            'blind_type' : $('#calc_1__blind_type option:selected').text(),
            'blind_height' : $('#calc_1__blind_height option:selected').text(),
            'tech' : techTexts.toString()
        }

        // create final filters obj
        this.finalFilters = {
            'category' : ['347'],
            'subgroup' : ['1362'],
            'subsubgroup' : ['1524', '1525'],
            'tech' : techValues,
            'pipeDiameter' : this.CalcInputs.pipeDiameterCode, 
            'nameQuery' : this.CalcInputs.nameQuery,
            'torque' : this.CalcInputs.torque
        }

        // save to localStorage
        localStorage.setItem('eu_calc_userInputs', JSON.stringify(this.userInputs));
        localStorage.setItem('eu_calc_finalFilters', JSON.stringify(this.finalFilters));

        // build url params
        let encodedParams = this.encodeData({'calculator' : true, 'category' : ['347'], 'subgroup' : ['1362']});
        let redirectUrl = somfyData.root_url + "/shop?" + encodedParams;


        // redirect to shop with filters
        window.location.href = redirectUrl;
    }




    // step form validation
    validateStepFrom(formElm){
        let formInputElms = formElm.elements;
        let errors = 0;

        for(let i = 0; i < formElm.elements.length; i++){
            let inp = $(formElm.elements[i]);
            let inpId = inp[0].id;
            
            if(inp.is('input[type="number"')){
                if(inp[0].validity.valid){
                    $('p.form_error[data-for="'+inpId+'"]').css('visibility', 'hidden');
                } else {
                    $('p.form_error[data-for="'+inpId+'"]').css('visibility', 'visible');
                    errors++;
                }
            } else if(inp.is('select')){
                let selected = $(inp).find('option:selected').val();
                
                if(selected == "-1"){
                    $('p.form_error[data-for="'+inpId+'"]').css('visibility', 'visible');
                    errors++;
                } else {
                    $('p.form_error[data-for="'+inpId+'"]').css('visibility', 'hidden');
                }
            }
        }

        return errors > 0 ? false : true;
    }


    nextStep(e){
        let formElm = $(e.currentTarget).closest('form.step__form_container')[0];
        let isValid = this.validateStepFrom(formElm);
        let next_step = e.currentTarget.dataset.next;
        let calculator = e.currentTarget.dataset.calculator;

        if(isValid){
            $(formElm).addClass('hidden');
            $(`#calculator_${calculator}`).find(`.step__form_container[data-step="${next_step}"`).removeClass('hidden');
        }
    }

    prevStep(e){
        let formElm = $(e.currentTarget).closest('form.step__form_container')[0];
        let prev_step = e.currentTarget.dataset.prev;
        let calculator = e.currentTarget.dataset.calculator;

        $(formElm).addClass('hidden');
        $(`#calculator_${calculator}`).find(`.step__form_container[data-step="${prev_step}"`).removeClass('hidden');
    }


    showCalculator(target, calculator_name) {
        this.calculator__title.html(calculator_name);
    }

    showCalculatorPopup(e){
        let calculator = 1;
        let calculatorTitle = 'מחשבון תריסים';

    
        this.calculator__title.html(calculatorTitle);
        this.calculators_selector__container.hide();
        this.calculators_steps__container.show();

        $('.step__form_container').addClass('hidden');
        $('.form_error').css('visibility', 'hidden');
        $('.calculator__container').hide();
        $('#calculator_1').show();
        $('#calculator_1').find('.step__form_container[data-step="1"]').removeClass('hidden');
        
        this.getBlineWeightsData(calculator);
        this.CalcInputs = {};

        let forms = $('.step__form_container');
        for(let i = 0; i < forms.length; i++){
            $(forms[i]).trigger('reset');
        };

        this.calculators_popup.fadeIn('fast');
    }
    

    hideCalculatorPopup(){
        this.calculators_popup.hide();
        this.calculators_selector__container.hide();
        this.calculator__title.html('');
    }


    getBlineWeightsData(target){
        $.getJSON( somfyData.template_url + "/assets/calc_json/calc"+target+"_bline_weights.json", (data) => {
            this.calc1_blindWeightsData = data;
        });
    }
    
    encodeData(data) {
        return Object.keys(data).map(function(key) {
            return [key, data[key]].map(encodeURIComponent).join("=");
        }).join("&");
    }  
}
export default Calculator;
