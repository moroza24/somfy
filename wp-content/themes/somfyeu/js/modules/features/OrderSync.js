import $ from 'jquery';

class OrderSync {

    constructor() {   
        setInterval(()=>{this.checkeIfSyncNeeded();}, 60000);
        this.isRunning = false;
        this.checkeIfSyncNeeded();
    }  

    checkeIfSyncNeeded(){
        let lastOrderSync = localStorage.getItem('eu_last_order_sync');
        let current = Date.now();

        if(!lastOrderSync){
            localStorage.setItem('eu_last_order_sync', current);       
            
            // call to sync orders
            this.syncOrders();
        } else {
            if(current - lastOrderSync >= 60000){
                localStorage.setItem('eu_last_order_sync', current);       
                
                // call to sync orders
                this.syncOrders();
            }
        }
    }

    syncOrders(){
        if(this.isRunning) return;

        this.isRunning = true;
        $.ajax({
            url : somfyData.ajax_url,
            type : 'POST',
            data : {
                action : 'somfy_eu_sync_user_orders',
                nonce: somfyData.ajax_nonce
            },
            dataType: "json",
            success: (response) =>{
                this.isRunning = false;
            },
            error:  (jqXHR, status) => {
                if(jqXHR.status != 200){
                    console.warn(jqXHR);
                    console.warn(status);
                }
                
            }
        });
    }
}
export default OrderSync;
