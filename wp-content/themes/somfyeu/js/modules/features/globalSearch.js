import $ from 'jquery';

class GlobalSearch {

    constructor() {
        this.globalSearch__trigger = $('#global_search__trigger');

        // desktop
        this.searchInput_container = $('.search_input_container');
        this.serachResults_container = $('.serach-results_container');
        this.closeSearchResults = $('#close_serach-results_container');

        this.searchInput = $('#global_search__term');
        this.searchLoader = $('#global_search_loader');

        this.searchResults = $('#serach-results');
        this.productsResultsContainer = $('#products_results');
        this.taxonomyResultsContainer = $('#taxonomy_results');
        this.showMoreResults = $('#show_more_results');
        this.noResultsMsg = $('#no_results_msg');
        this.overlayStatus = false;

        // Mobile
        this.mobileSearchOverlay = $('#mobile_search__container');
        this.mobileSearchInput = $('#mobile_search__term');
        this.closeMobileSearchOverlay = $('#close_mobile_search_container');
        this.mobileSearchLoader = $('#mobile_global_search_loader');
        this.mobileSearchResults = $('#mobile_serach-results');
        this.mobileProductsResultsContainer = $('#mobile_products_results');
        this.mobileTaxonomyResultsContainer = $('#mobile_taxonomy_results');
        this.mobileShowMoreResults = $('#mobile_show_more_results');
        this.mobileNoResultsMsg = $('#mobile_no_results_msg');
        this.mobileOverlayStatus = false;


        this.previousValue;
        this.isLoading = false;
        this.typingTimer;

        var w = window.innerWidth;
        this.isMobileView = w > 1140 ? false : true;
        
        $(window).resize(function() {
            let w = window.innerWidth;
            this.isMobileView = w > 1140 ? false : true;
        });

        this.initEvents();
    }
  
    initEvents(){
        $(document).on('keydown', this.keyPressDispatch.bind(this));

        // show/close search overlay
        this.searchInput.on('keyup', this.typingLogic.bind(this));
        this.mobileSearchInput.on('keyup', this.typingLogicMobile.bind(this));

        this.globalSearch__trigger.on('click',(e)=>{
            if(this.isMobileView == false){
                if(this.overlayStatus == false){
                    this.searchInput.fadeIn('slow');
                    $(this.searchInput).val('');
                    $(this.searchInput).focus();
                    this.overlayStatus = true;
                } else {
                    this.searchInput.fadeOut('slow');
                    this.serachResults_container.fadeOut('slow');
                    this.overlayStatus = false;
                }
            } else {
                if(this.mobileOverlayStatus == false){
                    this.mobileSearchOverlay.fadeIn('slow');
                    $(this.mobileSearchInput).focus();
                    this.mobileOverlayStatus = true;
                } else {
                    this.mobileSearchOverlay.fadeOut('slow');
                    this.mobileOverlayStatus = false;
                }
            }
            
        });

        this.closeSearchResults.on('click', () => {
            this.searchInput.fadeOut('slow');
            this.serachResults_container.fadeOut('slow');
            this.overlayStatus = false;
            this.clearResults();
        });

        this.closeMobileSearchOverlay.on('click', () => {
            this.mobileSearchOverlay.fadeOut('slow');
            this.mobileOverlayStatus = false;
            this.mobileShowMoreResults.hide();
            this.mobileSearchResults.hide();
            this.mobileSearchInput.val('');
            this.mobileNoResultsMsg.hide();
            this.mobileClearResults();
        });
    }
    

    typingLogic(e){
        if(this.searchInput.val() != this.previousValue && this.searchInput.val().length >= 3){
            clearTimeout(this.typingTimer);

            if(this.searchInput.val()){
                if(!this.isLoading){
                    this.searchLoader.fadeIn('slow');
                    this.serachResults_container.fadeIn('slow');
                    this.noResultsMsg.hide('fast');
                    this.clearResults();
                    this.isLoading = true;  
                }
                this.showMoreResults.hide();
                this.typingTimer = setTimeout(this.getResults.bind(this), 750); 
            } else {
                this.searchLoader.fadeOut('slow');
                this.clearResults();
                this.isLoading = false;
            }            
        } else {
            this.showMoreResults.hide();
            this.searchLoader.fadeOut('slow');
            this.clearResults();
            this.isLoading = false;
        }
        this.previousValue = this.searchInput.val();
    }

    typingLogicMobile(e){
        if(this.mobileSearchInput.val() != this.previousValue && this.mobileSearchInput.val().length >= 3){
            clearTimeout(this.typingTimer);

            if(this.mobileSearchInput.val()){
                if(!this.isLoading){
                    this.mobileSearchLoader.fadeIn('slow');
                    this.mobileNoResultsMsg.hide();
                    this.mobileClearResults();
                    this.isLoading = true;  
                }
                this.mobileShowMoreResults.hide();
                this.typingTimer = setTimeout(this.mobileGetResults.bind(this), 750); 
            } else {
                this.mobileSearchLoader.fadeOut('slow');
                this.mobileClearResults();
                this.isLoading = false;
            }            
        } else {
            this.mobileShowMoreResults.hide();
            this.mobileSearchLoader.fadeOut('slow');
            this.mobileClearResults();
            this.isLoading = false;
        }
        this.previousValue = this.mobileSearchInput.val();
    }



    getResults(){
        let term = this.searchInput.val();


        $.ajax({
            url : somfyData.ajax_url,
            type : 'POST',
            data : {
                action : 'somfy_eu_global_search',
                term : term,
                nonce: somfyData.ajax_nonce
            },
            dataType: "json",
            success: (response) =>{
                if(response.status){
                    let products_results = response.results.products;
                    let taxonomy_results = response.results.taxonomy;

                    if(response.total_products == 0){
                        this.searchResults.hide();
                        this.noResultsMsg.fadeIn('fast');
                    } else {
                        this.noResultsMsg.hide();
                        this.printProductsResults(products_results);
                        this.printTaxonomyResults(taxonomy_results);
                        this.searchResults.fadeIn('slow');
                        this.showMoreResults.attr('href', somfyData.root_url + '/search' + '?term=' + term);
                        this.showMoreResults.show();
                    }
                }
                this.isLoading = false;
            },
            error:  (jqXHR, status) => {
                console.warn(jqXHR);
                console.warn(status);
            }
          });
        this.searchLoader.fadeOut('slow');

    }


    mobileGetResults(){
        let term = this.mobileSearchInput.val();

        $.ajax({
            url : somfyData.ajax_url,
            type : 'POST',
            data : {
                action : 'somfy_eu_global_search',
                term : term,
                nonce: somfyData.ajax_nonce
            },
            dataType: "json",
            success: (response) =>{
                if(response.status){
                    let products_results = response.results.products;
                    let taxonomy_results = response.results.taxonomy;

                    if(response.total_products == 0){
                        this.mobileSearchResults.hide();
                        this.mobileNoResultsMsg.fadeIn('fast');
                    } else {
                        this.mobileNoResultsMsg.hide();
                        this.printProductsResults(products_results);
                        this.printTaxonomyResults(taxonomy_results);
                        this.mobileSearchResults.fadeIn('slow');
                        this.mobileShowMoreResults.attr('href', somfyData.root_url + '/search' + '?term=' + term);
                        this.mobileShowMoreResults.show();
                    }
                }
                this.isLoading = false;
            },
            error:  (jqXHR, status) => {
                console.warn(jqXHR);
                console.warn(status);
            }
          });
        this.mobileSearchLoader.fadeOut('slow');

    }

    printProductsResults(products_results){
        if(products_results.length > 0){
            if(this.isMobileView == false){
                this.productsResultsContainer.html(`${products_results.map(product => `
                    <li>
                        <a class='result_title' href='${product.permalink}'  alt='${product.post_title}'>${product.post_title}</a>
                        <div class='result_type'>PRODUCT</div>
                    </li>`).join('')}`);  
            } else {
                this.mobileProductsResultsContainer.html(`${products_results.map(product => `
                    <li>
                        <a class='result_title' href='${product.permalink}'  alt='${product.post_title}'>${product.post_title}</a>
                        <div class='result_type'>PRODUCT</div>
                    </li>`).join('')}`);  
            }
            
        }
    }


    printTaxonomyResults(taxonomy_results){
        if(taxonomy_results.length > 0){
            if(this.isMobileView == false){
                this.taxonomyResultsContainer.html(`${taxonomy_results.map(taxonomy => `
                    <li>
                        <a class='result_title' href='${taxonomy.permalink}' alt='${taxonomy.post_title}'>${taxonomy.post_title}</a>
                        <div class='result_type'>${taxonomy.post_type}</div>
                    </li>`).join('')}`);
            } else {
                this.mobileTaxonomyResultsContainer.html(`${taxonomy_results.map(taxonomy => `
                    <li>
                        <a class='result_title' href='${taxonomy.permalink}' alt='${taxonomy.post_title}'>${taxonomy.post_title}</a>
                        <div class='result_type'>${taxonomy.post_type}</div>
                    </li>`).join('')}`);
            }
        }
        
    }

    /*------ searchResults close ------*/
    keyPressDispatch(e){
        if(e.keyCode == 27 && this.overlayStatus){
            this.serachResults_container.fadeOut('slow');
            this.searchInput.fadeOut('slow');
            this.serachResults_container.fadeOut('slow');
            this.overlayStatus = false;
        }
    }

    
    clearResults() {
        this.productsResultsContainer.html(``);
        this.taxonomyResultsContainer.html(``);
    }

    mobileClearResults() {
        this.mobileProductsResultsContainer.html(``);
        this.mobileTaxonomyResultsContainer.html(``);
    }
}



export default GlobalSearch;
