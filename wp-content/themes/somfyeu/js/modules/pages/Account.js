
import $ from 'jquery';
class Account {

    constructor() {
        // password reset tab
        $('#um_field_password_current_user_password').find('label').html('סיסמא נוכחית');
        $('#um_field_password_user_password').find('label').html('סיסמא חדשה');
        $('#um_field_password_confirm_user_password').find('label').html('אימות סיסמה חדשה');
        $('.um-account-tab-password').find('input[type=password]').attr('placeholder', '');

        // delete account
        $('.um-account-tab-delete').children('p').html('לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.');
        $('#um_field_delete_single_user_password').find('label').html('סיסמא');

        // update account
        $('#um_field_general_user_login').find('label').html('שם משתמש');
        $('#um_field_general_first_name').find('label').html('שם פרטי');
        $('#um_field_general_last_name').find('label').html('שם משפחה');
        $('#um_field_general_user_email').find('label').html('כתובת דוא"ל');

        this.accountOrders__loader = $('#account_orders__loader');
        this.no_orders__message = $('#no_orders__message');
        this.orders_resultes__container = $('.orders_resultes__container');
        
        
        this.orderDetails_popup = $('#order_details_popup');
        this.orderDetails_popup__close = $('#order_details_popup__close');
        this.orderDetails__loader = $('#order_details__loader');
        this.no_ordersitems__message = $('#no_ordersitems__message');
        this.order_details__results = $('#order_details__results');
        this.sort_orders_btn = $('.sort_orders_btn');
        this.getUserOrders();
    }


    getUserOrders(){
        let userInternalid = $('#user_internalid').val();
        
        this.accountOrders__loader.show();

        $.ajax({
            url : somfyData.ajax_url,
            type : 'POST',
            data : {
                action : 'somfy_eu_user_orders',
                user_internalid: userInternalid,
                nonce: somfyData.ajax_nonce
            },
            dataType: "json",
            success: (response) =>{
                if(response.status){
                    
                    if(response.userOrders && response.userOrders.length > 0){
                        // print active orders
                        // print old orders
                        this.printUserOrders(response.userOrders);
                        // show results
                        this.orders_resultes__container.show();
                    } else {
                        // show no results msg
                        this.no_orders__message.removeClass('hidden');
                    }
                    
                } else {
                    // SHOW ERROR
                    console.warn(response);
                }
                
                this.accountOrders__loader.hide();

            },
            error:  (jqXHR, status) => {
                console.warn(jqXHR);
                console.warn(status);
            }
        });
    }


    printUserOrders(orders) {
        let activeOrdersTbody = $('tbody[data-for="active_orders_results"]');
        
        orders.forEach(order => {
            let orderTrHtml = `<tr>
                    <th scope='row'>
                        <a class='show_order_items' data-order='${order.tranid}' alt='${order.tranid}'>${order.tranid}</a></td>
                    </th>
                    <td>${order.saleseffectivedate}</td>
                    <td class='order_actions'>
                        <a class='sf_btn sf_btn__small show_order_items' data-order='${order.tranid}' alt='פרטי הזמנה'>פרטי הזמנה</a></td>
                </tr>`;

            activeOrdersTbody.append(orderTrHtml);
        });

        this.initShowOrderItemsEvent();
    }

    initShowOrderItemsEvent(){
        let showOrderItemsBtn = $('.show_order_items');

        showOrderItemsBtn.on('click', (e)=>{
            let orderTranid = e.currentTarget.dataset.order;
            
            this.resetOrderInfo();
            this.orderDetails__loader.show();
            this.no_ordersitems__message.hide();
            this.order_details__results.hide();
            this.orderDetails_popup.fadeIn('fast');

            $.ajax({
                url : somfyData.ajax_url,
                type : 'POST',
                data : {
                    action : 'somfy_eu_order_details',
                    order_tranid: orderTranid,
                    nonce: somfyData.ajax_nonce
                },
                dataType: "json",
                success: (response) =>{
                    if(response.status){
                        if(response.items && response.items.length > 0){
                            this.printOrderItems(response.items);
                            this.printOrderInfo(response.orderInfo);
                            this.order_details__results.show();
                        } else {
                            // show no results found
                            this.no_ordersitems__message.show();
                        }

                        // hide loader
                        this.orderDetails__loader.hide();
                    } else {
                        // SHOW ERROR
                        console.warn(response);
                    }
    
                },
                error:  (jqXHR, status) => {
                    console.warn(jqXHR);
                    console.warn(status);
                }
            });
        });

        this.orderDetails_popup__close.on('click', () =>{
            this.orderDetails_popup.fadeOut('fast');
        })

        this.sort_orders_btn.on('click', (e) =>{
            let target = e.currentTarget.dataset.for;
            let order = e.currentTarget.dataset.order;

            let newOrder = order == 'asc' ? 'desc' : 'asc';
            $(e.currentTarget).attr('data-order', newOrder);
            
            this.sortByDate(target, order);
        })
    }


    
    printOrderItems(items){
        let orderDetails__orderitems = $('tbody[data-for="order_details__orderitems"]');
        orderDetails__orderitems.html('');
        
        let shippingproductID = $('#shipping_product_id').val();
        $('#order_details__shipping').html('').hide();
        
        items.forEach(item => {
            if(item.isInternalItem && item.productID == shippingproductID) {
                $('#order_details__shipping').html("₪" + this.numberWithCommas(item.grossamount)).show();
                return;
            };
            let orderItemTrHtml = `<tr>
                    <th scope='row'>
                        <a href='${item.permalink}' alt='${item.itemid}'> ${item.itemid}</a>
                    </th>
                    <td>${item.post_title}</td>
                    <td>${item.quantity}</td>
                    <td>${item.quantityshiprecv}</td>
                    <td>${item.grossamount}</td>
                </tr>`;

            orderDetails__orderitems.append(orderItemTrHtml);
        });
    }


    printOrderInfo(orderInfo){
        $('#order_details__subtotal').html("₪" + this.numberWithCommas(orderInfo.acf.subtotal));
        $('#order_details__total').html("₪" + this.numberWithCommas(orderInfo.acf.total));
        $('#order_details__order_tranid').html(orderInfo.acf.tranid);
    }


    resetOrderInfo(){
        $('#order_details__subtotal').html("");
        $('#order_details__total').html("");
        $('#order_details__order_tranid').html("");
    }

    numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }


    convertDate(d) {
        var p = d.split("/");
        return +(p[2]+p[1]+p[0]);
    }
      
    sortByDate(target, order = 'asc') {
        var tbody = document.querySelector("tbody[data-for='" + target + "']");
        // get trs as array for ease of use
        var rows = [].slice.call(tbody.querySelectorAll("tr"));
        rows.sort(function(a,b) {
            let aArr = a.cells[1].innerHTML.split("/");
            let aVal = 0 + (aArr[2]+aArr[1]+aArr[0]);
            let bArr = b.cells[1].innerHTML.split("/");
            let bVal = 0 + (bArr[2]+bArr[1]+bArr[0]);
            
            if(order == 'asc'){
                return bVal - aVal;
            } else {
                return aVal - bVal;
            }
        });
        
        rows.forEach(function(v) {
          tbody.appendChild(v); // note that .appendChild() *moves* elements
        });
      }
      
}

export default Account;
    