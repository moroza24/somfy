import $ from 'jquery';

class SearchPage {

    constructor(stockNotifictaion, wishlist, cart) {
        this.stockNotifictaion = stockNotifictaion;
        this.wishlist = wishlist;
        this.cart = cart;

        this.sortFiltersBtn = $('.sort_filter_btn');
        
        this.searchResults_container = $('#search_results_container');
        this.searchResults = $('#search_results');
        this.noResults_msg = $('#search_page_no_results_msg');
        this.searchLoader = $('#search_loader');
        this.totalSearchResults = $('#total_search_results');
        
        this.loadMoreCTA = $('#load_more_cta');
        this.loadingMore = false;
        this.sortFilter = {
            sortType : '',
            sortOrder : ''
        };

        this.paginators = {
            limit: 20,
            page: 1,
            totalPages: 0
        };

        this.isLoading = false;
        this.term = $('#search_term').val();

        this.initEvents();
    }


    initEvents(){
        this.sortFiltersBtn.on('click', (e)=>{
            this.setFilters(e);
        });

        if(this.term == ''){
            this.searchLoader.hide();
            this.noResults_msg.fadeIn('fast');
            this.searchResults.html('');
        } else {
            this.getResults();
        }


        this.loadMoreCTA.on('click', ()=>{
            this.loadMoreCTA.hide();
            this.paginators.page += 1;
            this.loadingMore = true;

            this.getResults();
        })
    }

    

    getResults(){
        this.searchLoader.fadeIn('slow');
        this.isLoading = true;
        this.noResults_msg.hide();

        $.ajax({
            url : somfyData.ajax_url,
            type : 'POST',
            data : {
                action : 'somfy_eu_full_search',
                term : this.term,
                filters : this.sortFilter,
                paginators: this.paginators,
                nonce: somfyData.ajax_nonce
            },
            dataType: "json",
            success: (response) =>{
                if(response.status){
                    let products_results = response.results.products;

                    if(response.total == 0){
                        // show "nothing found msg"
                        this.noResults_msg.fadeIn('fast');
                        this.searchResults.html('');
                        this.searchResults.hide();
                    } else {
                        // print results
                        
                        this.noResults_msg.hide();
                        this.printProductsResults(products_results);
                        
                        this.totalSearchResults.html('סה"כ תוצאות: ' + response.total);

                        // print #load_more_cta
                        this.paginators.totalPages = Math.ceil(response.total / this.paginators.limit);
                        
                        if(this.paginators.totalPages > this.paginators.page){
                            this.loadMoreCTA.fadeIn('fast');
                            this.paginators.totalPages = Math.ceil(response.total / this.paginators.limit);
                        } else if(this.paginators.totalPages == this.paginators.page){
                            this.loadMoreCTA.hide();
                        }
                        
                        this.stockNotifictaion.initCtaEvents();
                        this.wishlist.addClickEvent();
                        this.wishlist.getUsersWishlist();
                        this.cart.initAddToCartEvent();

                        this.searchResults.fadeIn('fast');
                    }
                }
                this.isLoading = false;
            },
            error:  (jqXHR, status) => {
                console.warn(jqXHR);
                console.warn(status);
            }
            });
        this.searchLoader.fadeOut('slow');

    }

    printProductsResults(products){
        let resultsHtml = this.loadingMore ? this.searchResults.html() : '';

        products.forEach((product) => {

            // print product thumbnail & tags
            resultsHtml += `
                <div class='product-card'>
                    <div class='product-thumbnail-col'>`;
            
            // print discount tag
            if(product.meta.discount_percentage && product.meta.discount_percentage != ''){
                resultsHtml += `
                    <span class="dicount_tag">
                        <span class='tag__title'>הנחה</span><br>
                        ${product.meta.discount_percentage}%
                    </span>`;
            }

            // print out_of_stock tag
            if(product.meta.stock_quantity < 3){
                resultsHtml += `<span class="out_of_stock_tag">אזל במלאי</span>`;
            }

            // wishlistCTA
            resultsHtml += `<a class='wishlist_cta' data-product='${product.ID}'  alt='מועדפים'>
                <i class="far fa-heart"></i>
            </a>`;

            resultsHtml += `
                <a href='${product.permalink}' alt='${product.post_title}'><img 
                    src='${product.thumbnail_url}' 
                    alt='${product.post_title}' 
                    class='product-thumbnail'></a>
                </div>`;


            // print product title
            resultsHtml += `
                <div class='product-meta-container'>
                    <a href="${product.permalink}" class='product-title' alt='${product.post_title}'>${product.post_title}</a>
                </div>`;

            // print product CTA & prices
            resultsHtml += `
                <hr class="divider">
                <div class="product-cta-container">
                    <div class='product-cta'>`;
                if(product.meta.stock_quantity < 3){
                    resultsHtml += `<button class='get_stock_reminder get_stock_reminder_small' data-product='${product.ID}'>
                            תודיעו לי שהמוצר חוזר
                        </button>`;
                } else {
                    resultsHtml += `<button 
                        class='add_to_cart__cta add_to_cart__cta_small' 
                        data-product='${product.ID}'
                        data-stock='${product.stock_quantity}'
                        data-add_warranty='false'
                        data-warranty_applicable='${product.warranty_applicable}'>
                            <img 
                                src="${somfyData.template_url}/assets/svg/002-shopping-cart.svg" 
                                alt="cart" 
                                loading="lazy">
                            הוסף לסל
                        </button>`;
                }
                resultsHtml += `</div>
                    <div class='product-price'>`;
            
            if(product.meta.on_sell == true){
                resultsHtml += `
                            <span class='old_price'>
                                <span class='currency_symbol'>₪</span>${product.meta.base_price}
                            </span>

                            <span class='discount_price'>
                                <span class='currency_symbol'>₪</span>${product.meta.discount_price}
                            </span>`;
            } else {
                resultsHtml += `
                    <span class='base_price'>
                                <span class='currency_symbol'>₪</span>${product.meta.base_price}
                            </span>`;
            }
            resultsHtml += `
                </div></div></div>`;
        });
        
        this.searchResults.html(resultsHtml);
    }



    setFilters(e){
        this.loadingMore = false;
        this.paginators.page = 1;

        if($(e.currentTarget).hasClass('active')){
            $(e.currentTarget).removeClass('active');
            this.sortFilter.sortType = '';
            this.sortFilter.sortOrder = '';
        } else {
            $('.sort_filter_btn.active').removeClass('active');
            $(e.currentTarget).addClass('active');
            this.sortFilter.sortType = e.currentTarget.dataset.sortType;
            this.sortFilter.sortOrder = e.currentTarget.dataset.sortOrder;
        }

        this.getResults();
    }
}

export default SearchPage;
