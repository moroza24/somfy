
import $ from 'jquery';
class Home {

    constructor(stockNotifictaion, wishlist, cart) {
      this.wishlist = wishlist;
      this.cart = cart;
      this.stockNotifictaion = stockNotifictaion;

        // Hero Slider 
        this.heroSlider = $(".hero_slider");

        // Categories slider
        this.categoriesSlider = $(".categories_slider");
        this.selectedProductsSlider = $(".selected_products_slider");

        this.initHeroSlider();
        this.initCategoriesSlider();
        this.initSelectedProductsSlider();

        this.wishlist.addClickEvent();
        this.stockNotifictaion.initCtaEvents();
        this.cart.initAddToCartEvent();
    }

    // init Hero slider
    initHeroSlider(){
        let args = {
            autoplay: true,
            arrows: false,
            dots: true,
            slidesToShow: 1,
            infinite: true,
            swipeToSlide: true
        };
        this.heroSlider.slick(args);
    }

    // init Categories slider
    initCategoriesSlider(){
        let args = {
            autoplay: false,
            arrows: false,
            dots: true,
            slidesToShow: 7,
            slidesToScroll: 1,
            infinite: false,
            swipeToSlide: true,
            prevArrow:"<a class='slick-prev pull-left' alt='prev'><i class='fas fa-chevron-left' aria-hidden='true'></i></a>",
            nextArrow:"<a class='slick-next pull-right' alt='next'><i class='fas fa-chevron-right' aria-hidden='true'></i></a>",
            responsive: [
                {
                  breakpoint: 1920,
                  settings: {
                    slidesToShow: 7,
                  }
                },
                {
                    breakpoint: 1600,
                    settings: {
                      slidesToShow: 5,
                    }
                  },
                {
                    breakpoint: 1366,
                    settings: {
                      slidesToShow: 4,
                    }
                  },
                {
                  breakpoint: 1024,
                  settings: {
                    slidesToShow: 3,
                  }
                },
                {
                  breakpoint: 800,
                  settings: {
                    slidesToShow: 2,
                  }
                },
                {
                  breakpoint: 580,
                  settings: {
                    slidesToShow: 1,
                  }
                }
            ]
        };
        this.categoriesSlider.slick(args);
    }

    // init Categories slider
    initSelectedProductsSlider(){
      let args = {
          autoplay: false,
          arrows: true,
          dots: false,
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: false,
          swipeToSlide: true,
          prevArrow:"<a class='slick-prev pull-left' alt='prev'><i class='fas fa-chevron-left' aria-hidden='true'></i></a>",
          nextArrow:"<a class='slick-next pull-right' alt='next'><i class='fas fa-chevron-right' aria-hidden='true'></i></a>",
          responsive: [
              {
                  breakpoint: 1600,
                  settings: {
                    slidesToShow: 2,
                  }
              },
              {
                breakpoint: 800,
                settings: {
                  slidesToShow: 1,
                }
              }
          ]
      };
      this.selectedProductsSlider.slick(args);
  }

}

export default Home;
    