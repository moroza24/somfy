import $ from 'jquery';


class TopNav {

    constructor() {
        this.userMenuTrigger = $('#user-menu__trigger');
        this.userMenuDropdown = $('#user-menu__dropdown');
        this.cartPopupTrigger = $('#cart__trigger');
        this.mobileMenuTrigger = $('#mobile_menu__trigger');
        this.mobileMenuClose = $('.mobile-menu__close');
        this.mobileMenuOverlay = $('#mobile-menu__overlay');
        this.cartPopup = $('#cart_popup__container');


        this.submenuTrigger = $('.category_link');
        this.mobileSubmenuTrigger = $('.link__container');
        this.userMenu__dropdown__close = $('.user-menu__dropdown__close');
        

        // megamenu
        this.subgroupTrigger = $('.group_link__container');

        this.initEvents();
    }

    initEvents(){
        let showLoginError = $('.um.um-login').hasClass('um-err');
        
        if(showLoginError){
            this.userMenuDropdown.fadeIn();
            this.userMenuTrigger.addClass('active');
        }

        this.userMenuTrigger.on('click', (e)=>{

            if( this.userMenuDropdown.is(':visible') ) {
                this.userMenuDropdown.fadeOut();
                this.userMenuTrigger.removeClass('active');

            } else {
                this.userMenuDropdown.fadeIn();
                this.userMenuTrigger.addClass('active');

            }

        })

        this.userMenu__dropdown__close.on('click', (e)=>{
            this.userMenuDropdown.fadeOut();
            this.userMenuTrigger.removeClass('active');
        })
        


        this.mobileMenuTrigger.on('click', (e)=>{
            this.mobileMenuOverlay.fadeIn('fast');
        });

        this.mobileMenuClose.on('click', (e)=>{
            this.mobileMenuOverlay.fadeOut('fast');
        });

        // show megamenu
        this.submenuTrigger.on('mouseenter', (e)=>{
            $(e.currentTarget).find('.megamenu__container').show();
        });

        this.submenuTrigger.on('mouseleave', (e)=>{
            $(e.currentTarget).find('.megamenu__container').hide();
        });

        // show megamenu subgroups
        this.subgroupTrigger.on('mouseenter', (e)=>{
            let target = e.currentTarget.dataset.target;

            let currentActive = $('.group_link__container.active');
            if(currentActive.length > 0){
                let currentActiveTarget = $(currentActive)[0].dataset.target;
                currentActive.removeClass('active');
                $('#'+currentActiveTarget).hide();
            }

            $(e.currentTarget).addClass('active');
            $('#'+target).slideDown();
        });

        this.mobileSubmenuTrigger.on('click', (e)=>{
            $(e.currentTarget).find('.mobile-submenu__container').slideToggle('fast');
            $(e.currentTarget).find('.mobile-link__icon').toggleClass('mobile-link__icon--reverse');
        })
    }
}

export default TopNav;
