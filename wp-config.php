<?php
define( 'WP_CACHE', true );

// BEGIN iThemes Security - Do not modify or remove this line
// iThemes Security Config Details: 2
define( 'DISALLOW_FILE_EDIT', true ); // Disable File Editor - Security > Settings > WordPress Tweaks > File Editor
// END iThemes Security - Do not modify or remove this line

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */

// Localhost
define( 'DB_NAME', 'somfy' );
define( 'DB_USER', 'root' );
define( 'DB_PASSWORD', '' );
define( 'DB_HOST', 'localhost' );
define( 'DB_CHARSET', 'utf8mb4' );
define( 'DB_COLLATE', '' );


// staging
// define( 'DB_NAME', 'jetadmin_somfy' );
// define( 'DB_USER', 'jetadmin_root' );
// define( 'DB_PASSWORD', 'TMBi(c0@2!v~' );
// define( 'DB_HOST', 'localhost' );
// define( 'DB_CHARSET', 'utf8mb4' );
// define( 'DB_COLLATE', '' );
// define('DISABLE_WP_CRON', true);


/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '!Dd{N%6aJ,{Z[qu[|NpoyUUKt(1@bw0D<D<L5eVhJ?E-/CQ4s}X3WY7l$^]/Y$Gj' );
define( 'SECURE_AUTH_KEY',  '$NrEv-{l$l9O>)Jp-<op(NN+Zt{IHJ26^wByK8Iyc(bE$TX}y9s~Z!n;*oEYro{@' );
define( 'LOGGED_IN_KEY',    'uPi=7<`kvONkl?cBL=Q(Mz!gOS5U.}$WJw?<5kg:F3$*Ho]M|y+C3{CmHQb2}qb|' );
define( 'NONCE_KEY',        '%fZj{^U(Im&1HC}2d,t}oUAl>+hL&CpoN=+wdkD@,P`uAc~*pz }crZB?7rr,Vo`' );
define( 'AUTH_SALT',        'h- UJQC ua=^9&W3<(n>6$*QypKf4tqVhoF>.MBGeh/*!^fJK$~CBT6p3uN>6K([' );
define( 'SECURE_AUTH_SALT', '3U>|7e2r<^Tgpn@)Ha*M0q1XjI/IO#oY@z%Dk@,NfA5i]t}`p%//$H!LS[*@J^ai' );
define( 'LOGGED_IN_SALT',   ';`B,{?DWPXZMGT$bI?*dX1`46asa~p@s%*{_5gXmpKgjsEbZD^ojfC6C~9A2L[`]' );
define( 'NONCE_SALT',       'P2&2:,7ck}f!A_r~v&aoJLuf$p>Vj^!3>C/QP1 L{xU.Bt?R=FpG:>[QspE>rm`C' );
define( 'NONCE_SECRET',       'P2&2:,7ck}f!A_r~v&aoJLuf$p>Vj^!3>C/QP1 L{xU.Bt?R=FpG:>[QspE>rm`C' );

/**
 * NetSuite Restlet Auth 1.0
 */
define('NETSUITE_URL', 				'https://5676669-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl');
define("NETSUITE_ACCOUNT", 			'5676669_SB1');
define("NETSUITE_CONSUMER_KEY", 	'33551acf2f601d3eca300f464bdabbe41b98d49efe6cd32e7c437228373b68d7');
define("NETSUITE_CONSUMER_SECRET", 	'87e0bed75a8919bf72c972c802eaba9c65570449dc85ca96fdebb38ab9cb2c6a');
define("NETSUITE_TOKEN_ID", 		'4a75f13ddf07ac6692e9593999a7e0b22b1aafbadab8ff1eb6ac80eb27e7530b');
define("NETSUITE_TOKEN_SECRET", 	'9d722ac4c4b07b99563413be0d32da695d73cd55207c13cf4fb33268a1897164');

define("NETSUITE_REST_TOKEN", 	'4uPog9qHvc7Mk9QHRSzySDOThXoP1OxZ9Bc5d3ub8ELNcyEWy8zkZOFRLAhnuaEW');


/**
 * Pelecard Cradentials
 */
define("PELE_TERMINAL", 		'0962210');
define("PELE_USER", 			'shivukTest');
define("PELE_PASSWORD", 		'r4FW1BDP');

 
/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'sm_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );

/* Multisite */
define( 'WP_ALLOW_MULTISITE', true );

define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', false);

// Localhost
define('DOMAIN_CURRENT_SITE', 'localhost');
define('PATH_CURRENT_SITE', '/somfy/');

// staging
// define('DOMAIN_CURRENT_SITE', '91.202.169.189');
// define('PATH_CURRENT_SITE', '/staging/');

define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);

define('EU_SITE_PREFIX', $table_prefix);
define('PRO_SITE_PREFIX', $table_prefix . '2_');

// log php errors
@ini_set('log_errors','On'); // enable or disable php error logging (use 'On' or 'Off')
@ini_set('display_errors','Off'); // enable or disable public display of errors (use 'On' or 'Off')
@ini_set('error_log',ABSPATH .'/log/php-errors.log'); // path to server-writable log file
/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';

