var gulp = require('gulp'),
settings = require('./settings'),
webpack = require('webpack'),
browserSync = require('browser-sync').create(),
postcss = require('gulp-postcss'),
rgba = require('postcss-hexrgba'),
autoprefixer = require('autoprefixer'),
cssvars = require('postcss-simple-vars'),
nested = require('postcss-nested'),
cssImport = require('postcss-import'),
mixins = require('postcss-mixins'),
colorFunctions = require('postcss-color-function'),
sass = require('gulp-sass');


// Plugin scripts
gulp.task('pluginStyles', function() {
  return gulp.src(settings.pluginLocation + 'assets/sass/style.sass')
    .pipe(sass())
    .on('error', (error) => console.log(error.toString()))
    .pipe(gulp.dest(settings.pluginLocation + 'assets/'));
});

gulp.task('pluginScripts', function(callback) {
  webpack(require('./webpack-plugin.config.js'), function(err, stats) {
    if (err) {
      console.log(err.toString());
    }

    console.log(stats.toString());
    callback();
  });
});


// Theme scripts
// EU
gulp.task('styles_eu', function() {
  return gulp.src(settings.themeLocationEU + 'sass/style.sass')
    .pipe(sass())
    .on('error', (error) => console.log(error.toString()))
    .pipe(gulp.dest(settings.themeLocationEU));
});


gulp.task('scripts_eu', function(callback) {
  webpack(require('./webpack-eu.config.js'), function(err, stats) {
    if (err) {
      console.log(err.toString());
    }
    console.log(stats.toString());
    callback();
  });
});

// PRO
gulp.task('styles_pro', function() {
  return gulp.src(settings.themeLocationPRO + 'sass/style.sass')
    .pipe(sass())
    .on('error', (error) => console.log(error.toString()))
    .pipe(gulp.dest(settings.themeLocationPRO));
});


gulp.task('scripts_pro', function(callback) {
  webpack(require('./webpack-pro.config.js'), function(err, stats) {
    if (err) {
      console.log(err.toString());
    }
    console.log(stats.toString());
    callback();
  });
});


gulp.task('watch', function() {
  browserSync.init({
    notify: false,
    proxy: settings.urlToPreview,
    ghostMode: false
  });

  gulp.watch('./**/*.php', function() {
    browserSync.reload();
  });

  gulp.watch([settings.themeLocationEU + 'sass/**/*.sass', settings.themeLocationEU + 'sass/**/**/*.sass'], ['waitForStylesEU']);
  gulp.watch([settings.themeLocationEU + 'sass/**/*.scss', settings.themeLocationEU + 'sass/**/**/*.scss'], ['waitForStylesEU']);
  gulp.watch([settings.themeLocationEU + 'js/modules/**/*.js', settings.themeLocationEU + 'js/scripts.js'], ['waitForScriptsEU']);

  gulp.watch([settings.themeLocationPRO + 'sass/**/*.sass', settings.themeLocationPRO + 'sass/**/**/*.sass'], ['waitForStylesPRO']);
  gulp.watch([settings.themeLocationPRO + 'sass/**/*.scss', settings.themeLocationPRO + 'sass/**/**/*.scss'], ['waitForStylesPRO']);
  gulp.watch([settings.themeLocationPRO + 'js/modules/**/*.js', settings.themeLocationPRO + 'js/scripts.js'], ['waitForScriptsPRO']);

  gulp.watch(settings.pluginLocation + 'assets/sass/**/*.sass', ['waitForPluginStyles']);
  gulp.watch(settings.pluginLocation + 'assets/sass/**/*.scss', ['waitForPluginStyles']);
  gulp.watch([settings.pluginLocation + 'assets/sass/**/*.sass', settings.pluginLocation + 'assets/sass/**/**/*.sass'], ['waitForPluginStyles']);
  gulp.watch([settings.pluginLocation + 'assets/sass/**/*.scss', settings.pluginLocation + 'assets/sass/**/**/*.scss'], ['waitForPluginStyles']);
  gulp.watch([settings.pluginLocation + 'assets/js/modules/**/*.js', settings.pluginLocation + 'assets/js/scripts.js'], ['waitForPluginScripts']);

});


// EU
gulp.task('waitForStylesEU', ['styles_eu'], function() {
  return gulp.src(settings.themeLocationEU + 'style.css')
    .pipe(browserSync.stream());
});

gulp.task('waitForScriptsEU', ['scripts_eu'], function() {
  browserSync.reload();
});

// PRO
gulp.task('waitForStylesPRO', ['styles_pro'], function() {
  return gulp.src(settings.themeLocationEU + 'style.css')
    .pipe(browserSync.stream());
});

gulp.task('waitForScriptsPRO', ['scripts_pro'], function() {
  browserSync.reload();
});

// Plugin
gulp.task('waitForPluginStyles', ['pluginStyles'], function() {
  return gulp.src(settings.pluginLocation + 'assets/style.css')
    .pipe(browserSync.stream());
});

gulp.task('waitForPluginScripts', ['pluginScripts'], function() {
  browserSync.reload();
});
